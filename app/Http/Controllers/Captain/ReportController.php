<?php

namespace App\Http\Controllers\Captain;

use App\Http\Controllers\Controller\Backend;
use App\Models\AcceptedOrder;
use App\Models\Corporate;
use App\Models\Customer;
use App\Models\Driver;
use App\Models\Order;
use App\Models\OrderComment;
use Config;
use ExcelReport;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use PdfReport;

//use Elibyy\TCPDF\Facades\TCPDF;
class ReportController extends Controller
{
    private $guardType;

    public function __construct(Request $request)
    {
        Config::set('auth.defaults.guard', 'captain');
        $this->guardType = Config::get('auth.defaults.guard');
    }

    public function print_police(Request $request)
    {

        $ids = is_array($request->ids) ? $request->ids : explode(",", $request->ids);

        if (is_array($ids) && count($ids)) {
            $orders = Order::with(['payment_method', 'to_government', 'to_city'])
                ->whereIn('id', $ids)
                ->orderBy('receiver_name', 'ASC')
                ->get();

            foreach ($orders as $order) {
                $customer = Customer::where('id', $order->customer_id)->value('corporate_id');
                $order->corporteName = Corporate::where('id', $customer)->value('name');
            }
            $data = ['orders' => $orders];
            $pdf = PDF::loadView('captain.orders.pdf-orders', $data, [], ['format' => 'A5-L']);
            return $pdf->stream(time());
        } else {
            //error
            flash(__('captain.Must_Choose_Orders_To_Print'))->error();
            redirect()->back();
        }

    }

    public function captain_report(Request $request)
    {

        $driver = Auth::guard($this->guardType)->user();
        $name = $driver->name;
        $type = $request->type;
        $status = app('request')->input('status');
        $driver_id = app('request')->input('driver_id');
        $from_date = app('request')->input('from_date');
        $to_date = app('request')->input('to_date');
        $ownDrivers = Driver::where(function ($query) use ($driver) {
            $query->where('id', $driver->id)
                ->orWhere('manager_id', $driver->id);
        })->pluck('id')->toArray();

        Excel::create(str_replace(' ', '_', $name) . '_' . date('Y_m_d'), function ($excel) use ($ownDrivers, $type, $from_date, $to_date, $driver_id, $status) {

            $orders = Order::join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
                ->whereIn('accepted_orders.driver_id', $ownDrivers)
                ->where('accepted_orders.status', '!=', 4)
                ->with(['to_government', 'to_city', 'delivery_problems' => function ($query) {
                    $query->orderBy('created_at', 'DESC');
                }, 'order_delay' => function ($query) {
                    $query->orderBy('created_at', 'DESC');
                }, 'comments' => function ($query) {
                    $query->orderBy('created_at', 'DESC');
                }])
                ->select('orders.*')->orderBy('orders.id', 'desc');

            if ($driver_id) {
                $orders = $orders->where('accepted_orders.driver_id', $driver_id);
            }

            if ($from_date) {
                if ($type == 'ofd' || $status == 2) {
                    $orders = $orders->whereDate('orders.received_at', '>=', $from_date);
                } else {
                    $orders = $orders->whereDate('orders.created_at', '>=', $from_date);
                }
            }

            if ($to_date) {
                if ($type == 'ofd' || $status == 2) {
                    $orders = $orders->whereDate('orders.received_at', '<=', $to_date);
                } else {
                    $orders = $orders->whereDate('orders.created_at', '<=', $to_date);
                }

            }

            if ($status) {
                $orders = $orders->where('orders.status', $status)->where('accepted_orders.status', $status);
            }

            if ($type == 'ofd') {
                $orders = $orders->where('orders.status', 2)->where('accepted_orders.status', 2);
            } else if ($type == 'finish_not_dropped') {
                $orders = $orders->where(function ($query) {
                    $query->orWhere(function ($query) {
                        $query->where('orders.status', 5)
                            ->where('accepted_orders.status', 5)
                            ->where('orders.warehouse_dropoff', 0);
                    })
                        ->orWhere(function ($query) {
                            $query->where('orders.status', 8)
                                ->where('accepted_orders.status', 8)
                                ->where('orders.warehouse_dropoff', 0);
                        })
                        ->orWhere(function ($query) {
                            $query->where('orders.status', 3)
                                ->where('accepted_orders.status', 3)
                                ->where('delivery_status', 2)
                                ->where('orders.warehouse_dropoff', 0);
                        })
                        ->orWhere(function ($query) {
                            $query->where('orders.status', 3)
                                ->where('accepted_orders.status', 3)
                                ->where('delivery_status', 4)
                                ->where('orders.warehouse_dropoff', 0);
                        });
                });
            } else if ($type == 'finish_dropped') {
                $orders = $orders->where(function ($query) {
                    $query->orWhere(function ($query) {
                        $query->where('orders.status', 5)
                            ->where('accepted_orders.status', 5)
                            ->where('orders.warehouse_dropoff', 1);
                    })
                        ->orWhere(function ($query) {
                            $query->where('orders.status', 8)
                                ->where('accepted_orders.status', 8)
                                ->where('orders.warehouse_dropoff', 1);
                        })
                        ->orWhere(function ($query) {
                            $query->where('orders.status', 3)
                                ->where('accepted_orders.status', 3)
                                ->where('delivery_status', 2)
                                ->where('orders.warehouse_dropoff', 1);
                        })
                        ->orWhere(function ($query) {
                            $query->where('orders.status', 3)
                                ->where('accepted_orders.status', 3)
                                ->where('delivery_status', 4)
                                ->where('orders.warehouse_dropoff', 1);
                        });
                });
            }

            $orders = $orders->orderBy('orders.created_at');

//            $lastOrder = AcceptedOrder::selectRaw('status as order_status')
//                ->where('driver_id', $driver->driver_id)
//                ->where('status', '!=', 4)
//                ->whereColumn('order_id', 'orders.id')
//                ->latest()
//                ->limit(1)
//                ->getQuery();
//
//            $orders = Order::with(['customer' => function ($query) {
//                $query->with('corporate');
//            }])
//                ->join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
//                ->with(['to_government', 'to_city', 'delivery_problems' => function ($query) {
//                    $query->orderBy('created_at', 'DESC');
//                }, 'order_delay' => function ($query) {
//                    $query->orderBy('created_at', 'DESC');
//                }, 'comments' => function ($query) {
//                    $query->orderBy('created_at', 'DESC');
//                }])
//                ->select('orders.*')
//                ->selectSub($lastOrder, 'order_status')
//                ->where('accepted_orders.status', '!=', 4)
//                ->where('accepted_orders.driver_id', $request->driver_id)
//                ->groupBy('order_id')
//                ->orderBy('orders.created_at');

            $excel->setTitle('Orders');

            $excel->sheet('Orders', function ($sheet) use ($orders) {
                $sheet->loadView('captain.reports.captain-orders')->with(['orders' => $orders]);
            });

        })->download('xls');
    }

    public function update_status(Request $request)
    {
        if ($request->hasFile('file')) {
            $fileExt = $request->file('file')->getClientOriginalExtension();
            $file = 'sheet-' . time() . "." . $fileExt;
            $request->file("file")->move(public_path('api_uploades/sheets/'), $file);
            $driver = Auth::guard($this->guardType)->user();
            $ownDrivers = Driver::where(function ($query) use ($driver) {
                $query->where('id', $driver->id)
                    ->orWhere('manager_id', $driver->id);
            })->pluck('id')->toArray();

            $orders_errors = array();
            $file_has_data = false;

            Excel::filter('chunk')->load(public_path('api_uploades/sheets/' . $file))->chunk(250, function ($results) use (&$orders_errors, &$file_has_data, &$actions, $ownDrivers) {

                foreach ($results as $i => $row) {

//                    dd($row);
                    $file_has_data = true;
                    $row['validation_errors'] = '';
//                    if ($i == 0) {
//                        $orders_errors = array($row);
//                        continue;
//                    }
//
                    $order_number = trim($row['rkm_alshhn']);
                    $order_status = strtolower(trim($row['alhal']));
                    $delivery_problem = trim($row['taalykk_aal_alshhn']);
                    $collected_cost = trim($row['almblgh_almhsl']);

                    if (!in_array($order_status, ['تحت التسليم', 'الغاء قبل المقابلة', 'رفض وجهه لوجهه', 'تسليم كلي', 'استبدال', 'تسليم جزئي'])) {
                        $row['validation_errors'] = $row['validation_errors'] . ' / ' . 'حالة غير مقبولة';
                    }

                    if (!$order_number) {
                        continue;
                        $row['validation_errors'] = $row['validation_errors'] . ' / ' . 'رقم الشحنة مطلوب';
                    }

                    if (!$order_status) {
                        $row['validation_errors'] = $row['validation_errors'] . ' / ' . 'حالة الشحنة مطلوبة';
                    }

//                    if (!$delivery_problem) {
//                        $row['validation_errors'] = $row['validation_errors'] . ' / ' . 'التعليق مطلوب';
//                    }

                    $order = Order::join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
                        ->whereIn('accepted_orders.driver_id', $ownDrivers)
                        ->where('accepted_orders.status', '!=', 4)
//                        ->where('accepted_orders.status', 2)
//                        ->where('orders.status', 2)
                        ->where(function ($query) use ($order_number) {
                            $query->where('order_number', $order_number)
                                ->orWhere('order_no', $order_number)
                                ->orWhere('orders.id', $order_number);
                        })
                        ->select('orders.*')
                        ->first();

                    if (!$order) {
                        $row['validation_errors'] = $row['validation_errors'] . ' / ' . 'الشحنة غير موجودة';
                    }

                    if ($order && $order->status != 2) {
                        $row['validation_errors'] = $row['validation_errors'] . ' / ' . 'الشحنة تم تغيير حالتها مسبقا';
                    }

                    if (($collected_cost == '' && $collected_cost !== 0) && $order_status != 'الغاء قبل المقابلة' && $order_status != 'تحت التسليم') {
                        $row['validation_errors'] = $row['validation_errors'] . ' / ' . 'المبلغ المحصل مطلوب';
                    }

                    if ($row['validation_errors']) {
                        array_push($orders_errors, $row);
                        continue;
                    }

                    if ($delivery_problem) {
                        OrderComment::create([
                            'comment' => $delivery_problem,
                            'order_id' => $order->id,
                            'user_type' => 3,
                            'user_id' => Auth::guard($this->guardType)->user()->id,
                            'driver_id' => Auth::guard($this->guardType)->user()->id,
                            'latitude' => 0,
                            'longitude' => 0
                        ]);

//                        event(new OrderUpdated([$order->id],
//                            Auth::guard($this->guardType)->user()->id,
//                            1,
//                            'comments',
//                            ['comment' => $delivery_problem]
//                        ));
                    }

                    if ($order_status == 'الغاء قبل المقابلة') {
                        if ($order->status == 2) {
                            AcceptedOrder::where('order_id', $order->id)
                                ->where('status', 2)
                                ->update(['status' => 5]);
                            $order->status = 5;
                            $order->recalled_at = date("Y-m-d H:i:s");
                            $order->recalled_by = 2;
                            $order->delivery_status = 3;
                        }
                    } elseif ($order_status == 'رفض وجهه لوجهه') {
                        if ($order->status == 2) {
                            AcceptedOrder::where('order_id', $order->id)
                                ->where('status', 2)
                                ->update(['status' => 8]);
                            $order->status = 8;
                            $order->rejected_at = date("Y-m-d H:i:s");
                            $order->delivery_status = 5;
                        }
                    } elseif (in_array($order_status, ['تسليم كلي', 'استبدال', 'تسليم جزئي'])) {
                        if ($order->status == 2) {
                            AcceptedOrder::where('order_id', $order->id)
                                ->where('status', 2)
                                ->update(['status' => 3, 'delivery_date' => date("Y-m-d H:i:s")]);
                            $order->status = 3;
                            $order->delivered_at = date("Y-m-d H:i:s");
                        }
                        $order->delivery_status = ($order_status == 'تسليم كلي' ? 1 : ($order_status == 'تسليم جزئي' ? 2 : 4));
                    }

                    if ($collected_cost || $collected_cost === 0) {
                        $order->collected_cost = $collected_cost;
                    } elseif ($order_status == 'الغاء قبل المقابلة') {
                        $order->collected_cost = 0;
                    }

                    $order->save();

                }

            }, false);

            File::delete(public_path('api_uploades/sheets/' . $file));

            if (count($orders_errors)) {
                $this->orders_status_errors($orders_errors);
            }

            if ($file_has_data) {
                return redirect()->back()->with('success', 'Orders Saved');
            } else {
                return redirect()->back()->with('warning', 'File is empty');
            }

        }
    }

    public function orders_status_errors($orders_errors)
    {
        $data[] = [];

        $data = array(array(
            'rkm_alshhn' => 'رقم الشحنة',
            'taalykk_aal_alshhn' => 'تعليقك على الشحنة',
            'alhal' => 'الحالة',
            'almblgh_almhsl' => 'المبلغ المحصل',
            'اﻷخطاء',
        ));

        foreach ($orders_errors as $i => $row) {
            array_push($data, array(
                $row['rkm_alshhn'],
                $row['taalykk_aal_alshhn'],
                $row['alhal'],
                $row['almblgh_almhsl'],
                $row['validation_errors'],
            ));
        }

        Excel::create('captain_update_status_result', function ($excel) use ($data) {
            $excel->sheet('Orders', function ($sheet) use ($data) {
                $sheet->fromArray($data, null, 'A1', false, false);
                // Set width for multiple cells
                for ($intRowNumber = 1; $intRowNumber <= count($data) + 1; $intRowNumber++) {
                    $sheet->setSize('A' . $intRowNumber, 20, 10);
                    $sheet->setSize('B' . $intRowNumber, 20, 19);
                    $sheet->setSize('C' . $intRowNumber, 20, 20);
                    $sheet->setSize('D' . $intRowNumber, 60, 18);
                    $sheet->setSize('E' . $intRowNumber, 60, 18);
                }

//                $sheet->setWidth(array(
//                    'A' => 20,
//                    'B' => 20,
//                    'C' => 60,
//                ));

                $sheet->cells('E1:E' . count($data), function ($cells) {
                    // manipulate the range of cells
                    $cells->setBackground('#ed1c24');
                    $cells->setBorder('solid');
                });

                $sheet->setStyle(array(
                    'font' => array(
                        'name' => 'Arial',
                        'size' => 10,
                        'bold' => false,
                        'Alignment' => 'center',
                    )
                ));

                $sheet->row(1, function ($row) {
                    // call row For Header
                    $row->setBackground('#002060');
                    $row->setFontColor('#ffffff');
                    $row->setFont(array(
                        'family' => 'Arial',
                        'size' => '14',
                        'bold' => false
                    ));
                    $row->setAlignment('center');

                });

            });
        })->export('xls');

    }
}

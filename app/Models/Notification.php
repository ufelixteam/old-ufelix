<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $guarded = ['id'];

    protected $appends = ['title', 'message'];


    public function getTitleAttribute()
    {
        if (session()->has('lang') == 'ar') {
            return $this->title_ar;
        } else {
            return $this->title_en;
        }
    }

    public function getMessageAttribute()
    {
        if (session()->has('lang') == 'ar') {
            return $this->message_ar;
        } else {
            return $this->message_en;
        }
    }

    public function getTypeAttribute($value)
    {
        if ($value == 1) {
            return "<span class='badge badge-pill label-info'>" . __('backend.notification') . "</span>";
        } else if ($value == 2) {
            return "<span class='badge badge-pill label-success'>" . __('backend.order') . "</span>";
        } else if ($value == 3) {
            return "<span class='badge badge-pill label-info'>" . __('backend.wallet') . "</span>";
        }
    }

    public function getUserAttribute()
    {
        /*
        * client_model == 1 :: Driver
        * client_model == 2 :: Customer
        * client_model == 3 :: User
        */
        if ($this->client_model == 1) {
            return Driver::where('id', $this->client_id)->first();
        } else if ($this->client_model == 2) {
            return Customer::where('id', $this->client_id)->first();
        } else if ($this->client_model == 3) {
            return User::where('id', $this->client_id)->first();
        }
    }

    public function getUserSpanAttribute()
    {
        /*
        * client_model == 1 :: Captain
        * client_model == 2 :: Agent
        * client_model == 3 :: Individual
        * client_model == 4 :: Corporate
        * client_model == 5 :: All
        */
        if ($this->client_model == 1) {
            return "<span class='badge badge-pill label-info'>" . __('backend.captains') . "</span>";
        } else if ($this->client_model == 2) {
            return "<span class='badge badge-pill label-warning'>" . __('backend.customers') . "</span>";
        } else if ($this->client_model == 3) {
            return "<span class='badge badge-pill label-info'>" . __('backend.individuals') . "</span>";
        } else if ($this->client_model == 4) {
            return "<span class='badge badge-pill label-info'>" . __('backend.corporate') . "</span>";
        } else if ($this->client_model == 5) {
            return "<span class='badge badge-pill label-info'>" . __('backend.all') . "</span>";
        }
    }

    public function getReadSpanAttribute()
    {
        /*
        * is_read == 0 :: Not Read
        * is_read == 1 :: Read
        */
        if ($this->is_read == 1) {
            $value = "<span class='badge badge-pill label-success'><i class='fa fa-check' aria-hidden='true'></i></span>";
        } else {
            $value = "<span class='badge badge-pill label-danger'><i class='fa fa-times' aria-hidden='true'></i></span>";
        }
        return $value;
    }

}

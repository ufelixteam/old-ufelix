@extends('frontend.profile.layouts.app')

@section('title')
    @lang('website.edit_order')
@endsection
@section('css')
    <style>
        .pac-container {
            z-index: 9999 !important;
        }

    </style>
@endsection
{{--@section('css')--}}

{{--    <link href="{{asset('public/profile/assets/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">--}}
{{--@endsection--}}

@section('content')



    <div class="row">
        <div class="col-12">
            <div class="">
                <div class="table-responsive m-t-40">
                    <h1 class="title">@lang('website.edit_order')</h1>
                    @if(! empty($order) && $order)
                        <form action="{{ route('profile.order_update', $order->id) }}" method="POST">
                            <input type="hidden" name="_method" value="PUT">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="corparate_id" id="corparate_id" value="{{@Auth::guard('Customer')->user()->corporate_id}}">
                            <div class="container-fluid">
                                @include('flash::message')
                            </div>
                            <div class="container-fluid">
                                <div class="row">

                                    <div class="col-md-6 col-xs-12">
                                        <div class="ribbon-wrapper card">
                                            <div class="ribbon ribbon-default">Receiver Data</div>

                                            <div class="form-group @if($errors->has('receiver_name')) has-error @endif">
                                                <div class="row">
                                                    <label class="col-sm-4"
                                                           style="font-size: 14px;margin-top: 5px;">@lang('website.ReceiverName')
                                                        : </label>
                                                    <div class="col-12 col-md-8">
                                                        <input type="text" id="receiver_name-field" name="receiver_name"
                                                               class="form-control"
                                                               value="{{ ! empty($order) ? $order->receiver_name : old("receiver_name") }}"/>
                                                        @if($errors->has("receiver_name"))
                                                            <span
                                                                class="help-block">{{ $errors->first("receiver_name") }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div
                                                class="form-group @if($errors->has('receiver_mobile')) has-error @endif">
                                                <div class="row">
                                                    <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;">Mobile
                                                        Number: </label>
                                                    <div class="col-12 col-md-8">
                                                        <input type="number" onkeypress="return event.charCode >= 48"
                                                               min="1" id="receiver_mobile-field"
                                                               name="receiver_mobile" class="form-control"
                                                               value="{{ ! empty($order) ? $order->receiver_mobile : old("receiver_mobile") }}"/>
                                                        @if($errors->has("receiver_mobile"))
                                                            <span
                                                                class="help-block">{{ $errors->first("receiver_mobile") }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div
                                                class="form-group @if($errors->has('r_government_id') || $errors->has('r_state_id')) has-error @endif">
                                                <div class="row">
                                                    <label class="col-sm-4">Government: </label>
                                                    <div class="col-12 col-md-4">

                                                        <select class=" select2 form-control r_government_id"
                                                                id="r_government_id"
                                                                name="r_government_id">
                                                            <option value="" data-display="Select"> Government</option>
                                                            @if(! empty($governments))
                                                                @foreach($governments as $government)
                                                                    <option
                                                                        value="{{$government->id}}" {{ ! empty($order) && $order->r_government_id  == $government->id ? 'selected':  '' }} >
                                                                        {{$government->name_en}}
                                                                    </option>

                                                                @endforeach
                                                            @endif

                                                        </select>
                                                        @if($errors->has("r_government_id"))
                                                            <span
                                                                class="help-block">{{ $errors->first("r_government_id") }}</span>
                                                        @endif
                                                        <div class="has-error hidden" id="no_fees">
                                                            <span class="help-block">{{__('backend.no_fees')}}</span>
                                                        </div>
                                                    </div>

                                                    <label class="col-sm-1"> City: </label>
                                                    <div class="col-12 col-md-3">
                                                        <select class="select2 form-control r_state_id" id="r_state_id"
                                                                name="r_state_id">
                                                            <option value=""> City</option>

                                                            @if(! empty($r_cities))
                                                                @foreach($r_cities as $r_city)
                                                                    <option
                                                                        value="{{$r_city->id}}" {{ ! empty($order) && $order->r_state_id  == $r_city->id ? 'selected':  '' }} >
                                                                        {{$r_city->name_en}}
                                                                    </option>

                                                                @endforeach
                                                            @endif

                                                        </select>
                                                        @if($errors->has("r_state_id"))
                                                            <span
                                                                class="help-block">{{ $errors->first("r_state_id") }}</span>
                                                        @endif
                                                    </div>

                                                </div>
                                            </div>

                                            <div
                                                class="form-group @if($errors->has('receiver_address')) has-error @endif">
                                                <div class="row">
                                                    <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;">Receiver
                                                        Address: </label>
                                                    <div class="col-12 col-md-8">
                                                        <input type="text" id="receiver_phone-field"
                                                               name="receiver_address" class="form-control"
                                                               value="{{ ! empty($order) ? $order->receiver_address : old("receiver_address") }}"/>
                                                        @if($errors->has("receiver_address"))
                                                            <span
                                                                class="help-block">{{ $errors->first("receiver_address") }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div
                                                class="form-group @if($errors->has('receiver_latitude') || $errors->has('receiver_longitude')) has-error @endif">
                                                <div class="row">
                                                    <label class="col-sm-4"> Location (optional): </label>
                                                    <div class="col-12 col-md-8">
                                                        <div class="row">
                                                            <div class="col-2 col-md-2">
                                                                <i class="icon-map-pin" data-toggle="modal"
                                                                   data-target="#map1Modal"
                                                                   data-whatever="@fat"
                                                                   style=""></i>

                                                            </div>
                                                            <div class="col-5 col-md-5">
                                                                <input class="form-control" type="text"
                                                                       name="receiver_latitude"
                                                                       id="receiver_latitude" placeholder="Latitude"
                                                                       value="{{ ! empty($order) ? $order->receiver_latitude : old(" receiver_latitude") }}">
                                                                @if($errors->has("receiver_latitude"))
                                                                    <span
                                                                        class="help-block">{{ $errors->first("receiver_latitude") }}</span>
                                                                @endif
                                                            </div>
                                                            <div class="col-5 col-md-5">
                                                                <input class="form-control" type="text"
                                                                       name="receiver_longitude"
                                                                       id="receiver_longitude" placeholder="Longitude"
                                                                       value="{{ ! empty($order) ? $order->receiver_longitude : old(" receiver_longitude") }}">
                                                                @if($errors->has("receiver_longitude"))
                                                                    <span
                                                                        class="help-block">{{ $errors->first("receiver_longitude") }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="ribbon-wrapper card">
                                            <div class="ribbon ribbon-danger">Order Data</div>

                                            <div class="form-group @if($errors->has('type')) has-error @endif">
                                                <div class="row">
                                                    <label class="col-sm-2" style="font-size: 14px;margin-top: 5px;">Order
                                                        Type: </label>
                                                    <div class="col-12 col-md-10">
                                                        <input type="text" id="type-field" name="type"
                                                               class="form-control"
                                                               value="{{ ! empty($order) ? $order->type : old("type") }}"/>
                                                        @if($errors->has("type"))
                                                            <span class="help-block">{{ $errors->first("type") }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group @if($errors->has('reference_number')) has-error @endif">
                                                <div class="row">
                                                    <label class="col-sm-2" style="font-size: 14px;margin-top: 5px;">Reference Number: </label>
                                                    <div class="col-12 col-md-10">
                                                        <input type="text" id="reference_number-field" name="reference_number"
                                                               class="form-control"
                                                               value="{{ ! empty($order) ? $order->reference_number : old("reference_number") }}"/>
                                                        @if($errors->has("reference_number"))
                                                            <span class="help-block">{{ $errors->first("reference_number") }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div
                                                class="form-group @if($errors->has('delivery_type') || $errors->has('sender_subphone')) has-error @endif">
                                                <div class="row">

                                                    <label class="col-sm-2" style="font-size: 14px;margin-top: 20px;">Delivery Type: </label>

                                                    <div class="col-md-4">

                                                        <input type="text" id="delivery_type-field" name="delivery_type"
                                                               class="form-control"
                                                               onkeypress="return event.charCode >= 48" min="1"
                                                               value="{{ ! empty($order) ? $order->delivery_type : old("delivery_type") }}"/>
                                                        @if($errors->has("delivery_type"))
                                                            <span class="help-block">{{ $errors->first("delivery_type") }}</span>
                                                        @endif
                                                    </div>

                                                    <label class="col-sm-2" style="font-size: 14px;margin-top: 20px;">Sender Subphone: </label>

                                                    <div class="col-md-4">

                                                        <input type="text" id="sender_subphone-field" name="sender_subphone"
                                                               class="form-control"
                                                               value="{{ ! empty($order) ? $order->sender_subphone : old("sender_subphone") }}"/>
                                                        @if($errors->has("sender_subphone"))
                                                            <span class="help-block">{{ $errors->first("sender_subphone") }}</span>
                                                        @endif
                                                    </div>


                                                </div>
                                            </div>


                                            <div
                                                class="form-group @if($errors->has('main_store') || $errors->has('store_id')) has-error @endif">
                                                <div class="row">
                                                    <label class="col-sm-2">Pick-up Center:</label>
                                                    <div class="col-md-4">
                                                        <select id="store_id-field" name="main_store"
                                                                class="form-control"
                                                                @if($stores->isEmpty()) readonly @endif>
                                                            <option value="">Choose Pick-up Center</option>
                                                            @if(! empty($allStores))
                                                                @foreach($allStores as $store)
                                                                    <option
                                                                        value="{{$store->id}}" {{ $store->id == old('main_store') || $store->id == @$order->main_store ? 'selected' : '' }} >
                                                                        {{$store->name}}
                                                                    </option>
                                                                @endforeach
                                                            @endif

                                                        </select>
                                                        @if($errors->has("main_store"))
                                                            <span
                                                                class="help-block">{{ $errors->first("main_store") }}</span>
                                                        @endif
                                                    </div>

                                                    <label class="col-sm-2">Stock: </label>

                                                    <div class="col-md-4">
                                                        <select id="stock_id-field" name="store_id" class="form-control"
                                                                @if($stores->isEmpty()) readonly @endif>
                                                            <option value="">Choose Stock</option>
                                                            @if(! empty($stores))
                                                                @foreach($stores as $stock)
                                                                    <option
                                                                        value="{{$stock->id}}" {{ $stock->id == old('store_id') || $stock->id == @$order->store_id ? 'selected' : '' }} >
                                                                        {{$stock->title}}
                                                                    </option>
                                                                @endforeach
                                                            @endif


                                                        </select>
                                                        @if($errors->has("store_id"))
                                                            <span
                                                                class="help-block">{{ $errors->first("store_id") }}</span>
                                                        @endif

                                                    </div>
                                                </div>
                                            </div>

                                            <div
                                                class="form-group @if($errors->has('qty') || $errors->has('overload')) has-error @endif">
                                                <div class="row">

                                                    <label class="col-sm-2" style="font-size: 14px;margin-top: 20px;">Quantity
                                                        Order: </label>


                                                    <div class="col-md-4">

                                                        <input class="form-control"
                                                               @if( ! empty($order->stock) ) max="{{$order->stock->available_quantity}}"
                                                               @endif id="qty-field" type="number" name="qty"
                                                               placeholder="Quantity"
                                                               @if($stores->isEmpty()) readonly @endif value="{{ !
                                empty($order) ? $order->qty : old("qty") }}">
                                                        Available Quantity: <span id="available_quantity">0</span>
                                                        @if($errors->has("qty"))
                                                            <span
                                                                class="help-block">{{ $errors->first("qty") }}</span>
                                                        @endif
                                                    </div>


                                                    <label class="col-sm-2" style="font-size: 14px;margin-top: 20px;">Overweight: </label>


                                                    <div class="col-md-4">

                                                        <input type="number" required id="overload-field"
                                                               name="overload" class="form-control"
                                                               placeholder="Overweight"
                                                               value="{{ ! empty($order) ? $order->overload : old(" overload") }}"
                                                               readonly/>
                                                        @if($errors->has("overload"))
                                                            <span
                                                                class="help-block">{{ $errors->first("overload") }}</span>
                                                        @endif
                                                    </div>


                                                </div>
                                            </div>

                                            <div
                                                class="form-group @if($errors->has('delivery_price') || $errors->has('order_price')) has-error @endif">
                                                <div class="row">

                                                    <label class="col-sm-2" style="font-size: 14px;margin-top: 20px;">Delivery
                                                        Fees: </label>

                                                    <div class="col-md-4">

                                                        <input class="form-control" type="number"
                                                               id="delivery_price-field" name="delivery_price"
                                                               placeholder="Delivery price"
                                                               value="{{ ! empty($order) ? $order->delivery_price : old(" delivery_price") }}"
                                                               readonly>
                                                        @if($errors->has("delivery_price"))
                                                            <span
                                                                class="help-block">{{ $errors->first("delivery_price") }}</span>
                                                        @endif

                                                    </div>

                                                    <label class="col-sm-2" style="font-size: 14px;margin-top: 20px;">Order
                                                        Price : </label>
                                                    <div class="col-md-4">

                                                        <input class="form-control" id="order_price-field" type="number"
                                                               name="order_price"
                                                               value="{{ ! empty($order) ? $order->order_price : old(" order_price") }}"
                                                               placeholder="Order Price">
                                                        @if($errors->has("order_price"))
                                                            <span
                                                                class="help-block">{{ $errors->first("order_price") }}</span>
                                                        @endif
                                                    </div>


                                                </div>
                                            </div>


                                            <div
                                                class="form-group @if($errors->has('payment_method_id')) has-error @endif">
                                                <div class="row">


                                                    <label class="col-sm-2" style="font-size: 14px;margin-top: 20px;">Delivery
                                                        Fees From : </label>
                                                    <div class="col-md-4">

                                                        <select id="payment_method_id-field" name="payment_method_id"
                                                                class="form-control select2">
                                                            <option value="" data-display="Select">Choose Method
                                                            </option>

                                                            @if(! empty($payments))
                                                                @foreach($payments as $payment)
                                                                    <option value="{{$payment->id}}" {{ ! empty($order) && $order->payment_method_id ==
                                    $payment->id ? 'selected': '' }}>
                                                                        {{$payment->name}}
                                                                    </option>
                                                                @endforeach
                                                            @endif

                                                        </select>
                                                        @if($errors->has("payment_method_id"))
                                                            <span
                                                                class="help-block">{{ $errors->first("payment_method_id") }}</span>
                                                        @endif
                                                    </div>


                                                    <label class="col-sm-2" style="font-size: 14px;margin-top: 20px;">Total
                                                        Price: </label>

                                                    <div class="col-md-4">

                                                        <div class="form-control"
                                                             id="total_price">{{@$order->total_price}}</div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="form-group @if($errors->has('notes')) has-error @endif">
                                                <div class="row">
                                                    <label class="col-sm-2"
                                                           style="font-size: 14px;margin-top: 5px;">Notes: </label>
                                                    <div class="col-12 col-md-10">
                                                    <textarea class="form-control" rows="3"
                                                              name="notes"
                                                              style="min-height: 150px;resize: vertical;"> {{! empty($order->notes) ? $order->notes : '-' }} </textarea>
                                                        @if($errors->has("notes"))
                                                            <span
                                                                class="help-block">{{ $errors->first("notes") }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>

                                <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
                                    <button type="submit" class="btn btn-primary">{{__('backend.save_edits')}}</button>
                                </div>
                            </div>
                        </form>
                    @else
                        <div class="container-fluid">

                            <div class="alert alert-danger text-center">
                                <p>
                                    No Order Found
                                </p>
                            </div>

                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    @include('frontend.profile.collections.map')
    @include('frontend.profile.collections.map1')

@endsection

@section('scripts')

    <script type="text/javascript" src="{{ asset('/assets/scripts/map-order.js')}}"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key=AIzaSyDQiN4qc7pC9b1BRQwkqXXD28peMfWcHvw&callback=myMap"></script>
    @include('frontend.profile.collections.js')
@endsection
{{--@section('scripts')--}}
{{--    <!-- Sweet-Alert  -->--}}
{{--    <script src="{{asset('public/Profile/assets/plugins/sweetalert/sweetalert.min.js') }}"></script>--}}
{{--    <script src="{{asset('public/Profile/assets/plugins/sweetalert/jquery.sweet-alert.custom.js') }}"></script>--}}


{{--@endsection--}}

@extends('backend.layouts.app')

@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> Customers Feedback
        </h3>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <table id="driversTable" class="table table-condensed table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>User</th>
                        <th>Title</th>
                        <th>Message</th>
                        <th>Created at</th>
                        <th class="text-right"></th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($list as $value)
                        <tr>
                            <td>{{$value->id}}</td>
                            @if(!empty($value->customer_id))
                                <td> <a href="{{ url('mngrAdmin/customers',$value->customer_id) }}">{{ App\Models\Customer::findOrFail($value->customer_id)->name }}</a></td>
                            @elseif(!empty($value->captain_id))
                                <td> <a href="{{ url('mngrAdmin/drivers',$value->captain_id) }}">{{ App\Models\Customer::findOrFail($value->captain_id)->name }}</a></td>
                            @endif
                            <td>{{$value->title}}</td>
                            <td>{{$value->message}}</td>
                            <td>{{$value->created_at->diffForHumans()}}</td>

                            <td class="text-right">
                                {{--<a class="btn btn-xs btn-primary" href="{{ url('mngrAdmin/feedback/show', $value->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>--}}

                                {{--<form action="{{ route('mngrAdmin.weights.destroy', $weight->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">--}}
                                    {{--<input type="hidden" name="_method" value="DELETE">--}}
                                    {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
                                    {{--<button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>--}}
                                {{--</form>--}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $list->links() }}
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $('#driversTable').DataTable({
            "bPaginate": false
        });
    </script>
@endsection
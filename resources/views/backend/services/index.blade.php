@extends('backend.layouts.app')
@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <style>
        .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20px; }
        .toggle.ios .toggle-handle { border-radius: 20px; }
    </style>
@endsection
@section('header')
<div class="page-header clearfix">
    <h3>
        <i class="fa fa-align-justify"></i> @if(! empty($title)) {{ $title}} @endif
         <a class="btn btn-success pull-right" href="{{ url('/mngrAdmin/services/create?type='.$type) }}"><i class="fa fa-plus"></i> {{__('backend.NEW')}} @if(! empty($title)) {{ $title}} @endif </a>
    </h3>

</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12 table-responsive">
        @if($services->count())
            <table class="table table-condensed table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th width="10%">@lang('backend.Image')</th>

                        <th>@lang('backend.Title_en')</th>
                        <th>@lang('backend.Small_description_ar')</th>
                        <th>@lang('backend.Publish')</th>
                        <th class="text-right"></th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($services as $project)
                        <tr>
                            <td>{{$project->id}}</td>
                        <td><img src="{{$project->image}}" width="100%" /></td>

                        <td>{{$project->title_en}}</td>
                        <td>{{$project->small_description_en}}</td>

                        <td>
                            <input data-id="{{$project->id}}" data-size="mini"  class="toggle change_publish "  {{$project->is_publish == 1 ? 'checked' : ''}} data-onstyle="success" type="checkbox" data-style="ios">

                        </td>


                        <td class="text-right">
                            <a class="btn btn-xs btn-primary" href="{{ route('mngrAdmin.services.show', $project->id) }}"><i class="fa fa-eye-open"></i> {{__('backend.VIEW')}}</a>
                            <a class="btn btn-xs btn-warning" href="{{ route('mngrAdmin.services.edit', $project->id) }}"><i class="fa fa-edit"></i> {{__('backend.EDIT')}}</a>

                        </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {!! $services->appends($_GET)->links() !!}
        @else
            <h3 class="text-center  alert alert-warning">{{__('backend.NO_DATA')}}</h3>
        @endif

    </div>
</div>

@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <script>
        $(function() {
            $('.toggle').bootstrapToggle();
            $('.change_publish').on('change',function(){
                let id = $(this).attr('data-id');
                $.ajax({
                    url: '{{URL::asset("/mngrAdmin/publish-service")}}',
                    type: 'post',
                    data:{id:id,_token:"{{csrf_token()}}"},
                    success: function(data) {

                    },
                    error: function(data) {
                        console.log('Error:', data);
                    }
                });
            })


        })
    </script>
@endsection

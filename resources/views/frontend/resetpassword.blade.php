@extends('frontend.layouts.app')

@section('title') @lang('website.ForgotTitle') @endsection

@section('content')


<section class="req body-in">
  <div class="container">
      <div class="col-md-6 col-md-offset-3 col-xs-12">
          <div class="req-x">
            <div class="modal-body mb-1">
                <div class="title">
                    <h3>@lang('website.ForgotTitle')</h3>
                    <span class="text-warning"><i class="fas fa-info-circle prefix"></i>@lang('website.ForgotMessage')</span>
                </div>
                @include('flash::message')
                <form action="{{  app()->getLocale() == 'en' ? url('/en/resetpassword') : url('resetpassword')}}" method="post" name="forgotForm" id="forgotForm">
                  {{csrf_field()}}

                    <input type="hidden"  id="mobile" name="mobile" value="{{! empty($mobile) ? $mobile : ''}}" />    
                   <input type="hidden"  id="code" name="code" value="{{! empty($code) ? $code : ''}}" />  

                   <div class="form-group @if($errors->has('password')) has-error @endif">
                        <input type="password" id="password" name="password"  placeholder="@lang('website.Password')" class="form-control" />
                        <i class="fa fa-lock"></i>
                        @if($errors->has("password"))
                            <span class="help-block">{{ $errors->first("password") }}</span>
                        @endif 
                    </div>
                    <div class="form-group @if($errors->has('password_confirmation')) has-error @endif">
                        <input type="password" id="password_confirmation" name="password_confirmation"  placeholder="@lang('website.ConfirmPassword')" class="form-control" />
                        <i class="fa fa-lock"></i>
                        @if($errors->has("password_confirmation"))
                            <span class="help-block">{{ $errors->first("password_confirmation") }}</span>
                        @endif 
                    </div>

                    

                     <div class="form-group text-center">
                        <button class="btn btn-info" type="submit"> @lang('website.ForgotBtn') </button>
                    </div>
                </form>

                <div class="my-account text-center">

                    <a href="{{ app()->getLocale() == 'en' ? url('/en/login') : url('/login') }}">
                        @lang('website.ReturnLogin')
                    </a>
                </div>
            </div>
      </div>
    </div>
    <!--/.Content-->
  </div>
</section>

 @endsection

  @section('scripts')
<script src="{{asset('/assets/scripts/jquery.validate.js')}}"></script>

<script>

    $(function(){
        $("form[name='forgotForm']").validate({
            rules: {
                 password: {
                    required: true,
                },
                password_confirmation: {
                    required: true,
                    equalTo: '#password'
                }
            },
            // Specify validation error messages
            messages: {
                password_confirmation:{
                    required: "{{__('backend.Confirm_password_is_wrong')}}",
                    equalTo: "{{__('backend.Confirm_password_is_wrong')}}",
                },
                password: {
                    required: "{{__('website.passwordLoginRequired')}}"
                }
            },

            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error)
            
            },
            highlight: function (element) {
                $(element).parent().addClass('error help-block');
                $(element).parent().find('label').addClass(' help-block');
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('error');
                $(element).parent().find('label').removeClass(' help-block');

            },
           
            submitHandler: function (form) {
                form.submit();
            }
        });
    })
  </script>
@endsection
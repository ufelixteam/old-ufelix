<table id="theTable" class="table table-condensed table-striped text-center">
    <thead>
      <tr>
        <th><?php echo e(__('backend.customer')); ?></th>
        <th><?php echo e(__('backend.receiver_name')); ?></th>
        <th><?php echo e(__('backend.Receiver_Mobile')); ?></th>
        <th><?php echo e(__('backend.Receiver_City')); ?></th>

        <th><?php echo e(__('backend.sender_name')); ?></th>
        <th><?php echo e(__('backend.Sender_Mobile')); ?></th>
        <th><?php echo e(__('backend.sender_City')); ?></th>
        <th></th>

      </tr>
    </thead>
    <tbody id="table-order">
      <?php if(! empty($orders) && count($orders) >0  ): ?>
        <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <tr>
          <td><?php echo e(! empty($order->customer )? $order->customer->name : ''); ?></td>
          <td><?php echo e($order->receiver_name); ?></td>
          <td><?php echo e($order->receiver_mobile); ?></td>

          <td><?php echo e(! empty($order->receiver_city )? $order->receiver_city->name_en : ''); ?></td>
          <td><?php echo e($order->sender_name); ?></td>
          <td><?php echo e($order->sender_mobile); ?></td>

          <td><?php echo e(! empty($order->sender_city )? $order->sender_city->name_en : ''); ?></td>


          <td><a class="btn btn-warning view-order" data-value="<?php echo e($order->id); ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
            <a class="btn btn-danger delete-order" data-value="<?php echo e($order->id); ?>"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
          </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      <?php endif; ?>
    </tbody>
  </table>
<?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/orders/tmptable.blade.php ENDPATH**/ ?>
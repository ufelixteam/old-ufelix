@extends('backend.layouts.app')
@section('css')
    <title>About Us</title>


    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <style>
        .toggle.ios, .toggle-on.ios, .toggle-off.ios {
            border-radius: 20px;
        }

        .toggle.ios .toggle-handle {
            border-radius: 20px;
        }
    </style>
@endsection
@section('header')
    <div class="content-header">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> ABOUT-Us
        </h3>
    </div>
@endsection

@section('content')

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row">

            @if ($message = Session::get('message'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if($settings->count())
                <form action="{{ URL::asset('/mngrAdmin/setting_update') }}" method="POST">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    @foreach($settings as $setting)

                        @if($setting->display != "terms")

                            @if($setting->key == "smsprovider")

                                <div class="form-group col-md-6 @if($errors->has($setting->key)) has-error @endif">
                                    <label for="key-field">{{ ucwords(str_replace("_", " ", $setting->key))}}</label>
                                    <select class="form-control"
                                            name="{{$setting->key}}" class="form-control"
                                    >{{$setting->value}}
                                        <option
                                            value="victorlink" {{ $setting->value =='victorlink'  ? 'selected' : ''}}>
                                            VICTORY link SMS
                                        </option>
                                        <option value="smsmsr" {{ $setting->value =='smsmsr'  ? 'selected' : ''}}>SMS
                                            Misr
                                        </option>
                                    </select>
                                    @if($errors->has("$setting->key"))
                                        <span class="help-block">{{ $errors->first("$setting->key") }}</span>
                                    @endif
                                </div>


                            @elseif($setting->options == "-1")
                                <div class="row col-md-12">
                                    <div class="form-group col-sm-6 @if($errors->has($setting->key)) has-error @endif">
                                        <label
                                            for="key-field">{{ ucwords(str_replace("_", " ", $setting->display))}}</label>
                                        <textarea id="key-field" name="{{$setting->key}}" class="form-control"
                                                  rows="5">{{$setting->value}}</textarea>
                                        @if($errors->has("$setting->key"))
                                            <span class="help-block">{{ $errors->first("$setting->key") }}</span>
                                        @endif
                                    </div>

                                    <div class="form-group col-sm-6 @if($errors->has($setting->key)) has-error @endif">
                                        <label for="key-field">{{ ucwords(str_replace("_", " ", $setting->display))}}
                                            AR</label>

                                        <textarea id="key-field" name="{{$setting->key.'_ar'}}" class="form-control"
                                                  rows="5">{{$setting->value_ar}}</textarea>

                                        @if($errors->has("$setting->key"))
                                            <span class="help-block">{{ $errors->first("$setting->key") }}</span>
                                        @endif
                                    </div>
                                </div>

                            @elseif($setting->options == "2")
                                <div class="form-group col-sm-6 @if($errors->has($setting->key)) has-error @endif">
                                    <label
                                        for="key-field">{{ ucwords(str_replace("_", " ", $setting->display))}}</label>
                                    <br>
                                    <input data-size="mini" class="toggle change_active" name="{{$setting->key}}"
                                           {{$setting->value == 1 ? 'checked' : ''}} data-onstyle="success" value="1"
                                           type="checkbox" data-style="ios" data-on="Yes" data-off="No">
                                    @if($errors->has("$setting->key"))
                                        <span class="help-block">{{ $errors->first("$setting->key") }}</span>
                                    @endif
                                </div>
                            @else

                                <div class="form-group col-sm-6 @if($errors->has($setting->key)) has-error @endif">
                                    <label
                                        for="key-field">{{ ucwords(str_replace("_", " ", $setting->display))}}</label>

                                    <input type="text" id="key-field" name="{{$setting->key}}" class="form-control"
                                           value="{{$setting->value}}"/>
                                    @if($errors->has("$setting->key"))
                                        <span class="help-block">{{ $errors->first("$setting->key") }}</span>
                                    @endif
                                </div>

                            @endif

                        @else


                            <div class="row col-md-12">


                                <div class="form-group col-md-6 @if($errors->has($setting->key)) has-error @endif">
                                    <label for="key-field">{{ ucwords(str_replace("_", " ", $setting->key))}}</label>
                                    <textarea type="text" class="form-control summernote" rows="10" cols="80"
                                              name="{{$setting->key}}" class="form-control"
                                              value="{{$setting->value}}">{{$setting->value}}</textarea>
                                    @if($errors->has("$setting->key"))
                                        <span class="help-block">{{ $errors->first("$setting->key") }}</span>
                                    @endif
                                </div>

                                <div class="form-group col-md-6  @if($errors->has($setting->key)) has-error @endif">
                                    <label
                                        for="key-field"> {{ ucwords(str_replace("_", " ", $setting->key_ar))}}</label>
                                    <textarea type="text" id="editor1" class="form-control summernote"
                                              name="{{$setting->key."_ar"}}" rows="10" cols="80" class="form-control"
                                              value="{{$setting->value_ar}}">{{$setting->value_ar}} </textarea>
                                    @if($errors->has("$setting->key"))
                                        <span class="help-block">{{ $errors->first("$setting->key") }}</span>
                                    @endif
                                </div>


                            </div>
                        @endif


                    @endforeach


                    <div class="well well-sm col-sm-12 col-md-12">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <a class="btn btn-link pull-right" href="{{ url('mngrAdmin/settings') }}"><i
                                class="glyphicon glyphicon-backward"></i> Back</a>
                    </div>
                </form>

            @endif

        </div>

    </div>

@endsection
@section("scripts")
    <!-- include summernote css/js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
    <script>
        $('.summernote').summernote();
        $('.toggle').bootstrapToggle();
    </script>
@endsection

@extends('backend.layouts.app')
@section('css')

    <title>{{__('backend.Order_Collection')}} - {{__('backend.add_Collection')}}</title>
@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-plus"></i> {{__('backend.Order_Collection')}}
            / {{__('backend.add_Collection')}} </h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <form action="{{ route('mngrAdmin.collection_orders.store') }}" method="POST" name="collection_create"
                  enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group col-md-6 col-sm-6 @if($errors->has('customer')) has-error @endif">
                    <label for="name-field">{{__('backend.corporate')}}</label>
                    <select id="corporate" name="corporate_id" data-dependent="customer" class="form-control"
                            value="{{ old("customer_id") }}">
                        <option value="">{{__('backend.choose_corporate')}}</option>
                        @foreach($customers as $value)
                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-md-6 col-sm-6 @if($errors->has('customer')) has-error @endif">
                    <label for="name-field">{{__('backend.customer')}}</label>
                    <select id="customer" name="customer_id" class="form-control" value="{{ old("customer_id") }}">
                        <option value="">{{__('backend.choose_customer')}}</option>
                    </select>
                </div>

                <!-- <div class="form-group col-md-6 col-sm-6 ">
                    <label for="count-field">Count</label>
                    <input required type="number" min=0 id="count-field" name="count" class="form-control" value=""/>
                </div> -->

                <div class="form-group col-md-6 col-sm-6 @if($errors->has('sheet')) has-error @endif">
                    <div id="sheet_upload">
                    </div>
                    <label for="sheet-field">{{__('backend.sheet')}}</label>
                    <input required class="uploadPhoto btn btn-primary" type="file" name="sheet" id="sheet">
                    @if($errors->has("sheet"))
                        <span class="help-block">{{ $errors->first("sheet") }}</span>
                    @endif
                </div>

                <div class="well well-sm col-md-12 col-sm-12 col-xs-12">
                    <button type="submit" class="btn btn-primary">{{__('backend.Add_File')}}</button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.collection_orders.index') }}"><i
                            class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {

            $('#corporate').change(function () {
                if ($(this).val() != '') {
                    var select = $(this).attr("id");
                    var value = $(this).val();
                    var dependent = $(this).data('dependent');
                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                        url: "{{ url('mngrAdmin/option/fetch') }}",
                        method: "POST",
                        data: {
                            select: select,
                            value: value,
                            _token: _token,
                            dependent: dependent
                        },
                        success: function (result) {
                            $('#' + dependent).html(result);
                        }

                    })
                }
            });

            // $('#country').change(function(){
            //  $('#state').val('');
            //  $('#city').val('');
            // });
            //
            // $('#state').change(function(){
            //  $('#city').val('');
            // });


        });
    </script>

@endsection

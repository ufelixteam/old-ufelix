<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceWallet extends Model
{
    protected $fillable = [
      'value', 'wallet_id', 'invoice_id'
    ];
}

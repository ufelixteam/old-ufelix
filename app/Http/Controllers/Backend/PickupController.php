<?php namespace App\Http\Controllers\Backend;

use App\Events\OrderUpdated;
use App\Http\Controllers\Controller\Backend;
use App\Models\Agent;
use App\Models\Corporate;
use App\Models\Customer;
use App\Models\Driver;
use App\Models\Invoice;
use App\Models\InvoicePickup;
use App\Models\Order;
use App\Models\PickUp;
use App\Models\PickUpOrder;
use App\Models\RefundCollection;
use App\Models\Store;
use App\Models\TaskRefundCollection;
use Auth;
use Carbon\Carbon;
use Flash;
use Illuminate\Http\Request;
use Response;

class PickupController extends Controller
{

    public function __construct(Request $request)
    {
        if ($request->is('mngrAdmin/*') || $request->is('admin/*')) {
            \Config::set('auth.defaults.guard', 'admin');

        } elseif ($request->is('mngrAgent/*') || $request->is('agent/*')) {
            \Config::set('auth.defaults.guard', 'agent');
        }
        $this->guardType = \Config::get('auth.defaults.guard');
    }

    public function index(Request $request)
    {
        $user = Auth::guard($this->guardType)->user();

        $collection_orders = PickUp::with(['driver', 'corporate'])
            ->select('pick_ups.*')
            ->orderBy('pick_ups.id', 'desc');

        $drivers = Driver::select('drivers.*')->where('is_active', 1);
        $corporates = Corporate::select('corporates.*')->where('is_active', 1);

        if ($user->is_warehouse_user) {
            $collection_orders = $collection_orders->join('customers', 'customers.id', '=', 'pick_ups.customer_id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id);
            $drivers = $drivers->join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                ->where('user_id', $user->id);
            $corporates = $corporates->join('customers', 'customers.corporate_id', '=', 'corporates.id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id);

        }

        $drivers = $drivers->get();
        $corporates = $corporates->get();

        if (app('request')->input('status') || app('request')->input('status') == '0') {
            $collection_orders = $collection_orders->where('status', app('request')->input('status'));
        }

        if (app('request')->input('driver_id')) {
            $collection_orders = $collection_orders->where('driver_id', app('request')->input('driver_id'));
        }

        if (app('request')->input('date') && app('request')->input('filter_date')) {
            if ($request->filter_date == 'created_at') {
                $collection_orders = $collection_orders->whereDate('created_at', app('request')->input('date'));
            } elseif ($request->filter_date == 'accepted_at') {
                $collection_orders = $collection_orders->whereDate('accepted_at', app('request')->input('date'));
            } elseif ($request->filter_date == 'collected_at') {
                $collection_orders = $collection_orders->whereDate('captain_received_time', app('request')->input('date'));
            } elseif ($request->filter_date == 'dropped_at') {
                $collection_orders = $collection_orders->whereDate('delivered_at', app('request')->input('date'));
            } elseif ($request->filter_date == 'cancelled_at') {
                $collection_orders = $collection_orders->whereDate('cancelled_at', app('request')->input('date'));
            }
        }

        $search = app('request')->input('search');
        if (isset($search) && $search != "") {
            $collection_orders = $collection_orders->join('customers', 'customers.id', '=', 'pick_ups.customer_id')
                ->join('corporates', 'corporates.id', '=', 'customers.corporate_id')
                ->where(function ($q) use ($search) {
                    $q->where('pick_ups.pickup_number', 'like', '%' . $search . '%');
                    $q->OrWhere('customers.mobile', 'like', '%' . $search . '%');
                    $q->OrWhere('customers.name', 'like', '%' . $search . '%');
                    $q->OrWhere('corporates.name', 'like', '%' . $search . '%');
                });
        }

        $collection_orders = $collection_orders->paginate(50);
        if ($request->ajax()) {
            return [
                'view' => view('backend.pickups.table', compact('collection_orders', 'drivers'))->render(),
            ];
        }
        return view('backend.pickups.index', compact('collection_orders', 'drivers', 'corporates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $user = Auth::guard($this->guardType)->user();

        $online_drivers = Driver::select('drivers.*')->where('is_active', 1);
        $corporates = Corporate::select('corporates.*')->where('is_active', 1);

        if ($user->is_warehouse_user) {
            $online_drivers = $online_drivers->join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                ->where('user_id', $user->id);
            $corporates = $corporates->join('customers', 'customers.corporate_id', '=', 'corporates.id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id);

        }

        $online_drivers = $online_drivers->get();
        $corporates = $corporates->get();

        $allStores = Store::where('status', 1);

        if ($user->is_warehouse_user) {
            $allStores = $allStores->join('store_user', 'store_user.store_id', '=', 'stores.id')
                ->where('user_id', $user->id);
        }

        $allStores = $allStores->get();
        $agents = Agent::get();

        return view('backend.pickups.create', compact('online_drivers', 'agents', 'allStores', 'corporates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $orderpickup = explode(",", $request->orderpickup[0]);

        if (count($orderpickup) < 1) {
            $data['status'] = 'false';
            $data['message'] = __('backend.Order_Packup_not_Saved_Something_Error');
            return $data;
        } else {

            $orderData = Order::where('id', $orderpickup[0])->first();
            if (!empty($orderData))
                $pickup = new PickUp();

            if ($request->has('corporate_id')) {
                $pickup->corporate_id = $request->input("corporate_id");
            }

            if ($request->has('collection_id')) {
                $pickup->collection_id = $request->input("collection_id");
            }

            $pickup->customer_id = $request->input("customer_id");
            $pickup->receiver_name = $request->input("receiver_name");
            $pickup->receiver_mobile = $request->input("receiver_mobile");
            $pickup->receiver_latitude = $request->input("receiver_latitude");
            $pickup->receiver_longitude = $request->input("receiver_longitude");
            $pickup->receiver_address = $request->input("receiver_address");
            $pickup->delivery_price = $request->input("delivery_price");
            $pickup->bonus_per_order = $request->input("bonus_per_order");
            $pickup->r_government_id = $request->input("r_government_id");

            $pickup->is_pickup = $request->input("is_pickup");
            $pickup->is_cash_collect = $request->input("is_cash_collect");
            $pickup->is_recall_orders = $request->input("is_recall_orders");

            $pickup->sender_name = $orderData->sender_name;
            $pickup->sender_mobile = $orderData->sender_mobile;
            $pickup->sender_latitude = $orderData->sender_latitude;
            $pickup->sender_longitude = $orderData->sender_longitude;
            $pickup->sender_address = $orderData->sender_address;
            $pickup->s_government_id = $orderData->s_government_id;

            if ($request->driver_id) {
                $pickup->accepted_at = date('Y-m-d h:i:s');
                $pickup->status = 1;
            }

            $pickup->captain_received_time = date('Y-m-d h:i:s', strtotime($request->captain_received_date . ' ' . $request->captain_received_time));

            $pickup->agent_id = $request->input("agent_id");
            $pickup->driver_id = $request->input("driver_id");

            $pickup->pickup_number = time();

            $pickup->notes = $request->input("notes");
            $pickup->save();

            foreach ($orderpickup as $id) {
                $pickup_order = new PickUpOrder();

                $pickup_order->pick_up_id = $pickup->id;
                $pickup_order->order_id = $id;
                $pickup_order->status = 0;
                $pickup_order->save();
            }

        }

        $data['status'] = 'true';
        $data['message'] = __('backend.PickUp_saved_successfully');
        Flash::success(__('backend.PickUp_saved_successfully'));

        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function create_fake_pickup(Request $request)
    {
        $this->validate($request, [
            'corporate_id' => 'required',
            'customer_id' => 'required',
            'captain_received_date' => 'required',
        ]);

        $existPickup = PickUp::where('customer_id', $request->customer_id)
            ->where('status', 0)
            ->whereDate('captain_received_time', $request->captain_received_date)
            ->first();

        if ($existPickup) {
            if ($request->has("notes")) {
                $existPickup->notes = $request->notes;
            }
            if ($request->has("is_cash_collect")) {
                $existPickup->is_cash_collect = $request->input("is_cash_collect");
            }
            if ($request->has("is_pickup")) {
                $existPickup->is_pickup = $request->input("is_pickup");
            }
            if ($request->has("is_recall_orders")) {
                $existPickup->is_recall_orders = $request->input("is_recall_orders");
            }

            $existPickup->save();

            $data['status'] = 'true';
            $data['message'] = __('backend.PickUp_merged_successfully');
            Flash::success(__('backend.PickUp_merged_successfully'));

            return redirect()->route('mngrAdmin.pickups.index');
        }

        $pickup = new PickUp();

        $pickup->corporate_id = $request->input("corporate_id");
        $pickup->customer_id = $request->input("customer_id");

        $pickup->delivery_price = $request->input("delivery_price") ? $request->input("delivery_price") : 0;
        $pickup->bonus_per_order = $request->input("bonus_per_order") ? $request->input("bonus_per_order") : 0;
        $pickup->is_fake_pickup = 1;
        $pickup->orders_count = 0;
        $pickup->sender_name = $request->input("sender_name");
        $pickup->sender_mobile = $request->input("sender_mobile");
        $pickup->sender_latitude = $request->input("sender_latitude");
        $pickup->sender_longitude = $request->input("sender_longitude");
        $pickup->sender_address = $request->input("sender_address");
        $pickup->s_government_id = $request->input("s_government_id");

        $pickup->is_pickup = $request->input("is_pickup");
        $pickup->is_cash_collect = $request->input("is_cash_collect");
        $pickup->is_recall_orders = $request->input("is_recall_orders");

        if ($request->driver_id) {
            $pickup->accepted_at = date('Y-m-d h:i:s');
            $pickup->status = 1;
        }

        $pickup->captain_received_time = date('Y-m-d h:i:s', strtotime($request->captain_received_date . ' ' . $request->captain_received_time));

        $pickup->agent_id = $request->input("agent_id");
        $pickup->driver_id = $request->input("driver_id");

        $pickup->notes = $request->input("notes");

        $number = substr(str_shuffle(str_repeat((time() * 1000), 5)), 0, 10);
        $pickup->pickup_number = $number;
        $pickup->save();

        $data['status'] = 'true';
        $data['message'] = __('backend.PickUp_saved_successfully');
        Flash::success(__('backend.PickUp_saved_successfully'));

        return redirect()->route('mngrAdmin.pickups.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {

        $pickup = PickUp::with(['refund_collections' => function ($query) {
            $query->withCount('orders');
        }])->findOrFail($id);
        $drivers = Driver::get();

        if ($pickup->agent_id != null) {
            $online_drivers = Driver::where('agent_id', $pickup->agent_id)->get();
        }
        $agents = Agent::get();
        $orders = [];
        $ids = PickUpOrder::where('pick_up_id', $id)->pluck('order_id')->toArray();
        if (count($ids) > 0) {
            $orders = Order::whereIn('id', $ids)->paginate(50);
            foreach ($orders as $order) {
                $order->bonous = PickUpOrder::where('pick_up_id', $id)->where('order_id', $order->id)->value('delivery_price');

            }
        }
        return view('backend.pickups.show', compact('pickup', 'drivers', 'agents', 'orders'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $user = Auth::guard($this->guardType)->user();

        $pickup = PickUp::findOrFail($id);
//        $online_drivers = Driver::get();
        $allStores = Store::where('status', 1);

        if ($user->is_warehouse_user) {
            $allStores = $allStores->join('store_user', 'store_user.store_id', '=', 'stores.id')
                ->where('user_id', $user->id);
        }

        $allStores = $allStores->get();

        if ($pickup->agent_id != null) {
            $online_drivers = Driver::where('agent_id', $pickup->agent_id)->get();
        }
        $agents = Agent::get();
        $orders = [];
        $ids = PickUpOrder::where('pick_up_id', $id)->pluck('order_id')->toArray();
        if (count($ids) > 0) {
            $orders = Order::whereIn('id', $ids)->paginate(50);
        }

        $drivers = Driver::select('drivers.*')->where('is_active', 1);
        $corporates = Corporate::select('corporates.*')->where('is_active', 1);
        $customers = Customer::select('customers.*')->where('corporate_id', $pickup->corporate_id);

        if ($user->is_warehouse_user) {
            $drivers = $drivers->join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                ->where('user_id', $user->id);
            $corporates = $corporates->join('customers', 'customers.corporate_id', '=', 'corporates.id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id);
            $customers = $customers->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id);

        }

        $drivers = $drivers->get();
        $corporates = $corporates->get();
        $customers = $customers->get();

        return view('backend.pickups.edit', compact('pickup', 'agents', 'orders', 'allStores', 'corporates', 'drivers', 'customers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @param Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'corporate_id' => 'required',
            'customer_id' => 'required',
            'captain_received_date' => 'required',
        ]);

        $pickup = PickUp::findOrFail($id);

        if (!$pickup->driver_id && $request->input("driver_id")) {
            $pickup->accepted_at = date('Y-m-d h:i:s');
            $pickup->status = 1;
        }

        $pickup->corporate_id = $request->input("corporate_id");
        $pickup->customer_id = $request->input("customer_id");

        $pickup->receiver_name = $request->input("receiver_name");
        $pickup->receiver_mobile = $request->input("receiver_mobile");
        $pickup->receiver_latitude = $request->input("receiver_latitude");
        $pickup->receiver_longitude = $request->input("receiver_longitude");
        $pickup->receiver_address = $request->input("receiver_address");
        $pickup->delivery_price = $request->input("delivery_price");
        $pickup->bonus_per_order = $request->input("bonus_per_order");
        $pickup->r_government_id = $request->input("r_government_id");

        $pickup->is_pickup = $request->input("is_pickup");
        $pickup->is_cash_collect = $request->input("is_cash_collect");
        $pickup->is_recall_orders = $request->input("is_recall_orders");

        $pickup->confirm_pickup = $request->input("confirm_pickup");
        $pickup->confirm_cash_collect = $request->input("confirm_cash_collect");
        $pickup->confirm_recall_orders = $request->input("is_recall_orders");
        $pickup->pickup_orders = $request->input("pickup_orders");
        $pickup->cash_money = $request->input("cash_money");
        $pickup->recall_orders = $request->input("recall_orders");

        $pickup->sender_name = $request->sender_name;
        $pickup->sender_mobile = $request->sender_mobile;
        $pickup->sender_latitude = $request->sender_latitude;
        $pickup->sender_longitude = $request->sender_longitude;
        $pickup->sender_address = $request->sender_address;
        $pickup->s_government_id = $request->s_government_id;

        $pickup->captain_received_time = $request->captain_received_date . ' ' . $request->captain_received_time;

        $pickup->agent_id = $request->input("agent_id");
        $pickup->driver_id = $request->input("driver_id");

        $pickup->notes = $request->input("notes");

        $pickup->save();

        Flash::warning(__('backend.PickUp_updated_successfully'));
        return redirect()->route('mngrAdmin.pickups.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $pickup = PickUp::findOrFail($id);
        if ($pickup) {
            $pickup->delete();

            PickUpOrder::where('pick_up_id', $id)->delete();
            flash(__('backend.Pickup_deleted_successfully'))->error();
            addActivityLog('Delete Pickup #No ' . $pickup->id, ' حذف  بيك اب #رقم ' . $pickup->id, 36); // Helper Function in App\Helpers.php
            return 'true';

        } else {
            flash(__('backend.Forward_Pickup_cancel'))->error();
            return 'false';
        }

    }

    static public function drop_pickups($ids)
    {
        PickUp::whereIn('id', $ids)
            ->update(['status' => 3, 'delivered_at' => date('Y-m-d h:i:s')]);

//        $store = Store::where('status', 1)->where('pickup_default', 1)->first();
        $data = ['orders.status' => 7, 'orders.dropped_at' => Carbon::now()];
//        if ($store) {
//            $data['orders.moved_in'] = $store->id;
//            $data['orders.moved_at'] = Carbon::now();
//        }

        $order_ids = Order::join('pick_up_orders', 'pick_up_orders.order_id', '=', 'orders.id')
            ->whereIn('pick_up_id', $ids)
            ->where('orders.status', 0)
            ->pluck('orders.id');

        Order::join('pick_up_orders', 'pick_up_orders.order_id', '=', 'orders.id')
            ->whereIn('pick_up_id', $ids)
            ->where('orders.status', 0)
            ->update($data);

        event(new OrderUpdated($order_ids, Auth::user()->id, 1, 'dropped'));

        foreach ($ids as $id) {
            $pickup = PickUp::find($id);
            if ($pickup) {
                if ($pickup->driver_id) {
                    $driver_invoice = Invoice::firstOrCreate(
                        ['object_id' => $pickup->driver_id, 'type' => 1, 'status' => 0],
                        ['object_id' => $pickup->driver_id, 'type' => 1, 'status' => 0, 'start_at' => now(), 'start_date' => now()]
                    );

                    if ($driver_invoice) {
                        InvoicePickup::create([
                            'invoice_id' => $driver_invoice->id,
                            'pickup_id' => $id,
                        ]);
                    }
                }

                if ($pickup->corporate_id) {
                    $corporate_pickup = InvoicePickup::join('invoices', 'invoices.id', '=', 'invoice_pickups.invoice_id')
                        ->where('object_id', $pickup->corporate_id)
                        ->where('type', 2)
                        ->where('pickup_id', $id)
                        ->first();
                    if (!$corporate_pickup) {
                        $corporate_invoice = Invoice::firstOrCreate(
                            ['object_id' => $pickup->corporate_id, 'type' => 2, 'status' => 0],
                            ['object_id' => $pickup->corporate_id, 'type' => 2, 'status' => 0, 'start_at' => now(), 'start_date' => now()]
                        );

                        if ($corporate_invoice) {
                            InvoicePickup::create([
                                'invoice_id' => $corporate_invoice->id,
                                'pickup_id' => $id,
                            ]);
                        }
                    }
                }
            }
        }

    }

    // drop off pickups
    public function dropoff_pickups(Request $request)
    {
        $ids = $request->ids;
        $this->drop_pickups($ids);

        PickUp::whereIn('id', $ids)
            ->update([
                'drop_comment' => $request->drop_comment,
                'confirm_pickup' => ($request->confirm_pickup == 'true' ? 1 : 0),
                'confirm_cash_collect' => ($request->confirm_cash_collect == 'true' ? 1 : 0),
                'confirm_recall_orders' => ($request->confirm_recall_orders == 'true' ? 1 : 0),
                'pickup_orders' => ($request->pickup_orders ? $request->pickup_orders : 0),
                'cash_money' => ($request->cash_money ? $request->cash_money : 0),
                'recall_orders' => ($request->recall_orders ? $request->recall_orders : 0),
            ]);

        if ($request->refund_collections) {
            $refund_collections_ids = $request->refund_collections;
            $refundCollections = RefundCollection::where(function ($query) use ($refund_collections_ids) {
                $query->whereIn('id', $refund_collections_ids)
                    ->orWhereIn('serial_code', $refund_collections_ids);
            })->pluck('id')->toArray();
            if (count($refundCollections)) {
                $orderIds = Order::whereIn('refund_collection_id', $refundCollections)->where('is_refund', 0)->pluck('id')->toArray();

                foreach ($refundCollections as $refundCollection) {
                    foreach ($ids as $id) {
                        TaskRefundCollection::create(['task_id' => $id, 'refund_collection_id' => $refundCollection]);
                    }

                    Order::where('refund_collection_id', $refundCollection)->where('is_refund', 0)
                        ->where('warehouse_dropoff', 1)
                        ->where('client_dropoff', 1)
                        ->where(function ($query) {
                            $query->where(function ($query) {
                                $query->where('status', 3)
                                    ->where('delivery_status', 2);
                            })
                                ->orWhere(function ($query) {
                                    $query->where('status', 3)
                                        ->where('delivery_status', 4);
                                })
                                ->orWhereIn('status', [5, 8, 4]);
                        })
                        ->update([
                            'is_refund' => 1,
                            'refund_at' => Carbon::now()
                        ]);

                }

                event(new OrderUpdated($orderIds,
                    Auth::guard($this->guardType)->user()->id,
                    1,
                    'refund'
                ));
            }
        }

        /*  PickUp::whereIn('id', $ids)->update(['status' => 3, 'delivered_at' => date('Y-m-d h:i:s')]);

          $store = Store::where('status', 1)->where('pickup_default', 1)->first();
          $data = ['orders.status' => 7, 'orders.dropped_at' => Carbon::now()];
          if ($store) {
              $data['orders.moved_in'] = $store->id;
              $data['orders.moved_at'] = Carbon::now();
          }

          Order::join('pick_up_orders', 'pick_up_orders.order_id', '=', 'orders.id')
              ->whereIn('pick_up_id', $ids)
              ->where('orders.status', 0)
              ->update($data);

          foreach ($ids as $id) {
              $pickup = PickUp::find($id);
              if ($pickup) {
                  if ($pickup->driver_id) {
                      $driver_invoice = Invoice::firstOrCreate(
                          ['object_id' => $pickup->driver_id, 'type' => 1, 'status' => 0],
                          ['object_id' => $pickup->driver_id, 'type' => 1, 'status' => 0, 'start_at' => now(), 'start_date' => now()]
                      );

                      if ($driver_invoice) {
                          InvoicePickup::create([
                              'invoice_id' => $driver_invoice->id,
                              'pickup_id' => $id,
                          ]);
                      }
                  }

                  if ($pickup->corporate_id) {
                      $corporate_pickup = InvoicePickup::join('invoices', 'invoices.id', '=', 'invoice_pickups.invoice_id')
                          ->where('object_id', $pickup->corporate_id)
                          ->where('type', 2)
                          ->where('pickup_id', $id)
                          ->first();
                      if (!$corporate_pickup) {
                          $corporate_invoice = Invoice::firstOrCreate(
                              ['object_id' => $pickup->corporate_id, 'type' => 2, 'status' => 0],
                              ['object_id' => $pickup->corporate_id, 'type' => 2, 'status' => 0, 'start_at' => now(), 'start_date' => now()]
                          );

                          if ($corporate_invoice) {
                              InvoicePickup::create([
                                  'invoice_id' => $corporate_invoice->id,
                                  'pickup_id' => $id,
                              ]);
                          }
                      }
                  }
              }
          }*/

        Flash::success(__('backend.drop_off_package'));

        $data['result'] = true;
        $data['status'] = "<span class='badge badge-pill label-black'>" . __('backend.dropped') . "</span>";
        $data['message'] = __('backend.drop_off_package');

        return $data;

    }

    //create pickup order
    public function pickup_orders(Request $request)
    {

        if ($request->has('pickup_type') && $request->pickup_type == 'collection') {
            $orders = Order::where('collection_id', $request->collection_id)->orderBy('id', 'desc')->get();

            $ids = $orders->map(function ($item, $key) {
                return $item->id;
            })->toArray();

        } else {
            $ids = $request->ids;

            $orders = Order::whereIn('id', $ids)->orderBy('id', 'desc')->get();
            $orders_data = Order::whereIn('id', $ids)->get(['r_government_id', 's_government_id']);

            $source_ids = $orders_data->map(function ($item, $key) {
                return $item->s_government_id;
            })->toArray();

            $destination_ids = $orders_data->map(function ($item, $key) {
                return $item->r_government_id;
            })->toArray();


            //check
            if (!$this->same_array($destination_ids) || !$this->same_array($source_ids)) {
                # code...
                $data['status'] = 'false';
                $data['message'] = __('backend.Order_Packup_not_Saved_Something_Error');
                return $data;
            }
        }

        if (count($orders) > 0) {

            $data['status'] = 'true';
            $data['orders'] = $ids;
            $data['frompick'] = $orders[0]->s_government_id;
            $data['cost'] = 0;
            $data['bonous'] = 0;
//            $pickup_store = Store::where('pickup_default', 1)->first();
//            if (!empty($pickup_store)) {
//                $cost = PickupPriceList::where('start_station', $orders[0]->s_government_id)->where('access_station', $pickup_store->governorate_id)->first();
//                if (!empty($cost)) {
//                    $data['cost'] = $cost->cost ? $cost->cost : 0;
//                    $data['bonous'] = $cost->bonous ? $cost->bonous : 0;
//
//                } else {
//                    $data['cost'] = 0;
//                    $data['bonous'] = 0;
//                }
//
//            } else {
//                $data['cost'] = 0;
//                $data['bonous'] = 0;
//            }
            return $data;

        } else {
//            Flash::error(__('backend.Order_Packup_not_Saved_Something_Error'));
            $data = [];
            $data['status'] = 'false';
            $data['message'] = __('backend.Order_Packup_not_Saved_Something_Error');
            return $data;
        }

    }/*pickup_orders*/


    public function same_array($arr)
    {
        return $arr === array_filter($arr, function ($element) use ($arr) {
                return ($element === $arr[0]);
            });
    }

    #forward to captain
    public function forward_pickup(Request $request)
    {
        $driver = Driver::where('id', $request->driver_id)->first();
        if (!empty($driver)) {
            if (isset($request->pickup_id) && $request->pickup_id) {
                $pickup = PickUp::where('id', $request->pickup_id)->first();
                if (!empty($pickup)) {
                    $pickup->driver_id = $request->driver_id;
                    $pickup->agent_id = $driver->agent_id;
                    $pickup->accepted_at = Carbon::now();
                    $pickup->save();
                    Flash::success(__('backend.Forward_Pickup_saved_successfully'));
                    return redirect('/mngrAdmin/pickups/' . $pickup->id);
                } else {
                    flash(__('backend.Forward_Pickup_cancel'))->error();
                    return redirect('/mngrAdmin/pickups/' . $pickup->id);
                }
            } elseif (isset($request->ids) && $request->ids) {
                $ids = is_array($request->ids) ? $request->ids : explode(",", $request->ids);
                if (is_array($ids) && count($ids)) {
                    PickUp::whereIn('id', $ids)->update([
                        'driver_id' => $request->driver_id,
                        'agent_id' => $driver->agent_id,
                        'status' => 1,
                        'accepted_at' => Carbon::now()
                    ]);

                    Flash::success(__('backend.Forward_Pickup_saved_successfully'));
                    return redirect('/mngrAdmin/pickups');
                } else {
                    flash(__('backend.Forward_Pickup_cancel'))->error();
                    return redirect('/mngrAdmin/pickups');
                }
            }

        } else {
            flash(__('backend.Forward_Pickup_cancel'))->error();
            if (isset($request->pickup_id) && $request->pickup_id) {
                return redirect('/mngrAdmin/pickups/' . $request->pickup_id);
            } else {
                return redirect('/mngrAdmin/pickups');
            }
        }
    }

    #print to captain
    public function print_pickup($pickup_id)
    {
        $pickups = PickUp::with(['corporate'])->withCount('pickup_orders')->where('id', $pickup_id)->get();

        if (!empty($pickups)) {

            $pdf = \PDF::loadView('backend.pickups.pdf', compact('pickups'), [], ['format' => 'A4-L']);

            return $pdf->stream(time());

        } else {
            Flash::warning(__('backend.No_Any_Selected_Pickups'));
            return redirect()->back();
        }

//        $pickup = PickUp::where('id', $pickup_id)->first();
//        if (!empty($pickup)) {
//            $orders = [];
//            $ids = PickUpOrder::where('pick_up_id', $pickup_id)->pluck('order_id')->toArray();
//            if (count($ids) > 0) {
//                $orders = Order::whereIn('id', $ids)->get();
//            }
//            return view('backend.pickups.pdf', compact('pickup', 'orders'));
//        } else {
//            flash(__('backend.Forward_Pickup_cancel'))->error();
//            return redirect('/mngrAdmin/pickups/' . $pickup->id);
//        }

    }


    public function forward_pickup_agent(Request $request)
    {
        $agent = Agent::where('id', $request->agent_id)->first();
        if (!empty($agent)) {
            $pickup = PickUp::where('id', $request->pickup_id)->first();
            if (!empty($pickup)) {
                $pickup->agent_id = $request->agent_id;

                $pickup->driver_id = 0;
                $pickup->save();

                Flash::success(__('backend.Forward_Pickup_saved_successfully'));
                return redirect('/mngrAdmin/pickups/' . $pickup->id);
            } else {
                flash(__('backend.Forward_Pickup_cancel'))->error();
                return redirect('/mngrAdmin/pickups/' . $pickup->id);
            }

        } else {
            flash(__('backend.Forward_Pickup_cancel'))->error();
            return redirect('/mngrAdmin/pickups/' . $request->pickup_id);
        }
    }


    public function update_order_pickup(Request $request)
    {

        $pickup = PickUp::where('id', $request->pickup_id)->first();
        if (!empty($pickup)) {
            //check order
            $orderpickup = PickUpOrder::where('pick_up_id', $request->pickup_id)->where('order_id', $request->order_id)->first();
            if (!empty($orderpickup)) {

                $order = Order::where('id', $request->order_id)->first();
                if (!empty($order)) {
                    $orderpickup->delivery_price = $request->delivery_price;
                    $orderpickup->save();
                    Flash::success(__('backend.Update_Pickup_saved_successfully'));
                    return redirect('/mngrAdmin/pickups/' . $pickup->id);

                } else {
                    flash(__('backend.Order_Pickup_Not_Found'))->error();
                    return redirect('/mngrAdmin/pickups/' . $pickup->id);
                }

            } else {
                flash(__('backend.Order_Pickup_Not_Found'))->error();
                return redirect('/mngrAdmin/pickups/' . $pickup->id);
            }


        } else {
            flash(__('backend.PickUp_Not_Found'))->error();
            return redirect('/mngrAdmin/pickups/' . $pickup->id);
        }
    }/*update_order_pickup*/


    public function receive_pickup($id)
    {

        $pickup = PickUp::where('id', $id)->first();
        if (!empty($pickup)) {
            $pickup->status = 3;
            $pickup->delivered_at = Carbon::now();
            $pickup->save();

            Order::join('pick_up_orders', 'pick_up_orders.order_id', '=', 'orders.id')
                ->where('pick_up_id', $id)
                ->where('orders.status', 0)
                ->update(['orders.status' => 7, 'orders.dropped_at' => Carbon::now()]);

            if ($pickup) {
                if ($pickup->driver_id) {
                    $driver_invoice = Invoice::firstOrCreate(
                        ['object_id' => $pickup->driver_id, 'type' => 1, 'status' => 0],
                        ['object_id' => $pickup->driver_id, 'type' => 1, 'status' => 0, 'start_at' => now(), 'start_date' => now()]
                    );

                    if ($driver_invoice) {
                        InvoicePickup::create([
                            'invoice_id' => $driver_invoice->id,
                            'pickup_id' => $id,
                        ]);
                    }
                }

                if ($pickup->corporate_id) {
                    $corporate_pickup = InvoicePickup::join('invoices', 'invoices.id', '=', 'invoice_pickups.invoice_id')
                        ->where('object_id', $pickup->corporate_id)
                        ->where('type', 2)
                        ->where('pickup_id', $id)
                        ->first();
                    if (!$corporate_pickup) {
                        $corporate_invoice = Invoice::firstOrCreate(
                            ['object_id' => $pickup->corporate_id, 'type' => 2, 'status' => 0],
                            ['object_id' => $pickup->corporate_id, 'type' => 2, 'status' => 0, 'start_at' => now(), 'start_date' => now()]
                        );

                        if ($corporate_invoice) {
                            InvoicePickup::create([
                                'invoice_id' => $corporate_invoice->id,
                                'pickup_id' => $id,
                            ]);
                        }
                    }
                }
            }

            Flash::success(__('backend.drop_off_package'));

            return redirect('/mngrAdmin/pickups/' . $pickup->id);


        } else {
            flash(__('backend.PickUp_Not_Found'))->error();
            return redirect('/mngrAdmin/pickups/' . $pickup->id);
        }
    }/*receive_pickup*/

    public function cancel_pickup($id)
    {

        $pickup = PickUp::where('id', $id)->first();
        if (!empty($pickup)) {
            $pickup->status = 4;
            $pickup->cancelled_at = Carbon::now();
            $pickup->save();
            /* invoice */
            Flash::success(__('backend.Update_Pickup_saved_successfully'));
            return redirect('/mngrAdmin/pickups/' . $pickup->id);


        } else {
            flash(__('backend.PickUp_Not_Found'))->error();
            return redirect('/mngrAdmin/pickups/' . $pickup->id);
        }
    }/*cancel_pickup*/

    public function delete_order_pickup(Request $request)
    {

        $pickup = PickUp::where('id', $request->pickup_id)->first();
        if (!empty($pickup)) {
            //check order
            $orderpickup = PickUpOrder::where('pick_up_id', $request->pickup_id)->where('order_id', $request->order_id)->first();
            if (!empty($orderpickup)) {

                $order = Order::where('id', $request->order_id)->first();
                if (!empty($order)) {
                    $orderpickup->delete();
                    Flash::success(__('backend.delete_Pickup_saved_successfully'));
//                    return redirect('/mngrAdmin/pickups/' . $pickup->id);
                    return response()->json(true);

                } else {
                    flash(__('backend.Order_Pickup_Not_Found'))->error();
//                    return redirect('/mngrAdmin/pickups/' . $pickup->id);
                    return response()->json(false);
                }

            } else {
                flash(__('backend.Order_Pickup_Not_Found'))->error();
//                return redirect('/mngrAdmin/pickups/' . $pickup->id);
                return response()->json(false);
            }


        } else {
            flash(__('backend.PickUp_Not_Found'))->error();
//            return redirect('/mngrAdmin/pickups/' . $pickup->id);
            return response()->json(false);
        }
    }/*delete_order_pickup*/

    public function pdf(Request $request)
    {

        $ids = is_array($request->ids) ? $request->ids : explode(",", $request->ids);

        if (is_array($ids) && count($ids)) {

            $drivers_count = Pickup::whereNotNull('driver_id')->whereIn('id', $ids)->distinct('driver_id')->count('driver_id');

            if ($drivers_count > 1) {
                Flash::error(__('backend.Something_Error'));
                return redirect()->back();
            }

            $pickups = Pickup::with(['corporate'])->withCount('pickup_orders')->whereIn('id', $ids)->get();

            $pdf = \PDF::loadView('backend.pickups.pdf', compact('pickups'), [], ['format' => 'A4-L']);
            return $pdf->stream(time());

        } else {
            Flash::warning(__('backend.No_Any_Selected_Pickups'));
            return redirect()->back();
        }
    }

    public function customer(Request $request)
    {
        $customer_id = $request->customer_id;
        $date = $request->date;
//        $can_collect_cash = false;
        $can_recall = false;

        if ($customer_id) {
            $wharehouseOrders = Order::where('customer_id', $customer_id)
                ->where('is_refund', 0)
                ->whereIn('status', [5, 8])
                ->where(function ($query) {
                    $query->where('warehouse_dropoff', 1)
                        ->orWhere('client_dropoff', 1);
                })
                ->count();

            if ($wharehouseOrders) {
                $can_recall = true;
            }

//            if ($date) {
//                $day = Carbon::parse($date)->format('j');
//                $dayName = strtolower(Carbon::parse($date)->format('l'));
//
//                $daysCount = CustomerTaskDay::where('customer_id', $customer_id)
//                    ->whereIn('collect_day', [$day, $dayName])
//                    ->count();
//
//                if ($daysCount) {
//                    $can_collect_cash = true;
//                }
//            }

        }

        return [
//            'can_collect_cash' => $can_collect_cash,
            'can_recall' => $can_recall
        ];

    }
}

function myMap() {

    var mapCanvas1 = document.getElementById("map1");
    var mapCanvas2 = document.getElementById("map2");


    if (mapCanvas1) {
        /*var latitude1 = document.getElementById("reciever_latitude").value;
        var longitude1 = document.getElementById("reciever_longitude").value;

        */


        // var myCenter1 = new google.maps.LatLng(latitude1,longitude1);
        var myCenter1 = new google.maps.LatLng(30.0668886, 31.1962743);
        var mapOptions = {center: myCenter1, zoom: 12};

        var map1 = new google.maps.Map(mapCanvas1, mapOptions);

        var marker = new google.maps.Marker({
            position: myCenter1,
            draggable: true,
        });
        marker.setMap(map1);
        // geocodePosition1(marker.getPosition());
        new google.maps.event.addListener(marker, 'dragend', function () {

            // geocodePosition1(marker.getPosition());
            $("#receiver_latitude").val(this.getPosition().lat());
            $("#receiver_longitude").val(this.getPosition().lng());

        });

        geocoder1 = new google.maps.Geocoder();
        var search1 = document.getElementById("receiver_address_autocomplete");
        var autocomplete1 = new google.maps.places.Autocomplete(search1);
        autocomplete1.addListener('place_changed', function () {
            var place1 = autocomplete1.getPlace();
            if (place1.geometry) {
                map1.setCenter(place1.geometry.location);
                marker.setPosition(place1.geometry.location);
                new google.maps.event.trigger(marker, 'dragend');
            }
        });
    }
    /*
      var latitude2 = document.getElementById("sender_latitude").value;
      var longitude2 = document.getElementById("sender_longitude").value;
      */
    // var myCenter2 = new google.maps.LatLng(latitude2,longitude2);

    if (mapCanvas2) {
        var myCenter2 = new google.maps.LatLng(30.0668886, 31.1962743);

        var mapOptions2 = {center: myCenter2, zoom: 12};
        var map2 = new google.maps.Map(mapCanvas2, mapOptions2);
        var marker2 = new google.maps.Marker({
            position: myCenter2,
            draggable: true,
        });
        marker2.setMap(map2);
        // geocodePosition2(marker2.getPosition());
        new google.maps.event.addListener(marker2, 'dragend', function () {

            // geocodePosition2(marker2.getPosition());
            $("#sender_latitude").val(this.getPosition().lat());
            $("#sender_longitude").val(this.getPosition().lng());

        });

        geocoder2 = new google.maps.Geocoder();
        var search2 = document.getElementById("sender_address_autocomplete");
        var autocomplete2 = new google.maps.places.Autocomplete(search2);
        autocomplete2.addListener('place_changed', function () {
            var place2 = autocomplete2.getPlace();
            if (place2.geometry) {
                map2.setCenter(place2.geometry.location);
                marker2.setPosition(place2.geometry.location);
                new google.maps.event.trigger(marker2, 'dragend');
            }
        });
    }

}

function geocodePosition1(pos) {
    geocoder = new google.maps.Geocoder();
    geocoder.geocode({
        latLng: pos

    }, function (responses) {
        if (responses && responses.length > 0) {
            $("#receiver_address").val(responses[0].formatted_address);
        } else {
        }
    });
}

function geocodePosition2(pos) {
    geocoder = new google.maps.Geocoder();
    geocoder.geocode({
        latLng: pos

    }, function (responses) {
        if (responses && responses.length > 0) {
            $("#sender_address").val(responses[0].formatted_address);
        } else {
        }
    });
}

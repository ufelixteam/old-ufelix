@extends('backend.layouts.app')
@section('css')
  <title>{{__('backend.Shipment_Orders')}} - {{__('backend.view')}}</title>
@endsection
@section('header')
<div class="page-header">
        <h3>{{__('backend.view')}} #{{$shipment_tool_order->id}}
          {!! $shipment_tool_order->status_span !!}
        </h3>
        @if(permission('deleteShipmentOrders')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
          <form action="{{ route('mngrAdmin.shipment_tool_orders.destroy', $shipment_tool_order->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
              <input type="hidden" name="_method" value="DELETE">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="btn-group pull-right" role="group" aria-label="...">
                  <button type="submit" class="btn btn-danger">{{__('backend.delete')}} <i class="glyphicon glyphicon-trash"></i></button>
              </div>
          </form>
        @endif

        @if(permission('editShipmentOrders')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
          <a class="btn btn-warning btn-group" role="group" href="{{ route('mngrAdmin.shipment_tool_orders.edit', $shipment_tool_order->id) }}" style="float: right;"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
        @endif
    </div>
@endsection

@section('content')

<div class="row">
<div class="col-md-12 col-xs-12 col-sm-12">

{{--    @if($shipment_tool_order->type == 2)--}}
{{--    <div class="form-group col-sm-6">--}}
{{--         <label for="agent_id">{{__('backend.agent')}}</label>--}}
{{--         @if(! empty($shipment_tool_order->agent))--}}
{{--         <p class="form-control-static">{{$shipment_tool_order->agent->name}}</p>--}}
{{--         <p class="form-control-static">{{$shipment_tool_order->agent->mobile}}</p>--}}
{{--         <p class="form-control-static">{{$shipment_tool_order->agent->email}}</p>--}}
{{--         @endif--}}
{{--    </div>--}}
{{--    @else--}}
    <div class="form-group col-sm-6">
         <label for="agent_id">{{__('backend.customer')}}</label>
         @if(! empty($shipment_tool_order->customer))
         <p class="form-control-static">{{$shipment_tool_order->customer->name}}</p>
         <p class="form-control-static">{{$shipment_tool_order->customer->mobile}}</p>
         <p class="form-control-static">{{$shipment_tool_order->customer->email}}</p>
         @endif
    </div>
    @endif
        <div class="form-group col-sm-6">
         <label for="notes">{{__('backend.notes')}}</label>
         <p class="form-control-static">{{$shipment_tool_order->notes}}</p>
    </div>

</div>
</div>

<div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
       <div class="col-sm-12" style="border: 2px solid #fdb801; color: #fff; margin-bottom: 20px; margin-top: 20px; background-color: #fdb801;">
      <h4>{{__('backend.Order_Items')}}</h4>
    </div>

             <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('backend.material')}}</th>
                            <th>{{__('backend.price')}}</th>
                            <th>{{__('backend.quantity')}}</th>
                            <th>{{__('backend.Total_Price')}}</th>
                            <th>{{__('backend.status')}}</th>
                            <th>{{__('backend.notes')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                      @if(! empty($shipment_tool_order->items))
                      @foreach ($shipment_tool_order->items as $order)
                        <tr>
                            <td>{{$order->id}}</td>
                            <td>{{$order->name_en}}</td>
                            <td>{{$order->price}}</td>
                            <td>{{$order->quantity}}</td>
                            <td>{{ $order->quantity * $order->price}}</td>
                            <td>
                              @if($order->status == 0 )
                                  <span class='badge badge-pill badge-black'>{{__('backend.pending')}}</span>
                                  @else
                                  <span class='badge badge-pill badge-success'>{{__('backend.done')}}</span>
                                @endif
                            </td>
                            <td>{{ $order->notes }}</td>
                        </tr>

                        @endforeach

                        @endif
                    </tbody>
            </table>
    </div>
</div>

<div class="col-md-12 col-xs-12 col-sm-12">
                <a class="btn btn-link" href="{{ route('mngrAdmin.shipment_tool_orders.index') }}"><i class="glyphicon glyphicon-backward"></i>  {{__('backend.back')}}</a>
            </div>

@endsection

<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class IsAgent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$guard = 'agent')
    {
      //  echo $guard;die();

        \Config::set('auth.defines.guard','agent');

        if ( ! Auth::guard($guard)->check()) {

            return redirect('/mngrAgent/login');
        }else{

            return $next($request);
        }
    }
}

<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use App\Models\CorporatePrice;
use App\Models\Governorate;
use App\Models\GovernoratePrice;
use App\Models\Order;
use Auth;
use Flash;
use Illuminate\Http\Request;
use Response;

class CorporatePriceController extends Controller
{

    // Get Cities By Country ID
    public function get_governorate($id)
    {
        return Governorate::where('country_id', $id)->get();
    }

    // Get All Governorate Price
    public function prices_index()
    {
        $corporatePrices = CorporatePrice::orderBy('id', 'desc')->paginate('15');
        return view('backend.corporatePrice.index', compact('corporatePrices'));
    }

    // Create Governorate Price Page
    public function create()
    {
        $list_governorates = Governorate::orderBy('name_en', 'ASC')->get();
        return view('backend.corporate_prices.create', compact('list_governorates'));
    }

    // Added New Governorate Price
    public function store(Request $request)
    {
        /*
        * status = 0 :: Not Verify
        * status = 1 :: Verify
        */

        $v = \Validator::make($request->all(), [
            'start_station' => 'required',
            'access_station' => 'required',
            'cost' => 'required',
            'overweight_cost' => 'required',
            'status' => 'required',
            'corporate_id' => 'required',
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        $corporatePrice = new CorporatePrice();
        $corporatePrice->start_station = $request->input("start_station");
        $corporatePrice->access_station = $request->input("access_station");
        $corporatePrice->cost = $request->input("cost");
        $corporatePrice->recall_cost = $request->input("recall_cost");
        $corporatePrice->reject_cost = $request->input("reject_cost");
        $corporatePrice->cancel_cost = $request->input("cancel_cost");
        $corporatePrice->overweight_cost = $request->input("overweight_cost");
        $corporatePrice->status = $request->input("status");
        $corporatePrice->corporate_id = $request->input("corporate_id");
        $corporatePrice->save();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Add New Shipment Destinations #No ' . $corporatePrice->id, ' اضافة وجهة شحن جديدة رقم ' . $corporatePrice->id, 11); // Helper Function in App\Helpers.php

        Flash::success(__('backend.Destination_Added_successfully'));
        return redirect()->route('mngrAdmin.corporates.show', ['corporate_id' => $corporatePrice->corporate_id]);
    }

    // View One Governorate Price
    public function show($id)
    {
        $governorate = Governorate::findOrFail($id);
        return view('backend.governorates.show', compact('governorate'));
    }

    // Edit Governorate Price Page
    public function edit($id)
    {
        $corporatePrice = CorporatePrice::findOrFail($id);
        $list_governorates = Governorate::orderBy('name_en', 'ASC')->get();
        return view('backend.corporate_prices.edit', compact('corporatePrice', 'list_governorates'));
    }

    // Update Governorate Price
    public function update(Request $request, $id)
    {
        /*
        * status = 0 :: Not Verify
        * status = 1 :: Verify
        */

        $v = \Validator::make($request->all(), [
            'start_station' => 'required',
            'access_station' => 'required',
            'cost' => 'required|numeric',
            'overweight_cost' => 'required|numeric',
            'status' => 'required',
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        $corporatePrice = CorporatePrice::findOrFail($id);
        $corporatePrice->start_station = $request->input("start_station");
        $corporatePrice->access_station = $request->input("access_station");
        $corporatePrice->cost = $request->input("cost");
        $corporatePrice->recall_cost = $request->input("recall_cost");
        $corporatePrice->reject_cost = $request->input("reject_cost");
        $corporatePrice->cancel_cost = $request->input("cancel_cost");
        $corporatePrice->overweight_cost = $request->input("overweight_cost");
        $corporatePrice->status = $request->input("status");
        $corporatePrice->save();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Edit Shipment Destinations #No ' . $corporatePrice->id, ' تعديل واجهة الشحن رقم  ' . $corporatePrice->id, 11); // Helper Function in App\Helpers.php

        Flash::warning(__('backend.Destination_Updated_successfully'));
        return redirect()->route('mngrAdmin.corporates.show', ['corporate_id' => $corporatePrice->corporate_id]);
    }

    // Delete Governorate Price
    public function destroy($id)
    {
        $corporatePrice = CorporatePrice::findOrFail($id);
        $OrderCorporatePrice = Order::where('governorate_cost_id', $id)->first();
        if ($OrderCorporatePrice == null) { // If Not Have Orders Governorate Price Deleted
            $corporatePrice->delete();

            // Function Add Action In Activity Log By Samir
            addActivityLog('Delete Shipment Destinations #No ' . $corporatePrice->id, ' حذف واجهة الشحن رقم ' . $corporatePrice->id, 11); // Helper Function in App\Helpers.php

            flash(__('backend.Destination_deleted_successfully'))->error();
            return redirect()->back();
        } else { // If Haves Orders Governorate Price Not Deleted
            Flash::error(__('backend.Can_not_delete_Destination_because_of_Order_Dependency'));
            return redirect()->back();
        }
    }

    public function generate(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'corporate_id' => 'required'
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        $destinations = GovernoratePrice::where('status', 1)->get();

        foreach ($destinations as $destination) {
            CorporatePrice::firstOrCreate([
                'start_station' => $destination->start_station,
                'access_station' => $destination->access_station,
                'corporate_id' => $request->input("corporate_id")
            ], [
                'start_station' => $destination->start_station,
                'access_station' => $destination->access_station,
                'cost' => $destination->cost,
                'recall_cost' => $destination->recall_cost,
                'reject_cost' => $destination->reject_cost,
                'cancel_cost' => $destination->cancel_cost,
                'overweight_cost' => $destination->overweight_cost,
                'status' => $destination->status,
                'corporate_id' => $request->input("corporate_id"),
            ]);
        }

        Flash::success(__('backend.Destination_Added_successfully'));
        return redirect()->route('mngrAdmin.corporates.show', ['corporate_id' => $request->input("corporate_id")]);
    }

    public function save_cost(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'cost' => 'required|numeric',
            'recall_cost' => 'required|numeric',
            'reject_cost' => 'required|numeric',
            'cancel_cost' => 'required|numeric',
            'id' => 'required',
        ]);

        if ($v->fails()) {
            return [false];
        }

        CorporatePrice::where('id', $request->input("id"))
            ->update([
                'cost' => $request->input("cost"),
                'recall_cost' => $request->input("recall_cost"),
                'reject_cost' => $request->input("reject_cost"),
                'cancel_cost' => $request->input("cancel_cost")
            ]);

        return [true];
    }
}

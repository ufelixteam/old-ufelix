<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{

    public function getImageAttribute($value)
    {
        if($value != ""){
            $value=url("/")."/public/backend/images/".$value;
        }else{
          $value = "";
        }
        return $value;
    }

}

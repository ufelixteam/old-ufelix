@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.stores')}} - {{__('backend.edit')}}</title>
    <link href="{{asset('assets/select2/select2.min.css')}}" rel="stylesheet">
    <style>
        .select2-container {
            width: 100% !important;
            display: block;
        }

        .input-group {
            width: 100%;
        }

        .form-group {
            min-height: 84px;
            margin-bottom: 0px;
        }

        .imgDiv img{
            border-radius: 50%;
        }
    </style>
@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit_store')}} - {{$store->name}}</h3>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('mngrAdmin.stores.update', $store->id) }}" method="POST" name="validateForm"
                  enctype="multipart/form-data">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="row">
                    <div class="form-group col-md-6  @if($errors->has('name')) has-error @endif">
                        <label for="name-field">{{__('backend.name')}}</label>
                        <input type="text" id="name-field" name="name" class="form-control"
                               value="{{ is_null(old("name")) ? $store->name : old("name") }}"/>
                        @if($errors->has("name"))
                            <span class="help-block">{{ $errors->first("name") }}</span>
                        @endif
                    </div>
                    @if(!$is_warehouse_role)
                        <div class="form-group col-md-3" style="margin-bottom: 14px;">
                            <label for="mobile-users">{{__('backend.responsible')}}</label>
                            <select class="form-control select2 disabled" id="responsible" name="responsible">
                                <option value="" data-dispaly="Select">{{__('backend.responsible')}}</option>
                                @if(! empty($responsibles))
                                    @foreach($responsibles as $responsible)
                                        <option value="{{$responsible->id}}"
                                                @if(!empty($selectedResponsible) && $responsible->id == $selectedResponsible->id) selected @endif>{{$responsible->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>

                        <div class="form-group col-md-3" style="margin-bottom: 14px;">
                            <label for="mobile-users">{{__('backend.users')}}</label>
                            <select class="form-control select2 disabled" id="users" name="users[]" multiple="multiple">
                                <option value="" data-dispaly="Select">{{__('backend.users')}}</option>
                                @if(! empty($responsibles))
                                    @foreach($responsibles as $responsible)
                                        <option value="{{$responsible->id}}"
                                                @if(in_array($responsible->id, $selectedResponsibles)) selected @endif>{{$responsible->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    @endif
                    <div class="form-group col-md-3  @if($errors->has('mobile')) has-error @endif">
                        <label for="mobile-field">{{__('backend.responsible_phone')}}</label>
                        <input type="text" id="mobile-field" name="mobile" class="form-control"
                               value="{{ is_null(old("mobile")) ? $store->mobile : old("mobile") }}"/>
                        @if($errors->has("mobile"))
                            <span class="help-block">{{ $errors->first("mobile") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-3 @if($errors->has('governorate_id')) has-error @endif">
                        <label for="governorate_id-field">{{__('backend.government')}}</label>
                        <select class="form-control" id="governorate_id" name="governorate_id">
                            <option value="" data-display="Select"> {{__('backend.government')}}</option>
                            @if(! empty($governorates))
                                @foreach($governorates as $government)
                                    <option
                                        value="{{$government->id}}" {{ $government->id == $store->governorate_id ? 'selected' : '' }} >
                                        @if(session()->has('lang') == 'en')
                                            {{$government->name_ar}}
                                        @else
                                            {{$government->name_en}}
                                        @endif
                                    </option>
                                @endforeach
                            @endif
                        </select>
                        @if($errors->has("governorate_id"))
                            <span class="help-block">{{ $errors->first("governorate_id") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-3 col-sm-3">
                        <label class="control-label" for="state_id"> {{__('backend.city')}}:</label>

                        <select class=" form-control " id="state_id" name="state_id">
                            <option value="Null">{{__('backend.city')}}</option>
                            @if(! empty($cities))
                                @foreach($cities as $state)
                                    <option
                                        value="{{$state->id}}" {{ $state->id == $store->state_id ? 'selected' : '' }} >
                                        @if(session()->has('lang') == 'en')
                                            {{$state->city_name}}
                                        @else
                                            {{$state->name_en}}
                                        @endif
                                    </option>
                                @endforeach
                            @endif

                        </select>

                    </div>
                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('pickup_default')) has-error @endif">
                        <label for="pickup_default-field">{{__('backend.pickup_default')}}</label>
                        <select id="pickup_default-field" name="pickup_default" class="form-control"
                                value="{{old("pickup_default")}}">
                            <option
                                value="0" {{ $store->pickup_default == "0" ? 'selected' : '' }}>{{__('backend.no')}}</option>
                            <option
                                value="1" {{ $store->pickup_default == "1" ? 'selected' : '' }}>{{__('backend.yes')}}</option>
                        </select>
                        @if($errors->has("pickup_default"))
                            <span class="help-block">{{ $errors->first("pickup_default") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('status')) has-error @endif">
                        <label for="status-field">{{__('backend.status')}}</label>
                        <select id="status-field" name="status" class="form-control" value="{{old("status")}}">
                            <option
                                value="0" {{ $store->status == "0" ? 'selected' : '' }}>{{__('backend.not_publish')}}</option>
                            <option
                                value="1" {{ $store->status == "1" ? 'selected' : '' }}>{{__('backend.publish')}}</option>
                        </select>
                        @if($errors->has("status"))
                            <span class="help-block">{{ $errors->first("status") }}</span>
                        @endif
                    </div>


                    <div class="form-group col-md-3  @if($errors->has('latitude')) has-error @endif">
                        <div class="input-group">
                            <label for="latitude">{{__('backend.latitude')}}</label>
                            <input type="text" id="latitude" name="latitude" class="form-control"
                                   value="{{ is_null(old("latitude")) ? $store->latitude : old("latitude") }}"/>
                            <span class="input-group-append">
                                <button class="input-group-text bg-transparent" type="button" data-toggle="modal"
                                        data-target="#map1Modal"><i class="fa fa-map-marker"></i></button>
                            </span>
                            @if($errors->has("latitude"))
                                <span class="help-block">{{ $errors->first("latitude") }}</span>
                            @endif
                        </div>

                    </div>
                    <div class="form-group col-md-3 @if($errors->has('longitude')) has-error @endif">
                        <label for="longitude">{{__('backend.longitude')}}</label>
                        <input type="text" id="longitude" name="longitude" class="form-control"
                               value="{{ is_null(old("longitude")) ? $store->longitude : old("longitude") }}"/>
                        @if($errors->has("longitude"))
                            <span class="help-block">{{ $errors->first("longitude") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-3 @if($errors->has('image')) has-error @endif">
                        <div id="image_upload">
                            @if($store->image)
                                <div class="imgDiv">
                                    <input type="hidden" value="{{ $store->image }}" name="image_old">
                                    <img src="{{ $store->image }}" style="width:100px;height:100px; border-radius: 50%"
                                         onerror="this.src='{{asset('assets/images/image.png')}}'">
                                    <button class="btn btn-danger cancel">&times;</button>
                                </div>
                            @endif
                        </div>
                        <label for="image-field">{{__('backend.image')}}: </label>
                        <input class="uploadPhoto btn btn-primary" type="file" name="image" id="image">
                        @if($errors->has("image"))
                            <span class="help-block">{{ $errors->first("image") }}</span>
                        @endif
                    </div>


                </div>
                <div class="row">
                    <div class="form-group col-md-12 col-sm-12 @if($errors->has('address')) has-error @endif">
                        <label for="address">{{__('backend.address')}}: </label>
                        <textarea type="text" id="address" name="address" rows="4"
                                  class="form-control">{{is_null(old("address")) ? $store->address : old("address")}}</textarea>
                        @if($errors->has("address"))
                            <span class="help-block">{{ $errors->first("address") }}</span>
                        @endif
                    </div>
                </div>

                @include('backend.map')

                <div class="well well-sm col-md-12">
                    <button type="submit" class="btn btn-primary">{{__('backend.save_edits')}}</button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.stores.index') }}"><i
                            class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{asset('/assets/scripts/map.js')}}"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key=AIzaSyDQiN4qc7pC9b1BRQwkqXXD28peMfWcHvw&callback=myMap"></script>
    <script src="{{asset('assets/select2/select2.min.js')}}"></script>

    <script type="text/javascript">
        $(function () {
            $('.select2').select2();

            $("form[name='validateForm']").validate({
                // Specify validation rules
                rules: {
                    name: {
                        required: true,
                    },
                    governorate_id: {
                        required: true,
                    },
                    status: {
                        required: true,
                    },
                    latitude: {
                        number: true,
                    },
                    longitude: {
                        number: true,
                    },
                    address: {
                        required: true,
                    },
                },
                // Specify validation error messages
                messages: {
                    name: {
                        required: "{{__('backend.Please_Enter_Store_Name')}}",
                    },
                    governorate_id: {
                        required: "{{__('backend.Please_Chosse_The_Government')}}",
                    },
                    status: {
                        required: "{{__('backend.Please_Chosse_The_Status')}}",
                    },
                    latitude: {
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                    },
                    longitude: {
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                    },
                    address: {
                        required: "{{__('backend.Please_Enter_The_Address')}}",
                    },
                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                },
                highlight: function (element) {
                    $(element).parent().addClass('error')
                },
                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                }
            });
        })

        $("#governorate_id").on('change', function () {
            $.ajax({
                url: "{{ url('/mngrAdmin/get_cities')}}" + "/" + $(this).val(),
                type: 'get',
                data: {},
                success: function (data) {
                    $('#state_id').html('<option value="" >Sender City</option>');
                    $.each(data, function (i, content) {
                        $('#state_id').append($("<option></option>").attr("value", content.id).text(content.city_name));
                    });
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

        });
    </script>

@endsection

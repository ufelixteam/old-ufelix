@extends('frontend.layouts.app')

@section('title') @lang('website.client') @endsection
@section('content')


@if(! empty($services ))
    @php
        $i =2;
    @endphp
    @foreach($services as $service)

        @if(! empty($i) && $i % 2 == 0)

            <section class="app body-in">
                <div class="container">
                    <div class="col-md-6 col-xs-12">
                        <div class="single-slider">
                            <div class="item">
                                <div class="img" style="background-image: url({{$service->image}})"></div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="text-single">
                            <h1 class="pre-wrap">{{$service->title}}</h1>
                            <p class="pre-wrap">
                                {{$service->description}}
                            </p>

                        </div>
                    </div>

                    </div>
            </section>
            <!-- End App1 -->
        @else
            <!-- Start App1 -->
            <section class="app four-app body-in backColor">
                <div class="container">
                    <div class="col-md-6 col-xs-12">
                        <div class="text-single">
                            <h1 class="pre-wrap">{{$service->title}}</h1>
                            <p class="pre-wrap">
                                {{$service->description}}
                            </p>

                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="single-slider">
                            <div class="item">
                                <div class="img" style="background-image: url({{$service->image}})"></div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <!-- End App1 -->


            @endif

            @php
                $i++;
            @endphp

        @endforeach

        {!! $services->appends($_GET)->links() !!}
    @endif

@endsection

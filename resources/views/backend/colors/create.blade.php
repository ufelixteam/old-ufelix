@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.Vehicle_Colors')}} - {{__('backend.create')}}</title>
@endsection

@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-plus"></i> {{__('backend.Vehicle_Colors')}} / {{__('backend.create')}} </h3>
    </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <form action="{{ route('mngrAdmin.colors.store') }}" method="POST" name='validateForm'>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group col-md-6 col-sm-6 @if($errors->has('name')) has-error @endif">
          <label for="name-field">{{__('backend.Color_Name')}}: </label>
          <input type="text" id="name-field" name="name" class="form-control" value="{{ old("name") }}"/>
          @if($errors->has("name"))
            <span class="help-block">{{ $errors->first("name") }}</span>
          @endif
        </div>
        <div class="form-group col-md-6 col-sm-6 @if($errors->has('status')) has-error @endif">
          <label for="status-field" style="display: block;">{{__('backend.active')}}</label>
          <select id="status-field" name="status" class="form-control" value="{{ old("status") }}" >
            <option value="">{{__('backend.chosse_status')}}</option>
            <option value="0" {{ old("status") == "0" ? 'selected' : '' }}>{{__('backend.no')}}</option>
            <option value="1" {{ old("status") == "1" ? 'selected' : '' }}>{{__('backend.yes')}}</option>
          </select>
          @if($errors->has("status"))
            <span class="help-block">{{ $errors->first("status") }}</span>
          @endif
        </div>
        <div class="well well-sm col-md-12 col-sm-12 col-xs-12">
          <button type="submit" class="btn btn-primary">{{__('backend.create')}}</button>
          <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.colors.index') }}"><i class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
        </div>
      </form>
    </div>
  </div>
@endsection

@section('scripts')
  <script type="text/javascript">
  $(function() {
    $("form[name='validateForm']").validate({
      // Specify validation rules
      rules: {
        name: {
          required: true,
        },
        status: {
          required: true,
        },
      },
      // Specify validation error messages
      messages: {
        name: {
          required: "{{__('backend.Please_Enter_Color_Name')}}",
        },
        status: {
          required: "{{__('backend.Please_Chosse_The_Status')}}",
        },
      },

      errorPlacement: function(error, element) {
          $(element).parents('.form-group').append(error)
      },

      highlight: function (element) {
          $(element).parent().addClass('error')
      },

      unhighlight: function (element) {
          $(element).parent().removeClass('error')
      },

      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function(form) {
          form.submit();
      }
    });
  })
</script>
@endsection

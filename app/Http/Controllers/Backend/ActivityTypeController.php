<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Activity_log_type;
use Response;
use Flash;

class ActivityTypeController extends Controller
{
    // Get All Activity Log Types
    public function index()
    {
        $activityType = Activity_log_type::orderBy('id', 'desc')->paginate('15');
        return view('backend.activity_log_type.index', compact('activityType'));
    }

    // Added New Activity Log Type
    public function store(Request $request)
    {
          $this->validate($request,[
             'name'=>'required',
          ]);

          $activityType         = new Activity_log_type();
          $activityType->name   = $request->input("name");
          $activityType->save();

          // Function Add Action In Activity Log By Samir
          addActivityLog('Add New Activity Log Type ' . $activityType->name , ' اضافة نوع سجل نشاط جديد ' . $activityType->name, 33); // Helper Function in App\Helpers.php

          Flash::success(__('backend.activity_log_Type_saved_successfully'));
          return redirect()->back();
    }

    // Update Activity Log Type
    public function update(Request $request, $id)
    {
        $this->validate($request,[
           'name'=>'required',
        ]);

        $activityType         = Activity_log_type::find($id);
        $activityType->name   = $request->input("name");
        $activityType->save();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Edit Activity Log Type ' . $activityType->name , ' تعديل نوع سجل النشاط ' . $activityType->name, 33); // Helper Function in App\Helpers.php

        Flash::success(__('backend.activity_log_Type_Updated_successfully'));
        return redirect()->back();
    }

    // Delete Activity Log Type
    public function destroy($id)
    {
        $activityType = Activity_log_type::find($id);
        $activityType->delete();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Delete Activity Log Type ' . $activityType->name , ' حذف نوع سجل النشاط ' . $activityType->name, 33); // Helper Function in App\Helpers.php

        Flash::success(__('backend.activity_log_Type_Deleted_successfully'));
        return redirect()->back();
    }

}

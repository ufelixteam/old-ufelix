<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use App\Models\TicketRule;
use Auth;
use DB;
use Flash;
use Illuminate\Http\Request;
use Response;

class TicketRuleController extends Controller
{

    private $guardType;

    public function __construct(Request $request)
    {
        if ($request->is('mngrAdmin/*') || $request->is('admin/*')) {
            \Config::set('auth.defaults.guard', 'admin');

        } elseif ($request->is('mngrAgent/*') || $request->is('agent/*')) {
            \Config::set('auth.defaults.guard', 'agent');
        }
        $this->guardType = \Config::get('auth.defaults.guard');
    }

    // Get All ticket rules
    public function index(Request $request)
    {
        $ticketRules = TicketRule::paginate(50);

        if ($request->ajax()) {
            return [
                'view' => view('backend.tickets.rules.table', compact('ticketRules'))->render(),
            ];
        }

        return view('backend.tickets.rules.index', compact('ticketRules'));

    }

    function change_active(Request $request)
    {
        $id = $request->id;
        $ticketRule = TicketRule::findOrFail($id);
        if ($ticketRule != null) {
            if ($ticketRule->status == 0) {
                $ticketRule->status = 1;
            } else {
                $ticketRule->status = 0;
            }
            $ticketRule->save();

            return $ticketRule->status;
        } else {
            return 'error';
        }

    }
}

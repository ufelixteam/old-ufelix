<?php

return [
    'exportExcel' => 'Export Excel',
    'company' => 'Ufelix',
    'in_slider' => 'In Website Slider',
    'dashboard' => 'Dashboard',
    'signout' => 'Sign Out',
    'profile' => 'Profile',
    'integration' => 'Integration',
    'No_result_found' => 'No Result Found !',
    'view' => 'View',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'back' => 'Back',
    'yes' => 'Yes',
    'no' => 'No',
    'msg_confirm_delete' => 'Delete? Are you sure?!',
    'Save_changes' => 'Save changes',
    'close' => 'Close',
    'open' => 'Open',
    'ok' => 'Ok',
    'id' => 'No',
    'create' => 'Create',
    'You_have' => 'You have',
    'closed' => 'Closed',
    'opened' => 'Opened',
    'processing' => 'Processing',
    'publish' => 'Publish',
    'not_publish' => 'Not Publish',
    'withdraw' => 'Withdraw',
    'deposit' => 'Deposit',
    'bonous' => 'Bonus',
    'subtract' => 'Subtract',
    'delivery_price' => 'Delivery Fees',
    'cancel_by' => 'Cancel by',
    'cancel_at' => 'Cancel At',
    'edit_forward' => 'Edit and Forward',
    'packup_Orders' => 'Pickup Orders',
    'packup' => 'Pickup',
    'not_found' => 'Not Found',
    'add_user' => 'Add User',

    //////////////////////////////////////////////////////////////////////////// Dashboard
    'Count_Customers' => 'No. Customers',
    'More_info' => 'More info',
    'Count_ALL_TRUCKS' => 'NO. ALL TRUCKS',
    'Count_Agents' => 'No. Agents',
    'Count_All_drivers' => 'No. All drivers',
    'Drivers_status_list' => 'Drivers status list',
    'Count_ONLINE_DRIVERS' => 'NO. ONLINE DRIVERS',
    'Count_OFFLINE_DRIVERS' => 'NO. OFFLINE DRIVERS',
    'Count_BUSY_DRIVERS' => 'NO.BUSY DRIVERS',
    'Count_VERIFIED_DRIVERS' => 'NO. VERIFIED DRIVERS',
    'Count_NOT_VERIFIED_DRIVERS' => 'NO. NOT VERIFIED DRIVERS',
    'Count_ALL_ORDERS' => 'NO. ALL ORDERS',
    'Order_status_list' => 'Order status list',

    //////////////////////////////////////////////////////////////////////////// Customers
    'individual_clients' => 'Individual Clients',
    'clients' => 'Clients',
    'customers' => 'Customers',
    'customer' => 'Customer',
    'add_customer' => 'Add Customer',
    'sort_by_blocked' => 'Sort By Blocked',
    'not_blocked' => 'Unblocked',
    'blocked' => 'Blocked',
    'sort_by_active' => 'Sort By Active',
    'not_activated' => 'Unctivate',
    'activated' => 'Activated',
    'sort_by_type' => 'Sort By Type',
    'individual' => 'Individual',
    'corporates' => 'Corporates',
    'name' => 'Name',
    'mobile' => 'Mobile',
    'corporate' => 'Corporate',
    'active' => 'Active',
    'inactive' => 'Inactive',
    'block' => 'Block',
    'verify_email' => 'verify Email',
    'verfiy_mobile' => 'Verify Mobile',
    'verfiy' => 'Verify',
    'not_verify' => 'Not Verify',
    'verified' => 'Verify',
    'job_name' => 'Specialization',
    'user_of_corporate' => 'User Of Corporate',
    'user' => 'User',
    'search_by_name_or_mobile' => 'Search By Name Or Mobile ',

    // Add Customers
    'email' => 'E-mail',
    'mobile_number' => 'Mobile Number',
    'mobile_number2' => 'Mobile Number 2',
    'password' => 'Password',
    'confirm_password' => 'Confirm Password',
    'customer_type' => 'Customer Type',
    'is_active' => 'Is Active',
    'address' => 'Address',
    'upload_image' => 'Upload Image',
    'bellonging_corporate' => 'Bellonging Corporate',
    'individual_user' => 'Individual User',
    'corporate_user' => 'Corporate User',
    'add_delivery_problem' => 'Add Delivery Problem',
    'location' => 'Location',
    'choose_reason' => 'Choose reason',
    'other_reason' => 'Other Reason',
    'add_problem_successfully' => 'Add delivery problem successfully',
    'print_invoice' => 'Print Invoice',
    'no_delivery_problems' => 'No delivery problems found',
    'order_delay' => 'Order Delay',
    'delay_at' => 'Delay At',
    'no_order_delay' => 'No order delay',
    "cannot_send_delivery_problem" => "You cannot currently submit a shipment complaint",

    // Edit Customers
    'edit_customer_id_no' => 'Edit Customer ID No',
    'enter_new_password' => 'Enter New Password',
    'if_you_does_not_change_password_please_leave_it_empty' => 'If You Does Not Change Password Please Leave It Empty',
    'save_edits' => 'Save Edits',

    'print' => 'Print',
    'order_notfound' => 'This order is not found',
    'scan_order' => 'Scan Order',

    // Show Customer
    'orders' => 'Orders',
    'personal_information' => 'Personal Information',
    'phone' => 'Phone',
    'corporate_information' => 'Corporate Information',
    'corporate_name' => 'Corporate Name',
    'corporate_email' => 'Corporate Email',
    'corporate_mobile' => 'Corporate Mobile',
    'corporate_phone' => 'Corporate Phone',

    // Show Orders Of Customer

    'orders' => 'orders',
    'of_customer' => 'Of Customer',
    'current' => 'Current',
    'finished' => 'Finished',
    'order_type' => 'Order Type',
    'order_number' => 'Sender Code',
    'order_price' => 'Order Price',
    'sender_name' => 'Sender Name',
    'receiver_name' => 'Receiver Name',
    'receiver_code' => 'Receiver Code',
    'sender' => 'Sender',
    'status' => 'Status',
    'select' => 'Select',
    'deselect' => 'Deselect',
    'accepted' => 'Accepted',
    'Accept' => 'Accept',
    'received' => 'O.F.D',
    'pdf' => 'PDF',
    'delivered' => 'Delivered',
    'cancelled' => 'Cancelled',
    'recalled' => 'Recalled',
    'Recall' => 'Recall',


    //////////////////////////////////////////////////////////////////////////// Corporate
    'corporates' => 'Corporates',
    'corporate' => 'Corporate',
    'add_corporate' => 'Add Corporate',
    'field' => 'Field',
    'commercial_record_number' => 'Commercial Record Number',
    'commercial_record_image' => 'Commercial Record Image',
    'customers_information' => 'Customers Information',
    'client_name' => 'Client Name',
    'company_information' => 'Company Information',
    'users_information' => 'Users Information',

    // Add Corporate
    'corporate_work_field' => 'Corporate Work Field',
    'Add_Client_For_Corporate' => 'Add User For Corporate',
    'Click_To_Add_Client_For_This_Corporate' => 'Create User For This Corporate',
    'Add_New_Client_For_This_Corporate' => 'Add New User For This Corporate',
    'Please_Choose_If_you_need_Add_Client_or_Not' => 'Please Choose If you Need Add User Or Not',
    'Officail_site' => 'Officail Site',
    'add_user' => 'Add User',

    // Edit Corporate
    'edit_corporate' => 'Edit Corporate',
    'latitude' => 'Latitude',
    'longitude' => 'Longitude',

    // Show Orders Of Corporate
    'orders_of_corporate' => 'Orders Of Corporate',
    'of_corporate' => 'Of Corporate',
    'Request_Review_Sent' => 'Request Review Sent!',
    'Your_request_sent_to_Ufelix_Admin_successfully' => 'Your request sent to Ufelix Admin successfully!',

    //////////////////////////////////////////////////////////////////////////// Agents
    'agents' => 'Agents',
    'agent' => 'Agent',
    'add_agent' => 'Add Agent',
    'commercual_record_no' => 'C-Record No',
    'deal_date' => 'Deal Date',
    'profit_rate' => 'Profit Rate',
    'drivers' => 'Drivers',

    // Add Agent
    'fax' => 'Fax',
    'contract_date' => 'Contract Date',
    'start_date' => 'Start Date',

    // Show Orders Of Agent
    'of_agent' => 'Of Agent',
    'received_date' => 'O.F.D Date',

    //////////////////////////////////////////////////////////////////////////// Drivers
    'driver' => 'Driver',
    'add_driver' => 'Add Driver',
    'sort_by_status' => 'Sort By Status',
    'online' => 'Online',
    'Offline' => 'Offline',
    'busy' => 'Busy',
    'sort_by_agent' => 'Sort By Agent',
    'ufelix' => 'Ufelix',
    'not_ufelix' => 'Not Ufelix',
    'This_Driver_Not_Have_Any_Vehicles' => 'This Driver Not Have Any Vehicles',
    'Click_To_Add_Vehicle_For_This_Driver' => 'Click To Add Vehicle For This Driver',
    'Click_To_Add_financial_For_This_Driver' => 'Click To Add financial For This Driver',
    'driver_financial' => 'Driver Financial',
    'all_vehicle_data_must_be_filled' => 'All Vehicle Data Must Be Filled',
    'all_user_data_must_be_filled' => 'All User Data Must Be Filled',
    'corporate_has_user' => 'Corporate Has User',

    // Add Driver
    'drivier_licence_number' => 'Driver Licence Number',
    'drivier_licence_expiration_date' => 'Driver Licence Expiration Date',
    'national_id_number' => 'National Id No',
    'national_id_expired_date' => 'National Id Expiry Date',
    'availability_status' => 'Availability Status',
    'isactive_verified_notverified' => 'Is Active',
    'choose_country' => 'Choose Country',
    'choose_beloning_agent' => 'Choose Beloning Agent',
    'notes' => 'Notes',
    'criminal_record_image' => 'Criminal Record Image',
    'profile_image' => 'Profile Image',
    'drivier_licence_image_front' => 'Drivier Licence Image',
    'drivier_licence_image_back' => 'Drivier Licence Image Back',
    'national_id_image_front' => 'National ID Image',
    'national_id_image_back' => 'National ID Image Back',
    'driver_has_vehicle' => 'Driver Has Vehicle',
    'basic_salary' => 'Basic Salary',
    'bonus_of_delivery' => 'Bonus Of Delivery',
    'pickup_price' => 'Pickup Price',
    'recall_price' => 'Recall Price',
    'reject_price' => 'Reject Price',
    'bonus_of_pickup_for_one_order' => 'Bonus Of Pickup For One Order',
    'choose_yes_or_no' => 'Choose Yes Or No',
    'has_vehicle' => 'Has Vehicle',
    'have' => 'Have',
    'not_have' => 'Not Have',
    'Add_New_Vehicle_For_This_Driver' => 'Add New Vehicle For This Driver',
    'Edit_Vehicle_For_This_Driver' => 'Edit Vehicle For This Driver',

    // Edit Drivers
    'edit_driver' => 'Edit Driver',
    'new_password' => 'New Passward',
    'attention' => 'attention',
    'If_It_Chosen_That_It_Does_Not_Have_a_Vehicle_its_Vehicle_Will_Be_Deleted' => 'If It Chosen That It Does Not Have a Vehicle its Vehicle Will Be Deleted',

    // Show Driver
    'No_Notes_to_Show' => 'No Notes to Show',
    'Driver_location' => 'Driver location',
    'Belonging_Agent_Information' => 'Belonging Agent Information',
    'Belonging_Agent_Name' => 'Belonging Agent Name',
    'Belonging_Agent_Mobile' => 'Belonging Agent Mobile',
    'Belonging_Agent_Mobile2' => 'Belonging Agent Mobile 2',
    'Truck_Information' => 'Truck Information',
    'id' => 'ID',
    'Model_Name' => 'Model Name',
    'Plate_Number' => 'Plate Number',
    'Chassi_Number' => 'Chassi Number',
    'View_All' => 'View All',

    // Show Orders Of Driver
    'of_driver' => 'Of Driver',
    'Search_by_Model_Name_Or_Plate_Number' => 'Search by Model Name Or Plate Number',
    'drivers_of_agent' => 'Driver Of Agent',
    'orders_of_driver' => 'Orders Of Driver',

    //////////////////////////////////////////////////////////////////////////// Vehicles
    'vehicles' => 'Vehicles',
    'vehicle' => 'Vehicle',
    'add_vehicle' => 'Add Vehicle',
    'edit_vehicle' => 'Edit Vehicle',

    // Add Vehicle
    'Model_Year' => 'Model Year',
    'Vehicle_Type' => 'Vehicle Type',
    'color' => 'Color',
    'Model_Type' => 'Model Type',
    'Belonging_Agent' => 'Belonging Agent',
    'Choose_Agent' => 'Choose Agent',
    'Owner_Driver' => 'Owner Driver',
    'License_Start_Date' => 'License Start Date',
    'License_End_Date' => 'Vehicle License Expiration Date',
    'Vehicle_Image' => 'Vehicle Image',
    'License_Image_Front' => 'Vehicle License Image',
    'License_Image_Back' => 'License Image Back',

    // Show Vehicle
    'year' => 'Year',
    'type' => 'Type',
    'Belonging_Driver_Information' => 'Belonging Driver Information',
    'driving_licence_expired_date' => 'DRIVING LICENCE EXPIRED DATE',
    'This_Truck_do_not_assigned_to_driver' => 'his Truck do not assigned to driver',

    // Map
    'map' => 'Map',
    'Track_Drivers' => 'Track Drivers',

    //////////////////////////////////////////////////////////////////////////// Wallets
    'the_wallets' => 'Wallets',
    'wallets' => 'Wallets',
    'wallet' => 'Wallet',
    'order' => 'Order',
    'add_wallet' => 'Add Wallet',
    'view_logs' => 'View Logs',
    'value' => 'Value',
    'Ufelix_Account' => 'Ufelix Account',

    // Add Wallet
    'User_has_Already_Wallet' => 'User has Already Wallet',
    'available' => 'Available',
    'Not_Available' => 'Not Available',

    // Show Wallet
    'Wallet_Owner_ID' => 'Wallet Owner ID',
    'Wallet_Owner' => 'Wallet Owner',
    'Wallet_Amount' => 'Wallet Amount',
    'Wallet_Log' => 'Wallet Log',
    'invoice' => 'Invoice',
    'reason' => 'Reason',
    'No_Wallet_Log' => 'No Wallet Log !',
    'Withdrawal_Amount_From_Wallet' => 'Withdrawal Amount From Wallet',
    'amount' => 'Amount',
    'Add_Amount_To_Wallet' => 'Add Amount To Wallet',
    'Confirm_Close' => 'Confirm Close',
    'msg_close_wallet' => 'We can not continue Closed Wallet Operation because Wallet have Money',
    'msg_close_wallet2' => 'Please withdraw before Close It',
    'Something_error' => 'Something error!',

    //////////////////////////////////////////////////////////////////////////// Invoices
    'the_invoices' => 'Invoices',
    'invoices' => 'Invoices',
    'invoice' => 'Invoice',
    'the_driver' => 'Driver',
    'the_corporate' => 'Corporate',
    'the_agent' => 'Agent',
    'invoice_type' => 'Invoice Type',

    // Show Invoice
    'Orders_List' => 'Orders List ',
    'Delivered_Item' => 'Delivered Item',
    'Item_Price' => 'Item Price',
    'Order_Date' => 'Order Date',
    'weight' => 'Weight',
    'Delivery_Price' => 'Delivery Fees',
    'Desrved_Price' => 'Desrved Price',
    'Received_Date	' => 'O.F.D Date	',
    'Delivered_Date' => 'Delivered Date',
    'Invoice_Bill' => 'Invoice Bill',
    'Total_order_Price' => 'Total Order Price',
    'Total_Delivery' => 'Total Delivery Fees',
    'Driver_Pocket' => 'Driver Pocket',
    'Driver_Commission' => 'Driver Commission',
    'UFelix_Commission' => 'UFelix Commission',
    'Driver_Depth' => 'Driver Depth',
    'UFelix_Depth' => 'UFelix Depth',
    'Total_Driver_Deserved' => 'Total Driver Deserved',
    'Total_UFelix_Deserved' => 'Total UFelix Deserved',
    'pay' => 'Pay',
    'Pay_Invoice' => 'Pay Invoice',
    'Something_Went_Wrong_please_Try_again_in_a_minute' => 'Something Went Wrong! please Try again in a minute.',
    'Please_Enter_the_Client_Phone_No_to_Confirm' => 'Please Enter the Client Phone No. to Confirm',
    'Phone_Number_can_not_be' => 'Phone Number can not be',
    'Empty_or_Less_than14' => 'Empty or Less than 14',
    'number' => 'Number!',
    'Phone_Number_is' => 'Phone Number is',
    'wrong' => 'Wrong',
    'Enter_the_Verification_Code_you_Recieved' => 'Enter the Verification Code you Recieved',
    'Verification_Code_can_not_be' => 'Verification Code can not be',
    'Empty_or_Less_than4' => 'Empty or Less than 4',
    'confirm' => 'Confirm',
    'Code_Verification' => 'Code Verification',
    'done' => 'Done',
    'Driver_Name' => 'Driver Name',
    'Driver_Mobile' => 'Driver Mobile',
    'Transfer_To_Other_Invoice' => 'Invoice Orders',
    'recall' => 'Recall',
    'reject' => 'Reject',
    'Confirm_Price_List' => 'Confirm Price List',
    'Transfer_Orders' => 'Transfer Orders',
    'Reset_To_Default' => 'Reset To Default',
    'Shipment_Orders' => 'Shipment Orders',
    'material' => 'Material',
    'price' => 'Price',
    'quantity' => 'Quantity',
    'Total_Price' => 'Total Price',
    'pending' => 'Pending',
    'Pending' => 'Pending',
    'dropped' => 'Dropped in warehouse',
    'This_Invoice_calculated_by_Price_list_$' => 'This Invoice calculated by Price list $',
    'This_Invoice_calculated_by_Percentage_%' => 'This Invoice calculated by Percentage %',
    'pocket' => 'Pocket',
    'remain' => 'Remain',
    'discount' => 'Discount',
    'Corporate_Deserved' => 'Corporate Deserved',
    'Ufelix_Deserved' => 'Ufelix Deserved',
    'Captain_Deserved' => 'Captain Deserved',
    'Target_Percentage_Discount_%' => 'Target Percentage Discount %',
    'commission' => 'Commission',
    'deserved' => 'Deserved',
    'Calculate_the_total_after_discount' => 'Calculate the total after discount',
    'Calculate_Percentage' => 'Calculate Percentage',
    'Confirm_Percentage' => 'Confirm Percentage',
    'Confirm_Transaction' => 'Confirm Transaction',
    'individual_client' => 'Individual Client',
    'convert_pending_to_dropped' => 'The orders were dropped to the warehouse successfully',
    'confirm_drop_orders' => 'Confirm drop orders',

    //////////////////////////////////////////////////////////////////////////// Orders
    'the_orders' => 'Orders',
    'orders' => 'Orders',
    'add_order' => 'Add Order',
    'Pending_Orders' => 'Pending Orders',
    'Dropped_Orders' => 'Dropped Orders',
    'Accepted_Orders' => 'Accepted Orders',
    'Received_Orders' => 'O.F.D Orders',
    'Delivered_Orders' => 'Delivered Orders',
    'Cancelled_Orders' => 'Cancelled Orders',
    'Recalled_Orders' => 'Recalled Orders',
    'Waiting_Orders' => 'Waiting Orders',
    'Final_Cancellation_Orders' => 'Final Cancellation Orders',
    'Expired_Waiting_Orders' => 'Expired waiting Orders',
    'Collection_Order_Excel' => 'Collection Orders Excel',
    'import_orders' => 'Import Orders',
    'Collection_Order_Form' => 'Collection Orders Form',
    'search' => 'Search',
    'sender_Government' => 'Sender Government',
    'sender_City' => 'Sender City',
    'all_orders' => 'All Orders',
    'Receiver_Government' => 'Receiver Government',
    'Receiver_City' => 'Receiver City',
    'from' => 'From',
    'to' => 'To',
    'forward' => 'Forward',
    'Forward_Orders_In_Agent_Or_Driver' => 'Forward Orders In Agent Or Driver',
    'Choose_Driver' => 'Choose Driver',
    'action_back' => 'Action Back',
    'order_cancel_requests' => 'Orders Cancel Requests',

    // Create Orders
    'Create_Orders_Collection' => 'Create Orders Collection',
    'government' => 'Government',
    'city' => 'City',
    'Order_Data' => 'Order Data',
    'Type_Name' => 'Type Name',
    'store' => 'Warehouse',
    'Choose_Store' => 'Choose Warehouse',
    'stock' => 'Stock',
    'Choose_Stock' => 'Choose Stock',
    'Quantity_Order' => 'Quantity Order',
    'overload' => 'Overweight',
    'Fees_From' => 'Delivery Fees From',
    'Choose_Method' => 'Choose Method',
    'Total_Price' => 'Total Price',
    'Sender_Mobile' => 'Sender Mobile',
    'Receiver_Mobile' => 'Receiver Mobile',

    // Edit Order
    'Edit_Order' => 'Edit Order',
    'Receiver_Mobile2' => 'Receiver Mobile 2',
    'Receiver_Latitude' => 'Receiver Latitude',
    'Receiver_Longitude' => 'Receiver Longitude',
    'Sender_Mobile2' => 'Sender Mobile 2',
    'created' => 'Created',
    'arrived' => 'Arrived',
    'Sender_Latitude' => 'Sender Latitude',
    'Sender_Longitude' => 'Sender Longitude',
    'Choose_Order_Type' => 'Choose Order Type',
    'Order_Type_ID' => 'Order Type ID',
    'Customer_ID' => 'Customer ID',
    'Payment_Method_ID' => 'Payment Method ID',
    'send_sms' => 'Send confirm Sms',
    // Show Orders
    'Order_No' => 'Order No.',
    'Cancel_and' => 'Cancel and Forward To Another Captain',
    'Change_Agent' => 'Change Agent',
    'Cancel_Order_Client_Required' => 'Cancel Order By Client',
    'Cancel_Order_Admin_Required' => 'Cancel Order By Admin',
    'Create_at' => 'Created at',
    'Accept_at' => 'Accepted at',
    'No_Date' => 'No Date',
    'Received_at' => 'O.F.D at',
    'Deliver_at' => 'Delivered at',
    'Receiver_Data' => 'Receiver Data',
    'Receiver_Address' => 'Receiver Address',
    'Customer_Account' => 'Customer Account',
    'Sender_Data' => 'Sender Data',
    'Sender_Address' => 'Sender Address',
    'Package_Data' => 'Package Data',
    'Driver_Data' => 'Driver Data',
    'Driver_Address' => 'Driver Address',
    'Order_is_not_Accepted_yet' => 'Order is not Accepted yet!',
    'Forward_Order' => 'Forward Order',
    'Forward_Order_To_Another_Agent' => 'Forward Order To Another Agent',

    //////////////////////////////////////////////////////////////////////////// Collection Orders Excel
    'Order_Collection' => 'Order Collection',
    'Collection_Orders' => 'Collection Orders',
    'form' => 'Form',
    'excel' => 'Excel',
    'add_Collection' => 'Add Collection',
    'Fillter_Page' => 'Fillter Page',
    'Upload_After_Check' => 'Upload After Check',
    'Sort_By_Saved' => 'Sort By Saved',
    'Filter_By_Completed' => 'Filter By Completed',
    'collection_type' => 'Collection Type',
    'Not_Saved' => 'Not Saved',
    'saved' => 'Saved',
    'File_Name' => 'File Name',
    'Saved_At' => 'Saved At',
    'file' => 'File',
    'Download_File' => 'Download File',
    'recall_by' => 'Recall By',
    'recall_by_receiver' => 'Recall By Receiver',
    'recall_by_sender' => 'Recall By Sender',
    'recall_at' => 'Recall at',
    'recall_at' => 'Rejected at',

    // Add Collection Orders
    'choose_corporate' => 'Choose Corporate',
    'choose_customer' => 'Choose Customer',
    'Add_File' => 'Add File',

    // Edit Collection Orders
    'sheet' => 'Sheet',

    //////////////////////////////////////////////////////////////////////////// Scan
    'scan' => 'Scan',
    'Scanned_Code' => 'Scanned code (Order Number / Receiver Code)',
    'scanned' => 'Scanned',
    'captain_entry' => 'Captain Entry',
    'captain_exit' => 'Captain Exit',
    'moved_to' => 'Moved To',
    'pending_dropped' => 'Pending dropped',
    'moved_dropped' => 'Moved dropped',
    'moved_drop' => 'Moved drop',
    'pickup_dropped' => 'Pickup dropped',
    'filter' => 'Filter',
    'orders_dropped' => 'Orders Dropped',
    'orders_moved_to' => 'Orders Moved To WareHouse',
    'collected_cost' => 'Collected Cost',
    'full_delivered' => 'Full Delivered',
    'part_recall' => 'Part Delivered',
    'part_delivered' => 'Part Delivered',
    'recall_drop' => 'Recall Drop',
    'client_drop' => 'Client Drop',
    'changes_saved_successfully' => 'Changes Saved Successfully',

    //////////////////////////////////////////////////////////////////////////// Notifications
    'notifications' => 'Notifications',
    'notification' => 'notification',
    'send_notifications' => 'Send Notifications',
    'Notifications_Types' => 'Notifications Types',
    'device' => 'device',
    'title' => 'Title',
    'message' => 'Message',
    'date' => 'Date',
    'audience' => 'Audience',
    'Published_by' => 'Published by',
    'Notifications_Timeline' => 'Notifications Timeline',

    // Add Notifications
    'new' => 'New',
    'New_Notification' => 'New Notification',
    'Notification_Type' => 'Notification Type',
    'Select_type' => 'Select type',
    'Send_to' => 'Send to',
    'Select_Sender_type' => 'Select Sender type',
    'all' => 'All',
    'captains' => 'Captains',
    'individuals' => 'Individuals',
    'devices' => 'Devices',
    'Select_device' => 'Select device',
    'android' => 'Android',
    'ios' => 'IOS',
    'browser' => 'Browser',
    'send' => 'Dashboard',

    // Show Notifications
    'show_notification' => 'Show Notification',

    // Edit Notifications
    'edit_notification' => 'Edit Notification',

    //////////////////////////////////////////////////////////////////////////// Notifications Types
    'update' => 'Update',
    'save' => 'Save',

    //////////////////////////////////////////////////////////////////////////// Shipment Tools
    'shipment' => 'Shipment',
    'shipment_material' => 'Shipment Material',
    'shipment_material_orders' => 'Shipment Material Orders',
    'shipment_destinations' => 'Shipment Destinations',
    'Shipment_Tools' => 'Shipment Tools',
    'add_shipment_tool' => 'Add Shipment Tool',

    // Add Shipment Tool
    'New_Shipment_Material' => 'New Shipment Material',
    'Title_in_English' => 'Title in English',
    'Title_in_Arabic' => 'Title in Arabic',
    'description' => 'Description',

    // Edit Shipmet Tools
    'Edit_Shipment_Tool' => 'Edit Shipment Tool',

    //////////////////////////////////////////////////////////////////////////// Shipment Tool Orders
    'Shipment_Orders' => 'Shipment Orders',
    'Shipment_Tool_Orders' => 'Shipment Tool Orders',
    'Add_Shipment_Orders' => 'Add Shipment Orders',
    'orgnization' => 'Orgnization',

    // Add Shipment Tool Orders
    'rejected' => 'Rejected',

    // Edit Shipment Tool Orders
    'Edit_Shipment_Orders' => 'Edit Shipment Orders',

    // Show Shipment Tool Orders
    'Order_Items' => 'Order Items',

    //////////////////////////////////////////////////////////////////////////// Shipment Destinations
    'add_shipment_destinations' => 'Add Shipment Destinations',
    'cost' => 'Cost',
    'overweight_cost' => 'Overweight Cost',

    // Add Shipment Destinations
    'Choose_Start' => 'Choose Start',
    'Choose_End' => 'Choose End',
    'add_destination' => 'Add Destination',

    // Edit Shipment Destinations
    'Edit_Destination' => 'Edit Destination',

    //////////////////////////////////////////////////////////////////////////// Stores
    'stores' => 'Warehouse List',
    'add_store' => 'Create Warehouse',
    'chosse_Government' => 'Chose Government',
    'no_of_stocks' => 'No Of Stocks',
    'responsible' => 'Responsible',
    'responsible_phone' => 'Responsible Phone',
    'select_all' => 'Select All',
    'no_selected_stores' => 'No Selected Warehouse To Print',
    // Edit Store
    'edit_store' => 'Edit Warehouse',

    // Show Store
    'show_store' => 'Warehouse',
    'no' => 'No.',
    'store_date' => 'Store Date',
    'dropped_by' => 'Dropped By',
    'actual_piece' => 'Actual Pieces',
    'used_piece' => 'Used Pieces',

    //////////////////////////////////////////////////////////////////////////// Stocks
    'stocks' => 'Stocks',
    'add_stock' => 'Add Stock',

    // Edit Stocks
    'Edit_Stock' => 'Edit Stock',
    'Show_Stock' => 'Show Stock',
    'used' => 'Used',
    'not_used' => 'Not Used',
    'piece_status' => 'Pieces Status',

    //////////////////////////////////////////////////////////////////////////// Promo Codes
    'promo_codes' => 'Promo Codes',
    'add_promo_code' => 'Add Promo Codes',
    'code' => 'Code',
    'no_tries' => 'Number Tries',

    // Edit Promo Codes
    'edit_promo_code' => 'Edit Promo Code',

    //////////////////////////////////////////////////////////////////////////// Employees
    'employees' => 'Employees',
    'add_employee' => 'Add Employee',
    'salary' => 'Salary',
    'job_title' => 'Job Title',

    // Add Employee
    'bio' => 'Bio',
    'image' => 'Image',

    // Edit Employee
    'edit_employee' => 'Edit Employee',

    // Show Employee
    'show_employee' => 'Show Employee',

    //////////////////////////////////////////////////////////////////////////// Permission
    'permissions' => 'Permissions',
    'type_permission' => 'Type Permission',
    'roles' => 'Roles',
    'permission_name' => 'Permission Name',
    'permission_type' => 'Permission Type',
    'choose_permission_type' => 'Chosse Permission Type',
    'Edit_Permission' => 'Edit Permission',

    //////////////////////////////////////////////////////////////////////////// Permission Types
    'Edit_Permission_Type' => 'Edit Permission Type',

    //////////////////////////////////////////////////////////////////////////// Roles
    'admin_roles' => 'Admin Roles',
    'role_name' => 'Role Name',
    'Choose_Permissions' => 'Choose Permissions',
    'Edit_Roles' => 'Edit Roles',
    'Show_Role' => 'Show Role',
    'permission' => 'Permissions',

    //////////////////////////////////////////////////////////////////////////// Others //////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////// Android Versions
    'others' => 'Others',
    'Android_Versions' => 'Android Versions',
    'version' => 'Version',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'platform' => 'Platform',
    'update' => 'Updated',
    'not_update' => 'Not Updated',
    'Android_Client' => 'Android Client',
    'Android_Driver' => 'Android Driver',
    'IOS_Client' => 'IOS Client',
    'web' => 'Web',
    'Edit_Forward_Order' => 'Edit and Forward Order To New Order',

    // Add Android Versions
    'Android_Versions' => 'Android Versions',
    'chosse_platform' => 'Chosse Platform',
    'chosse_status' => 'Chosse Status',


    // Edit Android Version
    'Edit_Android_Varsion' => 'Edit Android Varsion',
    'version_name' => 'Version Name',

    //////////////////////////////////////////////////////////////////////////// Order Types
    'Order_Types' => 'Order Types',
    'name_en' => 'Name En',
    'name_ar' => 'Name Ar',

    // Edit Order Type
    'Edit_Order_Type' => 'Edit Order Type',

    //////////////////////////////////////////////////////////////////////////// Resources
    'resources' => 'Resources',
    'started_at' => 'Started At',
    'renew_at' => 'Renew At',
    'username' => 'Username',

    // Show Resource
    'Resources_No' => 'Resources No',

    // Edit Resource
    'Edit_Resources' => 'Edit Resources',

    //////////////////////////////////////////////////////////////////////////// Vehicles Model
    'Vehicles_Model' => 'Vehicles Model',

    // Edit Vehicles Model
    'Edit_Vehicles_Model' => 'Edit Vehicles Model',

    //////////////////////////////////////////////////////////////////////////// Vehicle Types
    'Vehicle_Types' => 'Vehicle Types',

    // Edit Vehicle Types
    'Edit_Vehicle_Types' => 'Edit Vehicle Types',

    //////////////////////////////////////////////////////////////////////////// Vehicle Colors
    'Vehicle_Colors' => 'Vehicle Colors',
    'color' => 'Color',
    'Color_Name' => 'Color Name',

    // Edit Vehicle Colors
    'Edit_Vehicle_Colors' => 'Edit Vehicle Colors',

    //////////////////////////////////////////////////////////////////////////// Payment Methods
    'Payment_Methods' => 'Payment Methods',
    'Payment_Method' => 'Payment Method',

    // Edit Payment Method
    'Edit_Payment_Method' => 'Edit Payment Method',

    //////////////////////////////////////////////////////////////////////////// Countries
    'countries' => 'Countries',
    'c_image' => 'Image',

    // Edit Countries
    'Edit_Countries' => 'Edit Countries',

    //////////////////////////////////////////////////////////////////////////// Governorates
    'governorates' => 'Governorates',
    'governorate' => 'Governorate',
    'country' => 'Country',

    // Add Government
    'governorate_en' => 'Governorate English',
    'governorate_ar' => 'Governorate Arabic',

    // Edit Governorate
    'Edit_Governorate' => 'Edit Governorate',

    //////////////////////////////////////////////////////////////////////////// Cities
    'cities' => 'Cities',
    'sort_by_city' => 'Sort By City',

    // Add Cities
    'city_ar' => 'City Name Arabic',
    'city_en' => 'City Name English',

    // Edit Cities
    'edit_city' => 'Edit City',

    //////////////////////////////////////////////////////////////////////////// Pricing List
    'Pricing_List' => 'Pricing List',
    'target' => 'Target',
    'discount_percentage' => 'Discount Percentage',

    // Edit Price List
    'Edit_Price_List' => 'Edit Price List',

    // Show Price List
    'Show_Price_List' => 'Show Price List',

    //////////////////////////////////////////////////////////////////////////// Corporate Target
    'corporate_target' => 'Corporate Target',
    'add_corporate_target' => 'Add Corporate Target',
    'Add_New_Deal_For_This_Corporate' => 'Add New Deal For This Corporate',
    'Add_New_Deal_For_Corporate' => 'Add New Deal For Corporate',
    'Add_Deal' => 'Add Deal',
    'select_corporate_for_show_targets' => 'Select Corporate For Show Targets',
    'target_from' => 'Target From',
    'target_to' => 'Target To',
    'this_corporate_not_have_any_target' => 'This Corporate Not Have Any Target',
    'Not_Have_Any_Target' => 'Not Have Any Target.. Added Target',
    'corporate_target_saved_successfully' => 'Corporate Target Saved Successfully',
    'corporate_target_updated_successfully' => 'Corporate Target Updated Successfully',
    'corporate_target_deleted_successfully' => 'Corporate Target Deleted Successfully',

    //////////////////////////////////////////////////////////////////////////// End Others //////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////// Documentations
    'documentations' => 'Documentations',
    'documentation_types' => 'Documentation Types',
    'add_doc_type' => 'Add Docs Type',
    'add_doc' => 'Add Docs',
    'modified' => 'modified',
    'added_by' => 'added_by',
    'edited_by' => 'Edited By',
    'slug' => 'Slug',
    'choose_docs_type' => 'Choose Docs Type',

    //////////////////////////////////////////////////////////////////////////// Reports
    'reports' => 'Reports',
    'From_Date' => 'From Date',
    'To_Date' => 'To Date',
    'Search_about' => 'Search about',
    'requests' => 'Requests',
    'Order_By' => 'Order By',
    'display' => 'Display',
    'download' => 'Download',

    //////////////////////////////////////////////////////////////////////////// Contact Us
    'Contact_Us' => 'Feedback',
    'contacts' => 'Feedback',

    // Add Contact
    'add_contact' => 'Add Feedback',

    // Edit Contact
    'Edit_Contacts' => 'Edit Feedback',

    //////////////////////////////////////////////////////////////////////////// Admins
    'admins' => 'Admins',
    'admin' => 'Admin',
    'add_admin' => 'Add Admin',
    'role' => 'Role',
    'choose_role' => 'Choose Role',

    // Edit Admins
    'edit_admin' => 'Edit Admin',

    // Show Admin
    'Block' => 'Block',
    'Unblock' => 'Unblock',

    //////////////////////////////////////////////////////////////////////////// About Us
    'about_us' => 'About Us',
    'Google_Plus' => 'Google Plus',
    'Website_Link' => 'Website Link',
    'Twitter_Link' => 'Twitter Link',
    'YouTube_Link' => 'YouTube Link',
    'Facebook_Link' => 'Facebook Link',
    'Telephone_No2' => 'Telephone No.2',
    'Telephone_No1' => 'Telephone No.1',
    'Support_E-mail' => 'Support E-mail',
    'LinkedIn_Link' => 'LinkedIn Link',
    'Instagram_Link' => 'Instagram Link',
    'Pinterest' => 'Pinterest',
    'Dribble' => 'Dribble',
    'Vimeo' => 'Vimeo',
    'Tumbler Link' => 'Tumbler Link',
    'About' => 'About',
    'No_Agents' => 'No. Agents',
    'No_Customers' => 'No. Customers',
    'No_Corporates' => 'No. Corporates',
    'No_Drivers' => 'No. Drivers',
    'No_Orders' => 'No. Orders',
    'No_Vehicles' => 'No. Vehicles',
    'settings' => 'Settings',

    //////////////////////////////////////////////////////////////////////////// Activity Log
    'activity_log' => 'Activity Log',
    'usertype' => 'User Type',
    'url' => 'Url',
    'subject' => 'Subject',
    'time' => 'Time',
    'select_date_for_show_activity_log' => 'Select Date For Show Activity Log',
    'select_date_for_show_individual_orders' => 'Select Date For Show Individual Orders',
    'deleted_activityLog_successfully' => 'Deleted Activity Log Successfuly',
    'select_activity_log_type' => 'Select Activity Log Type',

    //////////////////////////////////////////////////////////////////////////// Activity Log Type
    'Activity_log_type' => 'Activity Log Type',
    'edit_type' => 'Edit Type',
    'activity_log_Type_saved_successfully' => 'Activity Log Type Saved Successfully',
    'activity_log_Type_Updated_successfully' => 'Activity Log Type Updated Successfully',
    'activity_log_Type_Deleted_successfully' => 'Activity Log_Type_Deleted Successfully',


    // validation error messages For Add Customer
    'Please_Enter_Customer_Password' => 'Please Enter Customer Password',
    'Password_must_be_more_than5_character' => 'Password must be more than 5 character',
    'Please_Enter_Customer_Name' => 'Please Enter Customer Name',
    'Please_Enter_Your_Address' => 'Please Enter Your Address',
    'Confirm_password_is_wrong' => 'Confirm password is wrong',
    'Please_Enter_Customer_E-mail' => 'Please Enter Customer E-mail',
    'Please_Enter_Customer_Correct_E-mail' => 'Please Enter Customer Correct E-mail',
    'E-mail_already_exist' => 'E-mail already exist!',
    'Please_Enter_Customer_Mobile' => 'Please Enter Customer Mobile',
    'Mobile_Must_Be11_Digits' => 'Mobile Must Be 11 Digits',
    'Mobile_already_exist' => 'Mobile already exist!',
    'Please_Enter_Customer_Phone' => 'Please Enter Customer Phone',
    'Please_Enter_Customer_Image' => 'Please Enter Customer Image',

    // validation error messages For Add Corporate
    'Please_Enter_Corporate_Name' => 'Please Enter Corporate Name',
    'Please_Choose_Corporate' => 'Please Choose Corporate',
    'Please_Choose_Customer' => 'Please Choose Customer',
    'Please_Enter_File' => 'Please Enter File',
    'Please_Enter_Corporate_E-mail' => 'Please Enter Corporate E-mail',
    'Please_Enter_Corporate_Mobile' => 'Please Enter Corporate Mobile',
    'Please_Enter_Corporate_Mobile2' => 'Please Enter Corporate Mobile 2',
    'Please_Enter_Work_field' => 'Please Enter Work field',
    'Please_Enter_Commercial_Record_Image' => 'Please Enter Commercial Record Image',

    // validation error messages For Add Agent
    'Please_Enter_Agent_Name' => 'Please Enter Agent Name',
    'Please_Enter_Agent_Mobile_Number' => 'Please Enter Agent Mobile Number',
    'Mobile_Number_already_exist' => 'Mobile Number already exist!',
    'Mobile_Number2_already_exist' => 'Mobile Number 2 already exist!',
    'Please_Enter_Agent_E-mail' => 'Please Enter Agent E-mail',
    'Please_Enter_Correct_E-mail' => 'Please Enter Agent Correct E-mail',
    'E-mail_already_exist' => 'E-mail already exist!',
    'Please_Enter_Commercial_Record_Number' => 'Please Enter Commercial Record Number',
    'Please_Enter_Agent_Password' => 'Please Enter Agent Password',
    'Password_must_be_more_than5_character' => 'Password must be more than 5 character',
    'Please_Enter_Agent_confirm_Password' => 'Please Enter Agent Confirm Password',
    'Confirm_password_is_wrong' => 'Confirm password is wrong',

    // validation error messages For Add Driver
    'Please_Enter_Driver_Password' => 'Please Enter Driver Password',
    'Password_must_be_more_than5_character' => 'Password Must Be Oore Than 5 Character',
    'Please_Enter_driver_Name' => 'Please Enter Driver Name',
    'Confirm_password_is_wrong' => 'Confirm password is wrong',
    'Please_Enter_Driver_E-mail' => 'Please Enter Driver E-mail',
    'E-mail_already_exist' => 'E-mail already exist!',
    'Mobile_Must_Be11_Digits' => 'Mobile Must Be 11 Digits',
    'Please_Enter_Driver_Mobile_Number' => 'Please Enter Driver Mobile Number',
    'Mobile_Number_already_exist' => 'Mobile Number already exist!',
    'Please_Select_Agent' => 'Please Select Agent',
    'Please_Enter_Latitude' => 'Please Enter Latitude',
    'Latitude_must_be_numbers' => 'Latitude must be numbers',
    'Please_Enter_Longitude' => 'Please Enter Longitude',
    'Longitude_must_be_numbers' => 'Longitude must be numbers',
    'Please_Enter_Correct_Date_Format' => 'Please Enter Correct Date Format',
    'Please_Enter_Driving_Licence_Expired_Date' => 'Please Enter Driving Licence Expired Date',
    'Please_Enter_National_Id_Number' => 'Please Enter National Id No',
    'National_Id_Must_Be14_Digits' => 'National Id Must Be 14 Digits',
    'National_Id_Must_Be_Number' => 'National Id Must Be Number',
    'Please_Enter_Driving_Licence_Number' => 'Please Enter Driving Licence Number',
    'Driving_Licence_Must_Be_Number' => 'Driving Licence Must Be Number',
    'Please_Enter_National_Id_Expired_Date' => 'Please Enter National Id Expired Date',
    'Please_Enter_Driver_Address' => 'Please Enter Driver Address',
    'please_enter_criminal_record_image_driver' => 'Please Enter Criminal Record Image Driver',
    'Please_Enter_Driver_Profile_Image' => 'Please Enter Driver Profile Image',
    'Please_Enter_drivier_licence_image_front' => 'Please Enter Drivier Licence Image Front',
    'Please_Enter_drivier_licence_image_back' => 'Please Enter Drivier Licence Image Back',
    'Please_Enter_drivier_national_id_image_front' => 'Please Enter Drivier National ID Image Front',
    'Please_Enter_drivier_national_id_image_back' => 'Please Enter Drivier National ID Image Back',
    'Please_Choose_If_Driver_Has_Vehicle_or_No' => 'Please Choose If Driver Has Vehicle Or No',
    'Please_Enter_Profit' => 'Please Enter Profit',
    'Please_Enter_Basic_Salary' => 'Please Enter Basic Salary',
    'Please_Enter_Bonus_Of_Delivery' => 'Please Enter Bonus Of Delivery',
    'Please_Enter_Pickup_Price' => 'Please Enter Pickup Price',
    'Please_Enter_Bonus_Of_Pickup_For_One_Order' => 'Please Enter Bonus Of Pickup For One Order',

    // validation error messages For Edit Driver
    'Please_Select_City' => 'Please Select City',
    'Please_Enter_Driver_Profile_Image' => 'Please Enter Driver Profile Image',
    'Please_Choose_One_Shipping_Type' => 'Please Choose at least One Shipping Type',
    'Please_Enter_Driver_Profile_Image' => 'Please Enter Driver Profile Image',
    'Please_Enter_Correct_Extenstion_jpeg_OR_png' => 'Please Enter Correct Extenstion (jpeg OR png)',
    'Please_Enter_Driving_Licence_Image' => 'Please Enter Driving Licence Image',
    'Please_Enter_National_Id_Image' => 'Please Enter National Id Image',

    // validation error messages For Add Vehicle
    'Please_Enter_Model_Name' => 'Please Enter Model Name',
    'Please_Enter_Plate_Number' => 'Please Enter Plate Number',
    'Please_Enter_Chassi_Number' => 'Please Enter Chassi Numbe',
    'Please_Enter_Vehicle_Type' => 'Please Enter Vehicle Type',
    'Please_Enter_Year' => 'Please Enter Year',
    'Please_Enter_Color' => 'Please Enter Color',
    'Please_Enter_Model_Type' => 'Please Enter Model Type',
    'Please_Enter_Driver' => 'Please Enter Driver',
    'Pleas_Enter_License_Start_Date' => 'Pleas Enter License Start Date',
    'Pleas_Enter_License_End_Date' => 'Pleas Enter License End Date',
    'Pleas_Chosse_Vehicle_Image' => 'Pleas Enter Vehicle Image',
    'Pleas_Chosse_License_Image_Front' => 'Pleas Enter License Image Front',
    'Pleas_Chosse_License_Image_Back' => 'Pleas Enter License Image Back',

    // validation error messages For Add Wallet
    'Please_Enter_the_value' => 'Please Enter the value',

    // validation error messages For Add Shipment Tools
    'Please_Enter_the_English_Tool_Title' => 'Please Enter the English Tool Title',
    'Please_Enter_the_Arabic_Tool_Title' => 'Please Enter the Arabic Tool Title',
    'Please_Enter_Price_Tool' => 'Please Enter Price Tool',
    'Please_Enter_Status_Tool' => 'Please Enter Status Tool',
    'Please_Chosse_Agent_Name' => 'Please Chosse Agent Name',
    'Please_Enter_The_Quantity' => 'Please Enter The Quantity',
    'Please_Enter_Shipment_Tool' => 'Please Enter Shipment Tool',

    // validation error messages For Add Shipment Destination
    'Please_Choose_Start_Destination' => 'Please Choose Start Destination',
    'Please_Choose_End_Destination' => 'Please Choose End Destination',
    'Please_Enter_The_Destination_Cost' => 'Please Enter The Destination Cost',
    'Please_Enter_The_Destination_Overweight_Cost' => 'Please Enter The Destination Overweight Cost',

    // validation error messages For Add Store
    'Please_Enter_Store_Name' => 'Please Enter Pick-up Center Name',
    'Please_Chosse_The_Government' => 'Please Chosse The Government',
    'Please_Chosse_The_Status' => 'Please Chosse The Status',
    'Please_Enter_The_Address' => 'Please Enter The Address',
    'Please_Enter_Responsible' => 'Please Enter Responsible',

    // validation error messages For Add Stock
    'Please_Enter_Stock_Name' => 'Please Enter Stock Name',
    'Please_Enter_Quantity' => 'Please Enter Quantity',
    'Please_Chosse_The_Main_Store' => 'Please Choose The Main Pick-up Center',
    'Please_Chosse_The_Corporate' => 'Please Choose The Corporate',
    'Please_Enter_The_Description' => 'Please Enter The Description',

    // validation error messages For Add promo Code
    'Please_Enter_Discount' => 'Please Enter Discount',
    'Please_Enter_No_of_tries' => 'Please Enter No. of tries',
    'Please_Enter_Code' => 'Please Enter Code',

    // validation error messages For Add Employee
    'Please_Enter_Employee_Name' => 'Please Enter Employee Name',
    'Please_Enter_Employee_Email' => 'Please Enter E-mail Address',
    'Please_Enter_Mobile_Number' => 'Please Enter Mobile Number',
    'Please_Enter_Employee_Salary' => 'Please Enter Salary',
    'Please_Enter_Job_Title' => 'Please Enter Job Title',
    'Please_Enter_Avalid_Number' => 'Please Enter a Valid Number',
    'Please_Enter_Employee_Correct_Email' => 'Please Enter Employee Correct E-mail',

    // validation error messages For Add Permission
    'Please_Enter_Permission_Name' => 'Please Enter Permission Name',
    'Please_Chosse_Type_Permission' => 'Please Chosse Type Permission',

    // validation error messages For Add Permission Type
    'Please_Enter_Permission_Type' => 'Please Enter Permission Type',

    // validation error messages For Add Admin Role
    'Please_Enter_Role_Name' => 'Please Enter Role Name',
    'Please_Selected_Permission_Administrator' => 'Please Selected Permission Administrator',

    // validation error messages For Add Activity Log Type
    'Please_Enter_Name_Activity_Log_Type' => 'Please Enter Name Activity Log Type',

    // validation error messages For Add Android Version
    'Please_Enter_Name_Android_Version' => 'Please Enter Name Android Version',
    'Please_Chosse_Platform_Version' => 'Please Chosse Platform Version',
    'Please_Chosse_Status_Version' => 'Please Chosse Status Version',

    // validation error messages For Add Admin
    'Please_Enter_Admin_Name' => 'Please Enter Admin Name',
    'Confirm_password_is_wrong' => 'Confirm password is wrong',
    'Please_Enter_Admin_Email' => 'Please Enter Admin Email',
    'Please_Enter_User_Correct_E-mail' => 'Please Enter User Correct E-mail',
    'Please_Enter_User_Password' => 'Please Enter User Password',
    'Password_must_be_more_than_5_character' => 'Password must be more than 5 character',
    'Please_Choose_Admin_Role' => 'Please Choose Admin Role',

    // validation error messages For Add Order Types
    'Please_Enter_Type_English_Name' => 'Please Enter Type English Name',
    'Please_Enter_Type_Arabic_Name' => 'Please Enter Type Arabic Name',
    'Please_Enter_Price_Type_Order' => 'Please Enter Price Type Order',
    'Please_Chosse_Status_Type_Order' => 'Please Chosse Status Type Order',

    // validation error messages For Sacn Order
    'no_changes_detected' => 'No Changes Detected',


    // validation error messages For Add Resources
    'Please_Enter_Resource_Name' => 'Please Enter Resource Name',
    'Please_Enter_Resource_Email' => 'Please Enter Resource Email',
    'Please_Enter_Started_At' => 'Please Enter Started At',
    'Please_Enter_Renew_At' => 'Please Enter Renew At',
    'Please_Enter_The_Cost' => 'Please Enter The Cost',
    'Please_Enter_Website_Link' => 'Please Enter Website Link',
    'Please_Enter_Username' => 'Please Enter Username',
    'Please_Enter_Password' => 'Please Enter Password',
    'Please_enter_avalid_date' => 'Please Enter a Valid Date',
    'Please_enter_avalid_price' => 'Please Enter a Valid Price',
    'Please_Enter_Correct_Website_Link' => 'Please Enter Correct Website Link',

    // validation error messages For Add Vehicles Model
    'Please_Enter_Model_Type_Name' => 'Please Enter Model Type Name',

    // validation error messages For Add Vehicles Type
    'Please_Enter_Vehicel_Price' => 'Please Enter Vehicel Price',

    // validation error messages For Add Vehicles Color
    'Please_Enter_Color_Name' => 'Please Enter Color Name',

    // validation error messages For Add Payment Method
    'Please_Enter_Title_of_Payment_Method' => 'Please Enter Title of Payment Method',

    // validation error messages For Add countries
    'Please_Enter_Country_Name' => 'Please Enter Country Name',
    'Please_Enter_Code_of_Country' => 'Please Enter Code of Country Such as (eg)',

    // validation error messages For Add countries And Governorates
    'Please_Enter_City_Name' => 'Please Enter Arabic Name City',
    'Please_Choose_a_Governorate_for_the_City' => 'Please Choose a Governorate for the City',
    'Please_Enter_Governorate_English_Name' => 'Please Enter Governorate English Name',
    'Please_Enter_Governorate_Arabic_Name' => 'Please Enter Governorate Arabic Name',
    'Please_Enter_English_City_Name' => 'Please Enter English Name City',
    'Please_Enter_City_Status' => 'Please Choose City Status',
    'Please_Enter_Governorate_Status' => 'Please Choose Governorate Status',

    // validation error messages For Edit Collection Orders
    'Corprate_already_exist' => 'Corporate already exist!',
    'Please_Enter_field' => 'Please Enter field',
    'Something_went_Wrong_please_try_again_later' => 'Something went Wrong!, please try again later',

    // validation error messages For Add Price List
    'Please_Enter_The_Target' => 'Please Enter The Target',
    'Please_Enter_discount_percentage' => 'Please Enter discount percentage',
    'Please_Enter_avalid_Target' => 'Please Enter a Valid Target',
    'Please_Enter_avalid_discount_percentage' => 'Please Enter a Valid discount percentage',

    // validation error messages For Add Documentations
    'Please_Choose_Documentation_Type' => 'Please Choose Documentation Type',
    'Please_Enter_Documentation_Title' => 'Please Enter Documentation Title',
    'Please_Enter_Documentation_Slug' => 'Please Enter Documentation Slug',
    'Please_Enter_Documentation_Description' => 'Please Enter Documentation Description',

    // validation error messages For Add Contact
    'Please_Enter_The_Name' => 'Please Enter The Name',
    'Please_Enter_Email_Address' => 'Please Enter E-mail Address',
    'Please_Enter_Mobile_Number' => 'Please Enter Mobile Number',
    'Please_Enter_Message_Title' => 'Please Enter The Message Title',
    'Please_Enter_The_Message_Content' => 'Please Enter The Message Content',
    'Please_Enter_Correct_Number' => 'Please Enter Correct Number',
    'Please_Enter_Notifaction_Type' => 'Please Enter Notifaction Type',

    //////////////////////////////////////////////////////////////////////////// Controllers /////////////////////////////////////////////////////////////////////////

    // AdminController
    'Admin_is_added_successfully' => 'Admin is added successfully.',
    'User_is_blocked_Successfully' => 'User is blocked Successfully.',
    'User_is_updated_successfully' => 'User is updated successfully.',
    'User_deleted_successfully' => 'User deleted successfully.',

    // AdminRoleController
    'Role_Admin_saved_successfully' => 'Role Admin saved successfully.',
    'Role_Admin_Updated_successfully' => 'Role Admin Updated successfully.',
    'Role_Admin_deleted_successfully' => 'Role Admin deleted successfully.',

    // AgentController
    'Agent_saved_successfully' => 'Agent saved successfully.',
    'Agent_Updated_successfully' => 'Agent Updated successfully.',
    'Agent_does_not_deleted_May_b_have_drivers_and_orders' => 'Agent does not deleted. May be have drivers and orders',
    'Agent_deleted_successfully' => 'Agent deleted successfully.',
    'Agent_does_not_exist' => 'Agent does not exist.',

    // AgentInvoiceController
    'AgentInvoice_saved_successfully' => 'Agent Invoice saved successfully.',
    'AgentInvoice_Updated_successfully' => 'Agent Invoice Updated successfully.',
    'AgentInvoice_deleted_successfully' => 'Agent Invoice deleted successfully.',

    // CityController
    'City_Added_successfully' => 'City Added successfully.',
    'City_Updated_successfully' => 'City Updated successfully.',
    'City_deleted_successfully' => 'City deleted successfully.',

    // CollectionOrderController
    'Order_Forward_saved_successfully' => 'Order Forward saved successfully.',
    'Order_Forward_not_Saved_Something_Error' => 'Order Forward not Saved .. Something Error',
    'Insert_Record_successfully' => 'Insert Record successfully.',
    'File_Does_not_Exist_Please_try_to_upload_it_again' => 'File Does not Exist, Please try to upload it again!',
    'Please_Enter_Correct_File' => 'Please Enter Correct File',
    'Collection_Orders_saved_successfully' => 'Collection Orders saved successfully.',
    'Collection_Orders_Updated_successfully' => 'Collection Orders Updated successfully.',
    'Collection_Order_deleted_successfully' => 'Collection Order deleted successfully.',

    // ColorController
    'Color_saved_successfully' => 'Color saved successfully.',
    'Color_Updated_successfully' => 'Color Updated successfully.',
    'Color_deleted_successfully' => 'Color deleted successfully.',

    // ContactController
    'Contact_saved_successfully' => 'Contact saved successfully.',
    'Contact_Updated_successfully' => 'Contact Updated successfully.',
    'Contact_deleted_successfully' => 'Contact deleted successfully.',

    // CorprateController
    'Corporate_saved_successfully' => 'Corporate saved successfully.',
    'Corporate_Updated_successfully' => 'Corporate Updated successfully.',
    'Corprate_deleted_successfully' => 'Corprate deleted successfully.',
    'Can_not_delete_Corporate_because_of_Orders_dependanices' => 'Can not delete Corporate because of Orders dependanices!',

    // CountryController
    'Country_saved_successfully' => 'Country saved successfully.',
    'Country_Updated_successfully' => 'Country Updated successfully.',
    'Country_deleted_successfully' => 'Country deleted successfully.',

    // CustomerController
    'we_sent_you_a_verification_message_please_check_your_email_and_verify_it' => 'we sent you a verification message, please check your email and verify it',
    'Something_Error' => 'Something Error',
    'Wrong_Mobile_or_Password' => 'Wrong Mobile or Password',
    'Wrong_Email_or_Password' => 'Wrong Email or Password',
    'Email_or_password_does_not_exist' => 'Email or password does not exist',
    'Send_a_message_to_change_your_password' => 'Send a message to change your password',
    'Wrong_Email' => 'Wrong Email',
    'You_are_already_Logged' => 'You are already Logged',
    'Validation_Link_is_wrong_Resent_E-mail_again' => 'Validation Link is wrong, Resent E-mail again.',
    'Email_does_not_exist' => 'Email does not exist',
    'Succeeded_your_password_has_changed_Try_login_Now' => 'Succeeded your password has changed. Try login Now',
    'Reset_Passworde_is_invalid_Something_is_wrong_Try_Again' => 'Reset Passworde is invalid ,Something is wrong , Try Again',
    'Customer_saved_successfully' => 'Customer saved successfully.',
    'Customer_Updated_successfully' => 'Customer Updated successfully.',
    'Customer_does_not_exist' => 'Customer does not exist.',
    'Customer_deleted_successfully' => 'Customer deleted successfully.',
    'problem_deleted_successfully' => 'Delivery problem deleted successfully.',
    'Customer_not_deleted_because_orders_not_empty_and_collectionorders_not_empty' => 'Customer not deleted because orders not empty and collectionorders not empty .',
    'Invalid_Format' => 'Invalid Format !',
    'Collection_that_you_try_to_open_saved_before' => 'Collection that you try to open saved before',
    'please_try_to_login' => 'please try to login',
    'please_try_again_something_error' => 'please try again .. something error !',

    // DriverController
    'Driver_saved_successfully' => 'Driver saved successfully.',
    'Please_Enter_Driver_Profile' => 'Please Enter Driver Profile',
    'Please_Enter_Criminal_Driver_Image' => 'Please Enter Criminal Driver Image',
    'Please_Enter_Driving_Licence_Image_Back' => 'Please Enter Driving Licence Image Back',
    'Please_Enter_Driving_Licence_Image_Front' => 'Please Enter Driving Licence Image Front',
    'Please_Enter_National_Id_Image_Front' => 'Please Enter National Id Image Front',
    'Please_Enter_National_Id_Image_Back' => 'Please Enter National Id Image Back',
    'Driver_Updated_successfully' => 'Driver Updated successfully.',
    'Driver_deleted_successfully' => 'Driver deleted successfully.',
    'Driver_not_delete_Delete_trucks_or_his_orders_before' => 'Driver not delete. Delete  trucks or his orders before',

    // EmployeeController
    'Employee_saved_successfully' => 'Employee saved successfully.',
    'Employee_updated_successfully' => 'Employee Updated successfully.',
    'Employee_deleted_successfully' => 'Employee deleted successfully.',

    // governorateController
    'Governorate_Added_successfully' => 'Governorate Added successfully.',
    'Governorate_Updated_successfully' => 'Governorate Updated successfully.',
    'Theres_a_Shipment_Line_already_setup_for_this_Governorate_try_to_delete_it_first' => 'There\'s a Shipment Line already set-up for this Governorate, try to delete it first!',
    'Governorate_deleted_successfully' => 'Governorate deleted successfully.',

    // governoratePriceController
    'Destination_Added_successfully' => 'Destination Added successfully.',
    'Destination_Updated_successfully' => 'Destination Updated successfully.',
    'Destination_deleted_successfully' => 'Destination deleted successfully.',
    'Can_not_delete_Destination_because_of_Order_Dependency' => 'Can not delete Destination because of Order Dependency!',

    // InvoiceController
    'Please_Eneter_Correct_Phone_Number' => 'Please Eneter Correct Phone Number !',
    'We_Can_not_send_SMS_Something_Error' => 'We Can not send SMS .. Something Error',
    'Phone_Number_is_Wrong' => 'Phone Number is Wrong!',
    'Invalid_Format' => 'Invalid Format!',
    'Wrong_Code_Something_Error' => 'Wrong Code .. Something Error',
    'No_Order_Sent_You_must_choose_Order_To_transfer_it' => 'No Order Sent .. You must choose Order To transfer it.',
    'Old_Wallet_Error_Not_Found' => 'Old Wallet Error .. Not Found',
    'Done_Orders_transfer_successfully_please_reload_this_page' => 'Done! Orders transfer successfully .. please reload this page ..',
    'This_User_does_not_have_wallet' => 'This User does not have wallet',
    'The_Wallet_of_This_User_is_closed' => 'The Wallet of This User is closed!',
    'Invoice_Payment' => 'Invoice Payment',
    'Ufelix_Deserved_From_Invoice_Payment' => 'Ufelix Deserved From Invoice Payment',
    'Order_Price_From_Invoice_Payment' => 'Order Price From Invoice Payment',
    'Done_This_Invoice_closed_successfully_please_reload_this_page' => 'Done! This Invoice closed successfully! .. please reload this page ..',
    'This_Inovice_does_not_exist' => 'This Inovice does not exist!',
    'Invoice_saved_successfully' => 'Invoice saved successfully.',
    'Invoice_Updated_successfully' => 'Invoice Updated successfully.',
    'Invoice_deleted_successfully' => 'Invoice deleted successfully.',
    'Return_to_Default_Saved_successfully' => 'Return to Default  Saved successfully',
    'This_invoice_does_not_exist' => 'This invoice does not exist',
    'Price_List_Saved_successfully' => 'Price List Saved successfully',
    'No_Pricing_Sent_You_must_predefine_Order_Price' => 'No Pricing Sent .. You must predefine Order Price',
    'Percentage_Saved_successfully' => 'Percentage Saved successfully',

    'Invoice_Order_count' => 'You must choose Order less than No. of Order In this Invoice',

    // LanguageController
    'Language_saved_successfully' => 'Language saved successfully.',
    'Language_Updated_successfully' => 'Language Updated successfully.',
    'Language_deleted_successfully' => 'Language deleted successfully.',

    // ModelTypeController
    'Model_Type_saved_successfully' => 'Model Type saved successfully.',
    'Model_Type_Updated_successfully' => 'Model Type Updated successfully.',
    'Model_Type_deleted_successfully' => 'Model Type deleted successfully.',

    // NotificationController
    'Notification_saved_successfully' => 'Notification saved successfully.',
    'Notification_Updated_successfully' => 'Notification Updated successfully.',
    'Notification_deleted_successfully' => 'Notification deleted successfully.',
    'Notification_Type_created_successfully' => 'Notification Type created successfully.',
    'Notification_Type_updated_successfully' => 'Notification Type updated successfully.',
    'Notification_Type_Deleted_successfully' => 'Notification Type Deleted successfully.',

    // OrderController
    'Order_Routed_saved_successfully' => 'Order Routed saved successfully.',
    'Order_does_not_exist' => 'Order does not exist.',
    'Order_saved_successfully' => 'Order saved successfully.',
    'Order_Canceled_after_Receiving' => 'Order Canceled after Receiving!',
    'Somthing_went_Wrong_Please_try_again' => 'Somthing went Wrong, Please try again!',
    'Collection_that_you_try_to_open_saved_before' => 'Collection that you try to open saved before',
    'Collection_saved_successfully' => 'Collection saved successfully',
    'Collection_not_Saved_Something_Error' => 'Collection not Saved .. Something Error',
    'Collection_that_you_try_to_save_does_not_have_orders' => 'Collection that you try to save does not have orders',
    'Collection_that_you_try_to_save_does_not_exist' => 'Collection that you try to save does not exist',
    'Order_Deleted_successfully' => 'Order Deleted successfully',
    'Order_Updated_successfully' => 'Order Updated successfully.',
    'Cannot_Be_Delete_This_Order' => 'Cannot Be Delete This Order.',
    'No_Any_Selected_Orders' => 'No Any Selected Orders.',
    'Please_Select_Any_Order' => 'Please Select Any Order.',
    'Order_Cancel_successfully' => 'Order Cancel successfully',
    'action_back_successfully' => 'Action Back successfully',
    'action_back_Error' => 'Error While Action Back',

    // OrderLogController
    'OrderLog_saved_successfully' => 'Order Log saved successfully.',
    'OrderLog_Updated_successfully' => 'Order Log Updated successfully.',
    'OrderLog_deleted_successfully' => 'Order Log deleted successfully.',

    // OrderViewController
    'OrderView_saved_successfully' => 'Order View saved successfully.',
    'OrderView_Updated_successfully' => 'Order View Updated successfully.',
    'OrderView_deleted_successfully' => 'Order View deleted successfully.',

    // PaymentMethodController
    'PaymentMethod_saved_successfully' => 'Payment Method saved successfully.',
    'PaymentMethod_Updated_successfully' => 'Payment Method Updated successfully.',
    'PaymentMethod_deleted_successfully' => 'Payment Method Deleted successfully.',

    // PermissionController
    'Permission_saved_successfully' => 'Permission saved successfully.',
    'Permission_Updated_successfully' => 'Permission Updated successfully.',
    'Permission_Deleted_successfully' => 'Permission Deleted successfully.',

    // PriceListController
    'Price_List_saved_successfully' => 'Price List saved successfully.',
    'Price_List_updated_successfully' => 'Price List updated successfully.',
    'Price_List_deleted_successfully' => 'Price List deleted successfully.',

    // PromoCodeController
    'PromoCode_saved_successfully' => 'Promo Code saved successfully.',
    'PromoCode_Updated_successfully' => 'Promo Code Updated successfully.',
    'PromoCode_deleted_successfully' => 'Promo Code deleted successfully.',

    // ResourceController
    'Resource_saved_successfully' => 'Resource saved successfully.',
    'Resource_updated_successfully' => 'Resource updated successfully.',
    'Resource_deleted_successfully' => 'Resource deleted successfully.',

    // SettingController
    'Data_created_successfully' => 'Data created successfully.',
    'Data_updated_successfully' => 'Data updated successfully.',
    'Data_deleted_successfully' => 'Data deleted successfully.',

    // ShipmentToolController
    'ShipmentTool_saved_successfully' => 'Shipment Tool saved successfully.',
    'ShipmentTool_Updated_successfully' => 'Shipment Tool Updated successfully.',
    'ShipmentTool_deleted_successfully' => 'Shipment Tool deleted successfully.',

    // ShipmentToolOrderController
    'ShipmentToolOrder_saved_successfully' => 'Shipment Tool Order saved successfully.',
    'ShipmentToolOrder_Updated_successfully' => 'Shipment Tool Order Updated successfully.',
    'ShipmentToolOrder_deleted_successfully' => 'Shipment Tool Order deleted successfully.',

    // SizeController
    'Size_saved_successfully' => 'Size saved successfully.',
    'Size_Updated_successfully' => 'Size Updated successfully.',
    'Size_deleted_successfully' => 'Size deleted successfully.',

    // StockController
    'Stock_created_successfully' => 'Stock created successfully.',
    'Stock_Updated_successfully' => 'Stock Updated successfully.',
    'Stock_deleted_successfully' => 'Stock deleted successfully.',

    // StoreController
    'Store_created_successfully' => 'Pick-up Center created successfully.',
    'Store_Updated_successfully' => 'Pick-up Center Updated successfully.',
    'Store_deleted_successfully' => 'Pick-up Center deleted successfully.',

    // TruckController
    'Truck_saved_successfully' => 'Truck saved successfully.',
    'Please_Enter_Veichle_Image' => 'Please Enter Veichle Image',
    'Please_Enter_License_Image_Front' => 'Please Enter License Image Front',
    'Please_Enter_License_Image_Back' => 'Please Enter License Image Back',
    'Truck_Updated_successfully' => 'Truck Updated successfully.',
    'Truck_deleted_successfully' => 'Truck deleted successfully.',

    // TypeController
    'Type_saved_successfully' => 'Type saved successfully.',
    'Type_Updated_successfully' => 'Type Updated successfully.',
    'Type_deleted_successfully' => 'Type deleted successfully.',

    // TypePermissionController
    'Type_Permission_saved_successfully' => 'Type Permission saved successfully.',
    'Type_Permission_Updated_successfully' => 'Type Permission Updated successfully.',
    'Type_Permission_Deleted_successfully' => 'Type Permission Deleted successfully.',

    // UserController
    'User_added_successfully' => 'User added successfully.',
    'user_is_unblocked_successfully' => 'user is unblocked successfully',
    'user_is_blocked_successfully' => 'user is blocked successfully',
    'User_is_updated_successfully' => 'User is updated successfully.',
    'User_deleted_successfully' => 'User deleted successfully',
    'You_are_blocke' => 'You are blocked.',
    'Email_or_Password_is_wrong' => 'Email or Password is wrong',
    'You_are_Not_Verify_Please_Contact_with_admin' => 'You are Not Verify.. Please Contact with admin!',

    // vehicleController
    'Vehicle_Type_saved_successfully' => 'Vehicle Type saved successfully.',
    'Vehicle_Type_Updated_successfully' => 'Vehicle Type Updated successfully.',
    'Vehicle_Type_deleted_successfully' => 'Vehicle Type deleted successfully.',

    // VehicleTypeController
    'Type_saved_successfully' => 'Type saved successfully.',
    'Type_Updated_successfully' => 'Type Updated successfully.',
    'Type_deleted_successfully' => 'Type deleted successfully.',

    // WalletController
    'Wallet_saved_successfully' => 'Wallet saved successfully.',
    'Wallet_does_not_deleted_Something_error' => 'Wallet does not deleted. Something error',
    'Something_error_Wallet_does_not_exist' => 'Something error. Wallet does not exist.',
    'Wallet_Updated_successfully' => 'Wallet Updated successfully.',
    'Wallet_does_not_deleted_Wallet_have_some_operation' => 'Wallet does not deleted. Wallet have some operation',
    'Wallet_deleted_successfully' => 'Wallet deleted successfully.',
    'Money_added_To_Wallet_successfully' => 'Money added To Wallet successfully.',
    'Wallet_does_not_exist_Something_error' => 'Wallet does not exist. Something error',
    'Money_withdrawal_To_Wallet_successfully' => 'Money withdrawal To Wallet successfully.',
    'Wallet_driver_not_found' => 'This Driver does not exist in our drivers list !',
    'Wallet_corporate_not_found' => 'This Corporate does not exist in our Corporates list !',
    'Wallet_agent_not_found' => 'This Agent does not exist in our agents list !',

    // WeightController
    'Weight_saved_successfully' => 'Weight saved successfully.',
    'Weight_Updated_successfully' => 'Weight Updated successfully.',
    'Weight_deleted_successfully' => 'Weight deleted successfully.',

    // Documentations
    'Docs_saved_successfully' => 'Docs Saved Successfully.',
    'Doc_Type_saved_successfully' => 'Doc Type Saved Successfully.',
    'Docs_updated_successfully' => 'Docs Updated Successfully.',
    'Docs_deleted_successfully' => 'Docs Deleted Successfully.',

    'PickUp_Orders' => 'PickUp Orders',
    'the_PickUp' => 'PickUp',

    'PickUp_saved_successfully' => 'Task Saved Successfully.',
    'PickUp_merged_successfully' => 'Task Merged Successfully.',
    'PickUp_updated_successfully' => 'PickUp Updated Successfully.',
    'PickUp_deleted_successfully' => 'PickUp Deleted Successfully.',
    'Ticket_saved_successfully' => 'Ticket Saved Successfully.',
    'Ticket_updated_successfully' => 'Ticket Updated Successfully.',
    'Ticket_deleted_successfully' => 'Ticket Deleted Successfully.',
//    'Order_Packup_not_Saved_Something_Error' => 'PickUp does not belong to the same Collection',
    'Order_Packup_not_Saved_Something_Error' => 'PickUp does not belong to the same Destination or same Source',

    'drop_off' => 'Drop-off',
    'drop_off_text' => 'Drop-off Orders',
    'drop_off_text_pickups' => 'Drop-off Pickups',
    'confirm_drop_off' => 'Confirm Drop Off',
    'drop_off_orders' => 'Are You sure To Drop-Off This Orders?',
    'drop_off_pickups' => 'Are You sure To Drop-Off This Pickups?',
    'drop_off_package' => 'Your Package is dropped off successfully',

    'Pickup_Number' => 'Pickup Number',
    'Task_Number' => 'Task Number',
    'Driver' => 'Captain',
    'Agent' => 'Agent',
    'Status' => 'Status',
    'Pickup_No' => 'Pickup No',
    'Task_No' => 'Task No',
    'Pickup_is_not_Accepted_yet' => 'Pickup does not added to Captain yet ..',
    'Packup_orders' => 'Pickup Orders',
    'Forward_pickup' => 'Forward Pickup',
    'Forward_Pickup_To_Another_Agent' => 'Forward Pickup To Another Agent',
    'Forward_Pickup_cancel' => 'Something wrong .. Forward Pickup not completed',
    'Forward_Pickup_saved_successfully' => 'Forward Pickup saved successfully',
    'Pickup_deleted_successfully' => 'Pickup deleted successfully',
    'captain_received_date' => 'Collect Date',
    'captain_received_time' => 'Collect Time',
    'Forward_at' => 'Forward Date',
    'Edit_PickUp' => 'Edit PickUp',
    'Order_Pickup_Not_Found' => 'Order_Pickup_Not_Found',
    'PickUp_Not_Found' => 'PickUp_Not_Found',
    'delete_Pickup_saved_successfully' => 'Delete Pickup successfully',
    'Update_Pickup_saved_successfully' => 'Update Pickup successfully',
    'bonous' => 'Bonus',
    'orders_no' => 'No. orders',
    'totalbounus' => 'Total Bonus',
    'total_price' => 'Total Cost',
    'receive' => 'Rreceive Pickup',
    'cancel_pickup' => 'Cancel Pickup',
    'create_pickup' => 'Create Pickup',
    'collection' => 'Collection',
    'dropped' => 'Dropped',
    'Dropped' => 'Dropped',
    'drop' => 'Drop',
    'collected' => 'Collected',
    'created' => 'Created',
    'logo' => 'Logo',
    'ContractNote' => 'Contract Note',
    'ContractForm' => 'Contract PDF OR Image',
    'add_pickup_price_list' => 'New Pickup Price List',
    'add_pickup_store' => 'New Warehouse',
    'pickup_store' => 'Warehouses',
    'pickup_price_list' => 'Pickup Price List',
    'the_bonous' => 'Bonus per order',
    'pickup_default' => 'Set As PickUp Default',
    'dropped_pickup_store' => 'Dropped Warehouse',
    'Add_Area_Title' => 'Add Area To Agent',
    'Add_Area' => 'Add Area',
    'Agent_Area_saved_successfully' => ' Area Added To Agent successfully',
    'SomeThing_Error' => ' SomeThing Error',
    'Agent_Area' => ' Agent Covered Area List',
    'delete_Agent_Area_saved_successfully' => ' Area Deleted To Agent successfully',

    'this_corporate_not_have_any_target' => 'This corporate does not have any target',

    'list_receivers' => 'List Receivers',
    'Must_Choose_Orders_To_Print' => 'Must Choose Orders To Print',
    'Must_Choose_Orders_To_Action_Back' => 'Must Choose Orders To Action Back',
    'why_us' => 'Why Us',
    'services' => 'Servics',
    'website' => 'Website',

    'Projects' => 'Projects',
    'Customers' => 'Customers',
    'Services' => 'Services',
    'Posts' => 'Posts',
    'ERRORS' => 'ERRORS',

    'Post' => 'Posts',

    'Project' => 'Projects',
    'Service' => 'Services',
    'the_Post' => 'Post',
    'the_Service' => 'Service',
    'the_Project' => 'Project',
    'Admin' => 'Admins',
    'waiting' => 'Waiting',

    'title_ar' => 'Title Arabic',
    'title_en' => 'Title English',
    'small_description_ar' => 'Small Description Arabic',
    'small_description_en' => 'Small Description English',
    'description_ar' => 'Description Arabic',
    'description_en' => 'Description English',
    'Image' => 'Image',
    'Publish' => 'Publish',
    'slider' => 'Slider',
    'accept_request' => 'Accept Request',
    'Invoice' => 'Invoice',
    'add_filter' => 'Add Filter',

    'choose_ship_line' => 'Choose Ship Line',
    'ship_line' => 'Ship Line',
    'between_governments' => 'Between Governments',
    'internal_government' => 'Internal Government',
    'internal_city' => 'Internal City',
    'start_government' => 'Start Government',
    'destination_government' => 'Destination Government',
    'Please_Choose_The_Ship_Line_Type' => 'Please Choose Ship Line',
    'available_quantity' => 'Available Quantity',
    'used_quantity' => 'Used Quantity',
    'stock_quantity' => 'Please choose quantity for selected stock',
    'not_verified' => 'Not Verified',
    'verified' => 'Verified',
    'Orderview' => 'Order View',
    'completed' => 'Completed',
    'under_processing' => 'Under Processing',

    'pay_by_captain' => 'Pay By Captain',
    'filter_orders' => 'Filter Orders',
    'paid' => 'Paid',
    'not_paid' => 'Not Paid',
    'view_transactions' => 'View Transactions',
    'Wallet_Transactions' => 'Wallet Transactions',

    'shipping_type' => 'Shipping Type',
    'n_shipping' => 'N Shipping',
    'rt_shipping' => 'R.T Shipping',
    'rt_b2c_shipping' => 'R.T B2C Shipping',
    'rt_c2c_shipping' => 'R.T C2C Shipping',
    'Please_Enter_Corporate_Order_Type' => 'Please Enter Corporate Order Type',
    'Please_Enter_Corporate_Shipping_Type' => 'Please Enter Corporate Shipping Type',
    'receiver' => 'Receiver',
    'client' => 'Client',
    'warehouse' => 'Warehouse',
    'w.h' => 'W.H',
    'forward_with_fees' => 'Forward with fees',
    'forward_without_fees' => 'Forward without fees',
    'choose_driver' => 'Choose driver',
    'fees_type' => 'Choose fees type',
    'remove_from_old_invoices' => 'Remove from old invoices',
    'Recall_order' => 'Recall Order',
    'corporate_number' => 'Corporate #Number',
    'corporate_username' => 'Corporate User Name',
    'special_action' => 'Special Action',
    'driver_name' => 'Driver Name',
    'driver_number' => 'Driver #Number',
    'invoice_number' => 'Invoice No',
    'Total' => 'Total',
    'add_discount' => 'Add Discount',
    'pickup_cost' => 'Pickup Cost',
    'final_deserved' => 'Final Deserved',
    'total_collect' => 'Total Collect',
    'bonus_delivery' => 'Bonus For Delivery',
    'total_pickup_cost' => 'Total Pickup Cost Deserved',
    'total_captain_deserved' => 'Total Captain Deserved',
    'Pickups_List' => 'Pickups List',
    'captain' => 'Captain',
    'pickup_no' => 'Pickup No',
    'orders_in_pickup' => 'No of Orders in Pickup',
    'Created_Date' => 'Created Date',
    'Received_Date' => 'O.F.D Date',
    'bonus_for_pickup' => 'Bonus for one Order in Pickup',
    'pickup_price' => 'Pickup Price',
    'total_bonus' => 'Total Bonus',
    'transfer_method' => 'Transfer Method',
    'choose_transfer_method' => 'Choose Transfer Method',
    'bank_name' => 'Bank Name',
    'bank_account' => 'Bank Account',
    'bank' => 'Bank',
    'mobicash' => 'Mobicash',
    'cash_with_captain' => 'Cash With Captain',
    'cash_in_office' => 'Cash In Office',
    'headoffice' => 'Head Office',
    'delay' => 'Delay',
    'delay_orders' => 'Delay Orders',
    'choose_another_day' => 'Choose Another Day',
    'delay_orders_successfully' => 'Orders have been successfully delayed',
    'order_delayed' => 'The order was delayed until',
    'delivery_problems' => 'Delivery Problems',
    'deliver' => 'Deliver',
    'Deliver' => 'Deliver',
    'Confirm' => 'Confirm',
    'Cancel' => 'Cancel',
    'Ok' => 'Ok',
    'delivery_orders_confirm' => 'Do you want to transfer orders to delivery status?',
    'orders_delivered' => 'The orders have been successfully delivered',
    'orders_recalled' => 'The orders have been successfully recalled',
    'orders_rejected' => 'The orders have been successfully rejected',
    'orders_cancelled' => 'The orders have been successfully cancelled',
    'Receive' => 'O.F.D',
    'receive_orders_confirm' => 'Do you want to transfer orders to O.F.D status?',
    'recall_orders_confirm' => 'Do you want to transfer orders to recalled status?',
    'reject_orders_confirm' => 'Do you want to transfer orders to rejected status?',
    'cancel_orders_confirm' => 'Do you want to transfer orders to cancelled status?',
    'dropped_orders_confirm' => 'Do you want to transfer orders to Dropped status?',
    'moved_to_orders_confirm' => 'Do you want to Move orders to WareHouse?',
    'orders_received' => 'The orders have been successfully O.F.D',
    'note' => 'Note',
    'no_fees' => 'Sorry ! Ufelix does not support this area.',
    'bad_choice' => 'The request was rejected due to a bad choice',
    'change_status' => 'Change Status',
    'other_reason' => 'Other reason',
    'delay_1' => "The receiver doesn't answer the call",
    'delay_2' => "The receiver ask to delay to other date",
    'delay_3' => 'The receiver phone is closed',
    'delay_4' => 'The receiver number is wrong',
    'delay_5' => 'The receiver ask to change the location',
    'corporate_users' => 'Corporate Users',
    'mobile_app' => 'Mobile app',
    'website' => 'Website',
    'dashboard' => 'C.S',
    'account_type' => 'Account Type',
    'other_order_type' => 'Another type of orders',
    'logistics' => 'Logistics',
    'daily_report' => 'Daily Report',
    'corporate_prices' => 'Corporate Prices',
    'customer_prices' => 'Customer Prices',
    'recall_cost' => 'Recall Cost',
    'reject_cost' => 'Reject Cost',
    'cancel_cost' => 'Cancel Cost',
    'add_price' => 'Add new price',
    'edit_price' => 'Edit Price',
    'generate_prices' => 'Generate Prices',
    'download_papers' => 'Download Papers',

    'move_collections' => 'Moved Orders',
    'move_collection' => 'Move Orders',
    'count_orders' => 'Count Orders',
    'move_by' => 'Move By',
    'not_dropped' => 'Not Dropped',
    'move_to' => 'Move To',
    'move_orders' => 'Move orders to warehouse',
    'move' => 'Move',
    'total_cost' => 'Total Cost',
    'remove' => 'Remove',
    'send_client' => 'Send Client',
    'action_date' => 'Action Date',
    'pr' => 'Part Delivered',
    'confirm_send_client' => 'Confirm Send to Client',
    'send_client_orders' => 'Are You sure To Send This Orders?',
    'send_client_text' => 'Send Orders',
    'send_client_successfully' => 'Orders are sent to client successfully',
    'refund_collections' => 'Refund Collections',
    'scan_collections' => 'Scan Collections',
    'replace' => 'Replace',
    'serial_code' => 'Serial Code',
    'export' => 'Export',
    'Dropped_at' => 'Dropped at',
    'Warehouse_Dropped_at' => 'Warehouse Dropped at',
    'Recalled_at' => 'Recalled at',
    'Rejected_at' => 'Rejected at',
    'Recalled_Date' => 'Recalled Date',
    'Rejected_Date' => 'Rejected Date',
    'Cancelled_at' => 'Cancelled at',
    'update_status' => 'Update Status',
    'update_delivery_date' => 'Update Delivery Date',
    'Excel' => 'Excel',
    'delivery_status' => 'Delivery Status',
    'reject' => 'Reject',
    'refund' => 'Refund',
    'responsibles' => 'Responsibles',
    'users' => 'Users',
    'task_orders' => 'Task Orders',
    'create_task_orders' => 'Create task orders',
    'task_type' => 'Task Type',
    'pickup' => 'Pickup',
    'cache_collect' => 'Cache collect',
    'recall_orders' => 'Recall orders',
    'rejected_orders' => 'Rejected orders',
    'cache' => 'Cache',
    'task' => 'Task',
    'edit_task' => 'Edit task',
    'to_warehouse' => 'To Warehouse',
    'from_warehouse' => 'From Warehouse',
    'scan_type' => 'Scan Type',
    'warehouse_role' => 'Warehouse Role',

    'Please_Enter_Order' => 'Please Enter the order number',
    'Please_Enter_Reason' => 'Please Enter the reason',
    'Please_Choose_Status' => 'Please Enter the status',
    'tickets' => 'Tickets',
    'ticket' => 'Ticket',
    'create_ticket' => 'Create Ticket',
    'ticket_no' => 'Ticket Number',
    'created_by' => 'Created By',
    'system' => 'System',
    'closed_at' => 'Closed At',
    'Order_Number' => 'Order Number',
    'order_log' => 'Order Log',
    'no_log' => 'No order log found',
    'comment' => 'Comment',
    'closed_comment' => 'Closed Comment',
    'assigned_to' => 'Assigned To',
    'closed_by' => 'Closed By',
    'dropped_in' => 'Deopped in',
    'close_tickets' => 'Close Tickets',
    'generate_tickets' => 'Generate Tickets',
    'in_warehouse' => 'In Warehouse',
    'in_client' => 'In Client',
    'warehouse_manager' => 'Warehouse Manager',
    'corporates_excel' => 'Corporates Excel',
    'drivers_excel' => 'Drivers Excel',
    'drivers_daily_report' => 'Drivers Daily Report',
    'money' => 'Money',
    'pickup_orders' => 'Pickup Orders',
    'task_confirm_type' => 'Confirm task type',
    'received_span' => 'Order in the way',
    'order_comments' => 'Order Comments',
    'ufelix_comments' => 'Ufelix Comments',
    'client_comments' => 'Client Comments',
    'add_order_comment' => 'Add order comment',
    'add_order_comment_successfully' => 'Add order comment successfully',
    'no_order_comments' => 'No order comments found',
    'order_comment_deleted_successfully' => 'Order comment deleted successfully.',
    'collection_number' => 'Collection Number',
    'quick_entry' => 'Quick Entry',
    'keywords' => 'Keywords',
    'manager' => 'Manager',
    'is_manager' => 'Is Manager ?',
    'No' => 'No',
    'Yes' => 'Yes',
    'edit_customer' => 'Edit Customer',
    'reference_number' => 'Reference Number',
    'delivery_type' => 'Delivery Type',
    'sender_subphone' => 'Sender Subphone',
    'day' => 'Day',
    'task_days' => 'Cash Collect Days',
    'add_day' => 'Add Day',
    'Customer_not_have_governorate' => 'This client does not specify his governorate',
    'unverified' => 'Unverified',
    'Unverified_Orders' => 'Unverified Orders',
    'add_day_by_number' => 'Add day by number',
    'add_day_by_name' => 'Add day by name',
    'day_number' => 'Day number',
    'day_name' => 'Day name',
    'saturday' => 'Saturday',
    'sunday' => 'Sunday',
    'monday' => 'Monday',
    'tuesday' => 'Tuesday',
    'wednesday' => 'Wednesday',
    'thursday' => 'Thursday',
    'friday' => 'Friday',
    'subcaptain' => 'Subcaptain',
    'drop_and_exit_again' => 'Drop And Exit Again',
    'task_No' => 'Task No',
    'add_task_order' => 'Add Task Order',
    'no_collect_days_available' => 'You cannot request a collection on this date',
    'no_warehouse_orders' => 'No orders in warehouse',
    'choose_task_type' => 'Please Choose Task Type',
    'choose' => 'Choose',
    'admin_role' => ' Admin Role',
    'add_comment' => 'Add Comment',
    'comments' => 'Comments',
    'solve' => 'Solve',
    'create_upload_at' => 'Create/Upload At',
    'complete_process_at' => 'Complete/Process At',
    'created_date' => 'Created Date',
    'accepted_date' => 'Accepted Date',
    'collected_date' => 'Collected Date',
    'dropped_date' => 'Dropped Date',
    'cancelled_date' => 'Cancelled Date',
    'Task_details' => 'Task Details',
    'confirmed' => 'Confirmed',
    'details' => 'Details',
    'add' => 'Add',
    'Sheet_uploaded_successfully' => 'Excel sheet uploaded successfully',
    'default' => 'Default',
    'created_by_user' => 'Created By User',
    'manual' => 'Manual',
    'client_comment' => 'Client Comment',
    'recall_if_dropped' => 'Recall If Dropped',
    'columns' => 'Columns',
    'final_action' => 'Final Action',
    'action_back_confirm' => 'Do you want to do action back orders to this orders?',
    'ticket_rules' => 'Ticket Rules',
    'ticket_rule' => 'Ticket Rule',
    'dest' => 'Dest',
    'fees' => 'Fees',
    'e.fees' => 'E.Fees',
    'f.fees' => 'F.Fees',
    'fees_from' => 'Fees From',
    'place' => 'Place',
    'last_update_date' => 'Last Update Date',
    'ship_cost' => 'Ship Cost',
    'settlement' => 'Settlement',
    'residual' => 'Residual',
    'net' => 'Net',
    'arrange_by_type' => 'Arrange By Type',
    'per_page' => 'Per Page',
    'change_ship_cost' => 'Change Ship Cost',
    'change' => 'Change',
    'debit_amount' => 'Debit Amount',
    'bonus' => 'Bonus',

    'tasks_list' => 'Tasks List',
    'task_no' => 'Task No',
    'orders_in_task' => 'No of Orders in Task',
    'task_price' => 'Task Price',
    'task_cost' => 'Task Cost',
    'invoice_tasks' => 'Invoice Tasks',

    'total_ship_cost' => 'Total Ship Cost',
    'total_task_cost' => 'Total Task Cost',
    'total_settlement' => 'Total Settlement',
    'debit' => 'Debit',
    'collect' => 'Collect',
    'auto_close_invoice' => 'Auto Close Invoice',
    'Invoice_collected_successfully' => 'Invoice collected successfully.',
    'total_residual' => 'Total Residual',
    'final_net' => 'Final Net',
    'client_deserved' => 'Client Deserved',
    'total_order_cost' => 'Total Order Cost',
    'total_fees' => 'Total Fees',
    'create_invoice' => 'Create Invoice',
   
    'payment_status' => 'Payment Status',
    'payment_date' => 'Payment Date',
    'upload_invoice' => 'Upload Invoice',
    'paid_amount' => 'Paid Amount'
];

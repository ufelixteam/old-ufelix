<div class="modal fade" id="forward_agentMo" tabindex="-1" role="dialog" aria-labelledby="forward_agentMo" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="forward_agentMo"><?php echo e(__('backend.Forward_Pickup_To_Another_Agent')); ?></h5>
        </div>
        <div class="modal-body">
            <form class="" action="<?php echo e(url('mngrAdmin/forward_pickup_agent')); ?>" method="post">
                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                <input type="hidden" name="pickup_id" value="<?php echo e($pickup->id); ?>">

                <div class="form-group">
                    <select id="agent-field" name="agent_id" id="agent" class="form-control">
                        <option ><?php echo e(__('backend.Choose_Agent')); ?></option>
                        <?php if(! empty($agents)): ?>

                            <?php $__currentLoopData = $agents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $agent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <option value="<?php echo e($agent->id); ?>"
                                        <?php echo e($agent->id == $pickup->agent_id ? 'selected' : ''); ?>><?php echo e($agent->name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><?php echo e(__('backend.forward')); ?></button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo e(__('backend.close')); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>
</div><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/pickups/agent_forward_modal.blade.php ENDPATH**/ ?>
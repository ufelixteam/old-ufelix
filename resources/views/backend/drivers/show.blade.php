@extends('backend.layouts.app')

@section('css')
    <title>{{__('backend.drivers')}} - {{__('backend.view')}}</title>
    <style>
        .img {
            /*border: 1px solid #8e7ad4;*/
            height: 100px;
            width: 100px;
            border-radius: 50%;
            /*padding: 10px;*/
        }

        .form-group {
            margin-top: 15px !important;
        }
    </style>
@endsection

@section('header')
    <div class="page-header">
        <h3># {{$driver->id}} - {{__('backend.driver')}} / {{$driver->mobile}} - {{$driver->name}} </h3>
        {!! $driver->status_span !!}    {!! $driver->active_span !!}    {!! $driver->block_span !!} {{$driver->created_at}}

        <form action="{{ url('mngrAdmin/del', $driver->id) }}" method="POST" style="display: inline;"
              onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
            @if(permission('blockedDriver')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <button id="block" type="button" value="{{ $driver->id }}"
                        class="btn btn-success">{{ $driver->block_txt }}</button>
            @endif

            @if(permission('activeDriver')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <button id="active" type="button" value="{{ $driver->id }}"
                        class="btn btn-info">{{ $driver->active_txt }}</button>
            @endif

            @if(permission('editDriver')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <a class="btn btn-warning btn-group" role="group"
                   href="{{ route('mngrAdmin.drivers.edit', $driver->id) }}"><i
                        class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
            @endif

            @if(permission('deleteDriver')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <button type="submit" class="btn btn-danger"><i
                        class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}</button>
            @endif

            @if(permission('reports')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <a class="btn btn-success btn-group"
                   href="{{url('mngrAdmin/reports/captain_report') . '?driver_id=' . $driver->id}}"
                   target="_blank"><i
                        class="fa fa-file-excel-o"></i> {{__('backend.daily_report')}}</a>
            @endif
            @if(permission('showDriver')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <a class="btn btn-success btn-group"
                   href="{{url('mngrAdmin/drivers/'.$driver->id.'/download_papers')}}"
                   target="_blank"><i
                        class="fa fa-file-zip-o"></i> {{__('backend.download_papers')}}</a>
                @endif
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="col-sm-12"
                 style="border: 2px solid #fdb801; color: #fff; margin-bottom: 20px; margin-top: 20px; background-color: #fdb801;">
                <h4>{{__('backend.personal_information')}}</h4>
            </div>
            <div class="form-group col-sm-3">
                <img src="{{ $driver->image }}" class="img" onerror="this.src='{{asset('assets/images/user.png')}}'">
            </div>
            <div class="form-group col-sm-3">
                <label for="mobile">{{__('backend.email')}}</label>
                <p class="form-control-static">{{$driver->email}}</p>
            </div>
            {{--            <div class="form-group col-sm-3">--}}
            {{--                <label for="phone">{{__('backend.mobile_number2')}}</label>--}}
            {{--                <p class="form-control-static">{{ $driver->phone }}</p>--}}
            {{--            </div>--}}
            {{--            <div class="form-group col-sm-3">--}}
            {{--                <label for="country">{{__('backend.country')}}</label>--}}
            {{--                <p class="form-control-static">{{ ! empty($driver->country) ?  $driver->country->name : '' }}</p>--}}
            {{--            </div>--}}
            <div class="form-group col-sm-3">
                <label for="city">{{__('backend.government')}}</label>
                <p class="form-control-static">{{ ! empty($driver->government) ?  $driver->government->name_en : '' }}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="city">{{__('backend.city')}}</label>
                <p class="form-control-static">{{ ! empty($driver->city) ?  $driver->city->name_en : '' }}</p>
            </div>

            <div class="form-group col-sm-3">
                <label for="profit_rate">{{__('backend.profit_rate')}}</label>
                <p class="form-control-static">{{ $driver->profit ?  $driver->profit : '0' }} %</p>
            </div>

            <div class="form-group col-sm-3">
                <label for="recall_price">{{__('backend.recall_price')}}</label>
                <p class="form-control-static">{{ $driver->recall_price ?  $driver->recall_price : '0' }} </p>
            </div>
            <div class="form-group col-sm-3">
                <label for="reject_price">{{__('backend.reject_price')}}</label>
                <p class="form-control-static">{{ $driver->reject_price ?  $driver->reject_price : '0' }} </p>
            </div>

            <div class="form-group col-sm-3">
                <label for="pickup_price">{{__('backend.pickup_price')}}</label>
                <p class="form-control-static">{{ $driver->pickup_price ?  $driver->pickup_price : '0' }} </p>
            </div>
            <div class="form-group col-sm-3">
                <label for="basic_salary">{{__('backend.basic_salary')}}</label>
                <p class="form-control-static">{{ $driver->basic_salary ?  $driver->basic_salary : '0' }} </p>
            </div>
            <div class="form-group col-sm-3">
                <label for="bonus_of_delivery">{{__('backend.bonus_of_delivery')}}</label>
                <p class="form-control-static">{{ $driver->bouns_of_delivery ?  $driver->bouns_of_delivery : '0' }} </p>
            </div>

            {{--            <div class="form-group col-sm-3">--}}
            {{--                <label for="driving_licence_number">{{__('backend.drivier_licence_number')}}: </label>--}}
            {{--                <p class="form-control-static">{{$driver->driving_licence_number}}</p>--}}
            {{--            </div>--}}
            <div class="form-group col-sm-3">
                <label for="driving_licence_expired_date">{{__('backend.drivier_licence_expiration_date')}}: </label>
                <p class="form-control-static">{{$driver->driving_licence_expired_date}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="national_id_expired_date">{{__('backend.national_id_expired_date')}}: </label>
                <p class="form-control-static">{{$driver->national_id_expired_date}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="national_id_number">{{__('backend.national_id_number')}}: </label>
                <p class="form-control-static">{{$driver->national_id_number}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="address">{{__('backend.address')}}: </label>
                <p class="form-control-static">{{$driver->address}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="store">{{__('backend.store')}}: </label>
                <p class="form-control-static">{{!empty($driver->store) ? $driver->store->name : ''}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="notes">{{__('backend.notes')}}: </label>
                <p class="form-control-static">
                    @if($driver->notes == null)
                        {{__('backend.No_Notes_to_Show')}}
                    @else
                        {{$driver->notes}}
                    @endif
                </p>
            </div>
            <div class="col-sm-12">
                @if($driver->driver_has_vehicle == 1)
                    <div class="form-group col-sm-4" style="margin-top: 60px;">
                        <h4 for="driving_licence_image">{{__('backend.drivier_licence_image_front')}}: </h4>
                        <p class="form-control-static"><img class="img" src="{{ $driver->driving_licence_image_front }}"
                                                            onerror="this.src='{{asset('assets/images/image.png')}}'">
                        </p>
                    </div>
                @endif
                <div class="form-group col-sm-4" style="margin-top: 60px;">
                    <h4 for="driving_licence_image">{{__('backend.criminal_record_image')}}: </h4>
                    <p class="form-control-static"><img class="img" src="{{ $driver->criminal_record_image_front }}"
                                                        onerror="this.src='{{asset('assets/images/image.png')}}'"></p>
                </div>
                <div class="form-group col-sm-4" style="margin-top: 60px;">
                    <h4 for="national_id_image">{{__('backend.national_id_image_front')}}: </h4>
                    <p class="form-control-static"><img class="img" src="{{ $driver->national_id_image_front }}"
                                                        onerror="this.src='{{asset('assets/images/image.png')}}'"></p>
                </div>
            </div>
        <!-- <div class="form-group col-sm-6">
       <h4 for="national_id_image">{{__('backend.national_id_image_back')}}: </h4>
       <p class="form-control-static"><img src="{{ $driver->national_id_image_back }}" style="width:360px; height: 350px;"></p>
    </div> -->


            <?php if(!empty($driver->device_active) ) { ?>
            <div class="form-group col-sm-12" style="width:100%;height:500px;">
                <h4>{{__('backend.Driver_location')}}: </h4>
                <?php

                //                if ($driver->device_active->status == 1) {


                Mapper::map($driver->device_active->latitude, $driver->device_active->longitude, ['marker' => false, 'locate' => false])->informationWindow($driver->device_active->latitude, $driver->device_active->longitude, "Driver Name:  {$driver->name} - Mobile: {$driver->mobile}", ['title' => 'Online', 'icon' => [
                    'path' => 'M365.027,44.5c-30-29.667-66.333-44.5-109-44.5s-79,14.833-109,44.5s-45,65.5-45,107.5    c0,25.333,12.833,67.667,38.5,127c25.667,59.334,51.333,113.334,77,162s38.5,72.334,38.5,71c4-7.334,9.5-17.334,16.5-30    s19.333-36.5,37-71.5s33.167-67.166,46.5-96.5c13.334-29.332,25.667-59.667,37-91s17-55,17-71    C410.027,110,395.027,74.167,365.027,44.5z M289.027,184c-9.333,9.333-20.5,14-33.5,14c-13,0-24.167-4.667-33.5-14    s-14-20.5-14-33.5s4.667-24,14-33s20.5-13.5,33.5-13.5c13,0,24.167,4.5,33.5,13.5s14,20,14,33S298.36,174.667,289.027,184z',
                    'fillColor' => '#62d233',
                    'fillOpacity' => 0.8,
                    'scale' => 0.08,
                    'strokeColor' => '#006400',
                    'locate' => false,
                    'strokeWeight' => 1.5
                ]]);

                //                } else {
                //                    Mapper::map($driver->device->latitude, $driver->device->longitude, ['marker' => false, 'locate' => false])->informationWindow($driver->device->latitude, $driver->device->longitude, "Driver Name:  {$driver->name} - Mobile: {$driver->mobile}", ['title' => 'Offline', 'icon' => [
                //                        'path' => 'M365.027,44.5c-30-29.667-66.333-44.5-109-44.5s-79,14.833-109,44.5s-45,65.5-45,107.5    c0,25.333,12.833,67.667,38.5,127c25.667,59.334,51.333,113.334,77,162s38.5,72.334,38.5,71c4-7.334,9.5-17.334,16.5-30    s19.333-36.5,37-71.5s33.167-67.166,46.5-96.5c13.334-29.332,25.667-59.667,37-91s17-55,17-71    C410.027,110,395.027,74.167,365.027,44.5z M289.027,184c-9.333,9.333-20.5,14-33.5,14c-13,0-24.167-4.667-33.5-14    s-14-20.5-14-33.5s4.667-24,14-33s20.5-13.5,33.5-13.5c13,0,24.167,4.5,33.5,13.5s14,20,14,33S298.36,174.667,289.027,184z',
                //                        'fillColor' => '#808080',
                //                        'fillOpacity' => 0.8,
                //                        'scale' => 0.08,
                //                        'locate' => false,
                //                        'strokeColor' => '#808080',
                //                        'strokeWeight' => 1.5
                //                    ]]);
                //                }

                $map = Mapper::render();
                echo $map;
                ?>
            </div>
            <?php } ?>

            @if (! empty($driver->agent))
                <div class="col-sm-12"
                     style="margin-top: 90px; border: 2px solid #fdb801;color: #fff;margin-bottom: 20px;background-color: #fdb801;">
                    <h4>{{__('backend.Belonging_Agent_Information')}}</h4>
                </div>
                <div class="form-group col-sm-4">
                    <label for="agent_id">{{__('backend.Belonging_Agent_Name')}}: </label>
                    <p class="form-control-static">{{$driver->agent->name}}</p>
                </div>
                <div class="form-group col-sm-4">
                    <label for="mobile">{{__('backend.Belonging_Agent_Mobile')}}: </label>
                    <p class="form-control-static">{{$driver->agent->mobile}}</p>
                </div>
                <div class="form-group col-sm-4">
                    <label for="phone">{{__('backend.Belonging_Agent_Mobile2')}}: </label>
                    <p class="form-control-static">{{$driver->agent->phone}}</p>
                </div>
            @endif

            @if (! empty($driver->truck))

                <div class="col-sm-12"
                     style="border: 2px solid #fdb801;color: #fff;margin-bottom: 20px;background-color: #fdb801;">
                    <h4>{{__('backend.Truck_Information')}}: </h4>
                </div>

                <div class="col-sm-12">
                    <div class="form-group col-sm-1">
                        <label for="notes"> {{__('backend.id')}}: </label>
                        <p class="form-control-static">
                            {{ ! empty($driver->truck->id)? $driver->truck->id : ''}}
                        </p>
                    </div>
                    <div class="form-group col-sm-2">
                        <label for="notes"> {{__('backend.Model_Name')}}: </label>
                        <p class="form-control-static">
                            {{ ! empty($driver->truck->model_name)? $driver->truck->model_name : ''}}
                        </p>
                    </div>
                    <div class="form-group col-sm-2">
                        <label for="notes"> {{__('backend.Plate_Number')}}: </label>
                        <p class="form-control-static">
                            {{ ! empty($driver->truck->plate_number)? $driver->truck->plate_number : ''}}
                        </p>
                    </div>

                    <div class="form-group col-sm-2">
                        <label for="notes"> {{__('backend.Chassi_Number')}}</label>
                        <p class="form-control-static">
                            {{ ! empty($driver->truck->chassi_number)? $driver->truck->chassi_number : ''}}
                        </p>
                    </div>

                    <div class="form-group col-sm-3">
                        <label for="notes"> {{__('backend.License_Image_Front')}}</label>
                        <p class="form-control-static"><img class="img" src="{{ $driver->truck->license_image_front }}"
                                                            onerror="this.src='{{asset('assets/images/image.png')}}'">
                        </p>

                    </div>
                    <div class="form-group col-sm-1">
                        <label for="notes"> </label>
                        <p class="form-control-static pull-left">
                            <a href="{{url('/mngrAdmin/trucks/'.$driver->truck->id)}}" class="btn btn-default"
                               target="_blank">{{__('backend.View_All')}}</a>
                        </p>
                    </div>

                </div>
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <hr style="color: green">
                </div>

            @else
                <div class="form-group col-sm-12 text-center"><b>{{__('backend.This_Driver_Not_Have_Any_Vehicles')}}</b>
                </div>

            @endif
        </div>

        <div class="col-md-12 col-xs-12 col-sm-12">
            <a class="btn btn-link pull-right" href="{{ URL::previous()}}"><i
                    class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function () {
            $('#block').on('click', function () {
                var Status = $(this).val();
                // alert(Status);
                $.ajax({
                    url: "{{ url('/mngrAdmin/driver/block/'.$driver->id) }}",
                    Type: "GET",
                    dataType: 'json',
                    success: function (response) {
                        if (response == 0) {
                            $('#block').html('Block');
                        } else {
                            $('#block').html('UnBlock');
                        }
                        location.reload();
                    }
                });
            });

            $('#active').on('click', function () {
                var Status = $(this).val();
                $.ajax({
                    url: "{{ url('/mngrAdmin/driver/active/'.$driver->id) }}",
                    Type: "GET",
                    dataType: 'json',
                    success: function (response) {
                        if (response == 0) {
                            $('#active').html('Active');

                        } else {
                            $('#active').html('UnActivate');
                        }
                        location.reload();
                    }
                });
            });
        });
    </script>
@endsection

@extends('frontend.profile.layouts.app')
@section('title')
    @lang('website.wallets')
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="table-responsive m-t-40">
                <h1 class="title">
                    {!! $wallet->corporates->name !!}
                    {!! $wallet->active_span !!}
                </h1>
                <div class="col-md-12 col-xs-12 col-sm-12" style="margin-top: 35px;">

                    @if($wallet->type != 0)
                        <div class="form-group col-sm-4">
                            <label for="object_id"> {{__('backend.Wallet_Owner_ID')}}: </label>
                            <p class="form-control-static">#{{$wallet->object_id}}

                            </p>
                        </div>


                        <div class="form-group col-sm-4">
                            <label for="type">{{__('backend.Wallet_Owner')}}:</label>
                            <p> @if($wallet->type == 1)
                                    {{$wallet->drivers->name}}
                                @elseif($wallet->type == 2)
                                    {{$wallet->corporates->name}}
                                @endif </p>
                        </div>

                    @endif
                    <div class="form-group col-sm-4">
                        <label for="value">{{__('backend.Wallet_Amount')}}: </label>
                        <p class="form-control-static">{{$wallet->amount}}</p>
                    </div>
                </div>


                <div class="col-md-12 col-xs-12 col-sm-12" style="margin-top: 35px;">

                    <h4 class="text-center text-warning"><strong> - - {{__('backend.Wallet_Transactions')}} -
                            - </strong></h4>


                    <div class="row">
                        <div class="col-md-12 col-xs-12 col-sm-12">
                            @if( ! empty($wallet->logs) && $wallet->logs->count())
                                <table id="theTable" class="table table-condensed table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{__('backend.value')}}</th>
                                        <th>{{__('backend.invoice')}}</th>
                                        <th>{{__('backend.reason')}}</th>
                                        {{--                                        <th>{{__('backend.type')}}</th>--}}
                                        {{--                                        <th class="text-left"></th>--}}
                                    </tr>
                                    </thead>
                                    <tbody class="text-center">
                                    @foreach($wallet->logs as $log)
                                        <tr>
                                            <td>{{ $log->id }}</td>

                                            <td>{{ $log->value }}</td>

                                            <td>@if($log->invoice_id > 0) <a
                                                    href="{{ URL::asset('/mngrAdmin/invoices/'.$log->invoice_id)}}"><strong> {{$log->invoice_id}} </strong></a> @endif
                                            </td>

                                            <td>{{ $log->reason }}</td>

                                            {{--                                            <td>{!! $log->type_span !!}</td>--}}

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <h3 class="text-center alert alert-warning">{{__('backend.No_Wallet_Log')}}</h3>
                            @endif
                        </div>
                    </div>

                </div>

                <div class="col-md-12 col-xs-12 col-sm-12">
                    <a class="btn btn-link pull-left" href="{{ route('mngrAdmin.wallets.index') }}"><i
                            class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.the_pickups')}} - {{__('backend.edit')}}</title>
    <style>
        .input-group {
            -moz-box-shadow: none !important;
            -webkit-box-shadow: none !important;
            box-shadow: none !important;
        }

        .input-group[class*=col-] {
            padding-right: inherit !important;
            padding-left: inherit !important;
            float: inherit;
        }

        .pac-container {
            z-index: 9999 !important;
        }

        .form-group {
            min-height: 94px;
        }

    </style>
@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-plus"></i> {{__('backend.PickUp_Orders')}} / {{__('backend.create_pickup')}}
        </h3>
    </div>
@endsection

@section('content')
    @include('error')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <form action="{{ route('mngrAdmin.pickups.create_fake') }}" method="POST" name="pickup_create"
                  enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                {{--                <div class="col-md-12 col-sm-12 col-xs-12" style="border: 2px solid #607d8b;margin-bottom: 15px;">--}}
                {{--                    <h4 style="color: #607d8b;font-weight: bold">Receiver Data</h4>--}}

                {{--                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('store_id')) has-error @endif">--}}
                {{--                        <label for="store_id">{{__('backend.dropped_pickup_store')}}:</label>--}}
                {{--                        <select id="store_id" name="store_id" class="form-control">--}}
                {{--                            <option value="">{{__('backend.Choose_Store')}}</option>--}}
                {{--                            @if(! empty($allStores))--}}
                {{--                                @foreach($allStores as $store)--}}
                {{--                                    <option--}}
                {{--                                        value="{{$store->id}}" {{ $store->id == old('store_id') ? 'selected' : '' }} >--}}
                {{--                                        {{$store->name}}--}}
                {{--                                    </option>--}}
                {{--                                @endforeach--}}
                {{--                            @endif--}}

                {{--                        </select>--}}
                {{--                        @if($errors->has("store_id"))--}}
                {{--                            <span class="help-block">{{ $errors->first("store_id") }}</span>--}}
                {{--                        @endif--}}
                {{--                    </div>--}}

                {{--                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('receiver_name')) has-error @endif">--}}
                {{--                        <label for="receiver_name-field"> Name: </label>--}}
                {{--                        <input type="text" id="receiver_name-field" name="receiver_name" class="form-control"--}}
                {{--                               value="{{ ! empty($pickup) ? $pickup->receiver_name : old("receiver_name") }}"/>--}}
                {{--                        @if($errors->has("receiver_name"))--}}
                {{--                            <span class="help-block">{{ $errors->first("receiver_name") }}</span>--}}
                {{--                        @endif--}}
                {{--                    </div>--}}
                {{--                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('receiver_mobile')) has-error @endif">--}}
                {{--                        <label for="reciever_mobile-field"> Mobile: </label>--}}
                {{--                        <input type="text" id="receiver_mobile-field" name="receiver_mobile" class="form-control"--}}
                {{--                               value="{{old("receiver_mobile") }}"/>--}}
                {{--                        @if($errors->has("receiver_mobile"))--}}
                {{--                            <span class="help-block">{{ $errors->first("receiver_mobile") }}</span>--}}
                {{--                        @endif--}}
                {{--                    </div>--}}

                {{--                    <div class="form-group col-md-3 col-sm-3">--}}
                {{--                        <label class="control-label" for="r_government_id"> Government</label>--}}

                {{--                        <select class="form-control r_government_id" id="r_government_id" name="r_government_id">--}}
                {{--                            <option value="" data-display="Select">Receiver Government</option>--}}
                {{--                            @if(! empty($app_governments))--}}
                {{--                                @foreach($app_governments as $government)--}}
                {{--                                    <option--}}
                {{--                                        value="{{$government->id}}" {{ old('r_government_id')  == $government->id ? 'selected':  '' }} >--}}
                {{--                                        {{$government->name_en}}--}}
                {{--                                    </option>--}}

                {{--                                @endforeach--}}
                {{--                            @endif--}}

                {{--                        </select>--}}

                {{--                    </div>--}}

                {{--                    <div class="form-group col-md-2 col-sm-2">--}}
                {{--                        <label class="control-label" for="r_state_id"> City</label>--}}

                {{--                        <select class=" form-control r_state_id" id="r_state_id" name="r_state_id">--}}
                {{--                            @if(! empty($r_cities))--}}

                {{--                                @foreach($r_cities as $r_city)--}}
                {{--                                    <option--}}
                {{--                                        value="{{$r_city->id}}" {{old('r_state_id')  == $r_city->id ? 'selected':  '' }} >--}}
                {{--                                        {{$r_city->name_en}}--}}
                {{--                                    </option>--}}

                {{--                                @endforeach--}}
                {{--                            @endif--}}

                {{--                        </select>--}}

                {{--                    </div>--}}
                {{--                    <div--}}
                {{--                        class="form-group input-group  col-md-2 col-sm-2 @if($errors->has('receiver_latitude')) has-error @endif">--}}
                {{--                        <label for="receiver_latitude"> Latitude: </label>--}}
                {{--                        <input type="text" id="receiver_latitude" name="receiver_latitude" class="form-control"--}}
                {{--                               value="{{ old('receiver_latitude') ? old('receiver_latitude') :  '30.0668886'  }}"/>--}}
                {{--                        <span class="input-group-append">--}}
                {{--                  <button class="input-group-text bg-transparent" type="button" data-toggle="modal"--}}
                {{--                          data-target="#map1Modal"> <i class="fa fa-map-marker"></i></button>--}}
                {{--                </span>--}}
                {{--                        @if($errors->has("receiver_latitude"))--}}
                {{--                            <span class="help-block">{{ $errors->first("receiver_latitude") }}</span>--}}
                {{--                        @endif--}}
                {{--                    </div>--}}
                {{--                    <div class="form-group col-md-2 col-sm-2 @if($errors->has('receiver_longitude')) has-error @endif">--}}
                {{--                        <label for="receiver_longitude"> Longitude: </label>--}}
                {{--                        <input type="text" id="receiver_longitude" name="receiver_longitude" class="form-control"--}}
                {{--                               value="{{ old('receiver_longitude') ? old('receiver_longitude') :  '31.1962743'  }}"/>--}}
                {{--                        @if($errors->has("receiver_longitude"))--}}
                {{--                            <span class="help-block">{{ $errors->first("receiver_longitude") }}</span>--}}
                {{--                        @endif--}}
                {{--                    </div>--}}


                {{--                    <div--}}
                {{--                        class="form-group col-md-6 col-sm-6 @if($errors->has('receiver_address')) has-error @endif">--}}
                {{--                        <label for="receiver_address">Receiver Address: </label>--}}

                {{--                        <input type="text" id="receiver_address" name="receiver_address" class="form-control"--}}
                {{--                               value="{{ old("receiver_address") }}"/>--}}


                {{--                        @if($errors->has("receiver_address"))--}}
                {{--                            <span class="help-block">{{ $errors->first("receiver_address") }}</span>--}}
                {{--                        @endif--}}
                {{--                    </div>--}}

                {{--                </div>--}}


                <div class="col-md-12 col-sm-12 col-xs-12" style="border: 2px solid #607d8b;margin-bottom: 15px;">
                    <h4 style="color: #607d8b;font-weight: bold">Sender Data</h4>

                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('corporate_id')) has-error @endif">
                        <label for="corparate_id-field">{{__('backend.corporate')}}</label>

                        <select class="form-control" id="corporate_id"
                                name="corporate_id">
                            <option value="" data-display="Select">{{__('backend.corporate')}}</option>
                            @if(! empty($corporates))
                                @foreach($corporates as $corporate)
                                    <option
                                        value="{{$corporate->id}}">
                                        {{ $corporate->id.' - '.$corporate->name}}
                                    </option>

                                @endforeach
                            @endif

                        </select>

                        @if($errors->has("corporate_id"))
                            <span class="help-block">{{ $errors->first("corporate_id") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('sender_name')) has-error @endif">
                        <label for="sender_mobile-field">{{__('backend.name')}}: </label>
                        <input type="text" id="sender_name" name="sender_name" class="form-control"
                               value="{{old("sender_name") }}"/>
                        @if($errors->has("sender_name"))
                            <span class="help-block">{{ $errors->first("sender_name") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('sender_mobile')) has-error @endif">
                        <label for="sender_mobile-field">{{__('backend.mobile')}}: </label>
                        <input type="text" id="sender_mobile-field" name="sender_mobile" class="form-control"
                               value="{{old("sender_mobile") }}"/>
                        @if($errors->has("sender_mobile"))
                            <span class="help-block">{{ $errors->first("sender_mobile") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-3 col-sm-3">
                        <label class="control-label" for="s_government_id">Government</label>

                        <select class="form-control s_government_id" id="s_government_id" name="s_government_id">
                            <option value="" data-display="Select">Government</option>
                            @if(! empty($app_governments))
                                @foreach($app_governments as $government)
                                    <option
                                        value="{{$government->id}}">
                                        {{$government->name_en}}
                                    </option>

                                @endforeach
                            @endif

                        </select>

                    </div>

                    <div class="form-group col-md-2 col-sm-2">
                        <label class="control-label" for="s_state_id">City</label>

                        <select class=" form-control s_state_id" id="s_state_id" name="s_state_id">
                            @if(! empty($s_cities))

                                @foreach($s_cities as $s_city)
                                    <option
                                        value="{{$s_city->id}}">
                                        {{$s_city->name_en}}
                                    </option>

                                @endforeach
                            @endif

                        </select>

                    </div>


                    <div
                        class="form-group input-group  col-md-2 col-sm-2 @if($errors->has('sender_latitude')) has-error @endif">
                        <label for="sender_latitude">Latitude: </label>
                        <input type="text" id="sender_latitude" name="sender_latitude" class="form-control"
                               value="{{  old('sender_latitude')}}"/>
                        <span class="input-group-append">
                  <button class="input-group-text bg-transparent" type="button" data-toggle="modal"
                          data-target="#map2Modal"> <i class="fa fa-map-marker"></i></button>
                </span>
                        @if($errors->has("sender_latitude"))
                            <span class="help-block">{{ $errors->first("sender_latitude") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-2 col-sm-2 @if($errors->has('sender_longitude')) has-error @endif">
                        <label for="sender_longitude">Longitude: </label>
                        <input type="text" id="sender_longitude" name="sender_longitude" class="form-control"
                               value="{{old('sender_longitude') }}"/>
                        @if($errors->has("sender_longitude"))
                            <span class="help-block">{{ $errors->first("sender_longitude") }}</span>
                        @endif
                    </div>

                    <div
                        class="form-group col-md-6 col-sm-6 @if($errors->has('sender_address')) has-error @endif">
                        <label for="sender_address">Sender Address: </label>
                        <input type="text" id="sender_address" name="sender_address" class="form-control"
                               value="{{  old("sender_address") }}"/>

                        @if($errors->has("sender_address"))
                            <span class="help-block">{{ $errors->first("sender_address") }}</span>
                        @endif
                    </div>

                </div>


                <div class="col-md-12 col-sm-12 col-xs-12" style="border: 2px solid #607d8b;margin-bottom: 15px;">
                    <h4 style="color: #607d8b;font-weight: bold">Order Data</h4>

{{--                    <div class="form-group col-md-2 col-sm-2 @if($errors->has('agent_id')) has-error @endif">--}}
{{--                        <label for="agent_id-field">Agent </label>--}}

{{--                        <select id="agent_id-field" name="agent_id" class="form-control">--}}
{{--                            <option value="">Choose Agent</option>--}}
{{--                            @if(! empty($agents))--}}
{{--                                @foreach($agents as $agent)--}}
{{--                                    <option--}}
{{--                                        value="{{$agent->id}}">--}}
{{--                                        {{$agent->name}}--}}
{{--                                    </option>--}}
{{--                                @endforeach--}}
{{--                            @endif--}}

{{--                        </select>--}}
{{--                        @if($errors->has("agent_id"))--}}
{{--                            <span class="help-block">{{ $errors->first("agent_id") }}</span>--}}
{{--                        @endif--}}
{{--                    </div>--}}

                    <div class="form-group col-md-2 col-sm-2">
                        <label class="control-label" for="driver_id">Driver</label>

                        <select class=" form-control driver_id" id="driver_id" name="driver_id">
                            <option value="">Choose Drivers</option>
                            @if(! empty($online_drivers))
                                @foreach($online_drivers as $driver)
                                    <option
                                        value="{{$driver->id}}">
                                        {{$driver->name}}
                                    </option>
                                @endforeach
                            @endif

                        </select>

                    </div>

                    <div class="form-group col-md-2 col-sm-3 @if($errors->has('delivery_price')) has-error @endif">
                        <label for="delivery_price-field">Delivery Price: </label>
                        <input type="text" class="form-control" id="delivery_price-field" name="delivery_price"
                               value="{{  old("delivery_price") }}">
                        @if($errors->has("delivery_price"))
                            <span class="help-block">{{ $errors->first("delivery_price") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-2 col-sm-2 @if($errors->has('bonus_per_order')) has-error @endif">
                        <label for="bonous-field">{{__('backend.the_bonous')}}: </label>
                        <input type="number" class="form-control" id="bonous-field" name="bonus_per_order"
                               value="{{old("bonus_per_order")}}">
                        @if($errors->has("bonus_per_order"))
                            <span class="help-block">{{ $errors->first("bonus_per_order") }}</span>
                        @endif
                    </div>
                    <div class="group-control col-md-2 col-sm-2">
                        <label class="control-label"
                               for="captain_received_date"> {{__('backend.captain_received_date')}}:</label>

                        <div class='input-group date' id='datepickerFilter'>
                            <input type="text" id="datepicker" name="captain_received_date" class="form-control"
                                   value=""/>
                            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>


                    <div class="group-control col-md-2 col-sm-2">
                        <label class="control-label" for="driver_id"> {{__('backend.captain_received_time')}}:</label>

                        <div class='input-group date' id='datepickerFilter'>
                            <input type="time" name="captain_received_time" class="form-control"
                                   value=""/>
                            <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                        </div>
                    </div>


                </div>

                <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
                    <button type="submit" class="btn btn-primary">{{__('backend.save')}}</button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.pickups.index') }}"><i
                            class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
                </div>
            </form>
        </div>
    </div>

    @include('backend.dialog-map1')
    @include('backend.dialog-map2')

@endsection
@section('scripts')
    <script type="text/javascript" src="{{asset('/assets/scripts/map-order.js')}}"></script>

    <script
        src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key=AIzaSyDQiN4qc7pC9b1BRQwkqXXD28peMfWcHvw&callback=myMap"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

@endsection

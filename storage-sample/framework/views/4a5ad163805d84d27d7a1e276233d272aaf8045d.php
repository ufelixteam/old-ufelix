<script src="<?php echo e(asset('/profile/js/jquery.js')); ?>"></script>
<script src="<?php echo e(asset('/profile/js/bootstrap.min.js')); ?>"></script>


<script src="<?php echo e(asset('/profile/js/retina.js')); ?>"></script>
<script src="<?php echo e(asset('/profile/js/custom-sparkline.js')); ?>"></script>
<script src="<?php echo e(asset('/profile/js/jquery.scrollUp.js')); ?>"></script>
<script src="<?php echo e(asset('/profile/js/d3.v3.min.js')); ?>"></script>
<script src="<?php echo e(asset('/profile/js/d3.powergauge.js')); ?>"></script>
<script src="<?php echo e(asset('/profile/js/gauge.js')); ?>"></script>
<script src="<?php echo e(asset('/profile/js/gauge-custom.js')); ?>"></script>
<script src="<?php echo e(asset('/profile/js/c3.min.js')); ?>"></script>
<script src="<?php echo e(asset('/profile/js/c3.custom.js')); ?>"></script>
<script src="<?php echo e(asset('/profile/js/nv.d3.js')); ?>"></script>
<script src="<?php echo e(asset('/profile/js/nv.d3.custom.boxPlotChart.js')); ?>"></script>
<script src="<?php echo e(asset('/profile/js/nv.d3.custom.stackedAreaChart.js')); ?>"></script>
<script src="<?php echo e(asset('/profile/js/horizBarChart.min.js')); ?>"></script>
<script src="<?php echo e(asset('/profile/js/horizBarCustom.js')); ?>"></script>
<script src="<?php echo e(asset('/profile/js/gaugeMeter-2.0.0.min.js')); ?>"></script>
<script src="<?php echo e(asset('/profile/js/gaugemeter.custom.js')); ?>"></script>


<script src="<?php echo e(asset('/profile/js/peity.min.js')); ?>"></script>
<script src="<?php echo e(asset('/profile/js/custom-peity.js')); ?>"></script>
<script src="<?php echo e(asset('/profile/js/circliful.min.js')); ?>"></script>
<script src="<?php echo e(asset('/profile/js/circliful.custom.js')); ?>"></script>
<script src="<?php echo e(asset('/profile/js/custom.js')); ?>"></script><?php /**PATH /home/www/ufelix/ufelix/resources/views/frontend/profile/layouts/js.blade.php ENDPATH**/ ?>
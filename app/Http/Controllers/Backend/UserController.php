<?php

namespace App\Http\Controllers\Backend;

use App\Models\Agent;
use App\Models\Contact;
use App\Models\Corporate;
use App\Models\Customer;
use App\Models\Driver;
use App\Models\Order;
use App\Models\Role;
use App\Models\Role_admin;
use App\Models\Truck;
use App\Models\User;
use Auth;
use Config;
use File;
use Flash;
use Illuminate\Http\Request;
use Validator;

class UserController extends Controller
{
    private $guardType;

    public function __construct(Request $request)
    {
        if ($request->is('mngrAdmin/*') || $request->is('admin/*')) {
            Config::set('auth.defaults.guard', 'admin');

        } elseif ($request->is('mngrAgent/*') || $request->is('agent/*')) {
            Config::set('auth.defaults.guard', 'agent');
        }
        $this->guardType = Config::get('auth.defaults.guard');

        //echo $this->guardType;die();

        // config(['auth.defaults.guard' => 'admin']);
    }

    //Users By Roles
    public function get_users_type($key)
    {
        $role = Role::where('name', $key)->first();
        if (count($role) > 0) {
            $users = User::whereHas('roles', function ($q) use ($key) {
                $q->where('name', $key);
            })->paginate(50);
            $title = $role->display_name;
            return view('backend.users.index', compact('users', 'title', 'key'));
        } else {
            return redirect('/mngrAdmin/404');
        }
    }

    // Get All Admins
    public function index()
    {
        $users = User::whereHas('roles', function ($q) {
            $q->where('name', 'user');
        })->paginate(50);
        return view('backend.users.index', compact('users'));
    }

    // Search In Admins
    public function search_users($key)
    {
        $users = User::where('mobile', 'LIKE', $key . "%")->
        orWhere('name', 'LIKE', $key . "%")->
        orWhere('email', 'LIKE', $key . "%")->
        orWhere('id', 'LIKE', $key . "%")->
        get();
        return $users;
    }

    // Create Admin Page
    public function create()
    {
        $role_admins = Role_admin::get();
        return view('backend.users.create', compact('role_admins'));
    }

    // Added New Admin
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'string|max:255',
            'email' => 'max:255|unique:users',
            'mobile' => 'required|string|max:255|unique:users',
            'password' => 'required|min:4|confirmed',
            'role_admin_id' => 'required',
        ]);

        $role = Role::where('name', 'admin')->first();
        $user = new User();
        $user->name = $request->input("name");
        $user->email = $request->input("email");
        $user->mobile = $request->input("mobile");
        $user->password = bcrypt($request->input("password"));
        $user->role_id = $role->id;
        $user->role_admin_id = $request->input("role_admin_id");
        $user->is_active = 1;
        $user->save();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Add New Admin ' . $user->name, ' اضافة مشرف جديد ' . $user->name, 31); // Helper Function in App\Helpers.php

        Flash::success(__('backend.User_added_successfully'));
        return redirect('/mngrAdmin/admins');
    }

    // Block Admin
    public function block_user(Request $request)
    {
        $user = User::findOrFail($request->user_id);
        if ($user->is_active == 1) {
            $user->is_active = 0;
            $msg = __('backend.user_is_unblocked_successfully');
        } else {
            $user->is_active = 1;
            $msg = __('backend.user_is_blocked_successfully');
        }

        return response()->json([
            'data' => [
                'success' => $user->save(),
                'message' => $msg,
            ]
        ]);
    }

    // View One Admin
    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('backend.users.show', compact('user'));
    }

    // Edit Admin Page
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $role_admins = Role_admin::get();
        return view('backend.users.edit', compact('user', 'role_admins'));
    }

    // Update Admin
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'string|max:255',
            'email' => 'email|max:255|unique:users,email,' . $id,
            'mobile' => 'required|string|max:255|unique:users,mobile,' . $id,
            'role_admin_id' => 'required',
        ]);

        $user = User::findOrFail($id);
        $user->name = $request->input("name");
        $user->email = $request->input("email");
        $user->mobile = $request->input("mobile");
        $user->role_admin_id = $request->input("role_admin_id");
        if ($request->input('password') != "") {
            $user->password = bcrypt($request->input("password"));
        }
        $user->save();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Edit Admin ' . $user->name, ' تعديل المشرف ' . $user->name, 31); // Helper Function in App\Helpers.php

        Flash::warning(__('backend.User_is_updated_successfully'));

        if ($user->id == Auth::guard($this->guardType)->user()->id && $request->input('password') != "") {
            Auth::guard($this->guardType)->login($user);
        }

        return redirect('/mngrAdmin/admins');
    }

    // Delete Admin
    public function destroy($id)
    {
        $user = User::where('id', $id)->first();

        if (!empty($user)) {
            $name = $user->name;
            User::where('id', $id)->delete();

            // Function Add Action In Activity Log By Samir
            addActivityLog('Delete Admin ' . $name, ' حذف المشرف ' . $name, 31); // Helper Function in App\Helpers.php

            flash(__('backend.User_deleted_successfully'))->error();
            return redirect('mngrAdmin/admins');

        } else {

        }

    }


    public function logout()
    {
        if (Auth::guard($this->guardType)->user()->role_id == '1') {
            Auth::guard($this->guardType)->logout();
            return redirect('/mngrAdmin');
        } elseif (Auth::guard($this->guardType)->user()->role_id == '2') {
            Auth::guard($this->guardType)->logout();
            return redirect('/mngrAgent');
        } else {
            return redirect('/mngrAdmin');
        }
    }

    function block($id)
    {
        $user = User::findOrFail($id);
        if ($user->is_active == 0) {
            $user->is_active = 1;

            // Function Add Action In Activity Log By Samir
            addActivityLog('Unblocked Admin ' . $user->name, ' الغاء حظر المشرف ' . $user->name, 31); // Helper Function in App\Helpers.php

        } else {
            $user->is_active = 0;

            // Function Add Action In Activity Log By Samir
            addActivityLog('Blocked Admin ' . $user->name, ' حظر المشرف ' . $user->name, 31); // Helper Function in App\Helpers.php
        }

        $user->save();
        return $user->is_active;
    }

    public function authenticate(Request $request)
    {
        //Fr3on
        $v = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password, 'role_id' => 1], true)) {
            if (Auth::guard('admin')->user()->is_active == 0) {

                Auth::guard('admin')->logout();
                return redirect('/mngrAdmin/login')->withErrors([__('backend.You_are_blocke')])->withInput($request->except('password'));
            }
            Config::set('auth.defines.guard', 'admin');
            return redirect('/mngrAdmin/dashboard');
        } else {
            Config::set('auth.defines.guard', 'admin');
            return redirect('/mngrAdmin/login')->withErrors([__('backend.Email_or_Password_is_wrong')])->withInput($request->except('password'));
        }
    }

    public function authenticate_agent(Request $request)
    {
        //Fr3on
        $v = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        if (Auth::guard('agent')->attempt(['email' => $request->email, 'password' => $request->password, 'role_id' => 2], true)) {
            if (Auth::guard('agent')->user()->is_active == 0) {
                Auth::guard('agent')->logout();
                return redirect('/mngrAgent/login')->withInput(\Request::except("password"))->withErrors(['error' => __('backend.You_are_blocke')]);

            } else if (!empty(Auth::guard('agent')->user()->agent) && Auth::guard('agent')->user()->agent->status == 0) {
                Auth::guard('agent')->logout();
                return redirect('/mngrAgent/login')->withInput(\Request::except("password"))->withErrors(['error' => __('backend.You_are_Not_Verify_Please_Contact_with_admin')]);
            }
            return redirect('/mngrAgent/dashboard');
        } else {

            Config::set('auth.defines.guard', 'agent');
            return redirect('/mngrAgent/login')->withErrors(['error' => __('backend.Email_or_Password_is_wrong')]);
        }
    }

    /* **-**-*-*-*-*-* Dashboard Admin -*-*-*-*-*-*-*-edit by samar *--*-*-*-*-*-*-- */
    public function dashboard()
    {
        //dd(Auth::guard($this->guardType)->user());die();
        $user = Auth::guard($this->guardType)->user();

        if ($user->is_warehouse_user) {
            $users = Customer::join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id)
                ->count();
            $drivers = Driver::join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                ->where('user_id', $user->id)
                ->count();
            $corporates = Corporate::join('customers', 'customers.corporate_id', '=', 'corporates.id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id)
                ->count();
            $orders = Order::join('store_user', 'store_user.store_id', '=', 'orders.moved_in')
                ->where('user_id', $user->id)
                ->count();
            $recentCorporates = Corporate::join('customers', 'customers.corporate_id', '=', 'corporates.id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id)
                ->select('corporates.*')
                ->orderBy('corporates.created_at', 'DESC')
                ->take(5)
                ->get();
            $recentCustomers = Customer::join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id)
                ->select('customers.*')
                ->where('role_id', 4)
                ->orderBy('customers.created_at', 'DESC')
                ->take(5)
                ->get();

            $neworders = Order::join('store_user', 'store_user.store_id', '=', 'orders.moved_in')
                ->where('user_id', $user->id)
                ->select('orders.*')
                ->orderBy('orders.created_at', 'DESC')->whereIn('status', [0, 6])->take(5)->get();
            $orderPending = Order::join('store_user', 'store_user.store_id', '=', 'orders.moved_in')
                ->where('user_id', $user->id)
                ->orderBy('orders.created_at', 'DESC')->whereIn('status', [0, 6])->count();
            $orderComplete = Order::join('store_user', 'store_user.store_id', '=', 'orders.moved_in')
                ->where('user_id', $user->id)
                ->orderBy('orders.created_at', 'DESC')->where('status', 3)->count();
            $orderRunning = Order::join('store_user', 'store_user.store_id', '=', 'orders.moved_in')
                ->where('user_id', $user->id)
                ->orderBy('orders.created_at', 'DESC')->where('status', 2)->count();
            $orderAccepted = Order::join('store_user', 'store_user.store_id', '=', 'orders.moved_in')
                ->where('user_id', $user->id)
                ->orderBy('orders.created_at', 'DESC')->where('status', 1)->count();
            $orderCancel = Order::join('store_user', 'store_user.store_id', '=', 'orders.moved_in')
                ->where('user_id', $user->id)
                ->orderBy('orders.created_at', 'DESC')->where('status', 4)->count();
        } else {
            $drivers = Driver::count();
            $users = Customer::count();
            $corporates = Corporate::count();
            $orders = Order::count();
            $recentCorporates = Corporate::orderBy('created_at', 'DESC')->take(5)->get();
            $recentCustomers = Customer::where('role_id', 3)->orderBy('created_at', 'DESC')->take(5)->get();
            $neworders = Order::orderBy('created_at', 'DESC')->whereIn('status', [0, 6])->take(5)->get();
            $orderPending = Order::orderBy('created_at', 'DESC')->whereIn('status', [0, 6])->count();
            $orderComplete = Order::orderBy('created_at', 'DESC')->where('status', 3)->count();
            $orderRunning = Order::orderBy('created_at', 'DESC')->where('status', 2)->count();
            $orderAccepted = Order::orderBy('created_at', 'DESC')->where('status', 1)->count();
            $orderCancel = Order::orderBy('created_at', 'DESC')->where('status', 4)->count();
        }

//        $corporates = Corporate::count();
//        $recentCorporates = Corporate::orderBy('created_at', 'DESC')->take(5)->get();

        $contacts = Contact::count();
        $admins = User::where('role_id', '1')->count();
        $agents = Agent::count();
        $trucks = Truck::count();
        $not_actived_drivers = Driver::where('is_active', 0)->count();
        $actived_drivers = Driver::where('is_active', 1)->count();
        $busy = Driver::where('status', 2)->count();
        $onlines = Driver::where('status', 1)->count();
        $offlines = Driver::where('status', 0)->count();



        return view('backend.dashboard', compact('actived_drivers', 'busy',
            'onlines', 'offlines', 'orders', 'users', 'contacts', 'admins', 'agents', 'trucks', 'drivers', 'corporates',
            'not_actived_drivers', 'orderComplete', 'orderPending', 'neworders', 'orderRunning', 'recentCorporates', 'recentCustomers', 'orderCancel', 'orderAccepted'));
    }

    public function search_user($key, $type)
    {
        $users = User::whereHas('roles', function ($q) use ($type) {
            $q->where('name', $type);
        })->where(function ($query) use ($key) {
            $query->where('mobile', 'LIKE', $key . "%");
            $query->orWhere('name', 'LIKE', $key . "%");
            $query->orWhere('email', 'LIKE', $key . "%");
            $query->orWhere('id', 'LIKE', $key . "%");
        })->get();
        return $users;
    }
}

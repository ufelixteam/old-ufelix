<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        @if($pickup)
            <form method="post" id="myform" action="{{ url('/mngrAdmin/forward_order') }}">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <div class="table-responsive">
                    <table id="theTable" class="table table-condensed table-striped text-center"
                           style="min-height: 200px;">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__("backend.id")}}</th>
                            <th>{{__("backend.Task_Number")}}</th>
                            <th>{{__('backend.collection')}}</th>
                            <th>{{__('backend.corporate')}}</th>
                            <th>{{__('backend.Driver')}}</th>
                            <th>{{__('backend.task_type')}}</th>
                            <th>{{__('backend.status')}}</th>

                            <th colspan="2">
                                <div>
                                    <a class="action_btn btn btn-warning" id="toggle"
                                       data-state="1"> {{__('backend.select')}}</a>
{{--                                    <a class="action_btn btn btn-danger disabled" id="dropped_orders">--}}
{{--                                        <i class="fa fa-inbox"></i> {{__('backend.drop')}}--}}
{{--                                    </a>--}}
                                </div>
                            </th>

                        </tr>
                        </thead>
                        <tbody id="tbody">

                        @include('backend.scan.pickup-scan-tr')

                        </tbody>
                    </table>
                </div>
            </form>
            {{--            {!! $pickups->appends($_GET)->links() !!}--}}
        @else
            <h3 class="text-center alert alert-warning">No Result Found !</h3>
        @endif
    </div>
</div>
<div id="js-form" class="js-form">
</div>

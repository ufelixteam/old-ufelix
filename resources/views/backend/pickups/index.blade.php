@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.task_orders')}}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <link href="{{asset('assets/select2/select2.min.css')}}" rel="stylesheet">
    <style>
        .select2-container {
            width: 100% !important;
            display: block;
        }

        #validateForm .form-group {
            min-height: 84px;
        }

        .form-check-inline {
            display: -webkit-inline-box;
            display: -ms-inline-flexbox;
            display: inline-flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            padding-left: 0;
            margin-right: .75rem;
        }

        .form-check-inline input {
            margin: 5px;
        }
    </style>
@endsection
@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.task_orders')}}
        </h3>
        @if(permission('addpickups'))
            {{--            <a class="btn btn-success pull-right" href="{{ url('mngrAdmin/pickups/create') }}"><i--}}
            {{--                    class="glyphicon glyphicon-plus"></i> {{__('backend.create_task_orders')}}</a>--}}
        @endif
    </div>
@endsection

@section('content')

    <div class="clearfix" style="clear:both"></div>
    <form action="{{ route('mngrAdmin.pickups.create_fake') }}" method="POST" name="validateForm" id="validateForm"
          style="border: 2px solid #eee;padding: 10px;margin: 10px 0;">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <input type="hidden" id="sender_name" name="sender_name"/>
        <input type="hidden" id="sender_mobile-field" name="sender_mobile"/>
        <input type="hidden" id="s_government_id" name="s_government_id"/>
        <input type="hidden" id="s_state_id" name="s_state_id"/>
        <input type="hidden" id="sender_latitude" name="sender_latitude"/>
        <input type="hidden" id="sender_longitude" name="sender_longitude"/>
        <input type="hidden" id="sender_address" name="sender_address"/>

        <h3 style="margin-top: 0">{{__('backend.create_task_orders')}}:</h3>
        <div class="row" style="margin-bottom:15px;">

            <div class="form-group col-md-3 col-sm-3 @if($errors->has('corporate_id')) has-error @endif">
                <label for="corporate_id">{{__('backend.corporate')}}</label>
                <select class="form-control select2" id="corporate_id" name="corporate_id">
                    <option value="" data-display="Select">{{__('backend.corporate')}}</option>
                    @if(! empty($corporates))
                        @foreach($corporates as $corporate)
                            <option value="{{$corporate->id}}">
                                {{$corporate->name}}
                            </option>
                        @endforeach
                    @endif
                </select>
                @if($errors->has("corporate_id"))
                    <span class="help-block">{{ $errors->first("corporate_id") }}</span>
                @endif
            </div>
            <div class="form-group col-md-3 col-sm-3 @if($errors->has('customer_id')) has-error @endif">
                <label for="user_id">{{__('backend.customer')}}</label>
                <select class="form-control select2 disabled" id="customer_id" name="customer_id">
                    <option value="" data-dispaly="Select">{{__('backend.customer')}}</option>
                    @if(! empty($customers))
                        @foreach($customers as $customer)
                            <option value="{{$customer->id}}">{{$customer->name}}</option>
                        @endforeach
                    @endif
                </select>
                @if($errors->has("customer_id"))
                    <span class="help-block">{{ $errors->first("customer_id") }}</span>
                @endif
            </div>

            <div class="form-group col-md-3 col-sm-3">
                <label class="control-label" for="driver_id">{{__('backend.driver')}}</label>
                <select class="form-control select2 driver_id" id="pickup_driver_id" name="driver_id">
                    <option value="">Choose Drivers</option>
                    @if(! empty($drivers))
                        @foreach($drivers as $driver)
                            <option
                                value="{{$driver->id}}">
                                {{$driver->name}}
                            </option>
                        @endforeach
                    @endif
                </select>
            </div>

            <div class="form-group col-md-3 col-sm-3 @if($errors->has('delivery_price')) has-error @endif">
                <label for="delivery_price-field">Delivery Price: </label>
                <input type="number" class="form-control" id="delivery_price-field" name="delivery_price"
                       value="{{  old("delivery_price") }}">
                @if($errors->has("delivery_price"))
                    <span class="help-block">{{ $errors->first("delivery_price") }}</span>
                @endif
            </div>

            <div class="form-group col-md-3 col-sm-3 @if($errors->has('bonus_per_order')) has-error @endif">
                <label for="bonous-field">{{__('backend.the_bonous')}}: </label>
                <input type="number" class="form-control" id="bonous-field" name="bonus_per_order"
                       value="{{old("bonus_per_order")}}">
                @if($errors->has("bonus_per_order"))
                    <span class="help-block">{{ $errors->first("bonus_per_order") }}</span>
                @endif
            </div>
            <div class="form-group col-md-3 col-sm-3">
                <label class="control-label"
                       for="captain_received_date"> {{__('backend.captain_received_date')}}:</label>

                <div class='input-group date' id='datepickerFilter'>
                    <input type="text" id="datepicker" name="captain_received_date" class="form-control"
                           value=""/>
                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                </div>
            </div>

            <div class="form-group col-md-4 col-sm-4">
                <label class="control-label"
                       for="captain_received_date"> {{__('backend.task_type')}}:</label>

                <div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="1" name="is_pickup">
                        <label class="form-check-label" for="inlineCheckbox1">{{__('backend.pickup')}}</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="1"
                               name="is_cash_collect">
                        <label class="form-check-label" for="inlineCheckbox2">{{__('backend.cache_collect')}}</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="1"
                               name="is_recall_orders">
                        <label class="form-check-label" for="inlineCheckbox3">{{__('backend.recall_orders')}}</label>
                    </div>
                </div>
            </div>

            <div class="form-group col-md-6 col-sm-6">
                <label class="control-label"
                       for="captain_received_date"> {{__('backend.notes')}}:</label>
                <textarea class="form-control" rows="3" style="resize: vertical;"
                          name="notes">{{  old("notes") }}</textarea>
            </div>

            <div class="form-group col-md-2 col-sm-2">
                <button type="submit" class="btn btn-success pull-right" style="margin-top: 25px;"><i
                        class="glyphicon glyphicon-plus"></i> {{__('backend.save')}} </button>
            </div>
        </div>
    </form>

    <div class="row" style="margin-bottom:15px;">
        <div class="form-group col-md-3 col-sm-3">
            <div id="imaginary_container">
                <div class="input-group stylish-input-group">
                    <input type="text" class="form-control" id="search-field" name="search"
                           value="{{old('search')}}"
                           placeholder="{{__('backend.search')}} ">
                    <span class="input-group-addon">
                      <button type="button" id="search-btn1">
                        <span class="glyphicon glyphicon-search"></span>
                      </button>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group col-md-2 col-sm-2">
            <select id="status" name="status" class="form-control">
                <option value="">{{__('backend.status')}}</option>
                <option value="0"
                        @if(app('request')->status == '0') selected @endif>{{__('backend.created')}}</option>
                <option value="1"
                        @if(app('request')->status == 1) selected @endif>{{__('backend.accepted')}}</option>
                <option value="2"
                        @if(app('request')->status == 2) selected @endif>{{__('backend.collected')}}</option>
                <option value="3"
                        @if(app('request')->status == 3) selected @endif>{{__('backend.dropped')}}</option>
                <option value="3"
                        @if(app('request')->status == 4) selected @endif>{{__('backend.cancelled')}}</option>
            </select>
        </div>
        <div class="form-group col-md-2 col-sm-2">
            <select id="driver_id" name="driver_id" class="form-control">
                <option value="">{{__('backend.captain')}}</option>
                @foreach($drivers as $driver)
                    <option value="{{$driver->id}}"
                            @if(app('request')->driver_id == $driver->id) sekected @endif>{{$driver->name}}</option>
                @endforeach

            </select>
        </div>
        <div class="form-group col-md-2 col-sm-2">
            <select id="filter_date" name="filter_date" class="form-control">
                <option value="">{{__('backend.date')}}</option>
                <option value="created_at"
                        @if(app('request')->filter_date == 'created_at') selected @endif>{{__('backend.created_date')}}</option>
                <option value="accepted_at"
                        @if(app('request')->filter_date == 'accepted_at') selected @endif>{{__('backend.accepted_date')}}</option>
                <option value="collected_at"
                        @if(app('request')->filter_date == 'collected_at') selected @endif>{{__('backend.collected_date')}}</option>
                <option value="dropped_at"
                        @if(app('request')->filter_date == 'dropped_at') selected @endif>{{__('backend.dropped_date')}}</option>
                <option value="cancelled_at"
                        @if(app('request')->filter_date == 'cancelled_at') selected @endif>{{__('backend.cancelled_date')}}</option>
            </select>
        </div>
        <div class="form-group col-md-2 col-sm-2">
            <div class='input-group date' id='datepickerFilter'>
                <input type="text" id="date" name="date" class="form-control" value="{{old('date')}}"/>
                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            </div>
        </div>
    </div>

    <div class="list">
        @include('backend.pickups.table')
    </div>
    @include('backend.pickups.driver_forward_modal')

    <div class="modal" tabindex="-1" role="dialog" id="dropModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('backend.drop_off')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-12 col-xs-12 col-sm-12">
                            <label class="control-label"
                                   for="reason"> {{__('backend.comment')}}:</label>
                            <textarea rows="4" id="drop_comment" name="drop_comment" style="resize: vertical"
                                      class="form-control"></textarea>
                        </div>
                        <div class="form-group col-md-12 col-xs-12 col-sm-12">
                            <label class="control-label"
                                   for="captain_received_date"> {{__('backend.task_type')}}:</label>

                            <div class="form-group">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" id="confirm_pickup" value="1"
                                           name="confirm_pickup">
                                    <label class="form-check-label"
                                           for="confirm_pickup">{{__('backend.pickup')}}</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-control" type="input" value="" name="pickup_orders"
                                           id="pickup_orders"
                                           placeholder="{{__('backend.orders')}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" id="confirm_cash_collect" value="1"
                                           name="confirm_cash_collect">
                                    <label class="form-check-label"
                                           for="confirm_cash_collect">{{__('backend.cache_collect')}}</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-control" type="input" value="" name="cash_money" id="cash_money"
                                           placeholder="{{__('backend.money')}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" id="confirm_recall_orders" value="1"
                                           name="confirm_recall_orders">
                                    <label class="form-check-label"
                                           for="confirm_recall_orders">{{__('backend.recall_orders')}}</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-control" type="input" value="" name="recall_orders"
                                           id="recall_orders"
                                           placeholder="{{__('backend.orders')}}">
                                </div>
                            </div>
                            <label class="control-label"
                                   for="captain_received_date"> {{__('backend.refund_collections')}}:</label>
                            <button type="button" class="btn btn-success" id="add_refund_collection">
                                {{__('backend.add')}}
                            </button>
                            <div class="form-group" id="refunWrapper">
                                <div class="form-check form-check-inline">
                                    <input class="form-control refund_collections" type="text" value="" name="refund_collections[]">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-success" id="drop_button">
                        {{__('backend.save')}}
                    </button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script src="{{asset('assets/select2/select2.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#add_refund_collection").click(function () {
                $("#refunWrapper").append(
                    `<div class="form-check form-check-inline">
                                    <input class="form-control refund_collections" type="text" value="" name="refund_collections[]">
                                </div>`
                );
            });

            $("#customer_id, #datepicker").change(function () {
                if ($("#customer_id").val()) {
                    let data = {
                        customer_id: $("#customer_id").val(),
                        date: $("#datepicker").val(),
                        _token: "{{csrf_token()}}",
                    };

                    $.ajax({
                        url: "{{ url('/mngrAdmin/pickups/customer')}}",
                        type: 'POST',
                        data: data,
                        success: function (data) {
                            // if (data.can_collect_cash) {
                            //     $("#inlineCheckbox2").prop('checked', true);
                            // }else{
                            //     $("#inlineCheckbox2").prop('checked', false);
                            // }

                            if (data.can_recall) {
                                $("#inlineCheckbox3").prop('checked', true);
                            }else{
                                $("#inlineCheckbox3").prop('checked', false);
                            }

                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }
            });

            $('.select2').select2();

            $("form[name='validateForm']").validate({
                // Specify validation rules
                rules: {
                    corporate_id: {
                        required: true,
                    },
                    customer_id: {
                        required: true,
                    },
                    captain_received_date: {
                        required: true,
                    }
                },
                // Specify validation error messages
                messages: {
                    corporate_id: {
                        required: "{{__('backend.Please_Choose_Corporate')}}",
                    },
                    customer_id: {
                        required: "{{__('backend.Please_Choose_Customer')}}",
                    },
                    file: {
                        required: "{{__('backend.Please_Enter_Collect_Date')}}",
                    }
                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                },
                highlight: function (element) {
                    $(element).parent().addClass('error')
                },
                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                }
            });

            var drivers_bonus = {};
            var drivers_fees = {};
            @foreach($drivers as $driver)
                drivers_bonus[{{$driver->id}}] = {{$driver->bouns_of_pickup_for_one_order ? $driver->bouns_of_pickup_for_one_order : 0}};
            drivers_fees[{{$driver->id}}] = {{$driver->pickup_price ? $driver->pickup_price : 0}};
            @endforeach

            $("#pickup_driver_id").on('change', function (e) {
                const bonus = $(this).val() ? drivers_bonus[$(this).val()] : 0;
                const fees = $(this).val() ? drivers_fees[$(this).val()] : 0;
                $("#bonous-field").val(bonus);
                $("#delivery_price-field").val(fees);
            });

            $('#corporate_id').on('change', function () {
                $.ajax({
                    url: "{{ url('/mngrAdmin/get_corporate_customers/')}}" + "/" + $(this).val(),
                    type: 'get',
                    success: function (data) {
                        console.log('dataaaa', data);
                        $('#customer_id').children('option:not(:first)').remove();
                        $('#customer_id').removeClass('disabled');

                        $.each(data, function (i, content) {
                            $('#customer_id').append($("<option></option>").attr("value", content.id).text(content.name));
                        });

                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            });

            $('#customer_id').on('change', function () {
                $.ajax({
                    url: "{{ url('/mngrAdmin/customers/details/')}}" + "/" + $(this).val(),
                    type: 'get',
                    data: {},
                    success: function (data) {
                        $('#sender_name').val(data.name);
                        $('#sender_mobile-field').val(data.mobile);
                        $('#s_government_id').val(data.government_id);
                        $('#s_state_id').val(data.government_id);
                        $('#sender_address').val(data.address);
                        $('#sender_latitude').val(data.latitude);
                        $('#sender_longitude').val(data.longitude);
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            });
        });

        $('body').on('click', '#dropAll', function (e) {
            $('input:checkbox.selectpack').not("[disabled]").not(this).prop('checked', this.checked);
        });

        $('body').on('click', '#printAll', function (e) {
            $('input:checkbox.printpack').not("[disabled]").not(this).prop('checked', this.checked);
        });

        $('body').on('click', '#forwardAll', function (e) {
            $('input:checkbox.forwardpack').not("[disabled]").not(this).prop('checked', this.checked);
        });

        $('body').on('click', '#forward', function (e) {

            let ids = [];

            $("input:checked.forwardpack ").each(function () {
                ids.push($(this).val());
            });
            if (ids.length > 0) {
                $("#forward_ids").val(ids);
                $("#exampleModal").modal("show");
            } else {

                $.confirm({
                    title: '',
                    theme: 'modern',
                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Print PDF</span></div><div style="font-weight:bold;"><p>{{__("backend.Must_Choose_Pickups_To_Print")}}</p></div>',
                    type: 'red',
                });
            }

        });

        $('body').on('click', '#pdf-submit', function (e) {

            let ids = [];

            $("input:checked.printpack ").each(function () {
                ids.push($(this).val());
            });
            if (ids.length > 0) {
                $('<form action="{{ url("/mngrAdmin/pickups/print")}}" method="post" ><input value="' + ids + ' " name="ids"  />" {{csrf_field()}}"</form>').appendTo('body').submit();

            } else {

                $.confirm({
                    title: '',
                    theme: 'modern',
                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Print PDF</span></div><div style="font-weight:bold;"><p>{{__("backend.Must_Choose_Pickups_To_Print")}}</p></div>',
                    type: 'red',
                });
            }

        });

        $("#agent_id-field").on('change', function () {
            $.ajax({
                url: "{{ url('/mngrAdmin/getdriver_by_agents')}}" + "/" + $(this).val(),
                type: 'get',
                data: {},
                success: function (data) {
                    $('#driver_id').html('<option value="Null" >Choose Drivers</option>');
                    $.each(data, function (i, content) {
                        $('#driver_id').append($("<option></option>").attr("value", content.id).text(content.name));
                    });
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        function filter_pickups() {
            $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
            $.ajax({
                url: '{{URL::asset("/mngrAdmin/pickups?")}}' + 'status=' + $("#status").val() + '&driver_id=' + $("#driver_id").val() + '&date=' + $("#date").val() + '&search=' + $("#search-field").val() + '&filter_date=' + $("#filter_date").val(),
                type: 'get',
                success: function (data) {
                    $('.list').html(data.view);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }

        $("#search-btn1").on('click', function () {
            filter_pickups();
        });

        $("#status, #date, #driver_id, #filter_date").on('change', function () {
            filter_pickups();
        });

        $('body').on('click', '#drop_button', function (e) {
            let ids = [];
            let refund_collections = [];
            $(".selectpack:checked").each(function () {
                ids.push($(this).val());
            });

            $(".refund_collections").each(function () {
                refund_collections.push($(this).val());
            });

            let data = {
                _token: "{{csrf_token()}}",
                ids: ids,
                refund_collections: refund_collections,
                drop_comment: $("#drop_comment").val(),
                confirm_pickup: $("#confirm_pickup").is(":checked"),
                confirm_cash_collect: $("#confirm_cash_collect").is(":checked"),
                confirm_recall_orders: $("#confirm_recall_orders").is(":checked"),
                pickup_orders: $("#pickup_orders").val(),
                cash_money: $("#cash_money").val(),
                recall_orders: $("#recall_orders").val()
            };

            $("#dropModal").modal("hide");
            $.ajax({
                url: "{{ url('/mngrAdmin/dropoff_pickups/')}}",
                type: 'POST',
                data: data,
                success: function (data) {
                    location.reload();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $('body').on('click', '#drop_off', function (e) {

            let ids = [];
            $(".selectpack:checked").each(function () {
                ids.push($(this).val());
            });

            if (ids.length == 0) {
                return false;
            }

            let collection_id = $('.selectpackup  input:checked').attr('data-value');

            let order_ids = $('.selectpackup  input:checked').val();

            $.confirm({
                title: '',
                theme: 'modern',
                class: 'warning',
                content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-simle-o  text-warning"></i></span><span class="jconfirm-title"> {{__("backend.confirm_drop_off")}}</span></div><div style="font-weight:bold;"><p>{{__("backend.drop_off_pickups")}}</p></div>',
                buttons: {
                    confirm: {
                        text: '{{__("backend.drop_off_text_pickups")}}',
                        btnClass: 'btn-warning',
                        action: function () {
                            $("#dropModal").modal("show");
                        }
                    },
                    cancel: {
                        text: 'Cancel',
                        action: function () {
                        }
                    }
                }
            });
        });
    </script>
@endsection

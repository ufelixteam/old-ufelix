@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.Show_Role')}} - {{$roleAdmin->name}}</title>
@endsection

@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i> {{__('backend.Show_Role')}} / {{$roleAdmin->name}}</h3>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">

            <h2><i class="fa fa-check-square"></i> {{__('backend.permission')}} - {{$roleAdmin->name}} </h2>
            <br><br>
            <div class="form-group">
                @foreach($permissionType as $type)
                    <div class="col-md-3">
                        <h4><b> {{$type->name}} </b></h4>
                        <div class="box box-primary box-body" style="min-height: 320px;">
                        <ul style="list-style: none; padding: 10px 5px;">
                            @foreach($permissions as $permission)
                                @if($permission->type_permission_id == $type->id)
                                    <li>
                                        <input type="checkbox" disabled="disabled" class="btn-lg" name="permation[]" value="{{$permission->id}}"
                                        @foreach($permission_admin as $per_adm) @if($per_adm->permission_id == $permission->id) checked @endif @endforeach>
                                        <label>{{$permission->name}}</label>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="well well-sm col-md-12">
                <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.admin_roles.index') }}">
                  <i class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}
                </a>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(function() {
    $("form[name='validateForm']").validate({

      // Specify validation rules
      rules: {
        name: {
          required: true,
        },
        permation: {
          required: true,
        },
      },

      // Specify validation error messages
      messages: {
        name: {
          required: "{{__('backend.Please_Enter_Role_Name')}}",
        },
        permation: {
          required: "{{__('backend.Please_Selected_Permission_Administrator')}}",
        },
      },

      errorPlacement: function(error, element) {
          $(element).parents('.form-group').append(error)
      },

      highlight: function (element) {
          $(element).parent().addClass('error')
      },

      unhighlight: function (element) {
          $(element).parent().removeClass('error')
      },
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function(form) {
          form.submit();
      },
    });
  })
</script>
@endsection

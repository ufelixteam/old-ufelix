<?php

namespace App\Models;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Role_admin;

class User extends Authenticatable
{
    use Notifiable,EntrustUserTrait;


    protected $fillable = [
        'name', 'email', 'password'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getIsWarehouseUserAttribute()
    {
        if($this->roleAdmin->is_warehouse_role){
            return true;
        }

        return false;
    }

	public function getBlockSpanAttribute($value)
  {
      /*
      * is_active = 0 :: Not Active
      * is_active = 1 :: Active
      */
      if($this->is_active != 1){
          $value="<span class='badge badge-pill label-danger'>".__('backend.blocked')."</span>";
      }else{
          $value="<span class='badge badge-pill label-success'>".__('backend.not_blocked')."</span>";
      }
      return $value;
  }

  public function getBlocksSpanAttribute($value)
    {
        /*
        * is_active = 0 :: Not Active
        * is_active = 1 :: Active
        */
        if($this->is_active != 1){
            $value="<span class='badge badge-pill label-danger'><i class='fa fa-times' aria-hidden='true'></i></span>";
        }else{
            $value="<span class='badge badge-pill label-success'><i class='fa fa-check' aria-hidden='true'></i></span>";
        }
        return $value;
    }

	 public function getBlockTxtAttribute($value)
    {
        /*
        * is_active = 0 :: Not Active
        * is_active = 1 :: Active
        */
        if($this->is_active != 1){
            $value = __('backend.not_blocked');
        }else{
            $value = __('backend.blocked');
        }
        return $value;
    }

    public function getImageAttribute($value)
    {
        if($value != ""){
            $value=url("/")."/public/backend/images/".$value;
        }else{
          $value = "";
        }
        return $value;
    }

    public function agent() {
        return $this->belongsTo('App\Models\Agent','agent_id');
    }
    public function roleAdmin(){
        return $this->belongsTo('App\Models\Role_admin', 'role_admin_id');
    }

}

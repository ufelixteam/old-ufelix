<!-- Start Nav -->
<div class="vertical-nav">
	<button class="collapse-menu">
    	<i class="icon-menu2"></i>
    </button>
    <div class="user-details clearfix">
    	<a href="" class="user-img">
        	<img src="<?php echo e(Auth::guard('Customer')->user() ?  Auth::guard('Customer')->user()->image : ''); ?>" alt="User Info"> 
            <span class="likes-info"><?php echo e(Auth::guard('Customer')->user() ?  Auth::guard('Customer')->user()->rate : ''); ?></span>
        </a>
        <h5 class="user-name"><?php echo e(Auth::guard('Customer')->user() ?  Auth::guard('Customer')->user()->name : ''); ?></h5>
     </div>
     <ul class="menu clearfix">
        <li class="active selected">
            <a href="<?php echo e(app()->getLocale() == 'en' ? url('/en/customer/account') : url('/customer/account')); ?>">
                <i class="icon-user"></i> 
                <span class="menu-item"><?php echo app('translator')->getFromJson('website.Profile'); ?></span>
            </a>
        </li>
        <li>
           <a href="">
               <i class="icon-align-justify"></i> 
               <span class="menu-item"><?php echo app('translator')->getFromJson('website.orders'); ?></span> 
               <span class="down-arrow"></span>
           </a>
           <ul>
              <li><a href="<?php echo e(app()->getLocale() == 'en' ? url('/en/customer/account/orders?type=pending') : url('/customer/account/orders?type=pending')); ?>"><?php echo app('translator')->getFromJson('website.pendingorders'); ?></a></li>
              <li><a href="<?php echo e(app()->getLocale() == 'en' ? url('/en/customer/account/orders?type=accept') : url('/customer/account/orders?type=accept')); ?>"><?php echo app('translator')->getFromJson('website.acceptorders'); ?></a></li>
              <li><a href="<?php echo e(app()->getLocale() == 'en' ? url('/en/customer/account/orders?type=receive') : url('/customer/account/orders?type=receive')); ?>"><?php echo app('translator')->getFromJson('website.receiveorders'); ?></a></li>
              <li><a href="<?php echo e(app()->getLocale() == 'en' ? url('/en/customer/account/orders?type=deliver') : url('/customer/account/orders?type=deliver')); ?>"><?php echo app('translator')->getFromJson('website.finishorders'); ?></a></li>
           </ul>
        </li>
        <li>
            <a href="<?php echo e(app()->getLocale() == 'en' ? url('/en/customer/account/invoices') : url('/customer/account/invoices')); ?>">
                <i class="icon-envelope"></i> 
                <span class="menu-item"> <?php echo app('translator')->getFromJson('website.invoices'); ?></span>
            </a>
        </li>
        <li>
            <a href="<?php echo e(app()->getLocale() == 'en' ? url('/en/customer/account/wallets') : url('/customer/account/wallets')); ?>">
                <i class="icon-wallet"></i> 
                <span class="menu-item"> <?php echo app('translator')->getFromJson('website.wallets'); ?></span>
            </a>
        </li>
        <li>
           	<a href="<?php echo e(app()->getLocale() == 'en' ? url('/en/customer/account/upload-excel') : url('/customer/account/upload-excel')); ?>">
                <i class="icon-upload"></i> 
                <span class="menu-item"><?php echo app('translator')->getFromJson('website.UploadExcel'); ?></span> 
            </a>
        </li>
        <li>
           <a href="<?php echo e(app()->getLocale() == 'en' ? url('/en/customer/account/form-collections') : url('/customer/account/form-collections')); ?>">
               <i class="icon-file-text2"></i> 
               <span class="menu-item">  <?php echo app('translator')->getFromJson('website.FormCollection'); ?></span> 
           </a>
        </li>
	</ul>
</div>
<!-- End Nav --><?php /**PATH /home/www/ufelix/ufelix/resources/views/frontend/profile/layouts/sidemenu.blade.php ENDPATH**/ ?>
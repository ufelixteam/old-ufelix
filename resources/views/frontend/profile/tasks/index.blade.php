@extends('frontend.profile.layouts.app')

@section('title') @lang('website.Orders') @endsection
@section('css')
    <title>{{__('backend.task_orders')}}</title>
@endsection
@section('content')
    <div class="container-fluid {{ app()->getLocale() == 'en' ? 'text-left' : 'text-right' }}">
        @include('flash::message')
    </div>

    <div class="row">
        <div class="col-12">
            <div class="">
                <div class="table-responsive m-t-40">
                    <h1 class="title" style="margin-bottom: 10px">
                        @lang('backend.task_orders')
                        <div class="button-box">
                            <a href="{{ app()->getLocale() == 'en' ? url('/en/customer/account/tasks/create') : url('/customer/account/tasks/create') }}"
                               class="btn btn-primary" style="">
                                @lang('backend.add_task_order')</a>
                        </div>
                    </h1>
                    <form
                        action="{{ app()->getLocale() == 'en' ? url('/en/customer/account/tasks') : url('/customer/account/tasks') }}"
                        method="get">
                        <input type="hidden" name="type" value="{{ app('request')->input('type')}}">
                        <div class="row" style="margin-bottom:15px;">
                            <div class="col-md-12" style="padding: 0">
                                <div class="group-control col-md-3 col-sm-3">
                                    <input type="text" class="form-control" id="search-field" name="search"
                                           value="{{ app('request')->input('search')}}"
                                           placeholder="{{__('backend.search')}} ">
                                </div>

                                <div class="group-control col-md-2 col-sm-2">
                                    <button type="submit" style="padding: 8px;"
                                            class="btn btn-primary btn-block"> {{__('backend.filter')}} </button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered"
                           cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('backend.task_No')}}</th>
                            <th>{{__('backend.customer')}}</th>
                            <th>{{__('backend.notes')}}</th>
                            <th style="width: 15%;">{{__('backend.address')}}</th>
                            <th style="width: 15%;">{{__('backend.task_type')}}</th>
                            <th>#{{__('backend.orders')}}</th>
                            <th>{{__('backend.captain_received_date')}}</th>
                            <th>{{__('backend.status')}}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($collection_orders as $collection)
                            <tr class='clickable-row' data-href="{{ route('mngrAdmin.pickups.show', $collection->id) }}"
                                style="text-align: center">
                                <td>{{$collection->id}}</td>
                                <td>{{$collection->pickup_number}}</td>
                                <td>{{! empty($collection->customer) ? $collection->customer->name : ''}}</td>
                                <td>{{$collection->notes}}</td>
                                <td>{{$collection->sender_address}}</td>
                                <td>{!! $collection->task_types !!}</td>
                                <td>{{$collection->is_fake_pickup ?  $collection->orders_count : $collection->total_order_no}}</td>
                                <td>{{$collection->captain_received_time ? date("Y-m-d", strtotime($collection->captain_received_time)) : ''}}</td>

                                <td>{!! $collection->status_span !!}</td>
                                <td>
                                    @if(!$collection->driver_id && $collection->status != 4)
                                        <a href="{{ app()->getLocale() == 'en' ? url('/en/customer/account/tasks/cancel_pickup/'.$collection->id) : url('/customer/account/tasks/cancel_pickup/'.$collection->id) }}"
                                           style="display: inline-block"
                                           class="btn btn-warning btn-pdf btn-xs ">{{__('backend.Cancel')}}
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @if (! empty($collection_orders) )
                        {!! $collection_orders->appends($_GET)->links() !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

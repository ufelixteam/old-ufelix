

<?php $__env->startSection('title'); ?> <?php echo app('translator')->getFromJson('website.Orders'); ?> <?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-12">
        <div class="">
            <div class="table-responsive m-t-40">
            	<h1 class="title">Pending Orders</h1>
                <table id="example23" class="display nowrap table table-hover table-striped table-bordered" 
                cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?php echo app('translator')->getFromJson('website.ID'); ?></th>
                            <th><?php echo app('translator')->getFromJson('website.SenderName'); ?></th>
                            <th><?php echo app('translator')->getFromJson('website.ReceiverName'); ?></th>
                            <th><?php echo app('translator')->getFromJson('website.Status'); ?></th>
                            <th><?php echo app('translator')->getFromJson('website.OrderNumber'); ?></th>
                            

                            <th><?php echo app('translator')->getFromJson('website.Delivery'); ?></th>
                            <th><?php echo app('translator')->getFromJson('website.Actions'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                    	<?php if(! empty($orders) ): ?>
                    		<?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<th><?php echo e($order->order_no); ?></th>
								<th><?php echo e($order->sender_name); ?></th>
								<th><?php echo e($order->receiver_name); ?></th>
								<td>
							      <?php echo $order->status_span; ?>

							    </td>
								<th><?php echo e($order->order_number); ?></th>
								<th><?php echo e($order->delivery_price); ?></th>
								<th>
									<a href="<?php echo e(app()->getLocale() == 'en'  ? route('profile.orders', ['locale' =>'en','id' =>$order->order_no] )  : url('/customer/account/orders?id='.$order->order_no )); ?>" class="text-inverse p-r-10" data-toggle="tooltip" title="" data-original-title="show" aria-describedby="tooltip468946">
										<i class="icon-eye"></i>
									</a>
									<?php if( ! ($type == 'pending' || $type=='cancel' )): ?>
					                     <a class="text-inverse p-r-10 printdata"  data-id="<?php echo e($order->id); ?>"><i class="icon-print"></i></a>
					              
					                <?php endif; ?>
								</th>
								</tr>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


					    <?php endif; ?>

						
					</tbody>
                </table>
                <?php if(! empty($orders) ): ?>
                    <?php echo $orders->appends($_GET)->links(); ?>

                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.profile.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/www/ufelix/ufelix/resources/views/frontend/profile/orders.blade.php ENDPATH**/ ?>
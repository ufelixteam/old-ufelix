<form action="{{ route('mngrAdmin.orders.add_order_comment', @$order->id) }}" method="POST" id="comments_form">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="order_id" value="{{ @$order->id }}">
    <input type="hidden" class="latitude" name="latitude" value="0" id="receiver_latitude">
    <input type="hidden" class="longitude" name="longitude" value="0" id="receiver_longitude">
    <div class="modal" tabindex="-1" role="dialog" id="commentModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-header">
                        <h5 class="modal-title">{{__('backend.add_order_comment')}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="form-group col-md-12 col-xs-12 col-sm-12 select_driver">
                                <label for="order_price-field">{{__('backend.driver')}}: </label>
                                <select class=" form-control driver_id" id="driver_id" name="driver_id">
                                    <option value="">{{__('backend.choose_driver')}}</option>
                                    @if(! empty($online_drivers))
                                        @foreach($online_drivers as $driver)
                                            <option
                                                value="{{$driver->id}}">
                                                {{$driver->name}}
                                            </option>
                                        @endforeach
                                    @endif

                                </select>
                            </div>
                            <div class="form-group col-md-12 col-xs-12 col-sm-12">
                                <label class="delay_at">{{__('backend.date')}}
                                    <div class='input-group date' id='delay_date_group'>
                                        <input type="text" id="created_at" name="created_at" class="form-control"
                                               value=""
                                               autocomplete="off"/>
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </label>
                            </div>

                            <div class="form-group col-md-12 col-xs-12 col-sm-12">
                                <label for="order_price-field">{{__('backend.comment')}} *: </label>
                                <textarea class="form-control" name="comment" id="comment" rows="3"
                                          style="min-height: 100px; resize: horizontal" required></textarea>
                            </div>
                            <div class="col-md-12 col-xs-12 col-sm-12" style="margin-bottom: 20px">
                                <label for="order_price-field">{{__('backend.location')}}: </label>
                                <input type="text" id="receiver_address_autocomplete" class="form-control"/>
                            </div>
                            <div class="col-md-12 col-xs-12 col-sm-12">
                                <div id="map1" style="width:100%;height:400px"></div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">

                        <button type="submit" class="btn btn-secondary" id="save_comment">
                            {{__('backend.save')}}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

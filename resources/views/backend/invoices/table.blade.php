<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        @if($invoices->count())
            <table id="theTable" class="table table-condensed table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ $type}}</th>
                    <th>{{__('backend.start_date')}}</th>
                    <th>{{__('backend.status')}}</th>
                    @if(app('request')->type == 1)
                        <th>{{__('backend.collected')}}</th>
                        <th>{{__('backend.paid_amount')}}</th>
                        <th>{{__('backend.debit_amount')}}</th>
                    @endif
                    <th class="text-right"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($invoices as $invoice)
                    <tr>
                        <td>{{$invoice->id}}</td>
                        <td>@if(! empty( $invoice->owner) ) {{$invoice->owner->name }} @endif</td>
                        <td>{{$invoice->start_date}}</td>
                        <td>{!! $invoice->invoice_type_span !!}</td>
                        @if(app('request')->type == 1)
                            <th>{!! $invoice->is_paid_span !!}</th>
                            <th>{{$invoice->paid_amount}}</th>
                            <th>{{$invoice->debit_amount}}</th>
                        @endif
                        <td class="text-right">
                        @if(permission('showInvoice')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                            <a class="btn btn-xs btn-primary"
                               href="{{ route('mngrAdmin.invoices.show', $invoice->id) }}"><i
                                    class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>
                        @endif
                        <!-- <a class="btn btn-xs btn-warning" href="{{ route('mngrAdmin.invoices.edit', $invoice->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a> -->
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {!! $invoices->appends($_GET)->links() !!}

        @else
            <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
        @endif
    </div>
</div>

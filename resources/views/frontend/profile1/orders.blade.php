@extends('frontend.profile.layouts.app')

@section('title') @lang('website.Orders') @endsection

@section('content')
    <style>
        input[type=checkbox], input[type=radio] {
            width: 18px;
            height: 18px;
        }

        .printdata {
            cursor: pointer;
        }
    </style>
    <div class="row">
        <div class="col-12">
            <div class="">
                <div class="table-responsive m-t-40">
                    <h1 class="title">
                        @if(app('request')->input('type') == 'pending')
                            @lang('website.pending_orders')
                        @elseif(app('request')->input('type') == 'accept')
                            @lang('website.accepted_orders')
                        @elseif(app('request')->input('type') == 'receive')
                            @lang('website.received_orders')
                        @elseif(app('request')->input('type') == 'deliver')
                            @lang('website.delivered_orders')
                        @elseif(app('request')->input('type') == 'cancel')
                            @lang('website.cancelled_orders')
                        @elseif(app('request')->input('type') == 'recall')
                            @lang('website.recalled_orders')
                        @elseif(app('request')->input('type') == 'waiting')
                            @lang('website.waiting_orders')
                        @elseif(app('request')->input('collection'))
                            @lang('website.collection_orders')
                        @endif
                    </h1>
                    <form method="post" id="myform" action="{{ url('/customer/account/print_police') }}">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <table id="example23" class="display nowrap table table-hover table-striped table-bordered"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>@lang('website.ID')</th>
                                <th>@lang('website.collection_number')</th>
                                <th>@lang('website.receiver_name')</th>
                                <th>@lang('website.order_name')</th>
                                <th>@lang('website.from')</th>
                                <th>@lang('website.to')</th>
                                <th>@lang('website.sender_code')</th>
{{--                                <th>@lang('website.receiver_code')</th>--}}
                                @if(!app('request')->input('collection'))
                                    <th>@lang('website.status')</th>
                                @endif
                                <th>
                                    <input type="checkbox" name="checkall" id="checkall" class="md-check">

                                    <a class="text-inverse p-r-10 printdata" id="pdf-print"><i
                                            class="icon-print"></i></a>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (! empty($orders) )
                                @foreach ($orders as $order)
                                    <tr>
                                        <td>{{ $order->order_no }}</td>
                                        <td>{{ $order->collection_id ? $order->collection_id : '-' }}</td>
                                        <td>{{ $order->receiver_name }}</td>
                                        <td>{{ $order->type ? $order->type : '-'}}</td>
                                        <td>
                                            @if(! empty($order->from_government)  && $order->from_government != null )
                                                {{$order->from_government->name_en}}
                                            @endif
                                        </td>

                                        <td>
                                            @if(! empty($order->to_government)  && $order->from_government != null  )
                                                {{$order->to_government->name_en}}
                                            @endif
                                        </td>
                                        <td>{{$order->order_number}}</td>
{{--                                        <td>{{$order->receiver_code}}</td>--}}
                                        @if(!app('request')->input('collection'))
                                            <td>
                                                {!! $order->status_span !!}
                                            </td>
                                        @endif
                                        <td class="selectpdf">
                                            <input type="checkbox" name="ids[]" id="checkbox2_{{$order->id}}"
                                                   value="{{$order->id}}">

                                            <a href="{{ app()->getLocale() == 'en'  ? route('profile.orders', ['locale' =>'en','id' =>$order->id] )  : url('/customer/account/orders?id='.$order->id ) }}"
                                               class="text-inverse p-r-10" data-toggle="tooltip" title=""
                                               data-original-title="show" aria-describedby="tooltip468946">
                                                <i class="icon-eye"></i>
                                            </a>

                                            @if(app('request')->input('type') == 'pending')
                                                <a href="{{ app()->getLocale() == 'en'  ? route('profile.order_edit', ['locale' =>'en','id' =>$order->id] )  : url('/customer/account/orders/'.$order->id.'/edit' ) }}"
                                                   class="text-inverse p-r-10" data-toggle="tooltip" title=""
                                                   data-original-title="show" aria-describedby="tooltip468946">
                                                    <i class="icon-edit"></i>
                                                </a>
                                            @endif

                                            <a class="text-inverse p-r-10 printdata" data-id="{{$order->id}}"
                                               href="{{ url('/customer/account/print_police?ids='.$order->id) }}"><i
                                                    class="icon-print"></i></a>

                                        </td>
                                    </tr>
                                @endforeach
                            @endif

                            </tbody>
                        </table>
                    </form>
                    @if (! empty($orders) )
                        {!! $orders->appends($_GET)->links() !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

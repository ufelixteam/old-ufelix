<?php $__env->startSection('css'); ?>
    <title><?php echo e(__('backend.Order_Collection')); ?></title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('header'); ?>
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> <?php echo e(__('backend.PickUp_Orders')); ?>

        </h3>




    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>



    <div class="row" style="margin-bottom:15px;">
        <div class="col-md-12">

            <div class="group-control col-md-3 col-sm-3">
                <select id="status" name="status" class="form-control">
                    <option value="-1">All</option>
                    <option value="0">Created</option>
                    <option value="1">Collected</option>
                    <option value="3">Droped</option>
                </select>
            </div>
        </div>
    </div>

    <div class="list">
        <?php echo $__env->make('backend.pickups.table', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

    <script type="text/javascript">
        $("#agent_id-field").on('change', function () {
            $.ajax({
                url: "<?php echo e(url('/mngrAdmin/getdriver_by_agents')); ?>" + "/" + $(this).val(),
                type: 'get',
                data: {},
                success: function (data) {
                    $('#driver_id').html('<option value="Null" >Choose Drivers</option>');
                    $.each(data, function (i, content) {
                        $('#driver_id').append($("<option></option>").attr("value", content.id).text(content.name));
                    });
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $("#status").on('change', function () {
            $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
            $.ajax({
                url: '<?php echo e(URL::asset("/mngrAdmin/pickups?")); ?>' + 'status=' + $("#status").val(),
                type: 'get',
                success: function (data) {
                    $('.list').html(data.view);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $("#drop_off").on('click', function () {

            let ids = [];
            $(".selectpack:checked").each(function () {
                ids.push($(this).val());
            });

            if (ids.length == 0) {
                return false;
            }

            let collection_id = $('.selectpackup  input:checked').attr('data-value');

            let order_ids = $('.selectpackup  input:checked').val();

            $.confirm({
                title: '',
                theme: 'modern',
                class: 'warning',
                content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-simle-o  text-warning"></i></span><span class="jconfirm-title"> <?php echo e(__("backend.confirm_drop_off")); ?></span></div><div style="font-weight:bold;"><p><?php echo e(__("backend.drop_off_orders")); ?></p></div>',
                buttons: {
                    confirm: {
                        text: '<?php echo e(__("backend.drop_off_text")); ?>',
                        btnClass: 'btn-warning',
                        action: function () {
                            $.ajax({
                                url: "<?php echo e(url('/mngrAdmin/dropoff_pickups/')); ?>",
                                type: 'POST',
                                data: {'_token': "<?php echo e(csrf_token()); ?>", 'ids': ids},
                                success: function (data) {
                                    location.reload();
                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                            });
                        }
                    },
                    cancel: {
                        text: 'Cancel',
                        action: function () {
                        }
                    }
                }
            });
        })
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/pickups/index.blade.php ENDPATH**/ ?>
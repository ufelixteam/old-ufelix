@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.tickets')}}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <link href="{{asset('assets/select2/select2.min.css')}}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <style>
        .select2-container {
            width: 100% !important;
            display: block;
        }

        .form-check-inline {
            display: -webkit-inline-box;
            display: -ms-inline-flexbox;
            display: inline-flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            padding-left: 0;
            margin-right: .75rem;
        }

        .form-check-inline input {
            margin: 5px;
        }

        .toggle.ios, .toggle-on.ios, .toggle-off.ios {
            border-radius: 20px;
        }

        .toggle.ios .toggle-handle {
            border-radius: 20px;
        }

        .form-height {
            min-height: 85px;
        }
    </style>
@endsection
@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="fa fa-ticket"></i> {{__('backend.tickets')}}

            <a class="btn btn-success pull-right" href="{{ route('mngrAdmin.tickets.close_tickets') }}">
                {{__('backend.close_tickets')}}
            </a>
            <a class="btn btn-danger pull-right" style="margin-right: 20px;"
               href="{{ route('mngrAdmin.tickets.generate_tickets') }}">
                {{__('backend.generate_tickets')}}
            </a>
        </h3>

        @if(permission('addtickets'))
            {{--            <a class="btn btn-success pull-right" href="{{ url('mngrAdmin/tickets/create') }}"><i--}}
            {{--                    class="glyphicon glyphicon-plus"></i> {{__('backend.create_ticket')}}</a>--}}
        @endif
    </div>
@endsection

@section('content')

    <div class="clearfix" style="clear:both"></div>
    <form action="{{ route('mngrAdmin.tickets.store') }}" method="POST" name="validateForm" id="validateForm"
          style="border: 2px solid #eee;padding: 10px;margin: 10px 0;">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <h3 style="margin-top: 0">{{__('backend.create_ticket')}}:</h3>
        <div class="row" style="margin-bottom:15px;">
            <div class="col-md-2 col-sm-2 @if($errors->has('ticket_model')) has-error @endif">
                <div class="form-group form-height">
                    <label for="order_id-field">{{__('backend.choose')}}: </label>
                    <select class="form-control select2" id="ticket_model" name="ticket_model">
                        <option value="1">
                            {{__('backend.order')}}
                        </option>
                        <option value="2">
                            {{__('backend.corporate')}}
                        </option>
                    </select>
                    @if($errors->has("ticket_model"))
                        <span class="help-block">{{ $errors->first("ticket_model") }}</span>
                    @endif
                </div>
            </div>

            <div class="col-md-2 col-sm-2 @if($errors->has('order_id')) has-error @endif" id="order-wrapper">
                <div class="form-group form-height">
                    <label for="order_id-field">{{__('backend.order')}}: </label>
                    <input type="text" class="form-control" id="order_id-field" name="order_id"
                           value="{{  old("order_id") }}">
                    @if($errors->has("order_id"))
                        <span class="help-block">{{ $errors->first("order_id") }}</span>
                    @endif
                </div>
            </div>

            <div class="col-md-2 col-sm-2 @if($errors->has('corporate_id')) has-error @endif" id="corporate-wrapper"
                 style="display: none">
                <div class="form-group form-height">
                    <label for="order_id-field">{{__('backend.corporate')}}: </label>
                    <select id="ticket_corporate_id" name="corporate_id" class="form-control">
                        <option value="">{{__('backend.corporate')}}</option>
                        @foreach($corporates as $corporate)
                            <option value="{{$corporate->id}}"
                                    @if(app('request')->corporate_id == $corporate->id) selected @endif>{{$corporate->name}}</option>
                        @endforeach

                    </select>
                    @if($errors->has("corporate_id"))
                        <span class="help-block">{{ $errors->first("corporate_id") }}</span>
                    @endif
                </div>
            </div>

            <div class="col-md-2 col-sm-2 @if($errors->has('role_id')) has-error @endif">
                <div class="form-group form-height">
                    <label for="order_id-field">{{__('backend.admin_role')}}: </label>
                    <select id="role_id" name="role_id" class="form-control">
                        <option value="">{{__('backend.role')}}</option>
                        @foreach($roles as $role)
                            <option value="{{$role->id}}"
                                    @if(app('request')->role_id == $role->id) selected @endif>{{$role->name}}</option>
                        @endforeach

                    </select>
                    @if($errors->has("role_id"))
                        <span class="help-block">{{ $errors->first("role_id") }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <div class="form-group form-height">
                    <label for="status">{{__('backend.assigned_to')}}</label>
                    <select id="assigned_to_id" name="assigned_to_id" class="form-control">
                        <option value="">{{__('backend.assigned_to')}}</option>
                        @foreach($users as $user)
                            <option value="{{$user->id}}"
                                    @if(app('request')->assigned_to_id == $user->id) selected @endif>{{$user->name}}</option>
                        @endforeach

                    </select>
                </div>
            </div>

            <div class="col-md-3 col-sm-3 @if($errors->has('reference_number')) has-error @endif">
                <div class="form-group form-height">
                    <label for="reference_number-field">{{__('backend.reference_number')}}: </label>
                    <input type="text" class="form-control" id="reference_number-field" name="reference_number"
                           value="{{  old("reference_number") }}">
                    @if($errors->has("reference_number"))
                        <span class="help-block">{{ $errors->first("reference_number") }}</span>
                    @endif
                </div>
            </div>

            <div class="col-md-6 col-sm-6">
                <div class="form-group ">
                    <label class="control-label"
                           for="reason"> {{__('backend.reason')}}:</label>
                    <textarea class="form-control" rows="3" style="resize: vertical;"
                              name="reason">{{  old("reason") }}</textarea>
                </div>

            </div>

            <div class="col-md-2 col-sm-2">
                <div class="form-group">
                    <button type="submit" class="btn btn-success pull-left" style="margin-top: 64px;"><i
                            class="glyphicon glyphicon-plus"></i> {{__('backend.save')}} </button>
                </div>
            </div>

            {{--            <div class="col-md-3 col-sm-3 @if($errors->has('ticket_model')) has-error @endif">--}}
            {{--                <div class="form-group">--}}
            {{--                    <label for="order_id-field">{{__('backend.choose')}}: </label>--}}
            {{--                    <select class="form-control select2" id="ticket_model-field" name="ticket_model">--}}
            {{--                        <option value="1">--}}
            {{--                            {{__('backend.order')}}--}}
            {{--                        </option>--}}
            {{--                        <option value="2">--}}
            {{--                            {{__('backend.corporate')}}--}}
            {{--                        </option>--}}
            {{--                    </select>--}}
            {{--                    @if($errors->has("ticket_model"))--}}
            {{--                        <span class="help-block">{{ $errors->first("ticket_model") }}</span>--}}
            {{--                    @endif--}}
            {{--                </div>--}}

            {{--                <div class="form-group">--}}
            {{--                    <label for="order_id-field">{{__('backend.order')}}: </label>--}}
            {{--                    <input type="text" class="form-control" id="order_id-field" name="order_id"--}}
            {{--                           value="{{  old("order_id") }}">--}}
            {{--                    @if($errors->has("order_id"))--}}
            {{--                        <span class="help-block">{{ $errors->first("order_id") }}</span>--}}
            {{--                    @endif--}}
            {{--                </div>--}}

            {{--                <div class="form-group">--}}
            {{--                    <button type="submit" class="btn btn-success pull-left" style="margin-top: 25px;"><i--}}
            {{--                            class="glyphicon glyphicon-plus"></i> {{__('backend.save')}} </button>--}}
            {{--                </div>--}}
            {{--            </div>--}}

            {{--            <div class="col-md-6 col-sm-6">--}}
            {{--                <div class="form-group">--}}
            {{--                    <label for="status">{{__('backend.assigned_to')}}</label>--}}
            {{--                    <select id="assigned_to_id" name="assigned_to_id" class="form-control">--}}
            {{--                        <option value="">{{__('backend.assigned_to')}}</option>--}}
            {{--                        @foreach($users as $user)--}}
            {{--                            <option value="{{$user->id}}"--}}
            {{--                                    @if(app('request')->assigned_to_id == $user->id) selected @endif>{{$user->name}}</option>--}}
            {{--                        @endforeach--}}

            {{--                    </select>--}}
            {{--                </div>--}}
            {{--                <div class="form-group ">--}}
            {{--                    <label class="control-label"--}}
            {{--                           for="reason"> {{__('backend.reason')}}:</label>--}}
            {{--                    <textarea class="form-control" rows="3" style="resize: vertical;"--}}
            {{--                              name="reason">{{  old("reason") }}</textarea>--}}
            {{--                </div>--}}

            {{--                --}}{{--                <div class="form-group">--}}
            {{--                --}}{{--                    <button type="submit" class="btn btn-success pull-left" style="margin-top: 25px;"><i--}}
            {{--                --}}{{--                            class="glyphicon glyphicon-plus"></i> {{__('backend.save')}} </button>--}}
            {{--                --}}{{--                </div>--}}
            {{--            </div>--}}

            {{--            <div class="form-group col-md-2 col-sm-2">--}}
            {{--                <button type="submit" class="btn btn-success pull-left" style="margin-top: 25px;"><i--}}
            {{--                        class="glyphicon glyphicon-plus"></i> {{__('backend.save')}} </button>--}}
            {{--            </div>--}}
        </div>
    </form>

    <div class="row" style="margin-bottom:15px;">
        <div class="form-group col-md-3 col-sm-3" style="margin-bottom: 10px;">
            <div id="imaginary_container">
                <div class="input-group stylish-input-group">
                    <input type="text" class="form-control" id="search-field" name="search"
                           value="{{old('search')}}"
                           placeholder="{{__('backend.search')}} ">
                    <span class="input-group-addon">
                      <button type="button" id="search-btn1">
                        <span class="glyphicon glyphicon-search"></span>
                      </button>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group col-md-3 col-sm-3">
            <select id="status" name="status" class="form-control">
                <option value="">{{__('backend.status')}}</option>
                <option value="0">
                    {{__('backend.opened')}}
                </option>
                <option value="1}">
                    {{__('backend.closed')}}
                </option>
            </select>
        </div>
        <div class="form-group col-md-3 col-sm-3">
            <select id="created_by_type" name="created_by_type" class="form-control">
                <option value="">{{__('backend.created_by')}}</option>
                <option value="system" @if(app('request')->created_by_type == 'system') selected @endif>{{__('backend.system')}}</option>
                <option value="manual" @if(app('request')->created_by_type == 'manual') selected @endif>{{__('backend.manual')}}</option>
                <option value="client_comment" @if(app('request')->created_by_type == 'client_comment') selected @endif>{{__('backend.client_comment')}}</option>
            </select>
        </div>
        <div class="form-group col-md-3 col-sm-3">
            <select id="user_id" name="user_id" class="form-control">
                <option value="">{{__('backend.created_by_user')}}</option>
                @foreach($users as $user)
                    <option value="{{$user->id}}"
                            @if(app('request')->user_id == $user->id) selected @endif>{{$user->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-3 col-sm-3">
            <select id="assigned_to" name="assigned_to" class="form-control">
                <option value="">{{__('backend.assigned_to')}}</option>
                @foreach($users as $user)
                    <option value="{{$user->id}}"
                            @if(app('request')->assigned_to == $user->id) selected @endif>{{$user->name}}</option>
                @endforeach

            </select>
        </div>
        <div class="form-group col-md-3 col-sm-3">
            <select id="closed_by" name="closed_by" class="form-control">
                <option value="">{{__('backend.closed_by')}}</option>
                @foreach($users as $user)
                    <option value="{{$user->id}}"
                            @if(app('request')->closed_by == $user->id) selected @endif>{{$user->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-3 col-sm-3">
            <select id="corporate_id" name="corporate_id" class="form-control">
                <option value="">{{__('backend.corporate')}}</option>
                @foreach($corporates as $corporate)
                    <option value="{{$corporate->id}}"
                            @if(app('request')->corporate_id == $corporate->id) selected @endif>{{$corporate->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-3 col-sm-3">
            <div class='input-group date' id='datepickerFilter'>
                <input type="text" id="date" name="date" class="form-control" value="{{old('date')}}"/>
                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            </div>
        </div>
    </div>

    <div class="list">
        @include('backend.tickets.table')
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="closeModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('backend.closed_comment')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-12 col-xs-12 col-sm-12">
                            <label class="control-label"
                                   for="reason"> {{__('backend.comment')}}:</label>
                            <textarea rows="4" id="closed_comment" name="closed_comment" style="resize: vertical"
                                      class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-success" id="close_button">
                        {{__('backend.save')}}
                    </button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script src="{{asset('assets/select2/select2.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script>
        $(function () {
            $('.toggle').bootstrapToggle();
        });

        $(document).ready(function () {
            $('.select2').select2();

            $("#role_id").on('change', function () {
                if ($(this).val()) {
                    $.ajax({
                        url: "{{ url('/mngrAdmin/get_role_users')}}" + "/" + $(this).val(),
                        type: 'get',
                        data: {},
                        success: function (data) {
                            $('#assigned_to_id').html('<option value="" >{{__('backend.assigned_to')}}</option>');
                            $.each(data, function (i, content) {
                                $('#assigned_to_id').append($("<option></option>").attr("value", content.id).text(content.name));
                            });
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }else{
                    $('#assigned_to_id').html('<option value="" >{{__('backend.assigned_to')}}</option>');
                }
            });

            $("#ticket_model").on('change', function () {
                if ($(this).val() == 1) {
                    $("#order-wrapper").show();
                    $("#corporate-wrapper").hide();
                    $("#ticket_corporate_id").val('');
                } else {
                    $("#order-wrapper").hide();
                    $("#corporate-wrapper").show();
                    $("#order_id").val('');
                }
            });

            $("form[name='validateForm']").validate({
                // Specify validation rules
                rules: {
                    order_id: {
                        required: function (element) {
                            return $("#ticket_model").val() == 1;
                        }
                    },
                    corporate_id: {
                        required: function (element) {
                            return $("#ticket_model").val() == 2;
                        }
                    },
                    // status: {
                    //     required: true,
                    // },
                    reason: {
                        required: true,
                    }
                },
                // Specify validation error messages
                messages: {
                    order_id: {
                        required: "{{__('backend.Please_Enter_Order')}}",
                    },
                    status: {
                        required: "{{__('backend.Please_Choose_Status')}}",
                    },
                    reason: {
                        required: "{{__('backend.Please_Enter_Reason')}}",
                    }
                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                },
                highlight: function (element) {
                    $(element).parent().addClass('error')
                },
                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                }
            });

            function filter_tickets() {
                $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
                $.ajax({
                    url: '{{URL::asset("/mngrAdmin/tickets?")}}' + 'status=' + $("#status").val() + '&user_id=' + $("#user_id").val() + '&date=' + $("#date").val() + '&search=' + $("#search-field").val() + '&assigned_to=' + $("#assigned_to").val() + '&closed_by=' + $("#closed_by").val() + '&corporate_id=' + $("#corporate_id").val() + '&created_by_type=' + $("#created_by_type").val(),
                    type: 'get',
                    success: function (data) {
                        $('.list').html(data.view);
                        $(".toggle").bootstrapToggle('destroy')
                        $(".toggle").bootstrapToggle();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }

            $("#search-btn1").on('click', function () {
                filter_tickets();
            });

            $("#status, #date, #user_id, #assigned_to, #closed_by, #corporate_id, #created_by_type").on('change', function () {
                filter_tickets();
            });

            var ticket_id = false;
            $('body').on('change', '.status', function (e) {
                if ($(this).is(':checked')) {
                    checked = true;
                    ticket_id = $(this).attr('data-id');
                    $(this).bootstrapToggle('off');
                    $("#closeModal").modal("show");
                }
            });

            $('body').on('click', '#close_button', function (e) {
                if ($("#closed_comment").val()) {
                    // $("#status_" + ticket_id).next('.toggle-group').find('.toggle-on').addClass('active');
                    // $("#status_" + ticket_id).next('.toggle-group').find('.toggle-off').removeClass('active');
                    $("#status_" + ticket_id).parent('.toggle').removeClass('btn-danger').removeClass('off');
                    $("#status_" + ticket_id).parent('.toggle').addClass('btn-success');
                    $("#status_" + ticket_id).bootstrapToggle("disable");
                    $("#closeModal").modal("hide");
                    $.ajax({
                        url: "{{ url('/mngrAdmin/tickets/close_ticket/')}}",
                        type: 'POST',
                        data: {'_token': "{{csrf_token()}}", ids: [ticket_id], comment: $("#closed_comment").val()},
                        success: function (data) {
                            $("#status_" + ticket_id).bootstrapToggle('on');
                            $("#status_" + ticket_id).bootstrapToggle('disable');
                            $("#closed_comment").val("");
                            $("#closeModal").modal("hide");
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }
            });
        });
    </script>
@endsection

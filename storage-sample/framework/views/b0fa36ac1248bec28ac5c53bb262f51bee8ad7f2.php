

<?php $__env->startSection('css'); ?>
  <title><?php echo e(__('backend.drivers')); ?> - <?php echo e(__('backend.view')); ?></title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
<div class="page-header">
  <h3># <?php echo e($driver->id); ?> - <?php echo e(__('backend.driver')); ?> / <?php echo e($driver->mobile); ?> - <?php echo e($driver->name); ?> </h3>
  <?php echo $driver->status_span; ?>    <?php echo $driver->active_span; ?>    <?php echo $driver->block_span; ?> <?php echo e($driver->created_at); ?>


  <form action="<?php echo e(url('mngrAdmin/del', $driver->id)); ?>" method="POST" style="display: inline;" onsubmit="if(confirm('<?php echo e(__('backend.msg_confirm_delete')); ?>')) { return true } else {return false };">
    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
    <div class="btn-group pull-right" role="group" aria-label="...">
      <?php if(permission('blockedDriver')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
        <button id="block" type="button" value="<?php echo e($driver->id); ?>"  class="btn btn-success"><?php echo e($driver->block_txt); ?></button>
      <?php endif; ?>

      <?php if(permission('activeDriver')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
        <button id="active" type="button" value="<?php echo e($driver->id); ?>"  class="btn btn-info"><?php echo e($driver->active_txt); ?></button>
      <?php endif; ?>

      <?php if(permission('editDriver')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
        <a class="btn btn-warning btn-group" role="group" href="<?php echo e(route('mngrAdmin.drivers.edit', $driver->id)); ?>"><i class="glyphicon glyphicon-edit"></i> <?php echo e(__('backend.edit')); ?></a>
      <?php endif; ?>

      <?php if(permission('deleteDriver')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
        <button type="submit" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i> <?php echo e(__('backend.delete')); ?></button>
      <?php endif; ?>
    </div>
  </form>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="row">
  <div class="col-md-12 col-xs-12 col-sm-12">
    <div class="col-sm-12" style="border: 2px solid #fdb801; color: #fff; margin-bottom: 20px; margin-top: 20px; background-color: #fdb801;">
      <h4><?php echo e(__('backend.personal_information')); ?></h4>
    </div>
    <div class="form-group col-sm-3">
       <img src="<?php echo e($driver->image); ?>" style="width:260px; height: 250px;">
    </div>
    <div class="form-group col-sm-3">
       <label for="mobile"><?php echo e(__('backend.email')); ?></label>
       <p class="form-control-static"><?php echo e($driver->email); ?></p>
    </div>
    <div class="form-group col-sm-3">
       <label for="phone"><?php echo e(__('backend.mobile_number2')); ?></label>
       <p class="form-control-static"><?php echo e($driver->phone); ?></p>
    </div>
    <div class="form-group col-sm-3">
       <label for="country"><?php echo e(__('backend.country')); ?></label>
       <p class="form-control-static"><?php echo e(! empty($driver->country) ?  $driver->country->name : ''); ?></p>
    </div>
    <div class="form-group col-sm-3">
       <label for="city"><?php echo e(__('backend.government')); ?></label>
       <p class="form-control-static"><?php echo e(! empty($driver->government) ?  $driver->government->name_en : ''); ?></p>
    </div>
    <div class="form-group col-sm-3">
       <label for="city"><?php echo e(__('backend.city')); ?></label>
       <p class="form-control-static"><?php echo e(! empty($driver->city) ?  $driver->city->name_en : ''); ?></p>
    </div>

     <div class="form-group col-sm-3">
         <label for="profit_rate"><?php echo e(__('backend.profit_rate')); ?></label>
         <p class="form-control-static"><?php echo e($driver->profit ?  $driver->profit : '0'); ?> %</p>
    </div>

    <div class="form-group col-sm-3">
         <label for="pickup_price"><?php echo e(__('backend.pickup_price')); ?></label>
         <p class="form-control-static"><?php echo e($driver->pickup_price ?  $driver->pickup_price : '0'); ?> </p>
    </div>
    <div class="form-group col-sm-3">
         <label for="basic_salary"><?php echo e(__('backend.basic_salary')); ?></label>
         <p class="form-control-static"><?php echo e($driver->basic_salary ?  $driver->basic_salary : '0'); ?> </p>
    </div>
    <div class="form-group col-sm-3">
         <label for="bonus_of_delivery"><?php echo e(__('backend.bonus_of_delivery')); ?></label>
         <p class="form-control-static"><?php echo e($driver->bonus_of_delivery ?  $driver->bonus_of_delivery : '0'); ?> </p>
    </div>

    <div class="form-group col-sm-3">
       <label for="driving_licence_number"><?php echo e(__('backend.drivier_licence_number')); ?>: </label>
       <p class="form-control-static"><?php echo e($driver->driving_licence_number); ?></p>
    </div>
    <div class="form-group col-sm-3">
       <label for="driving_licence_expired_date"><?php echo e(__('backend.drivier_licence_expiration_date')); ?>: </label>
       <p class="form-control-static"><?php echo e($driver->driving_licence_expired_date); ?></p>
    </div>
    <div class="form-group col-sm-3">
       <label for="national_id_expired_date"><?php echo e(__('backend.national_id_expired_date')); ?>: </label>
       <p class="form-control-static"><?php echo e($driver->national_id_expired_date); ?></p>
    </div>
    <div class="form-group col-sm-3">
       <label for="national_id_number"><?php echo e(__('backend.national_id_number')); ?>: </label>
       <p class="form-control-static"><?php echo e($driver->national_id_number); ?></p>
    </div>
    <div class="form-group col-sm-3">
       <label for="address"><?php echo e(__('backend.address')); ?>: </label>
       <p class="form-control-static"><?php echo e($driver->address); ?></p>
    </div>
    <div class="form-group col-sm-3">
       <label for="notes"><?php echo e(__('backend.notes')); ?>: </label>
       <p class="form-control-static">
         <?php if($driver->notes == null): ?>
          <?php echo e(__('backend.No_Notes_to_Show')); ?>

         <?php else: ?>
          <?php echo e($driver->notes); ?>

         <?php endif; ?>
       </p>
    </div>
    <?php if($driver->driver_has_vehicle == 1): ?>
    <div class="form-group col-sm-4" style="margin-top: 60px;">
       <h4 for="driving_licence_image"><?php echo e(__('backend.drivier_licence_image_front')); ?>: </h4>
       <p class="form-control-static"><img src="<?php echo e($driver->driving_licence_image_front); ?>" style="width:260px; height: 250px;"></p>
    </div>
    <?php endif; ?>
    <div class="form-group col-sm-4" style="margin-top: 60px;">
       <h4 for="driving_licence_image"><?php echo e(__('backend.criminal_record_image')); ?>: </h4>
       <p class="form-control-static"><img src="<?php echo e($driver->criminal_record_image_front); ?>" style="width:260px; height: 250px;"></p>
    </div> 
    <div class="form-group col-sm-4" style="margin-top: 60px;">
       <h4 for="national_id_image"><?php echo e(__('backend.national_id_image_front')); ?>: </h4>
       <p class="form-control-static"><img src="<?php echo e($driver->national_id_image_front); ?>" style="width:260px; height: 250px;"></p>
    </div>
    <!-- <div class="form-group col-sm-6">
       <h4 for="national_id_image"><?php echo e(__('backend.national_id_image_back')); ?>: </h4>
       <p class="form-control-static"><img src="<?php echo e($driver->national_id_image_back); ?>" style="width:360px; height: 350px;"></p>
    </div> -->

        
    <?php if(! empty($driver->device) ) { ?>
      <div class="form-group col-sm-12" style="width:100%;height:500px;">
        <h4><?php echo e(__('backend.Driver_location')); ?>: </h4>
    <?php

        if($driver->device->status == 1 ) {


          Mapper::map($driver->device->latitude, $driver->device->longitude, ['marker' => false,'locate'=>false])->informationWindow($driver->device->latitude, $driver->device->longitude,"Driver Name:  {$driver->name} - Mobile: {$driver->mobile}", [ 'title' => 'Online','icon' => [
                   'path' => 'M365.027,44.5c-30-29.667-66.333-44.5-109-44.5s-79,14.833-109,44.5s-45,65.5-45,107.5    c0,25.333,12.833,67.667,38.5,127c25.667,59.334,51.333,113.334,77,162s38.5,72.334,38.5,71c4-7.334,9.5-17.334,16.5-30    s19.333-36.5,37-71.5s33.167-67.166,46.5-96.5c13.334-29.332,25.667-59.667,37-91s17-55,17-71    C410.027,110,395.027,74.167,365.027,44.5z M289.027,184c-9.333,9.333-20.5,14-33.5,14c-13,0-24.167-4.667-33.5-14    s-14-20.5-14-33.5s4.667-24,14-33s20.5-13.5,33.5-13.5c13,0,24.167,4.5,33.5,13.5s14,20,14,33S298.36,174.667,289.027,184z',
                   'fillColor' => '#62d233',
                   'fillOpacity' => 0.8,
                   'scale' => 0.08,
                   'strokeColor' => '#006400',
                   'locate'=>false,
                   'strokeWeight' => 1.5
            ]]);


          } else {
            Mapper::map($driver->device->latitude, $driver->device->longitude, ['marker' => false,'locate'=>false])->informationWindow($driver->device->latitude, $driver->device->longitude,"Driver Name:  {$driver->name} - Mobile: {$driver->mobile}", [ 'title' => 'Offline','icon' => [
                     'path' => 'M365.027,44.5c-30-29.667-66.333-44.5-109-44.5s-79,14.833-109,44.5s-45,65.5-45,107.5    c0,25.333,12.833,67.667,38.5,127c25.667,59.334,51.333,113.334,77,162s38.5,72.334,38.5,71c4-7.334,9.5-17.334,16.5-30    s19.333-36.5,37-71.5s33.167-67.166,46.5-96.5c13.334-29.332,25.667-59.667,37-91s17-55,17-71    C410.027,110,395.027,74.167,365.027,44.5z M289.027,184c-9.333,9.333-20.5,14-33.5,14c-13,0-24.167-4.667-33.5-14    s-14-20.5-14-33.5s4.667-24,14-33s20.5-13.5,33.5-13.5c13,0,24.167,4.5,33.5,13.5s14,20,14,33S298.36,174.667,289.027,184z',
                     'fillColor' => '#808080',
                     'fillOpacity' => 0.8,
                     'scale' => 0.08,
                     'locate'=>false,
                     'strokeColor' => '##808080',
                     'strokeWeight' => 1.5
              ]]);
          }

        $map= Mapper::render() ;
        echo $map;
    ?>
      </div>
<?php } ?>

    <?php if(! empty($driver->agent)): ?>
      <div class="col-sm-12" style="margin-top: 90px; border: 2px solid #fdb801;color: #fff;margin-bottom: 20px;background-color: #fdb801;">
        <h4><?php echo e(__('backend.Belonging_Agent_Information')); ?></h4>
      </div>
      <div class="form-group col-sm-6">
         <label for="agent_id"><?php echo e(__('backend.Belonging_Agent_Name')); ?>: </label>
         <p class="form-control-static"><?php echo e($driver->agent->name); ?></p>
      </div>
      <div class="form-group col-sm-6">
         <label for="mobile"><?php echo e(__('backend.Belonging_Agent_Mobile')); ?>: </label>
         <p class="form-control-static"><?php echo e($driver->agent->mobile); ?></p>
      </div>
      <div class="form-group col-sm-6">
         <label for="phone"><?php echo e(__('backend.Belonging_Agent_Mobile2')); ?>: </label>
         <p class="form-control-static"><?php echo e($driver->agent->phone); ?></p>
      </div>
   <?php endif; ?>

   <?php if(! empty($driver->truck)): ?>

      <div class="col-sm-12" style="border: 2px solid #fdb801;color: #fff;margin-bottom: 20px;background-color: #fdb801;" >
        <h4><?php echo e(__('backend.Truck_Information')); ?>: </h4>
      </div>

           <div class="col-sm-12">
            <div class="form-group col-sm-1">
              <label for="notes"> <?php echo e(__('backend.id')); ?>: </label>
              <p class="form-control-static">
                <?php echo e(! empty($driver->truck->id)? $driver->truck->id : ''); ?>

              </p>
            </div>
            <div class="form-group col-sm-2">
              <label for="notes"> <?php echo e(__('backend.Model_Name')); ?>: </label>
              <p class="form-control-static">
                <?php echo e(! empty($driver->truck->model_name)? $driver->truck->model_name : ''); ?>

              </p>
            </div>
            <div class="form-group col-sm-2">
              <label for="notes"> <?php echo e(__('backend.Plate_Number')); ?>: </label>
              <p class="form-control-static">
                <?php echo e(! empty($driver->truck->plate_number)? $driver->truck->plate_number : ''); ?>

              </p>
            </div>

          <div class="form-group col-sm-2">
            <label for="notes"> <?php echo e(__('backend.Chassi_Number')); ?></label>
            <p class="form-control-static">
              <?php echo e(! empty($driver->truck->chassi_number)? $driver->truck->chassi_number : ''); ?>

            </p>
          </div>

          <div class="form-group col-sm-3">
            <label for="notes"> <?php echo e(__('backend.License_Image_Front')); ?></label>
            <p class="form-control-static"><img src="<?php echo e($driver->truck->license_image_front); ?>" width="100%"></p>
            
          </div>
           <div class="form-group col-sm-1">
              <label for="notes">  </label>
              <p class="form-control-static pull-left">
               <a href="<?php echo e(url('/mngrAdmin/trucks/'.$driver->truck->id)); ?>" class="btn btn-default" target="_blank"><?php echo e(__('backend.View_All')); ?></a>
              </p>
            </div>

        </div>
        <div class="col-md-12 col-xs-12 col-sm-12">
          <hr style="color: green">
        </div>

        <?php else: ?>
            <div class="form-group col-sm-12 text-center"><b><?php echo e(__('backend.This_Driver_Not_Have_Any_Vehicles')); ?></b></div>

      <?php endif; ?>
  </div>

  <div class="col-md-12 col-xs-12 col-sm-12">
    <a class="btn btn-link pull-right" href="<?php echo e(URL::previous()); ?>"><i class="glyphicon glyphicon-backward"></i> <?php echo e(__('backend.back')); ?></a>
  </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script>
$(function () {
    $('#block').on('click', function () {
       var Status = $(this).val();
   // alert(Status);
        $.ajax({
            url: "<?php echo e(url('/mngrAdmin/driver/block/'.$driver->id)); ?>",
            Type:"GET",
            dataType : 'json',
        	success: function(response){
        		if(response==0){
        			$('#block').html('Block');
        		}else{
        			$('#block').html('UnBlock');
        		}
             location.reload();
   			}
        });
    });

 $('#active').on('click', function ()
{
       var Status = $(this).val();
        $.ajax({
            url: "<?php echo e(url('/mngrAdmin/driver/active/'.$driver->id)); ?>",
            Type:"GET",
            dataType : 'json',
        	success: function(response){
        		if(response==0){
        			$('#active').html('Active');

        		}else{
        			$('#active').html('UnActivate');
        		}
            	location.reload();
   			}
        });
    });
});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/drivers/show.blade.php ENDPATH**/ ?>
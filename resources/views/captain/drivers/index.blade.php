@extends('captain.layouts.app')

@section('css')
    <title>{{__('captain.drivers')}}</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <style>
        .toggle.ios, .toggle-on.ios, .toggle-off.ios {
            border-radius: 20px;
        }

        .toggle.ios .toggle-handle {
            border-radius: 20px;
        }

        @media only screen and (max-width: 580px) {
            .hide-td {
                display: none;
            }
        }

        .box-body {
            min-height: 100px;
        }

        .stylish-input-group .input-group-addon {
            background: white !important;
        }

        .stylish-input-group .form-control {
            border-right: 0;
            box-shadow: 0 0 0;
            border-color: #ccc;
        }

        .stylish-input-group button {
            border: 0;
            background: transparent;
        }
    </style>
@endsection

@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> {{__('captain.drivers')}}
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-success btn-group" role="group" href="{{ route('captainDashboard.drivers.create') }}"><i
                        class="glyphicon glyphicon-plus"></i> {{__('captain.add_driver')}}</a>
            </div>
        </h3>
    </div>
@endsection

@section('content')
    <div class="row" style="margin-bottom:15px;">
        <div class="col-md-12">
            <div class="row">
                <div class="group-control col-md-4 col-sm-4">
                    <form action="{{URL::asset('/captainDashboard/drivers')}}" method="get" id="search-form"/>
                    <div id="imaginary_container">
                        <div class="input-group stylish-input-group">
                            <input type="text" class="form-control" id="search-field" name="search"
                                   placeholder="{{__('captain.search_by_name_or_mobile')}}">
                            <span class="input-group-addon">
              <button type="button" id="search-btn1">
                <span class="glyphicon glyphicon-search"></span>
              </button>
            </span>
                        </div>
                    </div>
                    </form>
                </div>

                <div class="group-control col-md-2 col-sm-2">
                    <select id="active-field" name="active" class="form-control">
                        <option value="-1">{{__('captain.sort_by_active')}} </option>
                        <option value="0">{{__('captain.not_activated')}}</option>
                        <option value="1">{{__('captain.activated')}}</option>
                    </select>
                </div>


            </div>
        </div>
    </div>

    <div class="list">
        @include('captain.drivers.table')
    </div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script>
        $('.toggle').bootstrapToggle();


        // $('#driversTable').DataTable({
        //   "pagingType": "full_numbers"
        // });

        $("#active-field").on('change', function () {
            $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
            $.ajax({
                url: '{{URL::asset("/captainDashboard/drivers")}}' + '?active=' + $("#active-field").val(),
                type: 'get',
                success: function (data) {
                    $('.list').html('');
                    $('.list').html(data.view);
                    $('#driversTable').DataTable({
                        "pagingType": "full_numbers"
                    });
                    $(".toggle").bootstrapToggle('destroy')
                    $(".toggle").bootstrapToggle();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $("#search-btn1").on('click', function () {
            $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
            $.ajax({
                url: '{{URL::asset("/captainDashboard/drivers")}}' + '?active=' + $("#status-field").val() + '&&block=' + $("#block-field").val() + '&&search=' + $("#search-field").val(),
                type: 'get',
                data: $("#search-form").serialize(),
                success: function (data) {
                    $('.list').html(data.view);
                    $('#driversTable').DataTable({
                        "pagingType": "full_numbers"
                    });
                    $(".toggle").bootstrapToggle('destroy')
                    $(".toggle").bootstrapToggle();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });


        $('body').on('change', '.change_verify', function (e) {

            let id = $(this).attr('data-id');
            $.ajax({
                url: '{{URL::asset("/captainDashboard/change-verify-driver")}}',
                type: 'post',
                data: {id: id, _token: "{{csrf_token()}}"},
                success: function (data) {

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

    </script>

@endsection

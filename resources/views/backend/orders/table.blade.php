<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        @if(! empty($orders) && count($orders) > 0)
            <form method="post" id="myform" action="{{ url('/mngrAdmin/forward_order') }}">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <div class="table-responsive">
                    <table id="theTable" class="table table-condensed table-striped text-center"
                           style="min-height: 200px;">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>ID</th>
                            <th>No</th>
                            @if(app('request')->filled('collection'))
                                <th>{{__('backend.status')}}</th>
                            @endif
                            {{--                            @if($type == 'pending' || $type == 'dropped'  )--}}
                            {{--                                <th>{{__('backend.agent')}}</th>--}}
                            {{--                            @endif--}}

                            <th>{{__('backend.collection')}}</th>

                            <th>{{__('backend.receiver_name')}}</th>
                            <th>{{__('backend.sender_name')}}</th>
                            <th>{{__('backend.from')}}</th>
                            <th>{{__('backend.to')}}</th>
                            @if($type == 'pending' || $type == 'dropped' )
                                <th>{{__('backend.city')}}</th>
                            @endif
                            {{--                            @if($type == 'dropped' )--}}
                            <th>{{__('backend.warehouse')}}</th>
                            <th>{{__('backend.warehouse_manager')}}</th>
                            {{--                            @endif--}}
                            <th style="font-weight: bold; color: #333;">{{__('backend.order_number')}}</th>
                            {{--                            @if($type != 'recall')--}}
                            {{--                                <th style="font-weight: bold; color: #333;">{{__('backend.receiver_code')}}</th>--}}
                            {{--                            @endif--}}
                            @if($type == 'receive' || $type=='deliver' )
                                <th>{{__('backend.delivery_price')}}</th>
                            @endif
                            @if($type=='deliver' )
                                <th>{{__('backend.payment_status')}}</th>
                            @endif
                            @if($type == 'recall')
                                <th>{{__('backend.status')}}</th>
                                <th>{{__('backend.action_date')}}</th>
                            @endif
                            @if($type == 'recall')
                                <th>{{__('backend.in_warehouse')}}</th>
                                <th>{{__('backend.in_client')}}</th>
                            @endif

                            {{--                            @if($type == 'pending' || $type == 'dropped')--}}
                            {{--                                <th>{{__('backend.headoffice')}}</th>--}}
                            {{--                            @endif--}}

                            @if($type == 'cancel' )
                                <th>{{__('backend.cancel_by')}}</th>
                                <th>{{__('backend.cancel_at')}}</th>

                            @endif

                            @if($type != 'recall' )
                                <th style="width: 150px;">
                                    @if($type == 'pending' || $type == 'dropped' || $type=='waiting' || app('request')->input('collection') || app('request')->input('move_collection') )

                                        <input type="button" class="btn btn-info btn-xs" id="toggle"
                                               value="select" onClick="do_this()"/>
                                        <!-- <button type="submit" class="btn btn-warning btn-xs btn-pdf">PDF</button> -->
                                    @endif
                                    @if($type == 'dropped' || $type=='waiting' )
                                        <a class="btn btn-xs btn btn btn-success" id="forward" data-toggle="modal"
                                           data-target="#forwardModal">
                                            <i class="fa fa-eye"></i> {{__('backend.forward')}}

                                        </a>
                                    @endif
                                    @if($type == 'receive'  || $type == 'accept')
                                        <a class="btn btn-xs btn btn btn-info disabled" id="delay_orders"
                                           data-toggle="modal"
                                           data-target="#delayModal"
                                           style="margin-bottom: 5px">
                                            <i class="fa fa-calendar"></i> {{__('backend.delay')}}
                                        </a>
                                    @endif
                                    @if($type == 'receive')
                                        <a class="btn btn-xs btn btn btn-success disabled" id="deliver_orders">
                                            <i class="fa fa-battery"></i> {{__('backend.deliver')}}
                                        </a>
                                    @endif
                                    @if($type == 'accept' )
                                        <a class="btn btn-xs btn btn btn-success disabled" id="receive_orders">
                                            <i class="fa fa-battery-half"></i> {{__('backend.Receive')}}
                                        </a>
                                    @endif

                                    <button type="button" id="pdf-submit"
                                            class="btn btn-warning btn-pdf btn-xs ">{{__('backend.pdf')}}</button>
                                </th>
                            @endif

                            @if(($type == 'pending') || app('request')->input('collection') )
                                <th>
                                    <input type="checkbox" class="form-control"
                                           style="display: inline-block;vertical-align: bottom;" id="dropAll">
                                    <a class="btn btn-xs btn btn btn-info" style="display: inline-block"
                                       id="drop_off">
                                        <i class="fa fa-dropbox"></i> {{__('backend.confirm_drop_orders')}}

                                    </a>
                                </th>
                            @endif

                            @if(app('request')->input('move_collection') )
                                <th>
                                    <input type="checkbox" class="form-control"
                                           style="display: inline-block;vertical-align: bottom;" id="dropWarehouseAll">
                                    <a class="btn btn-xs btn btn btn-info" style="display: inline-block"
                                       id="drop_warehouse_off">
                                        <i class="fa fa-dropbox"></i> {{__('backend.confirm_drop_orders')}}

                                    </a>
                                </th>
                            @endif

                            @if($type == 'recall')
                                <th>
                                    <input type="checkbox" class="form-control"
                                           style="display: inline-block;vertical-align: bottom;" id="recallClientAll">
                                    <a class="btn btn-xs btn btn btn-info disabled" style="display: inline-block"
                                       id="send_client">{{__('backend.send_client')}}</a>
                                </th>
                            @endif

                            @if($type == 'dropped' || $type == 'recall'  )
                                <th>
                                    <input type="checkbox" class="form-control"
                                           style="display: inline-block;vertical-align: bottom;" id="moveAll">
                                    <a class="btn btn-xs btn btn btn-info disabled" style="display: inline-block"
                                       data-toggle="modal"
                                       data-target="#moveModal"
                                       id="move_to">
                                        <i class="fa fa-dropbox"></i> {{__('backend.move_to')}}

                                    </a>
                                </th>
                            @endif

                            {{--                            @if($type == 'pending' || $type=='waiting' )--}}

                            {{--                                <th>--}}
                            {{--                                    <a class="btn btn-xs btn btn btn-primary" id="packup">--}}
                            {{--                                        <i class="fa fa-eye"></i> {{__('backend.packup')}}--}}

                            {{--                                    </a>--}}
                            {{--                                </th>--}}

                            {{--                            @endif--}}


                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $i => $order)
                            <tr>
                                <td>{{($i+1)}}</td>
                                <td>{{$order->id}}</td>
                                @if(app('request')->filled('collection'))
                                    <td>{!! $order->status_details !!}</td>
                                @endif
                                <td>{{$order->order_no}}</td>
                                {{--                                @if($type == 'pending' || $type == 'dropped' )--}}
                                {{--                                    <td>@if(! empty($order->agent) ) <a style="color:#f5b404;font-weight: bold"--}}
                                {{--                                                                        target="_blank"--}}
                                {{--                                                                        href="{{URL::asset('/mngrAdmin/agents/'.$order->agent_id)}}"> {{$order->agent->name}} </a> @endif--}}
                                {{--                                    </td>--}}
                                {{--                                @endif--}}
                                <td>{{$order->collection_id}}</td>
                                <td>{{$order->receiver_name}}</td>
                                <td>
                                    {{$order->sender_name}}
                                    @if($order->customer && $order->customer->Corporate)
                                        <strong style="color: #0c81bc"> {{$order->customer->Corporate->name}}</strong>
                                    @endif

                                </td>
                                <td>
                                    @if(! empty($order->from_government)  && $order->from_government != null )
                                        {{--                                        <a style="color:#f5b404;font-weight: bold" target="_blank"--}}
                                        {{--                                           href="{{URL::asset('/mngrAdmin/governorates/'.$order->s_government_id)}}"> {{$order->from_government->name_en}} </a>--}}
                                        {{$order->from_government->name_en}}
                                    @endif
                                </td>

                                <td>
                                    @if(! empty($order->to_government)  && $order->from_government != null  )
                                        {{--                                        <a--}}
                                        {{--                                        style="color:#f5b404;font-weight: bold" target="_blank"--}}
                                        {{--                                        href="{{URL::asset('/mngrAdmin/governorates/'.$order->r_government_id)}}"> {{$order->to_government->name_en}} </a>--}}
                                        {{$order->to_government->name_en}}
                                    @endif
                                </td>
                                @if($type == 'pending' || $type == 'dropped' )
                                    <td> {{isset($order->to_city->name_en) ? $order->to_city->name_en : '-'}}</td>
                                @endif
                                {{--                                @if($type == 'dropped' )--}}
                                <td> {{isset($order->warehouse->name) ? $order->warehouse->name : '-'}}</td>
                                <td>
                                    @if(!empty($order->warehouse->responsibles))
                                        @foreach($order->warehouse->responsibles as $responsible)
                                            {{$responsible->name}}
                                        @endforeach
                                    @endif
                                </td>
                                {{--                                @endif--}}
                                <td style="font-weight: bold; color: #a43;">{{$order->order_number}}</td>
                                {{--                                @if($type != 'recall')--}}
                                {{--                                    <td style="font-weight: bold; color: #a4e;">{{$order->receiver_code}}</td>--}}
                                {{--                                @endif--}}
                                @if($type == 'receive' || $type=='deliver' )
                                    <td>{{$order->delivery_price}}</td>
                                @endif
                                @if($type=='deliver' )
                                    <td>{!! $order->payment_status_span !!}</td>
                                @endif
                                @if($type == 'recall' )
                                    <td>{!! $order->recall_span !!}</td>
                                    <td>
                                        @if($order->status == 5)
                                            {!! date("Y-m-d H:i:s", strtotime($order->recalled_at)) !!}
                                        @elseif($order->status == 8)
                                            {!! date("Y-m-d H:i:s", strtotime($order->rejected_at)) !!}
                                        @elseif($order->status == 4)
                                            {!! date("Y-m-d H:i:s", strtotime($order->cancelled_at)) !!}
                                        @else
                                            {!! date("Y-m-d H:i:s", strtotime($order->delivered_at)) !!}
                                        @endif
                                    </td>
                                @endif
                                @if($type == 'recall')
                                    <td><input data-id="{{$order->id}}" data-size="mini"
                                               class="toggle warehouse_dropoff"
                                               {{$order->warehouse_dropoff == 1 ? 'checked' : ''}} disabled
                                               data-onstyle="success"
                                               type="checkbox" data-style="ios" data-on="Yes" data-off="No"></td>
                                    <td><input data-id="{{$order->id}}" data-size="mini"
                                               class="toggle client_dropoff"
                                               {{$order->client_dropoff == 1 ? 'checked' : ''}} disabled
                                               {{--$order->warehouse_dropoff == 0 || $order->client_dropoff == 1 ? 'disabled' : ''--}} data-onstyle="success"
                                               type="checkbox" data-style="ios" data-on="Yes" data-off="No"
                                               id="client_dropoff_{{$order->id}}"></td>
                                @endif
                                {{--                                @if($type == 'pending' || $type == 'dropped')--}}
                                {{--                                    <td><input data-id="{{$order->id}}" data-size="mini"--}}
                                {{--                                               class="toggle head_office"--}}
                                {{--                                               {{$order->head_office == 1 ? 'checked' : ''}} data-onstyle="success"--}}
                                {{--                                               type="checkbox" data-style="ios" data-on="Yes" data-off="No"></td>--}}
                                {{--                                @endif--}}
                                @if($type == 'cancel' )
                                    <td>{!! $order->cancel_span !!}</td>
                                    <td>{!! $order->cancelled_at !!}</td>

                                @endif

                                @if($type != 'recall' )
                                    <td class="selectpdf" id="selectpdf">
                                        @if($order->is_pickup == "0")

                                            <input type="checkbox" name="select[]" value="{{$order->id}}"
                                                   class="selectOrder"
                                                   data-value="{{$order->id}}"/>


                                        @endif

                                    </td>
                                @endif

                                @if(($type == 'pending') || app('request')->input('collection') )
                                    <td class="selectpackup">
                                        <input type="checkbox" class="selectdrop" name="selectdrop[]"
                                               value="{{$order->id}}"
                                               @if($order->status != '0') disabled @endif/>
                                    </td>
                                @endif

                                @if(app('request')->input('move_collection') )
                                    <td class="selectpackup">
                                        <input type="checkbox" class="selectdropWarehouse" name="selectdropWarehouse[]"
                                               value="{{$order->id}}"
                                               @if($order->dropped) disabled @endif/>
                                    </td>
                                @endif

                                @if($type == 'recall' )
                                    <td class="selectpackup">
                                        <input type="checkbox" class="selectrecallClient" name="selectrecallClient[]"
                                               id="selectrecallClient_{{$order->id}}"
                                               value="{{$order->id}}"
                                               data-customer="{{isset($order->customer_id) ? $order->customer_id : ''}}"
                                               @if(($order->warehouse_dropoff == 0 && $order->client_dropoff == 0) || $order->client_dropoff == 1 || $order->refund_collection_id) disabled @endif/>
                                    </td>
                                @endif

                                @if($type == 'dropped' || $type == 'recall' )
                                    <td class="selectpackup">
                                        <input type="checkbox" class="selectMoved" name="selectMoved[]"
                                               id="selectMoved_{{$order->id}}"
                                               data-warehouse="{{isset($order->moved_in) ? $order->moved_in : ''}}"
                                               value="{{$order->id}}"
                                               @if($order->move_collection_id || (in_array($order->status, [4,5]) && ($order->warehouse_dropoff==0 || $order->client_dropoff==1)) || !in_array($order->status, [4,5,7]))
                                               disabled
                                            @endif/>
                                    </td>
                                @endif

                                {{--                                <td class="selectpackup" id="selectpackup">--}}
                                {{--                                    @if ( ($type == 'pending') && $order->is_packedup !=1  && $order->collection_id != null )--}}
                                {{--                                        @if($order->is_pickup == "0")--}}
                                {{--                                            <input type="checkbox" class="selectpack" name="selectpackup[]"--}}
                                {{--                                                   value="{{$order->id}}" data-value="{{$order->collection_id}}"/>--}}

                                {{--                                        @else--}}
                                {{--                                            <span class='badge badge-pill label-warning'>Pick Up</span>--}}
                                {{--                                        @endif--}}
                                {{--                                    @endif--}}
                                {{--                                </td>--}}

                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-default dropdown-toggle" type="button"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li>
                                                <a href="{{ route('mngrAdmin.orders.show', $order->id ) }}"><i
                                                        class="fa fa-eye"></i> {{__('backend.view')}}</a>
                                            </li>
                                            @if ( $type != 'deliver' && $type != 'recall'  && ($type == 'cancel' && ( $order->cancelled_by == 1 ||  $order->cancelled_by == 2 ) ) )

                                                <li>
                                                    <a href="{{ route('mngrAdmin.orders.edit', $order->id) }}"><i
                                                            class="fa fa-edit"></i> {{__('backend.edit')}}</a>
                                                </li>
                                            @endif
                                            @if ( $type == 'recall' )
                                                <li>
                                                    <a href="{{ url('/mngrAdmin/forward_recall/'.$order->id) }}"><i
                                                            class="fa fa-share"></i> {{__('backend.edit_forward')}}</a>
                                                </li>
                                            @endif
                                            <li>
                                                <a class=" printdata" data-id="{{$order->id}}"><i
                                                        class="fa fa-print"></i> {{__('backend.pdf')}}</a>
                                            </li>

                                            @if (  $type ==  'expired_waiting' || $type == 'pending'  )
                                                <li>
                                                    <a data-id="{{$order->id}}" class="delete-order"><i
                                                            class="fa fa-trash"></i> {{__('backend.delete')}}</a>
                                                </li>
                                            @endif
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                        <div class="modal fade" id="forwardModal" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <input type="hidden" class="form-control" name="orderTypeID[]"
                                                   value="{{$order->order_type_id}}">
                                            <input type="hidden" class="form-control" name="governorateCostID[]"
                                                   value="{{$order->governorate_cost_id}}">
                                        </div>
                                        <p>{{__('backend.Forward_Orders_In_Agent_Or_Driver')}}</p>
                                        <div class="form-group">
                                            {{--                                            <br>--}}
                                            {{--                                            <select name="agent" id="agent" class="form-control agent" required>--}}
                                            {{--                                                <option value="" selected>{{__('backend.Choose_Agent')}}</option>--}}
                                            {{--                                                @if(! empty($agents))--}}
                                            {{--                                                    @foreach($agents as $agent)--}}
                                            {{--                                                        <option value="{{$agent->id}}">{{$agent->name}}</option>--}}
                                            {{--                                                    @endforeach--}}
                                            {{--                                                @endif--}}
                                            {{--                                            </select>--}}

                                            <br>
                                            <select class="form-control" name="driverID" id="driver">
                                                <option value="">{{__('backend.Choose_Driver')}}</option>
                                                @if(! empty($online_drivers))
                                                    @foreach($online_drivers as $driver)
                                                        <option value="{{$driver->id}}">{{$driver->name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary"
                                                    onclick="document.forms['myform'].submit(); return false;">{{__('backend.forward')}}</button>
                                            <button type="button" class="btn btn-danger"
                                                    data-dismiss="modal">{{__('backend.close')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </tbody>
                    </table>
                </div>
            </form>
            {!! $orders->appends($_GET)->links() !!}
        @else
            <h3 class="text-center alert alert-warning">No Result Found !</h3>
        @endif
    </div>
</div>
<div id="js-form" class="js-form">
</div>


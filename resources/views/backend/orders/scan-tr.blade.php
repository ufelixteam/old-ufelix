@if($orders)
    <tr id="{{$orders->id}}">
        <td class="serial"></td>
        <td>{{$orders->id}}</td>
        <td class="status">{!!  $orders->status_span !!}</td>
        <td>{{$orders->collection_id ? $orders->collection_id : '-'}}</td>
        <td>{{$orders->receiver_name}}</td>
        <td>
            {{$orders->sender_name}}
            <br>
            @if($orders->customer && $orders->customer->Corporate)
                <strong style="color: #0c81bc"> {{$orders->customer->Corporate->name}}</strong>
            @endif

        </td>
        <td>
            @if(! empty($orders->from_government)  && $orders->from_government != null )
                {{$orders->from_government->name_en}}
            @endif
        </td>

        <td>
            @if(! empty($orders->to_government)  && $orders->from_government != null  )
                {{$orders->to_government->name_en}}
            @endif
        </td>
        <td style="font-weight: bold; color: #a43;">{{$orders->order_number}}</td>
        <td style="font-weight: bold; color: #0c81bc;">
            @if(! empty($orders->accepted ) && ! empty($orders->accepted->driver))
                {{$orders->accepted->driver->name}}
            @else
                -
            @endif
        </td>
        <td class="selectpdf" id="selectpdf">
            @if($orders->is_pickup == "0")
                <input type="checkbox" name="select[]" value="{{$orders->id}}"
                       class="selectOrder"
                       data-value="{{$orders->id}}"/>
            @endif

        </td>
        <td>
            <div class="dropdown">
                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                </button>
                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                    <li>
                        <a href="{{ route('mngrAdmin.orders.show', $orders->id ) }}"><i
                                class="fa fa-eye"></i> {{__('backend.view')}}</a>
                    </li>
                    <li>
                        <a href="{{ route('mngrAdmin.orders.print_police', ['ids' => $orders->id] ) }}" target="_blank"><i
                                class="fa fa-print"></i> {{__('backend.print')}}</a>
                    </li>
                </ul>
            </div>
        </td>
    </tr>
@endif

<div class="modal fade" id="box-id" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
  	<div class="modal-dialog cascading-modal" role="document">
    	<!--Content-->
    	<div class="modal-content">
     		<div class="modal-c-tabs">
            	<div class="modal-body mb-1">
            		<div class="box-id">
					<div class="icon-logo">
						<img src="{{asset('website/images/flogo.png')}}" alt="" />
					</div>
					<div class="box-head">
						<h1>@lang('website.track_order') </h1>
						<span>@lang('website.track_order_hint')</span>
					</div>
					<div class="box-body">

						<form action="{{url('tracking')}}" action="get">
							<p>
								<label class="contw">@lang('website.sender')
									<input type="radio" checked="{{ ! empty($search_type) && $search_type == 1 ? 'checked' : '' }}" value="1" name="check" required="true" />
									<span class="checkmark"></span>
								</label>
							</p>
{{--							<p>--}}
{{--								<label class="contw">@lang('website.receiver')--}}
{{--									<input type="radio" name="check" checked="{{ ! empty($search_type) && $search_type == 2 ? 'checked' : '' }}" value="2" />--}}
{{--									<span class="checkmark"></span>--}}
{{--								</label>--}}
{{--							</p>--}}
							<input type="text" placeholder="@lang('website.order_no')"  name="search" value="{{ ! empty($search) ? $search : ''}}" />
							<button type="submit"><i class="fa fa-search"></i></button>
						</form>

					</div>
				</div>

            	</div>

            </div>

        </div>

    </div>

</div>


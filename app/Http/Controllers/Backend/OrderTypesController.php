<?php

namespace App\Http\Controllers\Backend;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Customer;
use App\Models\Corporate;
use App\Models\Order;
use App\Models\Invoice;
use App\Models\AcceptedOrder;
use App\Models\InvoiceOrder;
use App\Models\CollectionOrder;
use App\Models\CustomerDevice;
use App\Models\OrderType;
use Response;
use Flash;
use File;
use Auth;

class OrderTypesController extends Controller
{

  // Get All Order Types
  public function index()
  {
    $types = OrderType::paginate(15);
    return view('backend.types.index' ,compact('types'));
  }

  // Create Order Type Page
  public function create()
  {
    return view('backend.types.create');
  }

  // Added New Order Type
  public function add(Request $request)
  {
    /*
    * status = 0 :: Not Active
    * status = 1 :: Active
    */
    $this->validate($request,[
       'name_en'   => 'required',
       'name_ar'   => 'required',
       'price'     => 'required',
       'status'    => 'required'
    ]);
    OrderType::insert([
      'name_en'   => $request->name_en,
      'name_ar'   => $request->name_ar,
      'status'    => $request->status,
      'price'     => $request->price
    ]);

    // Function Add Action In Activity Log By Samir
    addActivityLog('Add New Order Type ' . $request->name_en , ' اضافة نوع طلب جديد ' . $request->name_ar, 20); // Helper Function in App\Helpers.php

    return Redirect('mngrAdmin/order_types');
  }

  // Edit Order Type Page
  public function edit($id)
  {
    $type = OrderType::where('id',$id)->first();
    return view('backend.types.edit',compact('type'));
  }

  // Updeted Order Type
  public function save_edit(Request $request ,$id)
  {
      /*
      * status = 0 :: Not Active
      * status = 1 :: Active
      */
      $this->validate($request,[
         'name_en'   => 'required',
         'name_ar'   => 'required',
         'price'     => 'required',
         'status'    => 'required'
      ]);
      OrderType::where('id',$id)->update([
        'name_en'   => $request->name_en,
        'name_ar'   => $request->name_ar,
        'price'     => $request->price,
        'status'    => $request->status
      ]);

      // Function Add Action In Activity Log By Samir
      addActivityLog('Edit Order Type ' . $request->name_en , ' تعديل نوع الطلب ' . $request->name_ar, 20); // Helper Function in App\Helpers.php

      return Redirect('mngrAdmin/order_types');
  }

  // Delete Order Type
  public function delete($id)
  {
    $type     = OrderType::find($id);
    $delete   = OrderType::where('id', $id)->delete();

    // Function Add Action In Activity Log By Samir
    addActivityLog('Delete Order Type ' . $type->name_en , ' حذف نوع الطلب ' . $type->name_ar, 20); // Helper Function in App\Helpers.php

    return Redirect('mngrAdmin/order_types');
  }

}

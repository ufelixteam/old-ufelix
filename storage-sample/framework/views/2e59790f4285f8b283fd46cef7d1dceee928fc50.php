<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12" >
        <?php if(! empty($orders) && count($orders) > 0): ?>
                <div class="table-responsive">
                    <table id="theTable" class="table table-condensed table-striped text-center table-bordered" style="min-height: 200px;">
                        <thead>
                        <tr>
                            <th>#</th>
                      
                            <th><?php echo e(__('backend.agent')); ?></th>
                     
                            <th><?php echo e(__('backend.receiver_name')); ?></th>
                            <th><?php echo e(__('backend.sender_name')); ?></th>
                            <th><?php echo e(__('backend.bonous')); ?></th>
                            <th><?php echo e(__('backend.from')); ?></th>
                            <th><?php echo e(__('backend.to')); ?></th>
                            <th style="font-weight: bold; color: #333;"><?php echo e(__('backend.order_number')); ?></th>
                            <th style="font-weight: bold; color: #333;"><?php echo e(__('backend.receiver_code')); ?></th>
              
                            <th></th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($order->id); ?></td>
                           
                                <td><?php if(! empty($order->agent) ): ?> <a style="color:#f5b404;font-weight: bold" target="_blank" href="<?php echo e(URL::asset('/mngrAdmin/agents/'.$order->agent_id)); ?>"> <?php echo e($order->agent->name); ?> </a> <?php endif; ?></td>
                              
                                <td><?php echo e($order->receiver_name); ?></td>
                                <td><?php echo e($order->sender_name); ?></td>
                                <td><?php echo e($order->bonous); ?></td>


                                <td>
                                    <?php if(! empty($order->from_government)  && $order->from_government != null ): ?>
                                        <a style="color:#f5b404;font-weight: bold" target="_blank" href="<?php echo e(URL::asset('/mngrAdmin/governorates/'.$order->s_government_id)); ?>" > <?php echo e($order->from_government->name_en); ?> </a>
                                    <?php endif; ?>
                                </td>

                                <td>
                                    <?php if(! empty($order->to_government)  && $order->from_government != null  ): ?> <a style="color:#f5b404;font-weight: bold" target="_blank" href="<?php echo e(URL::asset('/mngrAdmin/governorates/'.$order->r_government_id)); ?>" > <?php echo e($order->to_government->name_en); ?> </a>
                                    <?php endif; ?>
                                </td>
                                <td style="font-weight: bold; color: #a43;"><?php echo e($order->order_number); ?></td>
                                <td style="font-weight: bold; color: #a4e;"><?php echo e($order->receiver_code); ?></td>
                                <td>
                                    <a class="btn btn-xs btn-primary"  href="<?php echo e(route('mngrAdmin.orders.show', $order->id )); ?>"><i class="fa fa-eye"></i> <?php echo e(__('backend.view')); ?></a>

                                    <?php if($pickup->status != 3): ?>

                                        <a class="btn btn-xs btn-warning editPickup " data-id="<?php echo e($order->id); ?>"
                                        data-bonous="<?php echo e($order->bonous); ?>"   ><i class="fa fa-edit"></i> <?php echo e(__('backend.edit')); ?></a>

                                        <a class="btn btn-xs btn-danger delete-order-pickup" data-id="<?php echo e($order->id); ?>" data-pickup="<?php echo e($pickup->id); ?>"  id="delete-order-pickup"><i class="fa fa-trash"></i> <?php echo e(__('backend.delete')); ?></a>

                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>

            </div>

            <?php echo $orders->appends($_GET)->links(); ?>

        <?php else: ?>
            <h3 class="text-center alert alert-warning">No Result Found !</h3>
        <?php endif; ?>
    </div>
</div>



<div class="modal fade" id="editPickupModal" tabindex="-1" role="dialog" aria-labelledby="editPickupModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editPickupModalLabel">Update Order Bonous Price</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo e(url('/mngrAdmin/update_order_pickup')); ?>" method="POST">
        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
        <input type="hidden" name="pickup_id" value="<?php echo e($pickup->id); ?>">
        <input type="hidden" name="order_id" value="0" id="order_id_modal">

      <div class="modal-body" style="min-height: 120px">
  
            <div class="form-group col-md-12 col-sm-12 <?php if($errors->has('delivery_price')): ?> has-error <?php endif; ?>">
                  <label for="delivery_price-field">Bonous Price: </label>
                  <input type="text" required class="form-control" id="delivery_price_modal" name="delivery_price" value="" required>
            </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>

  </form>
    </div>
  </div>
</div><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/pickups/orders.blade.php ENDPATH**/ ?>
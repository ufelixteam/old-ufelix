@extends('captain.layouts.app')

@section('css')
    <title>{{__('captain.drivers')}} - {{__('captain.view')}}</title>
    <style>
        .img {
            /*border: 1px solid #8e7ad4;*/
            height: 100px;
            width: 100px;
            border-radius: 50%;
            /*padding: 10px;*/
        }

        .form-group {
            margin-top: 15px !important;
        }
    </style>
@endsection

@section('header')
    <div class="page-header">
        <h3># {{$driver->id}} - {{__('captain.driver')}} / {{$driver->mobile}} - {{$driver->name}} </h3>
        {!! $driver->status_span !!}    {!! $driver->active_span !!}    {!! $driver->block_span !!} {{$driver->created_at}}

        <form action="#" method="POST" style="display: inline;"
              onsubmit="if(confirm('{{__('captain.msg_confirm_delete')}}')) { return true } else {return false };">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-success btn-group"
                   href="{{url('captainDashboard/reports/captain_report') . '?driver_id=' . $driver->id}}"
                   target="_blank"><i
                        class="fa fa-file-excel-o"></i> {{__('captain.daily_report')}}</a>
{{--                <a class="btn btn-success btn-group"--}}
{{--                   href="{{url('captainDashboard/drivers/'.$driver->id.'/download_papers')}}"--}}
{{--                   target="_blank"><i--}}
{{--                        class="fa fa-file-zip-o"></i> {{__('captain.download_papers')}}</a>--}}
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="col-sm-12"
                 style="border: 2px solid #fdb801; color: #fff; margin-bottom: 20px; margin-top: 20px; background-color: #fdb801;">
                <h4>{{__('captain.personal_information')}}</h4>
            </div>
            <div class="form-group col-sm-3">
                <img src="{{ $driver->image }}" class="img" onerror="this.src='{{asset('assets/images/user.png')}}'">
            </div>
            <div class="form-group col-sm-3">
                <label for="mobile">{{__('captain.email')}}</label>
                <p class="form-control-static">{{$driver->email}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="city">{{__('captain.government')}}</label>
                <p class="form-control-static">{{ ! empty($driver->government) ?  $driver->government->name_en : '' }}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="city">{{__('captain.city')}}</label>
                <p class="form-control-static">{{ ! empty($driver->city) ?  $driver->city->name_en : '' }}</p>
            </div>

            <div class="form-group col-sm-3">
                <label for="profit_rate">{{__('captain.profit_rate')}}</label>
                <p class="form-control-static">{{ $driver->profit ?  $driver->profit : '0' }} %</p>
            </div>

            <div class="form-group col-sm-3">
                <label for="recall_price">{{__('captain.recall_price')}}</label>
                <p class="form-control-static">{{ $driver->recall_price ?  $driver->recall_price : '0' }} </p>
            </div>
            <div class="form-group col-sm-3">
                <label for="reject_price">{{__('captain.reject_price')}}</label>
                <p class="form-control-static">{{ $driver->reject_price ?  $driver->reject_price : '0' }} </p>
            </div>

            <div class="form-group col-sm-3">
                <label for="pickup_price">{{__('captain.pickup_price')}}</label>
                <p class="form-control-static">{{ $driver->pickup_price ?  $driver->pickup_price : '0' }} </p>
            </div>
            <div class="form-group col-sm-3">
                <label for="basic_salary">{{__('captain.basic_salary')}}</label>
                <p class="form-control-static">{{ $driver->basic_salary ?  $driver->basic_salary : '0' }} </p>
            </div>
            <div class="form-group col-sm-3">
                <label for="bonus_of_delivery">{{__('captain.bonus_of_delivery')}}</label>
                <p class="form-control-static">{{ $driver->bouns_of_delivery ?  $driver->bouns_of_delivery : '0' }} </p>
            </div>
            <div class="form-group col-sm-3">
                <label for="driving_licence_expired_date">{{__('captain.drivier_licence_expiration_date')}}: </label>
                <p class="form-control-static">{{$driver->driving_licence_expired_date}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="national_id_expired_date">{{__('captain.national_id_expired_date')}}: </label>
                <p class="form-control-static">{{$driver->national_id_expired_date}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="national_id_number">{{__('captain.national_id_number')}}: </label>
                <p class="form-control-static">{{$driver->national_id_number}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="address">{{__('captain.address')}}: </label>
                <p class="form-control-static">{{$driver->address}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="store">{{__('captain.store')}}: </label>
                <p class="form-control-static">{{!empty($driver->store) ? $driver->store->name : ''}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="notes">{{__('captain.notes')}}: </label>
                <p class="form-control-static">
                    @if($driver->notes == null)
                        {{__('captain.No_Notes_to_Show')}}
                    @else
                        {{$driver->notes}}
                    @endif
                </p>
            </div>
            <div class="col-sm-12">
                @if($driver->driver_has_vehicle == 1)
                    <div class="form-group col-sm-4" style="margin-top: 60px;">
                        <h4 for="driving_licence_image">{{__('captain.drivier_licence_image_front')}}: </h4>
                        <p class="form-control-static"><img class="img" src="{{ $driver->driving_licence_image_front }}"
                                                            onerror="this.src='{{asset('assets/images/image.png')}}'">
                        </p>
                    </div>
                @endif
                <div class="form-group col-sm-4" style="margin-top: 60px;">
                    <h4 for="driving_licence_image">{{__('captain.criminal_record_image')}}: </h4>
                    <p class="form-control-static"><img class="img" src="{{ $driver->criminal_record_image_front }}"
                                                        onerror="this.src='{{asset('assets/images/image.png')}}'"></p>
                </div>
                <div class="form-group col-sm-4" style="margin-top: 60px;">
                    <h4 for="national_id_image">{{__('captain.national_id_image_front')}}: </h4>
                    <p class="form-control-static"><img class="img" src="{{ $driver->national_id_image_front }}"
                                                        onerror="this.src='{{asset('assets/images/image.png')}}'"></p>
                </div>
            </div>

            <?php if(!empty($driver->device_active) ) { ?>
            <div class="form-group col-sm-12" style="width:100%;height:500px;">
                <h4>{{__('captain.Driver_location')}}: </h4>
                <?php

                Mapper::map($driver->device_active->latitude, $driver->device_active->longitude, ['marker' => false, 'locate' => false])->informationWindow($driver->device_active->latitude, $driver->device_active->longitude, "Driver Name:  {$driver->name} - Mobile: {$driver->mobile}", ['title' => 'Online', 'icon' => [
                    'path' => 'M365.027,44.5c-30-29.667-66.333-44.5-109-44.5s-79,14.833-109,44.5s-45,65.5-45,107.5    c0,25.333,12.833,67.667,38.5,127c25.667,59.334,51.333,113.334,77,162s38.5,72.334,38.5,71c4-7.334,9.5-17.334,16.5-30    s19.333-36.5,37-71.5s33.167-67.166,46.5-96.5c13.334-29.332,25.667-59.667,37-91s17-55,17-71    C410.027,110,395.027,74.167,365.027,44.5z M289.027,184c-9.333,9.333-20.5,14-33.5,14c-13,0-24.167-4.667-33.5-14    s-14-20.5-14-33.5s4.667-24,14-33s20.5-13.5,33.5-13.5c13,0,24.167,4.5,33.5,13.5s14,20,14,33S298.36,174.667,289.027,184z',
                    'fillColor' => '#62d233',
                    'fillOpacity' => 0.8,
                    'scale' => 0.08,
                    'strokeColor' => '#006400',
                    'locate' => false,
                    'strokeWeight' => 1.5
                ]]);
                $map = Mapper::render();
                echo $map;
                ?>
            </div>
            <?php } ?>

            @if (! empty($driver->agent))
                <div class="col-sm-12"
                     style="margin-top: 90px; border: 2px solid #fdb801;color: #fff;margin-bottom: 20px;background-color: #fdb801;">
                    <h4>{{__('captain.Belonging_Agent_Information')}}</h4>
                </div>
                <div class="form-group col-sm-4">
                    <label for="agent_id">{{__('captain.Belonging_Agent_Name')}}: </label>
                    <p class="form-control-static">{{$driver->agent->name}}</p>
                </div>
                <div class="form-group col-sm-4">
                    <label for="mobile">{{__('captain.Belonging_Agent_Mobile')}}: </label>
                    <p class="form-control-static">{{$driver->agent->mobile}}</p>
                </div>
                <div class="form-group col-sm-4">
                    <label for="phone">{{__('captain.Belonging_Agent_Mobile2')}}: </label>
                    <p class="form-control-static">{{$driver->agent->phone}}</p>
                </div>
            @endif

            @if (! empty($driver->truck))

                <div class="col-sm-12"
                     style="border: 2px solid #fdb801;color: #fff;margin-bottom: 20px;background-color: #fdb801;">
                    <h4>{{__('captain.Truck_Information')}}: </h4>
                </div>

                <div class="col-sm-12">
                    <div class="form-group col-sm-1">
                        <label for="notes"> {{__('captain.id')}}: </label>
                        <p class="form-control-static">
                            {{ ! empty($driver->truck->id)? $driver->truck->id : ''}}
                        </p>
                    </div>
                    <div class="form-group col-sm-2">
                        <label for="notes"> {{__('captain.Model_Name')}}: </label>
                        <p class="form-control-static">
                            {{ ! empty($driver->truck->model_name)? $driver->truck->model_name : ''}}
                        </p>
                    </div>
                    <div class="form-group col-sm-2">
                        <label for="notes"> {{__('captain.Plate_Number')}}: </label>
                        <p class="form-control-static">
                            {{ ! empty($driver->truck->plate_number)? $driver->truck->plate_number : ''}}
                        </p>
                    </div>

                    <div class="form-group col-sm-2">
                        <label for="notes"> {{__('captain.Chassi_Number')}}</label>
                        <p class="form-control-static">
                            {{ ! empty($driver->truck->chassi_number)? $driver->truck->chassi_number : ''}}
                        </p>
                    </div>

                    <div class="form-group col-sm-3">
                        <label for="notes"> {{__('captain.License_Image_Front')}}</label>
                        <p class="form-control-static"><img class="img" src="{{ $driver->truck->license_image_front }}"
                                                            onerror="this.src='{{asset('assets/images/image.png')}}'">
                        </p>

                    </div>
{{--                    <div class="form-group col-sm-1">--}}
{{--                        <label for="notes"> </label>--}}
{{--                        <p class="form-control-static pull-left">--}}
{{--                            <a href="{{url('/captainDashboard/trucks/'.$driver->truck->id)}}" class="btn btn-default"--}}
{{--                               target="_blank">{{__('captain.View_All')}}</a>--}}
{{--                        </p>--}}
{{--                    </div>--}}

                </div>
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <hr style="color: green">
                </div>

            @else
                <div class="form-group col-sm-12 text-center"><b>{{__('captain.This_Driver_Not_Have_Any_Vehicles')}}</b>
                </div>

            @endif
        </div>

        <div class="col-md-12 col-xs-12 col-sm-12">
            <a class="btn btn-link pull-right" href="{{ URL::previous()}}"><i
                    class="glyphicon glyphicon-backward"></i> {{__('captain.back')}}</a>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function () {
            $('#block').on('click', function () {
                var Status = $(this).val();
                // alert(Status);
                $.ajax({
                    url: "{{ url('/captainDashboard/driver/block/'.$driver->id) }}",
                    Type: "GET",
                    dataType: 'json',
                    success: function (response) {
                        if (response == 0) {
                            $('#block').html('Block');
                        } else {
                            $('#block').html('UnBlock');
                        }
                        location.reload();
                    }
                });
            });

            $('#active').on('click', function () {
                var Status = $(this).val();
                $.ajax({
                    url: "{{ url('/captainDashboard/driver/active/'.$driver->id) }}",
                    Type: "GET",
                    dataType: 'json',
                    success: function (response) {
                        if (response == 0) {
                            $('#active').html('Active');

                        } else {
                            $('#active').html('UnActivate');
                        }
                        location.reload();
                    }
                });
            });
        });
    </script>
@endsection

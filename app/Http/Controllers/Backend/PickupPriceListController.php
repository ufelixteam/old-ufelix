<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Country;
use App\Models\Governorate;
use App\Models\PickupPriceList;
use App\Models\Order;
use Response;
use Flash;
use File;
use Auth;
class PickupPriceListController extends Controller {

	// Get Cities By Country ID
	public function get_governorate($id) {
		return Governorate::where('country_id', $id)->get();
	}

	// Get All Governorate Price
	public function index() {
		$governoratePrices = PickupPriceList::orderBy('id', 'desc')->paginate('15');
		return view('backend.pickup_price_lists.index', compact('governoratePrices'));
	}

	// Create Governorate Price Page
	public function create() {
		$list_governorates = Governorate::orderBy('name_en','ASC')->get();
		return view('backend.pickup_price_lists.create',compact('list_governorates'));
	}

	// Added New Governorate Price
	public function store(Request $request) {
		/*
		* status = 0 :: Not Verify
		* status = 1 :: Verify
		*/

		$v = \Validator::make($request->all(), [
					'start_station'    		=> 'required',
					'access_station' 	 	=> 'required',
					'cost'    				=> 'required',
					'bonous'    			=> 'required',
					'status'  				=> 'required',
		]);

		if ($v->fails()) {
				return redirect()->back()->withErrors($v->errors());
		}

		$governoratePrice 						= new GovernoratePrice();
		$governoratePrice->start_station 		= $request->input("start_station");
		$governoratePrice->access_station 		= $request->input("access_station");
	    $governoratePrice->cost 				= $request->input("cost");
	    $governoratePrice->bonous 				= $request->input("bonous");
	    $governoratePrice->status 				= $request->input("status");
		$governoratePrice->save();

		// Function Add Action In Activity Log By Samir
		addActivityLog('Add New Shipment Destinations #No ' . $governoratePrice->id , ' اضافة وجهة شحن جديدة رقم ' . $governoratePrice->id, 11); // Helper Function in App\Helpers.php

    	Flash::success(__('backend.Destination_Added_successfully'));
		return redirect()->route('mngrAdmin.pickup_price_lists.index');
	}

	// View One Governorate Price
	public function show($id) {
		$governorate = Governorate::findOrFail($id);
		return view('backend.governorates.show', compact('governorate'));
	}

	// Edit Governorate Price Page
	public function edit($id){
		$governoratePrice 		= PickupPriceList::findOrFail($id);
		$list_governorates 		= Governorate::orderBy('name_en','ASC')->get();
		return view('backend.pickup_price_lists.edit', compact('governoratePrice','list_governorates'));
	}

	// Update Governorate Price
	public function update(Request $request, $id) {
		/*
		* status = 0 :: Not Verify
		* status = 1 :: Verify
		*/

		$v = \Validator::make($request->all(), [
					'start_station'    		=> 'required',
					'access_station' 	 		=> 'required',
					'cost'    						=> 'required',
					'status'  						=> 'required',
		]);

		if ($v->fails()) {
				return redirect()->back()->withErrors($v->errors());
		}

		$governoratePrice 								= PickupPriceList::findOrFail($id);
		$governoratePrice->start_station 				= $request->input("start_station");
		$governoratePrice->access_station 				= $request->input("access_station");
    	$governoratePrice->cost 						= $request->input("cost");
	    $governoratePrice->bonous 						= $request->input("bonous");

    	$governoratePrice->status 						= $request->input("status");
		$governoratePrice->save();

		// Function Add Action In Activity Log By Samir
		addActivityLog('Edit Shipment Destinations #No ' . $governoratePrice->id , ' تعديل واجهة الشحن رقم  ' . $governoratePrice->id, 11); // Helper Function in App\Helpers.php

    Flash::warning(__('backend.Destination_Updated_successfully'));
		return redirect()->route('mngrAdmin.pickup_price_lists.index');
	}

	// Delete Governorate Price
	public function destroy($id){
		$governoratePrice 				= PickupPriceList::findOrFail($id);
		$OrderGovernoratePrice 		= Order::where('governorate_cost_id', $id)->first();
		if($OrderGovernoratePrice == null) { // If Not Have Orders Governorate Price Deleted
			$governoratePrice->delete();

			// Function Add Action In Activity Log By Samir
			addActivityLog('Delete Shipment Destinations #No ' . $governoratePrice->id , ' حذف واجهة الشحن رقم ' . $governoratePrice->id, 11); // Helper Function in App\Helpers.php

	    flash(__('backend.Destination_deleted_successfully'))->error();
			return redirect()->route('mngrAdmin.pickup_price_lists.index');
		} else { // If Haves Orders Governorate Price Not Deleted
			Flash::error(__('backend.Can_not_delete_Destination_because_of_Order_Dependency'));
			return redirect()->back();
		}
	}

	public function get_cost_sender_receiver($from_id,$to_id)
    {
		/** this fn used to git shipment price by from and to stations (sender to receiver ) */
    	$cost 		= PickupPriceList::where('start_station' , $from_id)->where('access_station' , $to_id)->first();
    	if ($cost) {
    		return $cost;
    	}else{
    		return "0";
    	}
    }/*get_cost_sender_receiver*/
}

<?php

namespace App\Console\Commands;

use App\Mail\SendReport;
use App\Models\Customer;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class DailyReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daily:report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send an email of daily report for corporate';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $customers = Customer::with(['orders' => function ($query) {
            $query->where(function ($query) {
                $query->whereIn('status', [0, 1, 2])
                    ->orWhere(function ($query) {
                        $query->where('status', 3)
                            ->whereDate('delivered_at', date('Y-m-d'));
                    })
                    ->orWhere(function ($query) {
                        $query->where('status', 4)
                            ->where('cancelled_by', 2)
                            ->whereDate('cancelled_at', date('Y-m-d'));
                    })
                    ->orWhere(function ($query) {
                        $query->where('status', 5)
                            ->whereDate('recalled_at', '<=', date('Y-m-d'))
                            ->where('warehouse_dropoff', '=', 0);
                    })
                    ->orWhere(function ($query) {
                        $query->where('status', 5)
                            ->whereDate('warehouse_dropoff_date', date('Y-m-d'))
                            ->where('warehouse_dropoff', '=', 1);
                    });
            })->with(['order_delay', 'comments', 'delivery_problems' => function ($query) {
                $query->with('problem');
            }]);
        }, 'corporate'])->whereHas('orders', function ($query) {
            $query->where(function ($query) {
                $query->whereIn('status', [0, 1, 2])
                    ->orWhere(function ($query) {
                        $query->where('status', 3)
                            ->whereDate('delivered_at', date('Y-m-d'));
                    })
                    ->orWhere(function ($query) {
                        $query->where('status', 4)
                            ->where('cancelled_by', 2)
                            ->whereDate('cancelled_at', date('Y-m-d'));
                    })
                    ->orWhere(function ($query) {
                        $query->where('status', 5)
                            ->whereDate('recalled_at', '<=', date('Y-m-d'))
                            ->where('warehouse_dropoff', '=', 0);
                    })
                    ->orWhere(function ($query) {
                        $query->where('status', 5)
                            ->whereDate('warehouse_dropoff_date', date('Y-m-d'))
                            ->where('warehouse_dropoff', '=', 1);
                    });
            });
        })->whereNotNull('corporate_id')->get();

        if (count($customers)) {
            foreach ($customers as $customer) {
                if ($customer->email) {
                    Mail::to($customer->email)->send(new SendReport(compact('customer')));
//                    Mail::to('iconxweb@gmail.com')->send(new SendReport(compact('customer')));
                }
            }
        }
    }
}

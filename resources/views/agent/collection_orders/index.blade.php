@extends('agent.layouts.app')
@section('css')
<title>{{__('backend.Order_Collection')}}</title>
@endsection
@section('header')
<div class="page-header clearfix">
  <h3>
    <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.Collection_Orders')}} @if(! empty($type) && $type ==1 ) {{__('backend.form')}} @else {{__('backend.excel')}} @endif
    @if(! empty($type) && $type ==1 )
    <a class="btn btn-success pull-right" href="{{ route('mngrAgent.orders.create') }}"><i class="glyphicon glyphicon-plus"></i> {{__('backend.add_Collection')}}</a>

    @else
    <a class="btn btn-success pull-right" href="{{ route('mngrAgent.collection_orders.create') }}"><i class="glyphicon glyphicon-plus"></i> {{__('backend.add_Collection')}}</a>
    <a class="btn btn-success pull-right" style="margin-right: 30px;" href="https://ufelix.com:3000/readdata" target="_blank"> {{__('backend.Fillter_Page')}}</a>
    <a class="btn btn-success pull-right" style="margin-right: 30px;" href="https://ufelix.com:3000/" target="_blank"> {{__('backend.Upload_After_Check')}}</a>
    @endif
</h3>
</div>
@endsection

@section('content')

@if(! empty($type) && $type ==1 )

<div class="row" style="margin-bottom:15px;">
  <div class="col-md-12">

    <div class="group-control col-md-3 col-sm-3">
      <select id="saved" name="saved" class="form-control">
        <option value="-1"> {{__('backend.Sort_By_Saved')}}</option>
        <option value="0">{{__('backend.Not_Saved')}}</option>
        <option value="1">{{__('backend.saved')}}</option>
      </select>
    </div>
  </div>
</div>
@endif
  <div class="list">
  @include('agent.collection_orders.table')
</div>
@endsection
@section('scripts')
<script type="text/javascript">

  $("#saved").on('change', function() {
  $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
    $.ajax({
        url: '{{URL::asset("/mngrAgent/collection_orders?type=1")}}'+'&&saved='+$("#saved").val(),
        type: 'get',
        success: function(data) {
            $('.list').html(data.view);
        },
        error: function(data) {
            console.log('Error:', data);
        }
    });
});


  $('#downloadStatus').on('click', function() {
    var _token = $('#_token');
    var id = $('#id');
    var status = $('#status');
    var file_name = $('#file_name');
    $.ajax({
      type:post,
      url:'mngrAgent\\collection_orders\\downloadStatus',
      data: {
        _token: _token.val(),
        id: id.val(),
        status: status.val(),
        file_name: file_name.val(),
      },
      success: function(Response){
        $('#downloadStatus').removeClass('btn-primary').addClass('btn-warning');
      },
      error: function() {
        alert("{{__('backend.Something_went_Wrong_please_try_again_later')}}");
      }
    });
  });
</script>
@endsection

@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.Vehicle_Types')}} - {{__('backend.edit')}}</title>
@endsection

@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i>  {{__('backend.Edit_Vehicle_Types')}} -
            @if(session()->has('lang') == 'en')
              {{$type->name_ar}}
            @else
              {{$type->name_en}}
            @endif
        </h3>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <form action="{{ url('mngrAdmin/vehicle/edit', $type->id) }}" method="POST" name='validateForm'>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                  <div class="form-group col-md-6 col-sm-6 @if($errors->has('name_en')) has-error @endif">
                    <label for="name-field">{{__('backend.name_en')}}: </label>
                    <input type="text" id="name_en-field" name="name_en" class="form-control" value="{{ $type->name_en }}"/>
                    @if($errors->has("name_en"))
                     <span class="help-block">{{ $errors->first("name_en") }}</span>
                    @endif
                  </div>
                  <div class="form-group col-md-6 col-sm-6 @if($errors->has('name_ar')) has-error @endif">
                    <label for="name-field">{{__('backend.name_ar')}}: </label>
                    <input type="text" id="name_ar-field" name="name_ar" class="form-control" value="{{ $type->name_ar }}"/>
                    @if($errors->has("name_ar"))
                     <span class="help-block">{{ $errors->first("name_ar") }}</span>
                    @endif
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-6 col-sm-6 @if($errors->has('price')) has-error @endif">
                    <label for="name-field">{{__('backend.price')}}: </label>
                    <input type="number" id="price-field" name="price" class="form-control" value="{{ $type->price }}"/>
                    @if($errors->has("price"))
                     <span class="help-block">{{ $errors->first("price") }}</span>
                    @endif
                  </div>
                  <div class="form-group col-md-6 col-sm-6 @if($errors->has('status')) has-error @endif">
                    <label for="status-field" style="display: block;">{{__('backend.active')}}</label>
                    <select id="status-field" name="status" class="form-control" value="{{ $type->status }}" >
                      <option value="0" <?php if ($type->status  == "0") { echo 'selected'; }  else  { echo '';} ?> >{{__('backend.not_activated')}}</option>
                      <option value="1" <?php if ($type->status  == "1") { echo 'selected'; }  else  { echo '';} ?> >{{__('backend.active')}}</option>
                    </select>
                    @if($errors->has("status"))
                     <span class="help-block">{{ $errors->first("status") }}</span>
                    @endif
                  </div>
                </div>
                <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
                    <button type="submit" class="btn btn-primary">{{__('backend.save_edits')}}</button>
                    <a class="btn btn-link pull-right" href="{{ url('mngrAdmin/vehicle') }}"><i class="glyphicon glyphicon-backward"></i>  {{__('backend.back')}}</a>
                </div>
            </form>

        </div>
    </div>
@endsection

@section('scripts')
   <script type="text/javascript">
  $(function() {
    $("form[name='validateForm']").validate({
      // Specify validation rules
      rules: {
        name_en: {
          required: true,
        },
        name_ar: {
          required: true,
        },
        price: {
          required: true,
          number: true,
        },
        status: {
          required: true,
        },
      },
      // Specify validation error messages
      messages: {
        name_en: {
          required: "{{__('backend.Please_Enter_Type_English_Name')}}",
        },
        name_ar: {
          required: "{{__('backend.Please_Enter_Type_Arabic_Name')}}",
        },
        price: {
          required: "{{__('backend.Please_Enter_Vehicel_Price')}}",
          number: "{{__('backend.Please_enter_avalid_price')}}",
        },
        status: {
          required: "{{__('backend.Please_Chosse_The_Status')}}",
        },
      },

      errorPlacement: function(error, element) {
          $(element).parents('.form-group').append(error)
      },
      highlight: function (element) {
          $(element).parent().addClass('error')
      },
      unhighlight: function (element) {
          $(element).parent().removeClass('error')
      },
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function(form) {
          form.submit();
      }
    });
  })
</script>
@endsection

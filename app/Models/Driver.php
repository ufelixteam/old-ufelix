<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Driver extends Authenticatable
{
    protected $hidden = ['password', 'created_at', 'updated_at', 'address', 'latitude', 'longitude', 'criminal_record_image_front', 'sms_code', 'driving_licence_number', 'driving_licence_image_front', 'driving_licence_image_back', 'driving_licence_expired_date', 'national_id_number', 'national_id_image_front', 'national_id_image_back', 'national_id_expired_date', 'notes', 'version', 'platform', 'device_id', 'on_session'];
    protected $fillable = ['name', 'email', 'password', 'mobile', 'sms_code', 'version_id', 'device_id'];

    public function getLatitudeAttribute($value)
    {
        if ($value == null || $value == "") {
            $value = 0;
        }
        return $value;
    }

    public function getLangitudeAttribute($value)
    {
        if ($value == null || $value == "") {
            $value = 0;
        }
        return $value;
    }

    public function getDeviceAttribute()
    {
        $lastDevice = Device::where('object_type', 2)->where('object_id', $this->id)->first();
        return $lastDevice;
    }

    public function getDeviceActiveAttribute()
    {
        $lastDevice = Device::where('object_type', 2)->where('object_id', $this->id)->where('status', 1)->first();
        return $lastDevice;
    }

    public function getActiveSpanAttribute($value)
    {
        /*
        * is_active = 0 :: Not Active
        * is_active = 1 :: Active
        */
        if ($this->is_active == 1) {
            $value = "<span class='badge badge-pill label-success'>" . __('backend.active') . "</span>";
        } else {
            $value = "<span class='badge badge-pill label-danger'>" . __('backend.not_activated') . "</span>";
        }
        return $value;
    }

    public function getTransferMethodSpanAttribute()
    {

        if ($this->transfer_method == 1) {
            $value = __('backend.bank');
        } elseif ($this->transfer_method == 2) {
            $value = __('backend.mobicash');
        } elseif ($this->transfer_method == 3) {
            $value = __('backend.cash_with_captain');
        } elseif ($this->transfer_method == 4) {
            $value = __('backend.cash_in_office');
        }

        return $value;
    }

    public function getActivesSpanAttribute($value)
    {
        /*
        * is_active = 0 :: Not Active
        * is_active = 1 :: Active
        */
        if ($this->is_active != 1) {
            $value = "<span class='badge badge-pill label-danger'><i class='fa fa-times' aria-hidden='true'></i></span>";
        } else {
            $value = "<span class='badge badge-pill label-success'><i class='fa fa-check' aria-hidden='true'></i></span>";
        }
        return $value;
    }

    public function getBlockSpanAttribute($value)
    {
        /*
        * is_block = 0 :: Not Blocked
        * is_block = 1 :: Blocked
        */
        if ($this->is_block == 1) {
            $value = "<span class='badge badge-pill label-danger'>" . __('backend.blocked') . "</span>";
        } else {
            $value = "<span class='badge badge-pill label-info'>" . __('backend.not_blocked') . "</span>";
        }
        return $value;
    }

    public function getBlockedSpanAttribute($value)
    {
        /*
        * is_block = 0 :: Not Blocked
        * is_block = 1 :: Blocked
        */
        if ($this->is_block == 1) {
            $value = "<span class='badge badge-pill label-danger'><i class='fa fa-times' aria-hidden='true'></i></span>";
        } else {
            $value = "<span class='badge badge-pill label-success'><i class='fa fa-check' aria-hidden='true'></i></span>";
        }
        return $value;
    }

    public function getBlockTxtAttribute($value)
    {
        /*
        * is_block = 0 :: Not Blocked
        * is_block = 1 :: Blocked
        */
        if ($this->is_block == 0 || $this->is_block != 1) {
            $value = __('backend.blocked');
        } else if ($this->is_block == 1) {
            $value = __('backend.not_blocked');
        } else {
            $value = "";
        }
        return $value;
    }

    public function getActiveTxtAttribute($value)
    {
        /*
        * is_active = 0 :: Not Active
        * is_active = 1 :: Active
        */
        if ($this->is_active == 0 || $this->is_active != 1) {
            $value = __('backend.active');
        } else if ($this->is_active == 1) {
            $value = __('backend.not_activated');
        } else {
            $value = "";
        }
        return $value;
    }

    public function getStatusSpanAttribute($value)
    {
        /*
        * online = 0 :: Ofline
        * online = 1 :: Online
        */
        $lastDevice = Device::where('object_type', 2)->where('object_id', $this->id)->where('status', 1)->first();
        //dd($lastDevice);die();
        if (!empty($lastDevice) && $lastDevice->status == 1) {
            $value = "<span class='badge badge-pill label-success'>" . __('backend.online') . "</span>";
        } else {
            $value = "<span class='badge badge-pill label-danger'>" . __('backend.Offline') . "</span>";
        }
        return $value;
    }

    public function getStatusRequestSpanAttribute($value)
    {
        $lastDevice = Device::where('object_type', 2)->where('object_id', $this->id)->where('status', 1)->first();


        /*
        * online = 0 :: Ofline
        * online = 1 :: Online
        */
        $lastDevice = Device::where('object_type', 2)->where('object_id', $this->id)
            ->where('status', 1)->where('request_status', 1)->first();

        if (!empty($lastDevice)) {
            $value = "<span class=' text-success'><i class='fa fa-check' aria-hidden='true'></i></span>";

        } else {
            $value = "<span class=' text-danger'><i class='fa fa-times' aria-hidden='true'></i></span>";

        }
        return $value;
    }

    public function getImageAttribute($value)
    {
        if ($value != "") {
            $value = url("/") . "/api_uploades/driver/profile/" . $value;
        } else {
            $value = "";
        }
        return $value;
    }

    public function getCriminalRecordImageFrontAttribute($value)
    {
        if ($value != "") {
            $value = url("/") . "/api_uploades/driver/cre_record/" . $value;
        } else {
            $value = "";
        }
        return $value;
    }

    public function getDrivingLicenceImageFrontAttribute($value)
    {
        if ($value != "") {
            $value = url("/") . "/api_uploades/driver/driver_licence/" . $value;
        } else {
            $value = "";
        }
        return $value;
    }

    public function getDrivingLicenceImageBackAttribute($value)
    {
        if ($value != "") {
            $value = url("/") . "/api_uploades/driver/driver_licence/" . $value;
        } else {
            $value = "";
        }
        return $value;
    }

    public function getNationalIdImageFrontAttribute($value)
    {
        if ($value != "") {
            $value = url("/") . "/api_uploades/driver/national_id_front/" . $value;
        } else {
            $value = "";
        }
        return $value;
    }

    public function getNationalIdImageBackAttribute($value)
    {
        if ($value != "") {
            $value = url("/") . "/api_uploades/driver/national_id_back/" . $value;
        } else {
            $value = "";
        }
        return $value;
    }

    public function truck()
    {
        return $this->hasOne('App\Models\Truck', 'driver_id');
    }

    public function agent()
    {
        return $this->belongsTo('App\Models\Agent', 'agent_id');
    }

    public function shipping_types()
    {
        return $this->hasMany('App\Models\ShippingType', 'driver_id');

    }

    #hash password
    public function setPasswordAttribute($pass)
    {
        $this->attributes['password'] = bcrypt($pass);
    }

    public function wallet()
    {
        return $this->hasOne('App\Models\Wallet');
    }

    public function store()
    {
        return $this->belongsTo('App\Models\Store', 'store_id');
    }

    public function getHasOrdersAttribute()
    {
        return AcceptedOrder::where('driver_id', $this->id)->count();
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [
            'type' => 'driver'
        ];
    }

    public function government()
    {
        return $this->belongsTo('App\Models\Governorate', 'government_id');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City', 'city_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country', 'country_id');
    }

    public function manager()
    {
        return $this->belongsTo('App\Models\Driver', 'manager_id');
    }

    public function devices()
    {
        return $this->hasMany('App\Models\Device', 'object_id', 'id')
            ->where('object_type', 2);
    }

    public function trucks()
    {
        return $this->hasMany('App\Models\Truck', 'driver_id', 'id');
    }

}

<!-- Start Clients -->
<section id="clients" class="clients-x">
	<div class="container">
    	<!-- Start Title -->
    	<div class="col-xs-12">
        	<div class="title">
            	<h3><?php echo app('translator')->getFromJson('website.Customers'); ?></h3>
            </div>
        </div>
        <!-- End Title -->
        <div class="col-xs-12">
    		<div class="clients-slider">
                <!-- Start Item -->
                <?php if(! empty($corporates) && count($corporates) > 0): ?>
                    <?php $__currentLoopData = $corporates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $corporate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="item">
                            <div class="img">
                                <img src="<?php echo e($corporate->logo != '' ? $corporate->logo : asset('/website/images/c1.jpg')); ?>" alt="<?php echo e($corporate->name); ?>" />
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
                <!-- End -->
            </div>
        </div>
    </div>
</section>
<!-- End Clients --><?php /**PATH /home/www/ufelix/ufelix/resources/views/frontend/home/customers.blade.php ENDPATH**/ ?>
<div class="row">
  <div class="col-md-12 col-xs-12 col-sm-12">
    @if(! empty($targets) && count($targets) > 0)
        <table id="theTable" class="table table-condensed table-striped text-center">
          <thead>
              <tr>
                <th>#{{__('backend.id')}}</th>
                <th>{{__('backend.target_from')}}</th>
                <th>{{__('backend.target_to')}}</th>
                <th>{{__('backend.discount_percentage')}}</th>
                <th>{{__('backend.corporate')}}</th>
                <td  class="text-right"></td>
              </tr>
          </thead>
          <tbody>
              @foreach($targets as $target)
                  <tr>
                      <td>{{$target->id}}</td>
                      <td>{{$target->corporate_target_from}}</td>
                      <td>{{$target->corporate_target_to}}</td>
                      <td>{{$target->corporate_discount}} %</td>
                      <td>
                        @if( \App\Models\Corporate::where('id', $target->corporate_id)->exists() )
                          {{\App\Models\Corporate::where('id', $target->corporate_id)->value('name')}}
                        @else
                          {{__('backend.not_found')}}
                        @endif
                      </td>
                      <td>
                      <a class="btn btn-xs btn-info" data-toggle="modal" data-target="#editTarget_{{$target->id}}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                      <form action="{{ route('mngrAdmin.CorporateTarget.destroy', $target->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
                          <input type="hidden" name="_method" value="DELETE">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <div class="btn-group pull-right" role="group" aria-label="...">
                              <button type="submit" class="btn btn-danger btn-xs"> {{__('backend.delete')}} <i class="glyphicon glyphicon-trash"></i></button>
                          </div>
                      </form>
                      </td>
                  </tr>

                  <div class="row">
                    <!-- Modal -->
                    <div class="modal fade" id="editTarget_{{$target->id}}" role="dialog">
                      <div class="modal-dialog" style="width: 80%;">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title text-center"> {{__('backend.edit_target')}}</h4>
                          </div>

                          <form action="{{ route('mngrAdmin.CorporateTarget.update', $target->id) }}" method="POST" name="validateForm">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="_method" value="PUT">
                            <div class="modal-body">
                              <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                  <div class="row">
                                    <div class="form-main">
                                      <div class="form-group col-md-3 col-sm-3 @if($errors->has('corporate_target_from')) has-error @endif">
                                          <label for="corporate_target_from_field">{{__('backend.target_from')}}</label>
                                          <input type="number" min="1" id="corporate_target_from_field" name="corporate_target_from" class="form-control" value="{{$target->corporate_target_from}}" />
                                         @if($errors->has("corporate_target_from"))
                                          <span class="help-block">{{ $errors->first("corporate_target_from") }}</span>
                                         @endif
                                      </div>
                                      <div class="form-group col-md-3 col-sm-3 @if($errors->has('corporate_target_to')) has-error @endif">
                                          <label for="corporate_target_to_field">{{__('backend.target_to')}}</label>
                                          <input type="number" min="1" id="corporate_target_to_field" name="corporate_target_to" class="form-control" value="{{$target->corporate_target_to}}" />
                                         @if($errors->has("corporate_target_to"))
                                          <span class="help-block">{{ $errors->first("corporate_target_to") }}</span>
                                         @endif
                                      </div>
                                      <div class="form-group col-md-3 col-sm-3 @if($errors->has('corporate_discount')) has-error @endif">
                                        <label for="corporate_discount_field">{{__('backend.discount_percentage')}}</label>
                                          <div class='input-group'>
                                                <input type="text" id="corporate_discount_field" name="corporate_discount" class="form-control" value="{{$target->corporate_discount}}"/>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-percent"></i>
                                                </div>
                                           </div>
                                           @if($errors->has("corporate_discount"))
                                            <span class="help-block">{{ $errors->first("corporate_discount") }}</span>
                                           @endif
                                      </div>
                                      <div class="form-group col-md-3 col-sm-3 @if($errors->has('corporate_id')) has-error @endif">
                                        <label for="type-field">{{__('backend.choose_corporate')}}: </label>
                                        <select id="type-field" name="corporate_id" id="corporate_id" class="form-control">
                                          @foreach($corporates as $corporate)
                                            <option value="{{ $corporate->id }}" {{$target->corporate_id == $corporate->id ? 'selected' : ''}} >{{ $corporate->name }}</option>
                                          @endforeach
                                        </select>
                                        @if($errors->has("corporate_id"))
                                          <span class="help-block">{{ $errors->first("corporate_id") }}</span>
                                        @endif
                                      </div>
                                    </div>
                                  </div>

                                  <div class="row">
                                    <div class="form-group col-md-12 col-sm-6 @if($errors->has('report')) has-error @endif">
                                      <label for="report-field">{{__('backend.report')}}: </label>
                                      <textarea class="form-control" id="report-field" rows="4" name="report">{{$target->report}}</textarea>
                                      @if($errors->has("report"))
                                        <span class="help-block">{{ $errors->first("report") }}</span>
                                      @endif
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div class="modal-footer">
                              <button type="submit" class="btn btn-primary">{{__('backend.create')}}</button>
                            </div>
                          </form>

                        </div>
                      </div>
                    </div>
                  </div>

              @endforeach
          </tbody>
        </table>
        {!! $targets->appends($_GET)->links() !!}
    @else
      <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
    @endif
  </div>
</div>

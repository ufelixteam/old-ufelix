'@extends('backend.layouts.app')
@section('css')
<title>{{__('backend.stores')}} - {{__('backend.view')}}</title>
@endsection
@section('header')
<div class="page-header">
        <h3>
            {{$store->name}} {{__('backend.show_store')}}
            {!! $store->status_span !!}
        </h3>

        <br> <strong style="color: #607d8b">{{__('backend.pickup_default')}} :: </strong> {!! $store->pickup_default_span !!}

        <form action="{{ route('mngrAdmin.stores.destroy', $store->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
              @if(permission('editStore')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <a class="btn btn-warning btn-group" role="group" href="{{ route('mngrAdmin.stores.edit', $store->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
              @endif

              @if(permission('deleteStore')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <button type="submit" class="btn btn-danger"> {{__('backend.delete')}} <i class="glyphicon glyphicon-trash"></i></button>
              @endif
                  <a href="{{ route('mngrAdmin.stores.download_pdf', $store->id) }}"
                     class="btn btn-primary btn-group" role="group"
                  >{{__('backend.print')}}</a>
                  <a href="{{ URL::asset('mngrAdmin/warehouse_excel?store_id='.$store->id) }}"
                     class="btn btn-success btn-group" role="group"
                  >{{__('backend.excel')}}</a>
            </div>
        </form>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group col-md-3">
                     <label for="responsible">{{__('backend.responsible')}}</label>
                     <p class="form-control-static">{{!empty($selectedResponsible) ? $selectedResponsible->name : '-'}}</p>
                </div>

                <div class="form-group col-md-3">
                     <label for="mobile">{{__('backend.responsible_phone')}}</label>
                     <p class="form-control-static">{{$store->mobile}}</p>
                </div>
                <div class="form-group col-md-3">
                    <label for="governorate_id">{{__('backend.government')}}</label>
                    <p class="form-control-static">{{ ! empty($store->governorate) ? $store->governorate->name_en : '' }}</p>
                </div>

                <div class="form-group col-md-3">
                    <label for="no_of_stocks">{{__('backend.no_of_stocks')}}</label>
                    <p class="form-control-static">{{$store->stocks_count}}</p>
                </div>

            </form>
            <div class="col-md-12">
                <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.stores.index') }}"><i class="glyphicon glyphicon-backward"></i>  {{__('backend.back')}}</a>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            @if($store->stocks->count())
                <div class="col-sm-12"
                    style="border: 2px solid #fdb801;color: #fff;margin-bottom: 20px;margin-top: 20px;background-color: #fdb801;">
                    <h4>
                        {{__('backend.stocks')}}
                    </h4>
                </div>
                <table class="table table-condensed table-striped">
                    <thead>
                    <tr>
                        <th class="text-center">{{__('backend.no')}}</th>
                        <th class="text-center">{{__('backend.title')}}</th>
                        <th class="text-center">{{__('backend.store_date')}}</th>
                        <th class="text-center">{{__('backend.corporate')}}</th>
                        <th class="text-center">{{__('backend.dropped_by')}}</th>
                        <th class="text-center">{{__('backend.description')}}</th>
                        <th class="text-center">{{__('backend.actual_piece')}}</th>
                        <th class="text-center">{{__('backend.used_piece')}}</th>
                        <th class="text-center">{{__('backend.piece_status')}}</th>
                        <th></th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($store->stocks as $stock)
                        <tr>
                            <td class="text-center">{{$loop->iteration}}</td>
                            <td class="text-center">{{$stock->title}}</td>
                            <td class="text-center">{{isset($stock->created_at) ? $stock->created_at : '-'}}</td>
                            <td class="text-center">{{isset($stock->corporate->name) ? $stock->corporate->name : '-'}}</td>
                            <td class="text-center">{{isset($stock->dropped_by) ? $stock->dropped_by : '-'}}</td>
                            <td class="text-center">{{isset($stock->description) ? $stock->description : '-'}}</td>
                            <td class="text-center">
                                <input type="text" class="form-control actual_pieces"  name="actual_pieces"
                                       value="{{$stock->qty}}" id="actual_pieces_{{$stock->id}}"
                                       data-id="{{$stock->id}}"
                                ></td>
                            <td class="text-center">{{$stock->used_qty}}</td>
                            <td class="text-center">
                                @if($stock->qty == $stock->used_qty)
                                    <span class='badge badge-pill label-success' id="status_used_{{$stock->id}}" >{{__('backend.used')}}</span>
                                @else
                                    <span class='badge badge-pill label-danger' id="not_used_{{$stock->id}}">{{__('backend.not_used')}}</span>
                                @endif
                            </td>
                            <td class="text-right">
                            @if(permission('editStocks')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <a class="btn btn-xs btn-success save_actual_pieces disabled" href="#"
                                   id="save_actual_pieces_{{$stock->id}}"
                                   data-id="{{$stock->id}}"
                                   data-value="{{$stock->used_qty}}"><i class="glyphicon glyphicon-edit">
                                    </i> {{__('backend.save')}}</a>
                                @endif
                                @if(permission('showStocks'))
                                    <a href="{{ route('mngrAdmin.stocks.show', $stock->id) }}"
                                       class="btn btn-xs btn-primary"
                                    ><i class="glyphicon glyphicon-eye-open"></i>{{__('backend.view')}}</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            @endif
        </div>

        </div>

@endsection
@section('scripts')
    <script>
        $('.actual_pieces').on('input', function () {
            $('#save_actual_pieces_' + $(this).attr('data-id')).removeClass('disabled');
        });
        $('.save_actual_pieces').on('click', function (e) {
            e.preventDefault();
            var self = $(this);
            let id = $(this).attr('data-id');
            let used_pieces = $(this).attr('data-value')
            let actual_pieces =  $('#actual_pieces_' + id).val();
            $.ajax({
                url: '{{URL::asset("/mngrAdmin/stocks/save_actual_pieces")}}',
                type: 'post',
                data: {id: id, actual_pieces: actual_pieces, _token: "{{csrf_token()}}"},
                success: function (data) {
                self.addClass('disabled');
                // if status change from used to not used then change class /id /value to not used span
                // if status change from not used to used then change class /id /value to used span
                if (used_pieces === actual_pieces) {
                    $('#not_used_' + id).attr({'class': 'badge badge-pill label-success', 'id': 'status_used_'+id}).text("{{__('backend.used')}}");
                } else {
                    $('#status_used_'+id).attr({'class': 'badge badge-pill label-danger', 'id': 'not_used_'+id}).text("{{__('backend.not_used')}}");
                }
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        })
    </script>
@endsection

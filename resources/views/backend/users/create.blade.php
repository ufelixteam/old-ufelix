@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.admins')}} - {{__('backend.create')}}</title>
@endsection

@section('header')
<div class="page-header">
  <h3><i class="glyphicon glyphicon-plus"></i> {{__('backend.add_admin')}}</h3>
</div>
@endsection

@section('content')

  <div class="row">
        <div class="col-md-12">
            <form method="POST" action="{{ route('mngrAdmin.users.store') }}" name='validateForm'>
                {{ csrf_field() }}

                <div class="row">
                  <div class="form-group col-md-6 col-sm-6{{ $errors->has('name') ? ' has-error' : '' }}">
                      <label for="name" class="control-label"> {{__('backend.name')}}</label>
                      <input id="name" type="text" class="form-control " name="name" value="{{ old('name') }}"   oninvalid="this.setCustomValidity('Please Enter  Name of User')" oninput="setCustomValidity('')" required="required"/>
                      @if ($errors->has('name'))
                          <span class="help-block">
                              <strong>{{ $errors->first('name') }}</strong>
                          </span>
                      @endif
                  </div>
                  <div class="form-group col-md-6 col-sm-6{{ $errors->has('email') ? ' has-error' : '' }}">
                      <label for="email" class="control-label"> {{__('backend.email')}}</label>
                      <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" >
                      @if ($errors->has('email'))
                          <span class="help-block">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                      @endif
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-6 col-sm-6{{ $errors->has('mobile') ? ' has-error' : '' }}">
                      <label for="mobile" class="control-label">{{__('backend.mobile')}}</label>
                      <input id="mobile" type="text" class="form-control" name="mobile" value="{{ old('mobile') }}"   oninvalid="this.setCustomValidity('Please Enter Mobile')" oninput="setCustomValidity('')" required="required"/>
                      @if ($errors->has('mobile'))
                          <span class="help-block">
                              <strong>{{ $errors->first('mobile') }}</strong>
                          </span>
                      @endif
                  </div>
                  <div class="form-group col-md-6 col-sm-6{{ $errors->has('role_admin_id') ? ' has-error' : '' }}">
                      <label for="role_admin_id" class="control-label"> {{__('backend.choose_role')}} </label>
                      <select class="form-control" name="role_admin_id">
                        <option value="">{{__('backend.choose_role')}}</option>
                        @foreach($role_admins as $role)
                          <option value="{{$role->id}}" {{ old("role_admin_id") == "$role->id" ? 'selected' : '' }}>{{$role->name}}</option>
                        @endforeach()
                      </select>
                      @if ($errors->has('role_admin_id'))
                          <span class="help-block">
                              <strong>{{ $errors->first('role_admin_id') }}</strong>
                          </span>
                      @endif
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-6 col-sm-6{{ $errors->has('password') ? ' has-error' : '' }}">
                      <label for="password" class="control-label"> {{__('backend.password')}}</label>
                      <input id="password-field" type="password" class="form-control" name="password"  oninvalid="this.setCustomValidity('Please Enter Password')" oninput="setCustomValidity('')" required="required">
                      @if ($errors->has('password'))
                          <span class="help-block">
                              <strong>{{ $errors->first('password') }}</strong>
                          </span>
                      @endif
                  </div>
                  <div class="form-group col-md-6 col-sm-6">
                      <label for="password-confirm" class="control-label"> {{__('backend.confirm_password')}} </label>
                      <input id="password-confirm" type="password" class="form-control" name="password_confirmation"  oninvalid="this.setCustomValidity('Please Enter Confirm Password')" oninput="setCustomValidity('')" required="required">
                      @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                      @endif
                  </div>
                </div>
                <div class="well well-sm col-md-12 col-sm-12">
                  <button type="submit" class="btn btn-primary">{{__('backend.save_edits')}}</button>
                  <a class="btn btn-link pull-right" href="{{ url()->previous() }}"><i class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
  <script type="text/javascript">
  $(function() {
    $("form[name='validateForm']").validate({
      // Specify validation rules
      rules: {
          name: {
              required: true,
            },
          email: {
              required: true,
              email: true,
            },
          mobile: {
              required: true,
              number: true,
              rangelength:[11,11],
            },
          role_admin_id: {
              required: true,
            },
          password: {
              required: true,
              minlength: 5
          },
          password_confirmation: {
            	required: true,
              equalTo: '#password-field',
              minlength: 5
          },
      },
      // Specify validation error messages
      messages: {
          name: {
              required: "{{__('backend.Please_Enter_Admin_Name')}}",
            },
          email: {
               required: "{{__('backend.Please_Enter_Admin_Email')}}",
               email: "{{__('backend.Please_Enter_User_Correct_E-mail')}}",
             },
          mobile: {
                required: "{{__('backend.Please_Enter_Mobile_Number')}}",
                number: "{{__('backend.Please_Enter_Correct_Number')}}",
                rangelength:"{{__('backend.Mobile_Must_Be11_Digits')}}",
             },
          role_admin_id: {
                required: "{{__('backend.Please_Choose_Admin_Role')}}",
            },
          password_confirmation: "{{__('backend.Confirm_password_is_wrong')}}",
          password: {
              required: "{{__('backend.Please_Enter_User_Password')}}",
              minlength: "{{__('backend.Password_must_be_more_than_5_character')}}"
          },
      },

       errorPlacement: function(error, element) {
          $(element).parents('.form-group').append(error)
      },

      highlight: function (element) {
          $(element).parent().addClass('error')
      },
      unhighlight: function (element) {
          $(element).parent().removeClass('error')
      },
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function(form) {
          form.submit();
      }
    });
  })
</script>
@endsection

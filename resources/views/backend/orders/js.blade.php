<script>
    $(document).ready(function () {
        var ajaxReq = 'ToCancelPrevReq'; // you can have it's value anything you like

        var s_state_id = $("#s_state_id").attr('data-id');
        var firstLoad = false;

        // if ($("#payment_method_id-field").val() == "1" || $("#payment_method_id-field").val() == "2" || $("#payment_method_id-field").val() == "7" || $("#payment_method_id-field").val() == "5") {
        //
        //     $("#order_price-field").val(0);
        //     $("#order_price-field").attr('readonly', 'readonly');
        // } else {
        //     $("#order_price-field").prop("readonly", false);
        // }

        /* */

        function roughScale(x, value = 0) {
            const parsed = parseInt(x);
            if (isNaN(parsed)) {
                return value;
            }
            return parsed;
        }

        function change_total_price() {
            delivery_fees = roughScale($("#delivery_price-field").val());
            order_price = roughScale($("#order_price-field").val());
            payment_method = $("#payment_method_id-field").val();
            if (payment_method == "1") {
                $("#total_price").text(order_price ? order_price : delivery_fees);
            } else if (payment_method == "2") {
                $("#total_price").text((delivery_fees + order_price));
            } else {
                $("#total_price").text(0);
            }
        }

        $("#payment_method_id-field").on('change', function () {
            change_total_price();
        });

        $("#order_price-field, #delivery_price-field").bind('keyup mouseup change', function () {
            change_total_price();
        });

        /* reset Form */
        $('#reset-btn').on('click', function () {
            $.ajax({
                url: "{{ url('/mngrAdmin/tmporders_reset_view')}}/" + $(this).attr('data-value'),
                type: 'get',
                success: function (data) {
                    if (data == "false") {
                        alert(' Something error')
                    } else {


                        $('#create-form').html(data.view);

                    }
                },
                error: function (data) {

                    console.log('Error:', data);
                }
            });

        });
        /*view-order*/

        @if(!isset($from_update))
        $(".list").on('click', '.view-order', function () {
            ajaxReq = $.ajax({
                url: "{{ url('/mngrAdmin/tmporders_update_view')}}/" + $(this).attr('data-value'),
                type: 'get',
                beforeSend: function () {
                    if (ajaxReq != 'ToCancelPrevReq' && ajaxReq.readyState < 4) {
                        ajaxReq.abort();
                    }
                },
                success: function (data) {
                    $('#create-form').html(data.view);

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    if (thrownError == 'abort' || thrownError == 'undefined') return;
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });

        });
        @endif

        /*delete-order*/
        $(".list").on('click', '.delete-order', function () {
            var id = $(this).attr('data-value');
            $.confirm({
                title: '',
                theme: 'modern',
                class: 'danger',
                content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Confirm Delete</span></div><div style="font-weight:bold;"><p>Are You want To delete This Order ?</p></div>',
                buttons: {
                    confirm: {
                        text: 'Delete',
                        btnClass: 'btn-blue',
                        action: function () {

                            $.ajax({
                                url: "{{ url('/mngrAdmin/delete_tmporder')}}",
                                type: 'post',
                                data: {'_token': "{{csrf_token()}}", 'id': id},
                                success: function (data) {

                                    jQuery('.alert-danger').html('');

                                    jQuery('.alert-danger').show();

                                    if (data != 'false') {
                                        jQuery('.alert-danger').append('<p>' + data + '</p>');

                                        setTimeout(function () {
                                            location.reload();
                                        }, 5000);
                                    } else {
                                        jQuery('.alert-danger').append('<p>Something Error</p>');

                                    }
                                    window.scrollTo({top: 0, behavior: 'smooth'});


                                },
                                error: function (data) {

                                    console.log('Error:', data);
                                }
                            });
                        }
                    },
                    cancel: {
                        text: 'Cancel',
                        action: function () {
                        }
                    }
                }
            });
        });
        $("#order-btn").on('click', function () {
            /*send to temp*/
            $.ajax({
                url: "{{ url('/mngrAdmin/tmporders_create')}}",
                type: 'post',
                data: $("#order-form").serialize(),
                success: function (data) {
                    if (data.errors) {
                        jQuery('.alert-danger').html('');
                        jQuery.each(data.errors, function (key, value) {
                            jQuery('.alert-danger').show();

                            jQuery('.alert-danger').append('<p>' + value + '</p>');
                        });
                        window.scrollTo({top: 0, behavior: 'smooth'});
                    } else {
                        jQuery('.alert-danger').hide();
                        $('#collection_id').val(data.collection_id);
                        if (data.first_one == "1") {
                            /*create-btn*/
                            window.history.replaceState(null, null, "/mngrAdmin/orders/create?collection=" + data.collection_id);
                            $("#saved_collection_id").val(data.collection_id);
                            jQuery('#saved').show();
                        }
                        $('.list').html(data.view);
                        // //* change collection id*/
                        $('.create-form').html(data.view_order);
                        $('.js-form').html(data.js);
                        // $("#order-form")[0].reset();
                        // $("#receiver_inputs").find('input[type=text]').val('');
                        // $("#receiver_inputs").find('input[type=number]').val(0);
                        $("#receiver_inputs").find("#reciever_name-field").val('');
                        $("#receiver_inputs").find("#receiver_mobile-field").val('');
                        $("#receiver_inputs").find("#receiver_phone-field").val('');
                        $("#receiver_inputs").find('select').each(function () {
                            $(this).prop('selectedIndex', 0);
                            $(this).change();
                        });
                        $("#receiver_inputs").find("#r_state_id").html('');

                        $("#order_inputs").find('input[type=text]').val('');
                        $("#order_inputs").find('input[type=number]').val(0);
                        $("#order_inputs").find('select').each(function () {
                            $(this).prop('selectedIndex', 0);
                            $(this).change();
                        });
                    }
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });
        $(".s_government_id").on('change', function () {
            $("#no_fees").addClass('hidden');
            $('.s_state_id').html('');
            if ($(this).val()) {
                $.ajax({
                    url: "{{ url('/get_cities')}}" + "/" + $(this).val(),
                    type: 'get',
                    data: {},
                    success: function (data) {
                        $('.s_state_id').html('');
                        $.each(data, function (i, content) {
                            $('.s_state_id').append($("<option></option>").attr("value", content.id).text(content.city_name));
                        });
                        if (s_state_id) {
                            $('#s_state_id').val(s_state_id);
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
            var receiver = $(".r_government_id").val();
            if (receiver != null) {
                if ($(this).val() && receiver) {
                    $.ajax({
                        url: "{{ url('/mngrAdmin/get_cost')}}" + "/" + $(this).val() + "/" + receiver + '?customer_id=' + $("#customer_id").val(),
                        type: 'get',
                        data: {},
                        success: function (data) {
                            if (!firstLoad) {
                                if (data == 0 || data == 'not_exist') {
                                    $("#delivery_price-field").css("border-color", "red");
                                    if (data == 'not_exist') {
                                        $("#no_fees").removeClass('hidden');
                                        $(".r_government_id").val('');
                                    }
                                } else {
                                    $("#delivery_price-field").css("border-color", "#d2d6de");
                                }
                                $('#delivery_price-field').val((data == 'not_exist' ? 0 : data)).change();
                            } else {
                                firstLoad = false;
                            }
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }
            }
        });

        $(".r_government_id").on('change', function () {
            $("#no_fees").addClass('hidden');
            $('.r_state_id').html('');
            if ($(this).val()) {

                $.ajax({
                    url: "{{ url('/get_cities')}}" + "/" + $(this).val(),
                    type: 'get',
                    data: {},
                    success: function (data) {

                        $.each(data, function (i, content) {
                            $('.r_state_id').append($("<option></option>").attr("value", content.id).text(content.city_name));
                        });
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
            var sender = $(".s_government_id").val();
            if ($(this).val() && sender) {
                $.ajax({
                    url: "{{ url('/mngrAdmin/get_cost')}}" + "/" + sender + "/" + $(this).val() + '?customer_id=' + $("#customer_id").val(),
                    type: 'get',
                    data: {},
                    success: function (data) {
                        if (!firstLoad) {
                            if (data == 0 || data == 'not_exist') {
                                $("#delivery_price-field").css("border-color", "red");
                                if (data == 'not_exist') {
                                    $("#no_fees").removeClass('hidden');
                                    $(".r_government_id").val('');
                                }
                            } else {
                                $("#delivery_price-field").css("border-color", "#d2d6de");
                            }
                            $('#delivery_price-field').val((data == 'not_exist' ? 0 : data)).change();
                        } else {
                            firstLoad = false;
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }

        });

        $("#store_id-field").on('change', function () {
            $('#stock_id-field').html('<option value="0" >Choose Stock</option>');
            get_stocks();
        });

        /*get_customers*/
        $("#corparate_id").on('change', function () {
            $('#stock_id-field').html('<option value="0" >Choose Stock</option>');

            $.ajax({
                //url: "{{ url('/mngrAdmin/get_customers/')}}"+"/"+$(this).val(),
                url: "{{ url('/mngrAdmin/corporates/order_details/')}}" + "/" + $(this).val(),
                type: 'get',
                data: {},
                success: function (data) {
                    $('#customer_id').html('');
                    $('#sender_name-field').val('');
                    $('#sender_mobile-field').val('');
                    $('#s_government_id').val('').change();
                    $('#sender_address').val('');
                    $('#order_type_id').val('');
                    s_state_id = '';

                    $.each(data.customers, function (i, content) {
                        $('#customer_id').append($("<option></option>").attr("value", content.id).text(content.name));
                        $('#customer_id').attr('data-name', content.name);
                        $('#customer_id').attr('data-mobile', content.mobile);
                    });
                    if (data.customers.length > 0) {
                        $('#sender_name-field').val(data.customers[0].name);
                        $('#sender_mobile-field').val(data.customers[0].mobile);
                        s_state_id = data.customers[0].city_id;
                        $('#s_government_id').val(data.customers[0].government_id).change();
                        $('#sender_address').val(data.customers[0].address);
                        $('#sender_latitude').val(data.customers[0].latitude);
                        $('#sender_longitude').val(data.customers[0].longitude);
                    }

                    // s_state_id = data.city_id;
                    // $('#s_government_id').val(data.government_id).change();
                    // $('#sender_address').val(data.address);
                    $('#order_type_id').val(data.order_type);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

            get_stocks();
        });

        function get_stocks() {
            $.ajax({
                url: "{{ url('/mngrAdmin/get_stock_by_corporate/')}}" + "/" + $("#corparate_id").val() + "?main_store=" + $("#store_id-field").val(),
                type: 'get',
                data: {},
                success: function (data) {
                    if (data.length > 0) {
                        $.each(data, function (i, content) {
                            $('#stock_id-field').append($("<option></option>").attr("value", content.id).text(content.title));
                        });
                        $("#qty-field").attr('disabled', false);
                    } else {
                        $("#qty-field").val(0);
                        $("#qty-field").attr('disabled', true);
                    }
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }

        $("#customer_id").on('change', function () {
            $('#sender_name-field').val($(this).attr('data-name'));
            $('#sender_mobile-field').val($(this).attr('data-mobile'));
        });


        /*getdriver_by_agents*/
        $("#agent_id-field").on('change', function () {
            $('#driver_id').html('<option value="" >Choose Drivers</option>');
            if ($(this).val()) {
                $.ajax({
                    url: "{{ url('/mngrAdmin/getdriver_by_agents')}}" + "/" + $(this).val(),
                    type: 'get',
                    data: {},
                    success: function (data) {

                        $.each(data, function (i, content) {
                            $('#driver_id').append($("<option></option>").attr("value", content.id).text(content.name));
                        });
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        });


        $("#qty-field").bind('keyup mouseup', function () {
            check_stock_quantity();
        });

        $("#stock_id-field").on('change', function () {
            check_stock_quantity();
        });

        function check_stock_quantity() {
            if ($("#stock_id-field").val() != '') {
                $("#qty-field").attr('disabled', false);
                $.ajax({
                    url: "{{ url('/mngrAdmin/check_stock_capacity/')}}" + "/" + $("#stock_id-field").val(),
                    type: 'get',
                    data: {},
                    success: function (data) {
                        $("#available_quantity").text(data);
                        if (parseInt($("#qty-field").val()) > data) {
                            $("#qty-field").val(data);
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            } else {
                $("#available_quantity").text(0);
                $("#qty-field").val(0);
                $("#qty-field").attr('disabled', true);
            }
        }

        @if((isset($order->store_id) && $order->store_id))
        $("#stock_id-field").val("{{ $order->store_id }}").change();
        @endif

        if ($("#s_government_id").attr('data-id')) {
            firstLoad = true;
        }

        if ($("#s_government_id").attr('data-first')) {
            firstLoad = false;
        }

        $("#s_government_id").val($("#s_government_id").attr('data-id')).change();

    });

</script>

@extends('backend.layouts.app')
@section('css')
  <title>{{__('backend.Vehicle_Colors')}}</title>
@endsection
@section('header')
  <div class="page-header clearfix">
    <h3>
      <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.Vehicle_Colors')}}
      @if(permission('addVehicleColor')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
        <a class="btn btn-success pull-right" href="{{ route('mngrAdmin.colors.create') }}"><i class="glyphicon glyphicon-plus"></i> {{__('backend.create')}}</a>
      @endif
    </h3>
  </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
      @if($colors->count())
        <table class="table table-condensed table-striped">
          <thead>
            <tr>
              <th>#</th>
              <th>{{__('backend.color')}}</th>
              <th>{{__('backend.status')}}</th>
              <th class="text-right"></th>
            </tr>
          </thead>
          <tbody>
              @foreach($colors as $color)
                <tr>
                  <td>{{$color->id}}</td>
                  <td>{{$color->name}}</td>
                  <td>{!! $color->status_span !!}</td>
                  <td class="text-right">
                    {{--<a class="btn btn-xs btn-primary" href="{{ route('mngrAdmin.colors.show', $color->id) }}"><i class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>--}}
                    @if(permission('editVehicleColor')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                      <a class="btn btn-xs btn-warning" href="{{ route('mngrAdmin.colors.edit', $color->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                    @endif
                    @if(permission('deleteVehicleColor')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                      <form action="{{ route('mngrAdmin.colors.destroy', $color->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
                          <input type="hidden" name="_method" value="DELETE">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}</button>
                      </form>
                    @endif
                  </td>
                </tr>
              @endforeach
          </tbody>
        </table>

        {!! $colors->appends($_GET)->links() !!}

      @else
        <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
      @endif
    </div>
  </div>
@endsection


<?php if( app()->getLocale() == 'en' ): ?>
    <link href="<?php echo e(asset('/profile/css/bootstrap.min.css')); ?>" rel="stylesheet" media="screen">
	<link href="<?php echo e(asset('/profile/css/main.css')); ?>" rel="stylesheet" media="screen">
<?php else: ?>
    <link href="<?php echo e(asset('/profile/css/bootstrap.min.css')); ?>" rel="stylesheet" media="screen">

	<link href="<?php echo e(asset('/profile/css/bootstrap-rtl.min.css')); ?>" rel="stylesheet" media="screen">
	<link href="<?php echo e(asset('/profile/css/main.css')); ?>" rel="stylesheet" media="screen">

	<link href="<?php echo e(asset('/profile/css/main-ar.css')); ?>" rel="stylesheet" media="screen">
<?php endif; ?>

<link href="<?php echo e(asset('/profile/fonts/icomoon.css')); ?>" rel="stylesheet">
<link href="<?php echo e(asset('/profile/css/c3.css')); ?>" rel="stylesheet">
<link href="<?php echo e(asset('/profile/css/nv.d3.css')); ?>" rel="stylesheet">
<link href="<?php echo e(asset('/profile/css/chart.css')); ?>" rel="stylesheet">
<link href="<?php echo e(asset('/profile/css/cal-heatmap.css')); ?>" rel="stylesheet">
<link rel="stylesheet" href="<?php echo e(asset('/profile/css/circliful.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('/profile/css/odometer.css')); ?>">




<?php /**PATH /home/www/ufelix/ufelix/resources/views/frontend/profile/layouts/css.blade.php ENDPATH**/ ?>
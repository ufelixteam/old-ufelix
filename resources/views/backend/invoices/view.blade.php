@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.the_invoices')}} - {{__('backend.view')}}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <style>
        .invoice-header {
            margin-bottom: 0px;
            margin-top: 5px;
            color: #676a6d;
            text-decoration: underline;
            font-style: italic;
        }
    </style>
@endsection
@section('header')
    <div class="page-header noprint">
        <div class="row">
            <div class="col-sm-2">
                <h3 class="invoice-header">{{__('backend.invoice_number')}}:</h3>
            </div>
            <div class="col-sm-2">
                <h4 class="invoice-header">#{{$invoice->invoice_no}}</h4>
            </div>
            <div class="col-sm-2">
                <h3 class="invoice-header">{{__('backend.date')}}:</h3>
            </div>
            <div class="col-sm-2">
                <h4 class="invoice-header">{{$invoice->created_at->format('Y-m-d')}}</h4>
            </div>
        </div>
        @if($invoice->type == 1)
            <div class="row">
                <div class="col-sm-2">
                    <h3 class="invoice-header">{{__('backend.driver_name')}}:</h3>
                </div>
                <div class="col-sm-2">
                    <h4 class="invoice-header">{{@$invoice->owner->name}}</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2">
                    <h3 class="invoice-header">{{__('backend.driver_number')}}:</h3>
                </div>
                <div class="col-sm-2">
                    <h4 class="invoice-header">#{{@$invoice->owner->id}}</h4>
                </div>
            </div>
        @elseif($invoice->type == 2)
            <div class="row">
                <div class="col-sm-2">
                    <h3 class="invoice-header">{{__('backend.corporate_name')}}:</h3>
                </div>
                <div class="col-sm-2">
                    <h4 class="invoice-header">{{@$invoice->owner->name}}</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2">
                    <h3 class="invoice-header">{{__('backend.corporate_number')}}:</h3>
                </div>
                <div class="col-sm-2">
                    <h4 class="invoice-header">#{{@$invoice->owner->id}}</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2">
                    <h3 class="invoice-header">{{__('backend.corporate_username')}}:</h3>
                </div>
                <div class="col-sm-2">
                    <h4 class="invoice-header">{{@$invoice->owner ? @$invoice->owner->customers()->first()->name : '-'}}</h4>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-sm-2">
                <h3 class="invoice-header">{{__('backend.transfer_method')}}:</h3>
            </div>
            <div class="col-sm-2">
                <h4 class="invoice-header">{!! @$invoice->owner->transfer_method_span !!}</h4>
            </div>
            @if(@$invoice->owner->transfer_method == 1)
                <div class="col-sm-2">
                    <h3 class="invoice-header">{{__('backend.bank_name')}}:</h3>
                </div>
                <div class="col-sm-2">
                    <h4 class="invoice-header">{{@$invoice->owner->bank_name}}</h4>
                </div>
                <div class="col-sm-2">
                    <h3 class="invoice-header">{{__('backend.bank_account')}}:</h3>
                </div>
                <div class="col-sm-2">
                    <h4 class="invoice-header">{{@$invoice->owner->bank_account}}</h4>
                </div>
            @elseif(@$invoice->owner->transfer_method == 2)
                <div class="col-sm-2">
                    <h3 class="invoice-header">{{__('backend.mobile_number')}}:</h3>
                </div>
                <div class="col-sm-2">
                    <h4 class="invoice-header">{{@$invoice->owner->mobile_transfer}}</h4>
                </div>
            @endif
        </div>

        <div class=" pull-right" role="group" id="group" aria-label="...">

        @if(permission('printInvoice')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
            <a href="{{url('mngrAdmin/print/invoice', $invoice->id)}}" target="_blank" id="printInvoice"
               class="btn btn-info"><i class="glyphicon glyphicon-print"></i> {{__('backend.print_invoice')}}</a>
            @endif

        </div>
    </div>
@endsection

@section('content')
    <h2 class="text-center">
        {{__('backend.Orders_List')}}
    </h2>

    <form action="" method="get">
        <div class="row" style="margin-bottom:15px;">
            <div class="col-md-12">

                <div class="group-control col-md-2 col-sm-2">
                    <div class='input-group date' id="datepickerFilter">
                        <input type="text" name="created_at" class="form-control"
                               value="{{app('request')->input('created_at')}}"
                               placeholder="{{__('backend.created_at')}}"/>
                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>

                <div class="group-control col-md-2 col-sm-2">
                    <div class='input-group date' id="datepickerFilter2">
                        <input type="text" name="delivered_at" class="form-control"
                               value="{{app('request')->input('delivered_at')}}"
                               placeholder="{{__('backend.Delivered_Date')}}"/>
                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>

                <div class="group-control col-md-2 col-sm-2">
                    <select name="status" class="form-control">
                        <option value="">{{__('backend.status')}}</option>
                        <option
                            value="2" {{app('request')->input('status') == 2 ? 'selected' : ''}}>{{__('backend.received')}}</option>
                        <option
                            value="3" {{app('request')->input('status') == 3 ? 'selected' : ''}}>{{__('backend.delivered')}}</option>
                        <option
                            value="5" {{app('request')->input('status') == 5 ? 'selected' : ''}}>{{__('backend.recalled')}}</option>
                    </select>
                </div>

                @if($invoice->type == 2)
                    <div class="group-control col-md-2 col-sm-2">
                        <select name="customer_id" class="form-control">
                            <option value="">{{__('backend.user')}}</option>
                            @foreach($customers as $customer)
                                <option
                                    value="{{$customer->id}}" {{app('request')->input('customer_id') == $customer->id ? 'selected' : ''}}>{{$customer->name}}</option>
                            @endforeach
                        </select>
                    </div>
                @endif

                @if($invoice->type == 2)
                    <div class="group-control col-md-2 col-sm-2">
                        <select name="paid" class="form-control">
                            <option value="">{{__('backend.pay_by_captain')}}</option>
                            <option
                                value="1" {{app('request')->input('paid') == 1 ? 'selected' : ''}}>{{__('backend.yes')}}</option>
                            <option
                                value="2" {{app('request')->input('paid') == 2 ? 'selected' : ''}}>{{__('backend.no')}}</option>
                        </select>
                    </div>
                @endif
                <div class="group-control col-md-2 col-sm-2">
                    <button type="submit" class="btn btn-primary btn-block"> {{__('backend.filter_orders')}} </button>
                </div>

            </div>
        </div>
    </form>

    <form class="noprint" id="transactionForm" action="{{ route('mngrAdmin.invoices.Transaction') }}"
          method="POST">
        {{csrf_field()}}
        <input type="hidden" name="status" value="{{app('request')->input('status')}}"/>
        <input type="hidden" name="created_at" value="{{app('request')->input('created_at')}}"/>
        <input type="hidden" name="delivered_at" value="{{app('request')->input('delivered_at')}}"/>
        <input type="hidden" name="paid" value="{{app('request')->input('paid')}}"/>

        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">

                <table class="table table-condensed table-striped text-center" style="margin-top: 25px;">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{__('backend.receiver_name')}}</th>
                        @if($invoice->type == 2)
                            <th>{{__('backend.Corporate_Deserved')}}</th>
                        @endif
                        <th>{{__('backend.Delivery_Price')}}</th>
                        <th>{{__('backend.order_price')}}</th>
                        <th>{{__('backend.overweight_cost')}}</th>
                        <th>{{__('backend.Total_Delivery')}}</th>
                        @if($invoice->type == 1)
                            <th>{{__('backend.Captain_Deserved')}}</th>
                            <th>{{__('backend.Total')}}</th>
                        @endif
                        <th>{{__('backend.status')}}</th>
                        <th>{{__('backend.special_action')}}</th>
                        @if($invoice->type == 2)
                            <th>{{__('backend.pay_by_captain')}}</th>
                        @endif
                        <th>{{__('backend.Fees_From')}}</th>
                        <th>{{__('backend.Order_Date')}}</th>
                        <th>{{__('backend.Delivered_Date')}}</th>

                        @if($invoice->type == 3)
                            <th>{{__('backend.Driver_Name')}}</th>
                            <th>{{__('backend.Driver_Mobile')}}</th>
                        @endif

                        <th class="noprint">{{__('backend.Transfer_To_Other_Invoice')}}</th>

                    </tr>
                    </thead>

                    <tbody>
                    @foreach($AllAcceptedPaginatedOrders as $index => $Order)

                        <input type="hidden" name="invoice_orders[]"
                               value="{{ $Order->invoice_order_id }}">
                        <tr class='clickable-row'
                            data-href="{{ route('mngrAdmin.orders.show', $Order->id ) }}">
                            <td>{{$Order->id}}</td>
                            <td>{{$Order->receiver_name}}</td>
                            @if($invoice->type == 2)
                                <td>{{$Order->corporate_deserved}}</td>
                            @endif
                            <td>{{$Order->delivery_price}}</td>
                            <td>{{$Order->order_price}}</td>
                            <td>{{$Order->overweight_cost}}</td>
                            <td>{{$Order->overweight_cost + $Order->delivery_price}}</td>
                            @if($invoice->type == 1)
                                <td class="exclude-td"><input class="form-control price_list"
                                                              name="price_list[{{@$Order->accepted->id}}]"
                                                              value="{{$invoice->status == 1 ? $Order->captain_bonus : ($Order->driver_bonus ? $Order->driver_bonus : 0)}}"
                                                              @if($Order->status == 2 || $invoice->status == 1 || ($Order->status == 5 && !$Order->warehouse_dropoff)) disabled @endif />
                                </td>
                                <td>{{$Order->total_price}}</td>
                            @endif
                            <td>{!! $Order->invoice_status_span !!}</td>
                            <td>{{$Order->warehouse_dropoff ? __('backend.warehouse') : '-'}}</td>
                            @if($invoice->type == 2)
                                <td>
                                    @if($Order->paid == 1)
                                        <span class='badge badge-pill label-warning'>{{__('backend.paid')}}</span>
                                    @else
                                        <span class='badge badge-pill label-info'>{{__('backend.not_paid')}}</span>
                                    @endif
                                </td>
                            @endif
                            <td>{{$Order->payment_name }}</td>
                            <td>{{$Order->created_at ? date('Y-m-d', strtotime($Order->created_at)) : '-'}}</td>
                            <td>{{$Order->delivered_at ? date('Y-m-d', strtotime($Order->delivered_at)) : '-'}}</td>
                            @if($invoice->type == 3)
                                <td>{{$Order->name}}</td>
                                <td>{{$Order->mobile}}</td>
                            @endif
                            <td class="text-center noprint sendIdClass exclude-td">
                                <input class="sendId" type="checkbox" name="sendId[]"
                                       data-price="{{$Order->status == 3 ? $Order->order_price : 0}}"
                                       data-deserved="{{$Order->corporate_deserved ? $Order->corporate_deserved: 0}}"
                                       data-fees="{{($Order->status == 3 || ($Order->status == 5 && $Order->warehouse_dropoff)) ? ($Order->overweight_cost + $Order->delivery_price) : 0}}"
                                       data-pocket="{{$Order->status == 3 && $Order->payment_method_id == 2 ? ($Order->overweight_cost + $Order->delivery_price) : '0'}}"
                                       data-total="{{$Order->status == 3 && $Order->payment_method_id == 1 ? $Order->order_price : ($Order->status == 3 && $Order->payment_method_id == 2 ? $Order->total_price : 0) }}"
                                       value="{{ ! empty($Order->accepted) ? $Order->accepted->id : '' }}"
                                       @if($Order->status == 2 || $invoice->status == 1 || ($Order->status == 5 && !$Order->warehouse_dropoff)) disabled
                                       @endif
                                       @if($Order->status == 3 || $invoice->status == 1 || ($Order->status == 5 && $Order->warehouse_dropoff)) checked @endif
                                ><br>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md-12 col-xs-12 col-sm-12">
                <h2 class="text-center">
                    {{__('backend.Pickups_List')}}
                </h2>
                <table class="table table-condensed table-striped text-center" style="margin-top: 25px;">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{__('backend.pickup_no')}}</th>
                        <th>{{__('backend.orders_in_pickup')}}</th>
                        @if($invoice->type == 2)
                            <th>{{__('backend.captain')}}</th>
                        @elseif($invoice->type == 1)
                            <th>{{__('backend.bonus_for_pickup')}}</th>
                            <th>{{__('backend.pickup_price')}}</th>
                        @endif
                        <th>{{__('backend.status')}}</th>
                        @if($invoice->type == 2)
                            <th>{{__('backend.Created_Date')}}</th>
                            <th>{{__('backend.Received_Date')}}</th>
                            <th>{{__('backend.Delivered_Date')}}</th>
                        @elseif($invoice->type == 1)
                            <th>{{__('backend.total_bonus')}}</th>
                        @endif

                        <th>{{__('backend.pickup_cost')}}</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($pickups as $index => $pickup)
                        <tr class='clickable-row'
                            data-href="{{ route('mngrAdmin.pickups.show', $pickup->id ) }}">
                            <td>{{$pickup->id}}</td>
                            <td>{{$pickup->pickup_number}}</td>
                            <td class="pickup_orders">{{$pickup->pickup_orders_count}}</td>
                            @if($invoice->type == 2)
                                <td>{{$pickup->name}}</td>
                            @elseif($invoice->type == 1)
                                <td class="exclude-td">
                                    <input class="form-control pickup_bonus" name="pickups[{{$pickup->id}}][bonus]"
                                           value="{{$invoice->status == 1 ? $pickup->captain_bonus : ($pickup->bonus_per_order ? $pickup->bonus_per_order : 0)}}"/>
                                </td>
                                <td class="exclude-td">
                                    <input class="form-control pickup_price" name="pickups[{{$pickup->id}}][price]"
                                           value="{{$invoice->status == 1 ? $pickup->pickup_price : ($pickup->delivery_price ? $pickup->delivery_price : 0)}}"/>
                                </td>
                            @endif

                            <td>{!! $pickup->status_span !!}</td>
                            @if($invoice->type == 2)
                                <td>{{$pickup->created_at ? date('Y-m-d', strtotime($pickup->created_at)) : '-'}}</td>
                                <td>{{$pickup->received_at ? date('Y-m-d', strtotime($pickup->received_at)) : '-'}}</td>
                                <td>{{$pickup->delivered_at ? date('Y-m-d', strtotime($pickup->delivered_at)) : '-'}}</td>
                            @elseif($invoice->type == 1)
                                <td class="total_bonus">{{($pickup->pickup_orders_count * ($invoice->status == 1 ? $pickup->captain_bonus : ($pickup->bonus_per_order ? $pickup->bonus_per_order : 0)))}}</td>
                            @endif

                            <td class="pickup_cost">
                                @if($invoice->type == 2)
                                    {{$pickup->pickup_orders_count < 3 ? 15 : 0}}
                                @elseif($invoice->type == 1)
                                    {{ ($pickup->pickup_orders_count * ($invoice->status == 1 ? $pickup->captain_bonus : ($pickup->bonus_per_order ? $pickup->bonus_per_order : 0))) + ($invoice->status == 1 ? $pickup->pickup_price : ($pickup->delivery_price ? $pickup->delivery_price : 0)) }}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md-12 col-xs-12 col-sm-12" style="margin-top: 30px;">
                <h2 class="text-center">
                    {{__('backend.Invoice_Bill')}}
                </h2>
                <div id="Transaction">

                    <table class="table table-condensed table-striped text-center">
                        <thead style="color:#0e8a29">
                        <tr>
                            @if($invoice->type == 2)

                                <th>{{__('backend.orders')}}#</th>
                                <th>{{__('backend.Total_order_Price')}}</th>
                                <th>{{__('backend.Total_Delivery')}}</th>
                                <th>{{__('backend.pocket')}}</th>
                                <th>{{__('backend.remain')}}</th>
                                <th>{{__('backend.discount')}}</th>
                                <th>{{__('backend.Corporate_Deserved')}}</th>
                                <th>{{__('backend.pickup_cost')}}</th>
                                <th>{{__('backend.final_deserved')}}</th>
                                <th>{{__('backend.Ufelix_Deserved')}}</th>
                                @if( $invoice->status == 0)
                                    <th>{{__('backend.add_discount')}}</th>
                                @endif

                            @elseif($invoice->type == 1)
                                <th>{{__('backend.orders')}}#</th>
                                <th>{{__('backend.total_collect')}}</th>
                                <th>{{__('backend.bonus_delivery')}}</th>
                                <th>{{__('backend.total_pickup_cost')}}</th>
                                <th>{{__('backend.total_captain_deserved')}}</th>
                                <th>{{__('backend.Ufelix_Deserved')}}</th>

                            @endif
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            @if($invoice->type == 2)

                                <td><span id="orders_number">0</span></td>
                                <td>
                                    <span id="priceTotal">0</span>
                                    <input type="hidden" class="inputtd2" id="priceTotalInput" name="order_price"
                                           value="0">
                                </td>
                                <td>  <span id="deliveryTotal">
                                        0
                                    </span>
                                </td>
                                <td>
                                    <span
                                        id="pocketTotal">0</span>

                                </td>
                                <td>
                                    <span
                                        id="remainTotal">0</span>
                                </td>

                                <td id="percentTd">
                                    <strong
                                        id="percentTotal">{{ $invoice->profit ? $invoice->profit : 0}}</strong>
                                </td>
                                <td>
                                    <span id="delivery_price">
                                            0
                                    </span>
                                </td>
                                <td>
                                    <span id="pickup_cost">0</span>
                                </td>
                                <td>
                                    <input type="hidden" class="delivery_price" name="customer_des"
                                           value="0">
                                    <span id="final_deserved">0</span>
                                </td>
                                <td>
                                    <input type="hidden" class="delivery_priceU" name="ufelix_des"
                                           value="0">
                                    <span id="delivery_priceU">
                                           0
                                    </span>
                                </td>
                                <td>
                                    @if( $invoice->status == 0)

                                        <div class="col-md-12">
                                            <div class='input-group'>
                                                <input id="percent" type="text" name="profit" class="form-control"
                                                       value="{{  $invoice->profit ? $invoice->profit : 0 }}"/>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-dollar"></i>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </td>

                            @elseif($invoice->type == 1)
                                <td><span id="orders_number">0</span></td>
                                <td>
                                    <span id="total_collect">0</span>
                                </td>
                                <td>  <span id="bonus_delivery">
                                        0
                                    </span>
                                </td>
                                <td>
                                    <span
                                        id="pickup_cost">0</span>

                                </td>
                                <td>
                                    <input type="hidden" class="delivery_price" name="customer_des"
                                           value="0">
                                    <span id="delivery_price">0</span>
                                </td>
                                <td>
                                    <input type="hidden" class="delivery_priceU" name="ufelix_des"
                                           value="0">
                                    <span id="delivery_priceU">
                                           0
                                    </span>
                                </td>
                            @endif
                        </tr>
                        </tbody>

                    </table>

                    <input type="hidden" name="Id" value="{{$invoice->id}}">
                    <input type="hidden" name="Type" value="{{$invoice->type}}">
                    <input type="hidden" name="objId" value="{{$invoice->object_id}}">

                @if($invoice->status == 0)
                    @if(permission('transactionInvoices')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                        <button type="button" class="btn btn-primary btn-block noprint" id="transactionBtn"
                                name="Transaction">{{__('backend.Confirm_Transaction')}}</button>
                        @endif
                    @endif


                </div>
            </div>
        </div>
    </form>

@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script>

        $(function () {

            $(window).keydown(function (event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });

            calculate_invoice();

            $('.sendId').on('change', function () {
                calculate_invoice();
            });

            /*transactionBtn*/
            $('#transactionBtn').on('click', function () {

                $.ajax({
                    url: '{{ url("mngrAdmin/invoices/Transact") }}',
                    type: 'post',

                    data: $("#transactionForm").serialize(),
                    success: function (data) {

                        if (data['statusCode'] == '420') {

                            $.alert({
                                title: '',
                                theme: 'modern',
                                class: 'danger',
                                content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Confirm Close</span></div><div style="font-weight:bold;"><p> We can not continue Transaction Operation .</p><p class="text-primary">' + data['message'] + '</p></div>',
                            });
                        } else {

                            $.alert({
                                title: '',
                                theme: 'modern',
                                class: 'success',
                                content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-smile-o  text-success"></i></span><span class="jconfirm-title">Confirm Done</span></div><div style="font-weight:bold;"><p class="text-primary">' + data['message'] + '</p></div>',
                                onClose: function () {
                                    window.location.reload();
                                },
                            });
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
                // }

            });

            {{--/* changePercentDriver*/--}}
            {{--$("#changePercentDriver").on('click', function () {--}}
            {{--    var percent = parseFloat($('#percentDriver').val());--}}
            {{--    var percentVal = percent * parseFloat("{{$totalDelivery}}") / 100;--}}
            {{--    $("#d_profit").text(percent);--}}
            {{--    $("#d_commission").text(percentVal);--}}
            {{--    $("#u_commission").text(parseFloat("{{$totalDelivery}}") - parseFloat(percentVal));--}}
            {{--    if (parseFloat(percentVal) > parseFloat($('#c_Delivery').text())) {--}}

            {{--        $('.td1').text(0);--}}
            {{--        $('.td2').text(parseFloat(percentVal) - parseFloat($('#c_Delivery').text()));--}}
            {{--        $('.td3').text(parseFloat(percentVal));--}}
            {{--        $('.td4').text(parseFloat("{{$totalItem}}") - (parseFloat(percentVal) - parseFloat($('#c_Delivery').text())));--}}


            {{--        $('.inputtd1').val(0);--}}
            {{--        $('.inputtd2').val(parseFloat(percentVal) - parseFloat($('#c_Delivery').text()));--}}
            {{--        $('.inputtd3').val(parseFloat(percentVal));--}}
            {{--        $('.inputtd4').val(parseFloat("{{$totalItem}}") - (parseFloat(percentVal) - parseFloat($('#c_Delivery').text())));--}}


            {{--    } else if (parseFloat(percentVal) < parseFloat($('#c_Delivery').text())) {--}}

            {{--        $('.td1').text(parseFloat($('#c_Delivery').text()) - parseFloat(percentVal));--}}
            {{--        $('.td2').text(0);--}}
            {{--        $('.td3').text(parseFloat(percentVal));--}}
            {{--        $('.td4').text(parseFloat("{{$totalItem}}") - (parseFloat(percentVal) - parseFloat($('#c_Delivery').text())));--}}

            {{--        $('.inputtd1').val(parseFloat($('#c_Delivery').text()) - parseFloat(percentVal));--}}
            {{--        $('.inputtd2').val(0);--}}
            {{--        $('.inputtd3').val(parseFloat(percentVal));--}}
            {{--        $('.inputtd4').val(parseFloat("{{$totalItem}}") - (parseFloat(percentVal) - parseFloat($('#c_Delivery').text())));--}}


            {{--    } else if (parseFloat(percentVal) == parseFloat($('#c_Delivery').text())) {--}}

            {{--        $('.td1').text(0);--}}
            {{--        $('.td2').text(0);--}}
            {{--        $('.td3').text(parseFloat(percentVal));--}}
            {{--        $('.td4').text(parseFloat("{{$totalItem}}") - (parseFloat(percentVal) - parseFloat($('#c_Delivery').text())));--}}

            {{--        $('.inputtd1').val(0);--}}
            {{--        $('.inputtd2').val(0);--}}
            {{--        $('.inputtd3').val(parseFloat(percentVal));--}}
            {{--        $('.inputtd4').val(parseFloat("{{$totalItem}}") - (parseFloat(percentVal) - parseFloat($('#c_Delivery').text())));--}}
            {{--    }--}}

            {{--});--}}

            $("#percent").on('input', function () {
                $("#percentTotal").text($(this).val());
                calculate_invoice();
            });

            $(".price_list").on('input', function () {
                calculate_invoice();
            });

            $(".pickup_bonus").on('input', function () {
                var pickup_bonus = $(this).val() ? parseFloat($(this).val()) : 0;
                var total_bonus = parseInt($(this).parents('tr').find('.pickup_orders').text()) * pickup_bonus;
                var pickup_price = parseFloat($(this).parents('tr').find('.pickup_price').val());
                var one_pickup_cost = (pickup_price ? pickup_price : 0) + total_bonus;
                $(this).parents('tr').find('.pickup_cost').text(one_pickup_cost);
                calculate_invoice();
            });

            $(".pickup_price").on('input', function () {
                var pickup_bonus = parseFloat($(this).parents('tr').find('.pickup_bonus').val());
                var total_bonus = parseInt($(this).parents('tr').find('.pickup_orders').text()) * (pickup_bonus ? pickup_bonus : 0);
                var pickup_price = $(this).val() ? parseFloat($(this).val()) : 0;
                var one_pickup_cost = pickup_price + total_bonus;
                $(this).parents('tr').find('.pickup_cost').text(one_pickup_cost);
                calculate_invoice();
            });

            @if($invoice->type==2)
            function calculate_invoice() {
                var orders_number = 0;
                var total_price = 0;
                var total_fees = 0;
                var pocket = 0;
                var deserved = 0;
                var final_deserved = 0;
                var pickup_cost = 0;
                var percent = 0;
                var remain = 0;
                var ufelix_deserved = 0;
                $('.sendId:checkbox:checked').each(function (e) {
                    orders_number += 1;
                    if ($(this).attr('data-price')) {
                        total_price += parseFloat($(this).attr('data-price'));
                    }
                    if ($(this).attr('data-fees')) {
                        total_fees += parseFloat($(this).attr('data-fees'));
                    }
                    if ($(this).attr('data-pocket')) {
                        pocket += parseFloat($(this).attr('data-pocket'));
                    }
                    if ($(this).attr('data-deserved')) {
                        deserved += parseFloat($(this).attr('data-deserved'));
                    }
                });

                $('.pickup_cost').each(function (e) {
                    pickup_cost += parseFloat($(this).text());
                });

                percent = parseFloat($('#percentTotal').text());
                percent = percent ? percent : 0;
                remain = (total_fees - pocket) - percent;
                deserved += percent;
                final_deserved = deserved - pickup_cost;
                ufelix_deserved = remain + pickup_cost;

                $("#orders_number").text(orders_number);
                $("#priceTotal").text(total_price);
                $("#priceTotalInput").val(total_price);
                $("#deliveryTotal").text(total_fees);
                $("#pocketTotal").text(pocket);
                $('#remainTotal').text(remain);
                $("#percentTotal").text(percent);
                $("#pickup_cost").text(pickup_cost);
                $('#delivery_price').text(deserved);
                $('.delivery_price').val(final_deserved);
                $('#final_deserved').text(final_deserved);
                $('.delivery_priceU').val(ufelix_deserved);
                $('#delivery_priceU').text(ufelix_deserved);
            }

            @elseif($invoice->type==1)
            function calculate_invoice() {
                var orders_number = 0;
                var total_collect = 0;
                var bonus_delivery = 0;
                var pickup_cost = 0;
                var deserved = 0;
                var ufelix_deserved = 0;
                $('.sendId:checkbox:checked').each(function (e) {
                    orders_number += 1;
                    if ($(this).attr('data-total')) {
                        total_collect += parseFloat($(this).attr('data-total'));
                    }

                    if ($(this).parents('tr').find('.price_list').val()) {
                        bonus_delivery += parseFloat($(this).parents('tr').find('.price_list').val());
                    }
                });

                $('.pickup_cost').each(function (e) {
                    pickup_cost += parseFloat($(this).text());
                    console.log(pickup_cost);
                });

                deserved = bonus_delivery + pickup_cost;
                ufelix_deserved = total_collect - deserved;

                $("#orders_number").text(orders_number);
                $("#total_collect").text(total_collect);
                $("#bonus_delivery").text(bonus_delivery);
                $("#pickup_cost").text(pickup_cost);
                $('.delivery_price').val(deserved);
                $('#delivery_price').text(deserved);
                $('.delivery_priceU').val(ufelix_deserved);
                $('#delivery_priceU').text(ufelix_deserved);
            }
            @endif
        })
    </script>
@endsection

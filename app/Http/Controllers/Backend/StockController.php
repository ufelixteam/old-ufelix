<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Corporate;
use App\Models\Stock;
use App\Models\Store;
use Illuminate\Http\Request;
use Auth;
use Config;

class StockController extends Controller
{

    public function __construct(Request $request)
    {
        if ($request->is('mngrAdmin/*') || $request->is('admin/*')) {
            Config::set('auth.defaults.guard', 'admin');

        } elseif ($request->is('mngrAgent/*') || $request->is('agent/*')) {
            Config::set('auth.defaults.guard', 'agent');
        }
        $this->guardType = Config::get('auth.defaults.guard');

        //echo $this->guardType;die();

        // config(['auth.defaults.guard' => 'admin']);
    }

    // Get All Stock
    public function index()
    {
        $user = Auth::guard($this->guardType)->user();

        $stocks = Stock::select('stocks.*')->orderBy('stocks.id', 'desc');

        if ($user->is_warehouse_user) {

            $stocks = $stocks->join('corporates', 'corporates.id', '=', 'stocks.corporate_id')
                ->join('store_user', 'store_user.store_id', '=', 'corporates.store_id')
                ->where('user_id', $user->id);
        }

        $stocks = $stocks->paginate(50);

        return view('backend.stocks.index', compact('stocks'));
    }

    // Create Stock Page
    public function create($warehouse_id = null)
    {
        $user = Auth::guard($this->guardType)->user();

        $warehouse = null;
        if ($warehouse_id) {
            $warehouse = Store::where('id', $warehouse_id)->first();
            echo ($warehouse);
        }
        $stores = Store::where('status', 1);

        if ($user->is_warehouse_user) {
            $stores = $stores->join('store_user', 'store_user.store_id', '=', 'stores.id')
                ->where('user_id', $user->id);
        }

        $stores = $stores->get();

        $corporates = Corporate::where('is_active', 1)->select('corporates.*')->orderBy('corporates.created_at', 'ASC');

        if ($user->is_warehouse_user) {
            $corporates = $corporates->join('customers', 'customers.corporate_id', '=', 'corporates.id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id);
        }

        $corporates = $corporates->get();

        return view('backend.stocks.create', compact('stores', 'corporates', 'warehouse'));
    }

    function check_stock_capacity($id, Request $request)
    {
        $available_quantity = 0;
        if ($id) {
            $stock = Stock::find($id);
            if ($stock) {
                $available_quantity = $stock->available_quantity;
            }
        }

        return $available_quantity;
    }

    // Added New Stock
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'qty' => 'required|integer',
            'store_id' => 'required|integer',
            'corporate_id' => 'required|integer',
            'description' => 'required',
        ]);

        $stock = new Stock();
        $stock->title = $request->input("title");
        $stock->qty = $request->input("qty");
        $stock->store_id = $request->input("store_id");
        $stock->corporate_id = $request->input("corporate_id");
        $stock->description = $request->input("description");
        $stock->save();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Add New Stock ' . $stock->title, ' اضافة مخزن فرعي ' . $stock->title, 13); // Helper Function in App\Helpers.php

        return redirect()->route('mngrAdmin.stocks.index')->with('message', __('backend.Stock_created_successfully'));
    }

    // View One Stock
    public function show($id)
    {
        $stock = Stock::findOrFail($id);
        return view('backend.stocks.show', compact('stock'));
    }

    // Edit Stock Page
    public function edit($id)
    {
        $user = Auth::guard($this->guardType)->user();

        $stock = Stock::findOrFail($id);
        $stores = Store::where('status', 1);

        if ($user->is_warehouse_user) {
            $stores = $stores->join('store_user', 'store_user.store_id', '=', 'stores.id')
                ->where('user_id', $user->id);
        }

        $stores = $stores->get();
        $corporates = Corporate::where('is_active', 1)->select('corporates.*')->orderBy('corporates.created_at', 'ASC');

        if ($user->is_warehouse_user) {
            $corporates = $corporates->join('customers', 'customers.corporate_id', '=', 'corporates.id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id);
        }

        $corporates = $corporates->get();
        return view('backend.stocks.edit', compact('stock', 'stores', 'corporates'));
    }

    // Update Stock
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'title' => 'required',
            'qty' => 'required|integer',
            'used_qty' => 'integer',
            'store_id' => 'required|integer',
            'corporate_id' => 'required|integer',
            'description' => 'required',
        ]);

        $stock = Stock::findOrFail($id);
        $stock->title = $request->input("title");
        $stock->used_qty = $request->input("used_qty");
        $stock->qty = $request->input("qty");
        $stock->store_id = $request->input("store_id");
        $stock->corporate_id = $request->input("corporate_id");
        $stock->description = $request->input("description");
        $stock->save();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Edit Stock ' . $stock->title, ' تعديل المخزن الفرعي ' . $stock->title, 13); // Helper Function in App\Helpers.php

        return redirect()->route('mngrAdmin.stocks.index')->with('message', __('backend.Stock_Updated_successfully'));

    }

    // Delete Stock
    public function destroy($id)
    {
        $stock = Stock::findOrFail($id);
        $stock->delete();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Delete Stock ' . $stock->title, ' حذف المخزن الفرعي ' . $stock->title, 13); // Helper Function in App\Helpers.php

        return redirect()->route('mngrAdmin.stocks.index')->with('message', __('backend.Stock_deleted_successfully'));
    }
    public function save_actual_pieces(Request $request) {
        $request->validate([
            'id' => 'required|integer',
            'actual_pieces' => 'required'
        ]);
        Stock::where('id', $request->input('id'))->update(['qty'=> $request->input('actual_pieces')]);
    }
}

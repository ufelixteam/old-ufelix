@extends('backend.layouts.app')

@section('css')
    <title>{{__('backend.customers')}} - {{__('backend.add_customer')}}</title>
    <link href="{{asset('assets/select2/select2.min.css')}}" rel="stylesheet">
    <style>
        .select2-container {
            width: 100% !important;
            display: block;
        }
        .input-group {
            -moz-box-shadow: none !important;
            -webkit-box-shadow: none !important;
            box-shadow: none !important;
        }

        .input-group {
            width: 100%;
        }

        .pac-container {
            z-index: 9999 !important;
        }

        .form-group {
            min-height: 94px;
            margin-bottom: 0px;
        }
    </style>
@endsection

@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-plus"></i> {{__('backend.corporate_users')}} / {{__('backend.add_user')}}
        </h3>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <form action="{{route('mngrAdmin.customers.store')}}" method="POST" name="customer_create"
                  enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('name')) has-error @endif">
                        <label for="name-field">{{__('backend.name')}}: </label>
                        <input type="text" id="name-field" name="name" class="form-control" value="{{ old("name") }}"/>
                        @if($errors->has("name"))
                            <span class="help-block">{{ $errors->first("name") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('email')) has-error @endif">
                        <label for="email-field">{{__('backend.email')}}: </label>
                        <input type="text" id="email-field" name="email" class="form-control"
                               value="{{ old("email") }}"/>
                        @if($errors->has("email"))
                            <span class="help-block">{{ $errors->first("email") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('mobile')) has-error @endif">
                        <label for="mobile-field">{{__('backend.mobile_number')}}: </label>
                        <input type="text" id="mobile-field" name="mobile" class="form-control" maxlength="11"
                               value="{{ old("mobile") }}"/>
                        @if($errors->has("mobile"))
                            <span class="help-block">{{ $errors->first("mobile") }}</span>
                        @endif
                    </div>


                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('phone')) has-error @endif">
                        <label for="phone-field">{{__('backend.phone')}}: </label>
                        <input type="text" id="phone-field" name="phone" class="form-control" maxlength="11"
                               value="{{ old("phone") }}"/>
                        @if($errors->has("phone"))
                            <span class="help-block">{{ $errors->first("phone") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('national_id')) has-error @endif">
                        <label for="national_id-field">{{__('backend.national_id_number')}}:</label>
                        <input type="text" id="national_id-field" maxlength="14" name="national_id" class="form-control"
                               value="{{old("national_id")}}"/>
                        @if($errors->has("national_id"))
                            <span class="help-block">{{ $errors->first("national_id") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('job')) has-error @endif">
                        <label for="job-field">{{__('backend.job_name')}}:</label>
                        <input type="text" id="job-field" name="job" class="form-control" value="{{old("job")}}"/>
                        @if($errors->has("job"))
                            <span class="help-block">{{ $errors->first("job") }}</span>
                        @endif
                    </div>

                    {{--                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('country_id')) has-error @endif">--}}
                    {{--                        <label for="country_id-field">{{__('backend.choose_country')}}: </label>--}}
                    {{--                        <select id="country_id-field" name="country_id" class="form-control">--}}
                    {{--                            @if(! empty($countries))--}}
                    {{--                                @foreach($countries as $country)--}}
                    {{--                                    <option--}}
                    {{--                                        value="{{$country->id}}" {{ old("country_id") == $country->id ? 'selected' : '' }}--}}
                    {{--                                    >{{$country->name}}</option>--}}
                    {{--                                @endforeach--}}
                    {{--                            @endif--}}
                    {{--                        </select>--}}
                    {{--                    </div>--}}
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('government_id')) has-error @endif">
                        <label for="government_id-field"> {{__('backend.government')}}: </label>
                        <select class="form-control government_id" id="government_id" name="government_id">
                            <option value="" data-display="Select">{{__('backend.government')}}</option>
                            @if(! empty($governments))
                                @foreach($governments as $government)
                                    <option value="{{$government->id}}">
                                        {{$government->name_en}}
                                    </option>

                                @endforeach
                            @endif
                        </select>
                        @if($errors->has("government_id"))
                            <span class="help-block">{{ $errors->first("government_id") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('city_id')) has-error @endif">
                        <label class="control-label" for="s_state_id">{{__('backend.city')}}</label>
                        <select class=" form-control city_id" id="city_id" name="city_id">
                            <option value="">{{__('backend.city')}}</option>
                        </select>
                        @if($errors->has("city_id"))
                            <span class="help-block">{{ $errors->first("city_id") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('address')) has-error @endif">
                        <label for="address-field">{{__('backend.address')}}: </label>
                        <input type="text" id="address-field" name="address" class="form-control"
                               value="{{ old("address") }}"/>
                        @if($errors->has("address"))
                            <span class="help-block">{{ $errors->first("address") }}</span>
                        @endif
                    </div>
                    <div
                        class="form-group col-md-4 col-sm-4 @if($errors->has('latitude')) has-error @endif">
                        <div class=" input-group ">
                            <label for="receiver_latitude"> {{__('backend.latitude')}}: </label>
                            <input type="text" required id="latitude" name="latitude" class="form-control"
                                   value="{{ old("latitude") ? old("latitude")  : '30.0668886'  }}"/>
                            <span class="input-group-append">
                            <button class="input-group-text bg-transparent" type="button" data-toggle="modal"
                                    data-target="#map1Modal"> <i class="fa fa-map-marker"></i></button>
                        </span>

                            @if($errors->has("latitude"))
                                <span class="help-block">{{ $errors->first("latitude") }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('longitude')) has-error @endif">
                        <label for="longitude"> {{__('backend.longitude')}}: </label>
                        <input type="text" required id="longitude" name="longitude"
                               class="form-control"
                               value="{{ old("longitude") ? old("longitude") : '31.1962743'  }}"/>
                        @if($errors->has("longitude"))
                            <span class="help-block">{{ $errors->first("longitude") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-4 col-sm-4">
                        <label for="mobile-users">{{__('backend.store')}}</label>
                        <select class="form-control select2 disabled" id="store_id" name="store_id">
                            <option value="" data-dispaly="Select">{{__('backend.store')}}</option>
                            @if(! empty($stores))
                                @foreach($stores as $store)
                                    <option value="{{$store->id}}">{{$store->name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('password')) has-error @endif">
                        <label for="password-field">{{__('backend.password')}}: </label>
                        <input type="password" id="password-field" name="password" class="form-control"
                               value="{{ old("password") }}"/>
                        @if($errors->has("password"))
                            <span class="help-block">{{ $errors->first("password") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4">
                        <label for="manager-field">{{__('backend.manager')}}: </label>
                        <select id="manager-field" name="is_manager" class="form-control">
                            <option
                                value="0" {{ old("is_manager") == "0"  ? 'selected' : '' }}>{{__('backend.No')}}</option>
                            <option
                                value="1" {{ old("is_manager") == "1"  ? 'selected' : '' }}>{{__('backend.Yes')}}</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('confirm_password')) has-error @endif">
                        <label for="confirm_password-field">{{__('backend.confirm_password')}}: </label>
                        <input type="password" id="confirm_password-field" name="confirm_password" class="form-control"
                               value="{{ old("confirm_password") }}"/>
                        @if($errors->has("confirm_password"))
                            <span class="help-block">{{ $errors->first("confirm_password") }}</span>
                        @endif
                    </div>

                    <div id="corprate_div" class="form-group col-md-4 col-sm-4 @if($errors->has('corporate_id')) has-error @endif">
                        <label for="type-field">{{__('backend.bellonging_corporate')}}: </label>
                        <select id="type-field" name="corporate_id" id="corporate_id" class="form-control">
                            @foreach($corporates as $corporate)
                                <option value="{{ $corporate->id }}"
                                        @if(app('request')->corporate_id == $corporate->id) selected @endif>{{ $corporate->name }}</option>
                            @endforeach
                        </select>
                        @if($errors->has("corporate_id"))
                            <span class="help-block">{{ $errors->first("corporate_id") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('image')) has-error @endif">
                        <div id="image_upload"></div>
                        <label for="image-field">{{__('backend.upload_image')}}: </label>
                        <input class="uploadPhoto btn btn-primary" type="file" name="image" id="image">
                        @if($errors->has("image"))
                            <span class="help-block">{{ $errors->first("image") }}</span>
                        @endif
                    </div>
                </div>

                {{--                <div class="row">--}}
                {{--                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('type')) has-error @endif">--}}
                {{--                        <label for="role-field">{{__('backend.customer_type')}}: </label>--}}
                {{--                        <select id="role-field" name="role_id" class="form-control" value="{{ old("role_id") }}">--}}
                {{--                            <option value="3">{{__('backend.individual_user')}}</option>--}}
                {{--                            <option value="5">{{__('backend.corporate_user')}}</option>--}}
                {{--                        </select>--}}
                {{--                        @if($errors->has("role_id"))--}}
                {{--                            <span class="help-block">{{ $errors->first("role_id") }}</span>--}}
                {{--                        @endif--}}
                {{--                    </div>--}}
                {{--                    <div id="corprate_div" class="form-group col-md-6 col-sm-6" style="display:none">--}}
                {{--                        <label for="type-field">{{__('backend.bellonging_corporate')}}: </label>--}}
                {{--                        <select id="type-field" name="corporate_id" id="corporate_id" class="form-control">--}}
                {{--                            @foreach($corporates as $corporate)--}}
                {{--                                <option value="{{ $corporate->id }}">{{ $corporate->name }}</option>--}}
                {{--                            @endforeach--}}
                {{--                        </select>--}}
                {{--                        @if($errors->has("type"))--}}
                {{--                            <span class="help-block">{{ $errors->first("type") }}</span>--}}
                {{--                        @endif--}}
                {{--                    </div>--}}
                {{--                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('is_active')) has-error @endif">--}}
                {{--                        <label for="is_active-field">{{__('backend.is_active')}}</label>--}}
                {{--                        <select id="is_active-field" name="is_active" class="form-control">--}}
                {{--                            <option--}}
                {{--                                value="0" {{ old("is_active") == "0" ? 'selected' : '' }}>{{__('backend.no')}}</option>--}}
                {{--                            <option--}}
                {{--                                value="1" {{ old("is_active") == "1" ? 'selected' : '' }}>{{__('backend.yes')}}</option>--}}
                {{--                        </select>--}}
                {{--                        @if($errors->has("is_active"))--}}
                {{--                            <span class="help-block">{{ $errors->first("is_active") }}</span>--}}
                {{--                        @endif--}}
                {{--                    </div>--}}
                {{--                </div>--}}

                <div class="well well-sm col-md-12 col-sm-12 col-xs-12">
                    <button type="submit" class="btn btn-primary">{{__('backend.add_customer')}}</button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.customers.index') }}"><i
                            class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
                </div>
            </form>
        </div>
    </div>

    @include('backend.map')
@endsection
@section('scripts')
    <script type="text/javascript" src="{{asset('/assets/scripts/map.js')}}"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key=AIzaSyDQiN4qc7pC9b1BRQwkqXXD28peMfWcHvw&callback=myMap"></script>
    <script src="{{asset('assets/select2/select2.min.js')}}"></script>

    <script type="text/javascript">
        $(function () {
            $('.select2').select2();

            $('#role-field').on('change', function () {
                if ($(this).val() == 5) {
                    $('#corprate_div').css('display', 'block');
                } else {
                    $("#corporate_id").val("");
                    $('#corprate_div').css('display', 'none');
                }
            });

            $("form[name='customer_create']").validate({
                // Specify validation rules
                rules: {
                    email: {
                        required: true,
                        email: true,
                        // remote: {
                        //     url: "{!! url('/check-email-customer') !!}",
                        //     type: "get"
                        // }
                    },
                    mobile: {
                        required: true,
                        number: true,
                        digits: true,
                        rangelength: [11, 11],
                        // remote: {
                        //     url: "{!! url('/check-mobile-customer') !!}",
                        //     type: "get"
                        // }
                    },
                    // phone: {
                    //     required: true,
                    //     number: true,
                    //     digits: true,
                    //     rangelength: [11, 11],
                    // },
                    password: {
                        required: true,
                        minlength: 5,
                    },
                    name: {
                        required: true,
                    },
                    address: {
                        required: true,
                    },
                    confirm_password: {
                        required: true,
                        equalTo: '#password-field',
                        minlength: 5,
                    },
                    national_id: {
                        number: true,
                        rangelength: [14, 14],
                    },
                    // country_id: {
                    //     required: true,
                    // },
                    government_id: {
                        required: true,
                    },
                    city_id: {
                        required: true,
                    },
                    latitude: {
                        number: true,
                    },
                    longitude: {
                        number: true,
                    },
                    corporate_id: {
                        required: true,
                    },
                    // image: {
                    //     required: true,
                    // },
                },

                // Specify validation error messages
                messages: {
                    password: {
                        required: "{{__('backend.Please_Enter_Customer_Password')}}",
                        minlength: "{{__('backend.Password_must_be_more_than5_character')}}"
                    },
                    name: {
                        required: "{{__('backend.Please_Enter_Customer_Name')}}",
                    },
                    address: {
                        required: "{{__('backend.Please_Enter_Your_Address')}}",
                    },
                    confirm_password: {
                        required: "{{__('backend.Confirm_password_is_wrong')}}",
                        equalTo: "{{__('backend.Confirm_password_is_wrong')}}",
                    },
                    email: {
                        required: "{{__('backend.Please_Enter_Customer_E-mail')}}",
                        email: "{{__('backend.Please_Enter_Customer_Correct_E-mail')}}",
                        remote: jQuery.validator.format("{{__('backend.E-mail_already_exist')}}")
                    },
                    mobile: {
                        required: "{{__('backend.Please_Enter_Customer_Mobile')}}",
                        rangelength: "{{__('backend.Mobile_Must_Be11_Digits')}}",
                        remote: jQuery.validator.format("{{__('backend.Mobile_already_exist')}}"),
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                    },
                    latitude: {
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",

                    },
                    longitude: {
                        required: "{{__('backend.Please_Enter_Longitude')}}",
                    },
                    phone: {
                        required: "{{__('backend.Please_Enter_Customer_Phone')}}",
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                        rangelength: "{{__('backend.Mobile_Must_Be11_Digits')}}",
                    },
                    national_id: {
                        number: "{{__('backend.National_Id_Must_Be_Number')}}",
                        rangelength: "{{__('backend.National_Id_Must_Be14_Digits')}}",
                    },
                    {{--country_id: {--}}
                        {{--    required: "{{__('backend.Please_Chosse_The_Country')}}",--}}
                        {{--},--}}
                    government_id: {
                        required: "{{__('backend.Please_Chosse_The_Government')}}",
                    },
                    city_id: {
                        required: "{{__('backend.Please_Select_City')}}",
                    },
                    corporate_id: {
                        required: "{{__('backend.Please_Enter_Corporate')}}",
                    },
                    // image: {
                    //     required: "{{__('backend.Please_Enter_Customer_Image')}}",
                    // },
                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                },

                highlight: function (element) {
                    $(element).parent().addClass('error')
                },

                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                }
            });

            $("#government_id").on('change', function () {
                $.ajax({
                    url: "{{ url('/get_cities')}}" + "/" + $(this).val(),
                    type: 'get',
                    data: {},
                    success: function (data) {
                        $('#city_id').html('<option value="" >Choose City</option>');
                        $.each(data, function (i, content) {
                            $('#city_id').append($("<option></option>").attr("value", content.id).text(content.name_en));
                        });
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            });

        })
    </script>
@endsection

@extends('frontend.layouts.app')
@section('content')
<!--************************* Header *************************-->
<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="content">
                    <h1>مع يوفيلكس .. <br> الشحن في رمضان بقي أحلي</h1>
                    <h3>دلوقتي تقدر تشحن منتجاتك و بضاعتك طول شهر رمضان المبارك بـ 20 جنية بس داخل القاهره و الجيزة شامل التحصيل</h3>
                    <p>استخدم برومو كود <span>U-Ramadan</span></p>
                    <div class="store-btn">
                        <a href=""><img src="{{ asset('/front/images/app-store.png')}}"></a>
                        <a href=""><img src="{{ asset('/front/images/google-play.png')}}"></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="mockup">
                    <img src="{{ asset('/front/images/01_ufelix-app.png')}}" width="550px">
                </div>
            </div>
        </div>
    </div>
</div>

<!--************************* Map *************************-->

<div class="map" id="map">
    <div class="container text-center">
        <p class="head-title">يوفيلكس بتغطي كل شارع في مصر</p>
    </div>
    <div>
        <img src="{{ asset('/front/images/map.png')}}" class="lg">
        <img src="{{ asset('/front/images/map-xs.png')}}" class="xs">
    </div>
</div>

<!--************************* Features *************************-->

<div class="features" id="features">
    <div class="container text-center">
        <p class="head-title">مميزات الشحن مع يوفيلكس</p>
        <div class="lines">
            <div class="line1"></div>
            <div class="space"></div>
            <div class="line2"></div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="feature-box">
                    <img src="{{ asset('/front/images/features/01_feature.png')}}">
                    <h3>شحن مباشر من المرسل إلي المستلم</h3>
                    <h4>انشأ شحنتك من خلال التطبيق وسيصلك مندوبنا ليستلم الشحنة و يرسلها مباشراً الي المسلتم بدون وسيط</h4>
                </div>
            </div>
            <div class="col-md-4">
                <div class="feature-box">
                    <img src="{{ asset('/front/images/features/02_feature.png')}}">
                    <h3>تتبع شحنتك حتي الوصول</h3>
                    <h4>تستطيع من خلال التطبيق او الموقع تتبع شحنتك لحظة بلحظة حتي يتم تسليمها</h4>
                </div>
            </div>
            <div class="col-md-4">
                <div class="feature-box">
                    <img src="{{ asset('/front/images/features/03_feature.png')}}">
                    <h3>تأمين شامل على الشحنة</h3>
                    <h4>تضمن لك شركة يوفيلكس شحنتك بتأمين %100 من قيمتها</h4>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-4">
                <div class="feature-box">
                    <img src="{{ asset('/front/images/features/04_feature.png')}}">
                    <h3>مخازن وتغليف المنتجات و البضائع</h3>
                    <h4>توفر لك يوفيلكس مخارن لتجميع منتجات وبضائعك .. كل ما عليك فعله هو إرسال طلب الشراء ونقوم بتوصيل شحنتك فوراً وتحصيل ثمنها لك</h4>
                </div>
            </div>
            <div class="col-md-4">
                <div class="feature-box">
                    <img src="{{ asset('/front/images/features/05_feature.png')}}">
                    <h3>التحصيل مع يوفيلكس</h3>
                    <h4>يوفيلكس هتحصلك ثمن شحناتك خلال ٢٤ ساعه من التحصيل لكل شحنة تم تسليمها ..</h4>
                </div>
            </div>
            <div class="col-md-4">
                <div class="feature-box">
                    <img src="{{ asset('/front/images/features/06_feature.png')}}">
                    <h3>تعديل مسار الشحنة</h3>
                    <h4>يوفيلكس بتوفر لعملائها امكانيه تغيير مكان و موعد تسليم الشحنة داخل نفس المحافظة ..</h4>
                </div>
            </div>
        </div>
    </div>
</div>



<!--************************* Services *************************-->

<div class="services" id="services">
    <div class="container text-center">
        <p class="head-title">تعرف علي خدمات يوفيلكس</p>
        <div class="lines">
            <div class="line1"></div>
            <div class="space"></div>
            <div class="line2"></div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="service-box b-l">
                    <img src="{{ asset('/front/images/features/01_feature.png')}}">
                    <h3>شحن الآفراد</h3>
                    <h4>من العميل إلي العميل ،، تقدر تشحن اي حاجه لآي مكان</h4>
                </div>
            </div>
            <div class="col-md-4">
                <div class="service-box">
                    <img src="{{ asset('/front/images/features/01_feature.png')}}">
                    <h3>شحن الشركات</h3>
                    <h4>تقدر ترسل مجموعة شحنات لمجموعة اشخاص في اماكن مخلتفة</h4>
                </div>
            </div>
            <div class="col-md-4">
                <div class="service-box b-r">
                    <img src="{{ asset('/front/images/features/01_feature.png')}}">
                    <h3>من العميل إلي العميل</h3>
                    <h4>تقدر تشحن منتجاتك وبضاعتك بين مخازنك وفروعك</h4>
                </div>
            </div>
        </div>
    </div>
</div>

<!--************************* Get App *************************-->

<div class="get-app" id="get-app">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="content">
                    <p class="head-title">يوفيلكس بتوفرلك اكتر من <br> وسيلة للشحن</p>
                    <h3>من خلال تطبيق الهاتف الاندرويد و الآيفون</h3>
                    <div class="store-btn">
                        <a href=""><img src="{{ asset('/front/images/app-store.png')}}"></a>
                        <a href=""><img src="{{ asset('/front/images/google-play.png')}}"></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="mockup">
                    <img src="{{ asset('/front/images/02_ufelix-app.png')}}"  width="600px">
                </div>
            </div>
        </div>
    </div>
</div>

<hr>
@endsection

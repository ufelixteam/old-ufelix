@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.employees')}}</title>
@endsection

@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.employees')}}
            @if(permission('addEmployee')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
              <a class="btn btn-success pull-right" href="{{ route('mngrAdmin.employees.create') }}"><i class="glyphicon glyphicon-plus"></i> {{__('backend.add_employee')}}</a>
            @endif
        </h3>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($employees->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('backend.name')}}</th>
                            <th>{{__('backend.email')}}</th>
                            <th>{{__('backend.mobile')}}</th>
                            <th>{{__('backend.salary')}}</th>
                            <th>{{__('backend.job_title')}}</th>
                            <th class="text-right"></th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($employees as $employee)
                            <tr>
                                <td>{{$employee->id}}</td>
                                <td>{{$employee->name}}</td>
                                <td>{{$employee->email}}</td>
                                <td>{{$employee->mobile}}</td>
                                <td>{{$employee->salary}}</td>
                                <td>{{$employee->job_title}}</td>
                                <td class="text-right">
                                  @if(permission('showEmployee')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                    <a class="btn btn-xs btn-primary" href="{{ route('mngrAdmin.employees.show', $employee->id) }}"><i class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>
                                  @endif

                                  @if(permission('editEmployee')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                    <a class="btn btn-xs btn-warning" href="{{ route('mngrAdmin.employees.edit', $employee->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                                  @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {!! $employees->render() !!}
            @else
                <h3 class="text-center alert alert-info">{{__('backend.No_result_found')}}</h3>
            @endif

        </div>
    </div>

@endsection

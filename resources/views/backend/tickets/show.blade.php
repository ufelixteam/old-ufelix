@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.ticket')}} - {{__('backend.view')}}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <style>
        h3 {
            margin-bottom: 5px;
            margin-top: 5px;
        }
    </style>
@endsection
@section('header')
    <div class="page-header">
        <h3>{{__('backend.ticket')}}: #{{$ticket->ticket_number}}</h3>
        <h3>{{__('backend.corporate')}}
            : {{!empty($ticket->order->customer->Corporate) ? $ticket->order->customer->Corporate->name : '-'}}</h3>
        <h3>{{__('backend.customer')}}
            : {{!empty($ticket->order->customer) ? $ticket->order->customer->name : '-'}}</h3>
        <h3>{{__('backend.responsible')}}
            : {{!empty($ticket->order->customer->Corporate->responsible_user) ? $ticket->order->customer->Corporate->responsible_user->name : '-'}}</h3>
        {!! $ticket->status_span !!}


        <div class="dropdown  pull-right">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="true">
                <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
            </button>
            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                @if(permission('deleteTickets'))
                    <li>
                        <a data-id="{{$ticket->id}}" id="delete-order"><i
                                class="fa fa-trash"></i> {{__('backend.delete')}}</a>
                    </li>
                @endif
            </ul>
        </div>

    </div>

@endsection

@section('content')


    <div class="row" style="padding: 12px 20px; border-bottom: 1px solid #eee;">
        <div class="alert alert-danger" style="display:none"></div>

        <div class="col-sm-2">
            <label for="agent">{{__('backend.corporate')}}: </label>
            <p class="form-control-static"> {!! ! empty($ticket->corporate) ? '<a href="'.route('mngrAdmin.corporates.show', $ticket->corporate_id).'">'.$ticket->corporate->name.'</a>' : '-' !!}   </p>
        </div>

        <div class="col-sm-2">
            <label for="agent">{{__('backend.Order_Number')}}: </label>
            <p class="form-control-static"> {!! ! empty($ticket->order) ? '<a href="'.route('mngrAdmin.orders.show', $ticket->order_id).'">'.$ticket->order->order_number.'</a>' : '-' !!}   </p>
        </div>

        <!-- Create at: -->
        <div class="col-sm-2">
            <label for="receiver_name">{{__('backend.created_by')}}: </label>
            <p class="form-control-static">{!! $ticket->created_by !!}  </p>
        </div>

        <div class="col-sm-2">
            <label for="reciever_mobile"> {{__('backend.role')}}: </label>
            <p class="form-control-static">
                {{!empty($ticket->role) ? $ticket->role->name : '-'}}
            </p>
        </div>

        <div class="col-sm-2">
            <label for="reciever_mobile"> {{__('backend.assigned_to')}}: </label>
            <p class="form-control-static">
                {{!empty($ticket->assigned_to) ? $ticket->assigned_to->name : '-'}}
            </p>
        </div>

        <div class="col-sm-2">
            <label for="reciever_mobile"> {{__('backend.reference_number')}}: </label>
            <p class="form-control-static">
                {{!empty($ticket->reference_number) ? '#'.$ticket->reference_number : '-'}}
            </p>
        </div>

        <div class="col-sm-12" style="padding: 0">
            <!-- Create at: -->
            <div class="col-sm-2">
                <label for="receiver_name">{{__('backend.Create_at')}}: </label>
                <p class="form-control-static">{{$ticket->created_at}}  </p>
            </div>

            <div class="col-sm-2">
                <label for="reciever_mobile"> {{__('backend.closed_by')}}: </label>
                <p class="form-control-static">
                    {{!empty($ticket->closed_by) ? $ticket->closed_by->name : '-'}}
                </p>
            </div>
            <div class="col-sm-2">
                <label for="reciever_mobile"> {{__('backend.closed_at')}}: </label>
                <p class="form-control-static">
                    {{$ticket->closed_at ? $ticket->closed_at : '-'}}
                </p>
            </div>
        </div>

        {{--        <div class="col-sm-12">--}}
        {{--            <label for="notes"> {{__('backend.closed_comment')}}: </label>--}}
        {{--            <p class="form-control-static">--}}
        {{--                {{$ticket->closed_comment ? $ticket->closed_comment : '-'}}--}}
        {{--            </p>--}}
        {{--        </div>--}}

        <div class="col-sm-12">
            <label for="notes"> {{__('backend.reason')}}: </label>
            <p class="form-control-static">
                {{$ticket->reason ? $ticket->reason : '-'}}
            </p>
        </div>


    </div>
    @if(count($ticket->comments))
        <div class="row" style="padding: 12px 20px;">
            <div class="col-md-12 col-sm-12">
                <h4>{{__('backend.comments')}}</h4>
            </div>
            @foreach($ticket->comments as $comment)
                <div class="col-md-12 col-sm-12" style="margin-top: 20px">
                    <div class="col-md-2 col-sm-2">
                        <img style="width: 25px;display: inline-block;vertical-align: top; margin: 0px 10px"
                             src="{{ $comment->user->image != '' ? $comment->user->image : asset('/images/user.png')}}"
                             class="img-circle" alt="Avatar">
                        <div style="display: inline-block">
                            <span>{!! $comment->user->name !!}</span>:
                            <br>
                            <span>{{$comment->created_at}}</span>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <span>{{$comment->comment}}</span>
                    </div>
                </div>
            @endforeach
        </div>
    @endif

    @if($ticket->status != 1 && (Auth::guard('admin')->user()->id == $ticket->user_id || permission('closeTicket') || Auth::guard('admin')->user()->id == $ticket->assigned_to_id || Auth::guard('admin')->user()->role_admin_id == $ticket->role_id || permission('solveTicket')))
        <div class="row" style="padding: 12px 20px;">
            <form action="{{ route('mngrAdmin.tickets.add_comment', ['id' => $ticket->id]) }}" method="POST"
                  name="validateForm" id="validateForm">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group">
                            <input type="text" class="form-control" id="comment" name="comment" required
                                   value="{{  old("comment") }}">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <button type="submit" class="btn btn-secondary pull-left"><i
                                class="glyphicon glyphicon-plus"></i> {{__('backend.add_comment')}} </button>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        @if(Auth::guard('admin')->user()->id == $ticket->user_id || permission('closeTicket'))
                            <a href="{{ route('mngrAdmin.tickets.change_status', ['status' => 1, 'id' => $ticket->id]) }}"
                               style="margin: 0 10px"
                               class="btn btn-success pull-left"> {{__('backend.close')}} </a>
                        @endif

                        @if(Auth::guard('admin')->user()->id == $ticket->assigned_to_id || Auth::guard('admin')->user()->role_admin_id == $ticket->role_id || permission('solveTicket'))
                            <a href="{{ route('mngrAdmin.tickets.change_status', ['status' => 2, 'id' => $ticket->id]) }}"
                               class="btn btn-warning pull-left"> {{__('backend.solve')}} </a>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    @endif
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

    <script>

        $(function () {
            $("#delete-order").on('click', function () {
                var id = $(this).attr('data-id');
                $.confirm({
                    title: '',
                    theme: 'modern',
                    class: 'danger',
                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Confirm Delete</span></div><div style="font-weight:bold;"><p>Are You sure To Delete This Ticket ?</p></div>',
                    buttons: {
                        confirm: {
                            text: 'Delete Ticket',
                            btnClass: 'btn-danger',
                            action: function () {
                                $.ajax({
                                    url: "{{ url('/mngrAdmin/tickets/')}}" + "/" + id,
                                    type: 'DELETE',
                                    data: {'_token': "{{csrf_token()}}"},
                                    success: function (data) {

                                        if (data != 'false') {

                                            setTimeout(function () {
                                                location.reload();
                                                window.location.href = "{{ url('/mngrAdmin/tickets')}}";
                                            }, 2000);
                                        } else {
                                            jQuery('.alert-danger').append('<p>Something Error</p>');
                                        }
                                        window.scrollTo({top: 0, behavior: 'smooth'});
                                    },
                                    error: function (data) {
                                        console.log('Error:', data);
                                    }
                                });
                            }
                        },
                        cancel: {
                            text: 'Cancel',
                            action: function () {
                            }
                        }
                    }
                });
            })

        });

    </script>

@endsection

<?php


namespace App\Http\Controllers\Backend;


use App\Models\OrderCancelRequest;
use Illuminate\Http\Request;
use Auth;

class OrderCancelRequestController extends Controller
{
    public function __construct(Request $request)
    {
        if ($request->is('mngrAdmin/*') || $request->is('admin/*')) {
            \Config::set('auth.defaults.guard', 'admin');

        } elseif ($request->is('mngrAgent/*') || $request->is('agent/*')) {
            \Config::set('auth.defaults.guard', 'agent');
        }
        $this->guardType = \Config::get('auth.defaults.guard');
    }

    public function index()
    {
        $user = Auth::guard($this->guardType)->user();

        $order_cancel_requests = OrderCancelRequest::with(['order', 'customer' => function ($query) {
            $query->with('Corporate');
        }]);

        if ($user->is_warehouse_user) {
            $order_cancel_requests = $order_cancel_requests->join('customers', 'customers.id', '=', 'order_cancel_requests.customer_id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id);
        }

        $order_cancel_requests = $order_cancel_requests->orderBy('id')->paginate(50);

        return view('backend.order_cancel_requests.index', compact('order_cancel_requests'));
    }
}

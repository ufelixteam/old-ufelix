@extends('backend.layouts.app')
@section('css')
  <title>{{__('backend.resources')}} - {{__('backend.view')}}</title>
@endsection
@section('header')
<div class="page-header">
        <h3>{{__('backend.Resources_No')}} #{{$resource->id}}</h3>
        <form action="{{ route('mngrAdmin.resources.destroy', $resource->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
              @if(permission('editResource')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                  <a class="btn btn-warning btn-group" role="group" href="{{ route('mngrAdmin.resources.edit', $resource->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
              @endif

              @if(permission('deleteResource')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                  <button type="submit" class="btn btn-danger">{{__('backend.delete')}} <i class="glyphicon glyphicon-trash"></i></button>
              @endif
            </div>
        </form>
    </div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">

            <div class="form-group col-md-6 col-sm-6">
                 <label for="name">{{__('backend.name')}}</label>
                 <p class="form-control-static">{{$resource->name}}</p>
            </div>
                <div class="form-group col-md-6 col-sm-6">
                 <label for="email">{{__('backend.email')}}</label>
                 <p class="form-control-static">{{$resource->email}}</p>
            </div>
                <div class="form-group col-md-6 col-sm-6">
                 <label for="started_at">{{__('backend.started_at')}}</label>
                 <p class="form-control-static">{{$resource->started_at}}</p>
            </div>
                <div class="form-group col-md-6 col-sm-6">
                 <label for="renew_at">{{__('backend.renew_at')}}</label>
                 <p class="form-control-static">{{$resource->renew_at}}</p>
            </div>
                <div class="form-group col-md-6 col-sm-6">
                 <label for="cost">{{__('backend.cost')}}</label>
                 <p class="form-control-static">{{$resource->cost}}</p>
            </div>
                <div class="form-group col-md-6 col-sm-6">
                 <label for="website_link">{{__('backend.Website_Link')}}</label>
                 <p class="form-control-static">{{$resource->website_link}}</p>
            </div>
                <div class="form-group col-md-6 col-sm-6">
                 <label for="username">{{__('backend.username')}}</label>
                 <p class="form-control-static">{{$resource->username}}</p>
            </div>
                <div class="form-group col-md-6 col-sm-6">
                 <label for="password">{{__('backend.password')}}</label>
                 <p class="form-control-static">{{$resource->password}}</p>
            </div>

        <div class="col-md-12 col-sm-12">
        <a class="btn btn-link" href="{{ route('mngrAdmin.resources.index') }}"><i class="glyphicon glyphicon-backward"></i>  {{__('backend.back')}}</a>
    </div>

    </div>
</div>

@endsection

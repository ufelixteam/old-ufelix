<!-- **** Modal **** -->
<div class="modal fade" id="moveModal" tabindex="-1" role="dialog" aria-labelledby="moveModal"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{__('backend.move_orders')}}</h5>
            </div>
            <div class="modal-body">
                <form class="" action="{{ url('mngrAdmin/move_collections') }}" method="post"
                      enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" value="" name="ids" id="move_ids"/>
                    <div class="form-group">
                        <div class="row">
                            <div class="form-group col-md-6 col-sm-6">
                                <label>{{__('backend.move_to')}}:</label>
                                <select name="store_id" id="store_id" class="form-control">
                                    @if(! empty($stores))
                                        @foreach($stores as $store)
                                            <option value="{{$store->id}}">{{$store->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-6 col-sm-6">
                                <label>{{__('backend.move_by')}}:</label>
                                <input class="form-control" type="text" name="move_by"
                                       value="">
                            </div>
                            <div class="form-group col-md-6 col-sm-6">
                                <label>{{__('backend.choose_driver')}}:</label>
                                <select name="driver_id" id="driver_id" class="form-control">
                                    @if(! empty($online_drivers))
                                        @foreach($online_drivers as $driver)
                                            <option value="{{$driver->id}}">{{$driver->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-6 col-sm-6">
                                <label>{{__('backend.total_cost')}}:</label>
                                <input class="form-control" type="text" name="cost"
                                       value="">
                            </div>
                            <div class="form-group col-md-6 col-sm-6">
                                <label>{{__('backend.image')}}:</label>
                                <input class="form-control" type="file" name="image"
                                       value="" accept="image/*">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id="saveMove">{{__('backend.move')}}</button>
                        <button type="button" class="btn btn-danger"
                                data-dismiss="modal">{{__('backend.close')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

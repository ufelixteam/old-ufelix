<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller\Backend;
use App\Mail\contectUs;
use App\Mail\message;
use App\Mail\ResetPassword;
use App\Mail\Verified;
use App\Mail\WelcomeCustomer;
use App\Models\City;
use App\Models\Country;
use App\Models\CustomerTaskDay;
use App\Models\Governorate;
use App\Models\Order;
use App\Models\PickUp;
use App\Models\Store;
use App\Models\Verification;
use Auth;
use Carbon\Carbon;
use File;
use Flash;
use Hash;
use Illuminate\Http\Request;
use Response;

//use DB;

class TaskOrderController extends Controller
{

    public function __construct(Request $request)
    {
        $this->middleware('setlocale');
    }

    // Get All Customer
    public function index(Request $request)
    {
        $user = Auth::guard('Customer')->user();
        $collection_orders = PickUp::select('pick_ups.*')
            ->orderBy('pick_ups.id', 'desc');

        if ($user->is_manager) {
            $collection_orders = $collection_orders->where('corporate_id', $user->corporate_id);
        } else {
            $collection_orders = $collection_orders->where('customer_id', $user->id);
        }

        if (app('request')->input('search')) {
            $collection_orders = $collection_orders->where('pickup_number', app('request')->input('search'));
        }

        $collection_orders = $collection_orders->paginate(50);

        return view('frontend.profile.tasks.index', compact('collection_orders'));
    }

    // Added New Task Page
    public function create()
    {
        return view('frontend.profile.tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $user = Auth::guard('Customer')->user();

        $this->validate($request, [
            'captain_received_date' => 'required',
        ]);

        if(!$request->input("is_pickup") && !$request->input("is_recall_orders") && !$request->input("is_cash_collect")){
            Flash::warning(__('backend.choose_task_type'));
            return redirect()->back()->withInput($request->all());
        }

        $date = $request->captain_received_date;

        if ($request->input("is_cash_collect") == 1) {
            $day = Carbon::parse($date)->format('j');
            $dayName = strtolower(Carbon::parse($date)->format('l'));

            $daysCount = CustomerTaskDay::where('customer_id', $user->id)
                ->whereIn('collect_day', [$day, $dayName])
                ->count();

            if (!$daysCount) {
                Flash::warning(__('backend.no_collect_days_available'));
                return redirect()->back()->withInput($request->all());
            }
        }

        $wharehouseOrders = Order::where('customer_id', $user->id)
            ->where('is_refund', 0)
            ->whereIn('status', [5, 8])
            ->where(function ($query) {
                $query->where('warehouse_dropoff', 1)
                    ->orWhere('client_dropoff', 1);
            })
            ->count();

        if (!$wharehouseOrders && $request->input("is_recall_orders") == 1) {
            Flash::warning(__('backend.no_warehouse_orders'));
            return redirect()->back()->withInput($request->all());
        }

        $existPickup = PickUp::where('customer_id', $user->id)
            ->where('status', 0)
            ->whereDate('captain_received_time', $request->captain_received_date)
            ->first();

        if ($existPickup) {
            if ($request->has("notes")) {
                $existPickup->notes = $request->notes;
            }
            if ($request->has("is_cash_collect")) {
                $existPickup->is_cash_collect = $request->input("is_cash_collect");
            }
            if ($request->has("is_pickup")) {
                $existPickup->is_pickup = $request->input("is_pickup");
            }
            if ($request->has("is_recall_orders")) {
                $existPickup->is_recall_orders = $request->input("is_recall_orders");
            }
            if ($request->has("orders_count")) {
                $existPickup->orders_count = $request->input("orders_count");
            }

            $existPickup->save();

            $data['status'] = 'true';
            $data['message'] = __('backend.PickUp_merged_successfully');
            Flash::success(__('backend.PickUp_merged_successfully'));

            return redirect()->route('profile.tasks.index', app()->getLocale() == 'en' ? 'en' : 'ar');
        }

        $pickup = new PickUp();

        $pickup->corporate_id = $user->corporate_id;
        $pickup->customer_id = $user->id;

        $pickup->delivery_price = 0;
        $pickup->bonus_per_order = 0;
        $pickup->is_fake_pickup = 1;
        $pickup->orders_count = $request->input("orders_count");

        $pickup->sender_name = $user->name;
        $pickup->sender_mobile = $user->mobile;
        $pickup->sender_latitude = $user->latitude;
        $pickup->sender_longitude = $user->longitude;
        $pickup->sender_address = ($request->has('sender_address') ? $request->sender_address : $user->address);
        $pickup->s_government_id = $user->government_id;

        $pickup->is_pickup = $request->input("is_pickup");
        $pickup->is_cash_collect = $request->input("is_cash_collect");
        $pickup->is_recall_orders = $request->input("is_recall_orders");

        $pickup->captain_received_time = date('Y-m-d h:i:s', strtotime($request->captain_received_date . ' ' . $request->captain_received_time));

        $pickup->notes = $request->input("notes");

        $number = substr(str_shuffle(str_repeat((time() * 1000), 5)), 0, 10);
        $pickup->pickup_number = $number;
        $pickup->save();

        $data['status'] = 'true';
        $data['message'] = __('backend.PickUp_saved_successfully');
        Flash::success(__('backend.PickUp_saved_successfully'));

        return redirect()->route('profile.tasks.index', app()->getLocale() == 'en' ? 'en' : 'ar');
    }

    public function cancel_pickup($id)
    {
        $pickup = PickUp::where('id', $id)->first();
        if (!empty($pickup)) {
            $pickup->status = 4;
            $pickup->cancelled_at = Carbon::now();
            $pickup->save();
            Flash::success(__('backend.Update_Pickup_saved_successfully'));
            return redirect()->back();
        } else {
            flash(__('backend.PickUp_Not_Found'))->error();
            return redirect()->back();
        }
    }/*cancel_pickup*/
}

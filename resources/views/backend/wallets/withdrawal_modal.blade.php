<!-- Modal -->
<div class="modal fade" id="subModal" tabindex="-1" role="dialog" aria-labelledby="subModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="subModalLabel">Withdrawal Amount From Wallet</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="" method="post">
      <div class="modal-body">

        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="type" value="{{ $type }}" />
        <input type="hidden" name="wallet_id" id="wallet_id" value="0">
        <div class="form-group col-md-12 col-sm-12 @if($errors->has('value')) has-error @endif">
          <label for="subvalue"> Amount: </label>
          <input type="number" min="1" id="subvalue" name="value" class="form-control" value="{{ old("value") }}"/>
          @if($errors->has("value"))
            <span class="help-block">{{ $errors->first("value") }}</span>
          @endif
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </form>
    </div>
  </div>
</div>

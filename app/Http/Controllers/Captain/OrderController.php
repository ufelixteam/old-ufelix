<?php

namespace App\Http\Controllers\Captain;

use App\Events\OrderUpdated;
use App\Http\Controllers\Controller\Backend;
use App\Http\Controllers\SmsController;
use App\Models\AcceptedOrder;
use App\Models\Corporate;
use App\Models\Customer;
use App\Models\Driver;
use App\Models\ForwardOrder;
use App\Models\Order;
use App\Models\OrderComment;
use Auth;
use Config;
use DB;
use ExcelReport;
use Flash;
use Illuminate\Http\Request;
use PDF;
use Redirect;
use Response;
use Validator;


class OrderController extends Controller
{
    private $guardType;

    /*
        order status ::
        0 == > pending
        1 == > accepted
        2 == > received
        3 == > delivered
        4 == > canceled
        5 == > recall
        6 == > waiting
        7 == > dropped
        8 == > reject
    */
    public function __construct(Request $request)
    {
        Config::set('auth.defaults.guard', 'captain');
        $this->guardType = Config::get('auth.defaults.guard');
    }

    public function index(Request $request)
    {

        $user = Auth::guard($this->guardType)->user();
        $type = app('request')->input('type');
        $date = app('request')->input('date');
        $search = app('request')->input('search');
        $from_date = app('request')->input('from_date');
        $to_date = app('request')->input('to_date');
        $driver_id = app('request')->input('driver_id');
        $ownDrivers = Driver::where(function($query) use($user){
            $query->where('id', $user->id)
                ->orWhere('manager_id', $user->id);
        })->pluck('id')->toArray();

        $orders = Order::join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
            ->whereIn('accepted_orders.driver_id', $ownDrivers)
            ->with(['customer' => function ($query) {
                $query->with('Corporate');
            }, 'warehouse' => function ($query) {
                $query->with(['responsibles' => function ($query) {
                    $query->where('is_responsible', 1);
                }]);
            }, 'to_government', 'driver', 'comments'])->select('orders.*')
            ->orderBy('orders.id', 'desc');

        if ($driver_id) {
            $orders = $orders->where('accepted_orders.driver_id', $driver_id);
        }

        if ($from_date) {
            if($type == 'ofd'){
                $orders = $orders->whereDate('orders.received_at', '>=', $from_date);
            }else{
                $orders = $orders->whereDate('orders.created_at', '>=', $from_date);
            }
        }

        if ($to_date) {
            if($type == 'ofd'){
                $orders = $orders->whereDate('orders.received_at', '<=', $to_date);
            }else{
                $orders = $orders->whereDate('orders.created_at', '<=', $to_date);
            }

        }

        if ($type == 'accept') {
            $orders = $orders->where('orders.status', 1)->where('accepted_orders.status', 0);
        } else if ($type == 'receive') {
            $orders = $orders->where('orders.status', 2)->where('accepted_orders.status', 2);
        } else if ($type == 'deliver') {
            $orders = $orders->where('orders.status', 3)->where('accepted_orders.status', 3);
        } else if ($type == 'cancel') {
            $orders = $orders->where('orders.status', 4)->where('accepted_orders.status', 4);
        } else if ($type == 'recall') {
            $orders = $orders->where('orders.status', 5)->where('accepted_orders.status', 5);
        } else if ($type == 'reject') {
            $orders = $orders->where('orders.status', 8)->where('accepted_orders.status', 8);
        } else if ($type == 'ofd') {
            $orders = $orders->where('orders.status', 2)->where('accepted_orders.status', 2);
        } else if ($type == 'finish_not_dropped') {
            $orders = $orders->where(function ($query) {
                $query->where(function ($query) {
                    $query->where('orders.status', 5)
                        ->where('accepted_orders.status', 5)
                        ->where(function ($query) {
                            $query->where('orders.warehouse_dropoff', 0)
                                ->orWhereNull('orders.warehouse_dropoff');
                        });
                })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 8)
                            ->where('accepted_orders.status', 8)
                            ->where(function ($query) {
                                $query->where('orders.warehouse_dropoff', 0)
                                    ->orWhereNull('orders.warehouse_dropoff');
                            });
                    })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 3)
                            ->where('accepted_orders.status', 3)
                            ->where('delivery_status', 2)
                            ->where(function ($query) {
                                $query->where('orders.warehouse_dropoff', 0)
                                    ->orWhereNull('orders.warehouse_dropoff');
                            });
                    })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 3)
                            ->where('accepted_orders.status', 3)
                            ->where('delivery_status', 4)
                            ->where(function ($query) {
                                $query->where('orders.warehouse_dropoff', 0)
                                    ->orWhereNull('orders.warehouse_dropoff');
                            });
                    });
            });
        } else if ($type == 'finish_dropped') {
            $orders = $orders->where(function ($query) {
                $query->where(function ($query) {
                    $query->where('orders.status', 5)
                        ->where('accepted_orders.status', 5)
                        ->where('orders.warehouse_dropoff', 1);
                })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 8)
                            ->where('accepted_orders.status', 8)
                            ->where('orders.warehouse_dropoff', 1);
                    })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 3)
                            ->where('accepted_orders.status', 3)
                            ->where('delivery_status', 2)
                            ->where('orders.warehouse_dropoff', 1);
                    })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 3)
                            ->where('accepted_orders.status', 3)
                            ->where('delivery_status', 4)
                            ->where('orders.warehouse_dropoff', 1);
                    });
            });
        } else if ($type == 'current') {
            $orders = $orders->where(function ($query) {
                $query->where(function ($query) {
                    $query->where('orders.status', 1)
                        ->where('accepted_orders.status', 0);
                })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 2)
                            ->where('accepted_orders.status', 2);
                    })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 5)
                            ->where('accepted_orders.status', 5)
                            ->where('orders.warehouse_dropoff', 0);
                    })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 8)
                            ->where('accepted_orders.status', 8)
                            ->where('orders.warehouse_dropoff', 0);
                    });
//                    ->orWhere(function ($query) {
//                        $query->where('orders.status', 4)
//                            ->where('accepted_orders.status', 4)
//                            ->where('orders.warehouse_dropoff', 0)
//                            ->where('orders.status_before_cancel', '!=', 0);
//                    });
            });
        } else if ($type == 'finish') {
            $orders = $orders->where(function ($query) {
                $query->where(function ($query) {
                    $query->where('orders.status', 3)
                        ->where('accepted_orders.status', 3);
                })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 5)
                            ->where('accepted_orders.status', 5)
                            ->where('orders.warehouse_dropoff', 1);
                    })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 8)
                            ->where('accepted_orders.status', 8)
                            ->where('orders.warehouse_dropoff', 1);
                    });
//                    ->orWhere(function ($query) {
//                        $query->where('orders.status', 4)
//                            ->where('accepted_orders.status', 4)
//                            ->where('orders.warehouse_dropoff', 1)
//                            ->where('orders.status_before_cancel', '!=', 0);
//                    });
            });
        }

        if (isset($search) && $search != "") {
            $orders = $orders->where(function ($q) use ($search) {
                $q->where('orders.id', 'like', '%' . $search . '%');
                $q->OrWhere('orders.order_number', 'like', '%' . $search . '%');
                $q->OrWhere('orders.receiver_name', 'like', '%' . $search . '%');
                $q->OrWhere('orders.receiver_mobile', 'like', '%' . $search . '%');
                $q->orWhere('orders.sender_mobile', 'like', '%' . $search . '%');
                $q->orWhere('orders.sender_name', 'like', '%' . $search . '%');
                $q->orWhere('orders.order_no', 'like', '%' . $search . '%');
            });
        }
        if (isset($date) && $date != "") {
            $orders = $orders->where('orders.created_at', 'like', $date . '%');
        }

        $orders = $orders->orderBy('orders.id', 'desc')->paginate(50);

        $online_drivers = Driver::where('is_active', 1)
            ->where(function ($query) use ($user) {
                $query->where('manager_id', $user->id)
                    ->Orwhere('id', $user->id);
            })
            ->select('drivers.*')
            ->orderBy('drivers.name', 'ASC')
            ->get();

        if ($request->ajax()) {
            return [
                'view' => view(($type == 'ofd' ? 'captain.orders.ofd_table' : 'captain.orders.table'), compact('orders', 'type', 'online_drivers'))->render(),
            ];
        }

        return view('captain.orders.index', compact('orders', 'type', 'online_drivers'));
    }/* list orders index*/

    public function all_orders(Request $request)
    {
        $user = Auth::guard($this->guardType)->user();

        $status = app('request')->input('status');
        $from_date = app('request')->input('fromDate');
        $to_date = app('request')->input('toDate');
        $search = app('request')->input('search');
        $type = '';
        $ownDrivers = Driver::where(function($query) use($user){
            $query->where('id', $user->id)
                ->orWhere('manager_id', $user->id);
        })->pluck('id')->toArray();


        $orders = Order::join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
            ->whereIn('accepted_orders.driver_id', $ownDrivers)
            ->with(['customer' => function ($query) {
                $query->with('Corporate');
            }, 'warehouse' => function ($query) {
                $query->with(['responsibles' => function ($query) {
                    $query->where('is_responsible', 1);
                }]);
            }, 'to_government', 'driver'])->orderBy('orders.id', 'desc')
            ->whereIn('orders.status', [1, 2, 3, 5, 8])
            ->whereIn('accepted_orders.status', [0, 2, 3, 5, 8]);

        if (isset($search) && $search != "") {
            $orders = $orders->where(function ($q) use ($search) {
                $q->where('orders.id', 'like', '%' . $search . '%');
                $q->OrWhere('orders.order_number', 'like', '%' . $search . '%');
                $q->OrWhere('orders.receiver_name', 'like', '%' . $search . '%');
                $q->OrWhere('orders.receiver_mobile', 'like', '%' . $search . '%');
                $q->orWhere('orders.sender_mobile', 'like', '%' . $search . '%');
                $q->orWhere('orders.sender_name', 'like', '%' . $search . '%');
                $q->orWhere('orders.order_no', 'like', '%' . $search . '%');
            });
        }

        if ($status || $status === '0') {
            $orders->where('orders.status', $status);
        }

        if ($from_date) {
            $orders = $orders->whereDate('orders.created_at', '>=', $from_date);
        }

        if ($to_date) {
            $orders = $orders->whereDate('orders.created_at', '<=', $to_date);
        }

        $orders = $orders->select('orders.*')->paginate(50);

        if (app('request')->has('status')) {
            if ($status == 1) {
                $type = 'accept';
            } else if ($status == 2) {
                $type = 'receive';
            } else if ($status == 3) {
                $type = 'deliver';
            } else if ($status == 4) {
                $type = 'cancel';
            } else if ($status == 5) {
                $type = 'recall';
            } else if ($status == 6) {
                $type = 'waiting';
            } else if ($status == 7) {
                $type = 'dropped';
            } else if ($status == 8) {
                $type = 'reject';
            }

        }

        return view('captain.orders.all_orders', compact('orders', 'type'));
    }

    public function show($id)
    {
        /** this fn used to display order details */
        $order = Order::findOrFail($id);

        $user = Auth::guard($this->guardType)->user();

        return view('captain.orders.show', compact('order'));

    }/*show*/

    public function print_order_pdf_byid($order_id)
    {
        $order = Order::where('id', $order_id)->first();
        $customer = Customer::where('id', $order->customer_id)->value('corporate_id');
        $corporteName = Corporate::where('id', $customer)->value('name');
        return view('captain.orders.pdf', compact('order', 'corporteName'));
    }/*print_order_pdf_byid*/

    public function addOrderComment(Request $request)
    {

        $message = '';
        DB::beginTransaction();
        try {

            $data = $request->only('order_id', 'latitude', 'longitude', 'driver_id', 'comment', 'created_at', 'user_type');
            if (!$request->created_at) {
                unset($data['created_at']);
            }
            if (!$request->driver_id) {
                $data['driver_id'] = Auth::guard($this->guardType)->user()->id;
            }
            $data['user_id'] = Auth::guard($this->guardType)->user()->id;

            OrderComment::create($data);

            event(new OrderUpdated([$request->order_id],
                Auth::guard($this->guardType)->user()->id,
                3,
                'comments',
                ['comment' => $request->get('comment')]
            ));

//            event(new OrderUpdated([$request->order_id],
//                Auth::guard($this->guardType)->user()->id,
//                1,
//                'comments',
//                ['comment' => $request->get('comment')]
//            ));

            if ($request->ajax()) {
                $message = __('captain.add_order_comment_successfully');
            } else {
                Flash::success(__('captain.add_order_comment_successfully'));
            }

            DB::commit();

        } catch (Exception $e) {
            if ($request->ajax()) {
                $message = __('captain.Something_Error');
            } else {
                Flash::error(__('captain.Something_Error'));
            }

            DB::rollback();
        }

        if ($request->ajax()) {
            return response()->json(['message' => $message]);
        } else {
            return redirect()->back();
        }
    }

    public function saveStatus(Request $request)
    {

        $message = '';
        DB::beginTransaction();
        try {
            $statusType = '';
            $details = [];
            $comment = $request->comment;
            $collected_cost = $request->collected_cost;
            $delivery_status = $request->delivery_status;
            $order_id = $request->order_id;
            $driver_id = Auth::guard($this->guardType)->user()->id;
            $ownDrivers = Driver::where(function($query) use($driver_id){
                $query->where('id', $driver_id)
                    ->orWhere('manager_id', $driver_id);
            })->pluck('id')->toArray();
            $acceptedOrderData = [];
            $orderData = [];

            $order = Order::join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
                ->whereIn('accepted_orders.driver_id', $ownDrivers)
                ->where('accepted_orders.status', 2)
                ->where('orders.status', 2)
                ->where('orders.id', $order_id)
                ->select('orders.*')
                ->first();

            if (!$order) {
                return response()->json(['message' => __('captain.have_not_permission_on_order'), 'error' => true]);
            }

            if (($delivery_status == 3 || $delivery_status == 5) && !$comment) {
                return response()->json(['message' => __('captain.must_write_comment'), 'error' => true]);
            }

            $orderData['delivery_status'] = (int) $delivery_status;
            $orderData['collected_cost'] = $collected_cost;
            if ($orderData['delivery_status'] == 1 || $orderData['delivery_status'] == 2 || $orderData['delivery_status'] == 4) {
                if ($orderData['delivery_status'] == 1){
                    $statusType = 'deliver';
                }elseif($orderData['delivery_status'] == 2){
                    $statusType = 'part_delivered';
                }elseif($orderData['delivery_status'] == 4){
                    $statusType = 'replace';
                }
                $orderData['status'] = 3;
                $orderData['delivered_at'] = date("Y-m-d H:i:s");
                $acceptedOrderData['status'] = 3;
                $acceptedOrderData['delivery_date'] = date("Y-m-d H:i:s");
            } elseif ($orderData['delivery_status'] == 3) {
                $statusType = 'recall';
                $orderData['status'] = 5;
                $orderData['recalled_at'] = date("Y-m-d H:i:s");
                $orderData['recalled_by'] = 2;
                $acceptedOrderData['status'] = 8;
            } elseif ($orderData['delivery_status'] == 5) {
                $statusType = 'reject';
                $details['recalled_by'] = 2;
                $orderData['status'] = 8;
                $orderData['rejected_at'] = date("Y-m-d H:i:s");
                $acceptedOrderData['status'] = 8;
            }

            AcceptedOrder::where('order_id', $order_id)
                ->where('status', 2)
                ->update($acceptedOrderData);


            Order::where('id', $order_id)
                ->where('status', 2)
                ->update($orderData);

            if ($comment) {
                OrderComment::create([
                    'comment' => $comment,
                    'order_id' => $order_id,
                    'user_type' => 3,
                    'user_id' => Auth::guard($this->guardType)->user()->id,
                    'driver_id' => Auth::guard($this->guardType)->user()->id,
                    'latitude' => 0,
                    'longitude' => 0
                ]);

                event(new OrderUpdated([$request->order_id],
                    Auth::guard($this->guardType)->user()->id,
                    3,
                    'comments',
                    ['comment' => $request->get('comment')]
                ));

            }

            event(new OrderUpdated([$request->order_id],
                Auth::guard($this->guardType)->user()->id,
                3,
                $statusType,
                $details
            ));

            $message = __('captain.order_updated_successfully');

            DB::commit();

        } catch (Exception $e) {
            $message = __('captain.Something_Error');

            DB::rollback();
        }

        return response()->json(['message' => $message]);
    }

    public function scan_print(Request $request)
    {
        $ids = explode(',', app('request')->input('ids'));
        if (is_array($ids) && count($ids)) {
            $ids_ordered = implode(',', $ids);

            $driver = Driver::find($request->driver_id);
            $orders = Order::with(['customer' => function ($query) {
                $query->with('corporate');
            }])->whereIn('id', $ids)->orderByRaw("FIELD(id, $ids_ordered)")->get();

            $pdf = PDF::loadView('captain.orders.pdf-scan', compact('orders', 'driver'), [], ['format' => 'A4-L']);
            return $pdf->stream(time());

        }
    }

    public function forward(Request $request)
    {
        $ids = explode(',', app('request')->input('ids'));

        if (!isset($request->ids) && empty ($ids) && !app('request')->input('driver_id')) {
            Flash::warning(__('backend.Please_Select_Any_Order'));
            return redirect()->back();
        }

        if (is_array($ids) && count($ids)) {
            $orders = Order::whereIn('id', $ids)->get();
            AcceptedOrder::whereIn('order_id', $ids)->update(
                [
                    'status' => 4
                ]
            );
            foreach ($orders as $key => $order) {
                AcceptedOrder::create(
                    [
                        'order_id' => $order->id,
                        'driver_id' => app('request')->input('driver_id'),
                        'status' => 2,
                        'old_status' => 2,
                        'delivery_price' => $order->delivery_price
                    ]
                );

            }

            Order::whereIn('id', $ids)->update(['received_at' => date('Y-m-d H:i:s')]);

            event(new OrderUpdated($ids, Auth::guard($this->guardType)->user()->id, 3, 'receive_orders'));

            Flash::success(__('backend.Order_Routed_saved_successfully'));
        }
        return back();
    }/* Forward Orders To Agent Or Driver function*/
}

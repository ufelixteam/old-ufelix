@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.Shipment_Orders')}}</title>
@endsection

@section('header')
  <div class="page-header clearfix">
    <h3>
      <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.Shipment_Tool_Orders')}}
      @if(permission('addShipmentOrders')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
        <a class="btn btn-success pull-right" href="{{ route('mngrAdmin.shipment_tool_orders.create') }}"><i class="glyphicon glyphicon-plus"></i> {{__('backend.Add_Shipment_Orders')}}</a>
      @endif
    </h3>
  </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
      @if($shipment_tool_orders->count())
        <table class="table table-condensed table-striped">
          <thead>
            <tr>
              <th>#{{__('backend.id')}}</th>
              <th>{{__('backend.orgnization')}}</th>
            	<th>{{__('backend.notes')}}</th>
            	<th>{{__('backend.status')}}</th>
              <th class="text-right"></th>
            </tr>
          </thead>
          <tbody>
            @foreach($shipment_tool_orders as $shipment_tool_order)
              <tr>
                <td>{{$shipment_tool_order->id}}</td>
                @if($shipment_tool_order->type == 1)
                  <td><strong>Customer:</strong> {{! empty($shipment_tool_order->customer) ? $shipment_tool_order->customer->name : ''}}</td>
                @elseif($shipment_tool_order->type == 2)
                <td><strong>Agent:</strong> {{! empty($shipment_tool_order->agent) ? $shipment_tool_order->agent->name : ''}}</td>
                @endif
    			      <td>{{$shipment_tool_order->notes}}</td>
    			      <td>{!! $shipment_tool_order->status_span !!}</td>
                <td class="text-right">
                @if(permission('showShipmentOrders')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                    <a class="btn btn-xs btn-primary" href="{{ route('mngrAdmin.shipment_tool_orders.show', $shipment_tool_order->id) }}"><i class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>
                @endif
                @if(permission('editShipmentOrders')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                    <a class="btn btn-xs btn-warning" href="{{ route('mngrAdmin.shipment_tool_orders.edit', $shipment_tool_order->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                @endif
                  <!--<form action="{{ route('mngrAdmin.shipment_tool_orders.destroy', $shipment_tool_order->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                  </form>-->
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>

        {!! $shipment_tool_orders->appends($_GET)->links() !!}

      @else
          <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
      @endif
    </div>
  </div>
@endsection
@section('scripts')
<script type="text/javascript">
$('#theTable').DataTable({
  "pagingType": "full_numbers"
});
</script>
@endsection

<!doctype html>
<html lang="en">

<head>
	<title>Dashboard</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	

	@include('agent.layouts.css')
	@yield('css')

</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		@include('agent.layouts.navbar')

		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		@include('agent.layouts.sidemenu')
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					
					<div class="panel ">
				
						@yield('header')
						<div class="text-center col-md-10 col-md-offset-1 " >
							@include('flash::message')
						</div>


						<div class="panel-body">
							@yield('content')
						</div>
					</div>
					
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<footer>
			<div class="container-fluid">
				<p class="copyright">&copy; 2019 <a href="#" target="_blank">Ufelix</a>. All Rights Reserved.</p>
			</div>
		</footer>
	</div>
	@include('agent.layouts.js')
	@yield('scripts')

</body>

</html>

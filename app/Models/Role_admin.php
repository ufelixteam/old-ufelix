<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role_admin extends Model
{

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'role_admin_id', 'id');
    }

    public function permations()
    {
        return $this->hasMany('App\Models\Permission_admin', 'role_admin_id', 'id');
    }

}

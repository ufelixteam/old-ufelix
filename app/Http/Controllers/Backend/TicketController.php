<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use App\Jobs\CloseTickets;
use App\Jobs\GenerateTickets;
use App\Models\Corporate;
use App\Models\InvoiceTicket;
use App\Models\Order;
use App\Models\Role_admin;
use App\Models\Ticket;
use App\Models\TicketComment;
use App\Models\TicketOrder;
use App\Models\User;
use Auth;
use Flash;
use Illuminate\Http\Request;
use Response;

class TicketController extends Controller
{

    public function __construct(Request $request)
    {
        if ($request->is('mngrAdmin/*') || $request->is('admin/*')) {
            \Config::set('auth.defaults.guard', 'admin');

        } elseif ($request->is('mngrAgent/*') || $request->is('agent/*')) {
            \Config::set('auth.defaults.guard', 'agent');
        }
        $this->guardType = \Config::get('auth.defaults.guard');
    }

    public function index(Request $request)
    {
        $user = Auth::guard($this->guardType)->user();

        $users = User::/*where('role_id', 1)->*/ where('is_active', 1)
            ->select('users.*')
            ->orderBy('users.name', 'ASC');

        $corporates = Corporate::where('is_active', 1)->select('corporates.*')
            ->orderBy('corporates.name', 'ASC');

        $tickets = Ticket::with(['user', 'order', 'closed_by', 'corporate'])
            ->select('tickets.*')
            ->orderBy('tickets.id', 'desc');

        if ($user->is_warehouse_user) {
            $tickets = $tickets->join('store_user', 'store_user.store_id', '=', 'tickets.user_id')
                ->where('user_id', $user->id);

            $users = $users->join('store_user', 'store_user.store_id', '=', 'users.id')
                ->where('user_id', $user->id);

            $corporates = $corporates->join('customers', 'customers.corporate_id', '=', 'corporates.id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id);
        }

        if (app('request')->input('status') || app('request')->input('status') == '0') {
            $tickets = $tickets->where('tickets.status', app('request')->input('status'));
        }

        if (app('request')->input('user_id')) {
            $tickets = $tickets->where('user_id', app('request')->input('user_id'));
        }

        if (app('request')->filled('created_by_type')) {
            if($request->created_by_type == 'system'){
                $tickets = $tickets->where('is_manual', 0)->whereNotNull('type');
            }elseif($request->created_by_type == 'manual'){
                $tickets = $tickets->where('is_manual', 1);
            }elseif($request->created_by_type == 'client_comment'){
                $tickets = $tickets->where('is_manual', 0)->whereNull('type');
            }
        }

        if (app('request')->input('assigned_to')) {
            $tickets = $tickets->where('assigned_to_id', app('request')->input('assigned_to'));
        }

        if (app('request')->input('closed_by')) {
            $tickets = $tickets->where('closed_by_id', app('request')->input('closed_by'));
        }

        if (app('request')->input('date')) {
            $tickets = $tickets->whereDate('tickets.created_at', app('request')->input('date'));
        }

        if (app('request')->input('corporate_id')) {
            $tickets = $tickets->join('customers', 'customers.id', '=', 'orders.customer_id')
                ->where("customers.corporate_id", app('request')->input('corporate_id'));
        }

        if (app('request')->input('search')) {
            $tickets = $tickets->where(function ($q) {
                $search = app('request')->input('search');

                $q->where('tickets.ticket_number', $search);
//                $q->OrWhere('orders.id', 'like', '%' . $search . '%');
//                $q->OrWhere('orders.order_number', 'like', '%' . $search . '%');
//                    $q->OrWhere('orders.receiver_name', 'like', '%' . $search . '%');
//                    $q->OrWhere('orders.receiver_mobile', 'like', '%' . $search . '%');
//                    $q->orWhere('orders.sender_mobile', 'like', '%' . $search . '%');
//                    $q->orWhere('orders.sender_name', 'like', '%' . $search . '%');
//                $q->orWhere('orders.order_no', 'like', '%' . $search . '%');

            });
        }

        $tickets = $tickets->paginate(50);

        $users = $users->get();
        $corporates = $corporates->get();
        $roles = Role_admin::all();

        if ($request->ajax()) {
            return [
                'view' => view('backend.tickets.table', compact('tickets', 'users', 'corporates', 'roles'))->render(),
            ];
        }
        return view('backend.tickets.index', compact('tickets', 'users', 'corporates', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.tickets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'order_id' => 'required_if:ticket_model,==,1',
            'corporate_id' => 'required_if:ticket_model,==,2',
            'reason' => 'required'
        ]);

        $user = Auth::guard($this->guardType)->user();

        $ticket = new Ticket();

        $order_id = $request->input("order_id");
        if ($order_id) {
            $order = Order::where('orders.id', 'like', '%' . $order_id . '%')
                ->OrWhere('orders.order_number', 'like', '%' . $order_id . '%')
                ->orWhere('orders.order_no', 'like', '%' . $order_id . '%')
                ->first();

            if ($order) {
                $ticket->order_id = $order->id;
            } else {
                flash(__('backend.order_notfound'))->error();
            }

        }

        $ticket->ticket_model = $request->input("ticket_model");
        $ticket->user_id = $user->id;
        $ticket->ticket_number = random_number(2, $user->id);
        $ticket->reason = $request->input("reason");
        $ticket->corporate_id = $request->input("corporate_id");
        $ticket->assigned_to_id = $request->input("assigned_to_id");
        $ticket->reference_number = $request->input("reference_number");
        $ticket->role_id = $request->input("role_id");
        $ticket->is_manual = 1;

        $ticket->save();

        Flash::success(__('backend.Ticket_saved_successfully'));


        return redirect()->back()->withInput($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $ticket = Ticket::with(['comments' => function ($query) {
            $query->with('user');
        }])->findOrFail($id);

        return view('backend.tickets.show', compact('ticket'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $ticket = Ticket::findOrFail($id);

        return view('backend.tickets.edit', compact('ticket'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @param Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'order_id' => 'required',
            'reason' => 'required',
            'status' => 'required',
        ]);

        $ticket = Ticket::findOrFail($id);

        $ticket->reason = $request->input("reason");
        $ticket->status = $request->input("status");
        $ticket->closed_comment = $request->input("closed_comment");

        $ticket->save();

        Flash::warning(__('backend.Ticket_updated_successfully'));

        return redirect()->route('mngrAdmin.tickets.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $ticket = Ticket::findOrFail($id);
        if ($ticket) {
            $ticket->delete();

            flash(__('backend.Ticket_deleted_successfully'))->error();
            addActivityLog('Delete Ticket #No ' . $ticket->id, ' حذف  شكوى #رقم ' . $ticket->id, 36); // Helper Function in App\Helpers.php
            return 'true';

        }
    }

    public function close_ticket(Request $request)
    {
        $user = Auth::guard($this->guardType)->user();

        foreach ($request->ids as $id) {
            Ticket::where('id', $id)->update([
                'status' => 1,
                'closed_at' => date('Y-m-d H:i:s'),
                'closed_comment' => $request->comment,
                'closed_by_id' => $user->id
            ]);
        }
        return response()->json(true);

    }

    public function generate_tickets(Request $request)
    {
        GenerateTickets::dispatch();

        Flash::warning(__('backend.Ticket_updated_successfully'));

        return redirect()->route('mngrAdmin.tickets.index');
    }

    public function close_tickets(Request $request)
    {
        CloseTickets::dispatch();

        Flash::warning(__('backend.Ticket_updated_successfully'));

        return redirect()->route('mngrAdmin.tickets.index');
    }

    public function add_comment($id, Request $request)
    {
        $user = Auth::guard($this->guardType)->user();

        TicketComment::create(['user_id' => $user->id, 'comment' => $request->comment, 'ticket_id' => $id]);

        Flash::warning(__('backend.Ticket_updated_successfully'));

        return redirect()->back();
    }

    public function change_status($id, Request $request)
    {
        $user = Auth::guard($this->guardType)->user();

        $data = ['status' => $request->status];
        if ($request->status == 1) {
            $data['closed_by_id'] = $user->id;
            $data['closed_at'] = date('Y-m-d H:i:s');
        }

        Ticket::where('id', $id)->update($data);

        Flash::warning(__('backend.Ticket_updated_successfully'));

        return redirect()->back();
    }
}

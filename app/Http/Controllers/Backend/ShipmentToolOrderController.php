<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Agent;
use App\Models\ShipmentToolOrder;
use App\Models\ShipmentOrderItem;
use App\Models\ShipmentTool;
use Response;
use Flash;
use File;
use Auth;
use DB;

class ShipmentToolOrderController extends Controller {

	private $guardType;

	public function __construct(Request $request)
    {
    	if($request->is('mngrAdmin/*') || $request->is('admin/*') ){
    		\Config::set('auth.defaults.guard','admin');

    	}elseif ($request->is('mngrAgent/*') || $request->is('agent/*')) {
    		\Config::set('auth.defaults.guard','agent');
    	}
        $this->guardType = \Config::get('auth.defaults.guard');
    }

	// Clone New Field Shipment Tool And Quantity
	public function new_shippeng_field() {
		$shipment_tools = ShipmentTool::get();
		return [
	        'view' => view('agent.shipment_tool_orders.fields', compact('shipment_tools'))->render(),
	  ];
	}

	// Get All Shipment Tool Orders
	public function index() {
		$user = Auth::guard($this->guardType)->user();
    if ($user->role_id  == '1') {
			$shipment_tool_orders = ShipmentToolOrder::orderBy('id', 'desc')->with('ShipmentTool')->paginate('15');
			return view('backend.shipment_tool_orders.index', compact('shipment_tool_orders'));
		} else {
				 	$shipment_tool_orders = ShipmentOrderItem::select(
	 				'shipment_order_items.status', 'shipment_tool_orders.notes' ,'shipment_tool_orders.id as shipitemordes_id',
	 				'shipment_order_items.quantity' ,'shipment_tools.price' ,'shipment_tool_orders.created_at')->
					join('shipment_tools', 'shipment_tools.id', '=', 'shipment_order_items.tool_id')->
				 	join('shipment_tool_orders', 'shipment_tool_orders.id', '=', 'shipment_order_items.shipment_order_id')->
		 			where('shipment_tool_orders.object_id', '=', $user->agent_id)->
				 	where('shipment_tool_orders.type', '=', 2)->paginate('15');
					return view('agent.shipment_tool_orders.index', compact('shipment_tool_orders'));
		}
	}

	// Create Shipment Tool Orders Page
	public function create() {
			$user 							= Auth::guard($this->guardType)->user();
    	$agents 						= Agent::orderBy('name', 'ASC')->get();
			$shipment_tools 		= ShipmentTool::get();
    	if ($user->role_id  == '1'){
				return view('backend.shipment_tool_orders.create',compact('agents','shipment_tools'));
			}else{
				return view('agent.shipment_tool_orders.create',compact('agents','shipment_tools'));
			}
	}

	// 	Added New Shipment Tool Orders
	public function store(Request $request) {
		/*
		* type = 1 :: Customer
		* type = 2 :: Agent
		*
		* object_id :: Agent Or Customer
		*
		*/

		$v = \Validator::make($request->all(), [
				'agent_id'  	=> 'required',
				'status'  		=> 'required',
				'quantity'  	=> 'required',
				'items'  			=> 'required',
				'address'  		=> 'required',
		]);

		if ($v->fails()) {
				return redirect()->back()->withErrors($v->errors());
		}

		$user 																= Auth::guard($this->guardType)->user();
		DB::beginTransaction();
    	try {
			$shipment_tool_order							 	= new ShipmentToolOrder();
			if ($user->role_id  == '1'){
				$shipment_tool_order->object_id 	= $request->input("agent_id");
			}else{
				$shipment_tool_order->object_id 	= Auth::guard($this->guardType)->user()->agent_id;
			}
			$shipment_tool_order->type 					= 2;
			$shipment_tool_order->address 			= $request->input("address");
			$shipment_tool_order->notes 				= $request->input("notes");
			$shipment_tool_order->save();

			// Function Add Action In Activity Log By Samir
			addActivityLog('Add New Shipment Tool Order #No ' . $shipment_tool_order->id , ' اضافة اداة شحن طلب جديد رقم ' . $shipment_tool_order->id, 10); // Helper Function in App\Helpers.php

			if (count($request->items) > 0 ) {
				$i = 0;
				foreach ($request->items as $item) {
					$order_item 											= new ShipmentOrderItem();
					$order_item->shipment_order_id 		= $shipment_tool_order->id;
					$order_item->quantity 						= $request->quantity[$i];
					$order_item->tool_id 							= $item;
					$order_item->save();
					$i ++;
				}
			}

			Flash::success(__('backend.ShipmentToolOrder_saved_successfully'));
			DB::commit();
			} catch (Exception $e) {
				DB::rollback();
			}
	   		$user =Auth::guard($this->guardType)->user();
	   	if ($user->role_id  == '1'){
			return redirect()->route('mngrAdmin.shipment_tool_orders.index');
		} else {
			return redirect()->route('mngrAgent.shipment_tool_orders.index');
		}
	}

	// View One Shipment Tool Orders
	public function show($id) {
			$user = Auth::guard($this->guardType)->user();

	    if ($user->role_id  == '1'){
				$shipment_tool_order 		= ShipmentToolOrder::findOrFail($id);
				return view('backend.shipment_tool_orders.show', compact('shipment_tool_order'));
			} else {
			$shipment_tool_order = ShipmentToolOrder::select(
				'shipment_tool_orders.object_id', 'shipment_order_items.status', 'shipment_tool_orders.total_price',
				'shipment_tool_orders.notes' ,'shipment_tool_orders.id as shipitemordes_id',
				'shipment_order_items.quantity' ,'shipment_tools.price' ,'shipment_tools.name_en')->
				 join('shipment_order_items', 'shipment_tool_orders.id', '=', 'shipment_order_items.shipment_order_id')->
				 join('shipment_tools', 'shipment_tools.id', '=', 'shipment_order_items.tool_id')->
				 where('shipment_order_items.shipment_order_id', '=', $id)->get();

			return view('agent.shipment_tool_orders.show', compact('shipment_tool_order' ,'id'));
		}
	}

	// Edit Shipment Tool Orders Page
	public function edit($id) {
		$user 								= Auth::guard($this->guardType)->user();
  	$agents 							= Agent::orderBy('name', 'ASC')->get();
		$shipment_tools 			= ShipmentTool::get();
		$shipment_tool_order 	= ShipmentToolOrder::findOrFail($id);

  	if ($user->role_id  == '1'){
				return view('backend.shipment_tool_orders.edit', compact('shipment_tool_order','shipment_tools','agents'));
		} else {
				return view('agent.shipment_tool_orders.edit', compact('shipment_tool_order','shipment_tools'));
		}
	}

	// Update Shipment Tool Orders
	public function update(Request $request, $id)
	{

		$v = \Validator::make($request->all(), [
				'agent_id'  	=> 'required',
				'status'  		=> 'required',
				'quantity'  	=> 'required',
				'items'  			=> 'required',
				'address'  		=> 'required',
		]);

		if ($v->fails()) {
				return redirect()->back()->withErrors($v->errors());
		}

		$user 																= Auth::guard($this->guardType)->user();
		DB::beginTransaction();
		try {
				$shipment_tool_order 							= ShipmentToolOrder::findOrFail($id);
				$shipment_tool_order->object_id 	= $request->input("agent_id");
		    $shipment_tool_order->notes 			= $request->input("notes");
		    $shipment_tool_order->status 			= $request->input("status");
				$shipment_tool_order->address 	  = $request->input("address");
				$shipment_tool_order->save();

				// Function Add Action In Activity Log By Samir
				addActivityLog('Edit Shipment Tool Order #No ' . $shipment_tool_order->id , ' تعديل اداة شحن الطلب رقم ' . $shipment_tool_order->id, 10); // Helper Function in App\Helpers.php

			if (count($request->items)>0 ) {
				$delete = ShipmentOrderItem::where('shipment_order_id',$shipment_tool_order->id)->delete();
				$i =0;
				foreach ($request->items as $item) {
					$order_item 											= new ShipmentOrderItem();
					$order_item->shipment_order_id 		= $shipment_tool_order->id;
					$order_item->quantity 						= $request->quantity[$i];
					$order_item->tool_id 							= $item;
					$order_item->save();
					$i ++;
				}
			}

			DB::commit();
			Flash::warning(__('backend.ShipmentToolOrder_Updated_successfully'));
		} catch (Exception $e) {
			DB::rollback();
		}

		if ($user->role_id  == '1'){
			return redirect()->route('mngrAdmin.shipment_tool_orders.index');
		} else {
			return redirect()->route('mngrAgent.shipment_tool_orders.index');
		}
	}

	// Delete Shipment Tool Orders
	public function destroy($id) {
		$shipment_tool_order = ShipmentToolOrder::findOrFail($id);
		$shipment_tool_order->delete();

		// Function Add Action In Activity Log By Samir
		addActivityLog('Delete Shipment Tool Order #No ' . $shipment_tool_order->id , ' حذف اداة شحن الطلب رقم ' . $shipment_tool_order->id, 10); // Helper Function in App\Helpers.php

  	flash(__('backend.ShipmentToolOrder_deleted_successfully'))->error();
		if ($user->role_id  == '1'){
			return redirect()->route('mngrAdmin.shipment_tool_orders.index');
		}else{
			return redirect()->route('mngrAgent.shipment_tool_orders.index');
		}
	}
}

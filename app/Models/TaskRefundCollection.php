<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskRefundCollection extends Model
{

    protected $table = 'task_refund_collections';

    protected $guarded = ['id'];

    public function refund_collection()
    {
        return $this->belongsTo('App\Models\RefundCollection', 'refund_collection_id');
    }
}

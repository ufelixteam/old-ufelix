<!-- **** Modal **** -->
<div class="modal fade" id="delayModal" tabindex="-1" role="dialog" aria-labelledby="delayModal"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{__('backend.delay_orders')}}</h5>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="delay_comment" value="{{__('backend.delay_1')}}" checked>
                        <label class="form-check-label" for="exampleRadios1">
                            {{__('backend.delay_1')}}
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="delay_comment" value="{{__('backend.delay_2')}}">
                        <label class="form-check-label" for="exampleRadios2">
                            {{__('backend.delay_2')}}
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="delay_comment" value="{{__('backend.delay_3')}}">
                        <label class="form-check-label" for="exampleRadios1">
                            {{__('backend.delay_3')}}
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="delay_comment" value="{{__('backend.delay_4')}}">
                        <label class="form-check-label" for="exampleRadios2">
                            {{__('backend.delay_4')}}
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="delay_comment" value="{{__('backend.delay_5')}}">
                        <label class="form-check-label" for="exampleRadios1">
                            {{__('backend.delay_5')}}
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="delay_comment" value="{{__('backend.other_reason')}}">
                        <label class="form-check-label" for="exampleRadios2">
                            {{__('backend.other_reason')}}
                        </label>
                    </div>
                </div>
                <div class="form-group" style="display: none" id="delay_reason">
                    <label>{{__('backend.reason')}}:</label>
                    <input name="del_reason" id="del_reason" class="form-control" value="" type="text">
                </div>
                <div class="form-group">
                    <label>{{__('backend.choose_another_day')}}*:</label>
                    <div class='input-group date' id='delay_date_group'>
                        <input type="text" id="delay_date" name="date" class="form-control" value=""
                               autocomplete="off"/>
                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="change_status" id="change_status"
                               value="1">
                        <label class="form-check-label" for="exampleRadios1">
                            {{__('backend.change_status')}}
                        </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="saveDelay">{{__('backend.delay')}}</button>
                    <button type="button" class="btn btn-danger"
                            data-dismiss="modal">{{__('backend.close')}}</button>
                </div>


            </div>
        </div>
    </div>
</div>

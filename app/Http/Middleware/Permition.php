<?php

namespace App\Http\Middleware;

use App\Models\Permission;
use App\Models\Permission_admin;
use Closure;
use Flash;

class Permition
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */


    public function handle($request, Closure $next, $permation)
    {
        //return $next($request);
        if (Permission_admin::where('role_admin_id', auth()->user()->role_admin_id)->where('permission_id', Permission::where('name', $permation)->value('id'))->exists() && Permission::where('name', $permation)->exists()) {
            return $next($request);
        } else {
            Flash::warning('Permission denied!');
            return redirect()->back();
        }

    }

}

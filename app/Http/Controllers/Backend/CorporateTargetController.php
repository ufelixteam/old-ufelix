<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;

use Illuminate\Http\Request;

use App\Models\Corporate;
use App\Models\CorporateTarget;
use Flash;
use File;
use Auth;
use DB;

class CorporateTargetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $corporates = Corporate::OrderBy('id', 'asc')->get();
        // return view('backend.corporate_target.index', compact('corporates'));

        $targets     = CorporateTarget::orderBy('id', 'desc');
        $filter      = app('request')->input('filter');

        if( isset($filter) && $filter !="" && $filter != -1 ) {
           $targets = $targets->where('corporate_id' , $filter);
        }

        $targets = $targets->paginate(50);

          if ($request->ajax()) {
              return [
                  'view' => view('backend.corporate_target.table', compact('targets'))->render(),
              ];
          }

          $corporates = Corporate::OrderBy('id', 'asc')->get();
          return view('backend.corporate_target.index', compact('targets', 'corporates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    // Add New Corporate Target
    public function store(Request $request)
    {

      $this->validate($request, [
					'corporate_target_from' 	    => 'required|integer',
          'corporate_target_to' 			  => 'required|integer',
					'corporate_discount' 		      => 'required|integer',
          'corporate_id' 			          => 'required',
					// 'report' 			            => 'required',
			]);



      $target 						              = new CorporateTarget();
      $target->corporate_target_from	 	= $request->input("corporate_target_from");
      $target->corporate_target_to	 		= $request->input("corporate_target_to");
      $target->corporate_discount 	    = $request->input("corporate_discount");
      $target->corporate_id 		        = $request->input("corporate_id");
      $target->report 		              = $request->input("report");
      $target->save();

      // foreach ( $request->all() as  $value) {
          // $target 						              = new CorporateTarget();
          // $target->corporate_target_from	 	= $request->input("corporate_target_from");
          // $target->corporate_target_to	 		= $request->input("corporate_target_to");
          // $target->corporate_discount 	    = $request->input("corporate_discount");
          // $target->corporate_id 		        = $request->input("corporate_id");
          // $target->report 		              = $request->input("report");
          // $target->save();
      // }
      // return $request->all();

			// Function Add Action In Activity Log By Samir
			addActivityLog('Add New Corporate Target #ID ' . $target->id , ' اضافة هدف شركة جديد #رقم ' . $target->id, 35); // Helper Function in App\Helpers.php

      Flash::success(__('backend.corporate_target_saved_successfully'));
			return redirect()->back();

    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
  					'corporate_target_from' 	    => 'required|integer',
            'corporate_target_to' 			  => 'required|integer',
  					'corporate_discount' 		      => 'required|integer',
  					// 'report' 			            => 'required',
  			]);

        $target 						              = CorporateTarget::findOrFail($id);
        $target->corporate_target_from	 	= $request->input("corporate_target_from");
        $target->corporate_target_to	 		= $request->input("corporate_target_to");
        $target->corporate_discount 	    = $request->input("corporate_discount");
        $target->report 		              = $request->input("report");
        $target->save();

        // Function Add Action In Activity Log By Samir
  			addActivityLog('Edit Corporate Target #ID ' . $target->id , ' تعديل هدف الشركة #رقم ' . $target->id, 35); // Helper Function in App\Helpers.php

        Flash::success(__('backend.corporate_target_updated_successfully'));
  			return redirect()->back();

    }

    public function destroy($id)
    {
        $corporate = Corporate::where('corporate_target_id', $id)->get();
        $target    = CorporateTarget::findOrFail($id);
        $target->delete();
        if(count($corporate ) > 0){
          DB::table('corporates')->where('corporate_target_id', $id)->update(['corporate_target_id' => NULL]);
        }
          // Function Add Action In Activity Log By Samir
    			addActivityLog('Deleted Corporate Target #ID ' . $target->id , ' حذف هدف الشركة #رقم ' . $target->id, 35); // Helper Function in App\Helpers.php

          Flash::success(__('backend.corporate_target_deleted_successfully'));
          return redirect()->back();

    }
}

<nav class="navbar navbar-default navbar-fixed-top" style="">
	<div class="brand">
		<a href="<?php echo e(url('/mngrAdmin')); ?>"><img src="<?php echo e(asset('/logo.jpeg')); ?>" width="50%" alt="Ufelix Logo" class="img-responsive logo"></a>
	</div>
	<div class="">
		<div class="navbar-btn">
			<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
		</div>
	
		<div id="navbar-menu">
			<ul class="nav navbar-nav navbar-right">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                        <i class="fa fa-globe"></i>
                    </a>
                    <ul class="dropdown-menu notifications">

                        <li class="dropdown notifications-menu">
                            <a href="<?php echo e(url('/lang/ar')); ?>" class="dropdown-toggle">AR</a>
                        </li>


                        <li class="dropdown notifications-menu">
                            <a href="<?php echo e(url('/lang/en')); ?>" class="dropdown-toggle">EN</a>
                        </li>
                    </ul>
                </li>


				<li class="dropdown">
					<a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
						<i class="lnr lnr-alarm"></i>
						<span class="badge bg-danger"><?php echo e(! empty($count_notification) ? $count_notification : '0'); ?></span>
					</a>
					<ul class="dropdown-menu notifications">
						<li><a href="#" class="notification-item">
							<span class="dot bg-warning"></span>System space is almost full</a>
						</li>
						<li><a href="#" class="notification-item"><span class="dot bg-danger"></span>You have 9 unfinished tasks</a></li>
						<li><a href="#" class="notification-item"><span class="dot bg-success"></span>Monthly report is available</a></li>
						<li><a href="#" class="notification-item"><span class="dot bg-warning"></span>Weekly meeting in 1 hour</a></li>
						<li><a href="#" class="notification-item"><span class="dot bg-success"></span>Your request has been approved</a></li>
						<li><a href="#" class="more">See all notifications</a></li>
					</ul>
				</li>
			
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img src="<?php echo e(Auth::guard('admin')->user()->image != '' ? Auth::guard('admin')->user()->image : asset('/assets/images/user.png')); ?>" class="img-circle" alt="Avatar"> <span><?php echo Auth::guard('admin')->user()->name; ?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
					<ul class="dropdown-menu">
						<li><a href="<?php echo url('/mngrAdmin/users/'.Auth::guard('admin')->user()->id); ?>"><i class="lnr lnr-user"></i> <span><?php echo e(__('backend.profile')); ?></span></a></li>
						<li><a href="#"><i class="lnr lnr-envelope"></i> <span>Message</span></a></li>
						<li><a href="#"><i class="lnr lnr-cog"></i> <span>Settings</span></a></li>

						<li> <a href="<?php echo url('/mngrAdmin/logout'); ?>"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="lnr lnr-exit"></i> <?php echo e(__('backend.signout')); ?>

                            </a>
                            <form id="logout-form" action="<?php echo e(url('/mngrAdmin/logout')); ?>" method="POST"
                                  style="display: none;">
                                <?php echo e(csrf_field()); ?>

                            </form></li>
					</ul>
				</li>



			</ul>
		</div>
	</div>
</nav>
<?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/layouts/navbar.blade.php ENDPATH**/ ?>
<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Events\OrderUpdated;

class OrderUpdateController extends Controller {

	public function updateOrder($id, Request $request) {
	    $data = $request->data;
        event(new OrderUpdated(...$data));
	}
}

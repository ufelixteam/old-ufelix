<?php $__env->startSection('title'); ?> Collection Orders <?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="row">
	<div class="col-12">
		<div class="">
			<div class="table-responsive m-t-40">
				<h1 class="title"> <?php echo app('translator')->getFromJson('website.CreateFormCollection'); ?></h1>
				<div class="button-box">
					<a href="<?php echo e(app()->getLocale() == 'en'  ? url('/en/customer/account/order-form') :  url('/customer/account/order-form')); ?>" class="btn btn-primary pull-right" style=""><i style="margin: 0 10px" class="mdi mdi-plus"></i><?php echo app('translator')->getFromJson('website.AddCollection'); ?></a>
				</div>
				<table id="example23" class="display nowrap table table-hover table-striped table-bordered" 
                            cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>#</th>
							<th><?php echo app('translator')->getFromJson('website.CreatedAt'); ?></th>
							<th> <?php echo app('translator')->getFromJson('website.Saved'); ?></th>
							<th><?php echo app('translator')->getFromJson('website.Actions'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php if(! empty($collections) ): ?>
							<?php $__currentLoopData = $collections; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $collection): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<tr>
									<td><?php echo e($collection->id); ?></td>
									
									<td><?php echo e($collection->created_at); ?></td>
									<td><?php echo $collection->saved_span; ?></td>
									<td>
										<?php if($collection->is_saved != 1): ?>
										<a class="btn btn-xs btn-info " href="<?php echo e(app()->getLocale() == 'en' ? url('/en/customer/account/order-form?collection='.$collection->id) : url('/customer/account/order-form?collection='.$collection->id)); ?>">
											Edit
										</a>
										<?php else: ?>
										<a class="btn btn-xs btn-primary "  href="<?php echo e(app()->getLocale() == 'en' ? url('/en/customer/account/orders?collection='.$collection->id) : url('/customer/account/orders?collection='.$collection->id)); ?>">
											List Orders
										</a>
										<?php endif; ?>
									</td>
								</tr>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												
						<?php endif; ?>
					</tbody>
				</table>
				
			</div>
		</div>
	</div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.profile.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/www/ufelix/ufelix/resources/views/frontend/profile/collections/index.blade.php ENDPATH**/ ?>
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission_admin extends Model
{

  public function permissions(){
      return $this->hasMany('App\Models\Permission', 'permission_id', 'id');
  }

}

@extends('agent.layouts.app')
@section('css')

@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-plus"></i> Vehicles / Create </h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <form action="{{ route('mngrAgent.trucks.store') }}" method="POST" name="truck_create" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="agent_id" value="{{ \Auth::guard('agent')->user()->agent_id }}">


                <div class="form-group col-md-6 col-sm-6 @if($errors->has('model_name')) has-error @endif">
                       <label for="model_name-field">Model Name</label>
                    <input type="text" id="model_name-field" name="model_name" class="form-control" value="{{ old("model_name") }}"/>
                       @if($errors->has("model_name"))
                        <span class="help-block">{{ $errors->first("model_name") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('plate_number')) has-error @endif">
                       <label for="plate_number-field">Plate Number</label>
                    <input type="text" id="plate_number-field" name="plate_number" class="form-control" value="{{ old("plate_number") }}"/>
                       @if($errors->has("plate_number"))
                        <span class="help-block">{{ $errors->first("plate_number") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('chassi_number')) has-error @endif">
                       <label for="chassi_number-field">Chassi Number</label>
                    <input type="text" id="chassi_number-field" name="chassi_number" class="form-control" value="{{ old("chassi_number") }}"/>
                       @if($errors->has("chassi_number"))
                        <span class="help-block">{{ $errors->first("chassi_number") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('year')) has-error @endif">
                       <label for="year-field">Year</label>

                     {{ Form::selectYear('year',  date('Y'),1980,null,["class"=>"form-control"]) }}
                       @if($errors->has("year"))
                        <span class="help-block">{{ $errors->first("year") }}</span>
                       @endif
                    </div>

                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('type_id')) has-error @endif">
                       <label for="type_id-field">Truck Type</label>
                    <select id="type_id-field" name="type_id" class="form-control" >
                            @foreach($truck_types as $type)
                              <option value="{{$type->id}}" {{ old("type_id") == $type->id ? 'selected' : '' }}>{{$type->name_en}}</option>
                            @endforeach
                    </select>
                       @if($errors->has("type_id"))
                        <span class="help-block">{{ $errors->first("type_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('color_id')) has-error @endif">
                       <label for="color_id-field">Color</label>
                     <select id="color_id-field" name="color_id" class="form-control" >
                        @if(! empty($colors))
                            @foreach($colors as $color)
                              <option value="{{$color->id}}" {{ old("color_id") == $color->id ? 'selected' : '' }}>{{$color->name}}</option>
                            @endforeach
                        @endif
                    </select>
                       @if($errors->has("color_id"))
                        <span class="help-block">{{ $errors->first("color_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('model_type_id')) has-error @endif">
                       <label for="model_type_id-field">vehicle Type</label>
                     <select id="model_type_id-field" name="vehicle_type_id" class="form-control" >
                          @foreach($model_types as $model)
                            <option value="{{$model->id}}" {{ old("model_type_id") == $model->id ? 'selected' : '' }}>{{$model->name}}</option>
                          @endforeach
                    </select>
                       @if($errors->has("model_type_id"))
                        <span class="help-block">{{ $errors->first("model_type_id") }}</span>
                       @endif
                    </div>
                     <div class="form-group col-md-6 col-sm-6 @if($errors->has('driver_id')) has-error @endif">
                       <label for="driver_id-field">Driver</label>
                        <select id="driver_id-field" name="driver_id" class="form-control" >
                          @foreach($drivers as $driver)
                            <option value="{{$driver->id}}" {{ old("driver_id") == $driver->id ? 'selected' : '' }}>{{$driver->name}} - {{$driver->mobile}}</option>
                          @endforeach

                    </select>
                       @if($errors->has("driver_id"))
                        <span class="help-block">{{ $errors->first("driver_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('license_start_date')) has-error @endif">
                       <label for="license_start_date_field">License Start Date</label>
                       <div class='input-group date ' id="datepicker">
                    <input type="text" id="datepicker" name="license_start_date" class="form-control date"  value="{{ old("license_start_date") }}"/>
                       @if($errors->has("license_start_date"))
                        <span class="help-block">{{ $errors->first("license_start_date") }}</span>
                       @endif
                       <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                    </div></div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('license_end_date')) has-error @endif">
                       <label for="license_end_date_field">License End Date</label>
                       <div class='input-group date datepicker2' >
                    <input type="text" id="datepicker1" name="license_end_date" class="form-control date"  value="{{ old("license_end_date") }}"/>
                       @if($errors->has("license_end_date"))
                        <span class="help-block">{{ $errors->first("license_end_date") }}</span>
                       @endif
                       <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        </div>
                    </div>
                      <div class="form-group col-md-6 col-sm-6 @if($errors->has('image')) has-error @endif">
                            <div id="image_upload">
                            </div>
                            <label for="image-field">Vehicle Image</label>
                            <input required class="uploadPhoto btn btn-primary" type="file"
                                   name="image" id="image">
                            @if($errors->has("image"))
                                <span class="help-block">{{ $errors->first("image") }}</span>
                            @endif
                        </div>
                        <div class="form-group col-md-6 col-sm-6 @if($errors->has('license_image_front')) has-error @endif">
                            <div id="license_image_front_upload">
                            </div>
                            <label for="image-field">License Image Front</label>
                            <input required class="uploadPhoto btn btn-primary" type="file"
                                   name="license_image_front" id="license_image_front">
                            @if($errors->has("license_image_front"))
                                <span class="help-block">{{ $errors->first("license_image_front") }}</span>
                            @endif
                        </div>
                        <div class="form-group col-md-12 col-sm-12 @if($errors->has('license_image_back')) has-error @endif">
                            <div id="license_image_back_upload">
                            </div>
                            <label for="license_image_back">License Image Back</label>
                            <input required class="uploadPhoto btn btn-primary" type="file"
                                   name="license_image_back" id="license_image_back">
                            @if($errors->has("license_image_back"))
                                <span class="help-block">{{ $errors->first("license_image_back") }}</span>
                            @endif
                        </div>
                <div class="well well-sm col-md-12 col-sm-12 col-xs-12">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAgent.trucks.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function () {
            $("form[name='truck_create']").validate({
                // Specify validation rules
                rules: {
                    model_name: {
                        required: true
                    },
                    plate_number: {
                        required: true
                    },
                    license_start_date: {
                        required: true,
                        date:true
                    },
                    license_end_date: {
                        required: true,
                        date:true
                    },
                    chassi_number: {
                        required: true
                    },
                    type_id: {
                        required: true
                    },
                    year: {
                        required: true
                    },
                    color_id: {
                        required: true
                    },
                    weight_id: {
                        required: true
                    },
                    model_type_id: {
                        date:true
                    },
                    driver_id: {
                        required: true
                    },
                    size_id: {
                        required: true
                    }
                },
                // Specify validation error messages
                messages: {
                    password: {
                        required: "Please Enter Driver Password",
                        minlength: "Password must be more than 5 character"
                    },
                    model_name: {
                        required: "Please Enter Model Name"
                    },
                    plate_number: {
                        required: "Please Enter Plate Number"
                    },
                    chassi_number: {
                        required: "Please Enter Chassi Number"
                    },
                    type_id: {
                        required: "Please Enter Type"
                    },
                    year: {
                        required: "Please Enter Year"
                    },
                    color_id: {
                        required: "Please Enter Color"
                    },
                    weight_id: {
                        required: "Please Enter Weight"
                    },
                    model_type_id: {
                        required: "Please Enter Model Type"
                    },
                    driver_id: {
                        required: "Please Enter Driver"
                    },
                    size_id: {
                        required: "Please Enter Size"
                    }
                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                },

                highlight: function (element) {
                    $(element).parent().addClass('error')
                },
                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {

                    form.submit();

                }
            });
        })
    </script>
@endsection

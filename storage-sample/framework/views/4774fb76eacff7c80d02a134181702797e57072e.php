<div class="row">
  <div class="col-md-12 col-xs-12 col-sm-12">
    <?php if($drivers->count()): ?>
      <table class="table table-condensed table-striped">
        <thead>
          <tr>
            <th>#</th>
            <th><?php echo e(__('backend.name')); ?></th>
            <th><?php echo e(__('backend.mobile')); ?></th>

             <th><?php echo e(__('backend.vehicle')); ?></th>
            <th><?php echo e(__('backend.accept_request')); ?></th>
            <th><?php echo e(__('backend.agent')); ?></th>
            <th><?php echo e(__('backend.active')); ?></th>
            <th><?php echo e(__('backend.block')); ?></th>
            <th><?php echo e(__('backend.verfiy_mobile')); ?></th>
            <th><?php echo e(__('backend.verify_email')); ?></th>

            <!-- <th><?php echo e(__('backend.status')); ?></th> -->



            <th class="text-right" ></th>
          </tr>
        </thead>
        <tbody>
          <?php $__currentLoopData = $drivers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $driver): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
              <td >

                <span class=" <?php echo e(! empty($driver->device) && $driver->device->status == 1 ? 'text-success' : 'text-danger'); ?>" style="font-weight: bold"><i class='fa fa-circle' aria-hidden='true'></i></span>
                <?php echo e($driver->id); ?></td>
              <td><?php echo e($driver->name); ?></td>
              <td><?php echo e($driver->mobile); ?></td>

              <td>
                  <?php if($driver->driver_has_vehicle != 0): ?>
                    <span class='text-success'><i class='fa fa-truck fa-lg' aria-hidden='true'></i></span>

                  <?php else: ?>
                    <span class=' text-danger'><i class='fa fa-times' aria-hidden='true'></i></span>
                  <?php endif; ?>
              </td>

              <td><?php echo $driver->status_request_span; ?></td>

              <td><?php echo e(! empty($driver->agent) ?  $driver->agent->name : ''); ?></td>


              <td> <input data-id="<?php echo e($driver->id); ?>" data-size="mini" class="toggle change_verify"  <?php echo e($driver->is_active == 1 ? 'checked' : ''); ?> data-onstyle="success" type="checkbox" data-style="ios" data-on="Yes" data-off="No">
              </td>
              <td> <input data-id="<?php echo e($driver->id); ?>" data-size="mini" class="toggle change_block"  <?php echo e($driver->is_block == 1 ? 'checked' : ''); ?> data-onstyle="danger" type="checkbox" data-style="ios" data-on="Yes" data-off="No">

              </td>
              <td>

                <input data-id="<?php echo e($driver->id); ?>" data-size="mini" class="toggle verify_mobile"  <?php echo e($driver->is_verify_mobile == 1 ? 'checked' : ''); ?> data-onstyle="success" type="checkbox" data-style="ios" data-on="Yes" data-off="No" >

              </td>
               <td>

                <input data-id="<?php echo e($driver->id); ?>" data-size="mini" class="toggle verify_email"  <?php echo e($driver->is_verify_email == 1 ? 'checked' : ''); ?> data-onstyle="success" type="checkbox" data-style="ios" data-on="Yes" data-off="No" >
              </td>

              <!-- <td><?php echo $driver->status_span; ?></td> -->


              <td class="text-right">

                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                         <?php if(permission('showOrdersMember')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                          <li><a  href="<?php echo e(url('mngrAdmin/orders/showOrders') . '/1' . '/' . $driver->id); ?>"><i class="glyphicon glyphicon-list"></i> <?php echo e(__('backend.orders')); ?></a></li>
                        <?php endif; ?>

                        <?php if(permission('showDriver')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                          <li><a  href="<?php echo e(route('mngrAdmin.drivers.show', $driver->id)); ?>"><i class="glyphicon glyphicon-eye-open"></i> <?php echo e(__('backend.view')); ?></a></li>
                        <?php endif; ?>

                        <?php if(permission('editDriver')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                          <li><a  href="<?php echo e(route('mngrAdmin.drivers.edit', $driver->id)); ?>"><i class="glyphicon glyphicon-edit"></i> <?php echo e(__('backend.edit')); ?></a></li>
                        <?php endif; ?>



                        </ul>
                      </div>
                    </td>

              </td>
            </tr>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
      </table>

      <?php echo $drivers->appends($_GET)->links(); ?>


    <?php else: ?>
        <h3 class="text-center alert alert-warning"><?php echo e(__('backend.No_result_found')); ?></h3>
    <?php endif; ?>
  </div>
</div>
<script>
$('.toggle').bootstrapToggle();
</script>
<?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/drivers/table.blade.php ENDPATH**/ ?>
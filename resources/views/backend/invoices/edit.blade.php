@extends('backend.layouts.app')
@section('css')
  <title>Invoices - Edit</title>
@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i>  Edit Invoices #{{$invoice->id}}</h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <form action="{{ route('mngrAdmin.invoices.update', $invoice->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group col-md-6 col-sm-6 @if($errors->has('start_date')) has-error @endif">
                       <label for="start_date-field">Start_date</label>
                    <input type="text" id="start_date-field" name="start_date" class="form-control" value="{{ is_null(old("start_date")) ? $invoice->start_date : old("start_date") }}"/>
                       @if($errors->has("start_date"))
                        <span class="help-block">{{ $errors->first("start_date") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('end_date')) has-error @endif">
                       <label for="end_date-field">End_date</label>
                    <input type="text" id="end_date-field" name="end_date" class="form-control" value="{{ is_null(old("end_date")) ? $invoice->end_date : old("end_date") }}"/>
                       @if($errors->has("end_date"))
                        <span class="help-block">{{ $errors->first("end_date") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('object_id')) has-error @endif">
                       <label for="object_id-field">Object_id</label>
                    <input type="text" id="object_id-field" name="object_id" class="form-control" value="{{ is_null(old("object_id")) ? $invoice->object_id : old("object_id") }}"/>
                       @if($errors->has("object_id"))
                        <span class="help-block">{{ $errors->first("object_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('type')) has-error @endif">
                       <label for="type-field">Type</label>
                    <input type="text" id="type-field" name="type" class="form-control" value="{{ is_null(old("type")) ? $invoice->type : old("type") }}"/>
                       @if($errors->has("type"))
                        <span class="help-block">{{ $errors->first("type") }}</span>
                       @endif
                    </div>
                    <!--<div class="form-group col-md-6 col-sm-6 @if($errors->has('agent_id')) has-error @endif">
                       <label for="agent_id-field">Agent_id</label>
                    <input type="text" id="agent_id-field" name="agent_id" class="form-control" value="{{ is_null(old("agent_id")) ? $invoice->agent_id : old("agent_id") }}"/>
                       @if($errors->has("agent_id"))
                        <span class="help-block">{{ $errors->first("agent_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('invoice_type')) has-error @endif">
                       <label for="invoice_type-field">Invoice_type</label>
                    <input type="text" id="invoice_type-field" name="invoice_type" class="form-control" value="{{ is_null(old("invoice_type")) ? $invoice->invoice_type : old("invoice_type") }}"/>
                       @if($errors->has("invoice_type"))
                        <span class="help-block">{{ $errors->first("invoice_type") }}</span>
                       @endif
                    </div>-->
                <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.invoices.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')

@endsection

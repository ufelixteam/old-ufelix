<?php

namespace App\Console\Commands;

use App\Events\OrderUpdated;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CancelOrdersFromFiveDays extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:cancel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cancel pending orders from 5 days ago';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        \DB::enableQueryLog();
        $orderIds = Order::doesntHave('logs')
            ->doesntHave('drivers')
            ->whereDate('created_at', '<=', Carbon::now()->subDays(5))
            ->where('status', 0)
//            ->limit(1)
            ->pluck('id')->toArray();

        Order::whereIn('id', $orderIds)
            ->update(['status_before_cancel' => 0, 'status' => '4', 'cancelled_by' => '2', 'cancelled_at' => Carbon::now(), 'cancelled_by_system' => 1]);

//        $orderIds = Order::where('cancelled_by_system', 1)
//            ->pluck('id')->toArray();

        $collection = collect($orderIds);
        foreach ($collection->chunk(200) as $orders) {
            event(new OrderUpdated($orders->toArray(), '', 1, 'cancel_by_system'));
        }

//        dd($orderIds);
    }
}

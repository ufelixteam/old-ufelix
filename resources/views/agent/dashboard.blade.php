@extends('agent.layouts.app')
@section('css')
<link rel="stylesheet" type="text/css" href="{{URL::asset('/assets/css/dashboard.css')}}">
<link href="{{URL::asset('/public/backend/css/style.css')}}" rel="stylesheet" type="text/css" media="all"/>
@endsection
@section('content')
<style>
.tab td{
  font-size: 15px;
  font-weight: bold;
  color: #334a5e;
  text-transform: capitalize;
}
.w3-message h5, .w3ls-touch h5 , .page-header {
  color: #566877;
  margin: 0;
  letter-spacing: 2px;
  font-size: 4rem;
  font-weight: bold;
}
</style>
<div class="panel panel-headline">
        <div class="profile-pic wthree">
            @if (! empty($user->image))
            <a data-toggle="lightbox" href="#demoLightbox" > <img
                        style="height: 100px;width:100px;"
                        src="{{ $user->image }}"></a>
			<div id="demoLightbox" class="lightbox fade"  tabindex="-1" role="dialog" aria-hidden="true">
   			 <div class='lightbox-dialog'>
       			 <div class='lightbox-content'>
     		       <img src="{{ $user->image }}">
        		</div>
    		</div>
  		</div>
            @endif
        <!-- </div> -->
        <div class="w3-message">
            <h5 class="page-header">About Me</h5>
            <table class="table table-condensed table-striped">
              <tr style="color:#297cc4;" class="tab">
                 <td>
                    name
                </td>
                 <td>
                    mobile
                </td>
                <td>
                     email
                </td>
              </tr>
              <tr style="color: #566877;">
                  <td >
                      {{$agent->name}}
                  </td>
                   <td>
                      {{! empty($agent) ? $agent->mobile : ''}}
                  </td>
                   <td>
                    {{ ! empty($agent) ?  $agent->email : '' }}
                  </td>
              </tr>
                <tr style="color:#297cc4;" class="tab">
                     <td>
                        Phone
                    </td>

                     <td>
                        Fax
                    </td>
                      <td>
                        Commercial Record Number
                    </td>

                </tr>
                <tr>

                    <td>
                        {{$agent->phone}}
                    </td>
                     <td>
                        {{$agent->fax}}
                    </td>
                     <td>
                        {{$agent->commercial_record_number}}
                    </td>
                </tr>

                <tr style="color:#297cc4;" class="tab">

                    <td>
                        Address
                    </td>

                    <td>
                        Deal Date
                    </td>
                    <td>
                        Profit Rate
                    </td>

                </tr>
                <tr>

                     <td>
                        {{$agent->address}}
                    </td>
                     <td>
                        {{$agent->deal_date}}
                    </td>

                    <td>
                        {{$agent->profit_rate}}
                    </td>

                </tr>

            </table>

        </div>

    </div>
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header" style="font-weight:bold;">Statistics</h1>
        </div>
    </div><!--/.row-->

    <div class="panel panel-container">
    <div class="row">
    <div class="col-xs-6 col-md-3 col-lg-3 no-padding">
        <div class="panel panel-havan panel-widget border-right">
            <div class="row no-padding"><em class="fa fa-xl fa-truck color-havan"></em>
                <div class="large">{{! empty($trucks) ? $trucks : '0'}}</div>
                <div class="text-muted">No. All Vehicles</div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-md-3 col-lg-3 no-padding">
        <div class="panel panel-blue panel-widget border-right">
            <div class="row no-padding"><em class="fa fa-xl fa-user-o  color-green"></em>
                <div class="large">{{! empty($drivers) ? $drivers : '0'}}</div>
                <div class="text-muted">No. All Drivers</div>
            </div>
        </div>
    </div>

    <div class="col-xs-6 col-md-3 col-lg-3 no-padding">
        <div class="panel panel-dark panel-widget ">
            <div class="row no-padding"><em class="fa fa-xl fa-times  color-red"></em>
                <div class="large">{{! empty($not_actived_drivers) ? $not_actived_drivers : '0'}}</div>
                <div class="text-muted">No. Not Verified Drivers</div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-md-3 col-lg-3 no-padding">
        <div class="panel panel-dark panel-widget ">
            <div class="row no-padding"><em class="fa fa-xl fa-check-circle  color-online"></em>
                <div class="large">{{! empty($actived_drivers) ? $actived_drivers : '0'}}</div>
                <div class="text-muted">No. Verified Drivers</div>
            </div>
        </div>
    </div>


     <div class="col-xs-6 col-md-3 col-lg-3 no-padding">
        <div class="panel panel-pink panel-widget border-right">
            <div class="row no-padding"><em class="fa fa-xl fa-compass color-pink"></em>
                <div class="large">{{! empty($orders) ? $orders : '0'}}</div>
                <div class="text-muted">No. All Orders</div>
            </div>
        </div>
    </div>

    <div class="col-xs-6 col-md-3 col-lg-3 no-padding">
        <div class="panel panel-green panel-widget border-right">
            <div class="row no-padding"><em class="fa fa-xl fa-circle color-online"></em>
                <div class="large">{{! empty($onlines) ? $onlines : '0'}}</div>
                <div class="text-muted">No. Online Drivers</div>
            </div>
        </div>
    </div>

     <div class="col-xs-6 col-md-3 col-lg-3 no-padding">
        <div class="panel panel-red panel-widget border-right">
            <div class="row no-padding"><em class="fa fa-xl fa-circle-o color-red"></em>
                <div class="large">{{! empty($offlines) ? $offlines : '0'}}</div>
                <div class="text-muted">No. Offline Drivers</div>
            </div>
        </div>
    </div>

     <div class="col-xs-6 col-md-3 col-lg-3 no-padding">
        <div class="panel panel-pink panel-widget border-right">
            <div class="row no-padding"><em class="fa fa-xl fa-star color-pink"></em>
                <div class="large">{{! empty($busy) ? $busy : '0'}}</div>
                <div class="text-muted">No.Busy Drivers</div>
            </div>
        </div>
    </div>
    </div>
    </div>
</div>

@endsection
@section('scripts')
<script src="{{URL::asset('/public/js/firebaseAgent.js')}}"></script>
@endsection

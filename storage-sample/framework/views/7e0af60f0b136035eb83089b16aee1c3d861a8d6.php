<div class="col-md-12 col-sm-12 col-xs-12">
    <form action="<?php echo e(URL::asset('/mngrAdmin/tmporders_create')); ?>" method="POST" id="order-form" class="order-form">
        <input type="hidden" id="collection_id" name="collection_id"
               value="<?php echo e(! empty($collection_id) ? $collection_id : '0'); ?>">
        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

        <div class="col-md-12 col-sm-12 col-xs-12" id="receiver_inputs"
             style="border: 2px solid #ea2;margin-bottom: 15px;">
            <h4 style="color: #ea2;font-weight: bold"><?php echo e(__('backend.Receiver_Data')); ?></h4>

            <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('receiver_name')): ?> has-error <?php endif; ?>">
                <label for="receiver_name-field"> <?php echo e(__('backend.name')); ?>: </label>
                <input type="text" required id="reciever_name-field" name="receiver_name" class="form-control"
                       value="<?php echo e(old('reciever_name')); ?>"/>
                <?php if($errors->has("receiver_name")): ?>
                    <span class="help-block"><?php echo e($errors->first("receiver_name")); ?></span>
                <?php endif; ?>
            </div>
            <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('receiver_mobile')): ?> has-error <?php endif; ?>">
                <label for="reciever_mobile-field"> <?php echo e(__('backend.mobile_number')); ?>: </label>
                <input type="text" required id="receiver_mobile-field" name="receiver_mobile" class="form-control"
                       value="<?php echo e(old('receiver_mobile')); ?>"/>
                <?php if($errors->has("receiver_mobile")): ?>
                    <span class="help-block"><?php echo e($errors->first("receiver_mobile")); ?></span>
                <?php endif; ?>
            </div>
            <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('receiver_phone')): ?> has-error <?php endif; ?>">
                <label for="receiver_phone-field"> <?php echo e(__('backend.mobile_number2')); ?>:</label>
                <input type="text" required id="receiver_phone-field" name="receiver_phone" class="form-control"
                       value="<?php echo e(old("receiver_phone")); ?>"/>
                <?php if($errors->has("receiver_phone")): ?>
                    <span class="help-block"><?php echo e($errors->first("receiver_phone")); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group col-md-3 col-sm-3">
                <label class="control-label" for="r_government_id"> <?php echo e(__('backend.government')); ?>:</label>

                <select class="form-control r_government_id" id="r_government_id" name="r_government_id">
                    <option value="" data-display="Select"><?php echo e(__('backend.Receiver_Government')); ?></option>
                    <?php if(! empty($governments)): ?>
                        <?php $__currentLoopData = $governments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $government): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($government->id); ?>">
                                <?php echo e($government->name_en); ?>

                            </option>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>

                </select>

            </div>

            <div class="form-group col-md-2 col-sm-2">
                <label class="control-label" for="r_state_id"> <?php echo e(__('backend.city')); ?>:</label>

                <select class=" form-control r_state_id" id="r_state_id" name="r_state_id">
                    <option value=""><?php echo e(__('backend.Receiver_City')); ?></option>

                </select>

            </div>
            <div class="form-group col-md-2 col-sm-2 <?php if($errors->has('receiver_latitude')): ?> has-error <?php endif; ?>">
                <label for="receiver_latitude"> <?php echo e(__('backend.latitude')); ?>: </label>
                <input type="text" required id="receiver_latitude" name="receiver_latitude" class="form-control"
                       value="<?php echo e(old("receiver_latitude") ? old("receiver_latitude")  : '30.0668886'); ?>"/>
                <?php if($errors->has("receiver_latitude")): ?>
                    <span class="help-block"><?php echo e($errors->first("receiver_latitude")); ?></span>
                <?php endif; ?>
            </div>
            <div class="form-group col-md-2 col-sm-2 <?php if($errors->has('receiver_longitude')): ?> has-error <?php endif; ?>">
                <label for="reciever_longitude"> <?php echo e(__('backend.longitude')); ?>: </label>
                <input type="text" required id="receiver_longitude" name="receiver_longitude" class="form-control"
                       value="<?php echo e(old("receiver_longitude") ? old("receiver_longitude")  : '31.1962743'); ?>"/>
                <?php if($errors->has("receiver_longitude")): ?>
                    <span class="help-block"><?php echo e($errors->first("receiver_longitude")); ?></span>
                <?php endif; ?>
            </div>


            <div
                class="form-group input-group col-md-6 col-sm-6 <?php if($errors->has('receiver_address')): ?> has-error <?php endif; ?>">
                <label for="reciever_address"><?php echo e(__('backend.Receiver_Address')); ?>: </label>

                <input type="text" required id="receiver_address" name="receiver_address" class="form-control"
                       value="<?php echo e(old("receiver_address")); ?>"/>

                <span class="input-group-append">
              <button class="input-group-text bg-transparent" type="button" data-toggle="modal"
                      data-target="#map1Modal"> <i class="fa fa-map-marker"></i></button>
            </span>

                <?php if($errors->has("receiver_address")): ?>
                    <span class="help-block"><?php echo e($errors->first("receiver_address")); ?></span>
                <?php endif; ?>
            </div>

        </div>

        <div class="col-md-12 col-sm-12 col-xs-12" style="border: 2px solid #ea2;margin-bottom: 15px;">
            <h4 style="color: #ea2;font-weight: bold"><?php echo e(__('backend.Sender_Data')); ?></h4>

            <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('corparate_id')): ?> has-error <?php endif; ?>">
                <label for="corparate_id-field"><?php echo e(__('backend.corporate')); ?></label>

                <select class="form-control" id="corparate_id"
                        name="corparate_id" <?php echo e(! empty($collection) && $collection != null ? 'disabled':  ''); ?>>
                    <option value="" data-display="Select"><?php echo e(__('backend.corporate')); ?></option>
                    <?php if(! empty($Corporate_ids)): ?>
                        <?php $__currentLoopData = $Corporate_ids; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $corparate_id): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option
                                value="<?php echo e($corparate_id->id); ?>" <?php echo e(! empty($collection) && $collection != null && $collection->corporate_id == $corparate_id->id  ? 'selected':  ''); ?> >
                                <?php echo e($corparate_id->id.' - '.$corparate_id->name); ?>

                            </option>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>

                </select>

                <?php if($errors->has("corparate_id")): ?>
                    <span class="help-block"><?php echo e($errors->first("corparate_id")); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('customer_id')): ?> has-error <?php endif; ?>">
                <label for="customer_id-field"><?php echo e(__('backend.customer')); ?> </label>
                <select class=" form-control" id="customer_id"
                        name="customer_id" <?php echo e(! empty($collection) && $collection != null && $customer != null ? 'disabled':  ''); ?> >

                    <?php if(! empty($customers) && count($customers) > 0): ?>
                        <?php $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customer_id): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option
                                value="<?php echo e($customer_id->id); ?>" <?php echo e(! empty($collection) && $collection != null && $collection->customer_id == $customer_id->id  ? 'selected':  ''); ?> >
                                <?php echo e($customer_id->name); ?>

                            </option>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <?php else: ?>
                        <option value=""><?php echo e(__('backend.customer')); ?></option>
                    <?php endif; ?>

                </select>
                <?php if($errors->has("customer_id")): ?>
                    <span class="help-block"><?php echo e($errors->first("customer_id")); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('sender_name')): ?> has-error <?php endif; ?>">
                <label for="sender_name-field"><?php echo e(__('backend.name')); ?>: </label>
                <input type="text" required id="sender_name-field" name="sender_name" class="form-control"
                       value="<?php echo e(! empty($customer) && $customer != null ? $customer->name : old("sender_name")); ?>"/>
                <?php if($errors->has("sender_name")): ?>
                    <span class="help-block"><?php echo e($errors->first("sender_name")); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('sender_mobile')): ?> has-error <?php endif; ?>">
                <label for="sender_mobile-field"><?php echo e(__('backend.mobile_number')); ?>: </label>
                <input type="text" required id="sender_mobile-field" name="sender_mobile" class="form-control"
                       value="<?php echo e(! empty($customer) && $customer != null ? $customer->mobile : old("sender_mobile")); ?>"/>
                <?php if($errors->has("sender_mobile")): ?>
                    <span class="help-block"><?php echo e($errors->first("sender_mobile")); ?></span>
                <?php endif; ?>
            </div>
            <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('sender_phone')): ?> has-error <?php endif; ?>">
                <label for="sender_phone-field"><?php echo e(__('backend.mobile_number2')); ?>: </label>
                <input type="text" required id="sender_phone-field" name="sender_phone" class="form-control"
                       value="<?php echo e(old("sender_phone")); ?>"/>
                <?php if($errors->has("sender_phone")): ?>
                    <span class="help-block"><?php echo e($errors->first("sender_phone")); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group col-md-3 col-sm-3">
                <label class="control-label" for="s_government_id"><?php echo e(__('backend.government')); ?></label>

                <select class="form-control s_government_id" id="s_government_id" name="s_government_id">
                    <option value="" data-display="Select"><?php echo e(__('backend.government')); ?></option>
                    <?php if(! empty($governments)): ?>
                        <?php $__currentLoopData = $governments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $government): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($government->id); ?>">
                                <?php echo e($government->name_en); ?>

                            </option>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>

                </select>

            </div>

            <div class="form-group col-md-2 col-sm-2">
                <label class="control-label" for="s_state_id"><?php echo e(__('backend.city')); ?></label>

                <select class=" form-control s_state_id" id="s_state_id" name="s_state_id">
                    <option value=""><?php echo e(__('backend.city')); ?></option>

                </select>

            </div>


            <div class="form-group col-md-2 col-sm-2 <?php if($errors->has('sender_latitude')): ?> has-error <?php endif; ?>">
                <label for="sender_latitude"><?php echo e(__('backend.latitude')); ?>: </label>
                <input type="text" required id="sender_latitude" name="sender_latitude" class="form-control"
                       value="<?php echo e(old("sender_latitude") ? old("sender_latitude")  : '30.0668886'); ?>"/>
                <?php if($errors->has("sender_latitude")): ?>
                    <span class="help-block"><?php echo e($errors->first("sender_latitude")); ?></span>
                <?php endif; ?>
            </div>
            <div class="form-group col-md-2 col-sm-2 <?php if($errors->has('sender_longitude')): ?> has-error <?php endif; ?>">
                <label for="sender_longitude"><?php echo e(__('backend.longitude')); ?>: </label>
                <input type="text" required id="sender_longitude" name="sender_longitude" class="form-control"
                       value="<?php echo e(old("sender_longitude") ? old("sender_longitude")  : '31.1962743'); ?>"/>
                <?php if($errors->has("sender_longitude")): ?>
                    <span class="help-block"><?php echo e($errors->first("sender_longitude")); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group input-group col-md-6 col-sm-6 <?php if($errors->has('sender_address')): ?> has-error <?php endif; ?>">
                <label for="sender_address"><?php echo e(__('backend.Sender_Address')); ?>: </label>
                <input type="text" required id="sender_address" name="sender_address" class="form-control"
                       value="<?php echo e(old("sender_address")); ?>"/>
                <span class="input-group-append">
              <button class="input-group-text bg-transparent" type="button" data-toggle="modal"
                      data-target="#map2Modal"> <i class="fa fa-map-marker"></i></button>
            </span>
                <?php if($errors->has("sender_address")): ?>
                    <span class="help-block"><?php echo e($errors->first("sender_address")); ?></span>
                <?php endif; ?>
            </div>

        </div>

        <div class="col-md-12 col-sm-12 col-xs-12" id="order_inputs"
             style="border: 2px solid #ea2;margin-bottom: 15px;">
            <h4 style="color: #ea2;font-weight: bold"><?php echo e(__('backend.Order_Data')); ?></h4>

            <div class="form-group col-md-2 col-sm-2 <?php if($errors->has('type')): ?> has-error <?php endif; ?>">
                <label for="type-field"><?php echo e(__('backend.order_type')); ?></label>
                <select class="form-control" id="order_type_id" name="order_type_id">
                    <option value="" data-display="Select"><?php echo e(__('backend.type')); ?></option>
                    <?php if(! empty($types)): ?>
                        <?php $__currentLoopData = $types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($type->id); ?>">
                                <?php echo e($type->name_en); ?>

                            </option>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </select>
                <?php if($errors->has("type")): ?>
                    <span class="help-block"><?php echo e($errors->first("type")); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group col-md-2 col-sm-2 <?php if($errors->has('type')): ?> has-error <?php endif; ?>">
                <label for="type-field"><?php echo e(__('backend.Type_Name')); ?>: </label>
                <input type="text" required id="type-field" name="type" class="form-control" value="<?php echo e(old("type")); ?>"/>
                <?php if($errors->has("type")): ?>
                    <span class="help-block"><?php echo e($errors->first("type")); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group col-md-2 col-sm-2 <?php if($errors->has('main_store')): ?> has-error <?php endif; ?>">
                <label for="store_id-field"><?php echo e(__('backend.store')); ?>:</label>
                <select id="store_id-field" name="main_store" class="form-control">
                    <option value=""><?php echo e(__('backend.Choose_Store')); ?></option>
                    <?php if(! empty($allStores)): ?>
                        <?php $__currentLoopData = $allStores; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $store): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($store->id); ?>" <?php echo e($store->id == old('main_store') ? 'selected' : ''); ?> >
                                <?php echo e($store->name); ?>

                            </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>

                </select>
                <?php if($errors->has("main_store")): ?>
                    <span class="help-block"><?php echo e($errors->first("main_store")); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group col-md-2 col-sm-2 <?php if($errors->has('stock_id')): ?> has-error <?php endif; ?>">
                <label for="stock_id-field"> <?php echo e(__('backend.stock')); ?>:</label>
                <select id="stock_id-field" name="store_id" class="form-control">
                    <option value=""><?php echo e(__('backend.Choose_Stock')); ?></option>
                    <?php if(! empty($stores)): ?>
                        <?php $__currentLoopData = $stores; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stock): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($stock->id); ?>" <?php echo e($stock->id == old('store_id') ? 'selected' : ''); ?> >
                                <?php echo e($stock->title); ?>

                            </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>

                </select>
                <?php if($errors->has("stock_id")): ?>
                    <span class="help-block"><?php echo e($errors->first("stock_id")); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group col-md-2 col-sm-2 <?php if($errors->has('qty')): ?> has-error <?php endif; ?>">
                <label for="qty-field"> <?php echo e(__('backend.Quantity_Order')); ?>: </label>
                <input type="number" id="qty-field" name="qty" class="form-control"
                       value="<?php echo e(old("qty") ? old("qty") : '0'); ?>"/>
                <?php if($errors->has("qty")): ?>
                    <span class="help-block"><?php echo e($errors->first("qty")); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group col-md-2 col-sm-2 <?php if($errors->has('overload')): ?> has-error <?php endif; ?>">
                <label for="overload-field"> <?php echo e(__('backend.overload')); ?>:</label>
                <input type="number" required id="overload-field" name="overload" class="form-control"
                       value="<?php echo e(old("overload") ? old("overload") : '0'); ?>"/>
                <?php if($errors->has("overload")): ?>
                    <span class="help-block"><?php echo e($errors->first("overload")); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group col-md-2 col-sm-2 <?php if($errors->has('agent_id')): ?> has-error <?php endif; ?>">
                <label for="agent_id-field"> <?php echo e(__('backend.agent')); ?>: </label>
                <select id="agent_id-field" name="agent_id" class="form-control">
                    <option value=""> <?php echo e(__('backend.Choose_Agent')); ?></option>
                    <?php if(! empty($agents)): ?>
                        <?php $__currentLoopData = $agents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $agent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($agent->id); ?>">
                                <?php echo e($agent->name); ?>

                            </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>

                </select>
                <?php if($errors->has("agent_id")): ?>
                    <span class="help-block"><?php echo e($errors->first("agent_id")); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group col-md-3 col-sm-3">
                <label class="control-label" for="driver_id"> <?php echo e(__('backend.driver')); ?>:</label>

                <select class=" form-control driver_id" id="driver_id" name="forward_driver_id">
                    <option value=""> <?php echo e(__('backend.Choose_Driver')); ?></option>
                    <?php if(! empty($drivers)): ?>

                        <?php $__currentLoopData = $drivers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $driver1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($driver1->id); ?>">
                                <?php echo e($driver1->name); ?>

                            </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <?php endif; ?>

                </select>

            </div>

            <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('payment_method_id')): ?> has-error <?php endif; ?>">
                <label for="payment_method_id-field"> <?php echo e(__('backend.Payment_Method')); ?>:</label>
                <select id="payment_method_id-field" name="payment_method_id" class="form-control">
                    <option value="" data-display="Select"> <?php echo e(__('backend.Payment_Method')); ?></option>

                    <?php if(! empty($payments)): ?>
                        <?php $__currentLoopData = $payments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($payment->id); ?>">
                                <?php echo e($payment->name); ?>

                            </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>

                </select>
                <?php if($errors->has("payment_method_id")): ?>
                    <span class="help-block"><?php echo e($errors->first("payment_method_id")); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group col-md-2 col-sm-3 <?php if($errors->has('')): ?> has-error <?php endif; ?>">
                <label for="order_price-field"><?php echo e(__('backend.order_price')); ?>: </label>
                <input type="number" required class="form-control" id="order_price-field" name="order_price"
                       value="<?php echo e(old("order_price")  ? old("order_price")  : '0'); ?>">
                <?php if($errors->has("order_price")): ?>
                    <span class="help-block"><?php echo e($errors->first("order_price")); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group col-md-2 col-sm-3 <?php if($errors->has('delivery_price')): ?> has-error <?php endif; ?>">
                <label for="delivery_price-field"><?php echo e(__('backend.Delivery_Price')); ?>: </label>
                <input type="number" required class="form-control" id="delivery_price-field" name="delivery_price"
                       value="<?php echo e(old("delivery_price")  ? old("delivery_price")  : '0'); ?>">
                <?php if($errors->has("delivery_price")): ?>
                    <span class="help-block"><?php echo e($errors->first("delivery_price")); ?></span>
                <?php endif; ?>
            </div>


            <div class="form-group col-md-12 col-sm-12 <?php if($errors->has('notes')): ?> has-error <?php endif; ?>">
                <label for="notes-field"><?php echo e(__('backend.notes')); ?>: </label>

                <input type="text" required id="notes-field" name="notes" class="form-control"
                       value="<?php echo e(old("notes")); ?>"/>

                <?php if($errors->has("notes")): ?>
                    <span class="help-block"><?php echo e($errors->first("notes")); ?></span>
                <?php endif; ?>
            </div>

        </div>

        <div class="col-md-12 col-sm-12 col-xs-12 text-center" style="margin-bottom: 15px;">
            <button type="button" class="btn btn-warning btn-block create-btn"
                    id="order-btn"><?php echo e(__('backend.add_order')); ?>

            </button>
        </div>
    </form>
</div>
<?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/orders/create-form.blade.php ENDPATH**/ ?>
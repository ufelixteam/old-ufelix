<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        @if($orders)
            <form method="post" id="myform" action="{{ url('/mngrAdmin/forward_order') }}">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <div class="table-responsive">
                    <table id="theTable" class="table table-condensed table-striped text-center"
                           style="min-height: 200px;">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__("backend.id")}}</th>
                            <th>{{__("backend.status")}}</th>
                            <th>{{__('backend.collection')}}</th>
                            <th>{{__('backend.receiver_name')}}</th>
                            <th>{{__('backend.sender_name')}}</th>
                            <th>{{__('backend.from')}}</th>
                            <th>{{__('backend.to')}}</th>
                            <th style="font-weight: bold; color: #333;">{{__('backend.order_number')}}</th>
                            <th style="font-weight: bold; color: #333;">{{__('backend.driver')}}</th>
                            <th colspan="2">
                                <div>
                                    <a class="action_btn btn btn-warning disabled" id="delivery_problems"
                                       style="width: 415px;">
                                        {{__('backend.delivery_problems')}}
                                    </a>
                                </div>
                                <div>
                                    <a class="action_btn btn btn-warning" id="toggle" data-state="1"> {{__('backend.select')}}</a>

                                    <a class="action_btn btn btn-danger disabled" id="recall_orders">
                                        <i class="fa fa-history"></i> {{__('backend.recall')}}
                                    </a>

                                    <a class="action_btn btn btn-success disabled" id="receive_orders">
                                        <i class="fa fa-battery-half"></i> {{__('backend.Receive')}}
                                    </a>

                                    <a class="action_btn btn btn-info disabled" id="delay_orders">
                                        <i class="fa fa-calendar"></i> {{__('backend.delay')}}
                                    </a>

                                </div>
                                <div>
                                    <button type="button" id="pdf-submit" disabled
                                            class="action_btn btn btn-warning">{{__('backend.print')}}</button>

                                    <a class="action_btn btn btn-danger disabled" id="cancel_orders">
                                        <i class="fa fa-close"></i> {{__('backend.Cancel')}}
                                    </a>

                                    <a class="action_btn btn btn-success disabled" id="deliver_orders">
                                        <i class="fa fa-battery"></i> {{__('backend.deliver')}}
                                    </a>

                                    <a class="action_btn btn btn-success disabled" id="forward">
                                        <i class="fa fa-eye"></i> {{__('backend.forward')}}

                                    </a>
                                </div>
                            </th>

                        </tr>
                        </thead>
                        <tbody id="tbody">

                        @include('backend.orders.scan-tr')

                        <div class="modal fade" id="forwardModal" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <input type="hidden" class="form-control" name="orderTypeID[]"
                                                   value="{{$orders->order_type_id}}">
                                            <input type="hidden" class="form-control" name="governorateCostID[]"
                                                   value="{{$orders->governorate_cost_id}}">
                                        </div>
                                        <p>{{__('backend.Forward_Orders_In_Agent_Or_Driver')}}</p>
                                        <div class="form-group">
                                            <br>
                                            <select name="agent" id="agent" class="form-control agent" required>
                                                <option value="" selected>{{__('backend.Choose_Agent')}}</option>
                                                @if(! empty($agents))
                                                    @foreach($agents as $agent)
                                                        <option value="{{$agent->id}}">{{$agent->name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>

                                            <br>
                                            <select class="form-control" name="driverID" id="driver">
                                                <option value="">{{__('backend.Choose_Driver')}}</option>
                                            </select>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" id="forward_action">{{__('backend.forward')}}</button>
                                            <button type="button" class="btn btn-danger"
                                                    data-dismiss="modal">{{__('backend.close')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </tbody>
                    </table>
                </div>
            </form>
            {{--            {!! $orders->appends($_GET)->links() !!}--}}
        @else
            <h3 class="text-center alert alert-warning">No Result Found !</h3>
        @endif
    </div>
</div>
<div id="js-form" class="js-form">
</div>

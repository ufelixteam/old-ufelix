<script>

 $("#agent_id").on('change',function(){
      $.ajax({
        url: "<?php echo e(url('/mngrAdmin/getdriver_by_agents')); ?>"+"/"+$(this).val(),
        type: 'get',
        data: {},
        success: function(data) {
          $('#driver_id').html('<option value="Null" >Choose Drivers</option>');
          $.each(data, function(i, content) {
            $('#driver_id').append($("<option></option>").attr("value",content.id).text(content.name)); 
          });
        },
        error: function(data) {
          console.log('Error:', data);
        }
      });
    });


 $("#r_government_id").on('change',function(){
      $.ajax({
        url: "<?php echo e(url('/mngrAdmin/get_cities')); ?>"+"/"+$(this).val(),
        type: 'get',
        data: {},
        success: function(data) {
          $('#r_state_id').html('<option value="" >Sender City</option>');
          $.each(data, function(i, content) {
            $('#r_state_id').append($("<option></option>").attr("value",content.id).text(content.city_name));
          });
        },
        error: function(data) {
          console.log('Error:', data);
        }
      });

      /* get price list*/
      $.ajax({
        url: "<?php echo e(url('/mngrAdmin/get_cost_pickup')); ?>"+"/"+$("#from-pick").val()+"/"+$(this).val(),
        type: 'get',
        data: {},
        success: function(data) {
          if(data != "0"){
            $('#delivery_price-field').val(data.cost);
            $('#bonous-field').val(data.bonous);

          }
         
        },
        error: function(data) {
          console.log('Error:', data);
        }
      });

    });

  $("#store_id").on('change',function(){
      $.ajax({
        url: "<?php echo e(url('/mngrAdmin/get_store')); ?>"+"/"+$(this).val(),
        type: 'get',
        data: {},
        success: function(data) {
          $('#reciever_name-field').val(data.name);
          $('#receiver_mobile-field').val(data.mobile);
          $('#receiver_latitude').val(data.latitude);
          $('#receiver_longitude').val(data.longitude);
          $('#receiver_address').val(data.address);
          $('#pick_r_government_id').val(data.governorate_id).change();
          console.log(data.governorate_id )
          $("div.pick_r_government_id > select > option[value=" + data.governorate_id + "]").attr("selected",true);
          
        },
        error: function(data) {
          console.log('Error:', data);
        }
      });

    });

</script><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/pickups/js.blade.php ENDPATH**/ ?>
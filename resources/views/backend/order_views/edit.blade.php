@extends('backend.layouts.app')
@section('css')

@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i>  Edit OrderViews #{{$order_view->id}}</h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <form action="{{ route('mngrAdmin.order_views.update', $order_view->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group col-md-6 col-sm-6 @if($errors->has('order_id')) has-error @endif">
                       <label for="order_id-field">Order_id</label>
                    <input type="text" id="order_id-field" name="order_id" class="form-control" value="{{ is_null(old("order_id")) ? $order_view->order_id : old("order_id") }}"/>
                       @if($errors->has("order_id"))
                        <span class="help-block">{{ $errors->first("order_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('status')) has-error @endif">
                       <label for="status-field">Status</label>
                    <input type="text" id="status-field" name="status" class="form-control" value="{{ is_null(old("status")) ? $order_view->status : old("status") }}"/>
                       @if($errors->has("status"))
                        <span class="help-block">{{ $errors->first("status") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('user_id')) has-error @endif">
                       <label for="user_id-field">User_id</label>
                    <input type="text" id="user_id-field" name="user_id" class="form-control" value="{{ is_null(old("user_id")) ? $order_view->user_id : old("user_id") }}"/>
                       @if($errors->has("user_id"))
                        <span class="help-block">{{ $errors->first("user_id") }}</span>
                       @endif
                    </div>
{{--                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('agent_id')) has-error @endif">--}}
{{--                       <label for="agent_id-field">Agent_id</label>--}}
{{--                    <input type="text" id="agent_id-field" name="agent_id" class="form-control" value="{{ is_null(old("agent_id")) ? $order_view->agent_id : old("agent_id") }}"/>--}}
{{--                       @if($errors->has("agent_id"))--}}
{{--                        <span class="help-block">{{ $errors->first("agent_id") }}</span>--}}
{{--                       @endif--}}
{{--                    </div>--}}
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('notes')) has-error @endif">
                       <label for="notes-field">Notes</label>
                    <textarea class="form-control" id="notes-field" rows="4" name="notes">{{ is_null(old("notes")) ? $order_view->notes : old("notes") }}</textarea>
                       @if($errors->has("notes"))
                        <span class="help-block">{{ $errors->first("notes") }}</span>
                       @endif
                    </div>
                <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.order_views.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')

@endsection

<?php

namespace App\Console\Commands;

use App\Models\Governorate;
use App\Models\Order;
use App\Models\Ticket;
use App\Models\TicketRule;
use Carbon\Carbon;
use Illuminate\Console\Command;

class GenerateTicket extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ticket:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate tickets from orders automatically';

    protected $types = [];
    protected $gizaID;
    protected $cairoID;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->types = TicketRule::where('status', 1)->pluck('name', 'type')->toArray();

        $this->gizaID = Governorate::where('name_en', 'Giza')->value('id');
        $this->cairoID = Governorate::where('name_en', 'Cairo')->value('id');

        foreach ($this->types as $key => $type) {
            $ruleMethos = 'rule' . $key;
            $this->{$ruleMethos}();
        }

    }

    public function rule1()
    {
        $orders1 = Order::with(['customer' => function ($query) {
            $query->with(['Corporate']);
        }])->whereDoesntHave('tickets', function ($query) {
            $query->where('status', 0);
        })
            ->where('orders.status', 0)
            ->whereDate('created_at', '<', Carbon::now()->subDays(2)->toDateString())
            ->get();

        foreach ($orders1 as $order) {
            Ticket::create([
                'order_id' => $order->id,
                'type' => 1,
                'status' => 0,
                'reason' => $this->types[1],
                'assigned_to_id' => (!empty($order->customer->Corporate) ? $order->customer->Corporate->responsible_id : null),
                'ticket_number' => random_number(2)
            ]);
        }
    }

    public function rule2()
    {
        $orders2 = Order::with(['customer' => function ($query) {
            $query->with(['Corporate']);
        }])->whereDoesntHave('tickets', function ($query) {
            $query->where('status', 0);
        })
            ->where('status', 7)
            ->whereDate('dropped_at', '<', Carbon::now()->subDays(1)->toDateString())
            ->doesntHave('drivers')
            ->get();

        foreach ($orders2 as $order) {
            Ticket::create([
                'order_id' => $order->id,
                'type' => 2,
                'status' => 0,
                'reason' => $this->types[2],
                'assigned_to_id' => (!empty($order->customer->Corporate) ? $order->customer->Corporate->responsible_id : null),
                'ticket_number' => random_number(2)
            ]);
        }
    }

    public function rule3()
    {
        $orders3 = Order::with(['customer' => function ($query) {
            $query->with(['Corporate']);
        }])->whereDoesntHave('tickets', function ($query) {
            $query->where('status', 0);
        })
            ->where('status', 7)
            ->doesntHave('order_delay')
            ->doesntHave('delivery_problems')
            ->has('drivers')
            ->get();

        foreach ($orders3 as $order) {
            Ticket::create([
                'order_id' => $order->id,
                'type' => 3,
                'status' => 0,
                'reason' => $this->types[3],
                'assigned_to_id' => (!empty($order->customer->Corporate) ? $order->customer->Corporate->responsible_id : null),
                'ticket_number' => random_number(2)
            ]);
        }

    }

    public function rule4()
    {
        $orders4 = Order::with(['customer' => function ($query) {
            $query->with(['Corporate']);
        }])->whereDoesntHave('tickets', function ($query) {
            $query->where('status', 0);
        })
            ->where('status', 7)
            ->whereNotIn('r_government_id', [$this->gizaID, $this->cairoID])
            ->has('drivers')
            ->get();

        foreach ($orders4 as $order) {
            Ticket::create([
                'order_id' => $order->id,
                'type' => 4,
                'status' => 0,
                'reason' => $this->types[4],
                'assigned_to_id' => (!empty($order->customer->Corporate) ? $order->customer->Corporate->responsible_id : null),
                'ticket_number' => random_number(2)
            ]);
        }
    }

    public function rule5()
    {
        $gizaID = $this->gizaID;
        $cairoID = $this->cairoID;

        $orders5 = Order::with(['customer' => function ($query) {
            $query->with(['Corporate']);
        }])->whereDoesntHave('tickets', function ($query) {
            $query->where('status', 0);
        })
            ->where('status', 2)
            ->whereDate('received_at', '<', Carbon::now()->subDays(1)->toDateString())
            ->where(function ($query) use ($gizaID, $cairoID) {
                $query->where('r_government_id', $gizaID)
                    ->orWhere('r_government_id', $cairoID);
            })
            ->get();

        foreach ($orders5 as $order) {
            Ticket::create([
                'order_id' => $order->id,
                'type' => 5,
                'status' => 0,
                'reason' => $this->types[5],
                'assigned_to_id' => (!empty($order->customer->Corporate) ? $order->customer->Corporate->responsible_id : null),
                'ticket_number' => random_number(2)
            ]);
        }
    }

    public function rule6()
    {
        $orders6 = Order::with(['customer' => function ($query) {
            $query->with(['Corporate']);
        }])->whereDoesntHave('tickets', function ($query) {
            $query->where('status', 0);
        })
            ->where('status', 2)
            ->whereDate('received_at', '<', Carbon::now()->subDays(4)->toDateString())
            ->whereNotIn('r_government_id', [$this->gizaID, $this->cairoID])
            ->get();

        foreach ($orders6 as $order) {
            Ticket::create([
                'order_id' => $order->id,
                'type' => 6,
                'status' => 0,
                'reason' => $this->types[6],
                'assigned_to_id' => (!empty($order->customer->Corporate) ? $order->customer->Corporate->responsible_id : null),
                'ticket_number' => random_number(2)
            ]);
        }
    }

    public function rule7()
    {
        $orders7 = Order::with(['customer' => function ($query) {
            $query->with(['Corporate']);
        }])->whereDoesntHave('tickets', function ($query) {
            $query->where('status', 0);
        })
            ->where('status', 2)
            ->whereDate('received_at', '<=', Carbon::now()->subDays(1)->toDateString())
            ->where(function ($query) {
                $query->has('order_delay')
                    ->orHas('delivery_problems');
            })
            ->get();

        foreach ($orders7 as $order) {
            Ticket::create([
                'order_id' => $order->id,
                'type' => 7,
                'status' => 0,
                'reason' => $this->types[7],
                'assigned_to_id' => (!empty($order->customer->Corporate) ? $order->customer->Corporate->responsible_id : null),
                'ticket_number' => random_number(2)
            ]);
        }
    }

    public function rule8()
    {
        $orders8 = Order::with(['customer' => function ($query) {
            $query->with(['Corporate']);
        }])->whereDoesntHave('tickets', function ($query) {
            $query->where('status', 0);
        })
            ->where('status', 3)
            ->whereNull('delivery_status')
            ->get();

        foreach ($orders8 as $order) {
            Ticket::create([
                'order_id' => $order->id,
                'type' => 8,
                'status' => 0,
                'reason' => $this->types[8],
                'assigned_to_id' => (!empty($order->customer->Corporate) ? $order->customer->Corporate->responsible_id : null),
                'ticket_number' => random_number(2)
            ]);
        }
    }

    public function rule9()
    {
        $orders9 = Order::with(['customer' => function ($query) {
            $query->with(['Corporate']);
        }])->whereDoesntHave('tickets', function ($query) {
            $query->where('status', 0);
        })
            ->where('status', 5)
            ->whereDate('recalled_at', '<=', Carbon::now()->subDays(1)->toDateString())
            ->where(function ($query) {
                $query->where('warehouse_dropoff', 0)
                    ->orWhereNull('warehouse_dropoff');
            })
            ->get();

        foreach ($orders9 as $order) {
            Ticket::create([
                'order_id' => $order->id,
                'type' => 9,
                'status' => 0,
                'reason' => $this->types[9],
                'assigned_to_id' => (!empty($order->customer->Corporate) ? $order->customer->Corporate->responsible_id : null),
                'ticket_number' => random_number(2)
            ]);
        }
    }

    public function rule10()
    {
        $orders10 = Order::with(['customer' => function ($query) {
            $query->with(['Corporate']);
        }])->whereDoesntHave('tickets', function ($query) {
            $query->where('status', 0);
        })
            ->where('status', 5)
            ->whereDate('recalled_at', '<=', Carbon::now()->subDays(1)->toDateString())
            ->where('warehouse_dropoff', 1)
            ->where(function ($query) {
                $query->where('client_dropoff', 0)
                    ->orWhereNull('client_dropoff');
            })
            ->get();

        foreach ($orders10 as $order) {
            Ticket::create([
                'order_id' => $order->id,
                'type' => 10,
                'status' => 0,
                'reason' => $this->types[10],
                'assigned_to_id' => (!empty($order->customer->Corporate) ? $order->customer->Corporate->responsible_id : null),
                'ticket_number' => random_number(2)
            ]);
        }
    }

    public function rule11()
    {
        $orders11 = Order::with(['customer' => function ($query) {
            $query->with(['Corporate']);
        }])->whereDoesntHave('tickets', function ($query) {
            $query->where('status', 0);
        })
            ->where('status', 5)
            ->whereDate('recalled_at', '<=', Carbon::now()->subDays(1)->toDateString())
            ->where('warehouse_dropoff', 1)
            ->where('client_dropoff', 1)
            ->where('is_refund', 0)
            ->whereNotNull('refund_collection_id')
            ->get();

        foreach ($orders11 as $order) {
            Ticket::create([
                'order_id' => $order->id,
                'type' => 11,
                'status' => 0,
                'reason' => $this->types[11],
                'assigned_to_id' => (!empty($order->customer->Corporate) ? $order->customer->Corporate->responsible_id : null),
                'ticket_number' => random_number(2)
            ]);
        }
    }
}

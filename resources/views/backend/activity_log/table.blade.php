<div class="row">
  <div class="col-md-12 col-xs-12 col-sm-12">
    @if(! empty($activityLogs) && count($activityLogs) > 0)
        <table id="theTable" class="table table-condensed table-striped text-center">
          <thead>
              <tr>
                <th>#{{__('backend.id')}}</th>
                <th>{{__('backend.username')}}</th>
                <th>{{__('backend.usertype')}}</th>
                {{--<th>{{__('backend.url')}}</th>--}}
                <th>{{__('backend.subject')}}</th>
                <th>{{__('backend.date')}}</th>
                <th>{{__('backend.time')}}</th>
{{--                <th>{{__('backend.delete')}}</th>--}}
                <td  class="text-right"></td>
              </tr>
          </thead>
          <tbody>
              @foreach($activityLogs as $activity)
                  <tr>
                      <td>{{$activity->id}}</td>
                      <td>{{$activity->object_name}}</td>
                      <td>
                          @if($activity->object_type == 1)
                              @if(session()->has('lang') == 'ar')
                                  مشرف
                              @else
                                  Admin
                              @endif
{{--                          @elseif($activity->object_type == 2)--}}
{{--                              @if(session()->has('lang') == 'ar')--}}
{{--                                  وكيل--}}
{{--                              @else--}}
{{--                                  Agent--}}
{{--                              @endif--}}
                          @endif
                      </td>
                      {{--<td>{{$activity->action}}</td>--}}
                      <td>
                          @if(session('lang') == 'ar')
                            {{$activity->subject_ar}}
                          @else
                            {{$activity->subject_en}}
                          @endif
                      </td>
                      <td>{{$activity->created_at->format('d-m-Y')}}</td>
                      <td>{{$activity->created_at->format('H:i:s')}}</td>
{{--                      <td>--}}
{{--                        @if(permission('deleteActivityLog')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->--}}
{{--                          <form action="{{ route('mngrAdmin.activity_log.delete', $activity->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">--}}
{{--                              <input type="hidden" name="_method" value="DELETE">--}}
{{--                              <input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
{{--                              <div class="btn-group pull-right" role="group" aria-label="...">--}}
{{--                                  <button type="submit" class="btn btn-danger btn-sm"> {{__('backend.delete')}} <i class="glyphicon glyphicon-trash"></i></button>--}}
{{--                              </div>--}}
{{--                          </form>--}}
{{--                        @endif--}}
{{--                      </td>--}}
                  </tr>
              @endforeach
          </tbody>
        </table>
        {!! $activityLogs->appends($_GET)->links() !!}
    @else
      <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
    @endif
  </div>
</div>

@extends('captain.layouts.app')
@section('css')
    <title>{{__('captain.the_orders')}} - {{__('captain.scan')}}</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">

    <style>
        .form-check-inline {
            display: -webkit-inline-box;
            display: -ms-inline-flexbox;
            display: inline-flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            padding-left: 0;
            margin-right: .75rem;
        }

        .form-check-inline input {
            margin: 5px;
        }

        .toggle.ios, .toggle-on.ios, .toggle-off.ios {
            border-radius: 20px;
        }

        .toggle.ios .toggle-handle {
            border-radius: 20px;
        }

        .btn-warning {
            background-color: #e47d33;
            border-color: #e47d33;
        }

        .action_btn {
            width: 100px;
            margin: 1px;
            padding: 5px;
        }

        #delivery_problems {
            background-color: #e4cb10;
            border-color: #d6be0f;
            color: #000;
        }

        .dropdown-menu-right {
            right: auto;
            left: 0;
        }

        .table-responsive {
            overflow-y: overlay;
        }

        .tooltip-inner {
            max-width: none !important;
            width: 100% !important;
        }

        .scan-problems.table-striped > tbody > tr:nth-of-type(odd) {
            background-color: initial !important;
        }

        .popover {
            max-width: 800px !important;
        }

        .popover-title .close {
            position: relative;
            bottom: 1px;
            right: 2px;
        }

        .popover-title {
            padding: 0 !important;
            border-bottom: none !important;
        }

        .pac-container {
            z-index: 9999 !important;
        }

        .collected_cost.disabled {
            pointer-events: none;
            opacity: 0.6;
        }

        .counter {
            background-color: white;
            display: flex;
            justify-content: center;
            align-items: center;
            color: black;
            user-select: none;
            width: 145px;
            height: 145px;
            border-radius: 50%;
            margin: auto;
            border: 10px solid grey;
            font-size: 78px;
        }

        table.entries {
            width: 100%;
            border-spacing: 0px;
            margin: 10px 0;
        }

        table.entries thead.fixed {
            position: fixed;
            top: 0px;
        }

        /*.table > thead > tr > th:first-child{*/
        /*    padding-left: 5px !important;*/
        /*}*/

        #theTable thead {
            background: #6d6868;
            color: white;
        }

        .searched-past1 {
            border-top: 3px solid #6c6a6a;
            border-left: 3px solid #6c6a6a;
            border-right: 3px solid #6c6a6a
        }

        .searched-past2 {
            border-bottom: 3px solid #6c6a6a;
            border-left: 3px solid #6c6a6a;
            border-right: 3px solid #6c6a6a
        }

        .searched-current1 {
            border-top: 6px solid #74d774;
            border-left: 6px solid #74d774;
            border-right: 6px solid #74d774
        }

        .searched-current2 {
            border-bottom: 6px solid #74d774;
            border-left: 6px solid #74d774;
            border-right: 6px solid #74d774
        }

        .navbar-fixed-top {
            position: relative !important;
        }

        #wrapper #sidebar-nav, #wrapper .main {
            padding-top: 0px;
        }

        #wrapper .sidebar {
            top: 0 !important;
            padding-top: 60px !important;
        }

        .firstOne td {
            border-top: 3px solid red !important;
        }

        .firstOne td:first-child {
            border-left: 3px solid red !important;
        }

        .firstOne td:last-child {
            border-right: 3px solid red !important;
        }

        .secondOne td {
            border-bottom: 2px solid red !important;
        }

        .secondOne td:first-child {
            border-left: 3px solid red !important;
        }

        .secondOne td:last-child {
            border-right: 3px solid red !important;
        }

        #wrapper .main{
            clear: both;
        }
    </style>
@endsection
@section('header')
    <div class="page-header">
        <h4>
            <i class="glyphicon glyphicon-plus"></i> {{__('captain.scan_order')}} </h4>
    </div>
@endsection

@section('content')

    <audio id="alert-ding" src="{{asset('alert_ding.wav')}}" preload="auto"></audio>

    <div class="container-fluid">
        <div class="alert alert-success" role="alert" style="display: none">
        </div>
        <div class="row">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group col-sm-6" style="display: none">
                <label class="control-label" for="code-scan"> {{__('captain.Scanned_Code')}}</label>
            </div>
            <div class="col-md-4" style="padding: 0">
                <label class="control-label" for="code-scan"> {{__('captain.Scanned_Code')}}</label>
                <textarea class="form-control" id="code-scan" rows="6"
                          style="min-height: 100px; resize: vertical; margin-top: 15px"
                          name="code"></textarea>

                <button type="button" class="btn btn-warning" id="scan-btn"
                        style="margin-top: 15px; display: inline-block;vertical-align: baseline;"> {{__('captain.scan')}}</button>
                <a class="action_btn btn btn-success disabled" id="forward">
                    <i class="fa fa-eye"></i> {{__('captain.forward')}}
                </a>
            </div>

            <div class="col-md-3" style="padding-top: 30px">
                <main>
                    <div class="counter" id="orders_count"><span class="number">0</span></div>
                </main>
            </div>

        </div>
    </div>

    <div class="text-center" style="color:#5673ea;font-size: 54px; display: none" id="tempo"><h1
            class="fa fa-refresh fa-spin text-center"></h1>
    </div>

    <div class="text-center list" id="table">

    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('/assets/scripts/map-order.js')}}"></script>

    <script
        src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key=AIzaSyDQiN4qc7pC9b1BRQwkqXXD28peMfWcHvw&callback=myMap"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

    <script src="{{ asset('/assets/scripts/jquery-code-scanner.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <script type="text/javascript" src="{{asset('/assets/scripts/bootstrap-notify.min.js')}}"></script>

    <script>
        TableThing = function (params) {
            settings = {
                table: $('#theTable'),
                thead: []
            };

            this.fixThead = function () {
                // empty our array to begin with
                settings.thead = [];
                // loop over the first row of td's in &lt;tbody> and get the widths of individual &lt;td>'s
                $('tbody tr:first td', $('#theTable')).each(function (i, v) {
                    settings.thead.push($(v).width());
                });

                // now loop over our array setting the widths we've got to the &lt;th>'s
                for (i = 0; i < settings.thead.length; i++) {
                    $('#floatThead th:eq(' + i + ')', $('#theTable')).width(settings.thead[i]);
                }


            }
        }
        $(function () {
            var table = new TableThing();

            $(window).scroll(function () {
                $("#floatThead", $('#theTable')).width($('#theTable').width());
                var windowTop = $(window).scrollTop();
                if ($('#theTable').length && windowTop > $('#theTable').offset().top) {
                    if (!$("#floatThead", $('#theTable')).hasClass("fixed")) {
                        $("#floatThead", $('#theTable')).show().addClass("fixed");
                        table.fixThead();
                    }
                } else {
                    $("#floatThead", $('#theTable')).hide().removeClass("fixed");
                }
            });
        });

        $.notifyDefaults({
            type: 'success',
            allow_dismiss: true
        });

        $(document).ready(function () {
            const counter = document.querySelector(".counter")
            let number = document.querySelector(".number")
            let i = 0

            function count() {
                i++
                number.innerHTML = i
            }

            counter.addEventListener("click", count);
        });

        function change_status(info = {}, orders = [], customer = "") {
            let ids = [];
            let idsWithCollectedCost = [];
            let customer_id = '';
            $(".selectpdf input:checked").each(function () {
                if (!orders || orders.length === 0) {
                    ids.push($(this).val());
                }
                if (!customer) {
                    customer_id = $(this).attr('data-customer');
                }

            });

            if (orders && orders.length !== 0) {
                ids = orders
            }

            if (customer) {
                customer_id = customer
            }

            if (ids.length > 0) {
                info['ids'] = ids;
                info['idsWithCollectedCost'] = idsWithCollectedCost;
                info['_token'] = "{{csrf_token()}}";
                info['customer_id'] = customer_id;
                $.ajax({
                    url: "{{ url('/captainDashboard/scan_order_action')}}",
                    type: 'post',
                    data: info,
                    success: function (data) {
                        if (data.ids_status) {
                            data.ids_status.forEach(function (entry) {
                                if (data.status) {
                                    $("#" + entry.id + " .status").html(entry.status);
                                    console.log('entry ', entry);
                                }
                            });
                        } else if (data.ids) {
                            data.ids.forEach(function (entry) {
                                if (data.status) {
                                    $("#" + entry + " .status").html(data.status);
                                    console.log('entry ', entry);
                                }

                                if (data.driver) {
                                    $('#driver_' + entry).html(data.driver.name); // Unchecks &disabled it
                                    $('#selectOrder_' + entry).attr('data-driver', data.driver.id); // Unchecks &disabled it
                                    $('#' + entry).attr('data-driver', data.driver.id);
                                }

                            });
                        }

                        if (data.result == true) {
                            $(".alert").removeClass('alert-danger').addClass('alert-success');
                        } else {
                            $(".alert").removeClass('alert-success').addClass('alert-danger');
                        }

                        window.scrollTo(0, 0);
                        $(".alert").html(data.message);
                        $(".alert").show();
                        window.setTimeout(function () {
                            $(".alert").hide();
                        }, 3000);
                    },
                    error: function (data) {
                        play_alert();
                        a.open();
                    }
                });
            }
        }

        function check_validation(info = {}, id = "", customer = "") {
            var ids = [];
            var customer_id = customer;
            var collected_cost = null;
            var one_row = 0;

            if (id) {
                ids.push(id);
                collected_cost = $(".collected_cost[data-id='" + id + "']").val();
                one_row = 1;
            } else {
                $(".selectpdf input:checked").each(function () {
                    ids.push($(this).val());
                });
            }

            if (ids.length > 0) {
                info['ids'] = ids;
                info['_token'] = "{{csrf_token()}}";

                $.ajax({
                    url: "{{ url('/captainDashboard/scan_order_validation')}}",
                    type: 'post',
                    data: info,
                    success: function (data) {
                        if (data.result == false) {
                            play_alert();
                            a.open();
                        } else {
                            if (info['type'] == 'forward') {
                                $("#forwardModal").modal("show");
                            }
                        }
                    },
                    error: function (data) {
                        play_alert();
                        a.open();
                    }
                });
            }
        }

        $(function () {
            /*print*/
            $('.toggle').bootstrapToggle();

            $('body').on('click', '#removeRow', function (e) {
                e.preventDefault();
                count--;
                console.log('hererererre');
                let order_id = $(this).attr('data-id');
                $(this).parents("tr").next().remove();
                $(this).parents("tr").remove();
                $("#orders_count").html(count);
                $('.serial').each(function (i, obj) {
                    $(this).html(++i);
                })
            });

            $('body').on('click', '#forward', function (e) {
                check_validation({
                    type: "forward"
                });

            });

            $('body').on('click', '#forward_action', function (e) {
                if ($("#driver").val()) {
                    change_status({
                        type: 'forward',
                        driver_id: $("#driver").val()
                    });

                    $("#forwardModal").modal("hide");
                }
            });

        });

        $('body').on('change', '.selectOrder', function (e) {
            if ($('input[name="select[]"]').is(':checked')) {
                $("#forward").removeClass('disabled');
            } else {
                $(".action_btn").not('#toggle').addClass('disabled');
            }
        });

        $('body').on('click', '#toggle', function () {

            switch ($(this).attr('data-state')) {
                case "1" :
                    $("input.selectOrder:not(:disabled)").prop('checked', true).trigger('change');
                    // $(".selectOrder").prop('checked', true).trigger('change');
                    $(this).attr('data-state', "2");
                    break;
                case "2" :
                    $("input.selectOrder:not(:disabled)").prop('checked', false).trigger('change');
                    // $(".selectOrder").prop('checked', false).trigger('change');
                    $(this).attr('data-state', "1");
                    break;
            }
        });

    </script>

    <script type="text/javascript">
        var page = 1;
        var init = 1;
        var count = 0;
        var sort_status = null;
        var rearrange = 0;
        var code_scan = "";

        $(function () {

            $('#code-scan').codeScanner({
                onScan: function ($element, code) {

                    if (!$('#quick_search').is(':checked')) {
                        code_scan = code;
                        console.log($element.val(), code, $element.val().includes(code));
                        if (!$element.val().includes(code)) {
                            if ($element.val()) {
                                $element.val($element.val() + ',' + code);
                            } else {
                                $element.val(code);
                            }
                        } else {
                            console.log($element.val().trim().split(code));
                            if ($("#code-scan").is(":focus") && $element.val().trim().split(code).length > 2) {
                                const str = $element.val().slice(0, parseInt('-' + code.length));
                                $element.val(str);
                            }
                        }

                        getData(page);
                    }

                }
            });

            $("#scan-btn").on('click', function () {
                sort_status = null;
                rearrange = 0;
                code_scan = $("#code-scan").val();
                getData(page);
            });

            $(document).on('click', '.pagination a', function (event) {
                event.preventDefault();

                $('li').removeClass('active');
                $(this).parent('li').addClass('active');

                var myurl = $(this).attr('href');
                var page = $(this).attr('href').split('page=')[1];

                //getData(page);
                location.hash = page;
            });


        });

        var open = false;
        var a = $.confirm({
            lazyOpen: true,
            animation: 'zoom',
            title: '',
            theme: 'material',
            content: '' +
                '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c">' +
                '<i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">' +
                '{{__("captain.scan_order")}}</span></div><div style="font-weight:bold;">' +
                '<p>{{__("captain.bad_choice")}}</p>' +
                '</div>',
            type: 'red',
            onOpen: function () {
                // after the modal is displayed.
                open = true;
            },
            onClose: function () {
                // after the modal is displayed.
                open = false;
            },
        });

        function getData(page) {
            console.log(page);
            $("#tempo").show();
            if (init == 1) {
                $('.list').html('');
            }
            if (open) {
                a.close();
            }
            $.ajax({
                url: '{{URL::asset("/captainDashboard/scan_order_qrcode")}}' + '?code=' + code_scan + '&page=' + page + '&init=' + init,
                type: 'get',
                data: {
                    'filter': $("input[name='radio_filter']:checked").val(),
                    'sort_status': sort_status,
                    'rearrange': rearrange
                },
                success: function (data) {

                    $("#tempo").hide();

                    if (init == 1) {
                        if (data.count != 0) {
                            init = 2;
                            count += data.count;
                            $("#orders_count").html(count);
                        }
                        $('.list').empty().html(data.view);

                    } else {
                        if (data.count != 0) {
                            data.views.forEach(function (entry) {
                                const myEle = document.getElementById(entry.id);
                                if (!myEle) {
                                    count++;
                                    $("#orders_count").html(count);
                                    $("#tbody").append(entry.view);
                                    $("#" + entry.id + " .serial").html(count);
                                } else {
                                    play_alert();
                                }
                            });
                        }
                    }

                    $(".toggle").bootstrapToggle('destroy');
                    $(".toggle").bootstrapToggle();
                    location.hash = page;

                    if (data.count != 0) {
                        let code_val = $("#code-scan").val();
                        if (code_val) {
                            if (!code_val.includes(code_scan)) {
                                code_val += ',' + code_scan;
                                // getData(page);
                            }
                        } else {
                            // getData(page);
                            code_val = code_scan;
                        }

                        $("#code-scan").val(code_val);
                    } else {
                        // document.getElementById('alert-ding').play();
                        play_alert();
                        a.open();
                    }

                    $('.testtooltip').popover({
                        container: 'body'
                    });
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }

        function play_alert() {
            var snd = new Audio("{{asset('alert_ding.wav')}}");
            snd.play();
        }

        $(document).on("click", ".popover .close", function () {
            $(this).parents(".popover").popover('hide');
            $(this).parents(".popover").remove();
        });

    </script>
@endsection

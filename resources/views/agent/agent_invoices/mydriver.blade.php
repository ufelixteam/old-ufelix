@extends('agent.layouts.app')
@section('header')
  <div class="page-header clearfix">
    <h3>
      <i class="glyphicon glyphicon-align-justify"></i> My Driver
    </h3>
  </div>
@endsection
  @section('content')
      @if(count($invoices) > 0)
      <table class="table table-striped">
        <thead class="thead-dark">
          <tr>
            <th scope="col">#ID</th>
            <th scope="col">Drive Name </th>
            <th scope="col">Drive Mobile</th>
            <th scope="col">Start Date</th>
            <th scope="col">End Date</th>
            <th scope="col">Status</th>
            <th scope="col">VEIW</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($invoices as $invoice)
            <tr>
              <th scope="row">{{$invoice->driver_id}}</th>
              <th scope="row">{{$invoice->name}}</th>
              <th scope="row">{{$invoice->mobile}}</th>
              <td>{{$invoice->start_date}}</td>
              <td>{{$invoice->end_date}}</td>
              <td>
                @if($invoice->status == 0)
                  <span class='badge badge-pill badge-success'>open</span>
                @elseif($invoice->status == 1)
                  <span class='badge badge-pill badge-danger'>Cancelled</span>
                @endif
              </td>
              <td>
                <a href="{{ url('mngrAgent/invoices/driveres/show' ,$invoice->driver_id)}}">
                  <button class="btn btn-primary btn-sm" type="button" name="button">
                    veiw
                  </button>
                </a>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
  @else
      <h3 class="text-center alert alert-warning">No Result Found !</h3>
  @endif
@endsection

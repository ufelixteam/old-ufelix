<div class="row">
  <div class="col-md-12 col-xs-12 col-sm-12">
    <?php if($customers->count()): ?>
      <table class="table table-condensed table-striped">
        <thead>
          <tr>
            <th>#</th>
            <th><?php echo e(__('backend.name')); ?></th>
            <th><?php echo e(__('backend.mobile')); ?></th>
            <th><?php echo e(__('backend.block')); ?></th>
            <th><?php echo e(__('backend.verify_email')); ?></th>
            <th><?php echo e(__('backend.verfiy_mobile')); ?></th>
            <th><?php echo e(__('backend.created_at')); ?></th>
            <th class="text-right"></th>
          </tr>
        </thead>
        <tbody>
          <?php $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
              <td><?php echo e($customer->id); ?></td>
              <td><?php echo e($customer->name); ?></td>
              <td><?php echo e($customer->mobile); ?></td>

              <td>
                <input data-id="<?php echo e($customer->id); ?>" data-size="mini" class="toggle change_block"  <?php echo e($customer->is_block == 1 ? 'checked' : ''); ?> data-onstyle="danger" type="checkbox" data-style="ios" data-on="Yes" data-off="No">
              </td>

              <td>
               
                <input data-id="<?php echo e($customer->id); ?>" data-size="mini" class="toggle verify_email"  <?php echo e($customer->is_verify_email == 1 ? 'checked' : ''); ?> data-onstyle="success" type="checkbox" data-style="ios" data-on="Yes" data-off="No" >
              </td>
              <td>
                
                <input data-id="<?php echo e($customer->id); ?>" data-size="mini" class="toggle verify_mobile"  <?php echo e($customer->is_verify_mobile == 1 ? 'checked' : ''); ?> data-onstyle="success" type="checkbox" data-style="ios" data-on="Yes" data-off="No" >

              </td>

              <td><?php echo e($customer->created_at); ?></td>

              <td class="text-right">
      
                <?php if(permission('showCustomer')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                  <a class="btn btn-xs btn-primary" href="<?php echo e(route('mngrAdmin.customers.show', $customer->id)); ?>"><i class="glyphicon glyphicon-eye-open"></i> <?php echo e(__('backend.view')); ?></a>
                <?php endif; ?>

                <?php if(permission('editCustomer')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                  <a class="btn btn-xs btn-warning" href="<?php echo e(route('mngrAdmin.customers.edit', $customer->id)); ?>"><i class="glyphicon glyphicon-edit"></i> <?php echo e(__('backend.edit')); ?></a>
                <?php endif; ?>
               
              </td>
            </tr>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
      </table>

      <?php echo $customers->appends($_GET)->links(); ?>


    <?php else: ?>
        <h3 class="text-center alert alert-warning"><?php echo e(__('backend.No_result_found')); ?></h3>
    <?php endif; ?>
  </div>
</div>
<script>
$('.toggle').bootstrapToggle();
</script><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/customers/table.blade.php ENDPATH**/ ?>
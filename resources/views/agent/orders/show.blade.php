@extends('agent.layouts.app')
@section('header')
<div class="page-header">
    <h3>Show Order No.#{{$order->id}}</h3> {!! $order->status_span !!} Payment : {!! ! empty($order->payment_method ) ? '<span class="badge badge-pill badge-success">'.$order->payment_method->name.'</span>' : '' !!} Price: {{! empty($order->accepted )  ? $order->accepted->total_price : ''}}
   
</div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
         
                   
                <div class="col-sm-12" style="border: 2px solid #fdb801;color: #fff;margin-bottom: 20px;background-color: #fdb801;" >
                <h5>Reciever Data</h5>
                </div>
                <div class="form-group col-sm-6">
                     <label for="reciever_name">NAME</label>
                     <p class="form-control-static">{{$order->reciever_name}}</p>
                </div>
                    <div class="form-group col-sm-6">
                     <label for="reciever_mobile">MOBILE</label>
                     <p class="form-control-static">{{$order->reciever_mobile}}</p>
                </div>
                <div class="form-group col-sm-6">
                     <label for="reciever_phone">PHONE</label>
                     <p class="form-control-static">{{$order->reciever_phone}}</p>
                </div>
               
                <div class="form-group col-sm-6">
                     <label for="reciever_address">ADDRESS</label>
                     <p class="form-control-static">{{$order->reciever_address}}</p>
                </div>
                    <div class="form-group col-sm-6">
                     <label for="reciever_famous_palce">FAMOUS PALCE</label>
                     <p class="form-control-static">{{$order->reciever_famous_palce}}</p>
                </div>


                 <div class="form-group col-sm-6">
                     <label for="customer_id">CUSTOMER Account</label>
                     <p class="form-control-static">{{! empty($order->customer) ? $order->customer->name : '' }}</p>
                     <p class="form-control-static">{{! empty($order->customer) ? $order->customer->email : '' }}</p>
                     <p class="form-control-static">{{! empty($order->customer) ? $order->customer->mobile : '' }}</p>
                </div>

               
                      
                <div class="col-sm-12" style="border: 2px solid #fdb801;color: #fff;margin-bottom: 20px;background-color: #fdb801;" >
                <h5>SENDER Data</h5>
                </div>
                <div class="form-group col-sm-6">
                     <label for="sender_name">NAME</label>
                     <p class="form-control-static">{{$order->sender_name}}</p>
                </div>
                <div class="form-group col-sm-6">
                     <label for="sender_mobile">MOBILE</label>
                     <p class="form-control-static">{{$order->sender_mobile}}</p>
                </div>
                <div class="form-group col-sm-6">
                     <label for="sender_phone">PHONE</label>
                     <p class="form-control-static">{{$order->sender_phone}}</p>
                </div>
                  
                <div class="form-group col-sm-6">
                     <label for="sender_address">ADDRESS</label>
                     <p class="form-control-static">{{$order->sender_address}}</p>
                </div>
                    <div class="form-group col-sm-6">
                     <label for="sender_famous_palce">FAMOUS_PALCE</label>
                     <p class="form-control-static">{{$order->sender_famous_palce}}</p>
                </div>
                    
                <div class="col-sm-12" style="border: 2px solid #fdb801;color: #fff;margin-bottom: 20px;background-color: #fdb801;" >
                <h5>Package Data</h5>
                </div>
                <div class="form-group col-sm-6">
                     <label for="type_id">TYPE</label>
                     <p class="form-control-static">{{! empty($order->typeData) ? $order->typeData->name : '' }}</p>
                </div>
                    <div class="form-group col-sm-6">
                     <label for="weight_id">WEIGHT</label>
                     <p class="form-control-static">{{! empty($order->weight) ? $order->weight->weight : '' }}</p>
                </div>
                 <div class="form-group col-sm-6">
                     <label for="order_number">Code</label>
                     <p class="form-control-static">{{$order->order_number}}</p>
                </div>

               
                  
                <div class="form-group col-sm-6">
                     <label for="size_id">SIZE</label>
                     <p class="form-control-static">{{! empty($order->size) ? $order->size->size : ''}}</p>

                </div>
                    <div class="form-group col-sm-6">
                     <label for="notes">NOTES</label>
                     <p class="form-control-static">{{$order->notes}}</p>
                </div>



                 <div class="col-sm-12" style="border: 2px solid #fdb801;color: #fff;margin-bottom: 20px;background-color: #fdb801;" >
                <h5>Driver Data</h5>
                </div>
                <div class="form-group col-sm-6">
                     <label for="sender_name">NAME</label>
                     <p class="form-control-static">{{ ! empty($order->accepted ) && ! empty($order->accepted->driver) ? $order->accepted->driver->sender_name : ''}}</p>
                </div>
                <div class="form-group col-sm-6">
                     <label for="sender_mobile">MOBILE</label>
                     <p class="form-control-static">{{ ! empty($order->accepted ) && ! empty($order->accepted->driver) ? $order->accepted->driver->sender_mobile : ''}}</p>
                </div>
                <div class="form-group col-sm-6">
                     <label for="email">EMAIL</label>
                     <p class="form-control-static">{{! empty($order->accepted ) && ! empty($order->accepted->driver) ? $order->accepted->driver->email : ''}}</p>
                </div>
                  
                <div class="form-group col-sm-6">
                     <label for="sender_address">ADDRESS</label>
                     <p class="form-control-static">{{  ! empty($order->accepted ) && ! empty($order->accepted->driver) ? $order->accepted->driver->address : ''}}</p>
                </div>
                   
               
          
            <div class="col-md-12 col-xs-12 col-sm-12">
                <a class="btn btn-link" href="{{ route('mngrAgent.orders.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
            </div>

        </div>
    </div>

@endsection
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GovernorateKeyword extends Model
{

    protected $guarded = ['id'];

    public function governorate()
    {
        return $this->belongsTo('App\Models\Governorate', 'governorate_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Truck extends Model
{

    public function getStatusSpanAttribute($value) {
        /*
        * status == 0 :: Not Verfiy
        * status == 1 :: Verfiy
        */
        if($this->status == 1){
            $value="<span class='badge badge-pill label-success'>".__('backend.active')."</span>";
        }else{
          $value="<span class='badge badge-pill label-danger'>".__('backend.Unactive')."</span>";
        }
        return $value;
    }

    public function getStatusTxtAttribute($value)
    {
        /*
        * status == 0 :: Not Verfiy
        * status == 1 :: Verfiy
        */
        if($this->status != 1){
            $value= __('backend.verfiy');
        }else{
          $value= __('backend.not_verify');
        }
        return $value;
    }

    public function color() {
        return $this->belongsTo('App\Models\Color','color_id');
    }
    public function driver() {
        return $this->belongsTo('App\Models\Driver','driver_id');
    }
    public function vehiceType() {
        return $this->belongsTo('App\Models\VehiceType','type_id');
    }
    public function agent() {
        return $this->belongsTo('App\Models\Agent','agent_id');
    }
    public function model_type() {
        return $this->belongsTo('App\Models\ModelType','model_type_id');
    }
//license_image_front
public function getLicenseImageFrontAttribute($value)
    {
        if ($value != "") {
            $value = url("/") . "/api_uploades/driver/car_license_front/" . $value;
        } else {
            $value = "";
        }
        return $value;
    }
}

<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Permission_admin;
use App\Models\Role_admin;
use App\Models\TypePermission;
use App\Models\User;
use Flash;
use Illuminate\Http\Request;

class AdminRoleController extends Controller
{

    // Get All Roles
    public function index()
    {
        $adminRoles = Role_admin::all();
        return view('backend.admin-roles.index', compact('adminRoles'));
    }

    // Create Role Page
    public function create()
    {
        $permissionType = TypePermission::with('permissions')->get();
        $permissions = Permission::all();
        return view('backend.admin-roles.create', compact('permissionType', 'permissions'));
    }

    // Added New Role
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'permation' => 'required',
        ]);

        $add_role = new Role_admin();
        $add_role->name = $request->name;
        $add_role->is_warehouse_role = $request->is_warehouse_role;
        $add_role->save();

        foreach ($request->permation as $permation) {
            $add_permation_admin = new Permission_admin();
            $add_permation_admin->permission_id = $permation;
            $add_permation_admin->role_admin_id = $add_role->id;
            $add_permation_admin->save();
        }

        // Function Add Action In Activity Log By Samir
        addActivityLog('Add New Admin Role ' . $add_role->name, ' اضافة دور مشرف جديد ' . $add_role->name, 18); // Helper Function in App\Helpers.php

        Flash::success(__('backend.Role_Admin_saved_successfully'));
        return redirect()->route('mngrAdmin.admin_roles.index');

    }

    // View One Role
    public function show($id)
    {
        $roleAdmin = Role_admin::find($id);
        $permission_admin = Permission_admin::where('role_admin_id', $id)->get();  // Get All Permissions For One Admin Role
        $permissionType = TypePermission::with('permissions')->get();
        $permissions = Permission::get();

        return view('backend.admin-roles.show', compact('roleAdmin', 'permissionType', 'permissions', 'permission_admin'));
    }

    // Edit Role Page
    public function edit($id)
    {
        $roleAdmin = Role_admin::find($id);
        $permission_admin = Permission_admin::where('role_admin_id', $id)->get();  // Get All Permissions For One Admin Role
        $permissionType = TypePermission::with('permissions')->get();
        $permissions = Permission::get();

        return view('backend.admin-roles.edit', compact('roleAdmin', 'permissionType', 'permissions', 'permission_admin'));
    }

    // Update Role
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'permation' => 'required',
        ]);

        $edit_role = Role_admin::find($id);
        $edit_role->name = $request->name;
        $edit_role->is_warehouse_role = $request->is_warehouse_role;
        $edit_role->save();

        $del_old_permation = Permission_admin::where('role_admin_id', $id)->delete();
        foreach ($request->permation as $permation) {
            $edit_permation_admin = new Permission_admin();
            $edit_permation_admin->permission_id = $permation;
            $edit_permation_admin->role_admin_id = $edit_role->id;
            $edit_permation_admin->save();
        }

        // Function Add Action In Activity Log By Samir
        addActivityLog('Edit Admin Role ' . $edit_role->name, ' تعديل دور المشرف ' . $edit_role->name, 18); // Helper Function in App\Helpers.php

        Flash::success(__('backend.Role_Admin_Updated_successfully'));
        return redirect()->route('mngrAdmin.admin_roles.index');
    }

    public function destroy($id)
    {
        $user = Role_admin::findOrFail($id);
        $user->delete();
        return redirect()->route('mngrAdmin.admin_roles.index')->with('message', __('backend.Role_deleted_successfully'));
    }

    // Get All users By role ID
    public function get_users($id)
    {
        $user = Auth::guard($this->guardType)->user();
        $users = User::where('role_admin_id', $id)->where('is_active', 1)
            ->select('users.*')
            ->orderBy('users.name', 'ASC');

        if ($user->is_warehouse_user) {
            $users = $users->join('store_user', 'store_user.store_id', '=', 'users.id')
                ->where('user_id', $user->id);

        }

        return $users->get();
    }


}

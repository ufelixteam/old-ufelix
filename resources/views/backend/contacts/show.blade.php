@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.contacts')}} - {{__('backend.view')}}</title> 
@endsection

@section('header')
  <div class="page-header">
      <h3>{{__('backend.view')}} {{__('backend.contacts')}} #{{$contact->id}}</h3>{!! $contact->type_span !!}
      <form action="{{ route('mngrAdmin.contacts.destroy', $contact->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
          <input type="hidden" name="_method" value="DELETE">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="btn-group pull-right" role="group" aria-label="...">
            @if(permission('editContact')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
              <a class="btn btn-warning btn-group" role="group" href="{{ route('mngrAdmin.contacts.edit', $contact->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
            @endif

            @if(permission('deleteContact')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
              <button type="submit" class="btn btn-danger"> {{__('backend.delete')}} <i class="glyphicon glyphicon-trash"></i></button>
            @endif
          </div>
      </form>
  </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="form-group col-md-6">
                     <label for="name">{{__('backend.name')}}</label>
                     <p class="form-control-static">{{$contact->name}}</p>
                </div>
                    <div class="form-group col-md-6">
                     <label for="email">{{__('backend.email')}}</label>
                     <p class="form-control-static">{{$contact->email}}</p>
                </div>
                    <div class="form-group col-md-6">
                     <label for="mobile">{{__('backend.mobile')}}</label>
                     <p class="form-control-static">{{$contact->mobile}}</p>
                </div>
                    <div class="form-group col-md-6">
                     <label for="title">{{__('backend.title')}}</label>
                     <p class="form-control-static">{{$contact->title}}</p>
                </div>
                    <div class="form-group col-md-6">
                     <label for="message">{{__('backend.message')}}</label>
                     <p class="form-control-static">{{$contact->message}}</p>
                </div>

            <div class="col-md-12 col-xs-12 col-sm-12">
                <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.contacts.index') }}"><i class="glyphicon glyphicon-backward"></i>  {{__('backend.back')}}</a>
            </div>

        </div>
    </div>

@endsection

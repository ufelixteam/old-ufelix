@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.contacts')}}</title>
@endsection

@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.contacts')}}
            @if(permission('addContact')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
              <a class="btn btn-success pull-right" href="{{ route('mngrAdmin.contacts.create') }}"><i class="glyphicon glyphicon-plus"></i> {{__('backend.create')}}</a>
            @endif
        </h3>
    </div>
@endsection

@section('content')
    <div class="row" style="overflow-x: scroll">
        <div class="col-md-12 col-xs-12 col-sm-12">
            @if($contacts->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('backend.name')}}</th>
                            <th>{{__('backend.email')}}</th>
                            <th>{{__('backend.mobile')}}</th>
                            <th>{{__('backend.date')}}</th>
                            <th>{{__('backend.title')}}</th>
                            <th width="20%">{{__('backend.message')}}</th>
                            <th>{{__('backend.type')}}</th>
                            <th class="text-right"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($contacts as $contact)
                            <tr>
                                <td>{{$contact->id}}</td>
                                <td>{{$contact->name}}</td>
                                <td>{{$contact->email}}</td>
                                <td>{{$contact->mobile}}</td>
                                <td>{{$contact->created_at->format('Y-m-d H:i:s')}}</td>
                                <td>{{$contact->title}}</td>
                                <td >{{$contact->message}}</td>
                                <td>{!! $contact->type_span !!}</td>
                                <td class="text-right">
                                    @if(permission('showContact')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                      <a class="btn btn-xs btn-primary" href="{{ route('mngrAdmin.contacts.show', $contact->id) }}"><i class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>
                                    @endif

                                    @if(permission('editContact')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                      <a class="btn btn-xs btn-warning" href="{{ route('mngrAdmin.contacts.edit', $contact->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                                    @endif

                                    @if(permission('deleteContact')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                      <form action="{{ route('mngrAdmin.contacts.destroy', $contact->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
                                          <input type="hidden" name="_method" value="DELETE">
                                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                          <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}</button>
                                      </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {!! $contacts->appends($_GET)->links() !!}

            @else
                <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
            @endif
        </div>
    </div>
@endsection

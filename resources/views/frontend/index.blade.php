
@extends('frontend.layouts.app')

@section('title') @lang('website.Home') @endsection


@section('content')

@include('frontend.home.slider')
@include('frontend.home.why_us')
@include('frontend.home.services')
@include('frontend.home.statistic')
@include('frontend.home.customers')


@endsection


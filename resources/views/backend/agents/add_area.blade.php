
<div class="modal fade" id="areaModal" tabindex="-1" role="dialog" aria-labelledby="areaModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="areaModalLabel">{{__('backend.Add_Area_Title')}} </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ url('mngrAdmin/add_area_agent') }}" method="POST" >

      <div class="modal-body">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="agent_id" value="{{ $agent->id }}">
        <div class="row">
          <div class="form-group col-md-12 col-sm-12 @if($errors->has('government_id')) has-error @endif">
            <label for="government_id-field"> {{__('backend.government')}}: </label>
            <select  class="form-control government_id" id="government_id"  name="government_id">
                
                @if(!empty($governments))
                    @foreach($governments as $government)
                        <option value="{{$government->id}}" >
                          {{$government->name_en}}
                        </option>
                    @endforeach
                @endif
            </select>
            @if($errors->has("government_id"))
            <span class="help-block">{{ $errors->first("government_id") }}</span>
            @endif
          </div>
        </div>
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">{{__('backend.Add_Area')}}</button>
      </div>
      </form>

    </div>
  </div>
</div>
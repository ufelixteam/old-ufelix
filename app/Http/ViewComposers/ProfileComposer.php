<?php

namespace App\Http\ViewComposers;

use App\Models\Device;
use App\Models\Governorate;
use App\Models\Notification;
use App\Models\Setting;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class ProfileComposer
{

    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $allsettings = Setting::get();

        $settings = [];
        foreach ($allsettings as $setting) {

            $settings[$setting->key] = $setting->value;
            $settings[$setting->key . '_ar'] = $setting->value_ar;
        }

        $view->with('app_about', $settings);

        $app_governments = Governorate::orderBy('name_en', 'ASC')->get();

        $view->with('app_governments', $app_governments);

        //1 admin , 2 driver , 3 agent , 4 customer
        if (Auth::guard('Customer')->user() && (app('request')->is('customer/*', 'en/customer/*', 'ar/customer/*'))) {
            if (Device::where('object_id', Auth::guard('Customer')->user()->id)->doesntExist()) {
                Device::insert(['object_id' => Auth::guard('Customer')->user()->id, 'object_type' => 4, 'device_id' => 10, 'device_type' => 'web', 'socket_token' => 'dIFLIOnM0jtTZ75WAAK2', 'status' => 0]);
            }
        }

    }

}

?>

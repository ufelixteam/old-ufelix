@extends('frontend.profile.layouts.app')

@section('title') @lang('website.Orders') @endsection
@section('css')
    <title>{{__('backend.corporates')}}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <style>
        input[type=checkbox], input[type=radio] {
            width: 18px;
            height: 18px;
        }

        .printdata {
            cursor: pointer;
        }

        .selectAll-button {
            color: #fff;
            background-color: #5bc0de;
            border-color: #46b8da;

        }

        .submit-button {
            color: #fff;
            background-color: #0376d9;
        }

        .cancel-button {
            background-color: #ff5900;
            border-color: #ed5301;
        }

        .button-padding {
            padding: 5px 14px;
        }
    </style>
@endsection
@section('content')
    <div class="flash-message"></div>
    <!-- Modal For Cancel Orders-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">{{__('website.add_cancel_comment')}}</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="reason">{{__('website.comment')}}</label>
                            <textarea class="form-control" id="reason" name="reason" rows="3"></textarea>
                        </div>
                        <div class="col-6"></div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal"
                                    style="margin-left: 15px;">Close
                            </button>
                            <button type="submit" class="btn btn-primary pull-right cancel-order submit-button"
                                    id="cancel-order" data-value="dropped"
                            >{{__('website.submit')}}</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="">
                <div class="table-responsive m-t-40">
                    <h1 class="title" style="margin-bottom: 10px">
                        @lang('backend.customers')
                        <div class="button-box">
                            <a href="{{ app()->getLocale() == 'en' ? url('/en/customer/account/customers/create') : url('/customer/account/customers/create') }}" class="btn btn-primary" style="">
                                @lang('backend.add_customer')</a>
                        </div>
                    </h1>
                    <form
                        action="{{ app()->getLocale() == 'en' ? url('/en/customer/account/customers') : url('/customer/account/customers') }}"
                        method="get">
                        <input type="hidden" name="type" value="{{ app('request')->input('type')}}">
                        <div class="row" style="margin-bottom:15px;" id="customers">
                            <div class="col-md-12" style="padding: 0">
                                <div class="group-control col-md-3 col-sm-3">
                                    <input type="text" class="form-control" id="search-field" name="search"
                                           value="{{ app('request')->input('search')}}"
                                           placeholder="{{__('backend.search')}} ">
                                </div>

{{--                                <div class="group-control col-md-3 col-sm-3">--}}
{{--                                    <select id="r_government_id" name="r_government_id" class="form-control">--}}
{{--                                        <option value=""--}}
{{--                                                data-display="Select">{{__('backend.Receiver_Government')}}</option>--}}
{{--                                        @if(! empty($app_governments))--}}
{{--                                            @foreach($app_governments as $government)--}}
{{--                                                <option value="{{$government->id}}"--}}
{{--                                                        @if(app('request')->input('r_government_id') == $government->id) selected @endif>--}}
{{--                                                    {{$government->name_en}}--}}
{{--                                                </option>--}}

{{--                                            @endforeach--}}
{{--                                        @endif--}}

{{--                                    </select>--}}
{{--                                </div>--}}

                                <div class="group-control col-md-2 col-sm-2">
                                    <button type="submit" style="padding: 8px;"
                                            class="btn btn-primary btn-block"> {{__('backend.filter')}} </button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <form method="post" id="myform" action="{{ url('/customer/account/print_police') }}"
                          target="_blank">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <table id="example23" class="display nowrap table table-hover table-striped table-bordered"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{{__('backend.name')}}</th>
                                <th>{{__('backend.mobile')}}</th>
                                <th>{{__('backend.created_at')}}</th>
                                <th class="text-right"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($customers as $customer)
                                <tr>
                                    <td>{{$customer->id}}</td>
                                    <td>{{$customer->name}}</td>
                                    <td>{{$customer->mobile}}</td>

                                    <td>{{ $customer->created_at }}</td>

                                    <td>

                                    {{--                @if(permission('showCustomer')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->--}}
                                    {{--                  <a class="btn btn-xs btn-primary" href="{{ route('mngrAdmin.customers.show', $customer->id) }}"><i class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>--}}
                                    {{--                @endif--}}

                                        <a class="btn btn-xs btn-warning"
                                           href="{{ route('profile.customers.edit', ['id' => $customer->id, 'locale' => ( app()->getLocale() == 'en' ? 'en' : 'ar')]) }}">
                                            <i class="icon-edit"></i> {{__('backend.edit')}}</a>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </form>
                    @if (! empty($customers) )
                        {!! $customers->appends($_GET)->links() !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

    <script>

        // Select All Orders function For Cancel
        function do_this() {
            var checkboxes = document.getElementsByName('select[]');
            var button = document.getElementById('toggle');

            if (button.value === '{{__('website.select_all')}}') {
                for (var i in checkboxes) {
                    checkboxes[i].checked = true;
                }
                button.value = '{{__('website.deselect_all')}}'
            } else {
                for (var i in checkboxes) {
                    checkboxes[i].checked = false;
                }
                button.value = '{{__('website.select_all')}}';
            }
        }

        $('.cancel-order').on('click', function () {
            $("#exampleModal").modal("hide");
            let selected_order_ids = [];
            let checkboxes = document.getElementsByName('select[]');
            var _token = $('#_token');
            var order_type = $(this).attr('data-value');
            console.log('orders type', order_type);

            for (let i in checkboxes) {
                if (checkboxes[i].checked === true) {
                    selected_order_ids.push(checkboxes[i].value);
                }
            }
            if (selected_order_ids.length > 0) {
                if (order_type === 'pending') {
                    $.ajax({
                        url: '{{ route('profile.cancel_orders') }}',
                        type: 'post',
                        data: {
                            _token: "{{ csrf_token() }}",
                            'orders_to_cancel': selected_order_ids,
                        },
                        success: function (data) {
                            location.reload();
                            var successAlert = '<div class="alert alert-success alert-block text-center">' +
                                '            <button type="button" class="close" data-dismiss="alert">×</button>' +
                                '            <strong>{{__('website.cancelled_orders_success')}}</strong></div>';
                            $('div.flash-message').html(successAlert);

                        },
                        error: function (data) {

                        }

                    });
                } else if (order_type === 'dropped') {
                    let reason = $('#reason').val();
                    console.log('drooppppep', reason);
                    $.ajax({
                        url: '{{ route('profile.cancel_orders_request') }}',
                        type: 'post',
                        data: {
                            _token: "{{ csrf_token() }}",
                            'orders_ids_to_cancel': selected_order_ids,
                            'reason': reason,
                        },
                        success: function (data) {
                            location.reload();
                            var successAlert = '<div class="alert alert-success alert-block text-center">' +
                                '            <button type="button" class="close" data-dismiss="alert">×</button>' +
                                '            <strong>{{__('website.cancelled_orders_success')}}</strong></div>';
                            $('div.flash-message').html(successAlert);

                        },
                        error: function (data) {
                            let warning = ' <button type="button" class="close" data-dismiss="alert">×</button>' +
                                '            <strong>{{__('website.')}}</strong>';
                            $('div.flash-message').html(warning);

                        }

                    });
                }


            } else {
                $.confirm({
                    title: '',
                    theme: 'modern',
                    content: '<div class="jconfirm-title-c jconfirm-hand">' +
                        '<span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span>' +
                        '<span class="jconfirm-title">{{__('website.cancel_order')}}</span></div>' +
                        '<div style="font-weight:bold;"><p>{{__("website.must_choose_order_to_cancel")}}</p></div>',
                    type: 'red',
                });
            }


        });


    </script>
@endsection

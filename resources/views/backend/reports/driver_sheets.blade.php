@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.drivers_excel')}}</title>
    <style>
        .form-check-inline {
            display: -webkit-inline-box;
            display: -ms-inline-flexbox;
            display: inline-flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            padding-left: 0;
            margin-right: .75rem;
        }

        .form-check-inline input {
            margin: 5px;
        }
    </style>
    <link href="{{asset('assets/select2/select2.min.css')}}" rel="stylesheet">
    <style>
        .select2-container {
            width: 100% !important;
            display: block;
        }
    </style>
@endsection
@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="fa fa-file-excel-o"></i> {{__('backend.drivers_excel')}}

        </h3>

    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <form action="{{URL::asset('/mngrAdmin/driver-sheets')}}" method="post" id="sort-form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group col-md-2 col-sm-6 col-xs-12">
                    <label for="date">{{__('backend.date')}}</label>
                    <select class="form-control select2" id="date" name="date">
                        <option value="created_at">
                            {{__('backend.Created_Date')}}
                        </option>
                        <option value="received_at">
                            {{__('backend.received_date')}}
                        </option>
                        <option value="delivered_at">
                            {{__('backend.Delivered_Date')}}
                        </option>
                        <option value="recalled_at">
                            {{__('backend.Recalled_Date')}}
                        </option>
                        <option value="rejected_at">
                            {{__('backend.Rejected_Date')}}
                        </option>
                    </select>
                </div>

                <div class="form-group col-md-2 col-sm-6 col-xs-12">
                    <label for="from_date-field">{{__('backend.From_Date')}}</label>
                    <div class='input-group date ' id="datepicker1">
                        <input type="text" id="from_date-field" name="from_date" class="form-control"/>
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-2 col-sm-6 col-xs-12">
                    <label for="to_date-field">{{__('backend.To_Date')}}</label>
                    <div class='input-group date ' id="datepickers">
                        <input type="text" id="to_date-field" name="to_date" class="form-control"/>
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                    </div>
                </div>

                <div class="form-group col-md-2 col-sm-6 col-xs-12">
                    <label for="driver_id">{{__('backend.driver')}}</label>
                    <select class="form-control select2" id="driver_id" name="driver_id[]" multiple="multiple">
                        @if(! empty($drivers))
                            @foreach($drivers as $driver)
                                <option value="{{$driver->id}}">
                                    {{$driver->name}}
                                </option>
                            @endforeach
                        @endif
                    </select>
                </div>

                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="user_id">{{__('backend.status')}}</label>
                    <div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="1"
                                   name="status[]">
                            <label class="form-check-label"
                                   for="inlineCheckbox3">{{__('backend.accepted')}}</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="2"
                                   name="status[]">
                            <label class="form-check-label"
                                   for="inlineCheckbox3">{{__('backend.received')}}</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="3"
                                   name="status[]">
                            <label class="form-check-label"
                                   for="inlineCheckbox3">{{__('backend.delivered')}}</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="5"
                                   name="status[]">
                            <label class="form-check-label"
                                   for="inlineCheckbox3">{{__('backend.recall')}}</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="8"
                                   name="status[]">
                            <label class="form-check-label"
                                   for="inlineCheckbox3">{{__('backend.reject')}}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group col-md-2 col-sm-6 col-xs-12 ">
                    <button id="download-field" type="submit" class="btn btn-primary btn-block"
                            style="margin-top: 24px;"> {{__('backend.download')}} </button>
                </div>
            </form>
        </div>

        <div class=" row col-md-12 text-center" id="table-view">

        </div>
    </div>



@endsection
@section('scripts')
    <script src="{{asset('assets/select2/select2.min.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.select2').select2();
        });
    </script>
@endsection

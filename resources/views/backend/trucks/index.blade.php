@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.vehicles')}}</title>
  <style type="text/css">
    @media only screen and (max-width: 580px) {
        .hide-td{
            display: none;
        }
    }
    .box-body {
       min-height: 100px;
    }
    .stylish-input-group .input-group-addon{
        background: white !important;
    }
    .stylish-input-group .form-control{
        border-right:0;
        box-shadow:0 0 0;
        border-color:#ccc;
    }
    .stylish-input-group button{
        border:0;
        background:transparent;
    }
  </style>
@endsection

@section('header')
    <div class="page-header clearfix">
      <h3>
        <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.vehicles')}}
        @if(permission('addVehicle')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
          <a class="btn btn-success pull-right" href="{{ route('mngrAdmin.trucks.create') }}"><i class="glyphicon glyphicon-plus"></i> {{__('backend.add_vehicle')}}</a>
        @endif
      </h3>
    </div>
@endsection

@section('content')
  <div class="row" style="margin-bottom:15px;">
    <div class="col-md-12">
      <div class="col-md-6 col-sm-6">
        <form action="{{URL::asset('/mngrAdmin/trucks')}}" method="get" id="search-form" />
         	<div id="imaginary_container">
            <div class="input-group stylish-input-group">
              <input type="text" class="form-control" id="search-field"  name="search" placeholder="{{__('backend.Search_by_Model_Name_Or_Plate_Number')}}" >
              <span class="input-group-addon">
                <button type="button" id="search-btn1">
                  <span class="glyphicon glyphicon-search"></span>
                </button>
              </span>
            </div>
          </div>
        </form>
      </div>
       <div class="group-control col-md-6 col-sm-3">
          <select id="type-field" name="type" class="form-control">
              <option value="-1">{{__('backend.sort_by_type')}}</option>
    	        @foreach($types as $type)
              	<option value="{{ $type->id }}">
                    @if(session()->has('lang') == 'ar')
                        {{ $type->name_ar }}
                    @else
                        {{ $type->name_en }}
                    @endif
                </option>
              @endforeach
          </select>
       </div>
  </div>
</div>
  <div class= "list">
	@include('backend.trucks.table')
	</div>
@endsection

@section('scripts')
<script>

// $('#theTable').DataTable({
//   "pagingType": "full_numbers"
// });

$("#type-field").on('change', function() {
	$('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
    $.ajax({
        url: '{{URL::asset("/mngrAdmin/trucks")}}'+'?type='+$("#type-field").val()+'&&size='+$("#size-field").val(),
        type: 'get',
        success: function(data) {
            $('.list').html(data.view);
            $('#theTable').DataTable({
              "pagingType": "full_numbers"
            });
        },
        error: function(data) {
            console.log('Error:', data);
        }
    });
});
$("#size-field").on('change', function() {
	$('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
    $.ajax({
        url: '{{URL::asset("/mngrAdmin/trucks")}}'+'?type='+$("#type-field").val()+'&&size='+$("#size-field").val(),
        type: 'get',
        success: function(data) {
            $('.list').html(data.view);
            $('#driversTable').DataTable({
              "pagingType": "full_numbers"
            });
        },
        error: function(data) {
            console.log('Error:', data);
        }
    });
});
$("#search-btn1").on('click', function() {
  // Wait icon
  	$('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
  	$.ajax({
        url: '{{URL::asset("/mngrAdmin/trucks")}}'+'?type='+$("#type-field").val()+'&&size='+$("#size-field").val()+'&&search='+$("#search-field").val(),
        type: 'get',
        data: $("#search-form").serialize(),
        success: function(data) {
            $('.list').html(data.view);
            $('#driversTable').DataTable({
              "pagingType": "full_numbers"
            });
        },
        error: function(data) {
            console.log('Error:', data);
        }
   	});
});
</script>
@endsection

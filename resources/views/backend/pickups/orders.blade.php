<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12" >
        @if(! empty($orders) && count($orders) > 0)
                <div class="table-responsive">
                    <table id="theTable" class="table table-condensed table-striped text-center table-bordered" style="min-height: 200px;">
                        <thead>
                        <tr>
                            <th>#</th>

{{--                            <th>{{__('backend.agent')}}</th>--}}

                            <th>{{__('backend.receiver_name')}}</th>
                            <th>{{__('backend.sender_name')}}</th>
{{--                            <th>{{__('backend.bonous')}}</th>--}}
                            <th>{{__('backend.from')}}</th>
                            <th>{{__('backend.to')}}</th>
                            <th style="font-weight: bold; color: #333;">{{__('backend.order_number')}}</th>
{{--                            <th style="font-weight: bold; color: #333;">{{__('backend.receiver_code')}}</th>--}}

                            <th></th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td>{{$order->id}}</td>

{{--                                <td>@if(! empty($order->agent) ) <a style="color:#f5b404;font-weight: bold" target="_blank" href="{{URL::asset('/mngrAdmin/agents/'.$order->agent_id)}}"> {{$order->agent->name}} </a> @endif</td>--}}

                                <td>{{$order->receiver_name}}</td>
                                <td>{{$order->sender_name}}</td>
{{--                                <td>{{$order->bonous}}</td>--}}


                                <td>
                                    @if(! empty($order->from_government)  && $order->from_government != null )
                                        <a style="color:#f5b404;font-weight: bold" target="_blank" href="{{URL::asset('/mngrAdmin/governorates/'.$order->s_government_id)}}" > {{$order->from_government->name_en}} </a>
                                    @endif
                                </td>

                                <td>
                                    @if(! empty($order->to_government)  && $order->from_government != null  ) <a style="color:#f5b404;font-weight: bold" target="_blank" href="{{URL::asset('/mngrAdmin/governorates/'.$order->r_government_id)}}" > {{$order->to_government->name_en}} </a>
                                    @endif
                                </td>
                                <td style="font-weight: bold; color: #a43;">{{$order->order_number}}</td>
{{--                                <td style="font-weight: bold; color: #a4e;">{{$order->receiver_code}}</td>--}}
                                <td>
                                    <a class="btn btn-xs btn-primary"  href="{{ route('mngrAdmin.orders.show', $order->id ) }}"><i class="fa fa-eye"></i> {{__('backend.view')}}</a>

                                    @if($pickup->status != 3)

{{--                                        <a class="btn btn-xs btn-warning editPickup " data-id="{{$order->id}}"--}}
{{--                                        data-bonous="{{$order->bonous}}"   ><i class="fa fa-edit"></i> {{__('backend.edit')}}</a>--}}

                                        <a class="btn btn-xs btn-danger delete-order-pickup" data-id="{{$order->id}}" data-pickup="{{$pickup->id}}"  id="delete-order-pickup"><i class="fa fa-trash"></i> {{__('backend.delete')}}</a>

                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>

            {!! $orders->appends($_GET)->links() !!}
        @else
            <h3 class="text-center alert alert-warning">No Result Found !</h3>
        @endif
    </div>
</div>



<div class="modal fade" id="editPickupModal" tabindex="-1" role="dialog" aria-labelledby="editPickupModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editPickupModalLabel">Update Order Bonous Price</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ url('/mngrAdmin/update_order_pickup') }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="pickup_id" value="{{$pickup->id}}">
        <input type="hidden" name="order_id" value="0" id="order_id_modal">

      <div class="modal-body" style="min-height: 120px">

            <div class="form-group col-md-12 col-sm-12 @if($errors->has('delivery_price')) has-error @endif">
                  <label for="delivery_price-field">Bonous Price: </label>
                  <input type="text" required class="form-control" id="delivery_price_modal" name="delivery_price" value="" required>
            </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>

  </form>
    </div>
  </div>
</div>

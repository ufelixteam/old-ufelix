@extends('backend.layouts.app')
@section('css')
    <title>@lang('backend.dashboard')</title>
@endsection
@section('content')
    <div class="panel panel-headline">
        <div class="panel-heading">
            <h3 class="panel-title"> Overview</h3>
            <p class="panel-subtitle"></p>
        </div>


        <div class="panel-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="metric">
                        <span class="icon"><i class="fa fa-users"></i></span>
                        <p>
                            <span class="number">{{ ! empty($users) ? $users : '0' }}</span>
                            <span class="title">Customers</span>
                        </p>
                    </div>
                </div>


                <div class="col-md-3">
                    <div class="metric">
                        <span class="icon"><i class="fa fa-handshake-o"></i></span>
                        <p>
                            <span class="number">{{ ! empty($corporates) ? $corporates : '0' }}</span>
                            <span class="title">Corporates</span>
                        </p>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="metric">
                        <span class="icon"><i class="lnr lnr-car"></i></span>
                        <p>
                            <span class="number">{{ ! empty($drivers) ? $drivers : '0' }}</span>
                            <span class="title">Captains</span>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="metric">
                        <span class="icon"><i class="fa fa-shopping-basket "></i></span>
                        <p>
                            <span class="number">{{ ! empty($orders) ? $orders : '0' }}</span>
                            <span class="title">Orders</span>
                        </p>
                    </div>
                </div>
                @if(permission('contacts'))
                    <div class="col-md-3">
                        <div class="metric">
                            <span class="icon"><i class="fa fa-envelope"></i></span>
                            <p>
                                <span class="number">{{ ! empty($contacts) ? $contacts : '0' }}</span>
                                <span class="title">Feedback</span>
                            </p>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-7">
            <!-- RECENT PURCHASES -->
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Recent Customers</h3>
                    <div class="right">
                        <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                        <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                    </div>
                </div>
                <div class="panel-body no-padding">
                    <table class="table table-striped">

                        <thead>
                        <tr>
                            <th width="10%">Photo</th>
                            <th>Name</th>
                            <th>Mobile</th>
                            <th>Date</th>
                            <th>Mobile Verified</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(! empty($recentCustomers) && count($recentCustomers)>0)

                            @foreach ($recentCustomers  as $user)
                                <tr>
                                    <td>
                                        @if(! empty($user->image))
                                            <img src="{{$user->image}}" width="100%">
                                        @else
                                            <img src="{{ URL::asset('/public/images/user.svg') }}" width="50%">
                                        @endif
                                    </td>
                                    <td>
                                        <a class="users-list-name"
                                           href="{{ URL::to('/mngrAdmin/customers') }}/{{ $user->id }}"> {{ $user->name }}</a>
                                    </td>
                                    <td>
                                        {{ $user->mobile }}
                                    </td>
                                    <td>
                                        <span
                                            class="users-list-date">{{ date('d-M', strtotime($user->created_at)) }}</span>
                                    </td>
                                    <td>
                                        {!! $user->mobile_verify_span !!}
                                    </td>

                                </tr>

                            @endforeach

                        @else
                            <tr>
                                <td colspan="4">
                                    <p style="padding: 8px 0 0 10px;">
                                        No Customer registered
                                    </p>
                                </td>
                            </tr>
                        @endif

                        </tbody>
                    </table>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-md-6"><span class="panel-note"><i
                                    class="fa fa-clock-o"></i> Last 24 hours</span></div>
                        <div class="col-md-6 text-right"><a href="{{url('/mngrAdmin/customers')}}"
                                                            class="btn btn-primary">View All Customers</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-5">
            <!-- TASKS -->
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Overall Orders</h3>
                    <div class="right">
                        <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                        <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                    </div>
                </div>
                <div class="panel-body">
                    <ul class="list-unstyled task-list">
                        @if($orders > 0)
                            <li>
                                <p>Cancelled Order <span class="label-percent">{{ $orderCancel }}</b>
                                        /{{ $orders }}</span></p>
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-danger" role="progressbar"
                                         aria-valuenow="{{ $orderCancel }}" aria-valuemin="0"
                                         aria-valuemax="{{$orders}}" style="width:{{ ($orderCancel/$orders) *100 }}%">
                                        <span class="sr-only">{{ $orderCancel }}</b>/{{ $orders }} Complete</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <p>Completed Orders <span class="label-percent">{{ $orderComplete }}</b>
                                        /{{ $orders }} </span></p>
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-success" role="progressbar"
                                         aria-valuenow="{{ $orderComplete }}" aria-valuemin="0"
                                         aria-valuemax="{{$orders}}"
                                         style="width: {{ ($orderComplete/$orders) *100 }}%">
                                        <span class="sr-only">{{ $orderComplete }}</b>/{{ $orders }} Complete</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <p>Pending Order <span class="label-percent">{{  $orderPending }}</b>
                                        /{{ $orders }}</span></p>
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-warning" role="progressbar"
                                         aria-valuenow="{{ $orderPending}}" aria-valuemin="0"
                                         aria-valuemax="{{$orders}}" style="width: {{ ($orderPending/$orders) *100 }}%">
                                        <span class="sr-only">{{ $orderPending}} </b>/{{ $orders }} Complete</span>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <p>Accepted <span class="label-percent">{{  $orderAccepted }}</b>/{{ $orders }}</span>
                                </p>
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-info" role="progressbar"
                                         aria-valuenow="{{  $orderAccepted }}" aria-valuemin="0"
                                         aria-valuemax="{{$orders}}"
                                         style="width: {{ ($orderAccepted/$orders) *100 }}%">
                                        <span class="sr-only">{{  $orderAccepted }}</b>/{{ $orders }}% Complete</span>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <p>O.F.D <span class="label-percent">{{  $orderRunning }}</b>/{{ $orders }}</span>
                                </p>
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-default" role="progressbar"
                                         aria-valuenow="{{  $orderRunning }}" aria-valuemin="0"
                                         aria-valuemax="{{$orders}}" style="width: {{ ($orderRunning/$orders) *100 }}%">
                                        <span class="sr-only">{{  $orderRunning }}</b>/{{ $orders }}% Complete</span>
                                    </div>
                                </div>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
            <!-- END TASKS -->
        </div>


        <div class="row">
            <div class="col-md-7">
                <!-- RECENT PURCHASES -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Recent Corporate</h3>
                        <div class="right">
                            <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i>
                            </button>
                            <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                        </div>
                    </div>
                    <div class="panel-body no-padding">
                        <table class="table table-striped">

                            <thead>
                            <tr>
                                <th>Photo</th>
                                <th>Name</th>
                                <th>Date &amp; Time</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(! empty($recentCorporates) && count($recentCorporates)>0)

                                @foreach ($recentCorporates  as $user)
                                    <tr>
                                        <td>
                                            @if(! empty($user->image))
                                                <img src="{{$user->image}}" width="50%">
                                            @else
                                                <img src="{{ URL::asset('/public/images/user.svg') }}" width="50%">
                                            @endif
                                        </td>
                                        <td>
                                            <a class="users-list-name"
                                               href="{{ URL::to('/admin/sellers') }}/{{ $user->id }}"> {{ $user->name }}</a>
                                        </td>
                                        <td>
                                            <span
                                                class="sellers-list-date">{{ date('d-M', strtotime($user->created_at)) }}</span>
                                        </td>
                                        <td>
                                            {!! $user->active_span !!}
                                        </td>

                                    </tr>

                                @endforeach

                            @else
                                <tr>
                                    <td colspan="4">
                                        <p style="padding: 8px 0 0 10px;">
                                            No Corporates registered
                                        </p>
                                    </td>
                                </tr>
                            @endif

                            </tbody>
                        </table>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-6"><span class="panel-note"><i
                                        class="fa fa-clock-o"></i> Last 24 hours</span></div>
                            <div class="col-md-6 text-right"><a href="{{url('/mngrAdmin/corporates')}}"
                                                                class="btn btn-primary">View All Corporates</a></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-5">
                <!-- TASKS -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">New Orders</h3>
                        <div class="right">
                            <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i>
                            </button>
                            <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Receiver</th>
                                    <th>Sender</th>
                                    <th>Created At</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(! empty($neworders) && count($neworders)>0)
                                    @foreach($neworders as $order)

                                        <tr>
                                            <td><a href="{{ URL::to('/mngrAdmin/orders') }}/{{ $order->id }}"
                                                   data-toggle="tooltip" data-placement="bottom"
                                                   title="Go to detail">{{ $order->id }}</a></td>
                                            <td>{{ $order->receiver_name }} </br> {{ $order->receiver_mobile }} </td>
                                            <td>{{ $order->sender_name }} </br> {{ $order->sender_mobile }} </td>

                                            <td> {{ $order->created_at }} </td>
                                            <td>
                                                {!! $order->status_span !!}
                                            </td>
                                        </tr>

                                    @endforeach

                                @else
                                    <tr>
                                        <td colspan="4" class="text-center">No Order Added</td>

                                    </tr>
                                @endif

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-6"><span class="panel-note"><i
                                        class="fa fa-clock-o"></i> Last 24 hours</span></div>
                            <div class="col-md-6 text-right"><a href="{{url('/mngrAdmin/orders??type=pending')}}"
                                                                class="btn btn-primary">View Pending Orders</a></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

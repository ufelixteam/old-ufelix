<form action="{{ route('mngrAdmin.orders.add_problem', @$order->id) }}" method="POST" id="problems_form">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="order_id" value="{{ @$order->id }}">
    <input type="hidden" class="latitude" name="latitude" value="0" id="receiver_latitude">
    <input type="hidden" class="longitude" name="longitude" value="0" id="receiver_longitude">
    <div class="modal" tabindex="-1" role="dialog" id="problemModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-header">
                        <h5 class="modal-title">{{__('backend.add_delivery_problem')}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="form-group col-md-12 col-xs-12 col-sm-12 select_driver">
                                <label for="order_price-field">{{__('backend.driver')}} *: </label>
                                <select class=" form-control driver_id" id="driver_id" name="driver_id" required>
                                    <option value="">{{__('backend.choose_driver')}}</option>
                                    @if(! empty($online_drivers))
                                        @foreach($online_drivers as $driver)
                                            <option
                                                value="{{$driver->id}}">
                                                {{$driver->name}}
                                            </option>
                                        @endforeach
                                    @endif

                                </select>
                            </div>
                            <div class="form-group col-md-12 col-xs-12 col-sm-12">
                                <label for="order_price-field">{{__('backend.reason')}} *: </label>
                                <select class=" form-control driver_id" id="delivery_problem_id"
                                        name="delivery_problem_id"
                                        required>
                                    <option value="">{{__('backend.choose_reason')}}</option>
                                    @if(! empty($problems))
                                        @foreach($problems as $problem)
                                            <option
                                                value="{{$problem->id}}">
                                                {{$problem->reason_en}}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-12 col-xs-12 col-sm-12">
                                <label class="delay_at">{{__('backend.date')}}
                                    <div class='input-group date' id='delay_date_group'>
                                        <input type="text" id="created_at" name="created_at" class="form-control"
                                               value=""
                                               autocomplete="off"/>
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </label>
                            </div>

                            <div class="form-group col-md-12 col-xs-12 col-sm-12" style="display: none"
                                 id="other_reason">
                                <label for="order_price-field">{{__('backend.other_reason')}}: </label>
                                <label for="order_price-field">{{__('backend.other_reason')}}: </label>
                                <textarea class="form-control" name="reason" id="reason" rows="3"
                                          style="min-height: 100px; resize: horizontal"></textarea>
                            </div>
                            @if(empty($order) || (!empty($order) && $order->status == 2))
                                <div class="form-group col-md-12 col-xs-12 col-sm-12" style="padding: 0 20px">
                                    <div class="col-md-12">
                                        <label class="radio"><input type="radio" name="problem_radio"
                                                                    class="problem_radio"
                                                                    value="delay"
                                                                    id="delay_problem"/>{{__('backend.delay')}}
                                        </label>
                                        <label class="hidden delay_at">{{__('backend.delay_at')}}
                                            {{--                                    <input type="date" class="form-control" name="delay_at"/>--}}
                                            <div class='input-group date' id='delay_date_group'>
                                                <input type="text" id="delay_at" name="delay_at" class="form-control"
                                                       value=""
                                                       autocomplete="off"/>
                                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </label>
                                        <label class="radio"><input type="radio" name="problem_radio"
                                                                    class="problem_radio"
                                                                    value="recall"
                                                                    id="recall_problem"/>{{__('backend.recall')}}
                                        </label>
                                    </div>

                                </div>
                            @endif
                            <div class="col-md-12 col-xs-12 col-sm-12" style="margin-bottom: 20px">
                                <label for="order_price-field">{{__('backend.location')}}: </label>
                                <input type="text" id="receiver_address_autocomplete" class="form-control"/>
                            </div>
                            <div class="col-md-12 col-xs-12 col-sm-12">
                                <div id="map1" style="width:100%;height:400px"></div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">

                        <button type="submit" class="btn btn-secondary" id="save_problems">
                            {{__('backend.save')}}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

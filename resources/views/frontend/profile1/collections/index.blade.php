@extends('frontend.profile.layouts.app')
@section('title') Collection Orders @endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="">
                <div class="table-responsive m-t-40">
                    <h1 class="title"> @lang('website.FormCollection')</h1>
                    <div class="button-box">
                        <a href="{{ app()->getLocale() == 'en'  ? url('/en/customer/account/order-form') :  url('/customer/account/order-form') }}"
                           class="btn btn-primary pull-right" style=""><i style="margin: 0 10px"
                                                                          class="mdi mdi-plus"></i>@lang('website.AddCollection')
                        </a>
                    </div>
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered"
                           cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>@lang('website.CreatedAt')</th>
                            <th> @lang('website.Saved')</th>
                            <th>@lang('website.Actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(! empty($collections) )
                            @foreach($collections as $collection)
                                <tr>
                                    <td>{{$collection->id}}</td>

                                    <td>{{$collection->created_at}}</td>
                                    <td>{!! $collection->saved_span !!}</td>
                                    <td>
                                        @if($collection->is_saved != 1)
                                            <a class="btn btn-xs @if($collection->complete_later == 1) btn-success @else btn-info @endif"
                                               href="{{ app()->getLocale() == 'en' ? url('/en/customer/account/order-form?collection='.$collection->id) : url('/customer/account/order-form?collection='.$collection->id) }}">
                                                @if($collection->complete_later == 1)
                                                    Complete
                                                @else
                                                    Edit
                                                @endif

                                            </a>
                                        @else
                                            <a class="btn btn-xs btn-primary "
                                               href="{{ app()->getLocale() == 'en' ? url('/en/customer/account/orders?collection='.$collection->id) : url('/customer/account/orders?collection='.$collection->id) }}">
                                                List Orders
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach

                        @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

@endsection

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoicePickup extends Model
{
    protected $guarded = ['id'];
}

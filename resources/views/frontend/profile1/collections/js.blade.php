<script>


    // if ($("#payment_method_id-field").val() == "1" || $("#payment_method_id-field").val() == "2" || $("#payment_method_id-field").val() == "7" || $("#payment_method_id-field").val() == "5") {
    //
    //     $("#order_price-field").val(0);
    //     $("#order_price-field").attr('readonly', 'readonly');
    // } else {
    //     $("#order_price-field").prop("readonly", false);
    // }
    //
    // /* */
    // $("#payment_method_id-field").on('change', function () {
    //     if ($(this).val() == "1" || $(this).val() == "2" || $(this).val() == "7" || $("#payment_method_id-field").val() == "5") {
    //
    //         $("#order_price-field").val(0);
    //         $("#order_price-field").attr('readonly', 'readonly');
    //     } else {
    //         $("#order_price-field").prop("readonly", false);
    //     }
    // });
    // /* */

    function roughScale(x, value = 0) {
        const parsed = parseInt(x);
        if (isNaN(parsed)) {
            return value;
        }
        return parsed;
    }

    function change_total_price() {
        delivery_fees = roughScale($("#delivery_price-field").val());
        order_price = roughScale($("#order_price-field").val());
        payment_method = $("#payment_method_id-field").val();
        if (payment_method == "1") {
            $("#total_price").text(order_price ? order_price : delivery_fees);
        } else if (payment_method == "2") {
            $("#total_price").text((delivery_fees + order_price));
        } else {
            $("#total_price").text(0);
        }
    }

    $("#payment_method_id-field").on('change', function () {
        change_total_price();
    });

    $("#order_price-field, #delivery_price-field").bind('keyup mouseup change', function () {
        change_total_price();
    });

    /* reset Form */
    $('#reset-btn').on('click', function () {
        $.ajax({
            url: "{{ url('/customer/account/order_form')}}?collection=" + $(this).attr('data-value'),
            type: 'get',
            success: function (data) {
                if (data == "false") {
                    alert(' Something error')
                } else {
                    $('#create-form').html(data.view);

                }
            },
            error: function (data) {

                console.log('Error:', data);
            }
        });

    });
    /*view-order*/
    @if(!isset($from_update))
    $(".list").on('click', '.view-order', function () {
        $.ajax({
            url: "{{ url('/customer/account/tmporders_update_view')}}/" + $(this).attr('data-value'),
            type: 'get',
            success: function (data) {

                $('#create-form').html(data.view);
            },
            error: function (data) {

                console.log('Error:', data);
            }
        });

    });
    @endif

    $("#complete_later").on('click', function () {
        if ($("#collection_id").val()) {
            /*send to temp*/
            $.ajax({
                url: "{{ url('/customer/account/complete_collection')}}" + "/" + $("#collection_id").val(),
                type: 'post',
                data: {'_token': "{{csrf_token()}}"},
                success: function (data) {
                    window.location.href = "{{ url('/'.app()->getLocale().'/customer/account/form-collections')}}";
                },
                error: function (data) {

                    console.log('Error:', data);
                }
            });
        }
    });

    $("#order-btn").on('click', function () {

        /*send to temp*/
        $.ajax({
            url: "{{ url('/'.app()->getLocale().'/customer/account/tmporders_create')}}",
            type: 'post',
            data: $("#order-form").serialize(),

            success: function (data) {
                if (data.errors) {
                    jQuery('.alert-danger').html('');
                    jQuery.each(data.errors, function (key, value) {
                        jQuery('.alert-danger').show();

                        jQuery('.alert-danger').append('<p>' + value + '</p>');
                    });
                    window.scrollTo({top: 0, behavior: 'smooth'});
                } else {
                    jQuery('.alert-danger').hide();
                    $('#collection_id').val(data.collection_id);

                    jQuery('.alert-success').show();
                    if (data.first_one == "1") {
                        /*create-btn*/
                        window.history.replaceState(null, null, "/customer/account/order-form?collection=" + data.collection_id);
                        jQuery('.alert-success').append('<p>Order Added successfully</p>');
                        $("#complete_later").removeClass('hidden');
                    } else {
                        jQuery('.alert-success').append('<p>Order Updated successfully</p>');
                    }

                    $('.list').html(data.view);

                    /* change collection id*/

                    $('.create-form').html(data.view_order);
                    $('.js-form').html(data.js);

                    $("#order-form")[0].reset();


                    window.scrollTo({top: 0, behavior: 'smooth'});

                    window.setTimeout(function () {
                        $(".alert-success").fadeTo(500, 0).slideUp(500, function () {
                            $(this).remove();
                        });
                    }, 4000);
                }

            },
            error: function (data) {

                console.log('Error:', data);
            }
        });

    });


    $(".r_government_id").on('change', function () {
        $("#no_fees").addClass('hidden');
        $('.r_state_id').html('');

        if ($(this).val()) {
            $.ajax({
                url: "{{ url('/get_cities')}}" + "/" + $(this).val(),
                type: 'get',
                data: {},
                success: function (data) {
                    $('.r_state_id').html('');
                    $.each(data, function (i, content) {
                        $('.r_state_id').append($("<option></option>").attr("value", content.id).text(content.city_name));
                    });
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }

        var sender = "{{Auth::guard('Customer')->user()->government_id}}"; //$(".s_government_id").val();
        if (sender && $(this).val()) {

            $.ajax({
                url: "{{ url('/customer/account/get_cost')}}" + "/" + sender + "/" + $(this).val(),
                type: 'get',
                data: {},
                success: function (data) {
                    if (data == 0 || data == 'not_exist') {
                        $("#delivery_price-field").css("border-color", "red");
                        if (data == 'not_exist') {
                            $("#no_fees").removeClass('hidden');
                            $(".r_government_id").val('');
                        }
                    } else {
                        $("#delivery_price-field").css("border-color", "#d2d6de");
                    }
                    $('#delivery_price-field').val((data == 'not_exist' ? 0 : data)).change();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }

    });


    $(".s_government_id").on('change', function () {
        $("#no_fees").addClass('hidden');
        $('.s_state_id').html('');

        if ($(this).val()) {

            $.ajax({
                url: "{{ url('/get_cities')}}" + "/" + $(this).val(),
                type: 'get',
                data: {},
                success: function (data) {
                    $('.s_state_id').html('');
                    $.each(data, function (i, content) {
                        $('.s_state_id').append($("<option></option>").attr("value", content.id).text(content.city_name));
                        @if(Auth::guard('Customer')->user())
                        $('.s_state_id').val("{{Auth::guard('Customer')->user()->city_id}}");
                        @endif
                    });
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }

        var receiver = $(".r_government_id").val();

        if (receiver && $(this).val()) {

            $.ajax({
                url: "{{ url('/customer/account/get_cost')}}" + "/" + $(this).val() + "/" + receiver,
                type: 'get',
                data: {},
                success: function (data) {
                    if (data == 0 || data == 'not_exist') {
                        $("#delivery_price-field").css("border-color", "red");
                        if (data == 'not_exist') {
                            $("#no_fees").removeClass('hidden');
                            $(".r_government_id").val('');
                        }
                    } else {
                        $("#delivery_price-field").css("border-color", "#d2d6de");
                    }
                    $('#delivery_price-field').val((data == 'not_exist' ? 0 : data)).change();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }

    });

    $("#store_id-field").on('change', function () {
        $('#stock_id-field').html('<option value="" >Choose Stock</option>');
        $("#qty-field").val(0);
        $("#qty-field").attr('readonly', true);
        get_stocks();
    });

    function get_stocks() {
        $.ajax({
            url: "{{ url('/mngrAdmin/get_stock_by_corporate/')}}" + "/{{@$customer->corporate_id}}" + "?main_store=" + $("#store_id-field").val(),
            type: 'get',
            data: {},
            success: function (data) {
                if (data.length > 0) {
                    $.each(data, function (i, content) {
                        $('#stock_id-field').append($("<option></option>").attr("value", content.id).text(content.title));
                    });
                }
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    }

    @if(Auth::guard('Customer')->user())
    $('#s_government_id').val("{{Auth::guard('Customer')->user()->government_id}}").change();
    @endif

    $("#qty-field").bind('keyup mouseup', function () {
        check_stock_quantity();
    });

    $("#stock_id-field").on('change', function () {
        check_stock_quantity();
    });

    function check_stock_quantity() {
        if ($("#stock_id-field").val() != '') {
            $("#qty-field").attr('readonly', false);
            $.ajax({
                url: "{{ url('/mngrAdmin/check_stock_capacity/')}}" + "/" + $("#stock_id-field").val(),
                type: 'get',
                data: {},
                success: function (data) {
                    $("#available_quantity").text(data);
                    if (parseInt($("#qty-field").val()) > data) {
                        $("#qty-field").val(data);
                    }
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        } else {
            $("#available_quantity").text(0);
            $("#qty-field").val(0);
            $("#qty-field").attr('readonly', true);
        }
    }
</script>


<?php $__env->startSection('css'); ?>
<title><?php echo e(__('backend.stores')); ?> - <?php echo e(__('backend.edit')); ?></title>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('header'); ?>
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i>  <?php echo e(__('backend.edit_store')); ?> - <?php echo e($store->name); ?></h3>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-md-12">

            <form action="<?php echo e(route('mngrAdmin.stores.update', $store->id)); ?>" method="POST" name="validateForm">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

                <div class="row">
                  <div class="form-group col-md-6  <?php if($errors->has('name')): ?> has-error <?php endif; ?>">
                    <label for="name-field"><?php echo e(__('backend.name')); ?></label>
                    <input type="text" id="name-field" name="name" class="form-control" value="<?php echo e(is_null(old("name")) ? $store->name : old("name")); ?>"/>
                     <?php if($errors->has("name")): ?>
                          <span class="help-block"><?php echo e($errors->first("name")); ?></span>
                     <?php endif; ?>
                   </div>

                    <div class="form-group col-md-6  <?php if($errors->has('mobile')): ?> has-error <?php endif; ?>">
                    <label for="mobile-field"><?php echo e(__('backend.mobile')); ?></label>
                    <input type="text" id="mobile-field" name="mobile" class="form-control" value="<?php echo e(is_null(old("mobile")) ? $store->mobile : old("mobile")); ?>"/>
                     <?php if($errors->has("mobile")): ?>
                          <span class="help-block"><?php echo e($errors->first("mobile")); ?></span>
                     <?php endif; ?>
                   </div>
                   <div class="form-group col-md-3 <?php if($errors->has('governorate_id')): ?> has-error <?php endif; ?>">
                      <label for="governorate_id-field"><?php echo e(__('backend.government')); ?></label>
                      <select  class="form-control" id="governorate_id"  name="governorate_id">
                          <option value="" data-display="Select"> <?php echo e(__('backend.government')); ?></option>
                          <?php if(! empty($governorates)): ?>
                              <?php $__currentLoopData = $governorates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $government): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <option value="<?php echo e($government->id); ?>" <?php echo e($government->id == $store->governorate_id ? 'selected' : ''); ?> >
                                    <?php if(session()->has('lang') == 'en'): ?>
                                      <?php echo e($government->name_ar); ?>

                                    <?php else: ?>
                                      <?php echo e($government->name_en); ?>

                                    <?php endif; ?>
                                  </option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          <?php endif; ?>
                      </select>
                     <?php if($errors->has("governorate_id")): ?>
                      <span class="help-block"><?php echo e($errors->first("governorate_id")); ?></span>
                     <?php endif; ?>
                  </div>

                   <div class="form-group col-md-3 col-sm-3">
                        <label class="control-label" for="state_id"> <?php echo e(__('backend.city')); ?>:</label>

                        <select  class=" form-control " id="state_id"  name="state_id" >
                            <option value="Null" ><?php echo e(__('backend.city')); ?></option>
                            <?php if(! empty($cities)): ?>
                              <?php $__currentLoopData = $cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $state): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <option value="<?php echo e($state->id); ?>" <?php echo e($state->id == $store->state_id ? 'selected' : ''); ?> >
                                    <?php if(session()->has('lang') == 'en'): ?>
                                      <?php echo e($state->city_name); ?>

                                    <?php else: ?>
                                      <?php echo e($state->name_en); ?>

                                    <?php endif; ?>
                                  </option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          <?php endif; ?>

                        </select>

                    </div>
                     <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('pickup_default')): ?> has-error <?php endif; ?>">
                      <label for="pickup_default-field"><?php echo e(__('backend.pickup_default')); ?></label>
                      <select id="pickup_default-field" name="pickup_default" class="form-control" value="<?php echo e(old("pickup_default")); ?>">
                        <option value="0" <?php echo e($store->pickup_default == "0" ? 'selected' : ''); ?>><?php echo e(__('backend.no')); ?></option>
                        <option value="1" <?php echo e($store->pickup_default == "1" ? 'selected' : ''); ?>><?php echo e(__('backend.yes')); ?></option>
                      </select>
                      <?php if($errors->has("pickup_default")): ?>
                        <span class="help-block"><?php echo e($errors->first("pickup_default")); ?></span>
                      <?php endif; ?>
                  </div>
                  <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('status')): ?> has-error <?php endif; ?>">
                      <label for="status-field"><?php echo e(__('backend.status')); ?></label>
                      <select id="status-field" name="status" class="form-control" value="<?php echo e(old("status")); ?>">
                        <option value="0" <?php echo e($store->status == "0" ? 'selected' : ''); ?>><?php echo e(__('backend.not_publish')); ?></option>
                        <option value="1" <?php echo e($store->status == "1" ? 'selected' : ''); ?>><?php echo e(__('backend.publish')); ?></option>
                      </select>
                      <?php if($errors->has("status")): ?>
                        <span class="help-block"><?php echo e($errors->first("status")); ?></span>
                      <?php endif; ?>
                  </div>
                </div>
                <div class="row">
                   <div class="form-group col-md-6 <?php if($errors->has('latitude')): ?> has-error <?php endif; ?>">
                       <label for="latitude"><?php echo e(__('backend.latitude')); ?></label>
                       <input type="text" id="latitude" name="latitude" class="form-control" value="<?php echo e(is_null(old("latitude")) ? $store->latitude : old("latitude")); ?>"/>
                       <?php if($errors->has("latitude")): ?>
                        <span class="help-block"><?php echo e($errors->first("latitude")); ?></span>
                       <?php endif; ?>
                    </div>
                    <div class="form-group col-md-6 <?php if($errors->has('longitude')): ?> has-error <?php endif; ?>">
                        <label for="longitude"><?php echo e(__('backend.longitude')); ?></label>
                        <input type="text" id="longitude" name="longitude" class="form-control" value="<?php echo e(is_null(old("longitude")) ? $store->longitude : old("longitude")); ?>"/>
                        <?php if($errors->has("longitude")): ?>
                          <span class="help-block"><?php echo e($errors->first("longitude")); ?></span>
                        <?php endif; ?>
                    </div>

                   
                  </div>
                  <div class="row">
                    <div class="form-group col-md-12 col-sm-12 <?php if($errors->has('address')): ?> has-error <?php endif; ?>">
                      <label for="address"><?php echo e(__('backend.address')); ?>: </label>
                      <textarea type="text" id="address" name="address" rows="4" class="form-control"><?php echo e(is_null(old("address")) ? $store->address : old("address")); ?></textarea>
                      <?php if($errors->has("address")): ?>
                        <span class="help-block"><?php echo e($errors->first("address")); ?></span>
                      <?php endif; ?>
                    </div>
                  </div>

                  <?php echo $__env->make('backend.map', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                  <div class="well well-sm col-md-12">
                    <button type="submit" class="btn btn-primary"><?php echo e(__('backend.save_edits')); ?></button>
                    <a class="btn btn-link pull-right" href="<?php echo e(route('mngrAdmin.stores.index')); ?>"><i class="glyphicon glyphicon-backward"></i>  <?php echo e(__('backend.back')); ?></a>
                </div>
            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script type="text/javascript" src="<?php echo e(URL::asset('/public/backend/js/map.js')); ?>"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQiN4qc7pC9b1BRQwkqXXD28peMfWcHvw&&callback=myMap"></script>

<script type="text/javascript">
  $(function() {
    $("form[name='validateForm']").validate({
      // Specify validation rules
      rules: {
        name: {
          required: true,
        },
        governorate_id: {
          required: true,
        },
        status: {
          required: true,
        },
        latitude: {
          number: true,
        },
        longitude: {
          number: true,
        },
        address: {
          required: true,
        },
      },
      // Specify validation error messages
      messages: {
        name: {
          required: "<?php echo e(__('backend.Please_Enter_Store_Name')); ?>",
        },
        governorate_id: {
          required: "<?php echo e(__('backend.Please_Chosse_The_Government')); ?>",
        },
        status: {
          required: "<?php echo e(__('backend.Please_Chosse_The_Status')); ?>",
        },
        latitude: {
          number: "<?php echo e(__('backend.Please_Enter_Avalid_Number')); ?>",
        },
        longitude: {
          number: "<?php echo e(__('backend.Please_Enter_Avalid_Number')); ?>",
        },
        address: {
          required: "<?php echo e(__('backend.Please_Enter_The_Address')); ?>",
        },
      },

      errorPlacement: function(error, element) {
          $(element).parents('.form-group').append(error)
      },
      highlight: function (element) {
          $(element).parent().addClass('error')
      },
      unhighlight: function (element) {
          $(element).parent().removeClass('error')
      },
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function(form) {
          form.submit();
      }
    });
  })

  $("#governorate_id").on('change',function(){
      $.ajax({
        url: "<?php echo e(url('/mngrAdmin/get_cities')); ?>"+"/"+$(this).val(),
        type: 'get',
        data: {},
        success: function(data) {
          $('#state_id').html('<option value="" >Sender City</option>');
          $.each(data, function(i, content) {
            $('#state_id').append($("<option></option>").attr("value",content.id).text(content.city_name));
          });
        },
        error: function(data) {
          console.log('Error:', data);
        }
      });

    });
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/stores/edit.blade.php ENDPATH**/ ?>
<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Ufelix</title>
    <style type="text/css" media="all">
        .polica {
            --padding: 20px;
            text-transform: capitalize;
            --margin: 30px;
        }

        .head {
            border-bottom: 4px solid #343a40;
        }

        .head h4 {
            font-weight: bold;

        }

        .head button {
            border: none;
            box-shadow: none;
            outline: none;
            padding: .3rem 0.75rem !important;
        }

        .btn.focus, .btn:focus {
            border: none !important;
            box-shadow: none !important;
            outline: none !important;
        }

        #invoice-body {
            padding: 20px 10px 1px 10px;
            max-width: 100%;
            box-shadow: 1px 1px 12px #343a40;
            border-radius: 5px;
        }

        .logo-ufelix .img-thumbnail {
            border: none;
        }

        .logo-ufelix .line {
            margin-top: 0px;
            margin-bottom: .1rem;
            border-top: 2px solid #e7e9ea;
            width: 95%;
        }

        .data {
            background-color: #f80;
            height: 40px;
            color: #fff;
            line-height: 40px;
            font-weight: bold;

        }

        .order_number {
            margin-left: 14px;
            margin-top: 10px;
            margin-bottom: 40px;
        }

        .order_number ul li {
            margin-bottom: 5px;
        }

        .customer-info {
            border: 1px solid #f80;
            margin: 10px;
            width: 97%;
        }

        .customer-info > div {
            border: 2px solid #f80;
            padding: 15px;

        }

        #table td, #table th {
            font-size: 12px;
            border-top: 1px solid #ff8800;
            text-align: center;
        }

        /*bootstrab class*/
        *,
        *::before,
        *::after {
            box-sizing: border-box;
        }

        html {
            font-family: sans-serif;
            line-height: 1.15;
            -webkit-text-size-adjust: 100%;
            -ms-overflow-style: scrollbar;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
        }


        article, aside, figcaption, figure, footer, header, hgroup, main, nav, section {
            display: block;
        }

        body {
            margin: 0;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, Noto Sans, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
            font-size: 12px;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            text-align: left;
            background-color: #fff;
        }

        .list-inline {
            padding-left: 0;
            list-style: none;
        }

        .row {
            margin-right: -15px;
            margin-left: -15px;
        }

        .row-no-gutters {
            margin-right: 0;
            margin-left: 0;
        }

        .row-no-gutters [class*="col-"] {
            padding-right: 0;
            padding-left: 0;
        }

        .col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col,
        .col-auto, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm,
        .col-sm-auto, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .col-md,
        .col-md-auto, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg,
        .col-lg-auto, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl,
        .col-xl-auto {
            position: relative;
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
        }

        .img-thumbnail {
            /* padding: 0.25rem; */
            background-color: #fff;
            border: 1px solid #dee2e6;
            border-radius: 0.25rem;
            max-width: 65%;
            /* height: auto; */
            height: 85px;
            margin: 12px 0 0 38px;
        }

        .d-inline {
            display: inline !important;
        }

        h1, h2, h3, h4, h5, h6 {
            margin-top: 0;
            margin-bottom: 0.5rem;
        }

        h1, h2, h3, h4, h5, h6,
        .h1, .h2, .h3, .h4, .h5, .h6 {
            margin-bottom: 0.5rem;
            font-family: inherit;
            font-weight: 500;
            line-height: 1.3;
            color: inherit;
        }

        h1, .h1 {
            font-size: 2.5rem;
        }

        h2, .h2 {
            font-size: 2rem;
        }

        h3, .h3 {
            font-size: 1.75rem;
        }

        h4, .h4 {
            font-size: 1.2rem;
        }

        h5, .h5 {
            font-size: 1.25rem;
        }

        h6, .h6 {
            font-size: 1rem;
        }

        .text-right {
            text-align: right !important;
        }

        .text-center {
            text-align: center !important;
        }

        .table {
            border-collapse: collapse !important;
        }

        .table td,
        .table th {
            background-color: #fff !important;
            padding: .75rem;
        }

        .table-bordered thead td, .table-bordered thead th {
            border-bottom-width: 1px;
        }

        .btn-primary {
            color: #fff;
            background-color: #007bff;
            border-color: #007bff;
        }

        .btn-secondary:focus, .btn-secondary.focus {
            box-shadow: 0 0 0 0.2rem rgba(130, 138, 145, 0.5);
        }

        .btn-secondary.disabled, .btn-secondary:disabled {
            color: #fff;
            background-color: #6c757d;
            border-color: #6c757d;
        }

        .btn-secondary {
            color: #fff;
            background-color: #6c757d;
            border-color: #6c757d;
        }

        .table thead th {
            vertical-align: bottom;
            border-bottom: 1px solid #1c1b1b;
        }

        .table-bordered {
            border: 1px solid #1c1b1b;
        }

        .table-bordered th {
            border: 1px solid #1c1b1b;
        }

        .table-bordered td {
            border: 1px solid #cdbfbf;
        }

        .table-bordered .firstRow td {
            border-top: 1px solid #1c1b1b;
        }

        .table-bordered .thirdRow td {
            border-bottom: 1px solid #1c1b1b;
        }

        .table-bordered td.right-border {
            border-right: 1px solid #1c1b1b !important;
        }

        .table-bordered td.right-bottom-border {
            border-right: 1px solid #1c1b1b !important;
            border-bottom: 1px solid #1c1b1b !important;
        }

        .table-bordered td.left-border {
            border-left: 1px solid #1c1b1b !important;
            border-top: 1px solid #1c1b1b !important;
            border-bottom: 1px solid #1c1b1b !important;
        }

        .d-inline {
            display: inline !important;
        }

        .col-sm-1 {
            -ms-flex: 0 0 8.333333%;
            flex: 0 0 8.333333%;
            max-width: 8.333333%;
        }

        .col-sm-2 {
            -ms-flex: 0 0 16.666667%;
            flex: 0 0 16.666667%;
            max-width: 16.666667%;
        }

        .col-sm-3 {
            -ms-flex: 0 0 25%;
            flex: 0 0 25%;
            max-width: 25%;
        }

        .col-sm-4 {
            -ms-flex: 0 0 33.333333%;
            flex: 0 0 33.333333%;
            max-width: 33.333333%;
        }

        .col-sm-5 {
            -ms-flex: 0 0 41.666667%;
            flex: 0 0 41.666667%;
            max-width: 41.666667%;
        }

        .col-sm-6 {
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }

        .col-sm-7 {
            -ms-flex: 0 0 58.333333%;
            flex: 0 0 58.333333%;
            max-width: 58.333333%;
        }

        .col-sm-8 {
            -ms-flex: 0 0 66.666667%;
            flex: 0 0 66.666667%;
            max-width: 66.666667%;
        }

        .col-sm-9 {
            -ms-flex: 0 0 75%;
            flex: 0 0 75%;
            max-width: 75%;
        }

        .col-sm-10 {
            -ms-flex: 0 0 83.333333%;
            flex: 0 0 83.333333%;
            max-width: 83.333333%;
        }

        .col-sm-11 {
            -ms-flex: 0 0 91.666667%;
            flex: 0 0 91.666667%;
            max-width: 91.666667%;
        }

        .col-sm-12 {
            -ms-flex: 0 0 100%;
            flex: 0 0 100%;
            max-width: 100%;
        }

        .container {
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }

        .row {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            /* margin-right: -15px;
            margin-left: -15px; */
            margin: 0 -15px 5px;
        }

        .borderless td, .borderless th {
            border: none;
        }

    </style>
</head>
<body>
<div class="container"
     style="width:100%;padding:10px;">

    <div class="invoice-title">

        <div style="width: 100%">
            <div style="width: 10% ;display: inline-block; float: left">
                <img class="img-thumbnail" src="{{public_path().'/website/images/logo.png'}}" alt=""
                     style="height:75px; padding: 5px; margin: 0; /*padding-top: 70px*/">
            </div>

            <div style="width: 67% ; display: inline-block; float: left ; text-align: center">
                <h3>The Delegate's Route Report</h3>
            </div>
            <div style="width: 20% ; display: inline-block; float: right">
                <table class="table table-bordered" style="width: 100%">
                    @if(! empty($orders[0]->accepted ) && ! empty($orders[0]->accepted->driver))
                        <tr>
                            <td>Captain name</td>
                            <td>
                                {{$orders[0]->accepted->driver->name}}
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <td>Number of shipments</td>
                        <td>
                            {{count($orders)}}
                        </td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td>
                            {{date("d/m/Y")}}
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <table class="table table-bordered" style="margin: 0; padding: 0; width: 100%; border: none; margin-top: 5px">
            <thead>
            <tr>
                <th>No</th>
                <th>
                    Corporate/Phone
                </th>
                <th>
                    Receiver/Phone
                </th>
                <th>
                    Address/Notes/Type
                </th>
                <th colspan="4">
                    Barcode/Total
                </th>
            </tr>
            </thead>
            <tbody>
            @php
                $i=1;
                $total=0;
            @endphp
            @if(! empty($orders))
                @foreach($orders as $order)
                    <tr class="firstRow">
                        <td class="left-border" style="text-align: center; font-weight: bold" rowspan="3">{{$i}}</td>
                        <td style="text-align: center; font-weight: bold">
                            {{isset($order->customer->corporate->name) ? $order->customer->corporate->name : '-'}}
                        </td>
                        <td style="text-align: center; font-weight: bold">
                            {{$order->receiver_name}}
                        </td>
                        <td style="text-align: center; font-weight: bold">
                            {{$order->receiver_address ? $order->receiver_address : '-'}}
                        </td>
                        {{--                        <td style="text-align: center; font-weight: bold" rowspan="2">--}}
                        {{--                            {{$order->total_price_driver}}--}}
                        {{--                        </td>--}}
                        {{--                        <td style="text-align: center; font-weight: bold" rowspan="2">--}}
                        {{--                            {{$order->type ? $order->type :'-'}}--}}
                        {{--                        </td>--}}
                        <td style="text-align: center; font-weight: bold" rowspan="2" colspan="4">
                            <img src="data:image/png;base64,{!! DNS1D::getBarcodePNG($order->order_number, 'I25') !!}"
                                 alt="barcode"/>
                            <br>
                            {{$order->order_number}}
                            <br>
                            {{$order->reference_number}}
                        </td>
                    </tr>
                    <tr class="secondRow">
                        <td style="text-align: center; font-weight: bold">
                            {{$order->sender_mobile ? $order->sender_mobile : '-'}}
                        </td>
                        <td style="text-align: center; font-weight: bold">
                            {{$order->receiver_mobile ? $order->receiver_mobile : '-'}}
                        </td>
                        <td style="text-align: center; font-weight: bold">
                            {{$order->notes ? $order->notes : '-'}}
                        </td>
                    </tr>
                    <tr class="thirdRow">
                        <td style="text-align: center; font-weight: bold">
                            {{$order->sender_subphone ? $order->sender_subphone : '-'}}
                        </td>
                        <td style="text-align: center; font-weight: bold">
                            {{$order->receiver_phone ? $order->receiver_phone : '-'}}
                        </td>
                        <td style="text-align: center; font-weight: bold">
                            {{$order->type ? $order->type : '-'}}
                        </td>
                        <td style="border-right: none;font-weight: bold">
                            Total
                        </td>
                        <td style="border-left: none">

                        </td>
                        <td style="border-right: none">

                        </td>
                        <td class="right-border" style="border-left: none;font-weight: bold">
                            {{$order->total_price_driver}}
                        </td>
                    </tr>
                    @php
                        $i++;
                        $total+=$order->total_price_driver;
                    @endphp
                @endforeach
                <tr>
                    {{--                    <td style="border: none"></td>--}}
                    {{--                    <td style="border: none"></td>--}}
                    {{--                    <td style="border: none"></td>--}}
                    <td style="border: none"></td>
                    <td style="border: none"></td>
                    <th>Total</th>
                    <td class="right-bottom-border" style="text-align: center; font-weight: bold">
                        {{$total}}
                    </td>
                    {{--                    <td style="border: none"></td>--}}
                    {{--                    <td style="border: none"></td>--}}
                </tr>
            @endif
            </tbody>
        </table>

        <br><br>

        <div style="width: 100%; ">
            <div style="width: 10% ;display: inline-block; float: left">
                Comment:
            </div>

            <div style="width: 55% ; display: inline-block; float: left ">
                <hr class="line" style="width: 100%;">
            </div>

            <div style="width: 35% ; display: inline-block; float: right; text-align: center">
                Signature:
            </div>
        </div>
    </div>
</div>
</body>
</html>

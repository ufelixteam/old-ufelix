<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
      @if($collection_orders->count())
        <table class="table table-condensed table-striped text-center">
          <thead>
            <tr>
              <th>#</th>
              <th>{{__('backend.customer')}}</th>
        		  <th>{{__('backend.Create_at')}}</th>
              <th>{{__('backend.File_Name')}}</th>

              @if(! empty($type) && $type ==1 )
              <th>{{__('backend.saved')}}</th>
              <th>{{__('backend.Saved_At')}}</th>
              @else

              <th>{{__('backend.file')}}</th>
              @endif

               <th></th>
            </tr>
          </thead>
          <tbody>

            @foreach($collection_orders as $collection)
              <tr>
                  <td>{{$collection->id}}</td>
                  <td>{{! empty($collection->customer) ? $collection->customer->name : ''}}</td>
				          <td>{{$collection->created_at}}</td>
                  <td>{{$collection->file_name}}</td>

                  @if(! empty($type) && $type ==1 )
                    <td>{!! $collection->saved_span !!}</td>
                    <td>{{$collection->saved_at}}</td>

                  @endif

                   @if(! empty($type) && $type == 1  )
                   <td>
                      @if($collection->is_saved != 1)
                      <a class="btn btn-xs btn-default " href="{{ URL::asset('mngrAdmin/orders/create?collection='.$collection->id) }}">
                        {{__('backend.edit')}}
                      </a>

                      <form action="{{ URL::asset('/mngrAdmin/collection_orders/'. $collection->id.'?type=1') }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
                          <input type="hidden" name="_method" value="DELETE">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}</button>
                      </form>

                      @else
                      <a class="btn btn-xs btn-primary "  href="{{ URL::asset('mngrAdmin/orders?collection='.$collection->id) }}">
                        {{__('backend.Orders_List')}}
                      </a>
                      @endif
                    </td>

                   @else
          		        <td >
                      <a href="{{ URL::asset('public/api_uploades/client/corporate/sheets/'.$collection->file_name) }}">
                        @if($collection->status == 0)
                        <form class="" action="{{ route('mngrAdmin.collection_orders.downloadStatus') }}" method="post">
                          <input id="_token" type="hidden" name="_token" value="{{ csrf_token() }}">
                          <input id="id" type="hidden" name="id" value="{{$collection->id}}">
                          <input id="status" type="hidden" name="status" value="1">
                          <input id="file_name" type="hidden" name="file_name" value="{{$collection->file_name}}">
                          <button id="downloadStatus" type="submit" class="btn btn-primary" name="button">{{__('backend.Download_File')}}</button>
                        </form>
                        @else
                        <button type="submit" class="btn btn-warning" name="button">{{__('backend.Download_File')}}</button>
                        @endif
                      </a>
                    </td>


                    <td class="text-right pull-right">
          			      <a class="btn btn-xs btn-warning" href="{{ route('mngrAdmin.collection_orders.edit', $collection->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                      <form action="{{ URL::asset('/mngrAdmin/collection_orders/'. $collection->id.'?type=0') }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                          <input type="hidden" name="_method" value="DELETE">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}</button>
                      </form>
                    </td>

                 @endif
              </tr>
            @endforeach
          </tbody>
        </table>
        {!! $collection_orders->appends($_GET)->links() !!}
      @else
          <h3 class="text-center alert alert-warning">No Result Found !</h3>
      @endif
    </div>
  </div>

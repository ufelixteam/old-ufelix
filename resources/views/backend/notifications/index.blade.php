@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.notifications')}}</title>
@endsection

@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.notifications')}}
            @if(permission('addNotification')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
              <a class="btn btn-success pull-right" href="{{ route('mngrAdmin.notifications.create') }}"><i class="glyphicon glyphicon-plus"></i> {{__('backend.send_notifications')}}</a>
            @endif
        </h3>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            @if($notifications->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('backend.type')}}</th>
                            <th>{{__('backend.device')}}</th>
                            <th>{{__('backend.title')}}</th>
                            <th>{{__('backend.message')}}</th>
                            <th>{{__('backend.date')}}</th>
                            <th>{{__('backend.audience')}}</th>
                            <th>{{__('backend.Published_by')}}</th>
                            <th class="text-right"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($notifications as $notification)
                            <tr>
                                <td>{{$notification->id}}</td>
                                <td>{!! $notification->type !!}</td>
                                <td>{{$notification->device_type == 1 ? 'Android' : ''}} {{$notification->device_type == 2 ? 'IOS' : ''}} {{$notification->device_type == 3 ? 'Browser' : ''}} {{$notification->device_type == 4 ? 'All' : ''}}</td>
                                <td>{{$notification->title}}</td>
                                <td>{{$notification->message}}</td>
                                <td>{{$notification->created_at->format("Y-m-d H:i:s")}}</td>
                                <td>{!! $notification->user_span !!}</td>
                                <td>{{ $notification->published_by != '' ? App\Models\User::where('id', $notification->published_by)->value('name') : '' }}</td>
                                <td class="text-right">
                                  @if(permission('showNotification')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                    <a class="btn btn-xs btn-primary" href="{{ route('mngrAdmin.notifications.show', $notification->id) }}"><i class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>
                                  @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $notifications->render() !!}
            @else
                <h3 class="text-center alert alert-warning"> {{__('backend.No_result_found')}} </h3>
            @endif

        </div>
    </div>

@endsection

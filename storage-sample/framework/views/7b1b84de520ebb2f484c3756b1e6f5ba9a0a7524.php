<?php $__env->startSection('title'); ?> <?php echo app('translator')->getFromJson('website.UploadExcel'); ?> <?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="row">
	<div class="col-12">
		<div class="">
			<div class="table-responsive m-t-40">
				<h1 class="title"> <?php echo app('translator')->getFromJson('website.UploadExcelSheets'); ?></h1>
				<div class="button-box pull-right">
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@fat"><i class="ti-upload"></i><?php echo app('translator')->getFromJson('website.UploadExcel'); ?></button>
					<button type="button" class="btn btn-info " ><a href="#" style="color: #fff" href="<?php echo e(asset('/backend/sheet.xlsx')); ?>" download="sheet.xlsx"><i class="ti-download"></i> <?php echo app('translator')->getFromJson('website.Download'); ?></a></button>
				</div>
				<table id="example23" class="display nowrap table table-hover table-striped table-bordered"
                            cellspacing="0" width="100%">
					<thead>
							<tr>
								<th><?php echo app('translator')->getFromJson('website.ID'); ?></th>
								<th><?php echo app('translator')->getFromJson('website.File'); ?></th>
								<th><?php echo app('translator')->getFromJson('website.CreatedAt'); ?></th>
								<th><?php echo app('translator')->getFromJson('website.Actions'); ?></th>
							</tr>
					</thead>
					<tbody>
						<?php if(! empty($orders)): ?>
							<?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<tr>
									<th scope="row"><?php echo e($file->id); ?></th>
									<td><?php echo e($file->file_name); ?></td>
									<td><?php echo e($file->created_at); ?></td>
									<td>
										<a href="<?php echo e($file->file_name); ?>" download="<?php echo e($file->file_name); ?>" class="text-inverse p-r-10" data-toggle="tooltip" title="" data-original-title="show" aria-describedby="tooltip468946">
											<i class="icon-download"></i>
										</a>
									</td>
								</tr>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						<?php endif; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>



	<!-- sample modal content -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="exampleModalLabel1"><?php echo app('translator')->getFromJson('website.UploadExcelSheets'); ?></h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

			</div>
			<form action="<?php echo e(app()->getLocale() == 'en' ? url('/en/customer/account/upload-excel') : url('/customer/account/upload-excel')); ?>" method="post" enctype="multipart/form-data">
				<div class="modal-body">
					<div class="card">
						<div class="card-body">
							<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
							<h4 class="card-title"><?php echo app('translator')->getFromJson('website.FileUpload'); ?></h4>
							<label for="input-file-max-fs"> <?php echo app('translator')->getFromJson('website.MaxSize'); ?></label>
							<input type="file" name="file" id="input-file-max-fs" class="dropify" data-max-file-size="2M" accept=".xls,.xlsx,.xlsm,.xlsb,.xml,application/ms-excel,application/vnd.ms-excel"/>
						</div>
					</div>
				</div>
				<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo app('translator')->getFromJson('website.Close'); ?></button>
						<input type="submit" class="btn btn-primary" value="Save">
				</div>
			</form>
		</div>
	</div>
</div>
<!-- /.modal -->

<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.profile.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/www/ufelix/ufelix/resources/views/frontend/profile/excel/upload-excel.blade.php ENDPATH**/ ?>
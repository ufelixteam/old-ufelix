@extends('agent.layouts.app')

@section('content')
    <div class="page-header noprint">
      <h3>Invoice #{{$id}}</h3>
      <div class=" pull-right" role="group" id="group" aria-label="...">
        <button type="button" id="printInvoice" class="btn btn-info"><i class="glyphicon glyphicon-print"></i> Print</button>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 col-xs-12 col-sm-12">
        <h2 class="text-center">Agent Orders</h2>
        <table class="table table-condensed table-striped" style="margin-top: 25px;">
            <thead class="text-center">
              <tr>
                <th>#</th>
                <th>Delivered Item</th>
                <th>Item Price</th>
                <th>Order Date</th>
                <th>Delivery Price</th>
                <th>Received Date</th>
                <th>Delivered Date</th>
              </tr>
            </thead>
              <tbody class="text-center">
                @foreach($invoices as $invoice)
                  <td>{{$invoice->id}}</td>
                  <td>{{$invoice->type}}</td>
                  <td>{{$invoice->order_price}}</td>
                  <th>{{$invoice->created_at}}</th>
                  <td>{{$invoice->delivery_price}}</td>
                  <td>{{$invoice->received_date}}</td>
                  <td>{{$invoice->delivery_date}}</td>
                </tr>
              @endforeach
            </tbody>
        </table>
        {{$invoices->links()}}
      </div>
    </div>
@endsection

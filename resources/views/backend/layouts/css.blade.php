<!-- VENDOR CSS -->
<link rel="stylesheet" href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendor/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendor/linearicons/style.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendor/chartist/css/chartist-custom.css')}}">
<!-- GOOGLE FONTS -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
<!-- ICONS -->
{{--<link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets/images/apple-icon.png')}}">--}}
<link rel="icon" type="image/png" sizes="96x96" href="{{asset('/website/images/flogo.png')}}">

<link rel="stylesheet" href="{{asset('assets/css/edits.css')}}">

<link rel="stylesheet" href="{{ asset('assets/css/bootstrap-datepicker.min.css')}}"/>
<style>
    .clickable-row {
        cursor: pointer;
    }
</style>

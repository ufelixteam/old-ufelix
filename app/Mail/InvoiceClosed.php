<?php

namespace App\Mail;

use App\Models\Invoice;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InvoiceClosed extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The invoice instance.
     *
     * @var Invoice
     */
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data = [])
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $pdf_name = 'Invoice' . '_' . $this->data['invoice']->id . '_' . date('Y_m_d') . '.pdf';

        if ($this->data['invoice']->type == 2) {
            $pdf = \PDF::loadView('backend.invoices.corporate_printInvoice', $this->data);
        } elseif ($this->data['invoice']->type == 1 && $this->data['invoice']->collected) {
            $pdf = \PDF::loadView('backend.invoices.driver_collected_printInvoice', $this->data);
        } else {
            $pdf = \PDF::loadView('backend.invoices.driver_printInvoice', $this->data);
        }

        return $this->subject('Invoice ' . $this->data['invoice']->id . date('Y_m_d'))
            ->view('emails.invoices.closed')
            ->with(
                [
                    'id' => $this->data['invoice']->id,
                    'name' => $this->data['invoice']->owner->name,
                ]
            )
            ->attachData($pdf->output(), $pdf_name, [
                'mime' => 'application/pdf',
            ]);
    }
}

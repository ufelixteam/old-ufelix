//Loading Firebase Package
var firebase = require("firebase");
var geofire = require('geofire');

/**
 * Update your Firebase Project
 * Credentials and Firebase Database
 * URL
 */
firebase.initializeApp({
    serviceAccount: "./ufelix-311a0-firebase-adminsdk-6l4zw-59cc731d2b.json",
    databaseURL: "https://ufelix-311a0.firebaseio.com"
});  //by adding your credentials, you get authorized to read and write from the database

/**
 * Loading Firebase Database and refering
 * to user_data Object from the Database
 */
var db = firebase.database();
var ref = db.ref("/user_data");  //Set the current directory you are working in

var firebaseRef = firebase.database().ref();
// Create a GeoFire index
var geoFireRef = new geofire.GeoFire(firebaseRef);

// geoFireRef.set('20', [40.607765, -73.758949]);
geoFireRef.query({
    center: [40.607765, -73.758949],
    radius: 10
}).on('key_entered' /*key_exited*/, (key, location, distance) => {
    console.log(key + ' entered query at ' + location + ' (' + distance + ' km from center)');
});

/**
 * Setting Data Object Value
 */
ref.set([
    {
        id: 20,
        name: "Jane Doe",
        email: "jane@doe.com",
        website: "https://jane.foo.bar"
    },
    {
        id: 21,
        name: "John doe",
        email: "john@doe.com",
        website: "https://foo.bar"
    }
]);

/**
 * Pushing New Value
 * in the Database Object
 */
ref.push({
    id: 22,
    name: "Jane Doe",
    email: "jane@doe.com",
    website: "https://jane.foo.bar"
});

/**
 * Reading Value from
 * Firebase Data Object
 */
// ref.once("value", function (snapshot) {
//     const data = snapshot.val();   //Data is in JSON format.
//     console.log(data);
// });
//
// ref.on('value', function (snapshot) {
//     const data = snapshot.val();   //Data is in JSON format.
//     console.log(data);
// });

ref.on('child_added', (snapshot) => {
    const data = snapshot.val();   //Data is in JSON format.
    console.log('data was added !!', data);
});

ref.on('child_removed', (snapshot) => {
    const data = snapshot.val();   //Data is in JSON format.
    console.log('data was removed !!', data);
});

ref.on('child_changed', (snapshot) => {
    const data = snapshot.val();   //Data is in JSON format.
    console.log('data was changed !!', data);
});

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ufelix</title>
    <!--<link rel="stylesheet" href="css/bootstrap.min.css">-->
    <!--<link rel="stylesheet" href="css/main.css">-->
    <style>
        .polica {
            padding: 20px;
            text-transform: capitalize;
        }

        .head {
            border-bottom: 4px solid #343a40;
        }

        .head h4 {
            font-weight: bold;

        }

        .m-auto {
            margin: auto !important;
        }

        .mt-auto,
        .my-auto {
            margin-top: auto !important;
        }

        .mr-auto,
        .mx-auto {
            margin-right: auto !important;
        }

        .mb-auto,
        .my-auto {
            margin-bottom: auto !important;
        }

        .ml-auto,
        .mx-auto {
            margin-left: auto !important;
        }

        .head button {
            border: none;
            box-shadow: none;
            outline: none;
            padding: .3rem 0.75rem !important;
        }

        .btn.focus, .btn:focus {
            border: none !important;
            box-shadow: none !important;
            outline: none !important;
        }

        .table-responsive {
            display: block;
            width: 100%;
            overflow-x: auto;
            -webkit-overflow-scrolling: touch;
            -ms-overflow-style: -ms-autohiding-scrollbar;
        }

        #invoice-body {
            padding: 19px 0;
            max-width: 1200px;
            margin: 30px auto;
            box-shadow: 1px 1px 12px #343a40;
            border-radius: 5px;
        }

        #invoice-body .invoice-title {

        }

        .logo-ufelix .img-thumbnail {
            border: none;
        }

        .logo-ufelix .line {
            margin-top: 5px;
            margin-bottom: .5rem;
            border-top: 2px solid #e7e9ea;
            width: 95%;
        }

        .data {
            background-color: #f80;
            height: 40px;
            color: #fff;
            line-height: 40px;
            font-weight: bold;

        }

        .order_number {
            margin-left: 14px;
            margin-top: 10px;
        }

        .order_number ul li {
            margin-bottom: 5px;
        }

        .customer-info {
            border: 1px solid #f80;
            margin: 10px;
            width: 97%;
        }

        .customer-info > div {
            border: 2px solid #f80;
            padding: 15px;

        }

        #table td, #table th {
            font-size: 10px;
            border-top: 1px solid #ff8800;
        }

        /*bootstrab class*/
        *,
        *::before,
        *::after {
            box-sizing: border-box;
        }

        html {
            font-family: sans-serif;
            line-height: 1.15;
            -webkit-text-size-adjust: 100%;
            -ms-overflow-style: scrollbar;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
        }


        article, aside, figcaption, figure, footer, header, hgroup, main, nav, section {
            display: block;
        }

        body {
            margin: 0;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, Noto Sans, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            text-align: left;
            background-color: #fff;
        }

        .list-inline {
            padding-left: 0;
            list-style: none;
        }

        .row {
            margin-right: -15px;
            margin-left: -15px;
        }

        .row-no-gutters {
            margin-right: 0;
            margin-left: 0;
        }

        .row-no-gutters [class*="col-"] {
            padding-right: 0;
            padding-left: 0;
        }

        .col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col,
        .col-auto, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm,
        .col-sm-auto, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .col-md,
        .col-md-auto, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg,
        .col-lg-auto, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl,
        .col-xl-auto {
            position: relative;
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
        }

        .img-thumbnail {
            padding: 0.25rem;
            background-color: #fff;
            border-radius: 0.25rem;
            max-width: 100%;
            height: auto;
        }

        .d-inline {
            display: inline !important;
        }

        h1, h2, h3, h4, h5, h6 {
            margin-top: 0;
            margin-bottom: 0.5rem;
        }

        h1, h2, h3, h4, h5, h6,
        .h1, .h2, .h3, .h4, .h5, .h6 {
            margin-bottom: 0.5rem;
            font-family: inherit;
            font-weight: 500;
            line-height: 1.2;
            color: inherit;
        }

        h1, .h1 {
            font-size: 2.5rem;
        }

        h2, .h2 {
            font-size: 2rem;
        }

        h3, .h3 {
            font-size: 1.75rem;
        }

        h4, .h4 {
            font-size: 1.5rem;
        }

        h5, .h5 {
            font-size: 1.25rem;
        }

        h6, .h6 {
            font-size: 1rem;
        }

        .text-right {
            text-align: right !important;
        }

        .text-center {
            text-align: center !important;
        }

        .table {
            border-collapse: collapse !important;
        }

        .table td,
        .table th {
            background-color: #fff !important;
            padding: .2rem 5px;
            font-size: 13px;
        }

        .table-bordered thead td, .table-bordered thead th {
            border-bottom-width: 2px;
        }

        .btn-primary {
            color: #fff;
            background-color: #007bff;
            border-color: #007bff;
        }

        .btn-secondary:focus, .btn-secondary.focus {
            box-shadow: 0 0 0 0.2rem rgba(130, 138, 145, 0.5);
        }

        .btn-secondary.disabled, .btn-secondary:disabled {
            color: #fff;
            background-color: #6c757d;
            border-color: #6c757d;
        }

        .btn-secondary {
            color: #fff;
            background-color: #6c757d;
            border-color: #6c757d;
        }

        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #e9ecef;
        }

        .ml-md-auto,
        .mx-md-auto {
            margin-left: auto !important;
        }

        .table-bordered th,
        .table-bordered td {
            border: 1px solid #dee2e6;
        }

        .table-bordered thead th,
        .table-bordered thead td {
            border-bottom-width: 2px;
        }

        .d-inline {
            display: inline !important;
        }

        .col-sm-1 {
            -ms-flex: 0 0 8.333333%;
            flex: 0 0 8.333333%;
            max-width: 8.333333%;
        }

        .col-sm-2 {
            -ms-flex: 0 0 16.666667%;
            flex: 0 0 16.666667%;
            max-width: 16.666667%;
        }

        .col-sm-3 {
            -ms-flex: 0 0 25%;
            flex: 0 0 25%;
            max-width: 25%;
        }

        .col-sm-4 {
            -ms-flex: 0 0 33.333333%;
            flex: 0 0 33.333333%;
            max-width: 33.333333%;
        }

        .col-sm-5 {
            -ms-flex: 0 0 41.666667%;
            flex: 0 0 41.666667%;
            max-width: 41.666667%;
        }

        .col-sm-6 {
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }

        .col-sm-7 {
            -ms-flex: 0 0 58.333333%;
            flex: 0 0 58.333333%;
            max-width: 58.333333%;
        }

        .col-sm-8 {
            -ms-flex: 0 0 66.666667%;
            flex: 0 0 66.666667%;
            max-width: 66.666667%;
        }

        .col-sm-9 {
            -ms-flex: 0 0 75%;
            flex: 0 0 75%;
            max-width: 75%;
        }

        .col-sm-10 {
            -ms-flex: 0 0 83.333333%;
            flex: 0 0 83.333333%;
            max-width: 83.333333%;
        }

        .col-sm-11 {
            -ms-flex: 0 0 91.666667%;
            flex: 0 0 91.666667%;
            max-width: 91.666667%;
        }

        .col-sm-12 {
            -ms-flex: 0 0 100%;
            flex: 0 0 100%;
            max-width: 100%;
        }

        .container {
            min-width: 992px !important;
        }

        .container {
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }

        @media (min-width: 576px) {
            .container {
                max-width: 540px;
            }
        }

        @media (min-width: 768px) {
            .container {
                max-width: 720px;
            }
        }

        @media (min-width: 992px) {
            .container {
                max-width: 960px;
            }
        }

        @media (min-width: 1200px) {
            .container {
                max-width: 1140px;
            }
        }

        .row {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -15px;
        }

        .text-title {
            font-size: 30px;
            font-weight: bold;
        }

        @media print {
            a[href]:after {
                content: none !important;
            }

            #print {
                visibility: hidden;
            }
        }

    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <button class="btn btn-info btn-block" id="print"> Print <i class="fa fa-file-pdf-o"></i></button>
    </div>
</div>
<header id="invoice-body">

    <div class="container">

        <div class="row">
            <div class="col-sm-4">
                <table class="table" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Emplyee Name</th>
                        <th>Invoice Date</th>
                    </tr>
                    <tr>
                        <td>@if(! empty( $invoice->owner) ) {{$invoice->owner->name }} @endif</td>
                        <td>{{date('Y-m-d')}}</td>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="col-sm-5 text-center">
                <b>Settlement Report</b>
            </div>
            <div class="col-sm-3 text-right">
                <table class="table" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Total Of Shipments</th>
                    </tr>
                    <tr>
                        <td>{{count($AllAcceptedPaginatedOrders)}}</td>
                    </tr>
                    </thead>
                </table>
            </div>

        </div>
        <hr style=" width: 100%; height: 3px; background-color: #000; ">

        <div class="row">
            <div class="col-sm-12">
                <table class="table table-bordered text-center" style="margin: auto; width: 100%">
                    <caption>Orders List</caption>
                    <thead>
                    <tr>
                        <th>SR</th>
                        <th>Corporate</th>
                        <th>AWE</th>
                        <th>Receiver Name</th>
                        <th>Dest</th>
                        <th>Status</th>
                        <th>Fees</th>
                        <th>E.Fees</th>
                        <th>F.Fees</th>
                        <th>Total Cost</th>
                        <th>Collected</th>
                        <th>Ship Cost</th>
                        <th>Residual</th>
                        <th>NET</th>
                        <th>Settlement</th>
                    </tr>
                    </thead>

                    <tbody>
                    @php
                        $totalShipCost = 0;
                        $totalPrice = 0;
                        $totalCollected = 0;
                        $totalResidual = 0;
                        $totalSettlement = 0;
                        $totalFees = 0;
                        $settlement = 0;
                        $totalNet = 0;
                        $i=0;
                    @endphp
                    @foreach($AllAcceptedPaginatedOrders as $index => $Order)
                        @if(($Order->status == 3 && (!$Order->delivery_status || $Order->delivery_status == 1)) || $invoice->status == 1
|| ($Order->status == 5 && $Order->warehouse_dropoff) || ($Order->status == 8 && $Order->warehouse_dropoff) || ($Order->status == 3 && in_array($Order->delivery_status, [2,4]) && $Order->warehouse_dropoff))

                            @php
                                if($invoice->status == 1){
                                    $shipCost = $Order->captain_bonus;
                                } else {
                                    $shipCost = $Order->driver_bonus ? $Order->driver_bonus : 0;
                                    if($Order->status == 5){
                                        $shipCost = $Order->recall_price ? $Order->recall_price : 0;
                                    }elseif($Order->status == 8){
                                        $shipCost = $Order->reject_price ? $Order->reject_price : 0;
                                    }
                                }

                                $fees = 0;
                                if($invoice->status == 1){
                                    $fees = $Order->fees;
                                }else{
                                    $prices = \App\Models\CustomerPrice::where('customer_id', $Order->customer_id)
                                    ->where('status', 1)
                                    ->where(function ($q) use ($Order) {
                                        $q->where(function ($q1) use ($Order) {
                                            $q1->where('start_station', $Order->s_government_id);
                                            $q1->where('access_station', $Order->r_government_id);
                                        })->orWhere(function ($q1) use ($Order) {
                                            $q1->where('access_station', $Order->s_government_id);
                                            $q1->where('start_station', $Order->r_government_id);
                                        });
                                    })->first();

                                    if (empty($prices)) {
                                        $prices = \App\Models\GovernoratePrice::where('status', 1)
                                        ->where(function ($q) use ($Order) {
                                            $q->where(function ($q1) use ($Order) {
                                                $q1->where('start_station', $Order->s_government_id);
                                                $q1->where('access_station', $Order->r_government_id);
                                            })->orWhere(function ($q1) use ($Order) {
                                                $q1->where('access_station', $Order->s_government_id);
                                                $q1->where('start_station', $Order->r_government_id);
                                            });
                                        })->first();
                                    }

                                    $fees = $Order->delivery_price ? $Order->delivery_price : 0;
                                    if($Order->status == 5){
                                        $fees = $prices->recall_cost ? $prices->recall_cost : 0;
                                    }elseif($Order->status == 8){
                                        $fees = $prices->reject_cost ? $prices->reject_cost : 0;
                                    }elseif($Order->status == 4){
                                        $fees = $prices->cancel_cost ? $prices->cancel_cost : 0;
                                    }
                                }

                                /*$fees = $invoice->status == 1 ? $Order->fees : $Order->delivery_price;*/
                                $e_fees = $invoice->status == 1 ? $Order->e_fees : $Order->overweight_cost;
                                $f_fees = $fees + $e_fees;

                                $totalShipCost += $shipCost;
                                $totalPrice += $Order->total_price;
                                $totalCollected += $Order->cost;
                                $totalResidual += $Order->cost - $f_fees;
                                $totalSettlement += $Order->cost - $shipCost;
                                $totalFees += $f_fees;
                                $totalNet += $f_fees - $shipCost;

                                $settlement = $Order->cost - $shipCost;
                                $net = $f_fees - $shipCost;
                                $residual = $Order->cost - $f_fees;
                            @endphp
                            <tr>
                                <td>{{++$i}}</td>
                                <td>{{$Order->customer->Corporate->name}}</td>
                                <td>{{$Order->order_number}}</td>
                                <td>{{$Order->receiver_name}}</td>
                                <td>{{$Order->to_government->name_en}}</td>
                                <td>{!! $Order->invoice_status_print !!}</td>
                                <th>{{$fees}}</th>
                                <th>{{$e_fees}}</th>
                                <th>{{$f_fees}}</th>
                                <th>{{$Order->total_price}}</th>
                                <td>{{$Order->cost}}</td>
                                <td>{{$shipCost}}</td>
                                <td>{{$residual}}</td>
                                <td>{{$net}}</td>
                                <td>{{$settlement}}</td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        @php
            $totalTaskCost = 0;
        @endphp
        <br><br>
        @if(count($pickups))
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-bordered text-center" style="margin: auto; width: 100%">
                    <caption>Tasks List</caption>
                    <thead>
                    <tr>
                        <th>SR</th>
                        <th>Task Number</th>
                        <th>Number of Orders</th>
                        <th>Bonus</th>
                        <th>Task Price</th>
                        <th>Task Cost</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($pickups as $index => $pickup)
                        @php
                            $bonus = $invoice->status == 1 ? $pickup->captain_bonus : ($pickup->bonus_per_order ? $pickup->bonus_per_order : 0);
                            $pickup_price = $invoice->status == 1 ? $pickup->pickup_price : ($pickup->delivery_price ? $pickup->delivery_price : 0);
                            $total_cost = $bonus + $pickup_price;
                            $totalTaskCost += $total_cost;
                        @endphp
                        <tr>
                            <td>{{$index+1}}</td>
                            <td>{{$pickup->pickup_number}}</td>
                            <td>{{$pickup->pickup_orders}}</td>
                            <td>{{$bonus}}</td>
                            <td>{{$pickup_price}}</td>
                            <td>{{$total_cost}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @endif
        <br><br>

        <div class="row">
            <div class="col-sm-3 text-left">
                <table class="table" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Total Collected</th>
                        <th>{{$totalCollected}}</th>
                    </tr>
                    <tr>
                        <th>Total Residual</th>
                        <th>{{$totalResidual}}</th>
                    </tr>

                    <tr>
                        <th>Total Other Cost</th>
                        <th>{{$totalTaskCost}}</th>
                    </tr>
                    <tr>
                        <th>Total Fees</th>
                        <th>{{$totalFees}}</th>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="col-sm-1">
            </div>
            <div class="col-sm-4">
                <table class="table" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Printed By</th>
                        <th>{{$user->name}}</th>
                    </tr>
                    <tr>
                        <th>Signature /</th>
                        <th></th>
                    </tr>
                    <tr>
                        <th>Signature /</th>
                        <th></th>
                    </tr>
                    <tr>
                        <th>Company Seal</th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="col-sm-1">
            </div>
            <div class="col-sm-3 text-right">
                <table class="table" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Total Settlement</th>
                        <th>{{$totalSettlement}}</th>
                    </tr>
                    <tr>
                        <th>Total Ship cost</th>
                        <th>{{$totalShipCost}}</th>
                    </tr>

                    <tr>
                        <th>Total Net</th>
                        <th>{{$totalNet}}</th>
                    </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>
</header>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>
    $(function () {
        window.print();
        // window.history.back();
    });

    $(function () {
        document.querySelector("#print").addEventListener("click", function () {
            window.print();
            // window.history.back();
        });
    });
</script>

</body>
</html>

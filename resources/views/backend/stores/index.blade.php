@extends('backend.layouts.app')

@section('css')
<title>{{__('backend.stores')}}</title>
@endsection

@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.stores')}}
            @if(permission('addStore')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
              <a class="btn btn-success pull-right" href="{{ route('mngrAdmin.stores.create') }}"><i class="glyphicon glyphicon-plus"></i> {{__('backend.add_store')}}</a>
            @endif
        </h3>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($stores->count())
                <form action="{{route('mngrAdmin.stores.stores_pdf')}}" method="get" target="_blank">

                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th class="text-center">{{__('backend.id')}}#</th>
                            <th class="text-center">{{__('backend.name')}}</th>
                            <th class="text-center">{{__('backend.government')}}</th>
                            <th class="text-center">{{__('backend.city')}}</th>
                            <th class="text-center">{{__('backend.address')}}</th>
                            <th class="text-center">{{__('backend.Created_Date')}}</th>
                            <th class="text-center">{{__('backend.status')}}</th>
                            <th class="text-center">{{__('backend.responsible')}}</th>

                            <th class="text-center">{{__('backend.mobile')}}</th>
                            <th class="text-center">{{__('backend.no_of_stocks')}}</th>
                            <th class="text-center">{{__('backend.pickup_default')}}</th>
                            <th>
                                <input type="button" class="btn btn-info btn-xs" id="toggle"
                                       value="{{__('backend.select_all')}}" onClick="do_this()"/>
                                <button type="submit"
                                        class="btn btn-warning btn-xs btn-pdf">{{__('backend.print')}}</button>
                            </th>
                            <th class="text-right"></th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($stores as $store)
                            <tr>
                                <td class="text-center">{{$store->id}}</td>
                                <td class="text-center">{{$store->name}}</td>
                                <td class="text-center">
                                  @if(! empty($store->governorate))
                                    @if(session()->get('lang') == 'en')
                                      {{$store->governorate->name_en}}
                                    @else
                                      {{$store->governorate->name_ar}}
                                    @endif
                                  @endif
                                </td>

                                <td class="text-center">
                                    @if(!empty($store->city))
                                        @if(session()->get('lang') == 'en')
                                            {{$store->city->name_en}}
                                        @else
                                            {{$store->city->city_name}}
                                        @endif

                                    @endif
                                </td>
                                <td class="text-center">{{$store->address}}</td>
                                <td class="text-center">{{$store->created_at}}</td>

                                <td class="text-center">{!! $store->status_span !!}</td>
                                <td class="text-center">
                                    @foreach($store->responsibles as $responsible)
                                        {{$responsible->name}}
                                    @endforeach
                                </td>
                                <td class="text-center">{{$store->mobile}}</td>
                                <td class="text-center">{{$store->stocks_count}}</td>
                                <td class="text-center">{!! $store->pickup_default_span !!}</td>
                                <td class="exclude-td">
                                    <input type="checkbox" name="select[]" value="{{$store->id}}"/>
                                </td>
                                <td class="text-right">
                                    @if(permission('addStocks'))
                                        <a class="btn btn-xs btn-primary" href="{{route('mngrAdmin.stocks.create', $store->id)}}">
                                            <i class="glyphicon glyphicon-plus"></i> {{__('backend.add_stock')}}
                                        </a>
                                    @endif
                                    @if(permission('showStore')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                      <a class="btn btn-xs btn-primary" href="{{ route('mngrAdmin.stores.show', $store->id) }}"><i class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>
                                    @endif
                                    @if(permission('editStore')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                      <a class="btn btn-xs btn-warning" href="{{ route('mngrAdmin.stores.edit', $store->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $stores->render() !!}
                </form>

            @else
                <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
            @endif
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function do_this() {

            var checkboxes = document.getElementsByName('select[]');
            var button = document.getElementById('toggle');
            console.log('checkboxes', checkboxes);
            console.log('button ', button);
            if (button.value === '{{__('backend.select_all')}}') {
                for (var i in checkboxes) {
                    checkboxes[i].checked = 'FALSE';
                }
                console.log('checkboxes if', checkboxes);
                button.value = "{{__('backend.deselect')}}"
            } else {
                for (var i in checkboxes) {
                    checkboxes[i].checked = '';
                }
                console.log('checkboxes else', checkboxes);

                button.value = "{{__('backend.select_all')}}";
            }
        }

        /*$('.responsible').on('input', function () {
            $('#save_responsible_' + $(this).attr('data-id')).removeClass('disabled');
        });

        $('.save_responsible').on('click', function (e) {
            e.preventDefault();
            var self = $(this);
            let id = $(this).attr('data-id');
                    url: '{{URL::asset("/mngrAdmin/stores/save_responsible")}}',
                type: 'post',
                data: {id: id, responsible:  $('#responsible_' + id).val(), _token: "{{csrf_token()}}"},
                success: function (data) {
                    self.addClass('disabled');
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

        });*/
    </script>
@endsection

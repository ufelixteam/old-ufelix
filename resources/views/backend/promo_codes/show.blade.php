@extends('backend.layouts.app')
@section('css')
  <title>Promo Codes - View</title>
@endsection
@section('header')
<div class="page-header">
        <h3>Show #{{$promo_code->id}}</h3>{!! $promo_code->status_span !!}
        <form action="{{ route('mngrAdmin.promo_codes.destroy', $promo_code->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('mngrAdmin.promo_codes.edit', $promo_code->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <div class="form-group">
                <label for="nome">#</label>
                <p class="form-control-static">{{$promo_code->id}}</p>
            </div>
            <div class="form-group col-sm-6">
                     <label for="code">CODE</label>
                     <p class="form-control-static">{{$promo_code->code}}</p>
                </div>
                    <div class="form-group col-sm-6">
                     <label for="no_tries">NO_TRIES</label>
                     <p class="form-control-static">{{$promo_code->no_tries}}</p>
                </div>
                    <div class="form-group col-sm-6">
                     <label for="discount">DISCOUNT</label>
                     <p class="form-control-static">{{$promo_code->discount}}</p>
                </div>


            <div class="col-md-12 col-xs-12 col-sm-12">
                <a class="btn btn-link" href="{{ route('mngrAdmin.promo_codes.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
            </div>

        </div>
    </div>

@endsection

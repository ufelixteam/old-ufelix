<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketRule extends Model
{
    protected $guarded = ['id'];
}

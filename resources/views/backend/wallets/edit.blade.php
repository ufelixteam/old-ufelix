@extends('backend.layouts.app')
@section('css')
  <title>Wallets - Edit</title>
@endsection
@section('header')
  <div class="page-header">
    <h3><i class="glyphicon glyphicon-edit"></i>  Edit {{ $header }} #{{$wallet->id}}</h3>
  </div>
@endsection

@section('content')
  @include('error')
  <div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
      <form action="{{ route('mngrAdmin.wallets.update', $wallet->id) }}" method="POST" name="wallet_create">
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
		    <input type="hidden" name="type" value="{{ $type }}" />
        <div class="form-group col-md-6 col-sm-6 @if($errors->has('value')) has-error @endif">
          <label for="value-field">Wallet Amount: </label>
          <input type="text" id="value-field" name="value" class="form-control" value="{{ is_null(old("value")) ? $wallet->value : old("value") }}"/>
          @if($errors->has("value"))
            <span class="help-block">{{ $errors->first("value") }}</span>
          @endif
        </div>
        <div class="form-group col-md-6 col-sm-6 @if($errors->has('status')) has-error @endif">
          <label for="status-field">Status</label>
        	<select class="form-control" name="status">
         		<option {{ ($wallet->status==1)?"selected":"" }} value="1" >Available</option>
         		<option {{ ($wallet->status==0)?"selected":"" }} value="0" >Not Available</option>
           </select>
           @if($errors->has("status"))
              <span class="help-block">{{ $errors->first("status") }}</span>
           @endif
        </div>
        <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
            <button type="submit" id="form_submit" class="btn btn-primary">Save Edits</button>
            <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.wallets.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
        </div>
      </form>
    </div>
  </div>
@endsection
@section('scripts')
<script type="text/javascript">
  $(function () {
    $("form[name='wallet_create']").validate({
        // Specify validation rules
      rules: {
          value: {
              required: true,
              number:true,
          },
      },
      // Specify validation error messages
      messages: {
          value: {
              required: "Please Enter the value",
          },
      },
      errorPlacement: function (error, element) {
          $(element).parents('.form-group').append(error);
      },
      highlight: function (element) {
          $(element).parent().addClass('error');
      },
      unhighlight: function (element) {
          $(element).parent().removeClass('error');
      },
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function (form) {
          form.submit();
      }
    });
   $("select[name='object_id']").change(function(){
    var object_id = $(this).val();
    var token = $("input[name='_token']").val();
    $.ajax({
      url: "{!! url('/check-user-wallet') !!}",
      method: 'POST',
      data: {object_id:object_id, _token:token,id:"{{$wallet->id}}" },
      success: function(data) {

          if(data=="false"){
            $("#object_error").show();
            $("#form_submit").attr('disabled',true);
          }else{
            $("#object_error").hide();
            $("#form_submit").removeAttr('disabled');
          }
      }
    });
  });

  var object_id = $('#object_id').val();
  var token = $("input[name='_token']").val();
  $.ajax({
    url: "{!! url('/check-user-wallet') !!}",
    method: 'POST',
    data: {object_id:object_id, _token:token,id:"{{$wallet->id}}" },
    success: function(data) {

    if(data=="false"){
        $("#object_error").show();
        $("#form_submit").attr('disabled',true);
      }else{
        $("#object_error").hide();
        $("#form_submit").removeAttr('disabled');
      }
    }
  });

 });
</script>
@endsection

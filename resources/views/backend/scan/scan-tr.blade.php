@if($order)
    {{--    <tr id="{{$order->id}}" data-driver="{{$order->accepted ? $order->accepted->driver->id : null}}" class="testtooltip" data-toggle="tooltip" data-html="true"--}}
    {{--        data-container="body" data-placement="bottom"--}}
    {{--        title='{!! $order->problems_table !!}'--}}
    {{--        @if($order->scan_color) style="{{$order->scan_color}}" @endif>--}}
    <tr id="{{$order->id}}" class="orderRow" data-driver="{{$order->accepted ? $order->accepted->driver->id : null}}"
        @if($order->scan_color) style="{{$order->scan_color}}" @endif>
        <td @if (count($order->comments)) class="testtooltip"
            tabindex="0"
            data-html="true"
            data-toggle="popover"
            data-trigger="hover"
            data-placement="top"
            title='<button class="close" data-dismiss="alert"><i class="fa fa-close"></i></button>'
            data-content='{!! $order->problems_table !!}' @endif>
            @if($order->status == 2 || $order->status == 5)
                <span class=" {{ $order->has_comments_during_two_days ? 'text-success' : 'text-danger' }}"
                      style="font-weight: bold"><i class='fa fa-circle' aria-hidden='true'></i></span>
            @endif
        </td>
        <td class="serial">
            {{isset($serial) ? $serial : ''}}
        </td>
        {{--        <td>{{$order->id}}</td>--}}
        <td class="status">{!!  $order->status_span !!}</td>
        <td>{{$order->collection_id ? $order->collection_id : '-'}}</td>
        <td>{{$order->receiver_name}}</td>
        <td>
            {{$order->sender_name}}
            <br>
            @if($order->customer && $order->customer->Corporate)
                <strong style="color: #0c81bc"> {{$order->customer->Corporate->name}}</strong>
            @endif

        </td>
        <td>
            @if(! empty($order->from_government)  && $order->from_government != null )
                {{$order->from_government->name_en}}
            @endif
        </td>

        <td>
            @if(! empty($order->to_government)  && $order->from_government != null  )
                {{$order->to_government->name_en}}
            @endif
        </td>
        <td style="font-weight: bold; color: #a43;" id="{{$order->order_number}}" class="order_number">{{$order->order_number}}</td>
        <td style="font-weight: bold; color: #0c81bc;">
            <span class="driver" id="driver_{{$order->id}}">
            @if(! empty($order->accepted ) && ! empty($order->accepted->driver))
                    {{$order->accepted->driver->name}}
                @else
                    -
                @endif
            </span>
        </td>
        <td style="font-weight: bold; color: #a43;">
            <span class="total_cost">{{$order->total_price}}</span>
        </td>
        @if ($additional_filter === 'captain_entry' || $additional_filter === 'quick_entry')
            <div class="captain_data">

                <td style="font-weight: bold; color: #a43;" class="text-center">
                    <input type="text"
                           class="form-control delivery_status_class collected_cost @if($order->status == 5) disabled @endif"
                           {{--                           @if($order->delivery_status == 3) disabled @endif--}}
                           value="{{($order->collected_cost || $order->collected_cost == 0) ? $order->collected_cost : ($order->payment_method_id == 1 && $order->order_price == 0 ? 0 : $order->total_price) }}"
                           data-id="{{$order->id}}"
                           name="collected_cost"
                           id="collected_cost_{{$order->id}}"
                           style="width: 90px;"/>
                </td>
                {{--            <td>--}}
                {{--                <button class="btn btn-success save_changes delivery_status_class" disabled--}}
                {{--                        id="save_changes_{{$order->id}}" data-id="{{$order->id}}">{{__('backend.save')}}</button>--}}
                {{--            </td>--}}
                <td>
                    <input type="radio" name="delivery_status_{{$order->id}}" value="1"
                           class="delivery_status delivery_status_class_{{$order->id}}"
                           data-id="{{$order->id}}"
                           @if($order->delivery_status == 3) disabled @endif
                           @if($order->delivery_status == 1) checked @endif>
                </td>
                <td>
                    <input type="radio" name="delivery_status_{{$order->id}}" value="2"
                           class="delivery_status delivery_status_class_{{$order->id}}"
                           data-id="{{$order->id}}"
                           @if($order->delivery_status == 3) disabled @endif
                           @if($order->delivery_status == 2) checked @endif>
                </td>
                <td>
                    <input type="radio" name="delivery_status_{{$order->id}}" value="4"
                           class="delivery_status delivery_status_class_{{$order->id}}"
                           data-id="{{$order->id}}"
                           @if($order->delivery_status == 3) disabled @endif
                           @if($order->delivery_status == 4) checked @endif>
                </td>
                <td>
                    <input type="radio" name="delivery_status_{{$order->id}}" value="3"
                           class="delivery_status delivery_status_class_{{$order->id}} delivery_status_value_3"
                           data-id="{{$order->id}}"
                           id="delivery_status_value_3_{{$order->id}}"
                           @if($order->delivery_status != null) disabled @endif
                           @if($order->delivery_status == 3) checked @endif>
                </td>
                <td>
                    <input type="radio" name="delivery_status_{{$order->id}}" value="5"
                           class="delivery_status delivery_status_class_{{$order->id}} delivery_status_value_5"
                           data-id="{{$order->id}}"
                           id="delivery_status_value_5_{{$order->id}}"
                           @if($order->delivery_status != null) disabled @endif
                           @if($order->delivery_status == 5) checked @endif>
                </td>
            </div>

        @endif
        <td class="selectpdf text-center" id="selectpdf">
            @if($order->is_pickup == "0")
                <input type="checkbox" name="select[]" value="{{$order->id}}"
                       class="selectOrder" id="selectOrder_{{$order->id}}"
                       data-warehouse="{{$order->moved_in ? $order->moved_in : ''}}"
                       data-customer="{{$order->customer_id ? $order->customer_id : null}}"
                       data-driver="{{$order->accepted ? $order->accepted->driver->id : null}}"
                       data-latitude="{{$order->accepted ? $order->accepted->driver->latitude : null}}"
                       data-longitude="{{$order->accepted ? $order->accepted->driver->longitude : null}}"
                       data-value="{{$order->id}}"/>
            @endif

        </td>
        <td>
            {{--                        <div class="dropdown" style="display: inline-block;">--}}
            {{--                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"--}}
            {{--                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">--}}
            {{--                                <i class="fa fa-ellipsis-v" aria-hidden="true"></i>--}}
            {{--                            </button>--}}
            {{--                            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">--}}
            {{--                                <li>--}}
            {{--                                    <a href="{{ route('mngrAdmin.orders.show', $order->id ) }}"><i--}}
            {{--                                            class="fa fa-eye"></i> {{__('backend.view')}}</a>--}}
            {{--                                </li>--}}
            {{--                                <li>--}}
            {{--                                    <a href="{{ route('mngrAdmin.orders.print_police', ['ids' => $order->id] ) }}"--}}
            {{--                                       target="_blank"><i--}}
            {{--                                            class="fa fa-print"></i> {{__('backend.print')}}</a>--}}
            {{--                                </li>--}}
            {{--                            </ul>--}}
            {{--                        </div>--}}
            <div id="removeRow" style="display: inline-block;vertical-align: middle;margin: 0 5px;"
                 data-id="{{$order->id}}">
                <a href="#" style="color: black;"><span class="glyphicon glyphicon-remove"></span></a>
            </div>
        </td>
        {{--        <td class="text-center">--}}
        {{--            <div id="removeRow"--}}
        {{--                 data-id="{{$order->id}}">--}}
        {{--                <a href="#" style="color: black;"><span class="glyphicon glyphicon-remove"></span></a>--}}
        {{--            </div>--}}
        {{--        </td>--}}
    </tr>
    <tr>
        <td colspan="@if ($additional_filter === 'captain_entry' || $additional_filter === 'quick_entry') 19 @else 13 @endif"
            style="text-align: right">

            @if($additional_filter == 'captain_exit')
                <span class="badge badge-secondary" style="border-radius:0; font-size: 15px">{{__('backend.Create_at')}}: {{$order->created_at ? date('d/m/Y', strtotime($order->created_at)) : '-'}}</span>
                <span class="badge badge-secondary" style="border-radius:0; font-size: 15px">{{__('backend.Dropped_at')}}: {{$order->dropped_at ? date('d/m/Y', strtotime($order->dropped_at)) : '-'}}</span>
            @endif

            <a href="#" class="btn btn-info disabled inline_comment" style=" margin-left: 100px"
               data-id="{{$order->id}}">{{__('backend.comment')}}</a>
            <input type="text" class="comment_text form-control" style="display: inline-block;width: 350px;"
                   name="comment"/>
            <a href="#"
               class="btn btn-secondary dropOne @if (!in_array($additional_filter, ['captain_entry', 'client_drop', 'recall_drop', 'quick_entry', 'pending_dropped', 'pickup_dropped', 'all_orders'])) disabled @endif"
               data-id="{{$order->id}}"
               data-customer="{{$order->customer_id ? $order->customer_id : null}}">{{__('backend.drop')}}</a>
            <a href="#"
               class="btn btn-danger recallOne @if (!in_array($additional_filter, ['captain_entry', 'quick_entry', 'all_orders'])) disabled @endif"
               data-id="{{$order->id}}"
               data-customer="{{$order->customer_id ? $order->customer_id : null}}">{{__('backend.recall')}}</a>
            <a href="#"
               class="btn btn-danger rejectOne @if (!in_array($additional_filter, ['captain_entry', 'quick_entry', 'all_orders'])) disabled @endif"
               data-id="{{$order->id}}"
               data-customer="{{$order->customer_id ? $order->customer_id : null}}">{{__('backend.reject')}}</a>
            <a style="padding-left: 15px;padding-right: 15px;" href="{{ route('mngrAdmin.orders.show', $order->id ) }}" class="btn btn-primary"
               target="_blank"><i
                    class="fa fa-eye"></i> {{__('backend.view')}}</a>
            <a style="padding-left: 10px;padding-right: 10px;" href="{{ route('mngrAdmin.orders.print_police', ['ids' => $order->id] ) }}" class="btn btn-primary"
               target="_blank"><i
                    class="fa fa-print"></i> {{__('backend.print')}}</a>


        </td>
    </tr>
@endif

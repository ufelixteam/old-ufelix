@extends('agent.layouts.app')

@section('content')
    <div class="page-header noprint">
      <h3>Invoice #{{$invoice->id}}</h3>
      <div class=" pull-right" role="group" id="group" aria-label="...">
        <button type="button" id="printInvoice" class="btn btn-info"><i class="glyphicon glyphicon-print"></i> Print</button>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 col-xs-12 col-sm-12">
        <h2 class="text-center">
          @if($invoice->type == 1)
            {{$invoice->owner->name}} Driver
          @elseif($invoice->type == 2)
            {{$invoice->owner->name}} Corporate
          @elseif($invoice->type == 3)
            {{$invoice->owner->name}} Agent
          @endif
          Orders
        </h2>
        <table class="table table-condensed table-striped" style="margin-top: 25px;">
            <thead class="text-center">
              <tr>
                <th>#</th>
                <th>Delivered Item</th>
                <th>Item Price</th>
                <th>Order Date</th>
                <th>Delivery Price</th>
                <th>Received Date</th>
                <th>Delivered Date</th>
              </tr>
            </thead>
            <form action="{{ route('mngrAdmin.invoices.Transfere', $invoice->id) }}" method="post" class="text-center" style="position: relative;">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <tbody class="text-center">
                @foreach($AllAcceptedOrders as $index => $AcceptedOrder)
                @if($AcceptedOrder->status == 4 && $AcceptedOrder->Order->status == 2 || $AcceptedOrder->status == 4 && $AcceptedOrder->Order->status == 4  )
                  @if($AcceptedOrder->Order->payment_method_id >= 5)
                  <tr style="color: red; background-color: yellow;">
                  @else
                  <tr style="color: red;">
                  @endif
                @elseif($AcceptedOrder->Order->payment_method_id >= 5)
                <tr style="background-color: yellow;">
                @else
                <tr>
                @endif
                  <td>{{$AcceptedOrder->Order->id}}</td>
                  <td>{{$AcceptedOrder->Order->type}}</td>
                  <td>{{$AcceptedOrder->Order->order_price}}</td>
                  <th>{{$AcceptedOrder->Order->created_at}}</th>
                  <td>{{$AcceptedOrder->delivery_price}}</td>
                  <td>{{$AcceptedOrder->received_date}}</td>
                  <td>{{$AcceptedOrder->delivery_date}}</td>
                </tr>
              @endforeach
            </tbody>
          </form>
        </table>
      </div>
      <div class="col-md-12 col-xs-12 col-sm-12" style="margin-top: 30px;">
        <h2 class="text-center">
            {{$invoice->owner->name}} Agent Bill
        </h2>
        <div id="Transaction">
          <table class="table table-condensed table-striped text-center">
            <thead>
              <tr>
                @if($invoice->type == 2)
                <th>Total Order Price</th>
                <th>Total Delivery</th>
                <th>Corporate Deserved</th>
                @else
                 <th>Total order Price</th>
                
                  <th>Total Delivery</th>
                  <th>
                    {{$invoice->type_txt}} Pocket</th>
                    
                  <th>
                  {{$invoice->type_txt}} Commission</th>
                  <th>UFelix Commission</th>
                  <th>
                    {{$invoice->type_txt}} Depth
                  </th>
                  <th>UFelix Depth</th>
                  <th>Total {{$invoice->type_txt}} Deserved</th>
                  <th>Total UFelix Deserved</th>

                @endif
              </tr>
            </thead>
            <tbody>
              <tr>
                @if($invoice->type == 2)
                <td>{{$totalItem}}</td>
                <td>{{is_null($othtotalDelivery)? 0 : $othtotalDelivery}}</td>
                <td>{{$totalItem  - (is_null($result)? 0 : $result)}}</td>
                @else
             <!--   <td>{{$totalItem}}</td>
                <td>{{$totalDelivery}}</td>
                <td>{{$collectedDelivery}}</td>
                <td>{{$totalDelivery-$commission}}</td>
                <td>{{$commission}}</td>
                <td>{{$othtotalDelivery-$othCommission}}</td>
                <td>{{$result}}</td>
              -->

              <td>{{$totalItem}}</td>
                <td>{{$totalDelivery}}</td>
                <td>{{$collectedDelivery}}</td>
               
                <td>{{$commission}}</td>
                <td>{{$totalDelivery-$commission}}</td>

                  @if($commission > $collectedDelivery )

                    <td>0</td>
                    <td>{{ ($commission) - $collectedDelivery }}</td>
                    <td>{{ $commission }}</td>
                    <td>{{ $totalItem - (($commission) - $collectedDelivery) }}</td>

                  @elseif(($commission) < $collectedDelivery)

                    <td>{{ $collectedDelivery - ($commission) }}</td>
                    <td>0</td>
                    <td>{{ $commission }}</td>
                    <td>{{ $totalItem - (($commission) - $collectedDelivery) }}</td>

                  @elseif(($commission) == $collectedDelivery)
                    <td>0</td>
                    <td>0</td>

                    <td>{{ $commission }}</td>
                    <td>{{ $totalItem - (($commission) - $collectedDelivery) }}</td>

                  @endif
                @endif
      
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
@endsection

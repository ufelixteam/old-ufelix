<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Activity_log;
use App\Models\Activity_log_type;
use Flash;

class ActitivyLogController extends Controller
{

    public function index(Request $request)
    {

        $activityLogs = Activity_log::orderBy('id', 'desc');
        $date = app('request')->input('date');
        $filter = app('request')->input('filter');

        if( isset($date) && $date != "") {
           $activityLogs = $activityLogs->whereDate('created_at','like',$date.'%');
        }

        if( isset($filter) && $filter !="" && $filter != -1 ) {
           $activityLogs = $activityLogs->where('activity_type' , $filter);
        }

        $activityLogs = $activityLogs->paginate(50);

          if ($request->ajax()) {
              return [
                  'view' => view('backend.activity_log.table', compact('activityLogs'))->render(),
              ];
          }

        $ActivityLogType = Activity_log_type::all();
        return view('backend.activity_log.index', compact('activityLogs', 'ActivityLogType'));
    }

    public function destroy($id)
    {
        $activityLog = Activity_log::findOrFail($id);
        $activityLog->delete();

        flash(__('backend.deleted_activityLog_successfully'))->error();

        return redirect()->back();
    }

}

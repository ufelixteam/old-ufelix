  <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            @if($trucks->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('backend.Model_Name')}}</th>
                            <th>{{__('backend.Plate_Number')}}</th>
                            <th>{{__('backend.Vehicle_Type')}}</th>
                            <th>{{__('backend.status')}}</th>
                            <th>{{__('backend.agent')}}</th>
                            <th class="text-right"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($trucks as $truck)
                            <tr>
                                <td>{{$truck->id}}</td>
                                <td>{{$truck->model_name}}</td>
                                <td>{{$truck->plate_number}}</td>
                                <th>
                                  @foreach($types as $type)
                                    @if($truck->vehicle_type_id == $type->id)
                                      @if(session()->has('lang') == 'ar')
                                        {{$type->name_ar}}
                                      @else
                                        {{$type->name_en}}
                                      @endif
                                    @else

                                    @endif
                                  @endforeach
                                </th>
                                <td>{!! $truck->status_span !!}</td>
                                <td>{{ ! empty($truck->agent) ? $truck->agent->name : ''}}</td>
                                <td class="text-right">
                                    @if(permission('showVehicle')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                      <a class="btn btn-xs btn-primary" href="{{ route('mngrAdmin.trucks.show', $truck->id) }}"><i class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>
                                    @endif
                                    @if(permission('editVehicle')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                      <a class="btn btn-xs btn-warning" href="{{ route('mngrAdmin.trucks.edit', $truck->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                                    @endif
                                    @if(permission('deleteVehicle')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                      <form action="{{ route('mngrAdmin.trucks.destroy', $truck->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
                                          <input type="hidden" name="_method" value="DELETE">
                                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                          <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}</button>
                                      </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {!! $trucks->appends($_GET)->links() !!}

            @else
                <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
            @endif
        </div>
    </div>

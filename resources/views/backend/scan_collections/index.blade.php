@extends('backend.layouts.app')
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">

    <title>{{__('backend.scan_collections')}}</title>
@endsection
@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.scan_collections')}}
        </h3>
    </div>
@endsection

@section('content')

    <div class="alert alert-danger" hidden>
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong id="errorId"></strong>
    </div>

    <div class="row" style="margin-bottom:15px;">
        <div class="col-md-12">
            <div class="col-md-3 col-sm-3">
                <div id="imaginary_container">
                    <div class="input-group stylish-input-group">
                        <input type="text" class="form-control" id="search-field" name="search"
                               placeholder="{{__('backend.search')}} ">
                        <span class="input-group-addon">
                      <button type="button" id="search-btn1">
                        <span class="glyphicon glyphicon-search"></span>
                      </button>
                    </span>
                    </div>
                </div>
            </div>
            <div class="group-control col-md-3 col-sm-3">
                <select id="scan_type" name="scan_type" class="form-control">
                    <option value=""> {{__('backend.scan_type')}}</option>
                    <option value="1">{{__('backend.captain_entry')}}</option>
                    <option value="2">{{__('backend.captain_exit')}}</option>
                    <option value="3">{{__('backend.quick_entry')}}</option>
                </select>
            </div>
        </div>
    </div>

    <div class="list">
        @include('backend.scan_collections.table')
    </div>
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

    <script type="text/javascript">
        $("#scan_type").on('change', function () {
            $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
            $.ajax({
                url: '{{URL::asset("/mngrAdmin/scan_collections")}}' + '?scan_type=' + $("#scan_type").val() + '&search=' + $("#search-field").val(),
                type: 'get',
                success: function (data) {
                    $('.list').html(data.view);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $("#search-btn1").on('click', function () {
            $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
            $.ajax({
                url: '{{URL::asset("/mngrAdmin/scan_collections")}}' + '?scan_type=' + $("#scan_type").val() + '&search=' + $("#search-field").val(),
                type: 'get',
                success: function (data) {
                    $('.list').html(data.view);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });
    </script>
@endsection

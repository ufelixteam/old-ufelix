@extends('backend.layouts.app')
@section('css')
  <title>{{__('backend.Shipment_Tools')}} - {{__('backend.view')}}</title>
@endsection
@section('header')
  <div class="page-header">
    <h3>
      {{__('backend.view')}} -
      @if(session()->has('lang') == 'en')
        {{$shipment_tool->name_ar}}
      @else
        {{$shipment_tool->name_en}}
      @endif
    </h3> {!! $shipment_tool->status_span !!}
    <form action="{{ route('mngrAdmin.shipment_tools.destroy', $shipment_tool->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
      <input type="hidden" name="_method" value="DELETE">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="btn-group pull-right" role="group" aria-label="...">
        @if(permission('editShipmentTools')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
          <a class="btn btn-warning btn-group" role="group" href="{{ route('mngrAdmin.shipment_tools.edit', $shipment_tool->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
        @endif

        @if(permission('deleteShipmentTools')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
          <button type="submit" class="btn btn-danger"><i class="glyphicon glyphicon-trash"> {{__('backend.delete')}}</i></button>
        @endif
      </div>
    </form>
  </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
      <div class="form-group col-sm-6">
         <label for="title">{{__('backend.title')}}</label>
         <p class="form-control-static">{{$shipment_tool->name_en}}</p>
      </div>
      <div class="form-group col-sm-6">
         <label for="item_price">{{__('backend.Item_Price')}}</label>
         <p class="form-control-static">{{$shipment_tool->price}}</p>
      </div>
      <div class="form-group col-sm-6">
         <label for="description">{{__('backend.description')}}</label>
         <p class="form-control-static">{{$shipment_tool->description}}</p>
      </div>
      <div class="col-md-12 col-xs-12 col-sm-12">
        <a class="btn btn-link" href="{{ route('mngrAdmin.shipment_tools.index') }}"><i class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
      </div>
    </div>
  </div>
@endsection

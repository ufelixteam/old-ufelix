@extends('frontend.profile.layouts.app')

@section('title') @lang('website.changepassword') @endsection

@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12">
       <div class="card card-default">
			<div class="card-header">
				<h4 class="card-title m-b-0">@lang('website.changepassword') </h4>
			</div>
          	<div class="container-fluid">
             	<div class="row">
	                <div class="col-md-6 col-md-offset-3 col-xs-12">
                    @include('flash::message')

	                   <div class="ribbon-wrapper card">
							<form action="{{  app()->getLocale() == 'en' ? url('/en/customer/account/changepassword') : url('/customer/accountchangepassword')}}" method="post" name="loginForm" id="loginForm">
							  	{{csrf_field()}}

							    <div class="form-group  @if($errors->has('oldpassword')) has-error @endif">
							        <input type="password" id="oldpassword" name="oldpassword" value="" class="form-control" placeholder="@lang('website.oldpassword')" />
							        <i class="fas fa-lock prefix"></i>
							        @if($errors->has("oldpassword"))
							            <span class="help-block">{{ $errors->first("oldpassword") }}</span>
							        @endif

							    </div>
							    <div class="form-group  @if($errors->has('password')) has-error @endif">
							        <input type="password" id="password" name="password" value="" class="form-control" placeholder="@lang('website.Password')" />
							        <i class="fas fa-lock prefix"></i>
							        @if($errors->has("password"))
							            <span class="help-block">{{ $errors->first("password") }}</span>
							        @endif

							    </div>
								<div class="form-group  @if($errors->has('password_confirmation')) has-error @endif">
							        <input type="password" id="password_confirmation" name="password_confirmation" value="" class="form-control" placeholder="@lang('website.password_confirmation')" />
							        <i class="fas fa-lock prefix"></i>
							        @if($errors->has("password_confirmation"))
							            <span class="help-block">{{ $errors->first("password_confirmation") }}</span>
							        @endif

							    </div>
							    <div class="form-group text-center">
								    <button type="submit"  class="btn btn-success btn-rounded " > @lang('website.changepassword')</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection


  @section('scripts')
<script src="{{asset('/assets/scripts/jquery.validate.js')}}"></script>

<script>

    $(function(){
        $("form[name='loginForm']").validate({
            rules: {
                oldpassword: {
                    required: true,
                },
                password: {
                    required: true,
                },
                password_confirmation: {
                    required: true,
                    equalTo: '#password'
                }
            },
            // Specify validation error messages
            messages: {
            	oldpassword: {
            		required: "{{__('website.passwordLoginRequired')}}"
            	},
                password_confirmation:{
                    required: "{{__('backend.Confirm_password_is_wrong')}}",
                    equalTo: "{{__('backend.Confirm_password_is_wrong')}}",
                },
                password: {
                    required: "{{__('website.passwordLoginRequired')}}"
                }
            },

            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error)

            },
            highlight: function (element) {
                $(element).parent().addClass('error help-block');
                $(element).parent().find('label').addClass(' help-block');
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('error');
                $(element).parent().find('label').removeClass(' help-block');

            },

            submitHandler: function (form) {
                form.submit();
            }
        });
    })
  </script>
@endsection


<!-- Start About -->
<section id="about" class="about-x">
    <div class="container">
        <!-- Start Title -->
        <div class="col-xs-12">
            <div class="title">
   
                <h3><?php echo app('translator')->getFromJson('website.why_us'); ?></h3>
                <p>
                   <?php if(! empty($app_about) ): ?>  <?php echo e(app()->getLocale() == 'en' ? $app_about['why_us'] : $app_about['why_us_ar']); ?> <?php endif; ?>
                </p>
            </div>
        </div>
        <!-- End Title -->
        <!-- Start Block -->

        <?php if(! empty($why_us )): ?>
            <?php $__currentLoopData = $why_us; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-md-4 col-xs-12">
                    <div class="block-item">
                        <div class="icon-block">
                            <img src="<?php echo e($service->image); ?>" alt="" />
                        </div>
                        <div class="details-serv">
                            <h1><?php echo e($service->title); ?></h1>
                            <p>
                               <?php echo e($service->description); ?>

                            </p>
                        </div>
                    </div>


                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            <div class="col-xs-12">
                <a href="<?php echo e(app()->getLocale() == 'en' ? url('/en/services') : url('/services')); ?>" class="btn btn-style"><?php echo app('translator')->getFromJson('website.more'); ?></a>
            </div>
        <?php endif; ?>
    </div>
</section>
<!-- End About --><?php /**PATH /home/www/ufelix/ufelix/resources/views/frontend/home/why_us.blade.php ENDPATH**/ ?>
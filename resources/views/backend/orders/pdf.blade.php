<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ufelix</title>
    <!--<link rel="stylesheet" href="css/bootstrap.min.css">-->
    <!--<link rel="stylesheet" href="css/main.css">-->
    <style>
      .polica {
       --padding: 20px;
        text-transform: capitalize;
        --margin: 30px;
      }
      .head {
        border-bottom: 4px solid #343a40;
      }
      .head h4 {
        font-weight: bold;

      }
      .head button {
        border: none;
        box-shadow: none;
        outline: none;
        padding: .3rem 0.75rem !important;
      }
      .btn.focus, .btn:focus {
        border: none !important;
        box-shadow: none !important;
        outline: none !important;
      }
      #invoice-body {
        padding: 20px 10px 1px 10px;
        max-width: 100%;
        margin: 30px 60px 0 20px;
        box-shadow: 1px 1px 12px #343a40;
        border-radius: 5px;
      }
      #invoice-body .invoice-title {

      }
      .logo-ufelix .img-thumbnail {
        border: none;
      }
      .logo-ufelix .line
      {
        margin-top: 5px;
        margin-bottom: .5rem;
        border-top: 2px solid #e7e9ea;
        width: 95%;
      }
      .data
      {
        background-color: #f80;
        height: 40px;
        color: #fff;
        line-height: 40px;
        font-weight: bold;

      }
      .order_number
      {
        margin-left: 14px;
        margin-top: 10px;
        margin-bottom: 40px;
      }
      .order_number ul li
      {
        margin-bottom: 5px;
      }
      .customer-info
      {
        border: 1px solid #f80;
        margin: 10px;
        width: 97%;
      }
      .customer-info > div {
        border: 2px solid #f80;
        padding: 15px;

      }
      #table td, #table th{
        font-size: 13px;
        border-top: 1px solid #ff8800;
        text-align: center;
      }

      /*bootstrab class*/
      *,
      *::before,
      *::after {
        box-sizing: border-box;
      }

      html {
        font-family: sans-serif;
        line-height: 1.15;
        -webkit-text-size-adjust: 100%;
        -ms-overflow-style: scrollbar;
        -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
      }


      article, aside, figcaption, figure, footer, header, hgroup, main, nav, section {
        display: block;
      }

      body {
        margin: 0;
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, Noto Sans, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #212529;
        text-align: left;
        background-color: #fff;
      }
      .list-inline {
        padding-left: 0;
        list-style: none;
      }
      .row {
        margin-right: -15px;
        margin-left: -15px;
      }
      .row-no-gutters {
        margin-right: 0;
        margin-left: 0;
      }
      .row-no-gutters [class*="col-"] {
        padding-right: 0;
        padding-left: 0;
      }
      .col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col,
      .col-auto, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm,
      .col-sm-auto, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .col-md,
      .col-md-auto, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg,
      .col-lg-auto, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl,
      .col-xl-auto {
        position: relative;
        width: 100%;
        padding-right: 15px;
        padding-left: 15px;
      }
      .img-thumbnail {
        /* padding: 0.25rem; */
        background-color: #fff;
        border: 1px solid #dee2e6;
        border-radius: 0.25rem;
        max-width: 65%;
        /* height: auto; */
        height: 85px;
        margin: 12px 0 0 38px;
      }
      .d-inline {
        display: inline !important;
      }
      h1, h2, h3, h4, h5, h6 {
        margin-top: 0;
        margin-bottom: 0.5rem;
      }
      h1, h2, h3, h4, h5, h6,
      .h1, .h2, .h3, .h4, .h5, .h6 {
        margin-bottom: 0.5rem;
        font-family: inherit;
        font-weight: 500;
        line-height: 1.2;
        color: inherit;
      }

      h1, .h1 {
        font-size: 2.5rem;
      }

      h2, .h2 {
        font-size: 2rem;
      }

      h3, .h3 {
        font-size: 1.75rem;
      }

      h4, .h4 {
        font-size: 1.5rem;
      }

      h5, .h5 {
        font-size: 1.25rem;
      }

      h6, .h6 {
        font-size: 1rem;
      }

      .text-right {
        text-align: right !important;
      }

      .text-center {
        text-align: center !important;
      }
      .table {
        border-collapse: collapse !important;
      }
      .table td,
      .table th {
        background-color: #fff !important;
        padding: .75rem;
      }
      .table-bordered thead td, .table-bordered thead th {
        border-bottom-width: 2px;
      }
      .btn-primary {
        color: #fff;
        background-color: #007bff;
        border-color: #007bff;
      }
      .btn-secondary:focus, .btn-secondary.focus {
        box-shadow: 0 0 0 0.2rem rgba(130, 138, 145, 0.5);
      }

      .btn-secondary.disabled, .btn-secondary:disabled {
        color: #fff;
        background-color: #6c757d;
        border-color: #6c757d;
      }
      .btn-secondary {
        color: #fff;
        background-color: #6c757d;
        border-color: #6c757d;
      }

      .table thead th {
        vertical-align: bottom;
        border-bottom: 2px solid #e9ecef;
      }
      .table-bordered {
        border: 1px solid #dee2e6;
      }

      .table-bordered th,
      .table-bordered td {
        border: 1px solid #dee2e6;
      }

      .table-bordered thead th,
      .table-bordered thead td {
        border-bottom-width: 2px;
      }
      .d-inline {
        display: inline !important;
      }
      .col-sm-1 {
        -ms-flex: 0 0 8.333333%;
        flex: 0 0 8.333333%;
        max-width: 8.333333%;
      }
      .col-sm-2 {
        -ms-flex: 0 0 16.666667%;
        flex: 0 0 16.666667%;
        max-width: 16.666667%;
      }
      .col-sm-3 {
        -ms-flex: 0 0 25%;
        flex: 0 0 25%;
        max-width: 25%;
      }
      .col-sm-4 {
        -ms-flex: 0 0 33.333333%;
        flex: 0 0 33.333333%;
        max-width: 33.333333%;
      }
      .col-sm-5 {
        -ms-flex: 0 0 41.666667%;
        flex: 0 0 41.666667%;
        max-width: 41.666667%;
      }
      .col-sm-6 {
        -ms-flex: 0 0 50%;
        flex: 0 0 50%;
        max-width: 50%;
      }
      .col-sm-7 {
        -ms-flex: 0 0 58.333333%;
        flex: 0 0 58.333333%;
        max-width: 58.333333%;
      }
      .col-sm-8 {
        -ms-flex: 0 0 66.666667%;
        flex: 0 0 66.666667%;
        max-width: 66.666667%;
      }
      .col-sm-9 {
        -ms-flex: 0 0 75%;
        flex: 0 0 75%;
        max-width: 75%;
      }
      .col-sm-10 {
        -ms-flex: 0 0 83.333333%;
        flex: 0 0 83.333333%;
        max-width: 83.333333%;
      }
      .col-sm-11 {
        -ms-flex: 0 0 91.666667%;
        flex: 0 0 91.666667%;
        max-width: 91.666667%;
      }
      .col-sm-12 {
        -ms-flex: 0 0 100%;
        flex: 0 0 100%;
        max-width: 100%;
      }
      .container {
        min-width: 992px !important;
      }
      .container {
        width: 100%;
        padding-right: 15px;
        padding-left: 15px;
        margin-right: auto;
        margin-left: auto;
      }

      @media (min-width: 576px) {
        .container {
          max-width: 540px;
        }
      }

      @media (min-width: 768px) {
        .container {
          max-width: 720px;
        }
      }

      @media (min-width: 992px) {
        .container {
          max-width: 960px;
        }
      }

      @media (min-width: 1200px) {
        .container {
          max-width: 1140px;
        }
      }

      .row {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        /* margin-right: -15px;
        margin-left: -15px; */
        margin: 0 -15px 5px;
      }

      @media print {
          #print {
            visibility: hidden;
          }

      }

    </style>
  </head>
  <body>
  <div class="polica">
    <div class="container">
      <div class="head">
        <div class="row">
          <div class="col-sm-4">
            <h4>print your documents</h4>
          </div>
          <div class="col-sm-5"></div>
          <div class="col-sm-3 text-right">
            <div class="form-group">
              <button type="button" class="btn btn-secondary" id="print"><i class="fa fa-print"></i> Print</button>

            </div>
          </div>
        </div>
      </div>

      <div id="invoice-body" style="position: relative">
        <div class="invoice-title">
          <h4 class="" style="background-color: #f80; height: 40px; color: #fff; line-height: 40px; font-weight: bold; width: 17%; text-align: center; /* text-transform: capitalize; */ position: relative; top: -31px; border-radius: 10px 0;">Invoice</h4>
          <h4 class="" style="background-color: #f80;color: #fff;font-weight: bold; width: auto; text-align: center;position: absolute; top: 0;right: 18px; padding: 6px;font-size: 16px; text-transform: lowercase;">www.ufelix.com</h4>
          <!-- start logo ufelix-->
          <div class="row logo-ufelix">
            <div class="col-sm-4 ">
              <img class="img-thumbnail" src="{{asset('/assets/logo2.png')}}" alt="" style="height:75px;">
            </div>
            <div class="col-sm-2"></div>
            <div class="col-sm-6">
              <h4 class="text-right" style="margin-top: 50px; margin-right: 10px; font-weight: bold;">Delivery Receipt</h4>
            </div>
            <hr class="line">
          </div>
          <!-- end logo ufelix-->

          <!--start address section-->

          <div class="row address">
            <div class="col-sm-9">
              <div class="order_number">
                <ul class="list-inline">

                  <li style="padding: 10px 0 0 0;">
                    <h6 class="d-inline" style="margin-right: 11px;">Order Number :</h6><span>{{ ! empty($order) ? $order->order_number : ''}}</span> <br>
                  </li>
                  <li>
                    <h6 class="d-inline" style="margin-right: 11px;">Invoice Number :</h6><span>25501388</span> <br>
                  </li>
                  <li>
                    {{--<h6 class="d-inline" style="margin-right: 11px;">Customer address :</h6> <span>{{ ! empty($order) ? $order->sender_name : ''}}</span>--}}
                    <h6 class="d-inline" style="margin-right: 11px;">Customer Name :</h6> <span>{{ ! empty($corporteName) ? $corporteName : ''}}</span>
                  </li>
                  <!-- <li>
                    <p class="d-inline" style="margin-right: 11px;">{{ ! empty($order) ? $order->sender_address : ''}}</p>
                  </li> -->
                    <li>
                        <h6 class="d-inline" style="margin-right: 11px;">Customer Phone :</h6>
                        <span>{{ ! empty($order) ? $order->sender_mobile : ''}}</span> <br>
                    </li>
                </ul>
              </div>
            </div>
              <div class="col-sm-3 data">
                  <h6 class="d-inline" style="margin-right: 11px;">Date:</h6>
                  <span> {{ ! empty($order) ? date('d M Y', strtotime($order->created_at)) : ''}}</span>

                  <img class="img-thumbnail"
                       src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(200)->generate(json_encode(['order_id'=>$order->id, 'sender_code' => $order->order_number]))) !!} "
                       alt="" style="height: 125px;">

              </div>
          </div>

            <!--end address ufelix section-->

            <!--customer info -->
            <div class="row customer-info" style="margin: 10px 0 0 10px;">
                <div class="col-sm-4">
                    <ul class="list-inline" style="font-size: 10px">
                <li style="">
                  <h6 class="d-inline" style="margin-right: 11px;">Receivered Date :</h6>
                </li>
                <li style="">
                  <p class="d-inline" style="margin-right: 11px; font-size: 13px;">- {{ ! empty($order) ? $order->receiver_name : ''}}</p>
                </li>

                <li>
                  <p class="d-inline" style="margin-right: 11px; font-size: 13px;">- {{ ! empty($order) ? $order->receiver_address : ''}}</p>
                </li>

              </ul>
            </div>
            <div class="col-sm-4" style="font-size: 10px">
              <h6 class="d-inline" style="margin-right: 11px;">Receiver mobile : </h6> <span style="font-size: 13px;">{{ ! empty($order) ? $order->receiver_mobile : ''}}</span>
            </div>
            <div class="col-sm-4">
              <ul class="list-inline" style="font-size: 10px">
                <li style="">
                  <h6 class="d-inline" style="margin-right: 11px;">Notes : </h6>
                </li>
                <li style="">
                  <p class="d-inline" style="margin-right: 11px; font-size: 12px;">
                    {{ ! empty($order) ? $order->notes : ''}}
                   </p>
                </li>

              </ul>
            </div>
          </div>
          <!--customer info -->

          <table class="table" id="table" style="border: 1px solid #f80;margin: 10px;     width: 97%;">
            <thead>
              <tr>
{{--                <th>Type</th>--}}
                <th>Type Name</th>
                <th>Total Price</th>

              </tr>
            </thead>
            <tbody>
              <tr>
{{--                <td>{{ ! empty($order) && ! empty($order->types) ? $order->types->name_en : ''}}</td>--}}
                <td>{{ ! empty($order)  ? $order->type : ''}}</td>
                <td>
                    {{ ! empty($order) ? $order->total_price: ''}}
                    {{--                  @if(! empty($order) && $order->payment_method_id == 1)--}}

                    {{--                    {{ ! empty($order) ?  $order->delivery_price + ($order->overload * 5) : ''}}--}}

                    {{--                  @elseif(! empty($order) && ($order->payment_method_id == 3 || $order->payment_method_id == 4 ))--}}

                    {{--                    {{ ! empty($order) ? $order->order_price + $order->delivery_price + ($order->overload * 5): ''}}--}}

                    {{--                  @elseif(! empty($order) && $order->payment_method_id == 6)--}}

                    {{--                    {{ ! empty($order) ? $order->order_price: ''}}--}}

                    {{--                  @elseif(! empty($order) && $order->payment_method_id == 5)--}}
                    {{--                    0--}}
                    {{--                  @endif--}}

                </td>

              </tr>
            </tbody>
          </table>
        </div>
        <p class="text-center lead" style="text-transform: lowercase;">www.ufelix.com</p>

      </div>
    </div>
  </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script>
      $(function() {
           window.print();
           // window.history.back();
          });

          $(function() {
              document.querySelector("#print").addEventListener("click", function() {
               window.print();
               window.history.back();
              });
          });

    </script>
  <!-- script -->

  </body>
</html>

@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.Vehicles_Model')}}</title>
@endsection

@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.Vehicles_Model')}}
            @if(permission('addVehicleModel')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
              <a class="btn btn-success pull-right" href="{{ route('mngrAdmin.model_types.create') }}"><i class="glyphicon glyphicon-plus"></i> {{__('backend.create')}}</a>
            @endif
        </h3>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            @if($model_types->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('backend.name')}}</th>
                            <th>{{__('backend.status')}}</th>
                            <th class="text-right"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($model_types as $model_type)
                            <tr>
                                <td>{{$model_type->id}}</td>
                                <td>{{$model_type->name}}</td>
                                <td>{!! $model_type->status_span !!}</td>
                                <td class="text-right">
                                    {{--<a class="btn btn-xs btn-primary" href="{{ route('mngrAdmin.model_types.show', $model_type->id) }}"><i class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>--}}
                                    @if(permission('editVehicleModel')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                      <a class="btn btn-xs btn-warning" href="{{ route('mngrAdmin.model_types.edit', $model_type->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                                    @endif

                                    @if(permission('deleteVehicleModel')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                      <form action="{{ route('mngrAdmin.model_types.destroy', $model_type->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
                                          <input type="hidden" name="_method" value="DELETE">
                                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                          <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}</button>
                                      </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {!! $model_types->appends($_GET)->links() !!}

            @else
                <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
            @endif
        </div>
    </div>
@endsection

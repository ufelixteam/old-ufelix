<?php

namespace App\Http\Controllers\Backend;

use App\Events\OrderUpdated;
use App\Http\Controllers\Controller\Backend;
use App\Models\AcceptedOrder;
use App\Models\CollectionOrder;
use App\Models\Corporate;
use App\Models\Customer;
use App\Models\CustomerPrice;
use App\Models\Driver;
use App\Models\Governorate;
use App\Models\GovernorateKeyword;
use App\Models\GovernoratePrice;
use App\Models\Order;
use App\Models\OrderComment;
use App\Models\OrderDeliveryProblem;
use App\Models\RefundCollection;
use App\Models\ScanCollection;
use App\Models\Setting;
use App\Models\Store;
use App\Models\User;
use Carbon\Carbon;
use Config;
use ExcelReport;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use PdfReport;

//use Elibyy\TCPDF\Facades\TCPDF;
class ReportController extends Controller
{
    private $guardType;

    public function __construct(Request $request)
    {
        if ($request->is('mngrAdmin/*') || $request->is('admin/*')) {
            Config::set('auth.defaults.guard', 'admin');

        } elseif ($request->is('mngrAgent/*') || $request->is('agent/*')) {
            Config::set('auth.defaults.guard', 'agent');
        }
        $this->guardType = Config::get('auth.defaults.guard');
    }

    public function show()
    {
        return view('backend.reports.index');
    }

    public function corporates_report(Request $request)
    {

        $name = '';
        if (isset($request->user_id)) {
            $name = User::where('id', $request->user_id)->value('name');
        }

        Excel::create(str_replace(' ', '_', $name) . '_' . date('Y_m_d'), function ($excel) use ($request) {

            $type = 1;

            $orders = Order::with(['payment_method', 'to_government', 'to_city', 'delivery_problems' => function ($query) {
                $query->orderBy('created_at', 'DESC');
            }, 'order_delay' => function ($query) {
                $query->orderBy('created_at', 'DESC');
            }, 'comments' => function ($query) {
                $query->orderBy('created_at', 'DESC');
            }, 'customer' => function ($query) {
                $query->with('Corporate');
            }])
                ->join('customers', 'customers.id', '=', 'orders.customer_id')
                ->join('corporates', 'corporates.id', '=', 'customers.corporate_id')
                ->where('responsible_id', $request->user_id)
                ->select('orders.*')
                ->withCount('comments')
                ->orderBy('orders.created_at');

            $excel->setTitle('Orders');

            $excel->sheet('Orders', function ($sheet) use ($orders, $type) {
                $sheet->loadView('backend.reports.orders')->with(['orders' => $orders, 'type' => $type]);
            });

        })->download('xls');
    }

    public function daily_report(Request $request)
    {

        $name = '';
        if (isset($request->corporate_id)) {
            $name = Corporate::where('id', $request->corporate_id)->value('name');
        }

        if (isset($request->customer_id)) {
            $name = Customer::where('id', $request->customer_id)->value('name');
        }

        $is_dashboard = $request->is('mngrAdmin/*');
        $customer = '';

        if (!$is_dashboard) {
            $customer = Auth::guard('Customer')->user();
        }

        Excel::create(str_replace(' ', '_', $name) . '_' . date('Y_m_d'), function ($excel) use ($request, $is_dashboard, $customer) {

            $type = '';

            $orders = Order::with(['payment_method', 'to_government', 'to_city', 'delivery_problems' => function ($query) {
                $query->orderBy('created_at', 'DESC');
            }, 'order_delay' => function ($query) {
                $query->orderBy('created_at', 'DESC');
            }, 'comments' => function ($query) {
                $query->orderBy('created_at', 'DESC');
            }, 'customer' => function ($query) {
                $query->with('Corporate');
            }])
                ->select('orders.*')
                ->withCount('comments')
                ->orderBy('orders.created_at');

            if (isset($request->corporate_id)) {
                $type = 1;
                $orders->join('customers', 'customers.id', '=', 'orders.customer_id')
                    ->where('corporate_id', $request->corporate_id);
            }

            if (isset($request->customer_id)) {
                if (!$is_dashboard && $customer->is_manager) {
                    $type = 1;
                    $orders->join('customers', 'customers.id', '=', 'orders.customer_id')
                        ->where('corporate_id', $customer->corporate_id);
                } else {
                    $type = 2;
                    $orders->where('customer_id', $request->customer_id);
                }
            }

            $excel->setTitle('Orders');

            $excel->sheet('Orders', function ($sheet) use ($orders, $type) {
                $sheet->loadView('backend.reports.orders')->with(['orders' => $orders, 'type' => $type]);
            });

        })->download('xls');
    }

    public function profile_daily_report(Request $request)
    {

        $name = '';
        $customer = '';
//        if (isset($request->corporate_id)) {
//            $name = Corporate::where('id', $request->corporate_id)->value('name');
//        }

        if ($request->filled('customer_id')) {
            $customer = Customer::find($request->customer_id);
            $name = $customer->name;
        } else {
            $customer = Auth::guard('Customer')->user();
            $name = $customer->name;
        }

        $settings = Setting::where('key', 'disable_real_date')->value('value');

//        $is_dashboard = $request->is('mngrAdmin/*');

//        if (!$is_dashboard) {
//            $customer = Auth::guard('Customer')->user();
//        }

        Excel::create(str_replace(' ', '_', $name) . '_' . date('Y_m_d'), function ($excel) use ($request/*, $is_dashboard*/, $customer, $settings) {

            $type = '';

            $orders = Order::with(['payment_method', 'to_government', 'to_city', 'delivery_problems' => function ($query) {
                $query->orderBy('created_at', 'DESC');
            }, 'order_delay' => function ($query) {
                $query->orderBy('created_at', 'DESC');
            }, 'comments' => function ($query) {
                $query->orderBy('created_at', 'DESC');
            }, 'customer' => function ($query) {
                $query->with('Corporate');
            }])
                ->select('orders.*')
                ->withCount(['order_delay', 'delivery_problems', 'comments'])
                ->orderBy('orders.created_at');

//            if (isset($request->corporate_id)) {
//                $type = 1;
//                $orders->join('customers', 'customers.id', '=', 'orders.customer_id')
//                    ->where('corporate_id', $request->corporate_id);
//            }

            if ($request->filled('customer_id')) {
                $type = 1;
                $orders->where('customer_id', $request->customer_id);
            } elseif ($customer->is_manager) {
                $type = 2;
                $orders->join('customers', 'customers.id', '=', 'orders.customer_id')
                    ->where('corporate_id', $customer->corporate_id);
            } else {
                $type = 2;
                $orders->where('customer_id', $customer->id);
            }

//            if (isset($request->customer_id)) {
//                if (!$is_dashboard && $customer->is_manager) {
//                    $type = 1;
//                    $orders->join('customers', 'customers.id', '=', 'orders.customer_id')
//                        ->where('corporate_id', $customer->corporate_id);
//                } else {
//                    $type = 2;
//                    $orders->where('customer_id', $request->customer_id);
//                }
//            }

            if ($request->filled('from_date')) {
                $orders = $orders->whereDate('orders.created_at', '>=', $request->from_date);
            }

            if ($request->filled('to_date')) {
                $orders = $orders->whereDate('orders.created_at', '<=', $request->to_date);
            }

            if ($request->filled('status') && is_array($request->status)) {
                $status = $request->status;
                if ($settings) {
                    $isReceived = false;
                    $isDelivered = false;
                    $orders = $orders->where(function ($query) use ($status, $isReceived, $isDelivered) {
                        if (($i = array_search(2, $status)) !== false) {
                            unset($status[$i]);
                            $isReceived = true;
                        }

                        if (($key = array_search(3, $status)) !== false) {
                            unset($status[$key]);
                            $isDelivered = true;
                        }

                        if (count($status)) {
                            $query->whereIn('orders.status', $status);
                        }

                        if ($isReceived) {
                            $query->orWhere(function ($query) {
                                $query->where('orders.status', 2)
                                    ->orWhere(function ($query) {
                                        $query->where('orders.status', 3)
                                            ->whereIn('orders.r_government_id', [1, 2])
                                            ->whereRaw('TIMESTAMPDIFF(SECOND, received_at, NOW()) < 172800');
                                    })
                                    ->orWhere(function ($query) {
                                        $query->where('orders.status', 3)
                                            ->whereNotIn('orders.r_government_id', [1, 2])
                                            ->whereRaw('TIMESTAMPDIFF(SECOND, received_at, NOW()) < 331200');
                                    });
                            });
                        }

                        if ($isDelivered) {
                            $query->orWhere(function ($query) {
                                $query->orWhere(function ($query) {
                                    $query->where('orders.status', 3)
                                        ->whereIn('orders.r_government_id', [1, 2])
                                        ->whereRaw('TIMESTAMPDIFF(SECOND, received_at, NOW()) >= 172800');
                                })
                                    ->orWhere(function ($query) {
                                        $query->where('orders.status', 3)
                                            ->whereNotIn('orders.r_government_id', [1, 2])
                                            ->whereRaw('TIMESTAMPDIFF(SECOND, received_at, NOW()) >= 331200');
                                    });
                            });
                        }

                    });
                } else {
                    $orders = $orders->whereIn('orders.status', $status);
                }
            }

            $excel->setTitle('Orders');

            $excel->sheet('Orders', function ($sheet) use ($orders, $type, $settings) {
                $sheet->loadView('backend.reports.profile_orders')->with(['orders' => $orders, 'type' => $type, 'settings' => $settings]);
            });

        })->download('xls');
    }

    public function captain_report(Request $request)
    {

        $name = '';
        if (isset($request->driver_id)) {
            $name = Driver::where('id', $request->driver_id)->value('name');
        }

        Excel::create(str_replace(' ', '_', $name) . '_' . date('Y_m_d'), function ($excel) use ($request) {
            $ownDrivers = Driver::where(function ($query) use ($request) {
                $query->where('id', $request->driver_id)
                    ->orWhere('manager_id', $request->driver_id);
            })->pluck('id')->toArray();

            $lastOrder = AcceptedOrder::selectRaw('status as order_status')
                ->whereIn('driver_id', $ownDrivers)
                ->where('status', '!=', 4)
                ->whereColumn('order_id', 'orders.id')
                ->latest()
                ->limit(1)
                ->getQuery();

            $orders = Order::with(['customer' => function ($query) {
                $query->with('corporate');
            }])
                ->join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
                ->with(['to_government', 'to_city', 'delivery_problems' => function ($query) {
                    $query->orderBy('created_at', 'DESC');
                }, 'order_delay' => function ($query) {
                    $query->orderBy('created_at', 'DESC');
                }, 'comments' => function ($query) {
                    $query->orderBy('created_at', 'DESC');
                }, 'oneDriver' => function ($query) {
                    $query->with('manager');
                }])
                ->select('orders.*')
                ->selectSub($lastOrder, 'order_status')
                ->where('accepted_orders.status', '!=', 4)
                ->whereIn('accepted_orders.driver_id', $ownDrivers)
                ->groupBy('order_id')
                ->orderBy('orders.created_at');

            $excel->setTitle('Orders');

            $excel->sheet('Orders', function ($sheet) use ($orders) {
                $sheet->loadView('backend.reports.captain-orders')->with(['orders' => $orders]);
            });

        })->download('xls');
    }

    public function pickups(Request $request)
    {
        Excel::create('pickups-' . time(), function ($excel) use ($request) {

            $type = '';

            $orders = $orders = Order::with(['payment_method', 'to_government', 'to_city', 'delivery_problems' => function ($query) {
                $query->orderBy('created_at', 'DESC');
            }, 'order_delay' => function ($query) {
                $query->orderBy('created_at', 'DESC');
            }, 'comments' => function ($query) {
                $query->orderBy('created_at', 'DESC');
            }, 'customer' => function ($query) {
                $query->with('Corporate');
            }])
                ->select('orders.*')
                ->withCount('comments')
                ->orderBy('orders.created_at');

            if (isset($request->corporate_id)) {
                $type = 1;
                $orders->join('customers', 'customers.id', '=', 'orders.customer_id')
                    ->where('corporate_id', $request->corporate_id);
            }

            if (isset($request->customer_id)) {
                $type = 2;
                $orders->where('customer_id', $request->customer_id);
            }

            $excel->setTitle('Orders');

            $excel->sheet('Orders', function ($sheet) use ($orders, $type) {
                $sheet->loadView('backend.reports.orders')->with(['orders' => $orders, 'type' => $type]);
            });

        })->download('xls');
    }

    public function print_police(Request $request)
    {

        $ids = is_array($request->ids) ? $request->ids : explode(",", $request->ids);

        if (is_array($ids) && count($ids)) {
            $orders = Order::with(['payment_method', 'to_government', 'to_city', 'warehouse', 'customer' => function ($query) {
                $query->with(['Corporate' => function ($query) {
                    $query->with('manager');
                }]);
            }])
                ->whereIn('id', $ids)
                ->orderBy('receiver_name', 'ASC')
                ->get();

            foreach ($orders as $order) {
                if (!empty($order->customer->Corporate->manager->first())) {
                    $order->corporteName = $order->customer->name;
                } else {
                    $order->corporteName = $order->customer->Corporate->name;
                }
//                $customer = Customer::where('id', $order->customer_id)->value('corporate_id');
//                $order->corporteName = $order->customer->name; //Corporate::where('id', $customer)->value('name');
            }
            $data = ['orders' => $orders];
            $pdf = PDF::loadView('backend.orders.pdf-orders', $data, [], ['format' => 'A5-L']);
            return $pdf->stream(time());
        } else {
            //error
            flash(__('backend.Must_Choose_Orders_To_Print'))->error();
            redirect()->back();
        }


    }

    public function print_refund(Request $request)
    {
        $refund_collection_id = app('request')->input('refund_collection_id');
        if ($refund_collection_id) {
            $refund_collection = RefundCollection::withCount(['orders'])
                ->with(['orders' => function ($query) {
                    $query->with(['from_government', 'to_government', 'warehouse']);
                }, 'customer' => function ($query) {
                    $query->with(['corporate', 'governorate']);
                }])
                ->find($refund_collection_id);
            $pdf = PDF::loadView('backend.orders.print_refund_collection', compact('refund_collection'), [], ['format' => 'A4-L']);
            return $pdf->stream(time());
        }
    }

    public function refund_excel(Request $request)
    {

        $ids = explode(',', app('request')->input('ids'));
        if (is_array($ids) && count($ids)) {
            $sheet_name = 'Refund_Collections_' . time();
            Excel::create($sheet_name, function ($excel) use ($ids, $sheet_name) {
                $excel->sheet($sheet_name, function ($sheet) use ($ids) {
                    $data = [];

                    $orders = Order::leftJoin('governorates', 'governorates.id', '=', 'orders.r_government_id')
                        ->leftJoin('stores', 'stores.id', '=', 'orders.moved_in')
                        ->select('governorates.name_en as governorate', 'stores.name as warehouse', 'orders.*')
                        ->whereIn('refund_collection_id', $ids)
                        ->get();

                    foreach ($orders as $order) {
                        $date = '';
                        if ($order->status == 0) {
                            $date = $order->created_at;
                        } else if ($order->status == 1) {
                            $date = $order->accepted_at;
                        } else if ($order->status == 2) {
                            $date = $order->received_at;
                        } else if ($order->status == 3) {
                            $date = $order->delivered_at;
                        } else if ($order->status == 4) {
                            $date = $order->cancelled_at;
                        } else if ($order->status == 5) {
                            $date = $order->recalled_at;
                        } else if ($order->status == 7) {
                            $date = $order->dropped_at;
                        } else if ($order->status == 8) {
                            $date = $order->rejected_at;
                        }

                        $date = date('Y-m-d H:i:s', strtotime($date));

                        $status = "";
                        if ($order->status == 5) {
                            $status = 'Full Recalled';
                        } else if ($order->status == 4) {
                            $status = 'Cancelled';
                        } else if ($order->status == 3 && $order->delivery_status == 2) {
                            $status = 'Part Delivered';
                        } else if ($order->status == 3 && $order->delivery_status == 4) {
                            $status = 'Replace';
                        }

                        $warehouse_date = date('Y-m-d H:i:s', strtotime($order->warehouse_dropoff_date));
                        $client_date = date('Y-m-d H:i:s', strtotime($order->client_dropoff_date));

                        $data[] = array(
                            $order->order_number,
                            $order->governorate,
                            $order->receiver_name,
                            date('Y-m-d H:i:s', strtotime($order->created_at)),
                            $date,
                            $warehouse_date,
                            $client_date,
                            $status,
                            $order->warehouse
                        );
                    }

                    $headings = array('Sender Code', 'To', 'Name', 'Created date', 'Last updated date', 'Warehouse Date', 'Client Date', 'Type', 'Warehouse');

                    $sheet->fromArray($data, null, 'A1', false, false);
                    $sheet->prependRow(1, $headings);

                });
            })->export('xls');
        }

//
//        $refund_collection_id = app('request')->input('refund_collection_id');
//        $refund_collection = RefundCollection::withCount(['orders'])
//            ->with(['customer' => function ($query) {
//                $query->with(['corporate', 'governorate']);
//            }])
//            ->find($refund_collection_id);
//        if ($refund_collection) {
//            $meta = [
//                'Duration' => $refund_collection->start_date . ' To ' . $refund_collection->end_date,
//                'Serial Code' => $refund_collection->serial_code,
//                'Corporate' => $refund_collection->customer->Corporate->name . ' / ' . $refund_collection->customer->name,
//                'Address' => (isset($refund_collection->customer->address) ? $refund_collection->customer->address : '-'),
//            ];
//            $title = "Returned Collection Orders";
//
//            $queryBuilder = Order::leftJoin('governorates', 'governorates.id', '=', 'orders.r_government_id')
//                ->leftJoin('stores', 'stores.id', '=', 'orders.moved_in')
//                ->select('governorates.name_en as governorate', 'stores.name as warehouse', 'orders.*')
//                ->where('refund_collection_id', $refund_collection_id);
//
//            $columns = [
//                'Sender Code' => 'order_number',
//                'To' => 'governorate',
//                'Name' => 'receiver_name',
//                'Created date' => function ($order) {
//                    return date('Y-m-d H:i:s', strtotime($order->created_at));
//                },
//                'Last updated date' => function ($order) {
//                    $date = '';
//                    if ($order->status == 0) {
//                        $date = $order->created_at;
//                    } else if ($order->status == 1) {
//                        $date = $order->accepted_at;
//                    } else if ($order->status == 2) {
//                        $date = $order->received_at;
//                    } else if ($order->status == 3) {
//                        $date = $order->delivered_at;
//                    } else if ($order->status == 4) {
//                        $date = $order->cancelled_at;
//                    } else if ($order->status == 5) {
//                        $date = $order->recalled_at;
//                    }
//                    return date('Y-m-d H:i:s', strtotime($date));
//                },
//                'Type' => function ($order) {
//                    if ($order->status == 5) {
//                        return 'Full Recalled';
//                    } else {
//                        return 'Part Delivered';
//                    }
//                },
//                'Warehouse' => 'warehouse'
//            ];
//
//            return ExcelReport::of($title, $meta, $queryBuilder, $columns)
//                ->setCss([
//                    '.bolder' => 'font-weight: 800;',
//                    '.italic-red' => 'color: red;font-style: italic;'
//                ])->download('refund_collection-' . date('Y-m-d'));
//        }
    }

//    public function refund_excel(Request $request)
//    {
//
//        $ids = explode(',', app('request')->input('ids'));
//        if (is_array($ids) && count($ids)) {
//            Excel::create('Refund_Collections_' . time(), function ($excel) use ($ids) {
//                foreach ($ids as $id) {
//                    $refund_collection = RefundCollection::withCount(['orders'])
//                        ->with(['customer' => function ($query) {
//                            $query->with(['corporate', 'governorate']);
//                        }])
//                        ->find($id);
//
//                    $sheet_name = $refund_collection->serial_code;
//
//                    $excel->sheet($sheet_name, function ($sheet) use ($id, $refund_collection) {
//                        $data = [];
//
//                        $orders = Order::leftJoin('governorates', 'governorates.id', '=', 'orders.r_government_id')
//                            ->leftJoin('stores', 'stores.id', '=', 'orders.moved_in')
//                            ->select('governorates.name_en as governorate', 'stores.name as warehouse', 'orders.*')
//                            ->where('refund_collection_id', $id)
//                            ->get();
//
//                        foreach ($orders as $order) {
//                            $date = '';
//                            if ($order->status == 0) {
//                                $date = $order->created_at;
//                            } else if ($order->status == 1) {
//                                $date = $order->accepted_at;
//                            } else if ($order->status == 2) {
//                                $date = $order->received_at;
//                            } else if ($order->status == 3) {
//                                $date = $order->delivered_at;
//                            } else if ($order->status == 4) {
//                                $date = $order->cancelled_at;
//                            } else if ($order->status == 5) {
//                                $date = $order->recalled_at;
//                            }
//                            $date = date('Y-m-d H:i:s', strtotime($date));
//
//                            $status = "";
//                            if ($order->status == 5) {
//                                $status = 'Full Recalled';
//                            } else {
//                                $status = 'Part Delivered';
//                            }
//
//                            $data[] = array(
//                                $order->order_number,
//                                $order->governorate,
//                                $order->receiver_name,
//                                date('Y-m-d H:i:s', strtotime($order->created_at)),
//                                $date,
//                                $status,
//                                $order->warehouse
//                            );
//                        }
//
//                        $headings = array('Sender Code', 'To', 'Name', 'Created date', 'Last updated date', 'Type', 'Warehouse');
//
//                        $sheet->fromArray($data, null, 'A1', false, false);
//                        $sheet->prependRow(1, $headings);
//                        $sheet->prependRow(1, []);
//                        $sheet->prependRow(1, [
//                            "Duration", $refund_collection->start_date . ' To ' . $refund_collection->end_date
//                        ]);
//                        $sheet->prependRow(1, [
//                            "Serial Code", $refund_collection->serial_code
//                        ]);
//                        $sheet->prependRow(1, [
//                            "Corporate", $refund_collection->customer->Corporate->name . ' / ' . $refund_collection->customer->name
//                        ]);
//                        $sheet->prependRow(1, [
//                            "Address",  (isset($refund_collection->customer->address) ? $refund_collection->customer->address : '-')
//                        ]);
//
//                        $sheet->mergeCells('B1:G1');
//                        $sheet->mergeCells('B2:G2');
//                        $sheet->mergeCells('B3:G3');
//                        $sheet->mergeCells('B4:G4');
//
////                        $sheet->cells('A1:D1', function ($cell) {
////
////                            $cell->setBackground('#58a3ee');
////                        });
//
//                    });
//                }
//            })->export('xls');
//        }
//
////
////        $refund_collection_id = app('request')->input('refund_collection_id');
////        $refund_collection = RefundCollection::withCount(['orders'])
////            ->with(['customer' => function ($query) {
////                $query->with(['corporate', 'governorate']);
////            }])
////            ->find($refund_collection_id);
////        if ($refund_collection) {
////            $meta = [
////                'Duration' => $refund_collection->start_date . ' To ' . $refund_collection->end_date,
////                'Serial Code' => $refund_collection->serial_code,
////                'Corporate' => $refund_collection->customer->Corporate->name . ' / ' . $refund_collection->customer->name,
////                'Address' => (isset($refund_collection->customer->address) ? $refund_collection->customer->address : '-'),
////            ];
////            $title = "Returned Collection Orders";
////
////            $queryBuilder = Order::leftJoin('governorates', 'governorates.id', '=', 'orders.r_government_id')
////                ->leftJoin('stores', 'stores.id', '=', 'orders.moved_in')
////                ->select('governorates.name_en as governorate', 'stores.name as warehouse', 'orders.*')
////                ->where('refund_collection_id', $refund_collection_id);
////
////            $columns = [
////                'Sender Code' => 'order_number',
////                'To' => 'governorate',
////                'Name' => 'receiver_name',
////                'Created date' => function ($order) {
////                    return date('Y-m-d H:i:s', strtotime($order->created_at));
////                },
////                'Last updated date' => function ($order) {
////                    $date = '';
////                    if ($order->status == 0) {
////                        $date = $order->created_at;
////                    } else if ($order->status == 1) {
////                        $date = $order->accepted_at;
////                    } else if ($order->status == 2) {
////                        $date = $order->received_at;
////                    } else if ($order->status == 3) {
////                        $date = $order->delivered_at;
////                    } else if ($order->status == 4) {
////                        $date = $order->cancelled_at;
////                    } else if ($order->status == 5) {
////                        $date = $order->recalled_at;
////                    }
////                    return date('Y-m-d H:i:s', strtotime($date));
////                },
////                'Type' => function ($order) {
////                    if ($order->status == 5) {
////                        return 'Full Recalled';
////                    } else {
////                        return 'Part Delivered';
////                    }
////                },
////                'Warehouse' => 'warehouse'
////            ];
////
////            return ExcelReport::of($title, $meta, $queryBuilder, $columns)
////                ->setCss([
////                    '.bolder' => 'font-weight: 800;',
////                    '.italic-red' => 'color: red;font-style: italic;'
////                ])->download('refund_collection-' . date('Y-m-d'));
////        }
//    }

    public function scan_excel(Request $request)
    {
        $scan_collection_id = app('request')->input('scan_collection_id');
        $scan_collection = ScanCollection::withCount(['orders'])
            ->with(['driver'])
            ->find($scan_collection_id);
        if ($scan_collection) {
            $meta = [
                'Captain name' => (isset($scan_collection->driver->name) ? $scan_collection->driver->name : '-'),
                'Number of shipments' => $scan_collection->orders_count,
                'Close Date' => date("d/m/Y", strtotime($scan_collection->created_at))
            ];
            $title = "Scan Collection Orders";

            $queryBuilder = Order::join('scan_collection_orders', 'scan_collection_orders.order_id', '=', 'orders.id')
                ->leftJoin('governorates', 'governorates.id', '=', 'orders.r_government_id')
                ->leftJoin('stores', 'stores.id', '=', 'orders.moved_in')
                ->leftJoin('customers', 'customers.id', '=', 'orders.customer_id')
                ->leftJoin('corporates', 'corporates.id', '=', 'customers.corporate_id')
                ->select('governorates.name_en as governorate', 'stores.name as warehouse', 'corporates.name as corporatename',
                    'customers.name as customername', 'orders.*')
                ->where('scan_collection_orders.collection_id', $scan_collection_id);

            $columns = [
                'Code' => 'order_number',
                'Corporate Name' => 'corporatename',
                'Receiver Name' => 'receiver_name',
                'Destination' => 'governorate',
                'Status' => function ($order) {
                    if ($order->status == 3 && $order->delivery_status == 1) {
                        return 'Full Delivered';
                    } elseif ($order->status == 3 && $order->delivery_status == 2) {
                        return 'Part Delivered';
                    } elseif ($order->status == 3 && $order->delivery_status == 4) {
                        return 'Replace';
                    } elseif ($order->status == 5) {
                        return 'Recall';
                    } elseif ($order->status == 8) {
                        return 'Reject';
                    } elseif ($order->status == 7) {
                        return 'Dropped';
                    }
                },
                'Total Price' => function ($order) {
                    $delivery_fees = $order->delivery_price;
                    $order_price = $order->order_price;
                    $payment_method = $order->payment_method_id;
                    $total_price = 0;
                    if ($payment_method == "1") {
                        $total_price = $order_price ? $order_price : $delivery_fees;
                    } else if ($payment_method == "2") {
                        $total_price = $delivery_fees + $order_price;
                    }

                    return $total_price;
                },
                'Collected Price' => function ($order) {
                    return $order->status != 7 ? $order->collected_cost : 0;
                },
//                'Barcode' => 'Barcode'
            ];

            return ExcelReport::of($title, $meta, $queryBuilder, $columns)
                ->setCss([
                    '.bolder' => 'font-weight: 800;',
                    '.italic-red' => 'color: red;font-style: italic;'
                ])->download('scan_collection-' . date('Y-m-d'));
        }
    }

    public function move_excel(Request $request)
    {

        $move_collection_id = app('request')->input('move_collection');

        Excel::create('move_collection_' . $move_collection_id . '_' . date('Y_m_d'), function ($excel) use ($request, $move_collection_id) {

            $orders = Order::with(['customer' => function ($query) {
                $query->with('corporate');
            }])
                ->join('move_collection_orders', 'move_collection_orders.order_id', '=', 'orders.id')
                ->with(['to_government', 'to_city', 'delivery_problems' => function ($query) {
                    $query->orderBy('created_at', 'DESC');
                }, 'order_delay' => function ($query) {
                    $query->orderBy('created_at', 'DESC');
                }, 'comments' => function ($query) {
                    $query->orderBy('created_at', 'DESC');
                }])
                ->where('move_collection_orders.collection_id', $move_collection_id)
                ->select('orders.*')
                ->groupBy('order_id')
                ->orderBy('orders.created_at');

            $excel->setTitle('Orders');

            $excel->sheet('Orders', function ($sheet) use ($orders) {
                $sheet->loadView('backend.reports.captain-orders')->with(['orders' => $orders]);
            });

        })->download('xls');
    }

    public function warehouse_excel(Request $request)
    {

        $store_id = app('request')->input('store_id');

        Excel::create('warehouse_' . $store_id . '_' . date('Y_m_d'), function ($excel) use ($request, $store_id) {

            $orders = Order::with(['customer' => function ($query) {
                $query->with('corporate');
            }])
                ->with(['to_government', 'to_city', 'delivery_problems' => function ($query) {
                    $query->orderBy('created_at', 'DESC');
                }, 'order_delay' => function ($query) {
                    $query->orderBy('created_at', 'DESC');
                }, 'comments' => function ($query) {
                    $query->orderBy('created_at', 'DESC');
                }])
                ->where('orders.moved_in', $store_id)
                ->select('orders.*')
                ->orderBy('orders.created_at');

            $excel->setTitle('Orders');

            $excel->sheet('Orders', function ($sheet) use ($orders) {
                $sheet->loadView('backend.reports.captain-orders')->with(['orders' => $orders]);
            });

        })->download('xls');
    }

    public function scan_collection_print(Request $request)
    {

        $scan_collection_id = app('request')->input('scan_collection_id');
        $scan_collection = ScanCollection::withCount(['orders'])
            ->with(['allOrders' => function ($query) {
                $query->with(['from_government', 'to_government', 'warehouse', 'customer' => function ($query) {
                    $query->with('corporate');
                }]);
            }, 'driver'])
            ->find($scan_collection_id);

        if ($scan_collection) {
            if ($scan_collection->scan_type == 2) {
                $driver = $scan_collection->driver;
                $orders = $scan_collection->allOrders;

                $pdf = PDF::loadView('backend.scan.pdf-scan', compact('orders', 'driver', 'scan_collection'), [], ['format' => 'A4-L']);
            } else {
                $pdf = PDF::loadView('backend.orders.print_scan_collection', compact('scan_collection'), [], ['format' => 'A4-L']);
            }

            return $pdf->stream(time());
        }
    }

    public function export_scan_orders(Request $request)
    {
        $ids = explode(',', app('request')->input('ids'));
        if (is_array($ids) && count($ids)) {

            $title = "Orders";
            $queryBuilder = Order::whereIn('id', $ids)
                ->orderBy('created_at');

            $columns = [
                'ID' => 'id',
                'Order No#' => 'order_no',
                'Sender Code' => 'order_number',
                'Status' => function ($order) {
                    if ($order->status == 0) {
                        return __('backend.pending');
                    } else if ($order->status == 1) {
                        return __('backend.accepted');
                    } else if ($order->status == 2) {
                        return __('backend.received');
                    } else if ($order->status == 7) {
                        return __('backend.dropped');
                    } else if ($order->is_refund == 1) {
                        return __('backend.refund');
                    } elseif (($order->status == 3 && $order->delivery_status == 1) || ($order->status == 3 && !$order->delivery_status)) {
                        return __('backend.full_delivered');
                    } elseif ($order->status == 3 && $order->delivery_status == 2) {
                        return __('backend.pr');
                    } elseif ($order->status == 3 && $order->delivery_status == 4) {
                        return __('backend.replace');
                    } elseif ($order->status == 5) {
                        return __('backend.recall');
                    } elseif ($order->status == 8) {
                        return __('backend.reject');
                    } else if ($order->status == 4) {
                        return __('backend.cancelled');
                    } else {
                        return "";
                    }
                }
            ];

            return ExcelReport::of($title, [], $queryBuilder, $columns)
                ->setCss([
                    '.bolder' => 'font-weight: 800;',
                    '.italic-red' => 'color: red;font-style: italic;'
                ])->download('orders-' . date('Y-m-d'));

        }
    }

    public function corporate_list(Request $request)
    {

        $ids = explode(',', app('request')->input('ids'));

        if (is_array($ids) && count($ids)) {

            $title = "Corporates";
            $queryBuilder = Corporate::with(['government'])->withCount(['customers', 'orders'])
                ->whereIn('id', $ids)
                ->orderBy('created_at');

            $columns = [
                'ID' => 'id',
                'CORPORATE NAME' => 'name',
                'MOBILE NO' => 'mobile',
                'GOV' => function ($corporate) {
                    return isset($corporate->government->name_en) ? $corporate->government->name_en : '';
                },
                'STATUS' => function ($corporate) {
                    if ($corporate->is_active == 1) {
                        return __('backend.active');
                    } else {
                        return __('backend.inactive');
                    }
                },
                'CREATE DATE' => 'created_at',
                'DEVICE' => function ($corporate) {
                    return $corporate->type_of_account;
                },
                'USER MOBILE' => function ($corporate) {
                    $mobilesList = $corporate->customers->pluck('mobile')->toArray();
                    $mobiles = '';
                    $numItems = count($mobilesList);
                    $i = 0;

                    if (count($mobilesList)) {
                        foreach ($mobilesList as $mobile) {
                            $mobiles .= $mobile;
                            if (++$i !== $numItems) {
                                $mobiles .= "/";
                            }
                        }
                    }
                    return $mobiles;
                },
                'NO.OF USERS' => function ($corporate) {
                    return $corporate->customers_count;
                },
                'NO.OF ORDERS' => function ($corporate) {
                    return $corporate->orders_count;
                },
                'ACTIVITY' => '',
                'RESPONSIBLE' => 'responsible'
            ];

            return ExcelReport::of($title, [], $queryBuilder, $columns)->setCss([
                '.bolder' => 'font-weight: 800;',
                '.italic-red' => 'color: red;font-style: italic;'
            ])->download('corporates-' . time());
        }
    }

    public function recall_warehouse(Request $request)
    {

        $title = "Orders";
        $queryBuilder = Order::leftJoin('governorates', 'governorates.id', '=', 'orders.r_government_id')
            ->select('governorates.name_en as governorate', 'orders.id', 'orders.order_number', 'orders.status')
            ->where('orders.status', 5)
            ->where('orders.warehouse_dropoff', 1)
            ->whereDate('orders.dropped_at', '>=', '2020-06-03');

        $columns = [
            'ID' => 'id',
            'Code' => 'order_number',
            'Status' => function ($order) {
                if ($order->status == 0) {
                    return __('backend.pending');
                } else if ($order->status == 1) {
                    return __('backend.accepted');
                } else if ($order->status == 2) {
                    return __('backend.received');
                } else if ($order->status == 3) {
                    return __('backend.delivered');
                } else if ($order->status == 4) {
                    return __('backend.cancelled');
                } else if ($order->status == 5) {
                    return __('backend.recalled');
                } else if ($order->status == 6) {
                    return __('backend.waiting');
                } else if ($order->status == 7) {
                    return __('backend.dropped');
                } else if ($order->status == 8) {
                    return __('backend.rejected');
                }
            },
            'Receiver Government' => 'governorate'
        ];

        return ExcelReport::of($title, [], $queryBuilder, $columns)->setCss([
            '.bolder' => 'font-weight: 800;',
            '.italic-red' => 'color: red;font-style: italic;'
        ])->download('orders-' . time());

    }

    public function index(Request $request)
    {

        $filterdColumns = $request->filled('columns') ? explode(',', $request->input('columns')) : [];
        $fromDate = $request->input('from_date');
        $toDate = $request->input('to_date');
        $sortBy = $request->input('sort_by');
        $meta = [
            'Duration From - To' => $fromDate . ' To ' . $toDate,
            'Order By' => $sortBy
        ];

        if ($request->type == 1) {
            $title = 'Drivers';
            $queryBuilder = Driver::whereDate('created_at', '>=', $fromDate)->whereDate('created_at', '<=', $toDate)->orderBy($sortBy);
            $columns = [
                'ID' => 'id',
                'Joined At' => 'created_at',
                'Name' => 'name',
                'Email' => 'email',
                'Mobile' => 'mobile',
                'Phone' => 'phone',
                'Address' => 'address',
            ];

            if (count($filterdColumns)) {
                $columns = array_merge(array_flip($filterdColumns), array_intersect_key($columns, array_flip($filterdColumns)));
            }

            if ($request->ajax()) {
                $drivers = $queryBuilder->paginate(50);
                return [
                    'view' => view('backend.drivers.table', compact('drivers'))->render(),
                ];
            }

            /*Excel::create('Drivers', function($excel) use($fromDate,$toDate,$sortBy) {

                $excel->sheet('Driver', function($sheet)  use($fromDate,$toDate,$sortBy) {

                $drivers =Driver::whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)->orderBy($sortBy)->get();

                      foreach($drivers as $driver) {
                         $data[] = array(
                            $driver->created_at,
                            $driver->name,
                            $driver->email,
                            $driver->mobile,
                        );
                    }

                    $headings = array('Created At', 'Name', 'Email','Mobile' );

                       $sheet->fromArray($data, null, 'A1', false, false);
                        $sheet->prependRow(1, $headings);

                       $sheet->cells('A1:D1', function($cell) {

                            $cell->setBackground('#58a3ee');
                        });

                    });
            })->export('xls');
            */

            return ExcelReport::of($title, $meta, $queryBuilder, $columns)->setCss([
                '.bolder' => 'font-weight: 800;',
                '.italic-red' => 'color: red;font-style: italic;'
            ])->download('drivers-' . time());

            $drivers = $queryBuilder->paginate(100);
            $pdf = PDF::loadView('backend.drivers.table', $drivers);
            return $pdf->stream('drivers-' . time());

        } else if ($request->type == 2) {

            $title = "Customers";

            $queryBuilder = Customer::whereDate('created_at', '>=', $fromDate)->whereDate('created_at', '<=', $toDate)->orderBy($sortBy);

            $columns = [
                'ID' => 'id',
                'Joined At' => 'created_at',
                'Name' => 'name',
                'Email' => 'email',
                'Mobile' => 'mobile',
                'Phone' => 'phone',
                'Address' => 'address',
            ];

            if (count($filterdColumns)) {
                $columns = array_merge(array_flip($filterdColumns), array_intersect_key($columns, array_flip($filterdColumns)));
            }

            if ($request->ajax()) {
                $customers = $queryBuilder->paginate(50);
                return [
                    'view' => view('backend.customers.table', compact('customers'))->render(),
                ];
            }

//            $queryBuilder = $queryBuilder->get();

            return ExcelReport::of($title, $meta, $queryBuilder, $columns)->setCss([
                '.bolder' => 'font-weight: 800;',
                '.italic-red' => 'color: red;font-style: italic;'
            ])->download('customers-' . time());

            /* } else if ($request->type == 3) {
                 $title = "Agents";
                 $queryBuilder = Agent::whereDate('created_at', '>=', $fromDate)->whereDate('created_at', '<=', $toDate)->orderBy($sortBy);
                 $columns = [
                     'ID' => 'id',
                     'Joined At' => 'created_at',
                     'Name' => 'name',
                     'Email' => 'email',
                     'Mobile' => 'mobile',
                     'Phone' => 'phone',
                     'Address' => 'address',
                     'Profit Rate' => 'profit_rate',
                     'Commercial Record Number' => 'commercial_record_number',
                     'Deal At' => 'deal_date',
                     'Started At' => 'start_date',
                 ];

                 if(count($filterdColumns)){
                     $columns = array_merge(array_flip($filterdColumns), array_intersect_key($columns, array_flip($filterdColumns)));
                 }

                 if ($request->ajax()) {
                     $agents = $queryBuilder->paginate(50);
                     return [
                         'view' => view('backend.agents.table', compact('agents'))->render(),
                     ];
                 }
                 return ExcelReport::of($title, $meta, $queryBuilder, $columns)->setCss([
                     '.bolder' => 'font-weight: 800;',
                     '.italic-red' => 'color: red;font-style: italic;'
                 ])->download('agents-' . time());
     */
        } else if ($request->type == 4) {
            $title = "Corporates";
            $queryBuilder = Corporate::whereDate('created_at', '>=', $fromDate)->whereDate('created_at', '<=', $toDate)->orderBy($sortBy);
            $columns = [
                'ID' => 'id',
                'Joined At' => 'created_at',
                'Name' => 'name',
                'Email' => 'email',
                'Mobile' => 'mobile',
                'Phone' => 'phone',
                'Field' => 'field',
                'Commercial Record Number' => 'commercial_record_number',
            ];

            if (count($filterdColumns)) {
                $columns = array_merge(array_flip($filterdColumns), array_intersect_key($columns, array_flip($filterdColumns)));
            }

            if ($request->ajax()) {
                $corporates = $queryBuilder->paginate(50);
                return [
                    'view' => view('backend.corporates.table', compact('corporates'))->render(),
                ];
            }
            return ExcelReport::of($title, $meta, $queryBuilder, $columns)->setCss([
                '.bolder' => 'font-weight: 800;',
                '.italic-red' => 'color: red;font-style: italic;'
            ])->download('corprates-' . time());

//            $title = "Corporates";
//            $queryBuilder = Customer::join('corporates', 'corporates.id', '=', 'customers.corporate_id')
//                ->orderBy('customers.created_at', 'DESC')
//                ->select('customers.id as id', 'customers.created_at as created_at', 'customers.name as name',
//                    'customers.email as email', 'customers.mobile as mobile',
//                    'corporates.id as c_id', 'corporates.created_at as c_created_at', 'corporates.name as c_name',
//                    'corporates.email as c_email', 'corporates.mobile as c_mobile', 'corporates.mobile as is_active');
//
//            $columns = [
//                'Customer ID' => 'id',
//                'Customer Joined At' => 'created_at',
//                'Customer Name' => 'name',
//                'Customer Email' => 'email',
//                'Customer Mobile' => 'mobile',
//                'Customer Phone' => 'phone',
//                'Customer Total Orders' => function ($customer) {
//                    return Order::where('customer_id', $customer->id)->count();
//                },
//                'Corporate ID' => 'c_id',
//                'Corporate Joined At' => 'c_created_at',
//                'Corporate Name' => 'c_name',
//                'Corporate Email' => 'c_email',
//                'Corporate Mobile' => 'c_mobile',
//                'Corporate Phone' => 'c_phone',
//                'Corporate Status' => function ($customer) {
//                    return $customer->is_active ? 'Active' : 'Not Active';
//                },
//                'Corporate Total Orders' => function ($customer) {
//                    $customers = Customer::where('corporate_id', $customer->c_id)->pluck('customers.id')->toArray();
//                    return Order::whereIn('customer_id', $customers)->count();
//                }
//            ];

            return ExcelReport::of($title, $meta, $queryBuilder, $columns)->setCss([
                '.bolder' => 'font-weight: 800;',
                '.italic-red' => 'color: red;font-style: italic;'
            ])->download('corprates-' . time());

        } else if ($request->type == 5) {
            $title = "Orders";
            $queryBuilder = Order::leftJoin('governorates', 'governorates.id', '=', 'orders.r_government_id')
                ->leftJoin('customers', 'customers.id', '=', 'orders.customer_id')
                ->leftJoin('payment_methods', 'payment_methods.id', '=', 'orders.payment_method_id')
                ->leftJoin('order_types', 'order_types.id', '=', 'orders.order_type_id')
                ->leftJoin('corporates', 'corporates.id', '=', 'customers.corporate_id')
                ->leftJoin('stores', 'stores.id', '=', 'orders.moved_in')
                ->leftJoin('cities', 'cities.id', '=', 'orders.r_state_id')
                ->select('governorates.name_en as governorate', 'orders.order_type_id', 'orders.payment_method_id',
                    'order_types.name_en as ordertype', 'payment_methods.name as payment', 'cities.name_en as city',
                    'orders.r_government_id', 'orders.id', 'orders.order_no', 'orders.delivery_price',
                    'corporates.name as corporatename', 'customers.name as customername', 'customers.corporate_id',
                    'orders.payment_status', 'orders.receiver_address', 'orders.receiver_mobile', 'orders.notes', 'orders.order_price',
                    'orders.order_number', 'orders.status', 'orders.receiver_name', 'orders.receiver_code',
                    'orders.overload', 'orders.created_at', 'orders.delivery_status', 'orders.warehouse_dropoff', 'orders.reference_number',
                    'orders.accepted_at', 'orders.received_at', 'orders.received_at', 'orders.delivered_at', 'orders.cancelled_at', 'orders.recalled_at', 'orders.dropped_at', 'orders.rejected_at',
                    'orders.client_dropoff', 'orders.collected_cost', 'orders.status_before_cancel', 'stores.name as store_name', 'orders.is_refund')->whereDate('orders.created_at', '>=', $fromDate)
                ->whereDate('orders.created_at', '<=', $toDate)->orderBy($sortBy);

            $type = 'all';
            $columns = [
                'ID' => 'id',
//                'Order No#' => 'order_no',
                'Corporate Name' => 'corporatename',
                'Account Name' => 'customername',
//                'Warehouse Center' => 'store_name',
                'Sender Code' => 'order_number',
                'Reference Number' => 'reference_number',

                'Created At' => function ($order) {
                    return date('Y-m-d', strtotime($order->created_at));
                },
                'Last Update Date' => function ($order) {
                    $date = '';
                    if ($order->status == 0) {
                        $date = $order->created_at;
                    } else if ($order->status == 1) {
                        $date = $order->accepted_at;
                    } else if ($order->status == 2) {
                        $date = $order->received_at;
                    } else if ($order->status == 3) {
                        $date = $order->delivered_at;
                    } else if ($order->status == 4) {
                        $date = $order->cancelled_at;
                    } else if ($order->status == 5) {
                        $date = $order->recalled_at;
                    } else if ($order->status == 7) {
                        $date = $order->dropped_at;
                    } else if ($order->status == 8) {
                        $date = $order->rejected_at;
                    }

                    return date('Y-m-d', strtotime($date));
                },
                'New Date' => function ($order) {

                    if ($order->received_at) {
                        $date = '';

                        if (in_array($order->r_government_id, [1, 2])) {
                            $date = Carbon::parse($order->received_at)->addSeconds(172800)->format('D, M d, Y h:m A');
                        } elseif (!in_array($order->r_government_id, [1, 2])) {
                            $date = Carbon::parse($order->received_at)->addSeconds(331200)->format('D, M d, Y h:m A');
                        }

                        return date('Y-m-d', strtotime($date));
                    }

                    return '-';

                },
                'Dropped Date' => function ($order) {
                    if ($order->dropped_at) {
                        return date('Y-m-d', strtotime($order->dropped_at));
                    }

                    return "";

                },
//                'O.F.D Date' => function ($order) {
//                    if ($order->received_at) {
//                        return date('Y-m-d', strtotime($order->received_at));
//                    }
//
//                    return "";
//
//                },
//                'Weight' => 'overload',
//
//                'Order Price' => 'order_price',
                'Fees' => 'delivery_price',
                'Total Price' => function ($order) {
                    return $order->total_price;
                },

//                'Payment Method' => 'payment',
//                'Order Type' => 'ordertype',
                'Status' => function ($order) {
                    if ($order->status == 0) {
                        return __('backend.pending');
                    } else if ($order->status == 1) {
                        return __('backend.accepted');
                    } else if ($order->status == 2) {
                        return __('backend.received');
                    } else if ($order->status == 7) {
                        return __('backend.dropped');
//                    } else if ($order->is_refund == 1) {
//                        return __('backend.refund');
                    } elseif (($order->status == 3 && $order->delivery_status == 1) || ($order->status == 3 && !$order->delivery_status)) {
                        return __('backend.full_delivered');
                    } elseif ($order->status == 3 && $order->delivery_status == 2) {
                        return __('backend.pr');
                    } elseif ($order->status == 3 && $order->delivery_status == 4) {
                        return __('backend.replace');
                    } elseif ($order->status == 5) {
                        return __('backend.recall');
                    } elseif ($order->status == 8) {
                        return __('backend.reject');
                    } else if ($order->status == 4) {
                        return __('backend.cancelled');
                    } else {
                        return "";
                    }
                },
                'Payment Status' => function ($order) {
                    if ($order->payment_status == 1) {
                        return __('backend.collected');
                    } else if ($order->payment_status == 2) {
                        return __('backend.paid');
                    } else {
                        return "";
                    }
                },
                'Warehouse' => function ($order) {
                    if ($order->status == 4 && $order->status_before_cancel == 0) {
                        return "Pending";
                    } elseif ($order->is_refund == 1) {
                        return "Refund";
                    } elseif ($order->client_dropoff == 1) {
                        return "Client";
                    } elseif ($order->warehouse_dropoff == 1) {
                        return "Warehouse";
                    } else {
                        return;
                    }
                },
                'Collected Cost' => 'collected_cost',
                'Customer Name' => 'receiver_name',
                'Phone Number' => 'receiver_mobile',
                'Receiver Government' => 'governorate',
//                'Receiver City' => 'city',
//                'Driver' => function ($order) {
//                    return !empty($order->accepted) && !empty($order->accepted->driver) ? $order->accepted->driver->name : '-';
//                },
                'Manager' => function ($order) {
                    $driver = !empty($order->oneDriver) ? $order->oneDriver->first() : null;
                    if ($driver && $driver->manager_id && !empty($driver->manager)) {
                        return $driver->manager->name;
                    } elseif ($driver) {
                        return $driver->name;
                    } else {
                        return '-';
                    }
                },
                'Captain' => function ($order) {
                    $driver = !empty($order->oneDriver) ? $order->oneDriver->first() : null;
                    if ($driver && $driver->manager_id && !empty($driver->manager)) {
                        return $driver->name;
                    } else {
                        return '-';
                    }
                },
                'Notes' => 'notes',
                'Address' => 'receiver_address',
            ];

            if (count($filterdColumns)) {
                $columns = array_merge(array_flip($filterdColumns), array_intersect_key($columns, array_flip($filterdColumns)));
            }

            if ($request->ajax()) {
                $orders = $queryBuilder->paginate(50);
                return [
                    'view' => view('backend.orders.table', compact('orders', 'type'))->render(),
                ];
            }
            return ExcelReport::of($title, $meta, $queryBuilder, $columns)
                ->showTotal([
                    'Order Price' => 'point',
                    'Fees' => 'point',
                    'Total Price' => 'point'
                ])->showNumColumn(false)
                ->setCss([
                    '.bolder' => 'font-weight: 800;',
                    '.italic-red' => 'color: red;font-style: italic;'
                ])->download('orders-' . date('Y-m-d'));

        }
    }

    public function export_sheet(Request $request)
    {
        $data[] = [];
        $data = array(array(
            'no.' => 'no.',
            'rkm_alshhn' => 'رقم الشحنة',
            'alasm' => 'الاسم',
            'rkm_altlyfon' => 'رقم التليفون',
            'rkm_tlyfon_aakhr' => 'رقم تليفون آخر',
            'alaanoan' => 'العنوان',
            'almhafth' => 'المحافظة',
            'alajmal' => 'الاجمالى',
            'altlb' => 'الطلب',
            'mlahthat' => 'ملاحظات',
            'اﻷخطاء',
        ));

        foreach ($request->orders as $i => $row) {
            array_push($data, array(
                $row['no.'],
                $row['rkm_alshhn'],
                $row['alasm'],
                $row['rkm_altlyfon'],
                $row['rkm_tlyfon_aakhr'],
                $row['alaanoan'],
                $row['government'],
                $row['alajmal'],
                $row['altlb'],
                $row['mlahthat'],
                $row['validation_errors'],
            ));
        }

        Excel::create('importing_order_result', function ($excel) use ($data) {
            $excel->sheet('SD 27-4', function ($sheet) use ($data) {
                $sheet->fromArray($data, null, 'A1', false, false);
                // Set width for multiple cells
                for ($intRowNumber = 1; $intRowNumber <= count($data) + 1; $intRowNumber++) {
                    $sheet->setSize('A' . $intRowNumber, 5, 10);
                    $sheet->setSize('B' . $intRowNumber, 25, 19);
                    $sheet->setSize('C' . $intRowNumber, 25, 19);
                    $sheet->setSize('D' . $intRowNumber, 25, 20);
                    $sheet->setSize('E' . $intRowNumber, 25, 18);
                    $sheet->setSize('F' . $intRowNumber, 100, 18);
                    $sheet->setSize('G' . $intRowNumber, 15, 18);
                    $sheet->setSize('H' . $intRowNumber, 10, 18);
                    $sheet->setSize('I' . $intRowNumber, 50, 18);
                    $sheet->setSize('J' . $intRowNumber, 100, 18);
                    $sheet->setSize('K' . $intRowNumber, 100, 18);
                }
                $sheet->cells('A1:I' . count($data), function ($cells) {

                    // manipulate the range of cells
                    $cells->setAlignment('center');

                });
                $sheet->cells('H1:H' . count($data), function ($cells) {

                    // manipulate the range of cells
                    $cells->setBackground('#9dc3e6');
                    $cells->setBorder('thin');

                });
                $sheet->cells('K1:K' . count($data), function ($cells) {

                    // manipulate the range of cells
                    $cells->setBackground('#ed1c24');
                    $cells->setBorder('solid');

                });

                $sheet->setStyle(array(
                    'font' => array(
                        'name' => 'Arial',
                        'size' => 10,
                        'bold' => false,
                        'Alignment' => 'center',
                    )
                ));
#

                $sheet->row(1, function ($row) {
                    // call row For Header
                    $row->setBackground('#002060');
                    $row->setFontColor('#ffffff');
                    $row->setFont(array(
                        'family' => 'Arial',
                        'size' => '14',
                        'bold' => false
                    ));
                    $row->setAlignment('center');

                });

            });
        })->export('xls');

    }

    public function orders_status_errors($orders_errors)
    {
        $data[] = [];
        $data = array(array(
            'rkm_alshhn' => 'رقم الشحنة',
            'hal_alshhn' => 'حالة الشحنة',
            'altklf_almjmaa' => 'التكلفة المجمعة',
            'mshkl_altslym' => 'مشكلة التسليم',
            'اﻷخطاء',
        ));

        foreach ($orders_errors as $i => $row) {
            array_push($data, array(
                $row['rkm_alshhn'],
                $row['hal_alshhn'],
                $row['altklf_almjmaa'],
                $row['mshkl_altslym'],
                $row['validation_errors'],
            ));
        }

        Excel::create('importing_order_result', function ($excel) use ($data) {
            $excel->sheet('Orders', function ($sheet) use ($data) {
                $sheet->fromArray($data, null, 'A1', false, false);
                // Set width for multiple cells
                for ($intRowNumber = 1; $intRowNumber <= count($data) + 1; $intRowNumber++) {
                    $sheet->setSize('A' . $intRowNumber, 20, 10);
                    $sheet->setSize('B' . $intRowNumber, 20, 19);
                    $sheet->setSize('C' . $intRowNumber, 20, 20);
                    $sheet->setSize('D' . $intRowNumber, 60, 18);
                    $sheet->setSize('E' . $intRowNumber, 60, 18);
                }

//                $sheet->setWidth(array(
//                    'A' => 20,
//                    'B' => 20,
//                    'C' => 60,
//                    'D' => 60
//                ));

                $sheet->cells('E1:E' . count($data), function ($cells) {
                    // manipulate the range of cells
                    $cells->setBackground('#ed1c24');
                    $cells->setBorder('solid');
                });

                $sheet->setStyle(array(
                    'font' => array(
                        'name' => 'Arial',
                        'size' => 10,
                        'bold' => false,
                        'Alignment' => 'center',
                    )
                ));

                $sheet->row(1, function ($row) {
                    // call row For Header
                    $row->setBackground('#002060');
                    $row->setFontColor('#ffffff');
                    $row->setFont(array(
                        'family' => 'Arial',
                        'size' => '14',
                        'bold' => false
                    ));
                    $row->setAlignment('center');

                });

            });
        })->export('xls');

    }

    public function orders_invoice_errors($orders_errors)
    {
        $data[] = [];
        $data = array(array(
            'rkm_alshhn' => 'رقم الشحنة',
            'tarykh_aldfaa' => 'تاريخ الدفع',
            'almblgh_almdfoaa' => 'المبلغ المدفوع',
            'اﻷخطاء',
        ));

        foreach ($orders_errors as $i => $row) {
            array_push($data, array(
                $row['rkm_alshhn'],
                $row['tarykh_aldfaa'],
                $row['almblgh_almdfoaa'],
                $row['validation_errors']
            ));
        }

        Excel::create('importing_order_result', function ($excel) use ($data) {
            $excel->sheet('Orders', function ($sheet) use ($data) {
                $sheet->fromArray($data, null, 'A1', false, false);
                // Set width for multiple cells
                for ($intRowNumber = 1; $intRowNumber <= count($data) + 1; $intRowNumber++) {
                    $sheet->setSize('A' . $intRowNumber, 20, 10);
                    $sheet->setSize('B' . $intRowNumber, 20, 19);
                    $sheet->setSize('C' . $intRowNumber, 20, 20);
                    $sheet->setSize('D' . $intRowNumber, 60, 18);
                }

                $sheet->cells('D1:D' . count($data), function ($cells) {
                    // manipulate the range of cells
                    $cells->setBackground('#ed1c24');
                    $cells->setBorder('solid');
                });

                $sheet->setStyle(array(
                    'font' => array(
                        'name' => 'Arial',
                        'size' => 10,
                        'bold' => false,
                        'Alignment' => 'center',
                    )
                ));

                $sheet->row(1, function ($row) {
                    // call row For Header
                    $row->setBackground('#002060');
                    $row->setFontColor('#ffffff');
                    $row->setFont(array(
                        'family' => 'Arial',
                        'size' => '14',
                        'bold' => false
                    ));
                    $row->setAlignment('center');

                });

            });
        })->export('xls');

    }

    public function handle_sheet(Request $request)
    {

        $customer_id = $request->customer_id;
        $collection_id = $request->collection_id;
        $customer = Customer::find($customer_id);
        $collection = CollectionOrder::find($collection_id);
        $store_id = $request->store_id;
        $orders = [];
        $orders_errors = array();
        $payment_id = 1;

        if (count($request->orders)) {
            foreach ($request->orders as $i => $row) {

                $row['validation_errors'] = '';

                if (!$row['alasm']) {
                    $row['validation_errors'] = $row['validation_errors'] . 'ادخل الاسم' . ' / ';
                }
                if (!$row['alaanoan']) {
                    $row['validation_errors'] = $row['validation_errors'] . 'ادخل العنوان' . ' / ';
                }

                if (!$row['rkm_altlyfon']) {
                    $row['validation_errors'] = $row['validation_errors'] . 'ادخل رقم الموبايل' . ' / ';
                } else {
                    $mobile = preg_replace('/[^0-9]/', '', $row['rkm_altlyfon']);
                    if (strlen($mobile) > 11 || strlen($mobile) < 10 || (strlen($mobile) == 10 && substr($mobile, 0, 1) === "0") || (strlen($mobile) == 11 && substr($mobile, 0, 1) !== "0")) {
                        $row['validation_errors'] = $row['validation_errors'] . 'خطأ فى رقم الموبايل' . ' / ';
                    } elseif (strlen($mobile) == 10) {
                        $mobile = '0' . $mobile;
                    }

                    $row['rkm_altlyfon'] = $mobile;
                }

                if (isset($row['rkm_alrasl_alfraay']) && $row['rkm_alrasl_alfraay']) {
                    $sub_phone = preg_replace('/[^0-9]/', '', $row['rkm_alrasl_alfraay']);
                    if (strlen($sub_phone) > 11 || strlen($sub_phone) < 10 || (strlen($sub_phone) == 10 && substr($sub_phone, 0, 1) === "0") || (strlen($sub_phone) == 11 && substr($sub_phone, 0, 1) !== "0")) {
                        $row['validation_errors'] = $row['validation_errors'] . 'خطأ فى رقم الراسل الفرعي' . ' / ';
                    } elseif (strlen($sub_phone) == 10) {
                        $sub_phone = '0' . $sub_phone;
                    }

                    $row['rkm_alrasl_alfraay'] = $sub_phone;
                }

                if ($row['alajmal'] === null || trim($row['alajmal']) === "") {
                    $row['validation_errors'] = $row['validation_errors'] . 'ادخل الإجمالي' . ' / ';
                }

                $government = Governorate::find($row['almhafth']);

                if (!$government) {
                    $row['validation_errors'] = $row['validation_errors'] . 'المحافظة غير موجودة' . ' / ';
                }

                $cost = 0;
                $constObj = null;
                if ($government) {
                    $to_id = $government->id;
                    $constObj = CustomerPrice::where('customer_id', $customer->id)
                        ->where(function ($q) use ($to_id, $customer) {
                            $q->where(function ($q1) use ($to_id, $customer) {
                                $q1->where('start_station', $customer->government_id);
                                $q1->where('access_station', $to_id);
                            });
                            $q->orWhere(function ($q1) use ($to_id, $customer) {
                                $q1->where('access_station', $customer->government_id);
                                $q1->where('start_station', $to_id);
                            });
                        })->where('status', 1)->first();

                    if (!$constObj) {
                        $constObj = GovernoratePrice::where(function ($q) use ($to_id, $customer) {
                            $q->where(function ($q1) use ($to_id, $customer) {
                                $q1->where('start_station', $customer->government_id);
                                $q1->where('access_station', $to_id);
                            });
                            $q->orWhere(function ($q1) use ($to_id, $customer) {
                                $q1->where('access_station', $customer->government_id);
                                $q1->where('start_station', $to_id);
                            });
                        })->where('status', 1)->first();

                        if (!$constObj) {
                            $row['validation_errors'] = $row['validation_errors'] . 'الشحن غير متاح للمحافظة' . ' / ';

                        } else {
                            $cost = $constObj->cost;
                        }
                    } else {
                        $cost = $constObj->cost;
                    }
                }

                if ($row['validation_errors']) {
                    array_push($orders_errors, [
                        "rkm_alshhn" => $row['rkm_alshhn'],
                        "alasm" => $row['alasm'],
                        "rkm_altlyfon" => $row['rkm_altlyfon'],
                        "rkm_tlyfon_aakhr" => $row['rkm_tlyfon_aakhr'],
                        "alaanoan" => $row['alaanoan'],
                        "almhafth" => (!empty($government) ? $government->id : ''),
                        "alajmal" => $row['alajmal'],
                        "altlb" => $row['altlb'],
                        "mlahthat" => $row['mlahthat'],
                        "government" => $row['government'],
                        "rkm_alrasl_alfraay" => (isset($row['rkm_alrasl_alfraay']) ? $row['rkm_alrasl_alfraay'] : ""),
                        "noaa_altslym" => (isset($row['noaa_altslym']) ? $row['noaa_altslym'] : ""),
                        "validation_errors" => $row['validation_errors']
                    ]);
                    continue;
                }

                $orders[] = $row;

                if ($row['government']) {
                    GovernorateKeyword::firstOrCreate([
                        'keyword' => $row['government'],
                        'governorate_id' => $row['almhafth']
                    ]);
                }


            }
        }

        if ($collection_id) {
            $collection = CollectionOrder::find($collection_id);
        }

        if (count($orders)) {

//            $collection = new CollectionOrder();
//            $collection->customer_id = $customer_id;
//            $collection->count_no = 0;
            $collection->is_saved = 1;
//            $collection->complete_later = 1;
            $collection->saved_at = Carbon::now();
//            $collection->type = 1;
            $collection->save();

            foreach ($orders as $row) {
                $order = new Order();
                $order->reference_number = $row['rkm_alshhn'];
                $order->receiver_name = $row['alasm'];
                $order->receiver_mobile = trim($row['rkm_altlyfon']);
                $order->receiver_phone = trim($row['rkm_tlyfon_aakhr']);
                $order->receiver_latitude = 0;
                $order->receiver_longitude = 0;
                $order->receiver_address = $row['alaanoan'];
                $order->r_government_id = $row['almhafth'];
                $order->r_state_id = null;
                if (isset($row['rkm_alrasl_alfraay'])) {
                    $order->sender_subphone = $row['rkm_alrasl_alfraay'];
                }
                if (isset($row['noaa_altslym'])) {
                    $order->delivery_type = $row['noaa_altslym'];
                }
                #//
                $order->sender_name = $customer->name;
                $order->sender_mobile = $customer->mobile;
                $order->sender_phone = $customer->phone;
                $order->sender_latitude = $customer->latitude;
                $order->sender_longitude = $customer->longitude;
                $order->sender_address = $customer->address;
                $order->s_government_id = $customer->government_id;
                $order->s_state_id = $customer->city_id;
                #//
                $order->collection_id = $collection->id;
                $order->status = 0;
                $order->type = $row['altlb'];
                $order->moved_in = $store_id;
                $order->order_type_id = 1;
                $order->agent_id = null;
                $order->customer_id = $customer->id;
                $order->payment_method_id = $payment_id;
                $order->order_price = ($row['alajmal'] ? trim($row['alajmal']) : 0);
                $order->delivery_price = $cost;
                $order->overload = 0;
                $order->notes = $row['mlahthat'];
                $order->order_no = $this->randomNumber(1);
                $order->order_number = $this->randomNumber(2);
                $order->receiver_code = $this->randomNumber(3);
                $order->qty = 0;
                $order->store_id = null;
                $order->main_store = null;
                $order->head_office = 0;

                $order->save();
            }
        }

        if (count($orders_errors)) {
            return view('backend.reports.upload_sheet', compact('orders_errors', 'store_id', 'customer_id', 'collection'));
        }

        return redirect()->route('mngrAdmin.collection_orders.index', ['type_id' => 1])->with('success', 'Orders Saved');


    }

//    public function test(Request $request)
//    {
//        Excel::filter('chunk')->load(public_path('test.xlsx'))->chunk(250, function ($results) {
//            foreach ($results as $i => $row) {
//                \DB::table('SMS_Client')->where('Card_Number', $row['id_no'])->update(['Phone_Number' => $row['arkam_altlyfon']]);
//            }
//        }, false);
//    }

    public function upload_sheet(Request $request)
    {
        if ($request->hasFile('file')) {
            $fileExt = $request->file('file')->getClientOriginalExtension();
            $file = 'sheet-' . time() . "." . $fileExt;
            $request->file("file")->move(public_path('api_uploades/sheets/'), $file);

            $collection = CollectionOrder::withCount('listorders')->find($request->collection_id);
            if ($collection && $collection->is_orders_pending) {
                Order::where('collection_id', $collection->id)->where('status', 0)->delete();

                $customer_id = $collection->customer_id;
                $customer = $collection->customer;
                $store_id = null;
                if (!$store_id) {
                    if ($customer && $customer->store_id) {
                        $store_id = $customer->store_id;
                    } else {
                        $store = Store::where('status', 1)->where('pickup_default', 1)->first();
                        if ($store) {
                            $store_id = $store->id;
                        }
                    }
                }
                $payment_id = 1;

                $orders = [];
                $orders_errors = array();
                $file_has_data = false;

                Excel::filter('chunk')->load(public_path('api_uploades/sheets/' . $file))->chunk(250, function ($results) use ($store_id, $customer, $payment_id, $collection, &$orders, &$orders_errors, &$file_has_data) {

                    foreach ($results as $i => $row) {

                        $file_has_data = true;
                        $row['validation_errors'] = '';
//                    if ($i == 0) {
//                        $orders_errors = array($row);
//                        continue;
//                    }
//
//                    dd($row);
                        if (!$row['alasm']) {
                            $row['validation_errors'] = $row['validation_errors'] . 'ادخل الاسم' . ' / ';
                        }
                        if (!$row['alaanoan']) {
                            $row['validation_errors'] = $row['validation_errors'] . 'ادخل العنوان' . ' / ';
                        }

                        if (!$row['rkm_altlyfon']) {
                            $row['validation_errors'] = $row['validation_errors'] . 'ادخل رقم الموبايل' . ' / ';
                        } else {
                            $mobile = preg_replace('/[^0-9]/', '', $row['rkm_altlyfon']);
                            if (strlen($mobile) > 11 || strlen($mobile) < 10 || (strlen($mobile) == 10 && substr($mobile, 0, 1) === "0") || (strlen($mobile) == 11 && substr($mobile, 0, 1) !== "0")) {
                                $row['validation_errors'] = $row['validation_errors'] . 'خطأ فى رقم الموبايل' . ' / ';
                            } elseif (strlen($mobile) == 10) {
                                $mobile = '0' . $mobile;
                            }

                            $row['rkm_altlyfon'] = $mobile;
                        }

                        if (isset($row['rkm_alrasl_alfraay']) && $row['rkm_alrasl_alfraay']) {
                            $sub_phone = preg_replace('/[^0-9]/', '', $row['rkm_alrasl_alfraay']);
                            if (strlen($sub_phone) > 11 || strlen($sub_phone) < 10 || (strlen($sub_phone) == 10 && substr($sub_phone, 0, 1) === "0") || (strlen($sub_phone) == 11 && substr($sub_phone, 0, 1) !== "0")) {
                                $row['validation_errors'] = $row['validation_errors'] . 'خطأ فى رقم الراسل الفرعي' . ' / ';
                            } elseif (strlen($sub_phone) == 10) {
                                $sub_phone = '0' . $sub_phone;
                            }

                            $row['rkm_alrasl_alfraay'] = $sub_phone;
                        }

                        if ($row['alajmal'] === null || trim($row['alajmal']) === "") {
                            $row['validation_errors'] = $row['validation_errors'] . 'ادخل الإجمالي' . ' / ';
                        }

                        if (!is_numeric(trim($row['alajmal']))) {
                            $row['validation_errors'] = $row['validation_errors'] . 'خطأ فى الإجمالي' . ' / ';
                        }

                        $government = GovernorateKeyword::where('keyword', $row['almhafth'])
                            ->select('governorate_id AS id')
                            ->first();

                        if (!$government) {
                            $government = Governorate::where('name_ar', $row['almhafth'])
                                ->OrWhere('name_en', $row['almhafth'])
                                ->first();
                        }

                        if (!$government) {
                            $row['validation_errors'] = $row['validation_errors'] . 'المحافظة غير موجودة' . ' / ';
                        }

                        $cost = 0;
                        $constObj = null;
                        if ($government) {
                            $to_id = $government->id;
                            $constObj = CustomerPrice::where('customer_id', $customer->id)
                                ->where(function ($q) use ($to_id, $customer) {
                                    $q->where(function ($q1) use ($to_id, $customer) {
                                        $q1->where('start_station', $customer->government_id);
                                        $q1->where('access_station', $to_id);
                                    });
                                    $q->orWhere(function ($q1) use ($to_id, $customer) {
                                        $q1->where('access_station', $customer->government_id);
                                        $q1->where('start_station', $to_id);
                                    });
                                })->where('status', 1)->first();

                            if (!$constObj) {
                                $constObj = GovernoratePrice::where(function ($q) use ($to_id, $customer) {
                                    $q->where(function ($q1) use ($to_id, $customer) {
                                        $q1->where('start_station', $customer->government_id);
                                        $q1->where('access_station', $to_id);
                                    });
                                    $q->orWhere(function ($q1) use ($to_id, $customer) {
                                        $q1->where('access_station', $customer->government_id);
                                        $q1->where('start_station', $to_id);
                                    });
                                })->where('status', 1)->first();

                                if (!$constObj) {
//                          dd($customer->government_id, $row['almhafth']);
                                    $row['validation_errors'] = $row['validation_errors'] . 'الشحن غير متاح للمحافظة' . ' / ';

                                } else {
                                    $cost = $constObj->cost;
                                }
                            } else {
                                $cost = $constObj->cost;
                            }
                        }

                        if ($row['validation_errors']) {
                            array_push($orders_errors, [
                                "rkm_alshhn" => $row['rkm_alshhn'],
                                "alasm" => $row['alasm'],
                                "rkm_altlyfon" => $row['rkm_altlyfon'],
                                "rkm_tlyfon_aakhr" => $row['rkm_tlyfon_aakhr'],
                                "alaanoan" => $row['alaanoan'],
                                "almhafth" => (!empty($government) ? $government->id : ''),
                                "government" => $row['almhafth'],
                                "alajmal" => $row['alajmal'],
                                "altlb" => $row['altlb'],
                                "mlahthat" => $row['mlahthat'],
                                "rkm_alrasl_alfraay" => (isset($row['rkm_alrasl_alfraay']) ? $row['rkm_alrasl_alfraay'] : ""),
                                "noaa_altslym" => (isset($row['noaa_altslym']) ? $row['noaa_altslym'] : ""),
                                "validation_errors" => $row['validation_errors']
                            ]);
                            continue;
                        }

                        $row['cost'] = $cost;
                        $row['almhafth'] = (!empty($government) ? $government->id : '');
                        $orders[] = $row;

                    }

                }, false);

                File::delete(public_path('api_uploades/sheets/' . $file));

                if (count($orders)) {

//                    $collection = new CollectionOrder();
//                    $collection->customer_id = $customer_id;
//                    $collection->count_no = 0;
                    $collection->is_saved = 1;
//                    $collection->complete_later = 1;
                    $collection->saved_at = Carbon::now();
//                    $collection->type = 1;
                    $collection->save();

                    foreach ($orders as $row) {
                        $order = new Order();
                        $order->reference_number = $row['rkm_alshhn'];
                        $order->receiver_name = $row['alasm'];
                        $order->receiver_mobile = trim($row['rkm_altlyfon']);
                        $order->receiver_phone = trim($row['rkm_tlyfon_aakhr']);
                        $order->receiver_latitude = 0;
                        $order->receiver_longitude = 0;
                        $order->receiver_address = $row['alaanoan'];
                        $order->r_government_id = $row['almhafth'];
                        $order->r_state_id = null;
                        if (isset($row['rkm_alrasl_alfraay'])) {
                            $order->sender_subphone = $row['rkm_alrasl_alfraay'];
                        }
                        if (isset($row['noaa_altslym'])) {
                            $order->delivery_type = $row['noaa_altslym'];
                        }
                        #//
                        $order->sender_name = $customer->name;
                        $order->sender_mobile = $customer->mobile;
                        $order->sender_phone = $customer->phone;
                        $order->sender_latitude = $customer->latitude;
                        $order->sender_longitude = $customer->longitude;
                        $order->sender_address = $customer->address;
                        $order->s_government_id = $customer->government_id;
                        $order->s_state_id = $customer->city_id;
                        #//
                        $order->collection_id = $collection->id;
                        $order->status = 0;
                        $order->type = $row['altlb'];
                        $order->moved_in = $store_id;
                        $order->order_type_id = 1;
                        $order->agent_id = null;
                        $order->customer_id = $customer->id;
                        $order->payment_method_id = $payment_id;
                        $order->order_price = ($row['alajmal'] ? trim($row['alajmal']) : 0);
                        $order->delivery_price = $row['cost'];
                        $order->overload = 0;
                        $order->notes = $row['mlahthat'];
                        $order->order_no = $this->randomNumber(1);
                        $order->order_number = $this->randomNumber(2);
                        $order->receiver_code = $this->randomNumber(3);
                        $order->qty = 0;
                        $order->store_id = null;
                        $order->main_store = null;
                        $order->head_office = 0;

                        $order->save();
                    }
                }
            } else {
                return redirect()->back()->with('warning', 'Not Allowed');
            }

            if (count($orders_errors)) {
                return view('backend.reports.upload_sheet', compact('orders_errors', 'store_id', 'customer_id', 'collection'));
//                $this->orders_report_errors($orders_errors);
            }

            if ($file_has_data) {
                return redirect()->back()->with('success', 'Orders Saved');
            } else {
                return redirect()->back()->with('warning', 'File is empty');
            }

        }
    }

    public function update_status(Request $request)
    {
        if ($request->hasFile('file')) {
            $fileExt = $request->file('file')->getClientOriginalExtension();
            $file = 'sheet-' . time() . "." . $fileExt;
            $request->file("file")->move(public_path('api_uploades/sheets/'), $file);

            $orders_errors = array();
            $file_has_data = false;

            $actions = [];

            Excel::filter('chunk')->load(public_path('api_uploades/sheets/' . $file))->chunk(250, function ($results) use (&$orders_errors, &$file_has_data, &$actions) {

                foreach ($results as $i => $row) {
                    $file_has_data = true;
                    $row['validation_errors'] = '';
//                    if ($i == 0) {
//                        $orders_errors = array($row);
//                        continue;
//                    }
//
                    $order_number = trim($row['rkm_alshhn']);
                    $order_status = strtolower(trim($row['hal_alshhn']));
                    $delivery_problem = trim($row['mshkl_altslym']);
                    $collected_cost = trim($row['altklf_almjmaa']);

                    $status = 0;
                    if ($order_status == 'received') {
                        $status = 2;
                    } elseif ($order_status == 'recalled') {
                        $status = 5;
                    } elseif ($order_status == 'rejected') {
                        $status = 8;
                    } elseif (in_array($order_status, ['delivered', 'replace', 'pr'])) {
                        $status = 3;
                    }

                    if (!$order_number) {
                        $row['validation_errors'] = $row['validation_errors'] . ' / ' . 'رقم الشحنة مطلوب';
                    }

                    if (!in_array($order_status, ['received', 'delivered', 'recalled', 'replace', 'pr', 'rejected'])) {
                        $row['validation_errors'] = $row['validation_errors'] . ' / ' . 'حالة غير مقبولة';
                    }

                    if (!$order_status) {
                        $row['validation_errors'] = $row['validation_errors'] . ' / ' . 'حالة الشحنة مطلوبة';
                    }

                    $order = Order::where(function ($query) use ($order_number) {
                        $query->where('order_number', $order_number)
                            ->orWhere('order_no', $order_number)
                            ->orWhere('id', $order_number);
                    })->first();

                    if (!$order) {
                        $row['validation_errors'] = $row['validation_errors'] . ' / ' . 'الشحنة غير موجودة';
                    } else {
                        if ($order->status != 2 && $order->status != $status) {
                            $row['validation_errors'] = $row['validation_errors'] . ' / ' . 'لا يمكن تغيير حالة الشحنة';
                        }

                        if ($order->is_refund) {
                            $row['validation_errors'] = $row['validation_errors'] . ' / ' . 'لا يمكن تغيير حالة الشحنة';
                        }
                    }

                    if ($row['validation_errors']) {
                        array_push($orders_errors, $row);
                        continue;
                    }

                    if ($delivery_problem) {
                        OrderComment::create([
                            'comment' => $delivery_problem,
                            'order_id' => $order->id,
                            'user_type' => 1,
                            'user_id' => Auth::guard($this->guardType)->user()->id,
                            'latitude' => 0,
                            'longitude' => 0
                        ]);

                        event(new OrderUpdated([$order->id],
                            Auth::guard($this->guardType)->user()->id,
                            1,
                            'comments',
                            ['comment' => $delivery_problem]
                        ));
                    }

                    if ($order_status == 'recalled') {
                        if ($order->status == 2) {
                            AcceptedOrder::where('order_id', $order->id)
                                ->where('status', 2)
                                ->update(['status' => 5]);
                            $order->status = 5;
                            $order->recalled_at = date("Y-m-d H:i:s");
                            $order->recalled_by = 2;
                            $actions['recall'][] = $order->id;
                        }
//                        event(new OrderUpdated([$order->id], Auth::guard($this->guardType)->user()->id, 1, 'recall', ['recalled_by' => 2]));
                    } elseif ($order_status == 'rejected') {
                        if ($order->status == 2) {
                            AcceptedOrder::where('order_id', $order->id)
                                ->where('status', 2)
                                ->update(['status' => 8]);
                            $order->status = 8;
                            $order->rejected_at = date("Y-m-d H:i:s");
                            $actions['reject'][] = $order->id;
                        }
//                        event(new OrderUpdated([$order->id], Auth::guard($this->guardType)->user()->id, 1, 'recall', ['recalled_by' => 2]));
                    } elseif (in_array($order_status, ['delivered', 'replace', 'pr'])) {
                        if ($order->status == 2) {
                            AcceptedOrder::where('order_id', $order->id)
                                ->where('status', 2)
                                ->update(['status' => 3, 'delivery_date' => date("Y-m-d H:i:s")]);
                            $order->status = 3;
                            $order->delivered_at = date("Y-m-d H:i:s");
                            $actions['deliver'][] = $order->id;
                        }
                        $order->delivery_status = ($order_status == 'delivered' ? 1 : ($order_status == 'pr' ? 2 : 4));

//                        event(new OrderUpdated([$order->id], Auth::guard($this->guardType)->user()->id, 1, 'deliver'));
                    } else {

//                        if ($delivery_problem) {
//                            $problem_count = OrderDeliveryProblem::where('order_id', $order->id)->count();
//                            if ($problem_count >= 5 && $order->status == 2) {
//                                AcceptedOrder::where('order_id', $order->id)
//                                    ->where('status', 2)
//                                    ->update(['status' => 5]);
//                                $order->status = 5;
//                                $order->recalled_at = date("Y-m-d H:i:s");
//                                $order->recalled_by = 2;
//                                $actions['recall'][] = $order->id;
////                                event(new OrderUpdated([$order->id], Auth::guard($this->guardType)->user()->id, 1, 'recall', ['recalled_by' => 2]));
//                            }
//                        }
                    }

                    if ($collected_cost) {
                        $order->collected_cost = $collected_cost;
                    }

                    $order->save();

                }

            }, false);

            if (count($actions)) {
                foreach ($actions as $key => $ids) {
                    $custom = ['update_reason' => 'update_status_sheet'];
                    if ($key == 'recall') {
                        $custom['recalled_by'] = 2;
                    }
                    event(new OrderUpdated($ids, Auth::guard($this->guardType)->user()->id, 1, $key, $custom));
                }
            }

            File::delete(public_path('api_uploades/sheets/' . $file));

            if (count($orders_errors)) {
                $this->orders_status_errors($orders_errors);
            }

            if ($file_has_data) {
                return redirect()->back()->with('success', 'Orders Saved');
            } else {
                return redirect()->back()->with('warning', 'File is empty');
            }

        }
    }

    public function upload_invoice(Request $request)
    {
        if ($request->hasFile('file')) {
            $fileExt = $request->file('file')->getClientOriginalExtension();
            $file = 'sheet-' . time() . "." . $fileExt;
            $request->file("file")->move(public_path('api_uploades/sheets/'), $file);

            $orders_errors = array();
            $file_has_data = false;

            $ids = [];

            Excel::filter('chunk')->load(public_path('api_uploades/sheets/' . $file))->chunk(250, function ($results) use (&$orders_errors, &$file_has_data, &$ids) {

                foreach ($results as $i => $row) {
                    $file_has_data = true;
                    $row['validation_errors'] = '';
                    $order_number = trim($row['rkm_alshhn']);
                    $payment_date = trim($row['tarykh_aldfaa']);
                    $residual = trim($row['almblgh_almdfoaa']);

                    if (!$order_number) {
                        $row['validation_errors'] = $row['validation_errors'] . ' / ' . 'رقم الشحنة مطلوب';
                    }

                    if (!$payment_date) {
                        $row['validation_errors'] = $row['validation_errors'] . ' / ' . 'التاريخ مطلوب';
                    }

                    //                    if (!$residual) {
//                        $row['validation_errors'] = $row['validation_errors'] . ' / ' . 'المبلغ المدفوع مطلوب';
//                    }

                    $order = Order::where('order_number', $order_number)->first();

                    if (!$order) {
                        $row['validation_errors'] = $row['validation_errors'] . ' / ' . 'الشحنة غير موجودة';
                    } elseif (in_array($order->status, [0, 1, 2, 7])) {
                        $row['validation_errors'] = $row['validation_errors'] . ' / ' . 'لا يمكن تغيير حالة الشحنة';
                    }

                    if ($row['validation_errors']) {
                        array_push($orders_errors, $row);
                        continue;
                    }

                    $order->payment_status = 2;
                    $order->payment_date = date("Y-m-d H:i:s", strtotime($payment_date));
                    $order->residual = $residual ?: 0;

                    $order->save();

                    $ids[] = $order->id;

                }

            }, false);

            File::delete(public_path('api_uploades/sheets/' . $file));

            if (count($ids)) {
                event(new OrderUpdated($ids, Auth::guard($this->guardType)->user()->id, 1, 'collect'));
            }

            if (count($orders_errors)) {
                $this->orders_invoice_errors($orders_errors);
            }

            if ($file_has_data) {
                return redirect()->back()->with('success', 'Orders Saved');
            } else {
                return redirect()->back()->with('warning', 'File is empty');
            }

        }
    }

    public function update_delivery_date(Request $request)
    {
        if ($request->hasFile('file')) {
            $fileExt = $request->file('file')->getClientOriginalExtension();
            $file = 'sheet-' . time() . "." . $fileExt;
            $request->file("file")->move(public_path('api_uploades/sheets/'), $file);

            Excel::filter('chunk')->load(public_path('api_uploades/sheets/' . $file))->chunk(250, function ($results) {

                foreach ($results as $i => $row) {

                    $order_number = trim($row['rkm_alshhn']);
                    $delivery_date = strtolower(trim($row['tarykh_altslym']));

                    if ($order_number && $delivery_date) {
                        $order = Order::where('status', 3)->where(function ($query) use ($order_number) {
                            $query->where('order_number', $order_number)
                                ->orWhere('order_no', $order_number)
                                ->orWhere('id', $order_number);
                        })->first();

                        if ($order) {
                            $order->delivered_at = date('Y-m-d H:i:s', strtotime($delivery_date));
                            $order->save();
                        }
                    }
                }

            }, false);

            File::delete(public_path('api_uploades/sheets/' . $file));

            return redirect()->back()->with('success', 'Orders Saved');

        }
    }

    public function updateCollectedCost(Request $request)
    {
        Excel::filter('chunk')->load('public/Collect.xlsx')->chunk(250, function ($results) {

            foreach ($results as $i => $row) {
                $order_number = trim($row['awe']);
                $collected_cost = trim($row['collected']);

                Order::where('order_number', $order_number)
                    ->update(['collected_cost' => $collected_cost]);
            }

        }, false);
    }

    public function updateRefund(Request $request)
    {
        Excel::filter('chunk')->load('public/Refund.xlsx')->chunk(250, function ($results) {

            foreach ($results as $i => $row) {
                $order_number = trim($row['sender_code']);
                $collected_cost = trim($row['collected']);

                Order::where('order_number', $order_number)
                    ->update([
                        'collected_cost' => $collected_cost,
                        'is_refund' => 1,
                        'warehouse_dropoff' => 1,
                        'warehouse_dropoff_date' => Carbon::now(),
                        'client_dropoff' => 1,
                        'client_dropoff_date' => Carbon::now(),
                    ]);
            }

        }, false);
    }

    public function updateRecall(Request $request)
    {
        Excel::filter('chunk')->load('public/Query1.xlsx')->chunk(250, function ($results) {

            foreach ($results as $i => $row) {
                $order_number = trim($row['sender_code']);
                $collected_cost = trim($row['collected']);
                $status = trim($row['status']);

                $order = Order::where('order_number', $order_number)->first();

                $order->collected_cost = $collected_cost;
                $order->status = 5;
                $order->delivered_at = null;
                $order->cancelled_at = null;
                $order->delivery_status = null;
                if ($status == 'Refund') {
                    $order->is_refund = 1;
                    $order->refund_at = $order->last_status_date;
                    $order->warehouse_dropoff = 1;
                    $order->warehouse_dropoff_date = $order->last_status_date;
                    $order->client_dropoff = 1;
                    $order->client_dropoff_date = $order->last_status_date;
                } else if ($status == 'Recall') {
                    $order->is_refund = 0;
                    $order->refund_at = null;
                    $order->warehouse_dropoff = 0;
                    $order->warehouse_dropoff_date = null;
                    $order->client_dropoff = 0;
                    $order->client_dropoff_date = null;
                }

                AcceptedOrder::where('order_id', $order->id)
                    ->where('status', $order->status)
                    ->update([
                        'status' => 5
                    ]);

                $order->save();

            }

        }, false);
    }

    public function updateCost(Request $request)
    {
        Excel::filter('chunk')->load('public/Query2.xlsx')->chunk(250, function ($results) {

            foreach ($results as $i => $row) {
                $order_number = trim($row['sender_code']);
                $collected_cost = trim($row['collect']);

                Order::where('order_number', $order_number)
                    ->update(["collected_cost" => $collected_cost]);
            }

        }, false);
    }

    public function updateStatus(Request $request)
    {
        Excel::filter('chunk')->load('public/Query3.xlsx')->chunk(250, function ($results) {

            foreach ($results as $i => $row) {
                $order_number = trim($row['sender_code']);
                $collected_cost = trim($row['collect']);
                $from_status = trim($row['mn']);
                $to_status = trim($row['al']);

                $updateOrder = [];
                $updateAcceptedOrder = [];

                $order = Order::where('order_number', $order_number)->first();
                if ($order) {
                    if ($from_status == 'Cancelled') {
                        $updateOrder['cancelled_at'] = null;
                        $updateOrder['cancelled_by'] = null;
                        $updateAcceptedOrder['cancelled_at'] = null;
                        $updateAcceptedOrder['cancelled_by'] = null;
                    } elseif ($from_status == 'Recall') {
                        $updateOrder['recalled_at'] = null;
                        $updateOrder['recalled_by'] = null;
                    } elseif ($from_status == 'Refund') {
                        $updateOrder['is_refund'] = 0;
                        $updateOrder['refund_at'] = null;
                        $updateOrder['refund_collection_id'] = null;
                        $updateOrder['recalled_at'] = null;
                        $updateOrder['recalled_by'] = null;
                    }

                    $updateOrder['delivered_at'] = $order->last_status_date;
                    $updateOrder['status'] = 3;
                    $updateOrder['collected_cost'] = $collected_cost;
                    $updateAcceptedOrder['status'] = 3;
                    if ($to_status == 'Full Delivered') {
                        $updateOrder['delivery_status'] = 1;
                    } elseif ($to_status == 'Part Delivered') {
                        $updateOrder['delivery_status'] = 2;
                    }

                    Order::where('order_number', $order_number)
                        ->update($updateOrder);

                    AcceptedOrder::where('order_id', $order->id)
                        ->where('status', $order->status)
                        ->update($updateAcceptedOrder);
                } else {
                    echo $order_number . '<br>';
                }
            }

        }, false);
    }

    public function randomNumber($type = 1, $user_id = '')
    {
        return random_number($type, Auth::guard($this->guardType)->user()->id);
    }

    public function recallDeliveryProblem($type = 1, $user_id = '')
    {
        $orders = Order::join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
            ->join('drivers', 'drivers.id', '=', 'accepted_orders.driver_id')
            ->where('customer_id', 821)
            ->where('orders.status', 5)
            ->where('accepted_orders.status', 5)
//            ->whereIn('order_id', [25937, 25832, 19015, 19008, 18810])
            ->select('order_id', 'driver_id', 'latitude', 'longitude', 'recalled_at')
            ->get();

        foreach ($orders as $order) {
            OrderDeliveryProblem::create([
                'order_id' => $order->order_id,
                'latitude' => $order->latitude,
                'longitude' => $order->longitude,
                'driver_id' => $order->driver_id,
                'delivery_problem_id' => 5,
                'reason' => 'المستلم رفض الاستلام وتم التواصل بالتاجر ولم يتجاوب',
                'created_at' => $order->recalled_at,
            ]);
            echo $order->order_id . '<br>';
        }
    }
}


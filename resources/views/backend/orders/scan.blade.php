@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.the_orders')}} - {{__('backend.scan')}}</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <style>
        .toggle.ios, .toggle-on.ios, .toggle-off.ios {
            border-radius: 20px;
        }

        .toggle.ios .toggle-handle {
            border-radius: 20px;
        }

        .btn-warning {
            background-color: #e47d33;
            border-color: #e47d33;
        }

        .action_btn {
            width: 100px;
            margin: 1px;
            padding: 5px;
        }

        #delivery_problems {
            background-color: #e4cb10;
            border-color: #d6be0f;
            color: #000;
        }

        .dropdown-menu-right {
            right: auto;
            left: 0;
        }

        .table-responsive {
            overflow-y: overlay;
        }
    </style>
@endsection
@section('header')
    <div class="page-header">
        <h4>
            <i class="glyphicon glyphicon-plus"></i> {{__('backend.scan')}} </h4>
    </div>
@endsection

@section('content')

    <div class="container-fluid">
        <div class="alert alert-success" role="alert" style="display: none">
        </div>
        <div class="row">
            <form class="form-horizontal" id="scan-form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group col-sm-6" style="display: none">
                    <label class="control-label" for="code-scan"> {{__('backend.Scanned_Code')}}</label>
                    {{--                    <input type="text" id="code-scan" style="display: none">--}}
                    {{--                    <textarea class="form-control" id="code-scan" rows="3" style="min-height: 100px; resize: vertical;"--}}
                    {{--                              name="code"></textarea>--}}

                </div>
                <div class="col-md-4" style="padding: 0">
                    <label class="control-label" for="code-scan"> {{__('backend.Scanned_Code')}}</label>
                    <br>
                    <br>
                    <textarea class="form-control" id="code-scan" rows="3" style="min-height: 100px; resize: vertical;"
                              name="code" readonly></textarea>
                    <br>
                    {{--                                        <select id="driver_filter" name="driver_filter" class="form-control" style="margin-bottom: 10px">--}}
                    {{--                                            <option value="" data-display="Select">{{__('backend.Choose_Driver')}}</option>--}}
                    {{--                                            @foreach($drivers as $driver)--}}
                    {{--                                                <option value="{{$driver->id}}" data-display="Select">{{$driver->name}}</option>--}}
                    {{--                                            @endforeach--}}
                    {{--                                            <option value="0">{{__('backend.pending')}} </option>--}}
                    {{--                                        </select>--}}
                    {{--                                        <select id="status" name="status" class="form-control" style="margin-bottom: 10px">--}}
                    {{--                                            <option value="" data-display="Select">{{__('backend.status')}}</option>--}}
                    {{--                                            <option value="0">{{__('backend.pending')}} </option>--}}
                    {{--                                            <option value="1">{{__('backend.accepted')}} </option>--}}
                    {{--                                            <option value="2">{{__('backend.received')}} </option>--}}
                    {{--                                            <option value="3">{{__('backend.delivered')}} </option>--}}
                    {{--                                            <option value="4">{{__('backend.cancelled')}} </option>--}}
                    {{--                                            <option value="5">{{__('backend.recalled')}} </option>--}}
                    {{--                                        </select>--}}
{{--                    <button type="button" class="btn btn-warning" id="scan-btn"> {{__('backend.scanned')}}</button>--}}

                </div>
            </form>
        </div>
    </div>
    <div class="text-center" style="color:#5673ea;font-size: 54px; display: none" id="tempo"><h1
            class="fa fa-refresh fa-spin text-center"></h1>
    </div>
    <div id="print_form" style="display: none">

    </div>
    <div class="text-center list" id="table">

    </div>
    {{--    @include('backend.pickups.create_pickup_popup')--}}
    @include('backend.orders.delay_order_modal')
    @include('backend.orders.delivery_problem')
@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('/assets/scripts/map-order.js')}}"></script>

    <script
        src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key=AIzaSyDQiN4qc7pC9b1BRQwkqXXD28peMfWcHvw&callback=myMap"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

    <script src="{{ asset('/assets/scripts/jquery-code-scanner.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <script>
        function change_status(info = {}) {
            let ids = [];

            $(".selectpdf input:checked").each(function () {
                ids.push($(this).val());
            });

            if (ids.length > 0) {
                info['ids'] = ids;
                info['_token'] = "{{csrf_token()}}";
                $.ajax({
                    url: "{{ url('/mngrAdmin/scan_action')}}",
                    type: 'post',
                    data: info,
                    success: function (data) {
                        if (data.ids) {
                            data.ids.forEach(function (entry) {
                                if (data.status) {
                                    $("#" + entry + " .status").html(data.status);
                                }
                            });
                        }

                        if (data.result == true) {
                            $(".alert").removeClass('alert-danger').addClass('alert-success');
                        } else {
                            $(".alert").removeClass('alert-success').addClass('alert-danger');
                        }

                        window.scrollTo(0, 0);
                        $(".alert").html(data.message);
                        $(".alert").show();
                        window.setTimeout(function () {
                            $(".alert").hide();
                        }, 3000);
                    },
                    error: function (data) {
                        a.open();
                    }
                });
            }
        }

        function check_validation(info = {}) {
            let ids = [];

            $(".selectpdf input:checked").each(function () {
                ids.push($(this).val());
            });
            if (ids.length > 0) {
                info['ids'] = ids;
                info['_token'] = "{{csrf_token()}}";

                $.ajax({
                    url: "{{ url('/mngrAdmin/scan_validation')}}",
                    type: 'post',
                    data: info,
                    success: function (data) {
                        if (data.result == false) {
                            a.open();
                        } else {
                            if (info['type'] == 'forward') {
                                $("#forwardModal").modal("show");
                            } else if (info['type'] == 'receive') {
                                $.confirm({
                                    title: '',
                                    theme: 'modern',
                                    class: 'danger',
                                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-meh-o text-success"></i></span><span class="jconfirm-title">{{__("backend.Confirm")}}</span></div><div style="font-weight:bold;"><p>{{__("backend.receive_orders_confirm")}}</p></div>',
                                    buttons: {
                                        confirm: {
                                            text: '{{__("backend.Ok")}}',
                                            btnClass: 'btn-danger',
                                            action: function () {
                                                change_status({
                                                    type: 'receive'
                                                });
                                            }
                                        },
                                        cancel: {
                                            text: '{{__("backend.Cancel")}}',
                                            action: function () {
                                            }
                                        }
                                    }
                                });
                            } else if (info['type'] == 'deliver') {
                                $.confirm({
                                    title: '',
                                    theme: 'modern',
                                    class: 'danger',
                                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-meh-o text-success"></i></span><span class="jconfirm-title">{{__("backend.Confirm")}}</span></div><div style="font-weight:bold;"><p>{{__("backend.delivery_orders_confirm")}}</p></div>',
                                    buttons: {
                                        confirm: {
                                            text: '{{__("backend.Ok")}}',
                                            btnClass: 'btn-danger',
                                            action: function () {
                                                change_status({
                                                    type: 'deliver'
                                                });
                                            }
                                        },
                                        cancel: {
                                            text: '{{__("backend.Cancel")}}',
                                            action: function () {
                                            }
                                        }
                                    }
                                });
                            } else if (info['type'] == 'recall') {
                                $.confirm({
                                    title: '',
                                    theme: 'modern',
                                    class: 'danger',
                                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-meh-o text-success"></i></span><span class="jconfirm-title">{{__("backend.Confirm")}}</span></div><div style="font-weight:bold;"><p>{{__("backend.recall_orders_confirm")}}</p></div>',
                                    buttons: {
                                        confirm: {
                                            text: '{{__("backend.Ok")}}',
                                            btnClass: 'btn-danger',
                                            action: function () {
                                                change_status({
                                                    type: 'recall'
                                                });
                                            }
                                        },
                                        cancel: {
                                            text: '{{__("backend.Cancel")}}',
                                            action: function () {
                                            }
                                        }
                                    }
                                });
                            } else if (info['type'] == 'cancel') {
                                $.confirm({
                                    title: '',
                                    theme: 'modern',
                                    class: 'danger',
                                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-meh-o text-success"></i></span><span class="jconfirm-title">{{__("backend.Confirm")}}</span></div><div style="font-weight:bold;"><p>{{__("backend.cancel_orders_confirm")}}</p></div>',
                                    buttons: {
                                        confirm: {
                                            text: '{{__("backend.Ok")}}',
                                            btnClass: 'btn-danger',
                                            action: function () {
                                                change_status({
                                                    type: 'cancel'
                                                });
                                            }
                                        },
                                        cancel: {
                                            text: '{{__("backend.Cancel")}}',
                                            action: function () {
                                            }
                                        }
                                    }
                                });

                            } else if (info['type'] == 'problems') {
                                $("#problemModal").modal("show");

                            } else if (info['type'] == 'delay') {
                                $("#delayModal").modal("show");

                            }
                        }
                    },
                    error: function (data) {
                        a.open();
                    }
                });
            }
        }

        $(function () {
            /*print*/
            $('.toggle').bootstrapToggle();

            $("#delivery_problem_id").on('change', function () {
                if ($(this).val() == 5) {
                    $("#other_reason").show();
                } else {
                    $("#reason").val("");
                    $("#other_reason").hide();
                }
            });

            $('input[type=radio][name=delay_comment]').change(function () {
                if (this.value == 'Other reason') {
                    $("#delay_reason").show();
                } else {
                    $("#del_reason").val("");
                    $("#delay_reason").hide();
                }
            });

            $('body').on('change', '.selectOrder', function (e) {
                if ($('input[name="select[]"]').is(':checked')) {
                    $(".action_btn").not('#toggle').removeClass('disabled');
                    $("#pdf-submit").prop('disabled', false);
                } else {
                    $(".action_btn").not('#toggle').addClass('disabled');
                    $("#pdf-submit").prop('disabled', true);
                }
            });

            $('body').on('change', '#agent', function (e) {
                $.ajax({
                    url: "{{ url('/mngrAdmin/getdriver_by_agents')}}" + "/" + $(this).val(),
                    type: 'get',
                    data: {},
                    success: function (data) {

                        $('#driver').html('<option value="">Chosse Driver</option>');
                        $.each(data, function (key, content) {
                            $("#driver").append($("<option></option>").attr("value", content.id).text(content.name));
                        });

                    },
                    error: function () {
                        alert('Error occured');
                    }
                });

            });

            $('body').on('click', '#saveDelay', function (e) {
                if ($("#delay_date").val()) {
                    let ids = [];
                    $(".selectOrder:checked").each(function () {
                        ids.push($(this).val());
                    });
                    $.ajax({
                        url: "{{ url('/mngrAdmin/delay_orders')}}",
                        type: 'post',
                        data: {
                            ids: ids,
                            delay_date: $("#delay_date").val(),
                            delay_comment: $("input[name='delay_comment']:checked").val(),
                            delay_reason: $("#del_reason").val(),
                            change_status: $("input[name='change_status']:checked").val(),
                            flash: 0,
                            '_token': "{{csrf_token()}}"
                        },
                        success: function (data) {
                            if (data.ids) {
                                data.ids.forEach(function (entry) {
                                    if (data.status) {
                                        $("#" + entry + " .status").html(data.status);
                                    }
                                });
                            }

                            if (data.result == true) {
                                $(".alert").removeClass('alert-danger').addClass('alert-success');
                            } else {
                                $(".alert").removeClass('alert-success').addClass('alert-danger');
                            }

                            $("#delayModal").modal("hide");

                            window.scrollTo(0, 0);
                            $(".alert").html(data.message);
                            $(".alert").show();
                            window.setTimeout(function () {
                                $(".alert").hide();
                            }, 3000);

                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }
            });

            $('body').on('click', '#delivery_problems', function (e) {

                check_validation({
                    type: 'problems'
                });

            });

            $('body').on('click', '#deliver_orders', function (e) {

                check_validation({
                    type: 'deliver'
                });

            });

            $('body').on('click', '#receive_orders', function (e) {
                check_validation({
                    type: 'receive'
                });

            });

            $('body').on('click', '#recall_orders', function (e) {

                check_validation({
                    type: 'recall'
                });

            });

            $('body').on('click', '#cancel_orders', function (e) {
                check_validation({
                    type: 'cancel'
                });
            });

            $('body').on('click', '#forward', function (e) {
                check_validation({
                    type: "forward"
                });

            });

            $('body').on('click', '#delay_orders', function (e) {
                check_validation({
                    type: "delay"
                });

            });

            $("#problems_form").submit(function (event) {
                event.preventDefault();

                if ($("#driver_id").val() && $("#delivery_problem_id").val()) {
                    change_status({
                        type: 'problems',
                        driver_id: $("#driver_id").val(),
                        latitude: $("#receiver_latitude").val(),
                        longitude: $("#receiver_longitude").val(),
                        delivery_problem_id: $("#delivery_problem_id").val(),
                        reason: $("#reason").val()
                    });

                    $("#problemModal").modal("hide");
                }
            });

            $('body').on('click', '#forward_action', function (e) {
                if ($("#driver").val()) {
                    change_status({
                        type: 'forward',
                        driver_id: $("#driver").val()
                    });

                    $("#forwardModal").modal("hide");
                }
            });

            $('#delay_date').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                language: 'en',
                todayHighlight: true,
                startDate: new Date()
            });

            $('body').on('click', '.printdata', function (e) {

                var url = "{{URL::asset('/mngrAdmin/print_pdf/')}}/" + $(this).attr('data-id');
                $('<form action="' + url + '" method="get" target="_blank"></form>').appendTo('#orders').submit();
            });

        });

        $('body').on('click', '#pdf-submit', function (e) {

            let ids = [];

            $(".selectpdf input:checked").each(function () {
                ids.push($(this).val());

            });
            if (ids.length > 0) {
                $.ajax({
                    url: "{{ url('/mngrAdmin/scan_validation')}}",
                    type: 'post',
                    data: {ids: ids, '_token': "{{csrf_token()}}", type: 'print'},
                    success: function (data) {
                        if (data.result == false) {
                            a.open();
                        } else {
                            $("#print_form").html('');
                            $('<form action="{{ url("/mngrAdmin/scan_print/")}}" method="post" target="_blank"><input value="' + ids + ' " name="ids"  /><input value="' + $("#driver_filter").val() + ' " name="driver_id"  />" {{csrf_field()}}"</form>').appendTo('#print_form').submit();
                        }
                    },
                    error: function (data) {
                        a.open();
                    }
                });
            }
        });

        $('body').on('click', '#toggle', function () {

            switch ($(this).attr('data-state')) {
                case "1" :
                    $(".selectOrder").prop('checked', true).trigger('change');
                    $(this).attr('data-state', "2");
                    break;
                case "2" :
                    $(".selectOrder").prop('checked', false).trigger('change');
                    $(this).attr('data-state', "1");
                    break;
            }
        });

    </script>

    <script type="text/javascript">
        var page = 1;
        var init = 1;
        var count = 0;
        var code_scan = "";

        $(function () {
            $('#code-scan').codeScanner({
                onScan: function ($element, code) {
                    code_scan = code;

                    getData(page);

                }
            });

            $("#scan-btn").on('click', function () {
                // page = 1;
                // location.hash = page;
                code_scan = $("#code-scan").val();
                getData(page);
            });

            // $("#status, #driver_filter").on('change', function () {
            //     init = 1;
            //     count = 0;
            //     $("#code-scan").val('');
            //     $('.list').empty().html('');
            // });

            $(window).on('hashchange', function () {
                if (window.location.hash) {
                    page = window.location.hash.replace('#', '');
                    if (page == Number.NaN || page <= 0) {
                        return false;
                    } else {
                        getData(page);
                    }
                }
            });

            $(document).on('click', '.pagination a', function (event) {
                event.preventDefault();

                $('li').removeClass('active');
                $(this).parent('li').addClass('active');

                var myurl = $(this).attr('href');
                var page = $(this).attr('href').split('page=')[1];

                //getData(page);
                location.hash = page;
            });


        });

        var open = false;
        var a = $.confirm({
            lazyOpen: true,
            animation: 'zoom',
            title: '',
            theme: 'material',
            content: '' +
                '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c">' +
                '<i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">' +
                '{{__("backend.scan_order")}}</span></div><div style="font-weight:bold;">' +
                '<p>{{__("backend.bad_choice")}}</p>' +
                '</div>',
            type: 'red',
            onOpen: function () {
                // after the modal is displayed.
                open = true;
            },
            onClose: function () {
                // after the modal is displayed.
                open = false;
            },
        });

        function getData(page) {
            console.log(page);
            $("#tempo").show();
            if (init == 1) {
                $('.list').html('');
            }

            if (open) {
                a.close();
            }

            $.ajax({
                url: '{{URL::asset("/mngrAdmin/scan_qrcode")}}' + '?code=' + code_scan + '&page=' + page + '&init=' + init,
                type: 'get',
                success: function (data) {

                    $("#tempo").hide();

                    if (init == 1) {
                        if (data.count != 0) {
                            init = 2;
                            count++;
                        }
                        $('.list').empty().html(data.view);
                    } else {
                        if (data.count != 0) {
                            const myEle = document.getElementById(data.id);
                            if (!myEle) {
                                count++;
                                $("#tbody").append(data.view);
                            }

                        }
                    }

                    $(".toggle").bootstrapToggle('destroy');
                    $(".toggle").bootstrapToggle();
                    location.hash = page;

                    if (data.count != 0) {
                        $("#" + data.id + " .serial").html(count);

                        let code_val = $("#code-scan").val();
                        if (code_val) {
                            if (!code_val.includes(code_scan)) {
                                code_val += ',' + code_scan;
                                getData(page);
                            }
                        } else {
                            getData(page);
                            code_val = code_scan;
                        }

                        $("#code-scan").val(code_val);
                    } else {
                        a.open();
                    }
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }

    </script>
@endsection

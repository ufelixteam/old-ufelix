<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use App\Http\Requests;
use App\Models\City;
use App\Models\Governorate;
use Auth;
use File;
use Flash;
use Illuminate\Http\Request;
use Response;

class CityController extends Controller
{

    // Get All Cities By Country ID
    public function get_cities($id)
    {
        return City::where('governorate_id', $id)->get();
    }

    // Get All Cities
    public function index(Request $request)
    {
        $filter = app('request')->input('filter'); // search By Filter City
        $cities = City::orderBy('id', 'desc');

        // Check If Active Or Block Filter Or Search By Name
        if (isset($filter) && $filter != "" && $filter != -1) {
            $cities = $cities->where('governorate_id', $filter);
        }

        $list_governorates = Governorate::orderBy('name_en', 'ASC')->get();
        $cities = $cities->paginate(15);

        if ($request->ajax()) {
            return [
                'view' => view('backend.cities.table', compact('cities'))->render(),
            ];
        } else {
            return view('backend.cities.index', compact('cities', 'list_governorates'));
        }
    }

    // Create City Page
    public function create()
    {
        $list_governorates = Governorate::orderBy('name_en', 'ASC')->get();
        return view('backend.cities.create', compact('list_governorates'));
    }

    // Added New City
    public function store(Request $request)
    {
        /*
        *	status = 0 :: Not Verify
        *	status = 1 :: Verify
        */
        $v = \Validator::make($request->all(), [
            'city_name' => 'required',
            'name_en' => 'required',
            'governorate_id' => 'required',
            'status' => 'required',
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        $city = new City();
        $city->city_name = $request->input("city_name");
        $city->name_en = $request->input("name_en");
        $city->governorate_id = $request->input("governorate_id");
        $city->status = $request->input("status");
        $city->save();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Add New City ' . $city->name_en, ' اضافة مدينة جديدة ' . $city->city_name, 28); // Helper Function in App\Helpers.php

        Flash::success(__('backend.City_Added_successfully'));
        return redirect()->route('mngrAdmin.cities.index');
    }

    // View One City
    public function show($id)
    {
        return back();
        // $city 	=		 City::findOrFail($id);
        // return view('backend.cities.show', compact('city'));
    }

    // Edit City Page
    public function edit($id)
    {
        $city = City::findOrFail($id);
        $list_governorates = Governorate::orderBy('name_en', 'ASC')->get();
        return view('backend.cities.edit', compact('city', 'list_governorates'));
    }

    // Update City
    public function update(Request $request, $id)
    {
        /*
        *	status = 0 :: Not Verify
        *	status = 1 :: Verify
        */
        $v = \Validator::make($request->all(), [
            'city_name' => 'required',
            'name_en' => 'required',
            'governorate_id' => 'required',
            'status' => 'required',
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        $city = City::findOrFail($id);
        $city->city_name = $request->input("city_name");
        $city->name_en = $request->input("name_en");
        $city->governorate_id = $request->input("governorate_id");
        $city->status = $request->input("status");
        $city->save();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Edit City ' . $city->name_en, ' تعديل المدينة ' . $city->city_name, 28); // Helper Function in App\Helpers.php

        Flash::warning(__('backend.City_Updated_successfully'));
        return redirect()->route('mngrAdmin.cities.index');
    }

    // Delete City
    public function destroy($id)
    {
        $city = City::findOrFail($id);
        $city->delete();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Delete City ' . $city->name_en, ' حذف المدينة ' . $city->city_name, 28); // Helper Function in App\Helpers.php

        flash(__('backend.City_deleted_successfully'))->error();
        return redirect()->route('mngrAdmin.cities.index');
    }

}

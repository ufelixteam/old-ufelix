@extends('agent.layouts.app')

@section('header')
  <div class="page-header clearfix">
    <h3>
      <i class="glyphicon glyphicon-align-justify"></i> Drivers
      <div class="btn-group pull-right" role="group" aria-label="...">
        <a class="btn btn-success btn-group" role="group"  href="{{ route('mngrAgent.drivers.create') }}"><i class="glyphicon glyphicon-plus"></i> Add New</a>
      </div>
    </h3>
  </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            @if($drivers->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>NAME</th>
                            <th>MOBILE</th>
                            <th>Status</th>
                            <th>IS ACTIVE</th>
                            <!--<th>Status</th>-->

                            <th>CITY</th>

                            <th class="text-right"></th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($drivers as $driver)
                            <tr>
                                <td>{{$driver->id}}</td>
                                <td>{{$driver->name}}</td>
                                <td>{{$driver->mobile}}</td>
                                <td>{!! $driver->status_span !!}</td>

                                <td>{!! $driver->active_span !!}</td>
                                <!--<td>{!! $driver->status_span !!}</td>-->

                                <td>{{! empty($driver->city) ? $driver->city->city : ''}}</td>

                                <td class="text-right">
                                    <a class="btn btn-xs btn-default" href="{{ url('mngrAgent/orders/ShowOrders') . '/1/' . $driver->id }}"><i class="glyphicon glyphicon-list"></i> Orders</a>

                                    <a class="btn btn-xs btn-primary" href="{{ route('mngrAgent.drivers.show', $driver->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    <form action="{{ url('mngrAgent/del', $driver->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                 
            @else
                <h3 class="text-center alert alert-warning">No Result Found !</h3>
            @endif

        </div>
    </div>
  </div>
@endsection

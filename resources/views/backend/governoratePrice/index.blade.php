@extends('backend.layouts.app')

@section('css')
    <title>{{__('backend.shipment_destinations')}}</title>
@endsection

@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.shipment_destinations')}}
        @if(permission('addShipmentDestinations')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
            <a class="btn btn-success pull-right" href="{{ route('mngrAdmin.governoratePrices.create') }}"><i
                    class="glyphicon glyphicon-plus"></i> {{__('backend.add_shipment_destinations')}}</a>
            @endif
        </h3>
    </div>
@endsection

@section('content')
    {{--    <div class="row">--}}
    {{--        <div class="col-md-12 col-xs-12 col-sm-12">--}}
    {{--            @if($governoratePrices->count())--}}
    {{--                <table class="table table-condensed table-striped text-center">--}}
    {{--                    <thead>--}}
    {{--                    <tr>--}}
    {{--                        <th>#</th>--}}
    {{--                        <th>{{__('backend.from')}}</th>--}}
    {{--                        <th></th>--}}
    {{--                        <th>{{__('backend.to')}}</th>--}}
    {{--                        <th>{{__('backend.cost')}}</th>--}}
    {{--                        <th>{{__('backend.status')}}</th>--}}
    {{--                        <th class="pull-right"></th>--}}
    {{--                    </tr>--}}
    {{--                    </thead>--}}
    {{--                    <tbody>--}}
    {{--                    @foreach($governoratePrices as $governoratePrice)--}}
    {{--                        <tr>--}}
    {{--                            <td>{{$governoratePrice->id}}</td>--}}
    {{--                            <td>{{$governoratePrice->Start->name_en}}</td>--}}
    {{--                            <td><i class="glyphicon glyphicon-arrow-right" style="color: blue;"></i></td>--}}
    {{--                            <td>{{$governoratePrice->End->name_en}}</td>--}}
    {{--                            <td>{{! empty($governoratePrice->cost) ? $governoratePrice->cost : 'Not Specified'}}</td>--}}
    {{--                            <td>{!! $governoratePrice->status_span !!}</td>--}}
    {{--                            <td class="pull-right">--}}
    {{--                            --}}{{--<a class="btn btn-xs btn-primary" href="{{route('mngrAdmin.governoratePrices.show', $governoratePrice->id)}}"><i class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>--}}
    {{--                            @if(permission('editShipmentDestinations')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->--}}
    {{--                                <a class="btn btn-xs btn-warning"--}}
    {{--                                   href="{{ route('mngrAdmin.governoratePrices.edit', $governoratePrice->id)}}"><i--}}
    {{--                                        class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>--}}
    {{--                            @endif--}}
    {{--                            @if(permission('deleteShipmentDestinations')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->--}}
    {{--                                <form action="{{ route('mngrAdmin.governoratePrices.destroy', $governoratePrice->id) }}"--}}
    {{--                                      method="POST" style="display: inline;"--}}
    {{--                                      onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false }">--}}
    {{--                                    <input type="hidden" name="_method" value="DELETE">--}}
    {{--                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
    {{--                                    <button type="submit" class="btn btn-xs btn-danger"><i--}}
    {{--                                            class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}</button>--}}
    {{--                                </form>--}}
    {{--                                @endif--}}
    {{--                            </td>--}}
    {{--                        </tr>--}}
    {{--                    @endforeach--}}
    {{--                    </tbody>--}}
    {{--                </table>--}}

    {{--                {!! $governoratePrices->appends($_GET)->links() !!}--}}

    {{--            @else--}}
    {{--                <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>--}}
    {{--            @endif--}}
    {{--        </div>--}}
    {{--    </div>--}}

    <div class="row">

        <div class="form-group col-sm-12">
            @if($governoratePrices->count())
                <table class="table table-condensed table-striped text-center">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{__('backend.from')}}</th>
                        <th></th>
                        <th>{{__('backend.to')}}</th>
                        <th>{{__('backend.cost')}}</th>
                        <th>{{__('backend.recall_cost')}}</th>
                        <th>{{__('backend.reject_cost')}}</th>
                        <th>{{__('backend.cancel_cost')}}</th>
                        <th>{{__('backend.status')}}</th>
                        <th class=""></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($governoratePrices as $governoratePrice)
                        <tr>
                            <td>{{$governoratePrice->id}}</td>
                            <td>{{$governoratePrice->Start->name_en}}</td>
                            <td><i class="glyphicon glyphicon-arrow-right" style="color: blue;"></i></td>
                            <td>{{$governoratePrice->End->name_en}}</td>
                            <td>
                                <input type="number" class="form-control cost" name="cost"
                                       value="{{$governoratePrice->cost}}" id="cost_{{$governoratePrice->id}}"
                                       data-id="{{$governoratePrice->id}}">
                            </td>
                            <td>
                                <input type="number" class="form-control recall_cost" name="recall_cost"
                                       value="{{$governoratePrice->recall_cost}}"
                                       id="recall_cost_{{$governoratePrice->id}}"
                                       data-id="{{$governoratePrice->id}}">
                            </td>
                            <td>
                                <input type="number" class="form-control reject_cost" name="reject_cost"
                                       value="{{$governoratePrice->reject_cost}}"
                                       id="reject_cost_{{$governoratePrice->id}}"
                                       data-id="{{$governoratePrice->id}}">
                            </td>
                            <td>
                                <input type="number" class="form-control cancel_cost" name="cancel_cost"
                                       value="{{$governoratePrice->cancel_cost}}"
                                       id="cancel_cost_{{$governoratePrice->id}}"
                                       data-id="{{$governoratePrice->id}}">
                            </td>
                            <td>{!! $governoratePrice->status_span !!}</td>
                            <td class="">
                            @if(permission('editShipmentDestinations')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->

                                <a class="btn btn-xs btn-success save_cost disabled"
                                   href="#" data-id="{{$governoratePrice->id}}"
                                   id="save_cost_{{$governoratePrice->id}}"><i
                                        class="glyphicon glyphicon-edit"></i> {{__('backend.save')}}</a>

                                <a class="btn btn-xs btn-warning"
                                   href="{{ route('mngrAdmin.governoratePrices.edit', $governoratePrice->id)}}"><i
                                        class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>

                            @endif
                            @if(permission('deleteShipmentDestinations')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <form action="{{ route('mngrAdmin.governoratePrices.destroy', $governoratePrice->id) }}"
                                      method="POST" style="display: inline;"
                                      onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="submit" class="btn btn-xs btn-danger"><i
                                            class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}</button>
                                </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                {{--                {!! $governoratePrices->appends($_GET)->links() !!}--}}

            @else
                <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
            @endif
        </div>
    </div>
@endsection
@section('scripts')
    {{--    <script type="text/javascript">--}}
    {{--        $('#theTable').DataTable({--}}
    {{--            "pagingType": "full_numbers"--}}
    {{--        });--}}
    {{--    </script>--}}
    <script>
        $(function () {
            $('.cost, .recall_cost, .cancel_cost, .reject_cost').on('input', function () {
                $("#save_cost_" + $(this).attr('data-id')).removeClass('disabled');
            });

            $('.save_cost').on('click', function (e) {
                e.preventDefault();
                var self = $(this);
                let id = $(this).attr('data-id');
                $.ajax({
                    url: '{{URL::asset("/mngrAdmin/governoratePrices/save_cost")}}',
                    type: 'post',
                    data: {
                        id: id,
                        cost: $("#cost_" + id).val(),
                        recall_cost: $("#recall_cost_" + id).val(),
                        reject_cost: $("#reject_cost_" + id).val(),
                        cancel_cost: $("#cancel_cost_" + id).val(),
                        _token: "{{csrf_token()}}"
                    },
                    success: function (data) {
                        self.addClass('disabled');
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            })
        });
    </script>
@endsection

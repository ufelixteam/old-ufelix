@extends('agent.layouts.app')
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<style>
    .jconfirm.jconfirm-modern .jconfirm-box div.jconfirm-title-c .jconfirm-icon-c {
    -webkit-transition: -webkit-transform .5s;
    transition: -webkit-transform .5s;
    transition: transform .5s;
    transition: transform .5s,-webkit-transform .5s;
    -webkit-transform: scale(0);
    transform: scale(0);
    display: block;
    margin-right: 0;
    margin-left: 0;
    margin-bottom: 10px;
    font-size: 69px;
    color: #9e1717;
}
.tab-content
{
  padding-top: 20px;
}
.nav-tabs li:first-child.active a
{
  color: #fff;background-color: #337ab7;
}
.nav-tabs li:last-child.active a
{
  color: #fff;background-color: #dd4b39;
}
</style>
@endsection
@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> {{ ! empty($title) ? $title : ''}} Requests
        </h3>
    </div>
@endsection

@section('content')

<div>
  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
      <div class="row">
          <div class="col-md-12 col-xs-12 col-sm-12">
              @if(count($orders) > 0)
              <table class="table table-condensed table-striped text-center">
                <thead>
                  <tr>
                    <th>#</th>

					<th>Order Price</th>
                    <th>Receiver Name</th>
                    <th>Receiver Mobile</th>
                    <th>Sender Name</th>
                    <th>Sender Mobile</th>
                    @if((! empty($type)) && $type == "1")
              		<th>Max Date</th>
              		@endif
                    <th>Forward</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($orders as $order)
                    <tr>
                      	<td>{{$order->id}}</td>
	                    <td>{{$order->order_price . ' '.'LE'}}</td>
	                    <td>{{$order->receiver_name}}</td>
	                    <td>{{$order->receiver_mobile}}</td>
	                    <td>{{$order->sender_name}}</td>
	                    <td>{{$order->sender_mobile}}</td>
               			@if((! empty($type)) && $type == "1")
	              			<th>{{$order->max_time}}</th>
	              		@endif

                      	<td>
                   			<a class="btn btn-xs btn btn btn-success" href="#" data-toggle="modal" data-target="#{{$order->id}}"><i class="glyphicon glyphicon-eye-open"></i> Forward</a>

                      </td>
                    </tr>

                    @if(! empty($online_drivers))
		              <div class="modal fade" id="{{$order->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		                <div class="modal-dialog" role="document">
		                  <div class="modal-content">
		                    <div class="modal-header">
		                      <h5 class="modal-title" id="exampleModalLabel">Order ID # {{$order->id}}</h5>
		                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                        <span aria-hidden="true">&times;</span>
		                      </button>
		                    </div>
		                    <div class="modal-body">
		                      <form class="" action="{{ url('mngrAgent/send_to_other_driver') }}" method="post">
		                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
		                        <div class="form-group">
		                          <input type="hidden" class="form-control" name="order_type_id" value="{{$order->order_type_id}}">
		                          <input type="hidden" class="form-control" name="order_id" value="{{$order->id}}">
		                          <input type="hidden" class="form-control" name="governorate_cost_id" value="{{$order->governorate_cost_id}}">
		                        </div>
		                        <div class="form-group">
		                           <select id="items-field" name="items" id="items" class="form-control">
		                                @foreach($online_drivers as $driver)
		                                    <option value="{{$driver->id}}"
		                                    >{{$driver->name}}</option>
		                                @endforeach
		                            </select>
		                        </div>
		                        <button type="submit" class="btn btn-primary">submit</button>
		                      </form>
		                    </div>
		                  </div>
		                </div>
		              </div>
		              @endif

                  @endforeach
                </tbody>
              </table>
              {{$orders->links()}}
              @else
                  <h3 class="text-center alert alert-warning">No Result Found !</h3>
              @endif
          </div>
      </div>
      <!-- end -->
    </div>
@endsection

@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<script>
$('#myTabs a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})


</script>

@endsection


@extends('frontend.profile.layouts.app')

@section('title') @lang('website.UploadExcel') @endsection

@section('content')

    <div class="row">
        <div class="col-md-4 bgwhite table-responsive text-center mb-50">
            <h1 class="title"> @lang('website.UploadExcel')</h1>
                <form
                    action="{{ app()->getLocale() == 'en' ? url('/en/customer/account/upload-excel') : url('/customer/account/upload-excel') }}"
                    method="post" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="card">
                            <div class="card-body">
                                <img src="{{asset('assets/images/upload.png')}}" class="uploadImg">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <h4 class="card-title">@lang('website.FileUpload')</h4>
                                <label for="input-file-max-fs mb-50" style="margin-bottom: 50px !important"> @lang('website.MaxSize')</label>
                                <input type="file" name="file" id="input-file-max-fs" class="dropify"
                                       data-max-file-size="2M"
                                       accept=".xls,.xlsx,.xlsm,.xlsb,.xml,application/ms-excel,application/vnd.ms-excel"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-default" -->
                                <!-- data-dismiss="modal">@lang('website.Close')</button> -->
                        <input type="submit" class="btn btn-primary" value="Save">
                    </div>
                </form>

        </div>
        <div class="col-md-8 text-center">
            <div class="">
                <div class="table-responsive m-t-40">
                    <div class="button-box pull-right">
                        <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"
                                data-whatever="@fat">@lang('website.UploadSheetExcel')</button> -->
                    </div>
                    <div class="button-box pull-left">
                        <button type="button" class="btn btn-info "><a href="#" style="color: #fff"
                                                                       href="{{ asset('/backend/sheet.xlsx')}}"
                                                                       download="sheet.xlsx">@lang('website.Download')</a></button>
                    </div>
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered"
                           cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>@lang('website.ID')</th>
                            <th>@lang('website.File')</th>
                            <th>@lang('website.ExcelCreatedAt')</th>
                            <th>@lang('website.Actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(! empty($orders))
                            @foreach ($orders as $file)
                                <tr>
                                    <th scope="row">{{$file->id}}</th>
                                    <td>{{$file->file_name}}</td>
                                    <td>{{$file->created_at}}</td>
                                    <td>
                                        <a href="{{ asset('/api_uploades/client/corporate/sheets/'.$file->file_name) }}"
                                           download="{{ asset('/api_uploades/client/corporate/sheets/'.$file->file_name) }}"
                                           class="text-inverse p-r-10" data-toggle="tooltip" title=""
                                           data-original-title="show" aria-describedby="tooltip468946">
                                            <i class="icon-download"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>



    <!-- sample modal content -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">@lang('website.UploadExcelSheets')</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>

                </div>
          
            </div>
        </div>
    </div>
    <!-- /.modal -->

@endsection

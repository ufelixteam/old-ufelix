<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShipmentOrderItem extends Model
{
    public function tool() {
        return $this->belongsTo('App\Models\ShipmentTool','tool_id');
    }
}

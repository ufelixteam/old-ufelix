@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.the_orders')}}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">

    <style type="text/css">
        @media only screen and (max-width: 580px) {
            .hide-td {
                display: none;
            }
        }

        .box-body {
            min-height: 100px;
        }

        .stylish-input-group .input-group-addon {
            background: white !important;
        }

        .stylish-input-group .form-control {
            border-right: 0;
            box-shadow: 0 0 0;
            border-color: #ccc;
        }

        .stylish-input-group button {
            border: 0;
            background: transparent;
        }
    </style>
@endsection

@section('header')
    <div class="page-header clearfix">
        <h3 style="display: inline-block">
            <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.list_receivers')}}
        </h3>

    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <form>
                <div class="row" style="margin-bottom: 50px">
                    <div class="col-md-4">
                        <div id="imaginary_container">
                            <div class="input-group stylish-input-group">
                                <input type="text" class="form-control" id="search-field" name="search" value="{{app('request')->input('search')}}"
                                       placeholder="{{__('backend.search')}} ">
                                <span class="input-group-addon">
                      <button type="submit" id="search-btn1">
                        <span class="glyphicon glyphicon-search"></span>
                      </button>
                    </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <a href="{{url('/mngrAdmin/list_receivers?export=true'.(app('request')->search ? '&search='.app('request')->search : ''))}}"
                           class="btn btn-primary btn-block pull-left"> {{__('backend.exportExcel')}} </a>

                    </div>
                </div>
            </form>

            @if(! empty($orders) && count($orders) > 0)
                <div class="table-responsive">
                    <table id="theTable" class="table table-condensed table-striped text-center"
                           style="min-height: 200px;">
                        <thead>
                        <tr>
                            <th>Receiver Name</th>
                            <th>Receiver Mobile</th>
                            <th>Receiver Phone</th>
                            <th>Total Orders</th>
                            <th>Delivered Orders</th>
                            <th>Delivery Rate</th>
                            <th>Whitelist</th>
                            <th></th>
                        </tr>

                        </thead>
                        <tbody>
                        @foreach($orders as $order)
                            @php
                                $rate = $order->total_orders ? ($order->delivered_orders / $order->total_orders) * 100 : 0;
                            @endphp
                            <tr>
                                <td>{{$order->receiver_name}}</td>
                                <td>{{$order->receiver_mobile}}</td>
                                <td>{{$order->receiver_phone}}</td>
                                <td>
                                    <a target="_blank"
                                        href="{!! URL::asset('mngrAdmin/orders/all?receiver_mobile='.htmlentities($order->receiver_mobile).'&receiver_phone='.htmlentities($order->receiver_phone)) !!}">
                                        <span>{{$order->total_orders}}</span>
                                    </a>
                                </td>
                                <td>{{$order->delivered_orders}}</td>
                                <td>{{$rate}}</td>
                                <td>
                                    @if(( $rate >= 0 ) && ($rate <= 30))
                                        <span class="badge badge-pill label-danger">BlackList</span>
                                    @elseif(( $rate > 30 ) && ($rate <= 60))
                                        <span class="badge badge-pill label-warning">MediumList</span>
                                    @elseif(( $rate > 60 ) && ($rate <= 100))
                                        <span class="badge badge-pill label-success">WhiteList</span>
                                    @endif
                                </td>
                                <td>
                                    <a target="_blank"
                                       href="{!! URL::asset('mngrAdmin/orders/all?receiver_mobile='.htmlentities($order->receiver_mobile).'&receiver_phone='.htmlentities($order->receiver_phone)) !!}">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </td>
                            </tr>

                        @endforeach
                        </tbody>

                    </table>
                    {!! $orders->appends($_GET)->links() !!}
                </div>

            @endif
        </div>
    </div>

@endsection


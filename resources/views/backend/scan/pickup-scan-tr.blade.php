@if($pickup)
    <tr id="{{$pickup->id}}">
        <td class="serial"></td>
        <td>{{$pickup->id}}</td>
        <td>{{$pickup->pickup_number}}</td>
        <td>{{$pickup->collection_id ? $pickup->collection_id : '-'}}</td>
        <td>{{$pickup->corporate->name}}</td>
        <td>{{$pickup->driver ? $pickup->driver->name : '-'}}</td>
        <td style="font-weight: bold; color: #a43;">{!! $pickup->task_types !!}</td>
        <td class="status">{!!  $pickup->status_span !!}</td>
        <td class="selectpdf" id="selectpdf">
            <input type="checkbox" class="selectOrder" @if($pickup->status == 3) disabled @endif
                   id="pickup_{{$pickup->id}}"
                   name="select[]" value="{{$pickup->id}}"
                   data-value="{{$pickup->id}}"/>
        </td>
        <td>
            <a href="#"
               class="btn btn-secondary dropPickup"
               data-id="{{$pickup->id}}">{{__('backend.drop')}}</a>
            <a href="{{ route('mngrAdmin.pickups.show', $pickup->id ) }}" class="btn btn-primary"
               target="_blank">
                {{__('backend.view')}}</a>
{{--            <div class="dropdown">--}}
{{--                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"--}}
{{--                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">--}}
{{--                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>--}}
{{--                </button>--}}
{{--                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">--}}
{{--                    <li>--}}
{{--                        <a href="{{ route('mngrAdmin.pickups.show', $pickup->id ) }}"><i--}}
{{--                                class="fa fa-eye"></i> {{__('backend.view')}}</a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </div>--}}
        </td>
    </tr>
@endif

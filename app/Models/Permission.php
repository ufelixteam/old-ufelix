<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{

    public function permissionAdmin(){
        return $this->belongsToMany('App\Models\Permission_admin', 'permission_id', 'id');
    }

    public function typePermission(){
        return $this->belongsTo('App\Models\TypePermission', 'type_permission_id', 'id');
    }

}

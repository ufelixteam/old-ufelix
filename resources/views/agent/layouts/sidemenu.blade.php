<div id="sidebar-nav" class="sidebar">
	<div class="sidebar-scroll">
		<nav>
			<ul class="nav">
				<li><a href="{{url('mngrAgent')}}" class="{{ Request::is('mngrAgent/dashboard')  ? 'active' : '' }}">
                        <i class="lnr lnr-home"></i> <span>{{__('backend.dashboard')}}</span>
                    </a>
				</li>
                <li>
                    <a href="{!! route('mngrAgent.drivers.index') !!}" class="{{ Request::is('mngrAgent/drivers*')  || Request::is('mngrAgent/orders/ShowOrders*')  ? 'active' : '' }}"
                    ><i class="fa fa-car"></i><span>@lang('backend.drivers')</span>
                    </a>
                </li>

                <li >  <a class="{{ Request::is('mngrAgent/track_drivers/all') ? 'active' : '' }}"
                     href="{{URL::asset('/mngrAgent/track_drivers/all')}}"><i class="fa fa-map"></i><span>@lang('backend.map')</span> </a>
                </li>

                <li class="{{ Request::is('mngrAgent/all_orders')  ? 'active' : '' }}">
                    <a href="{{ url('mngrAgent/all_orders') }}"><i class="fa fa-archive"></i><span>@lang('backend.orders')</span></a>
                </li>

                <li>  <a class="{{ Request::is('mngrAgent/pending_orders')  ? 'active' : '' }}"
                    href="{{ url('mngrAgent/pending_orders') }}"><i class="fa fa-info"></i><span>@lang('backend.Pending_Orders')</span></a>
                </li>

                <li>  <a  class = "{{ Request::is('mngrAgent/waiting_orders')  ? 'active' : '' }}"
                     href="{{ url('mngrAgent/waiting_orders') }}"><i class="fa fa-hourglass-start "></i><span>@lang('backend.Waiting_Orders')</span></a>
                </li>


                <li>
                    <a href="#catPages" data-toggle="collapse" class="{{  Request::is('mngrAgent/invoices*')   ? 'active' : 'collapsed' }} "
                       aria-expanded="{{  Request::is('mngrAgent/invoices*')   ? 'true' : 'false' }} ">
                        <i class="fa fa-usd"></i> <span>{{__('backend.invoices')}}</span>
                        <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                    <div id="catPages" class="collapse {{   Request::is('mngrAgent/invoices*')   ? 'in' : '' }}" aria-expanded="{{  Request::is('mngrAgent/invoices*')   ? 'true' : 'false' }}">
                        <ul class="nav">
                            <li>
                                <a href="{{url('mngrAgent/invoices/1')}}"><i class="fa fa-check"></i><span>My Invoices </span></a>
                            </li>
                            <li>
                                <a href="{{url('mngrAgent/invoices/2')}}"><i class="fa fa-info"></i><span>Drivers</span></a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li  > <a class="{{ Request::is('mngrAgent/shipment_tool_orders*') ? 'active' : '' }}"
                    href="{!! route('mngrAgent.shipment_tool_orders.index') !!}"><i class="fa fa-gavel"></i><span>Shipment Materials</span></a>
                </li>

                <li>
                    <a href="{!! url('/mngrAgent/logout') !!}"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out"></i> Sign out
                    </a>
                    <form id="logout-form" action="{{ url('/mngrAgent/logout') }}" method="POST"
                          style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>

            </ul>
		</nav>
	</div>
</div>

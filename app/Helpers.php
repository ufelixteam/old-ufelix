<?php
// use Illuminate\Support\Facades\URL;

use App\Models\User;

function checkWarehouse($user_id, $store_id)
{
    $count = User::join('store_user', 'store_user.user_id', '=', 'users.id')
        ->where('store_user.user_id', $user_id)
        ->where('store_user.store_id', $store_id)
        ->count();

    return $count ? true : false;
}

if (!function_exists('random_number')) {
    function random_number($type = 1, $user_id = '')
    {

        $number = '';
        $time = time();

        $pool = mt_rand(1, 9999999999) . $time . $user_id;
        $pool2 = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

        if ($type == 1) {
            //$number = substr(str_shuffle(str_repeat($pool, 5)), 0, 4);//substr(uniqid(sha1($pool)), 0, 4);
            //$number = bin2hex($number);
            $number = substr(str_shuffle(str_repeat(md5($pool2), 5)), 0, 8);
        } else if ($type == 2) {
            $number = substr(str_shuffle(str_repeat($pool, 5)), 0, 10);
//            $number = substr(str_shuffle(str_repeat($pool, 5)), 0, 5);
//            $number = substr(str_shuffle(str_repeat($pool, 5)), 0, 10);
//            $number = bin2hex($number);
        } elseif ($type == 3) {
            $number = str_pad(mt_rand(1, 99999999), 10, '0', STR_PAD_LEFT);
        }

        return $number;

    }
}

// This Function For Add New Activity Log In Any Action
function addActivityLog($subject_en, $subject_ar, $activity_type)
{
    $activityLog = new App\Models\Activity_log;
    $activityLog->object_id = auth()->user()->id;
    $activityLog->object_name = auth()->user()->name;
    if (App\Models\User::where('email', auth()->user()->email)->exists()) {
        $activityLog->object_type = 1;     // Admin = 1
    } else {
        $activityLog->object_type = 2;   // Agent = 2
    }
    $activityLog->action = \URL::current();
    $activityLog->subject_en = $subject_en;
    $activityLog->subject_ar = $subject_ar;
    $activityLog->activity_type = $activity_type;

    $activityLog->save();
}


// This Function Displays All The Buttons For All Peoples Have The Permission
function permission($permissionName)
{
    if (\App\Models\Permission_admin::where('role_admin_id', auth()->user()->role_admin_id)->where('permission_id', \App\Models\Permission::where('name', $permissionName)->value('id'))->exists() && \App\Models\Permission::where('name', $permissionName)->exists()) {
        return 'true';
    }
}

if (!function_exists('change_locale_url')) {

    function change_locale_url()
    {
        $url = '';
        $params = $_GET;
        array_shift($params);

        $segments = explode('/', trim(parse_url((isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : ''), PHP_URL_PATH), '/'));
        if (isset($segments[0]) && in_array($segments[0], ['en', 'ar'])) {
            array_shift($segments);
        }

        foreach ($segments as $segment) {
            $url .= '/' . $segment;
        }

        $i = 0;
        $len = count($params);
        foreach ($params as $key => $value) {
            if ($i == 0) {
                $url .= '?';
            }
            $url .= $key . '=' . $value;

            if ($i !== $len - 1) {
                $url .= '&';
            }

            $i++;
        }

        $lang = app()->getLocale() == 'en' ? 'ar' : 'en';
        $url = '/' . $lang . $url;

        return $url;

    }
}


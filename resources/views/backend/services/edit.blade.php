@extends('backend.layouts.app')
@section('css')

@endsection
@section('header')
<div class="page-header">
    <h3><i class="fa fa-edit"></i>  {{__('backend.EDIT')}} #{{$service->id}}</h3>
</div>
@endsection

@section('content')

<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">

        <form action="{{ route('mngrAdmin.services.update', $service->id) }}" method="POST" enctype='multipart/form-data'>
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="form-group col-md-6 col-sm-6 col-xs-12 @if($errors->has('title_ar')) has-error @endif">
               <label for="title_ar-field">@lang('backend.title_ar')</label>
            <input type="text" id="title_ar-field" name="title_ar" class="form-control" value="{{ is_null(old("title_ar")) ? $service->title_ar : old("title_ar") }}"/>
               @if($errors->has("title_ar"))
                <span class="help-block">{{ $errors->first("title_ar") }}</span>
               @endif
            </div>
            <div class="form-group col-md-6 col-sm-6 col-xs-12 @if($errors->has('title_en')) has-error @endif">
               <label for="title_en-field">@lang('backend.title_en')</label>
            <input type="text" id="title_en-field" name="title_en" class="form-control" value="{{ is_null(old("title_en")) ? $service->title_en : old("title_en") }}"/>
               @if($errors->has("title_en"))
                <span class="help-block">{{ $errors->first("title_en") }}</span>
               @endif
            </div>
            <div class="form-group col-md-6 col-sm-6 col-xs-12 @if($errors->has('small_description_ar')) has-error @endif">
               <label for="small_description_ar-field">@lang('backend.small_description_ar')</label>
            <input type="text" id="small_description_ar-field" name="small_description_ar" class="form-control" value="{{ is_null(old("small_description_ar")) ? $service->small_description_ar : old("small_description_ar") }}"/>
               @if($errors->has("small_description_ar"))
                <span class="help-block">{{ $errors->first("small_description_ar") }}</span>
               @endif
            </div>
            <div class="form-group col-md-6 col-sm-6 col-xs-12 @if($errors->has('small_description_en')) has-error @endif">
               <label for="small_description_en-field">@lang('backend.small_description_en')</label>
            <input type="text" id="small_description_en-field" name="small_description_en" class="form-control" value="{{ is_null(old("small_description_en")) ? $service->small_description_en : old("small_description_en") }}"/>
               @if($errors->has("small_description_en"))
                <span class="help-block">{{ $errors->first("small_description_en") }}</span>
               @endif
            </div>

            <div class="form-group col-md-6 col-sm-6 col-xs-12 @if($errors->has('description_ar')) has-error @endif">
                 <label for="description_ar-field">@lang('backend.description_ar')</label>
              <textarea id="description_ar-field" name="description_ar" class="form-control" rows="10">
                {{is_null(old("description_ar")) ? $service->description_ar : old("description_ar") }}
              </textarea>
                 @if($errors->has("description_ar"))
                  <span class="help-block">{{ $errors->first("description_ar") }}</span>
                 @endif
              </div>
              <div class="form-group col-md-6 col-sm-6 col-xs-12 @if($errors->has('description_en')) has-error @endif">
                 <label for="description_en-field">@lang('backend.description_en')</label>
               <textarea id="description_en-field" name="description_en" class="form-control" rows="10">
                {{ is_null(old("description_en")) ? $service->description_en : old("description_en") }}
              </textarea>
                 @if($errors->has("description_en"))
                  <span class="help-block">{{ $errors->first("description_en") }}</span>
                 @endif
              </div>

            <div class="form-group col-md-6 col-sm-6 col-xs-12 @if($errors->has('is_publish')) has-error @endif">
               <label for="is_publish-field">@lang('backend.Publish')</label>
            <select name="is_publish" id="is_publish-field" class="form-control" ><option value="1">YES</option><option value="0">NO</option></select>
               @if($errors->has("is_publish"))
                <span class="help-block">{{ $errors->first("is_publish") }}</span>
               @endif
            </div>

            <div class="form-group col-md-6 col-sm-6 col-xs-12 @if($errors->has('image')) has-error @endif">
               <label for="image-field">@lang('backend.Image')</label>
            <input type="file" id="image-field" name="image" class="form-control" value="{{ is_null(old("image")) ? $service->image : old("image") }}"/>
               @if($errors->has("image"))
                <span class="help-block">{{ $errors->first("image") }}</span>
               @endif
            </div>
            <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
                <button type="submit" class="btn btn-primary">{{__('backend.EDIT')}}</button>
                @include('backend.layouts.back')
                
            </div>
        </form>

    </div>
</div>
@endsection
@section('scripts')

@endsection

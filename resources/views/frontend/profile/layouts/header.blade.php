<!-- Start Header -->
<header>

   

<!-- 
    <a href="{{ app()->getLocale() == 'en' ? url('/en/') : url('/') }}" class="logo navbar-brand">
        <img src="{{asset('/profile/images/logo.png')}}" lat=""/>
    </a> -->
    <ul id="header-actions" class="clearfix">
        <li class="list-box hidden-xs dropdown">
            <a id="drop2" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">
                <i class=" icon-bell"></i>
            </a>
            <span class="info-label blue-bg">{{ ! empty($count_notification) ? $count_notification : '0'}}</span>
            <ul class="dropdown-menu imp-notify">
                <li class="dropdown-header notification-header"> @lang('website.youhave') {{ ! empty($count_notification) ? $count_notification : '0'}} @lang('website.notifications')</li>
                @if(! empty($app_notification))
                    @foreach($app_notification as $notification)
                        <li>
                            <a href="{{ app()->getLocale() == 'en'  ? route('profile.orders', ['locale' =>'en','id' =>$notification->object_id] )  : url('/customer/account/orders?id='.$notification->object_id ) }}">
                                <div class="icon">
                                     <img src="{{asset('assets/images/notificationIcon.png')}}" class="personIconSidemenu">
                                    <!-- <i class=" icon-info"></i> -->
                                </div>
                                <div class="details">
                                    <strong class="text-warning">{{$notification->title}}</strong>
                                    <span>{{$notification->message}}</span>
                                </div>
                            </a>
                        </li>

                    @endforeach
                    <li class="dropdown-footer"><a
                            href="{{ app()->getLocale() == 'en' ? url('/en/customer/account/notifications') : url('/customer/account/notifications') }}">@lang('website.seeallnotification')</a>
                    </li>
                @endif
            </ul>
        </li>

        <li class="list-box user-admin hidden-xs dropdown">
            <div class="admin-details">
                <div
                    class="name">{{ Auth::guard('Customer')->user() ?  Auth::guard('Customer')->user()->name : '' }}</div>
                <div
                    class="designation">{{ Auth::guard('Customer')->user() && Auth::guard('Customer')->user()->Corporate ?  Auth::guard('Customer')->user()->Corporate->name : '' }}</div>
            </div>
            <a id="drop4" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">
                <img src="{{asset('assets/images/user.ico')}}" class="personIcon">
                <!-- <i class="icon-user"></i> -->
            </a>
            <ul class="dropdown-menu sm">
                <li class="dropdown-content">
                    <a href="{{ app()->getLocale() == 'en' ? url('/en/customer/account/changepassword') : url('/customer/account/changepassword') }}">
                        <img src="{{asset('assets/images/password_reset2.png')}}" class="user-options-img-1">@lang('website.changepassword')</a>
                    <a href="{{ url(change_locale_url() )}}">
                        <img src="{{asset('assets/images/notificationIcon.png')}}" class="user-options-img-2">{{ app()->getLocale() == 'en' ? __('website.Arabic') : __('website.English') }}</a>
                    <a href="{{ app()->getLocale() == 'en' ? url('/en/customer/account/logout') : url('/customer/account/logout') }}">
                        <img src="{{asset('assets/images/logout.png')}}" class="user-options-img-3">@lang('website.Logout')</a>
                </li>
            </ul>
        </li>
        <li>
            <button type="button" id="toggleMenu" class="toggle-menu">
                <i class="collapse-menu-icon"></i>
            </button>
        </li>
    </ul>
</header>
<!-- End Header -->

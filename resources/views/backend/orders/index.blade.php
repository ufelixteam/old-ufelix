@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.the_orders')}}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <style type="text/css">
        @media only screen and (max-width: 580px) {
            .hide-td {
                display: none;
            }
        }

        .box-body {
            min-height: 100px;
        }

        .stylish-input-group .input-group-addon {
            background: white !important;
        }

        .stylish-input-group .form-control {
            border-right: 0;
            box-shadow: 0 0 0;
            border-color: #ccc;
        }

        .stylish-input-group button {
            border: 0;
            background: transparent;
        }

        .toggle.ios, .toggle-on.ios, .toggle-off.ios {
            border-radius: 20px;
        }

        .toggle.ios .toggle-handle {
            border-radius: 20px;
        }

        .filter {
            margin-bottom: 10px;
        }
    </style>
@endsection

@section('header')
    <div class="page-header clearfix">
        <h3 style="display: inline-block">
            <i class="glyphicon glyphicon-align-justify"></i>
            @if(app('request')->input('type') != "")
                @if(app('request')->input('type') == "cancel")
                    {{__('backend.unverified')}}
                @else
                    {{__('backend.'.ucfirst(app('request')->input('type')))}}
                @endif

            @endif
            Orders
        </h3>

        @if($type == 'all')
            <a href="{{url('mngrAdmin/print/collection_orders/' . $collection . '/' . $type)}}"
               class="btn btn-info pull-right" target="_blank"> {{__('backend.pdf')}} </a>
        @endif

    </div>
@endsection
@section('content')

    @include('error')

    <div class="alert alert-danger" hidden>
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong id="errorId"></strong>
    </div>

    <form action="{{URL::asset('/mngrAdmin/orders')}}" method="get">
        <div class="row" style="margin-bottom:15px;" id="orders">
            <div class="col-md-12">
                <div class="col-md-3 col-sm-3 filter">
                    <div id="imaginary_container">
                        <div class="input-group stylish-input-group">
                            <input type="text" class="form-control" id="search-field" name="search"
                                   value="{{app('request')->search}}"
                                   placeholder="{{__('backend.search')}} ">
                            <span class="input-group-addon">
                              <button type="button" id="search-btn1">
                                <span class="glyphicon glyphicon-search"></span>
                              </button>
                            </span>
                        </div>
                    </div>
                </div>
                {{--                @if($type == 'recall')--}}
                <div class="col-md-3 col-sm-3 filter">
                    <select id="corporateID" name="corporateID" class="form-control">
                        <option value="" data-display="Select">{{__('backend.corporate')}}</option>
                        @if(! empty($corporates))
                            @foreach($corporates as $corporate)
                                <option value="{{$corporate->id}}"
                                        @if(app('request')->input('corporate_id') == $corporate->id) selected @endif>
                                    {{$corporate->name}}
                                </option>
                            @endforeach
                        @endif

                    </select>
                </div>
                <div class="col-md-3 col-sm-3 filter">
                    <div class='input-group date ' id="datepicker1">
                        <input type="text" id="fromDate" name="fromDate" class="form-control"
                               value="{{app('request')->fromDate}}"
                               placeholder="{{__('backend.From_Date')}}" autocomplete="off"/>
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 filter">
                    <div class='input-group date ' id="datepickers">
                        <input type="text" id="toDate" name="toDate" class="form-control"
                               value="{{app('request')->toDate}}"
                               placeholder="{{__('backend.To_Date')}}" autocomplete="off"/>
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                    </div>
                </div>

                {{--                @endif--}}
                {{--                @if($type != 'recall')--}}
                <div class="col-md-3 col-sm-3 filter" style="clear: both">
                    <div class='input-group date' id='datepickerFilter'>
                        <input type="text" id="date" name="date" class="form-control" value="{{app('request')->date}}"/>
                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 filter">
                    <select id="s_government_id" name="s_government_id" class="form-control">
                        <option value="" data-display="Select">{{__('backend.sender_Government')}}</option>
                        @if(! empty($app_governments))
                            @foreach($app_governments as $government)
                                <option value="{{$government->id}}"
                                        @if(app('request')->input('s_government_id') == $government->id) selected @endif>
                                    {{$government->name_en}}
                                </option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-md-3 col-sm-3 filter">
                    <select id="s_state_id" name="s_state_id" class="form-control">
                        <option value="">{{__('backend.sender_City')}}</option>

                    </select>
                </div>
                {{--                @endif--}}

                {{--            </div>--}}
                {{--        </div>--}}
                {{--        <div class="row" style="margin-bottom:15px;">--}}
                {{--            <div class="col-md-12">--}}
                {{--                @if($type != 'recall')--}}
                {{--                @endif--}}
                <div class="col-md-3 col-sm-3 filter">
                    <select id="r_government_id" name="r_government_id" class="form-control">
                        <option value="" data-display="Select">{{__('backend.Receiver_Government')}}</option>
                        @if(! empty($app_governments))
                            @foreach($app_governments as $government)
                                <option value="{{$government->id}}"
                                        @if(app('request')->input('r_government_id') == $government->id) selected @endif>
                                    {{$government->name_en}}
                                </option>

                            @endforeach
                        @endif

                    </select>
                </div>
                <div class="col-md-3 col-sm-3 filter">
                    <select id="r_state_id" name="r_state_id" class="form-control">
                        <option value="">{{__('backend.Receiver_City')}}</option>

                    </select>
                </div>
                <div class="col-md-3 col-sm-3 filter">
                    <select id="dropped_in" name="dropped_in" class="form-control">
                        <option value="">{{__('backend.dropped_in')}}</option>
                        <option value="warehouse_dropoff"
                                @if(app('request')->input('dropped_in') == 'warehouse_dropoff') selected @endif>{{__('backend.warehouse')}}</option>
                        <option value="client_dropoff"
                                @if(app('request')->input('dropped_in') == 'client_dropoff') selected @endif>{{__('backend.client')}}</option>
                    </select>
                </div>
                <div class="col-md-3 col-sm-3 filter">
                    <input type="hidden" name="export" value="true"/>
                    <input type="hidden" name="collection"
                           value="{{ ! empty(app('request')->input('collection')) ? app('request')->input('collection') : ''}}"/>
                    <input type="hidden" name="type" id="order_list_type"
                           value="{{ ! empty(app('request')->input('type')) ? app('request')->input('type') : ''}}"/>
                    @if($type != 'recall')
                        <button type="submit" class="btn btn-primary btn-block"> {{__('backend.exportExcel')}} </button>
                    @endif
                </div>


            </div>
        </div>
    </form>
    <div class='list'>
        @include('backend.orders.table')
    </div>
    @include('backend.pickups.create_pickup_popup')
    @include('backend.orders.delay_order_modal')
    @include('backend.orders.move_order_modal')
@endsection
@section('scripts')
    @include('backend.orders.js-search')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script>
        $(function () {
            /*print*/
            $('.toggle').bootstrapToggle();

            $('body').on('change', '.selectOrder', function (e) {
                if ($('input[name="select[]"]').is(':checked')) {
                    $("#delay_orders").removeClass('disabled');
                    $("#deliver_orders").removeClass('disabled');
                    $("#receive_orders").removeClass('disabled');
                } else {
                    $("#delay_orders").addClass('disabled');
                    $("#deliver_orders").addClass('disabled');
                    $("#receive_orders").addClass('disabled');
                }
            });

            $('input[type=radio][name=delay_comment]').change(function () {
                if (this.value == 'Other reason') {
                    $("#delay_reason").show();
                } else {
                    $("#del_reason").val("");
                    $("#delay_reason").hide();
                }
            });

            $('body').on('click', '#saveDelay', function (e) {
                if ($("#delay_date").val()) {
                    let ids = [];
                    $(".selectOrder:checked").each(function () {
                        ids.push($(this).val());
                    });
                    $.ajax({
                        url: "{{ url('/mngrAdmin/delay_orders')}}",
                        type: 'post',
                        data: {
                            ids: ids,
                            delay_date: $("#delay_date").val(),
                            delay_comment: $("input[name='delay_comment']:checked").val(),
                            delay_reason: $("#del_reason").val(),
                            change_status: $("input[name='change_status']:checked").val(),
                            flash: 1,
                            '_token': "{{csrf_token()}}"
                        },
                        success: function (data) {
                            // if (data['status'] != 'false') {
                            location.reload();
                            // }
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }
            });

            $('body').on('click', '#deliver_orders', function (e) {

                let ids = [];

                $(".selectpdf input:checked").each(function () {
                    ids.push($(this).val());

                });

                $.confirm({
                    title: '',
                    theme: 'modern',
                    class: 'danger',
                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-meh-o text-success"></i></span><span class="jconfirm-title">{{__("backend.Confirm")}}</span></div><div style="font-weight:bold;"><p>{{__("backend.delivery_orders_confirm")}}</p></div>',
                    buttons: {
                        confirm: {
                            text: '{{__("backend.Ok")}}',
                            btnClass: 'btn-danger',
                            action: function () {
                                let ids = [];
                                $(".selectOrder:checked").each(function () {
                                    ids.push($(this).val());
                                });
                                $.ajax({
                                    url: "{{ url('/mngrAdmin/deliver_orders')}}",
                                    type: 'post',
                                    data: {ids: ids, '_token': "{{csrf_token()}}"},
                                    success: function (data) {
                                        // if (data['status'] != 'false') {
                                        location.reload();
                                        // }
                                    },
                                    error: function (data) {
                                        console.log('Error:', data);
                                    }
                                });
                            }
                        },
                        cancel: {
                            text: '{{__("backend.Cancel")}}',
                            action: function () {
                            }
                        }
                    }
                });

            });

            $('body').on('click', '#receive_orders', function (e) {

                let ids = [];

                $(".selectpdf input:checked").each(function () {
                    ids.push($(this).val());

                });

                $.confirm({
                    title: '',
                    theme: 'modern',
                    class: 'danger',
                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-meh-o text-success"></i></span><span class="jconfirm-title">{{__("backend.Confirm")}}</span></div><div style="font-weight:bold;"><p>{{__("backend.receive_orders_confirm")}}</p></div>',
                    buttons: {
                        confirm: {
                            text: '{{__("backend.Ok")}}',
                            btnClass: 'btn-danger',
                            action: function () {
                                let ids = [];
                                $(".selectOrder:checked").each(function () {
                                    ids.push($(this).val());
                                });
                                $.ajax({
                                    url: "{{ url('/mngrAdmin/receive_orders')}}",
                                    type: 'post',
                                    data: {ids: ids, '_token': "{{csrf_token()}}"},
                                    success: function (data) {
                                        // if (data['status'] != 'false') {
                                        location.reload();
                                        // }
                                    },
                                    error: function (data) {
                                        console.log('Error:', data);
                                    }
                                });
                            }
                        },
                        cancel: {
                            text: '{{__("backend.Cancel")}}',
                            action: function () {
                            }
                        }
                    }
                });

            });

            $('#delay_date').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                language: 'en',
                todayHighlight: true,
                startDate: new Date()
            });

            $('body').on('click', '.printdata', function (e) {

                var url = "{{URL::asset('/mngrAdmin/print_pdf/')}}/" + $(this).attr('data-id');
                $('<form action="' + url + '" method="get" target="_blank"></form>').appendTo('#orders').submit();
            });

            $('body').on('click', '.delete-order', function (e) {

                var id = $(this).attr('data-id');
                $.confirm({
                    title: '',
                    theme: 'modern',
                    class: 'danger',
                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Confirm Delete</span></div><div style="font-weight:bold;"><p>Are You sure To Delete This Order ?</p></div>',
                    buttons: {
                        confirm: {
                            text: 'Delete Order',
                            btnClass: 'btn-danger',
                            action: function () {
                                $.ajax({
                                    url: "{{ url('/mngrAdmin/orders/')}}" + "/" + id,
                                    type: 'DELETE',
                                    data: {'_token': "{{csrf_token()}}"},
                                    success: function (data) {
                                        jQuery('.alert-danger').html('');
                                        jQuery('.alert-danger').show();
                                        if (data != 'false') {
                                            jQuery('.alert-danger').append('<p>' + data + '</p>');
                                            setTimeout(function () {
                                                location.reload();
                                            }, 2000);
                                        } else {
                                            jQuery('.alert-danger').append('<p>Something Error</p>');
                                        }
                                        window.scrollTo({top: 0, behavior: 'smooth'});
                                    },
                                    error: function (data) {
                                        console.log('Error:', data);
                                    }
                                });
                            }
                        },
                        cancel: {
                            text: 'Cancel',
                            action: function () {
                            }
                        }
                    }
                });
            });

            $('body').on('click', '#packup', function (e) {

                let ids = [];
                $(".selectpack:checked").each(function () {
                    ids.push($(this).val());
                });

                let collection_id = $('.selectpackup  input:checked').attr('data-value');

                let order_ids = $('.selectpackup  input:checked').val();

                $.confirm({
                    title: '',
                    theme: 'modern',
                    class: 'warning',
                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-simle-o  text-warning"></i></span><span class="jconfirm-title">Confirm Pick Up</span></div><div style="font-weight:bold;"><p>Are You sure To Pick Up This Orders ?</p></div>',
                    buttons: {
                        confirm: {
                            text: 'Pickup Order',
                            btnClass: 'btn-warning',
                            action: function () {
                                $.ajax({
                                    url: "{{ url('/mngrAdmin/pickup_orders/')}}",
                                    type: 'POST',
                                    data: {'_token': "{{csrf_token()}}", 'ids': ids, 'collection_id': collection_id},
                                    success: function (data) {
                                        if (data['status'] != 'false') {
                                            $(".alert").hide();

                                            $('#orderpickup').val(data['orders']);
                                            $('#from-pick').val(data['frompick']);
                                            $('#delivery_price-field').val(data['cost']);
                                            $('#bonous-field').val(data['bonous']);

                                            $('#pickupModal').modal('show');

                                        } else {
                                            $('#errorId').html(data['message']);
                                            $(".alert").show();

                                            $('#pickupModal').modal('hide');

                                        }
                                        window.scrollTo({top: 0, behavior: 'smooth'});
                                    },
                                    error: function (data) {
                                        console.log('Error:', data);
                                    }
                                });
                            }
                        },
                        cancel: {
                            text: 'Cancel',
                            action: function () {
                            }
                        }
                    }
                });
            })

        });

        $('body').on('click', '#pdf-submit', function (e) {

            let ids = [];

            $(".selectpdf input:checked").each(function () {
                ids.push($(this).val());

            });
            if (ids.length > 0) {
                $('<form action="{{ url("/mngrAdmin/print_police/")}}" method="post" ><input value="' + ids + ' " name="ids"  />" {{csrf_field()}}"</form>').appendTo('body').submit();

            } else {

                $.confirm({
                    title: '',
                    theme: 'modern',
                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Print PDF</span></div><div style="font-weight:bold;"><p>{{__("backend.Must_Choose_Orders_To_Print")}}</p></div>',
                    type: 'red',
                });
            }

        });

        $('body').on('change', '.warehouse_dropoff', function (e) {
            if ($(this).is(':checked')) {
                var self = $(this).attr('data-id');
                $(this).bootstrapToggle('disable');
                $("#client_dropoff_" + self).bootstrapToggle('enable');
                $("#selectrecallClient_" + self).prop('disabled', false);
                $("#selectMoved_" + self).prop('disabled', false);
                $.ajax({
                    url: "{{ url('/mngrAdmin/orders/warehouse_dropoff/')}}",
                    type: 'POST',
                    data: {'_token': "{{csrf_token()}}", ids: [self]},
                    success: function (data) {

                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        });

        $('body').on('change', '.head_office', function (e) {
            var self = $(this).attr('data-id');
            var is_checked = $(this).is(':checked');
            $.ajax({
                url: "{{ url('/mngrAdmin/orders/head_office/')}}",
                type: 'POST',
                data: {'_token': "{{csrf_token()}}", ids: [self], head_office: is_checked},
                success: function (data) {

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

        });

        $('body').on('change', '.client_dropoff', function (e) {
            if ($(this).is(':checked')) {
                var self = $(this).attr('data-id');
                $(this).bootstrapToggle('disable');
                $("#selectrecallClient_" + self).prop('disabled', true);
                $("#selectMoved_" + self).prop('disabled', true);
                $.ajax({
                    url: "{{ url('/mngrAdmin/orders/client_dropoff/')}}",
                    type: 'POST',
                    data: {'_token': "{{csrf_token()}}", ids: [self]},
                    success: function (data) {

                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });

            }
        });

        $('body').on('click', '#dropAll', function (e) {
            $('input:checkbox.selectdrop').not("[disabled]").not(this).prop('checked', this.checked);
        });

        $('body').on('click', '#recallClientAll', function (e) {
            $('input:checkbox.selectrecallClient').not("[disabled]").not(this).prop('checked', this.checked).change();
        });

        $('body').on('change', '.selectrecallClient', function (e) {
            let customers = [];
            $(".selectrecallClient:checked").each(function () {
                customers.push($(this).attr('data-customer'));
            });

            const allEqual = arr => arr.every(v => v === arr[0]);

            if ($('input[name="selectrecallClient[]"]').is(':checked') && allEqual(customers)) {
                $("#send_client").removeClass('disabled');
            } else {
                $("#send_client").addClass('disabled');
            }
        });

        $('body').on('click', '#send_client', function (e) {

            let ids = [];
            let customer_id = '';
            $(".selectrecallClient:checked").each(function () {
                ids.push($(this).val());
                customer_id = $(this).attr('data-customer');
            });

            if (ids.length == 0) {
                return false;
            }

            $.confirm({
                title: '',
                theme: 'modern',
                class: 'warning',
                content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-simle-o  text-warning"></i></span><span class="jconfirm-title"> {{__("backend.confirm_send_client")}}</span></div><div style="font-weight:bold;"><p>{{__("backend.send_client_orders")}}</p></div>',
                buttons: {
                    confirm: {
                        text: '{{__("backend.send_client_text")}}',
                        btnClass: 'btn-warning',
                        action: function () {
                            $.ajax({
                                url: "{{ url('/mngrAdmin/orders/send_to_client/')}}",
                                type: 'POST',
                                data: {'_token': "{{csrf_token()}}", 'ids': ids, 'customer_id': customer_id},
                                success: function (data) {
                                    $('<form action="{{ url("/mngrAdmin/print_refund/")}}" method="post" target="_blank"><input value="' + data + ' " name="refund_collection_id"  />" {{csrf_field()}}"</form>').appendTo('body').submit();
                                    location.reload();
                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                            });
                        }
                    },
                    cancel: {
                        text: 'Cancel',
                        action: function () {
                        }
                    }
                }
            });
        });

        $('body').on('click', '#drop_off', function (e) {

            let ids = [];
            $(".selectdrop:checked").each(function () {
                ids.push($(this).val());
            });

            if (ids.length == 0) {
                return false;
            }

            $.confirm({
                title: '',
                theme: 'modern',
                class: 'warning',
                content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-simle-o  text-warning"></i></span><span class="jconfirm-title"> {{__("backend.confirm_drop_off")}}</span></div><div style="font-weight:bold;"><p>{{__("backend.drop_off_orders")}}</p></div>',
                buttons: {
                    confirm: {
                        text: '{{__("backend.drop_off_text")}}',
                        btnClass: 'btn-warning',
                        action: function () {
                            $.ajax({
                                url: "{{ url('/mngrAdmin/orders/drop_orders/all/')}}",
                                type: 'POST',
                                data: {'_token': "{{csrf_token()}}", 'ids': ids},
                                success: function (data) {
                                    location.reload();
                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                            });
                        }
                    },
                    cancel: {
                        text: 'Cancel',
                        action: function () {
                        }
                    }
                }
            });
        });

        $('body').on('click', '#dropWarehouseAll', function (e) {
            $('input:checkbox.selectdropWarehouse').not("[disabled]").not(this).prop('checked', this.checked);
        });

        $('body').on('click', '#drop_warehouse_off', function (e) {

            let ids = [];
            $(".selectdropWarehouse:checked").each(function () {
                ids.push($(this).val());
            });

            if (ids.length == 0) {
                return false;
            }

            $.confirm({
                title: '',
                theme: 'modern',
                class: 'warning',
                content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-simle-o  text-warning"></i></span><span class="jconfirm-title"> {{__("backend.confirm_drop_off")}}</span></div><div style="font-weight:bold;"><p>{{__("backend.drop_off_orders")}}</p></div>',
                buttons: {
                    confirm: {
                        text: '{{__("backend.drop_off_text")}}',
                        btnClass: 'btn-warning',
                        action: function () {
                            $.ajax({
                                url: "{{ url('/mngrAdmin/move_collections/drop_orders/')}}",
                                type: 'POST',
                                data: {
                                    '_token': "{{csrf_token()}}",
                                    'ids': ids,
                                    'collection_id': "{{app('request')->move_collection}}"
                                },
                                success: function (data) {
                                    location.reload();
                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                            });
                        }
                    },
                    cancel: {
                        text: 'Cancel',
                        action: function () {
                        }
                    }
                }
            });
        });

        $('body').on('click', '#moveAll', function (e) {
            $('input:checkbox.selectMoved').not("[disabled]").not(this).prop('checked', this.checked).trigger('change');
        });

        $('body').on('click', '#move_to', function (e) {
            let ids = [];
            $(".selectMoved:checked").each(function () {
                ids.push($(this).val());
            });

            $("#move_ids").val(ids);
        });

        $('body').on('change', '.selectMoved', function (e) {
            let warehouses = [];
            $(".selectMoved:checked").each(function () {
                warehouses.push($(this).attr('data-warehouse'));
            });

            const allEqual = arr => arr.every(v => v === arr[0]);

            if ($('input[name="selectMoved[]"]').is(':checked') && allEqual(warehouses)) {
                $("#move_to").removeClass('disabled');
            } else {
                $("#move_to").addClass('disabled');
            }
        });


    </script>
@endsection

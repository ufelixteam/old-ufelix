<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class OrderCancelRequest extends Model
{
    public function order() {
        return $this->belongsTo('App\Models\Order', 'order_id');
    }
    public function customer() {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }
    /*public function corporate() {
        return $this->hasOneThrough('App\Models\Corporate', 'App\Models\Customer', 'corporate_id', 'id', 'customer_id');
    }*/

}

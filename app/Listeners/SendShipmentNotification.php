<?php

namespace App\Listeners;

use App\Events\OrderUpdated;
use App\Models\Customer;
use App\Models\Driver;
use App\Models\Invoice;
use App\Models\InvoiceOrder;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Store;
use App\Models\User;
use FCM;
use GuzzleHttp\Client;
use Illuminate\Contracts\Queue\ShouldQueue;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

class SendShipmentNotification implements ShouldQueue
{
    /**
     * The name of the queue the job should be sent to.
     *
     * @var string|null
     */
    public $queue = 'listeners';

    public $notification_message = [];
    public $eventData = [];

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->comment_by = 'Ufelix';
        $this->notification_message = [
            'title_ar' => '',
            'title_en' => '',
            'message_ar' => "",
            'message_en' => "",
            'object_id' => '',
            'published_by' => '',
            'type' => 2,
            'client_id' => '',
            'client_model' => 4,
            'device_type' => 4,
        ];
    }

    /**
     * Handle the event.
     *
     * @param OrderUpdated $event
     * @return void
     */
    public function handle(OrderUpdated $event)
    {
        $this->eventData = $event->data;

        $user = null;
        if($event->user_type == 3) {
            $user = Driver::find($event->user_id);
        }elseif($event->user_type == 2){
            $user = Customer::find($event->user_id);
            $this->comment_by = "Client";
        }else{
            $this->notification_message['published_by'] = $event->user_id;
            $user = User::find($event->user_id);
        }

        $orders = Order::with('invoiceCaptain', 'drivers')->whereIn('id', $event->ids)
            ->get()
            ->groupBy('customer_id');

        $orders->each(function ($items, $key) use ($event, $user) {
            $customer = Customer::with(['Corporate', 'devices' => function ($query) {
                $query->whereNotNull('fcm_token');
            }])
                ->where('id', $key)
                ->first();

            $parent_notification = false;

            $count_orders = count($items);
            if ($count_orders > 1) {
                $this->notification_message['object_id'] = '';
                $this->notification_message['type'] = 1;
                $this->notification_message['client_id'] = $key;
                $this->notification_message['client_model'] = 4;
                $this->notification_message['parent_id'] = null;
                $parent_data = [
                    'orders_count' => $count_orders,
//                    'type' => 1,
//                    'client_id' => $key,
//                    'client_model' => 4,
                    'parent_id' => null
                ];
//                if ($event->action == 'receive_orders') {
                $parent_notification = $this->setNotificationTitle($event->action, $parent_data);
//                }
                $this->send_notification($customer ? $customer : false);
            }
            $items->each(function ($item, $i) use ($parent_notification, $customer, $event, $user) {
                $this->notification_message['object_id'] = $item->id;
                $this->notification_message['type'] = 2;
                $this->notification_message['client_id'] = $item->customer_id;
                $this->notification_message['client_model'] = 4;
                $this->notification_message['parent_id'] = ($parent_notification ? $parent_notification->id : null);
                $data = [
                    'order_id' => $item->order_number,
//                    'type' => 2,
//                    'client_id' => $item->customer_id,
//                    'client_model' => 4,
                    'parent_id' => ($parent_notification ? $parent_notification->id : null)
                ];
//                if ($event->action == 'receive_orders') {
                $this->setNotificationTitle($event->action, $data);
//                }

                if (!$parent_notification) {
                    $this->send_notification($customer ? $customer : false);
                }

                activity($this->notification_message['title_en'])
                    ->performedOn($item)
                    ->causedBy($user)
                    ->withProperties($event->data)
                    ->log($this->notification_message['message_en']);

                if ($event->action == 'receive_orders') {
//                    $message = 'شحنتك من ' . $customer->Corporate->name . ' في الطريق إليك';
                    $message = "شحنتك من " . $customer->Corporate->name . " برقم " . $item->order_number . " مع المندوب للتبع ufelix.com";
//                    (new SMSController())->SendSMSCode($item->receiver_mobile, $message);
                }

                $this->saveInvoice($item, $customer);
                $this->updateExternalStatus($item, $event);

            });
        });
        // Access the order using $event->order...
    }

    public function setNotificationTitle($type = '', $data = [])
    {
        if ($type == 'pending') {
            $this->notification_message['title_ar'] = 'تعليق شحنات';
            $this->notification_message['title_en'] = 'Pending Orders';
            if (isset($data['orders_count'])) {
                $this->notification_message['message_ar'] = "تم تعليق {$data['orders_count']} شحنة";
                $this->notification_message['message_en'] = "{$data['orders_count']} Orders were pending";
            } elseif (isset($data['order_id'])) {
                $this->notification_message['message_ar'] = "تم تعليق الشحنة {$data['order_id']}#";
                $this->notification_message['message_en'] = "The order #{$data['order_id']} was pending";
            }
        } elseif ($type == 'receive_orders') {
            $this->notification_message['title_ar'] = 'شحنات خرجت للتسليم';
            $this->notification_message['title_en'] = 'O.F.D Orders';
            if (isset($data['orders_count'])) {
                $this->notification_message['message_ar'] = "تم خروج {$data['orders_count']} شحنة مع المندوب للتسليم";
                $this->notification_message['message_en'] = "{$data['orders_count']} Orders were O.F.D";
            } elseif (isset($data['order_id'])) {
                $this->notification_message['message_ar'] = "تم خروج الشحنة {$data['order_id']}# مع المندوب للتسليم";
                $this->notification_message['message_en'] = "The order #{$data['order_id']} was O.F.D";
            }
        } elseif ($type == 'deliver') {
            $this->notification_message['title_ar'] = 'تسليم شحنات';
            $this->notification_message['title_en'] = 'Deliver Orders';
            if (isset($data['orders_count'])) {
                $this->notification_message['message_ar'] = "تم تسليم {$data['orders_count']} شحنة";
                $this->notification_message['message_en'] = "{$data['orders_count']} Orders were delivered";
            } elseif (isset($data['order_id'])) {
                $this->notification_message['message_ar'] = "تم تسليم الشحنة {$data['order_id']}#";
                $this->notification_message['message_en'] = "The order #{$data['order_id']} was delivered";
            }
        } elseif ($type == 'part_delivered') {
            $this->notification_message['title_ar'] = 'تسليم شحنات جزئي';
            $this->notification_message['title_en'] = 'Part Deliver Orders';
            if (isset($data['orders_count'])) {
                $this->notification_message['message_ar'] = "تم تسليم جزئي ل {$data['orders_count']} شحنة";
                $this->notification_message['message_en'] = "{$data['orders_count']} Orders were part delivered";
            } elseif (isset($data['order_id'])) {
                $this->notification_message['message_ar'] = "تم تسليم جزئي للشحنة {$data['order_id']}#";
                $this->notification_message['message_en'] = "The order #{$data['order_id']} was part delivered";
            }
        } elseif ($type == 'replace') {
            $this->notification_message['title_ar'] = 'تبديل شحنات';
            $this->notification_message['title_en'] = 'Replace Orders';
            if (isset($data['orders_count'])) {
                $this->notification_message['message_ar'] = "تم تبديل {$data['orders_count']} شحنة";
                $this->notification_message['message_en'] = "{$data['orders_count']} Orders were replaced";
            } elseif (isset($data['order_id'])) {
                $this->notification_message['message_ar'] = "تم تبديل الشحنة {$data['order_id']}#";
                $this->notification_message['message_en'] = "The order #{$data['order_id']} was replaced";
            }
        } elseif ($type == 'dropped') {
            $this->notification_message['title_ar'] = 'إنزال شحنات';
            $this->notification_message['title_en'] = 'Drop Orders';
            if (isset($data['orders_count'])) {
                $this->notification_message['message_ar'] = "تم إنزال {$data['orders_count']} شحنة";
                $this->notification_message['message_en'] = "{$data['orders_count']} Orders were dropped";
            } elseif (isset($data['order_id'])) {
                $this->notification_message['message_ar'] = "تم إنزال الشحنة {$data['order_id']}#";
                $this->notification_message['message_en'] = "The order #{$data['order_id']} was dropped";
            }
        } elseif ($type == 'forward') {
            $driverName = false;
            if (isset($this->eventData['driver_id'])) {
                $driverName = Driver::where('id', $this->eventData['driver_id'])->value('name');
            }
            $this->notification_message['title_ar'] = 'قبول شحنات';
            $this->notification_message['title_en'] = 'Accept Orders';
            if (isset($data['orders_count'])) {
                $this->notification_message['message_ar'] = "تم قبول {$data['orders_count']} شحنة بواسطة الكابتن $driverName";
                $this->notification_message['message_en'] = "{$data['orders_count']} Orders were accepted to captain $driverName";
            } elseif (isset($data['order_id'])) {
                $this->notification_message['message_ar'] = "تم قبول الشحنة {$data['order_id']}# بواسطة الكابتن $driverName";
                $this->notification_message['message_en'] = "The order #{$data['order_id']} was accepted to captain $driverName";
            }
        } elseif ($type == 'recall') {
            $type_en = 'receiver';
            $type_ar = 'المستلم';
            if (isset($this->eventData['recalled_by'])) {
                if ($this->eventData['recalled_by'] == 3) {
                    $type_en = 'sender';
                    $type_ar = 'المرسل';
                }
            }

            $this->notification_message['title_ar'] = 'رجوع شحنات';
            $this->notification_message['title_en'] = 'Recall Orders';
            if (isset($data['orders_count'])) {
                $this->notification_message['message_ar'] = "تم رجوع {$data['orders_count']} شحنة بواسطة $type_ar";
                $this->notification_message['message_en'] = "{$data['orders_count']} Orders were recalled by $type_en";
            } elseif (isset($data['order_id'])) {
                $this->notification_message['message_ar'] = "تم رجوع الشحنة {$data['order_id']}# بواسطة $type_ar";
                $this->notification_message['message_en'] = "The order #{$data['order_id']} was recalled by $type_en";
            }
        } elseif ($type == 'reject') {
            $type_en = 'receiver';
            $type_ar = 'المستلم';

            $this->notification_message['title_ar'] = 'رفض شحنات';
            $this->notification_message['title_en'] = 'Reject Orders';
            if (isset($data['orders_count'])) {
                $this->notification_message['message_ar'] = "تم رفض {$data['orders_count']} شحنة بواسطة $type_ar";
                $this->notification_message['message_en'] = "{$data['orders_count']} Orders were rejected by $type_en";
            } elseif (isset($data['order_id'])) {
                $this->notification_message['message_ar'] = "تم رفض الشحنة {$data['order_id']}# بواسطة $type_ar";
                $this->notification_message['message_en'] = "The order #{$data['order_id']} was rejected by $type_en";
            }
        } elseif ($type == 'recall_if_dropped') {
            $this->notification_message['title_ar'] = 'مرتجع عند الإنزال';
            $this->notification_message['title_en'] = 'Recall If Dropped';
            if (isset($data['orders_count'])) {
                $this->notification_message['message_ar'] = "تم تحديد {$data['orders_count']} شحنة مرتجع عند الإنزال";
                $this->notification_message['message_en'] = "{$data['orders_count']} Orders were selected as recalled if dropped";
            } elseif (isset($data['order_id'])) {
                $this->notification_message['message_ar'] = "تم تحديد الشحنة {$data['order_id']}# مرتجع عند الإنزال";
                $this->notification_message['message_en'] = "The order #{$data['order_id']} was selected as recalled if dropped";
            }
        } elseif ($type == 'cancel') {
            $type_en = 'client';
            $type_ar = 'العميل';
            if (isset($this->eventData['cancelled_by'])) {
                if ($this->eventData['cancelled_by'] == 1) {
                    $type_en = 'admin';
                    $type_ar = 'الأدمن';
                }
            }

            $this->notification_message['title_ar'] = 'إلغاء شحنات';
            $this->notification_message['title_en'] = 'Cancel Orders';
            if (isset($data['orders_count'])) {
                $this->notification_message['message_ar'] = "تم إلغاء {$data['orders_count']} شحنة بواسطة $type_ar";
                $this->notification_message['message_en'] = "{$data['orders_count']} Orders were cancelled by $type_en";
            } elseif (isset($data['order_id'])) {
                $this->notification_message['message_ar'] = "تم إلغاء الشحنة {$data['order_id']}# بواسطة $type_ar";
                $this->notification_message['message_en'] = "The order #{$data['order_id']} was cancelled by $type_en";
            }
        } elseif ($type == 'cancel_by_system') {
            $this->notification_message['title_ar'] = 'إلغاء الشحنات تلقائيا';
            $this->notification_message['title_en'] = 'Cancel Orders Automatically';
            if (isset($data['orders_count'])) {
                $this->notification_message['message_ar'] = "تم إلغاء {$data['orders_count']} شحنات لعدم توفر المحتوى ولم يتم الإنزال فى المخزن";
                $this->notification_message['message_en'] = "{$data['orders_count']} Orders were cancelled because the content was not available and they were not in stock";
            } elseif (isset($data['order_id'])) {
                $this->notification_message['message_ar'] = "تم إلغاء الشحنة {$data['order_id']}# لعدم توفر المحتوى ولم يتم الإنزال فى المخزن";
                $this->notification_message['message_en'] = "The order #{$data['order_id']} was cancelled because the content was not available and they were not in stock";
            }
        } elseif ($type == 'moved_to') {
            $storeName = false;
            if (isset($this->eventData['store_id'])) {
                $storeName = Store::where('id', $this->eventData['store_id'])->value('name');
            }
            $this->notification_message['title_ar'] = 'نقل شحنات';
            $this->notification_message['title_en'] = 'Moving Orders';
            if (isset($data['orders_count'])) {
                $this->notification_message['message_ar'] = "يتم نقل {$data['orders_count']} شحنة إلى مخزن $storeName";
                $this->notification_message['message_en'] = "{$data['orders_count']} Orders are moving to store $storeName";
            } elseif (isset($data['order_id'])) {
                $this->notification_message['message_ar'] = "يتم نقل الشحنة {$data['order_id']}# إلى مخزن $storeName";
                $this->notification_message['message_en'] = "The order #{$data['order_id']} is moving to store $storeName";
            }
        } elseif ($type == 'problems') {
            $this->notification_message['title_ar'] = 'مشكلة تسليم';
            $this->notification_message['title_en'] = 'Delivery Problem';
            if (isset($data['orders_count'])) {
                $this->notification_message['message_ar'] = "تم إضافة مشكلة تسليم إلى {$data['orders_count']} شحنة";
                $this->notification_message['message_en'] = "Add delivery problem to {$data['orders_count']} orders";
            } elseif (isset($data['order_id'])) {
                $this->notification_message['message_ar'] = "تم إضافة مشكلة تسليم إلى الشحنة {$data['order_id']}#";
                $this->notification_message['message_en'] = "Add delivery problem to the order #{$data['order_id']}";
            }
        } elseif ($type == 'comments') {
            $this->notification_message['title_ar'] = 'تعليقات الشحنات';
            $this->notification_message['title_en'] = 'Order Comments';
            if (isset($data['orders_count'])) {
                $this->notification_message['message_ar'] = "تم إضافة تعليق إلى {$data['orders_count']} شحنة";
                $this->notification_message['message_en'] = "Add comment to {$data['orders_count']} orders";
            } elseif (isset($data['order_id'])) {
                $this->notification_message['message_ar'] = "تم إضافة تعليق إلى الشحنة {$data['order_id']}#";
                $this->notification_message['message_en'] = "Add comment to the order #{$data['order_id']}";
            }
        } elseif ($type == 'refund') {
            $this->notification_message['title_ar'] = 'إعادة شحنات';
            $this->notification_message['title_en'] = 'Refund Orders';
            if (isset($data['orders_count'])) {
                $this->notification_message['message_ar'] = "تم إعادة {$data['orders_count']} شحنة إلى العميل";
                $this->notification_message['message_en'] = "{$data['orders_count']} Orders were refunded to client";
            } elseif (isset($data['order_id'])) {
                $this->notification_message['message_ar'] = "تم إعادة الشحنة {$data['order_id']}# إلى العميل";
                $this->notification_message['message_en'] = "The order #{$data['order_id']} was refunded to client";
            }
        } elseif ($type == 'delay') {
            $date = false;
            if (isset($this->eventData['delay_at'])) {
                $date = $this->eventData['delay_at'];
            }
            $this->notification_message['title_ar'] = 'تأجيل شحنات';
            $this->notification_message['title_en'] = 'Delay Orders';
            if (isset($data['orders_count'])) {
                $this->notification_message['message_ar'] = "تم تأجيل {$data['orders_count']} شحنة إلى التاريخ $date";
                $this->notification_message['message_en'] = "{$data['orders_count']} Orders were delayed to date $date";
            } elseif (isset($data['order_id'])) {
                $this->notification_message['message_ar'] = "تم تأجيل الشحنة {$data['order_id']}# إلى تاريخ $date";
                $this->notification_message['message_en'] = "The order #{$data['order_id']} was delayed to date $date";
            }
        } elseif ($type == 'moved_dropped') {
            $storeName = false;
            if (isset($this->eventData['store_id'])) {
                $storeName = Store::where('id', $this->eventData['store_id'])->value('name');
            }
            $this->notification_message['title_ar'] = 'نقل شحنات';
            $this->notification_message['title_en'] = 'Move Orders';
            if (isset($data['orders_count'])) {
                $this->notification_message['message_ar'] = "تم نقل {$data['orders_count']} شحنة إلى مخزن $storeName";
                $this->notification_message['message_en'] = "{$data['orders_count']} Orders were moved to store $storeName";
            } elseif (isset($data['order_id'])) {
                $this->notification_message['message_ar'] = "تم نقل الشحنة {$data['order_id']}# إلى مخزن $storeName";
                $this->notification_message['message_en'] = "The order #{$data['order_id']} was moved to store $storeName";
            }
        } elseif ($type == 'recall_drop') {
            $this->notification_message['title_ar'] = 'إنزال شحنات مرتجعة';
            $this->notification_message['title_en'] = 'Drop Recall Orders';
            if (isset($data['orders_count'])) {
                $this->notification_message['message_ar'] = "يتم إنزال {$data['orders_count']} شحنة فى المخزن";
                $this->notification_message['message_en'] = "{$data['orders_count']} Orders were dropped in warehouse";
            } elseif (isset($data['order_id'])) {
                $this->notification_message['message_ar'] = "يتم إنزال الشحنة {$data['order_id']}# فى المخزن";
                $this->notification_message['message_en'] = "The order #{$data['order_id']} was dropped in warehouse";
            }
        } elseif ($type == 'collect') {
            $this->notification_message['title_ar'] = 'تحصيل شحنات';
            $this->notification_message['title_en'] = 'Collect Orders';
            if (isset($data['orders_count'])) {
                $this->notification_message['message_ar'] = "تم تحصيل {$data['orders_count']}شحنة ";
                $this->notification_message['message_en'] = "{$data['orders_count']} Orders were collected";
            } elseif (isset($data['order_id'])) {
                $this->notification_message['message_ar'] = "تم تحصيل الشحنة {$data['order_id']}#";
                $this->notification_message['message_en'] = "The order #{$data['order_id']} was collected";
            }
        } elseif ($type == 'pay') {
            $this->notification_message['title_ar'] = 'إستلام مبلغ شحنات';
            $this->notification_message['title_en'] = 'Pay Orders';
            if (isset($data['orders_count'])) {
                $this->notification_message['message_ar'] = "تم إستلام مبلغ {$data['orders_count']}شحنة ";
                $this->notification_message['message_en'] = "{$data['orders_count']} Orders were paid";
            } elseif (isset($data['order_id'])) {
                $this->notification_message['message_ar'] = "تم إستلام مبلغ الشحنة {$data['order_id']}#";
                $this->notification_message['message_en'] = "The order #{$data['order_id']} was paid";
            }
        } elseif ($type == 'client_drop') {
            $this->notification_message['title_ar'] = 'إنزال شحنات مرتجعة';
            $this->notification_message['title_en'] = 'Drop Recall Orders';
            if (isset($data['orders_count'])) {
                $this->notification_message['message_ar'] = "يتم إنزال {$data['orders_count']} شحنة إلى العميل";
                $this->notification_message['message_en'] = "{$data['orders_count']} Orders were dropped to client";
            } elseif (isset($data['order_id'])) {
                $this->notification_message['message_ar'] = "يتم إنزال الشحنة {$data['order_id']}# إلى العميل";
                $this->notification_message['message_en'] = "The order #{$data['order_id']} was dropped to client";
            }
        }

        return $this->create_notification();
    }

    public function create_notification()
    {
        return Notification::create($this->notification_message);
    }

    public function saveInvoice($order, $customer)
    {

        try {
            if (in_array($order->status, [0, 1, 7])) {
                $order->drivers->each(function ($item, $key) {
                    InvoiceOrder::where('accepted_order_id', $item->id)->delete();
                });
            }

            if (in_array($order->status, [2, 3, 5, 8])) {
                $invoiceCaptain = Invoice::firstOrCreate(
                    ['object_id' => $order->invoiceCaptain->driver_id, 'type' => 1, 'status' => 0],
                    [
                        'invoice_no' => uniqid(),
                        'object_id' => $order->invoiceCaptain->driver_id,
                        'type' => 1,
                        'status' => 0,
                        'start_at' => date("Y-m-d"),
                        'start_date' => date("Y-m-d H:i:s")
                    ]
                );

                InvoiceOrder::firstOrCreate(
                    [
                        'accepted_order_id' => $order->invoiceCaptain->id,
                        'invoice_id' => $invoiceCaptain->id,
                    ],
                    [
                        'price' => $order->delivery_price,
                    ]
                );
            }
            if (in_array($order->status, [3, 5, 8]) || ($order->status == 4 && $order->status_before_cancel)) {
                $invoice = Invoice::firstOrCreate(
                    ['object_id' => $customer->corporate_id, 'type' => 2, 'status' => 0],
                    [
                        'invoice_no' => uniqid(),
                        'object_id' => $customer->corporate_id,
                        'type' => 2,
                        'status' => 0,
                        'start_at' => date("Y-m-d"),
                        'start_date' => date("Y-m-d H:i:s")
                    ]
                );

                InvoiceOrder::firstOrCreate(
                    [
                        'accepted_order_id' => $order->invoiceCaptain->id,
                        'invoice_id' => $invoice->id,
                    ],
                    [
                        'price' => $order->delivery_price,
                    ]
                );
            }

        } catch (\Exception $e) {

        }
    }

    public function send_notification($customer)
    {
        if (!empty($customer->devices)) {
            $title = $customer->lang == 'ar' ? $this->notification_message['title_ar'] : $this->notification_message['title_en'];
            $message = $customer->lang == 'ar' ? $this->notification_message['message_ar'] : $this->notification_message['message_en'];

            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60 * 20);
            $optionBuilder->setPriority('high');
            $optionBuilder->setContentAvailable(true);

            $notificationBuilder = new PayloadNotificationBuilder($title);
            $notificationBuilder->setBody($message)
                ->setSound('default')
                ->setBadge(1);

            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData([
                'body' => $title,
                'title' => $message,
                'badge' => 1,
                'sound' => 'default',
            ]);

            $option = $optionBuilder->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();

            foreach ($customer->devices as $device) {
                $token = $device->fcm_token;
                if ($token) {
                    $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
                }
            }
        }
    }

    public function updateExternalStatus($order, $event = '')
    {

        if ($order->hook_url) {

            $data = [];

            if ($event->action == 'comments') {
                $data = [
                    'type' => 'comment',
                    'order_number' => $order->order_number,
                    'date' => date('Y-m-d H:i:s'),
                    'comment_by' => $this->comment_by,
                    'comment' => $event->data['comment']
                ];
            } else {
                $data = [
                    'type' => 'update_status',
                    'order_number' => $order->order_number,
                    'date' => date('Y-m-d H:i:s'),
                    'status' => $event->action,
                    'collected_cost' => $order->collected_cost
                ];
            }

            try {
                $client = new Client([
                    'headers' => ['Content-Type' => 'application/json']
                ]);

                $response = $client->post($order->hook_url,
                    ['body' => json_encode($data)]
                );

                $data = json_decode($response->getBody(), true);
                $statusCode = $response->getStatusCode();
            } catch (\Exception $e) {
//                if ($e instanceof \GuzzleHttp\Exception\ClientException) {
//                    $response = $e->getResponse();
//                    $responseBodyAsString = $response->getBody()->getContents();
//                }
//                report($e);
//                return false;
            }

//            } catch (GuzzleHttp\Exception\RequestException $e) {
//                if ($e->hasResponse()) {
//                    // Get response body
//                    // Modify message as proper response
//                    $message = $e->getResponse()->getBody();
//                    return (string) $message;
//                }else {
//                    return $e->getMessage();
//                }
//
//            }


        }
    }
}


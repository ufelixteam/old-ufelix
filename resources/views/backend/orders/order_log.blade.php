@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.order_log')}}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <style type="text/css">
        @media only screen and (max-width: 580px) {
            .hide-td {
                display: none;
            }
        }

        .box-body {
            min-height: 100px;
        }

        .stylish-input-group .input-group-addon {
            background: white !important;
        }

        .stylish-input-group .form-control {
            border-right: 0;
            box-shadow: 0 0 0;
            border-color: #ccc;
        }

        .stylish-input-group button {
            border: 0;
            background: transparent;
        }

        .toggle.ios, .toggle-on.ios, .toggle-off.ios {
            border-radius: 20px;
        }

        .toggle.ios .toggle-handle {
            border-radius: 20px;
        }
    </style>
@endsection

@section('header')
    <div class="page-header clearfix">
        <h3 style="display: inline-block"><i class="fa fa-history"></i> {{__('backend.order_log')}} </h3>
    </div>
@endsection
@section('content')

    @include('error')

    <div class="alert alert-danger" hidden>
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong id="errorId"></strong>
    </div>

    <form action="{{URL::asset('/mngrAdmin/order_log')}}" method="get">
        <div class="row" style="margin-bottom:15px;" id="orders">
            <div class="col-md-12">

                <div class="col-md-3 col-sm-3">
                    <input type="text" class="form-control" id="search-field" name="search"
                           value="{{ app('request')->input('search')}}"
                           placeholder="{{__('backend.search')}} ">
                </div>
                <div class="col-md-3 col-sm-3">
                    <select id="user_id" name="user_id" class="form-control">
                        <option value="" data-display="Select">{{__('backend.user')}}</option>
                        @if(! empty($users))
                            @foreach($users as $user)
                                <option value="{{$user->id}}"
                                        @if(app('request')->input('user_id') == $user->id) selected @endif>
                                    {{$user->name}}
                                </option>

                            @endforeach
                        @endif

                    </select>
                </div>
                <div class="group-control col-md-3 col-sm-3">
                    <div class='input-group date' id='datepickerFilter'>
                        <input type="text" id="date" name="date" class="form-control"
                               value="{{ app('request')->input('date')}}"/>
                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2">
                    <button type="submit"
                            class="btn btn-primary btn-block"> {{__('backend.filter')}} </button>
                </div>
            </div>
        </div>
{{--        <div class="row" style="margin-bottom:15px;">--}}
{{--            <div class="col-md-12">--}}
{{--                <div class="group-control col-md-2 col-sm-2" style="margin-top: 20px">--}}
{{--                    <button type="submit"--}}
{{--                            class="btn btn-primary btn-block"> {{__('backend.filter')}} </button>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
    </form>
    <div class='list'>
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                @if(! empty($activities) && count($activities) > 0)
                    <div class="table-responsive">
                        <table id="theTable" class="table table-condensed table-striped text-center">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>ID</th>
                                <th>{{__('backend.order_number')}}</th>
                                <th>{{__('backend.created_by')}}</th>
                                <th>{{__('backend.subject')}}</th>
                                <th>{{__('backend.description')}}</th>
                                <th>{{__('backend.created_at')}}</th>
                            </tr>
                            </thead>

                            @foreach($activities as $i => $log)
                                <tr>
                                    <td>{{($i+1)}}</td>
                                    <td>{{$log->order_id}}</td>
                                    <td>{{$log->order_number}}</td>
                                    <td>{{@$log->causer->name}}</td>
                                    <td>{{$log->log_name}}</td>
                                    <td>{{$log->description}}</td>
                                    <td>{{date("Y-m-d H:i:s", strtotime($log->created_at))}}</td>
                                </tr>
                            @endforeach

                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    {!! $activities->appends($_GET)->links() !!}
                @else
                    <h3 class="text-center alert alert-warning">{{__('backend.no_log')}}</h3>
                @endif
            </div>
        </div>
        <div id="js-form" class="js-form">
        </div>

    </div>

@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script>
        $(function () {
        });
    </script>
@endsection

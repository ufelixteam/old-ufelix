@if($order)
    <tr id="{{$order->id}}" class="orderRow" data-driver="{{$order->accepted ? $order->accepted->driver->id : null}}"
        @if($order->getIsAlert()) class="firstOne" @endif>

        <td class="serial @if (count($order->comments)) testtooltip @endif" @if (count($order->comments))
        tabindex="0"
            data-html="true"
            data-toggle="popover"
            data-trigger="hover"
            data-placement="top"
            title='<button class="close" data-dismiss="alert"><i class="fa fa-close"></i></button>'
            data-content='{!! $order->problems_table !!}' @endif>
            {{isset($serial) ? $serial : ''}}
        </td>
        <td class="status">{!!  $order->status_span !!}</td>
        <td>{{$order->receiver_name}}</td>
        <td>
            {{$order->sender_name}}
            <br>
            @if($order->customer && $order->customer->Corporate)
                <strong style="color: #0c81bc"> {{$order->customer->Corporate->name}}</strong>
            @endif

        </td>

        <td>
            @if(! empty($order->to_government)  && $order->from_government != null  )
                {{$order->to_government->name_en}}
            @endif
        </td>
        <td style="font-weight: bold; color: #a43;" id="{{$order->order_number}}">{{$order->order_number}}</td>
        <td style="font-weight: bold; color: #0c81bc;">
            <span class="driver" id="driver_{{$order->id}}">
            @if(! empty($order->driver ) && ! empty($order->driver))
                    {{$order->driver->name}}
                @else
                    -
                @endif
            </span>
        </td>
        <td style="font-weight: bold; color: #a43;">
            <span class="total_cost">{{$order->total_price}}</span>
        </td>

        <td class="selectpdf text-center" id="selectpdf">
            @if($order->is_pickup == "0")
                <input type="checkbox" name="select[]" value="{{$order->id}}"
                       class="selectOrder" id="selectOrder_{{$order->id}}"
                       data-warehouse="{{$order->moved_in ? $order->moved_in : ''}}"
                       data-customer="{{$order->customer_id ? $order->customer_id : null}}"
                       data-driver="{{$order->accepted ? $order->accepted->driver->id : null}}"
                       data-latitude="{{$order->accepted ? $order->accepted->driver->latitude : null}}"
                       data-longitude="{{$order->accepted ? $order->accepted->driver->longitude : null}}"
                       data-value="{{$order->id}}"/>
            @endif

        </td>
        <td>
            <div id="removeRow" style="display: inline-block;vertical-align: middle;margin: 0 5px;"
                 data-id="{{$order->id}}">
                <a href="#" style="color: black;"><span class="glyphicon glyphicon-remove"></span></a>
            </div>
        </td>
    </tr>
    <tr @if($order->getIsAlert()) class="secondOne" @endif>
        <td colspan="10" style="padding-right: 35px; padding-top: 15px; padding-bottom: 15px;">
            <strong class="pull-right">{{$order->receiver_address}}</strong>
        </td>
    </tr>
@endif

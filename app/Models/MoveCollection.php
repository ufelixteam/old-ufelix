<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MoveCollection extends Model
{

    protected $guarded = ['id'];

    public function driver()
    {
        return $this->belongsTo('App\Models\Driver');
    }

    public function from_store()
    {
        return $this->belongsTo('App\Models\Store', 'from_store_id', 'id');
    }


    public function store()
    {
        return $this->belongsTo('App\Models\Store');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\MoveCollectionOrder', 'collection_id');

    }

    public function getDroppedStatusAttribute()
    {
        $orders = $this->orders()->where('dropped', 0)->count();

        if ($orders == 0) {
            return 1;
        } else {
            return 0;
        }

    }

    public function getDroppedSpanAttribute()
    {
        $orders = $this->orders()->where('dropped', 0)->count();

        if ($orders == 0) {
            return "<span class='badge badge-pill label-success'>" . __('backend.dropped') . "</span>";
        } else {
            return "<span class='badge badge-pill label-danger'>" . __('backend.not_dropped') . "</span>";
        }

    }
}

<!-- Start Clients -->
<section id="clients" class="clients-x">
	<div class="container">
    	<!-- Start Title -->
    	<div class="col-xs-12">
        	<div class="title">
            	<h3>@lang('website.Customers')</h3>
            </div>
        </div>
        <!-- End Title -->
        <div class="col-xs-12">
    		<div class="clients-slider">
                <!-- Start Item -->
                @if(! empty($corporates) && count($corporates) > 0)
                    @foreach($corporates as $corporate)
                        <div class="item">
                            <div class="img">
                                <img src="{{$corporate->logo != '' ? $corporate->logo : asset('/website/images/c1.jpg') }}" alt="{{$corporate->name}}" />
                            </div>
                        </div>
                    @endforeach
                @endif
                <!-- End -->
            </div>
        </div>
    </div>
</section>
<!-- End Clients -->
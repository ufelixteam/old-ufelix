<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\ShipmentToolOrder;
use App\Models\ShipmentTool;
use Response;
use Flash;
use File;
use Auth;

class ShipmentToolController extends Controller {

	// Get All Shipment Tool
	public function index()
	{
		$shipment_tools = ShipmentTool::orderBy('id', 'desc')->paginate(50);
		return view('backend.shipment_tools.index', compact('shipment_tools'));
	}

	// Create Shipment Tool Page
	public function create() {
		return view('backend.shipment_tools.create');
	}

	// Added New Shipment Tool
	public function store(Request $request) {
			/*
			* status = 0 :: Not Available
			* status = 1 :: Available
			*/

			$v = \Validator::make($request->all(), [
					'name_en'              	=> 'required',
					'name_ar'            		=> 'required',
					'price'                 => 'required',
					'status'                => 'required',
			]);

			if ($v->fails()) {
					return redirect()->back()->withErrors($v->errors());
			}

			$shipment_tool 								= new ShipmentTool();
			$shipment_tool->name_en 			= $request->input("name_en");
			$shipment_tool->name_ar 			= $request->input("name_ar");
	    $shipment_tool->description 	= $request->input("description");
	    $shipment_tool->price 				= $request->input("price");
	    $shipment_tool->status 				= $request->input("status");
			$shipment_tool->save();

			// Function Add Action In Activity Log By Samir
			addActivityLog('Add Shipment Tool ' . $shipment_tool->name_en , ' اضافة اداة شحن  ' . $shipment_tool->name_ar, 9); // Helper Function in App\Helpers.php

	    Flash::success(__('backend.ShipmentTool_saved_successfully'));
			return redirect()->route('mngrAdmin.shipment_tools.index');
	}

	// View One Shipment Tool
	public function show($id) {
		$shipment_tool = ShipmentTool::findOrFail($id);
		return view('backend.shipment_tools.show', compact('shipment_tool'));
	}

	// eDIT shipment Tool Page
	public function edit($id) {
		$shipment_tool = ShipmentTool::findOrFail($id);
		return view('backend.shipment_tools.edit', compact('shipment_tool'));
	}

	// Upate Shipment Tool
	public function update(Request $request, $id) {
			/*
			* status = 0 :: Not Available
			* status = 1 :: Available
			*/

			$v = \Validator::make($request->all(), [
					'name_en'              	=> 'required',
					'name_ar'            		=> 'required',
					'price'                 => 'required',
					'status'                => 'required',
			]);

			if ($v->fails()) {
					return redirect()->back()->withErrors($v->errors());
			}

			$shipment_tool 								= ShipmentTool::findOrFail($id);
			$shipment_tool->name_en 			= $request->input("name_en");
			$shipment_tool->name_en 			= $request->input("name_ar");
	    $shipment_tool->description 	= $request->input("description");
	    $shipment_tool->price 				= $request->input("price");
	    $shipment_tool->status 				= $request->input("status");
			$shipment_tool->save();

			// Function Add Action In Activity Log By Samir
			addActivityLog('Edit Shipment Tool ' . $shipment_tool->name_en , ' تعديل اداة الشحن ' . $shipment_tool->name_ar, 9); // Helper Function in App\Helpers.php

	    Flash::warning(__('backend.ShipmentTool_Updated_successfully'));
			return redirect()->route('mngrAdmin.shipment_tools.index');
	}

	// Delete Shipment Tool
	public function destroy($id)
	{
		$shipment_tool = ShipmentTool::findOrFail($id);
		$shipment_tool->delete();

		// Function Add Action In Activity Log By Samir
		addActivityLog('Delete Shipment Tool ' . $shipment_tool->name_en , ' حذف اداة الشحن ' . $shipment_tool->name_ar, 9); // Helper Function in App\Helpers.php

    flash(__('backend.ShipmentTool_deleted_successfully'))->error();

		return redirect()->route('mngrAdmin.shipment_tools.index');
	}

}

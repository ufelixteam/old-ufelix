<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class IsCaptain
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'captain')
    {
        \Config::set('auth.defines.guard', 'captain');
        config(['auth.defaults.guard' => $guard]);

        if (Auth::guard($guard)->check()) {
            return $next($request);
        } else {
            return redirect('/captainDashboard/login');

        }
    }
}

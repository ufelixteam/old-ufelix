@extends('agent.layouts.app')

@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> ShipmentToolOrders
            <a class="btn btn-success pull-right" href="{{ route('mngrAgent.shipment_tool_orders.create') }}"><i class="glyphicon glyphicon-plus"></i> New Request Tool</a>
        </h3>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            @if($shipment_tool_orders->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Status</th>
                            <th>created_at</th>
                            <th class="text-right"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($shipment_tool_orders as $shipment_tool_order)
                            <tr>
                                <td>{{$shipment_tool_order->shipitemordes_id}}</td>
                                <td>
                                  @if($shipment_tool_order->order_status == 0 )
                                      <span class='badge badge-pill badge-black'>Pending</span>
                                      @else
                                      <span class='badge badge-pill badge-success'>done</span>
                                    @endif
                                </td>
                                <td>{{$shipment_tool_order->created_at}}</td>
                                <td class="text-right">
                                  <a class="btn btn-xs btn-primary" href="{{ route('mngrAgent.shipment_tool_orders.show', $shipment_tool_order->shipitemordes_id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <h3 class="text-center alert alert-warning">No Result Found !</h3>
            @endif

        </div>
    </div>

@endsection

@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.drivers')}}</title>
    <style type="text/css">
        @media only screen and (max-width: 580px) {
            .hide-td {
                display: none;
            }
        }

        .box-body {
            min-height: 100px;
        }

        .stylish-input-group .input-group-addon {
            background: white !important;
        }

        .stylish-input-group .form-control {
            border-right: 0;
            box-shadow: 0 0 0;
            border-color: #ccc;
        }

        .stylish-input-group button {
            border: 0;
            background: transparent;
        }
    </style>
@endsection
@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.drivers_of_agent')}} - {{$agent}}
        </h3>
    </div>
@endsection

@section('content')
    <div class="list">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                @if($drivers->count())
                    <table class="table table-condensed table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('backend.name')}}</th>
                            <th>{{__('backend.mobile')}}</th>
                            <th>{{__('backend.active')}}</th>
                            <th>{{__('backend.block')}}</th>
                            <th>{{__('backend.status')}}</th>
                            <th>{{__('backend.vehicle')}}</th>
                            <th>{{__('backend.agent')}}</th>
                            <th class="text-right"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($drivers as $driver)
                            <tr>
                                <td>{{$driver->id}}</td>
                                <td>{{$driver->name}}</td>
                                <td>{{$driver->mobile}}</td>
                                <td>{!! $driver->active_span !!}</td>
                                <td>{!! $driver->block_span !!}</td>
                                <td>{!! $driver->status_span !!}</td>
                                <td>
                                    @if (isset($driver->truck))
                                        <span class='badge badge-pill label-success'>{{__('backend.have')}}</span>
                                    @else
                                        <span class='badge badge-pill label-danger'>{{__('backend.not_have')}}</span>
                                    @endif
                                </td>
                                <td><span class='badge badge-pill label-default'>
                                  {{! empty($driver->agent) ?  $driver->agent->name : ''}}
                                </span></td>
                                <td class="text-right"></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
                @endif

            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>


        $("#active-field").on('change', function () {
            $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
            $.ajax({
                url: '{{URL::asset("/mngrAdmin/drivers")}}' + '?active=' + $("#active-field").val(),
                type: 'get',
                success: function (data) {
                    $('.list').html('');
                    $('.list').html(data.view);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

        });
        $("#block-field").on('change', function () {
            $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
            $.ajax({
                url: '{{URL::asset("/mngrAdmin/drivers")}}' + '?active=' + '&&block=' + $("#block-field").val(),
                type: 'get',
                success: function (data) {

                    $('.list').html(data.view);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

        });
        $("#status-field").on('change', function () {
            $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
            $.ajax({
                url: '{{URL::asset("/mngrAdmin/drivers")}}' + '?active=' + '&&status=' + $("#status-field").val(),
                type: 'get',
                success: function (data) {

                    $('.list').html(data.view);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

        });

        $("#search-btn1").on('click', function () {
            // Wait icon

            $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
            $.ajax({
                url: '{{URL::asset("/mngrAdmin/drivers")}}' + '?active=' + $("#status-field").val() + '&&block=' + $("#block-field").val() + '&&search=' + $("#search-field").val(),
                type: 'get',
                data: $("#search-form").serialize(),
                success: function (data) {

                    $('.list').html(data.view);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

        });

    </script>

@endsection

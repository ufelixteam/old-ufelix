@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.Payment_Methods')}}</title>
@endsection

@section('header')
  <div class="page-header clearfix">
    <h3>
      <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.Payment_Methods')}}
      @if(permission('addPaymentMethod')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
        <a class="btn btn-success pull-right" href="{{ route('mngrAdmin.payment_methods.create') }}"><i class="glyphicon glyphicon-plus"></i> {{__('backend.create')}}</a>
      @endif
    </h3>
  </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
      @if($payment_methods->count())
        <table class="table table-condensed table-striped">
          <thead>
              <tr>
                <th>#</th>
                <th>{{__('backend.Payment_Method')}}</th>
                <th>{{__('backend.status')}}</th>
                <th class="text-right"></th>
              </tr>
          </thead>
          <tbody>
            @foreach($payment_methods as $payment_method)
              <tr>
                <td>{{$payment_method->id}}</td>
                <td>{{$payment_method->name}}</td>
                <td>{!! $payment_method->status_span !!}</td>
                <td class="text-right">
                  {{--<a class="btn btn-xs btn-primary" href="{{ route('mngrAdmin.payment_methods.show', $payment_method->id) }}"><i class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>--}}
                  @if(permission('editPaymentMethod')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                    <a class="btn btn-xs btn-warning" href="{{ route('mngrAdmin.payment_methods.edit', $payment_method->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                  @endif

                  @if(permission('deletePaymentMethod')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                    <form action="{{ route('mngrAdmin.payment_methods.destroy', $payment_method->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
                      <input type="hidden" name="_method" value="DELETE">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}</button>
                    </form>
                  @endif
                </td>
              </tr>
            @endforeach
        </tbody>
      </table>

      {!! $payment_methods->appends($_GET)->links() !!}

      @else
        <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
      @endif
    </div>
  </div>
@endsection

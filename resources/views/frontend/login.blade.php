@extends('frontend.layouts.app')
@section('title') @lang('website.Login') @endsection
@section('content')
    <!-- Start App1 -->
    <section class="req body-in">
        <div class="container">
            <div class="col-md-5 col-xs-12">
                <div class="req-x">
                    <div class="modal-body mb-1">
                        <div class="title">
                            <h3>@lang('website.Login')</h3>
                        </div>

                        <form action="{{  app()->getLocale() == 'en' ? url('/en/login') : url('login')}}" method="post"
                              name="loginForm" id="loginForm">
                            {{csrf_field()}}
                            <div class="form-group  @if($errors->has('mobile')) has-error @endif">
                                <input type="text" id="mobile" name="mobile" value="{{old('mobile')}}"
                                       class="form-control" placeholder="@lang('website.Mobile')"/>
                                <i class="fas fa-mobile prefix"></i>

                                @if($errors->has("mobile"))
                                    <span class="help-block">{{ $errors->first("mobile") }}</span>
                                @endif
                            </div>

                            <div class="form-group  @if($errors->has('password')) has-error @endif">
                                <input type="password" id="password" name="password" value="" class="form-control"
                                       placeholder="@lang('website.Password')"/>
                                <i class="fas fa-lock prefix"></i>
                                @if($errors->has("password"))
                                    <span class="help-block">{{ $errors->first("password") }}</span>
                                @endif

                            </div>
                            <div class="form-group">
                                <p>
                                    <label class="contw">@lang('website.RememberMe')
                                        <input type="radio" checked="checked" name="check">
                                        <span class="checkmark"></span>
                                    </label>
                                </p>


                                <p>
                                    <a href="{{ app()->getLocale() == 'en' ? url('/en/forgotpassword') : url('/forgotpassword') }}">@lang('website.Forgot')</a>
                                </p>
                            </div>

                            @include('flash::message')

                            <div class="form-group text-center">
                                <button class="btn btn-info" type="submit"
                                        id="loginBtn">    @lang('website.Login') </button>
                            </div>
                        </form>

                        <div class="my-account">

                            @lang('website.AskRegister')
                            <a href="{{ app()->getLocale() == 'en' ? url('/en/register') : url('/register') }}"
                               class="btn login-btn">
                                @lang('website.Register')
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.Content-->


            <div class="col-md-7 col-xs-12">
                <div class="text-req">
                    <h1>@lang('website.LoginHint')</h1>
                    <div class="img" style="background-image: url({{asset('/website/images/req.jpg')}});"></div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('scripts')
    <script src="{{asset('/assets/scripts/jquery.validate.js')}}"></script>

    <script>

        $(function () {
            $("form[name='loginForm']").validate({
                rules: {
                    password: {
                        required: true,
                        minlength: 5
                    },
                    mobile: {
                        required: true,
                        number: true,
                        rangelength: [11, 11],
                    }
                },
                // Specify validation error messages
                messages: {
                    password: {
                        required: "{{__('website.passwordLoginRequired')}}",
                        minlength: "{{__('backend.Password_must_be_more_than5_character')}}"

                    },

                    mobile: {
                        required: "{{__('website.ContactMobileRequired')}}",
                        number: "{{__('website.Please_Enter_Avalid_Number')}}",
                        rangelength: "{{__('website.Mobile_Must_Be11_Digits')}}",

                    }
                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error);
                    $(element).parent().addClass('error');
                    $(element).parent().find('label').addClass(' help-block');

                },
                highlight: function (element) {
                    $(element).parent().addClass('error help-block');
                    $(element).parent().find('label').addClass(' help-block');
                },
                unhighlight: function (element) {
                    $(element).parent().removeClass('error');
                    $(element).parent().find('label').removeClass(' help-block');

                },

                submitHandler: function (form) {
                    form.submit();
                }
            });
        })
    </script>
@endsection

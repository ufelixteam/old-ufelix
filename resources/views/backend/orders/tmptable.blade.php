<table id="theTable" class="table table-condensed table-striped text-center">
    <thead>
    <tr>
        <th>{{__('backend.customer')}}</th>
        <th>{{__('backend.receiver_name')}}</th>
        <th>{{__('backend.Receiver_Mobile')}}</th>
        <th>{{__('backend.Receiver_City')}}</th>

        <th>{{__('backend.sender_name')}}</th>
        <th>{{__('backend.Sender_Mobile')}}</th>
        <th>{{__('backend.sender_City')}}</th>
        <th></th>

    </tr>
    </thead>
    <tbody id="table-order">
    @if(! empty($orders) && count($orders) >0  )
        @foreach($orders as $order)
            <tr>
                <td>{{ ! empty($order->customer )? $order->customer->name : ''  }}</td>
                <td>{{$order->receiver_name}}</td>
                <td>{{$order->receiver_mobile}}</td>

                <td>{{ ! empty($order->receiver_city )? $order->receiver_city->name_en : ''  }}</td>
                <td>{{$order->sender_name}}</td>
                <td>{{$order->sender_mobile}}</td>

                <td>{{ ! empty($order->sender_city )? $order->sender_city->name_en : ''  }}</td>


                <td><a class="btn btn-warning view-order" data-value="{{$order->id}}"><i class="fa fa-eye"
                                                                                         aria-hidden="true"></i></a>
                    <a class="btn btn-danger delete-order" data-value="{{$order->id}}"><i class="fa fa-trash"
                                                                                          aria-hidden="true"></i></a>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
{!! $orders->appends($_GET)->links() !!}

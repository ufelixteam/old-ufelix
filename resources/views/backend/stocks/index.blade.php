@extends('backend.layouts.app')

@section('css')
<title>{{__('backend.stocks')}}</title>
@endsection

@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.stocks')}}
            @if(permission('addStocks')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
              <a class="btn btn-success pull-right" href="{{ route('mngrAdmin.stocks.create') }}"><i class="glyphicon glyphicon-plus"></i> {{__('backend.add_stock')}}</a>
            @endif
        </h3>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            @if ($message = Session::get('message'))
            <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{ $message }}</strong>
            </div>
            @endif

            @if($stocks->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th class="text-center">{{__('backend.id')}}</th>
                            <th class="text-center">{{__('backend.title')}}</th>
                            <th class="text-center">{{__('backend.store')}}</th>
                            <th class="text-center">{{__('backend.corporate')}}</th>
                            <th class="text-center">{{__('backend.quantity')}}</th>
                            <th class="text-center">{{__('backend.description')}}</th>
                            <th class="text-right"></th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($stocks as $stock)
                            <tr>
                                <td class="text-center">{{$stock->id}}</td>
                                <td class="text-center">{{$stock->title}}</td>
                                <td class="text-center">{{ ! empty($stock->store) ? $stock->store->name : ' Not Found ' }}</td>
                                <td class="text-center">{{ ! empty($stock->corporate) ? $stock->corporate->name : ' Not Found ' }}</td>
                                <td class="text-center">{{$stock->qty}}</td>
                                <td class="text-center">{{$stock->description}}</td>
                                <td class="text-right">
                                  @if(permission('showStocks')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                    <a class="btn btn-xs btn-primary" href="{{ route('mngrAdmin.stocks.show', $stock->id) }}"><i class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>
                                  @endif
                                  @if(permission('editStocks')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                    <a class="btn btn-xs btn-warning" href="{{ route('mngrAdmin.stocks.edit', $stock->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                                  @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $stocks->render() !!}
            @else
                <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
            @endif

        </div>
    </div>

@endsection

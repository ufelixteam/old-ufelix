<?php

namespace App\Http\Controllers\Backend;

use App\Events\OrderUpdated;
use App\Http\Controllers\Controller\Backend;
use App\Http\Controllers\VerifyMobileController;
use App\Mail\InvoiceClosed;
use App\Models\AcceptedOrder;
use App\Models\Agent;
use App\Models\Corporate;
use App\Models\Corprate;
use App\Models\Customer;
use App\Models\Driver;
use App\Models\Invoice;
use App\Models\InvoiceOrder;
use App\Models\InvoicePickup;
use App\Models\Order;
use App\Models\PickUp;
use App\Models\ScanCollection;
use App\Models\ScanCollectionOrder;
use App\Models\Wallet;
use App\Models\WalletLog;
use Auth;
use Carbon\Carbon;
use Config;
use DB;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Response;

class InvoiceController extends Controller
{
    private $guardType;

    public function __construct(Request $request)
    {
        if ($request->is('mngrAdmin/*') || $request->is('admin/*')) {
            Config::set('auth.defaults.guard', 'admin');

        } elseif ($request->is('mngrAgent/*') || $request->is('agent/*')) {
            Config::set('auth.defaults.guard', 'agent');
        }
        $this->guardType = Config::get('auth.defaults.guard');
    }

    // agent dashbord
    public function invoicee($id)
    {
        $user = Auth::guard($this->guardType)->user()->agent_id;
        if ($id == '1') {
            $invoices = Invoice::orderBy('id', 'desc')->where(['type' => 3, 'object_id' => $user])->paginate(50);
            return view('agent.agent_invoices.index', compact('invoices'));
        } else {
            $invoices = Driver::select('drivers.id as driver_id', 'drivers.name', 'drivers.mobile', 'invoices.start_date', 'invoices.end_date', 'invoices.status')->
            join('accepted_orders', 'accepted_orders.driver_id', '=', 'drivers.id')->
            join('invoice_orders', 'accepted_orders.id', '=', 'invoice_orders.accepted_order_id')->
            join('invoices', 'invoice_orders.invoice_id', '=', 'invoices.id')->
            where('invoices.type', '=', 3)->
            where('invoices.object_id', '=', $user)->
            DISTINCT()->get();
            return view('agent.agent_invoices.mydriver', compact('invoices'));
        }
        //drivereshow invoce by id
    }

    // agent dashbord
    public function scanInvoice($id)
    {
        $scanCollection = ScanCollection::find($id);

        if (!$scanCollection) {
            return redirect()->back();
        }

        $order = ScanCollectionOrder::where('collection_id', $scanCollection->id)->first();
        if (empty($order->order->accepted_driver)) {
            return redirect()->back();
        }

        $openInvoice = Invoice::where('object_id', $order->order->accepted_driver->driver_id)->where('type', 1)->where('status', 0)->first();
        if (!$openInvoice) {
            return redirect()->back();
        }

        return redirect()->route('mngrAdmin.invoices.show', ['id' => $openInvoice->id, 'scan_collection_id' => $scanCollection->id]);

    }

    public function mydrivereshow($id)
    {
        $user = Auth::guard($this->guardType)->user()->agent_id;
        $invoices = Driver::join('invoices', 'drivers.agent_id', '=', 'invoices.object_id')->
        join('accepted_orders', 'accepted_orders.driver_id', '=', 'drivers.id')->
        join('invoice_orders', 'accepted_orders.id', '=', 'invoice_orders.accepted_order_id')->
        join('orders', 'accepted_orders.order_id', '=', 'orders.id')->
        where([
            ['invoices.type', '=', 3],
            ['drivers.agent_id', '=', $user],
            ['drivers.id', '=', $id],
        ])->
        paginate(15);
        return view('agent.agent_invoices.showmydriver', compact('invoices', 'id'));
    }

    public function shwoInvoiceid($id)
    {
        $invoice = Invoice::findOrFail($id);
        $invOrds = InvoiceOrder::where('invoice_id', $id)->get();
        foreach ($invOrds as $invOrder) {
            $AccOrdersId[] = $invOrder->accepted_order_id;
        }
        $AccOrders = AcceptedOrder::whereIn('id', $AccOrdersId)->get();
        foreach ($AccOrders as $AccOrder) {
            $OrdersId[] = $AccOrder->order_id;
        }
        $Orders = Order::whereIn('id', $OrdersId)->get();
        foreach ($Orders as $Order) {
            if ($Order->payment_method_id < 5) {
                $regOrders[] = $Order->id;
                $arr[] = $Order->id;
            } elseif ($Order->payment_method_id >= 5) {
                $othOrders[] = $Order->id;
                $arr[] = $Order->id;
            }
        }
        if (isset($regOrders)) {
            $regAcceptedOrders = AcceptedOrder::whereIn('order_id', $regOrders)->get();
        }
        if (isset($othOrders)) {
            $othAcceptedOrders = AcceptedOrder::whereIn('order_id', $othOrders)->get();
        }
        $AllAcceptedOrders = AcceptedOrder::whereIn('order_id', $arr)->get();
        $wallet = Wallet::where('object_id', $invoice->object_id)->first();
        $totalItem = 0;
        $totalDelivery = 0;
        $collectedDelivery = 0;
        $othtotalDelivery = 0;
        if (isset($regAcceptedOrders)) {
            foreach ($regAcceptedOrders as $regAcceptedOrder) {
                if ($regAcceptedOrder->status == 4 && $regAcceptedOrder->Order->status == 2) {
                    $othtotalDelivery += $regAcceptedOrder->delivery_price;
                    continue;
                } else {
                    $totalItem += $regAcceptedOrder->Order->order_price;
                }
                $collectedDelivery += $regAcceptedOrder->delivery_price;
            }
        }
        if (isset($othAcceptedOrders)) {
            foreach ($othAcceptedOrders as $othAcceptedOrder) {
                if ($othAcceptedOrder->status == 4 && $othAcceptedOrder->Order->status == 2) {
                    $othtotalDelivery += $othAcceptedOrder->delivery_price;
                    continue;
                } else {
                    $totalItem += $othAcceptedOrder->Order->order_price;
                }
                $othtotalDelivery += $othAcceptedOrder->delivery_price;
            }
        }
        $totalDelivery = $collectedDelivery + $othtotalDelivery;
        if ($invoice->type == 3) {
            $agent = Agent::where('id', $invoice->object_id)->first();
            if ($invoice->status == 1) {
                $commission = ($invoice->profit / 100) * $totalDelivery;
                $othCommission = ($invoice->profit / 100) * $othtotalDelivery;
            } else {
                $commission = ($agent->profit_rate / 100) * $totalDelivery;
                $othCommission = ($agent->profit_rate / 100) * $othtotalDelivery;
            }

        } else {

            $commission = (5 / 100) * $totalDelivery;
            $othCommission = (5 / 100) * $othtotalDelivery;
        }
        if ($invoice->type == 2) {
            $result = $othtotalDelivery;
        } else {
            $result = $totalItem + $commission;
        }
        $message = 0;
        if ($wallet == null) {
            if ($invoice->type == 2) {
                if ($result < $totalItem) {
                    $message = 0;
                } else {
                    $message = 2;
                }
            } else {
                $message = 2;
            }
        } elseif ($result > $wallet->value) {
            if ($invoice->type == 2) {
                if ($result < $totalItem) {
                    $message = 0;
                } else {
                    $message = 1;
                }
            } else {
                $message = 1;
            }
        }
        return view('agent.agent_invoices.show', compact('invoice', 'id', 'AllAcceptedOrders', 'totalItem', 'totalDelivery', 'collectedDelivery', 'othtotalDelivery', 'commission', 'othCommission', 'result', 'message'));
    }
    // end egent dashbord
    // website profile
    public function indexprofilewebsite()
    {
        $idcustome = auth()->guard('Customer')->user()->corporate_id;
        $corprates = Corporate::where('id', $idcustome)->get()->First();
        $invoices = Invoice::orderBy('id', 'desc')->where(['type' => 2, 'object_id' => $corprates->id])->paginate(50);
        return view('profile.invaice', compact('invoices'));
    }

    //website profile end
    public function showInvoice($id)
    {
        $invoice = Invoice::findOrFail($id);
        $invOrds = InvoiceOrder::where('invoice_id', $id)->get();
        foreach ($invOrds as $invOrder) {
            $AccOrdersId[] = $invOrder->accepted_order_id;
        }
        $AccOrders = AcceptedOrder::whereIn('id', $AccOrdersId)->get();
        foreach ($AccOrders as $AccOrder) {
            $OrdersId[] = $AccOrder->order_id;
        }
        $Orders = Order::whereIn('id', $OrdersId)->get();
        foreach ($Orders as $Order) {
            if ($Order->payment_method_id < 5) {
                $regOrders[] = $Order->id;
                $arr[] = $Order->id;
            } elseif ($Order->payment_method_id >= 5) {
                $othOrders[] = $Order->id;
                $arr[] = $Order->id;
            }
        }
        if (isset($regOrders)) {
            $regAcceptedOrders = AcceptedOrder::whereIn('order_id', $regOrders)->get();
        }
        if (isset($othOrders)) {
            $othAcceptedOrders = AcceptedOrder::whereIn('order_id', $othOrders)->get();
        }
        $AllAcceptedOrders = AcceptedOrder::whereIn('order_id', $arr)->get();

        $wallet = Wallet::where('object_id', $invoice->object_id)->first();
        $totalItem = 0;
        $totalDelivery = 0;
        $collectedDelivery = 0;
        $othtotalDelivery = 0;
        if (isset($regAcceptedOrders)) {
            foreach ($regAcceptedOrders as $regAcceptedOrder) {
                if ($regAcceptedOrder->status == 4 && $regAcceptedOrder->Order->status == 2) {
                    $othtotalDelivery += $regAcceptedOrder->delivery_price;
                    continue;
                } else {
                    $totalItem += $regAcceptedOrder->Order->order_price;
                }
                $collectedDelivery += $regAcceptedOrder->delivery_price;
            }
        }
        if (isset($othAcceptedOrders)) {
            foreach ($othAcceptedOrders as $othAcceptedOrder) {
                if ($othAcceptedOrder->status == 4 && $othAcceptedOrder->Order->status == 2) {
                    $othtotalDelivery += $othAcceptedOrder->delivery_price;
                    continue;
                } else {
                    $totalItem += $othAcceptedOrder->Order->order_price;
                }
                $othtotalDelivery += $othAcceptedOrder->delivery_price;
            }
        }
        $totalDelivery = $collectedDelivery + $othtotalDelivery;
        if ($invoice->type == 3) {
            $agent = Agent::where('id', $invoice->object_id)->first();
            $commission = ($agent->profit_rate / 100) * $totalDelivery;
            $othCommission = ($agent->profit_rate / 100) * $othtotalDelivery;
        } else {
            $commission = (5 / 100) * $totalDelivery;
            $othCommission = (5 / 100) * $othtotalDelivery;
        }
        if ($invoice->type == 2) {
            $result = $othtotalDelivery;
        } else {
            $result = $totalItem + $commission;
        }
        $message = 0;
        if ($wallet == null) {
            if ($invoice->type == 2) {
                if ($result < $totalItem) {
                    $message = 0;
                } else {
                    $message = 2;
                }
            } else {
                $message = 2;
            }
        } elseif ($result > $wallet->value) {
            if ($invoice->type == 2) {
                if ($result < $totalItem) {
                    $message = 0;
                } else {
                    $message = 1;
                }
            } else {
                $message = 1;
            }
        }
        return view('profile.veiw', compact('invoice', 'AllAcceptedOrders', 'totalItem', 'totalDelivery', 'collectedDelivery', 'othtotalDelivery', 'commission', 'othCommission', 'result', 'message'));

    }

    public function send_verify_code(Request $request)
    {
        if (!isset($request->phoneNo)) {
            $statusCode = 418;
            $message = __('backend.Please_Eneter_Correct_Phone_Number');
            return response()->json(['message' => $message, 'statusCode' => $statusCode]);
        }

        if ($request->type == 1) {
            $VerifyPhoneNo = Driver::where('id', $request->id)->first();
        } elseif ($request->type == 2) {
            $VerifyPhoneNo = Customer::where('corporate_id', $request->id)->where('mobile', $request->phoneNo)->first();
        } elseif ($request->type == 3) {
            $VerifyPhoneNo = Agent::where('id', $request->id)->first();
        }

        if ($VerifyPhoneNo && $VerifyPhoneNo->mobile == $request->phoneNo) {
            $resp_json = (new VerifyMobileController())->send_verify_code($request->phoneNo);
            //return response()->json(['message' => $resp_json, 'statusCode' => 410]);

            if (!empty($resp_json) && $resp_json != false && !empty($resp_json['requestError'])) {
                $statusCode = 410;
                $message = __('backend.We_Can_not_send_SMS_Something_Error');

            } else if (!empty($resp_json) && $resp_json != false && $resp_json['smsStatus'] == "MESSAGE_SENT") {
                $statusCode = 200;
                $message = array('message' => 'Done', 'pin_id' => $resp_json['pinId']);
            } else {
                $statusCode = 410;
                $message = __('backend.Something_error');
            }

            return response()->json(['message' => $message, 'statusCode' => $statusCode]);

        } else {
            $statusCode = 410;
            $message = __('backend.Phone_Number_is_Wrong');
            return response()->json(['message' => $message, 'statusCode' => $statusCode]);

        }
        return response()->json(['message' => $message, 'statusCode' => $statusCode]);
    }

    public function verify_phone(Request $request)
    {
        $concPhone = '002' . $request->phoneNo;
        $verify_code = $request->verify_code;
        $pin_id = $request->pin_id;
        if (!isset($request->phoneNo) || !isset($request->verify_code) || !isset($request->pin_id)) {
            $statusCode = 418;
            $message = __('backend.Invalid_Format');
        } else {
            $resp_json = (new VerifyMobileController())->verify_mobile_code($pin_id, $verify_code);
            return response()->json($resp_json, 410);

            if (!empty($resp_json) && $resp_json != false && !empty($resp_json['requestError'])) {
                $statusCode = 410;
                $message = __('backend.We_Can_not_send_SMS_Something_Error');

            } else if (!empty($resp_json) && $resp_json != false && !empty($resp_json['pinError'])) {
                #pinError
                $statusCode = 410;
                $message = __('backend.Wrong_Code_Something_Error');
            } else if (!empty($resp_json) && $resp_json != false) {
                $statusCode = 200;
                $message = array('message' => $resp_json);
            } else {
                $statusCode = 410;
                $message = __('backend.Something_error');
            }
        }
        return response()->json($message, $statusCode);
    }

    public function Transfer($sendIds, $id)
    {
        if ($sendIds == null) {
            return 2;
        } else {
            $Oldinvoice = Invoice::where('id', $id)->first();
            $NewInvoice = Invoice::create([
                'start_date' => Carbon::now(),
                'end_date' => null,
                'object_id' => $Oldinvoice->object_id,
                'type' => $Oldinvoice->type,
            ]);
            $invOrders = InvoiceOrder::where('invoice_id', $id)->whereIn('accepted_order_id', $sendIds)->update(['invoice_id' => $NewInvoice->id]);
            if ($invOrders == 1) {
                return 1;
            } else {
                return 0;
            }
        }

    }

    public function transfer_orders(Request $request)
    {

        $sendIds = $request->sendId;  // order id
        $id = $request->invoice_id;  // invoice id

        $invOrdersCount = InvoiceOrder::where('invoice_id', $id)->count();

        if (count($sendIds) < 1) {
            return response()->json(array('message' => __('backend.No_Order_Sent_You_must_choose_Order_To_transfer_it'), 'statusCode' => 420), 200);

        } else if (count($sendIds) >= $invOrdersCount) {
            //this to check if transfer order less than count of invoice order
            return response()->json(array('message' => __('backend.Invoice_Order_count'), 'statusCode' => 420), 200);

        } else {
            $Oldinvoice = Invoice::where('id', $id)->first();
            if (!$Oldinvoice) {
                return response()->json(array('message' => __('backend.Old_Wallet_Error_Not_Found'), 'statusCode' => 420), 200);
            }
            DB::beginTransaction();
            try {
                $NewInvoice = Invoice::create([
                    'start_date' => Carbon::now(),
                    'end_date' => null,
                    'object_id' => $Oldinvoice->object_id,
                    'type' => $Oldinvoice->type,
                ]);

                $invOrders = InvoiceOrder::where('invoice_id', $id)->whereIn('accepted_order_id', $sendIds)->update(['invoice_id' => $NewInvoice->id]);

                DB::commit();
                return response()->json(array('message' => __('backend.Done_Orders_transfer_successfully_please_reload_this_page'), 'statusCode' => 200), 200);


            } catch (Exception $e) {
                DB::rollback();

                return response()->json(array('message' => __('backend.Something_error'), 'statusCode' => 420), 200);

            }
        }
    }

    public function collect($id, Request $request)
    {

        $invoice = Invoice::find($id);

        $invoice->collected = 1;
        if ($invoice->ufelix_deserved > 0) {
            $invoice->paid_amount = min($request->collect_amount, $invoice->ufelix_deserved);
        }
        $invoice->save();

        Order::join('accepted_orders', 'accepted_orders.order_id', 'orders.id')
            ->join('invoice_orders', 'invoice_orders.accepted_order_id', 'accepted_orders.id')
            ->where('invoice_id', $id)
            ->update(['payment_status' => 1, 'payment_date' => Carbon::now()]);

        $orderIds = AcceptedOrder::join('invoice_orders', 'invoice_orders.accepted_order_id', 'accepted_orders.id')
            ->where('invoice_id', $id)
            ->pluck('accepted_orders.order_id')->toArray();

        event(new OrderUpdated($orderIds, Auth::guard($this->guardType)->user()->id, 1, 'collect'));

        $wallet = Wallet::where('type', $invoice->type)->where('object_id', $invoice->object_id)->first();

        //add
        WalletLog::insert([
            [
                'value' => $request->collect_amount,
                'reason_en' => 'Collected Amount',
                'reason_ar' => 'المبلغ المدفوع',
                'wallet_id' => $wallet->id,
                'invoice_id' => $invoice->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ]);

        if ($invoice->ufelix_deserved > 0 && $request->collect_amount < $invoice->ufelix_deserved) {
            Invoice::create([
                'start_date' => Carbon::now(),
                'end_date' => null,
                'debit_amount' => ($invoice->ufelix_deserved - $request->collect_amount),
                'object_id' => $invoice->object_id,
                'type' => $invoice->type,
            ]);
        }

        Flash::success(__('backend.Invoice_collected_successfully'));
        return redirect()->back();
    }

    public function Transaction(Request $request)
    {

        $invoice = Invoice::where('type', $request->Type)->where('id', $request->Id)->first();  // get invoice type

        if ($invoice) {

            $wallet = Wallet::firstOrCreate(
                ['type' => $request->Type, 'object_id' => $request->objId],
                ['type' => $request->Type, 'object_id' => $request->objId, 'status' => 1]
            );

//            if (!$wallet) {
//                return array('message' => __('backend.This_User_does_not_have_wallet'), 'statusCode' => 420);
//            } else if ($wallet->status == 0) {
//                return array('message' => __('backend.The_Wallet_of_This_User_is_closed'), 'statusCode' => 420);
//            }

            DB::beginTransaction();

            try {
                $walletTransactions = [];
                if (($request->customer_des > 0 && $request->Type == 2)
                    || ($request->Type == 1 && ($request->customer_des || ($request->ufelix_des - $invoice->debit_amount) > 0))) {
                    $walletTransactions[] = [
                        'value' => $request->customer_des,
                        'reason_en' => 'Deserved Amount',
                        'reason_ar' => 'المبلغ المستحق',
//                        'type' => $request->Type,
                        'wallet_id' => $wallet->id,
                        'invoice_id' => $invoice->id,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ];
                    if ($request->Type == 1) {
                        $walletTransactions[] = [
                            'value' => ($request->ufelix_des - $invoice->debit_amount) * -1,
                            'reason_en' => 'Debit Amount',
                            'reason_ar' => 'المبلغ المدين',
//                            'type' => $request->Type,
                            'wallet_id' => $wallet->id,
                            'invoice_id' => $invoice->id,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now()
                        ];
                    }

                    //add
                    WalletLog::insert($walletTransactions);

                    //sub save to ufelix
//                    WalletLog::create([
//                        'value' => $request->customer_des,
//                        'reason' => __('backend.Invoice_Payment'),
//                        'type' => '1',
//                        'wallet_id' => 1,
//                        'invoice_id' => $invoice->id,
//                    ]);
                }

//                if ($request->ufelix_des > 0 || $request->customer_des > 0) {
//
//                    WalletLog::create([
//                        'value' => $request->ufelix_des + $request->customer_des,
//                        'reason' => __('backend.Ufelix_Deserved_From_Invoice_Payment'),
//                        'type' => '2',
//                        'wallet_id' => 1,
//                        'invoice_id' => $invoice->id,
//
//                    ]);
//                }

//                if ($request->order_price > 0 && $request->Type == 2) {
////                    WalletLog::create([
////                        'value' => $request->order_price,
////                        'reason' => __('backend.Order_Price_From_Invoice_Payment'),
////                        'type' => '2',
////                        'wallet_id' => $wallet->id,
////                        'invoice_id' => $invoice->id,
////
////                    ]);
//
//                    WalletLog::create([
//                        'value' => $request->order_price,
//                        'reason' => __('backend.Order_Price_From_Invoice_Payment'),
//                        'type' => '1',
//                        'wallet_id' => 1,
//                        'invoice_id' => $invoice->id,
//
//                    ]);
//                }

//                if ($request->order_price > 0 && $request->Type == 1) {
//                    WalletLog::create([
//                        'value' => $request->order_price,
//                        'reason' => __('backend.Order_Price_From_Invoice_Payment'),
//                        'type' => '1',
//                        'wallet_id' => $wallet->id,
//                        'invoice_id' => $invoice->id,
//
//                    ]);
//                }

                /*UfelixWallet*/
                Invoice::where('type', $request->Type)
                    ->where('id', $request->Id)
                    ->where('object_id', $request->objId)
                    ->update([
                        'end_date' => now(), 'status' => '1', 'profit' => $request->profit,
                        'customer_deserved' => $request->customer_des, 'ufelix_deserved' => $request->ufelix_des
                    ]);

                if (isset($request->price_list)) {
                    $price_list = $request->price_list;
                    $fees = $request->fees_list;
                    $e_fees = $request->e_fees_list;
                    foreach ($price_list as $key => $bonus) {
                        if (is_array($request->sendId) && in_array($key, $request->sendId)) {
                            InvoiceOrder::where('invoice_id', '=', $request->Id)
                                ->where('accepted_order_id', $key)
                                ->update(['captain_bonus' => $bonus, 'fees' => $fees[$key], 'e_fees' => $e_fees[$key]]);
                        }
                    }

                    if ($request->Type == 2) {
                        Order::join('accepted_orders', 'accepted_orders.order_id', 'orders.id')
                            ->join('invoice_orders', 'invoice_orders.accepted_order_id', 'accepted_orders.id')
                            ->whereIn('invoice_orders.accepted_order_id', array_keys($price_list))
                            ->update(['payment_status' => 2, 'payment_date' => Carbon::now()]);

                        $orderIds = AcceptedOrder::whereIn('accepted_orders.id', array_keys($price_list))
                            ->pluck('accepted_orders.order_id')->toArray();


                        event(new OrderUpdated($orderIds, Auth::guard($this->guardType)->user()->id, 1, 'pay'));
                    }
                }

                if (isset($request->pickups)) {
                    foreach ($request->pickups as $key => $pickup) {
                        InvoicePickup::where('invoice_id', '=', $request->Id)
                            ->where('pickup_id', $key)
                            ->update(['captain_bonus' => (isset($pickup['bonus']) ? $pickup['bonus'] : 0), 'pickup_price' => $pickup['price']]);
                    }
                }

                $invOrdersObj = InvoiceOrder::where('invoice_id', '=', $request->Id)
                    ->select('invoice_orders.id');

                $invPickupsObj = InvoicePickup::where('invoice_id', '=', $request->Id)
                    ->select('invoice_pickups.id');

                if (is_array($request->sendId) && count($request->sendId)) {
                    $invOrdersObj->whereNotIn('accepted_order_id', array_values($request->sendId));
                }

                if (is_array($request->sendIdTask) && count($request->sendIdTask)) {
                    $invPickupsObj->whereNotIn('pickup_id', array_values($request->sendIdTask));
                }

                $invOrders = $invOrdersObj->get();
                $invPickups = $invPickupsObj->get();

                if (count($invOrders) || count($invPickups)) {
                    $NewInvoice = Invoice::create([
                        'start_date' => Carbon::now(),
                        'end_date' => null,
                        'object_id' => $request->objId,
                        'type' => $request->Type,
                    ]);

                    foreach ($invOrders as $invOrder) {
                        $invOrder->invoice_id = $NewInvoice->id;
                        $invOrder->save();
                    }

                    foreach ($invPickups as $invPickup) {
                        $invPickup->invoice_id = $NewInvoice->id;
                        $invPickup->save();
                    }

                }

//                $this->print($request->Id, $request, 1);

                DB::commit();
                return array('message' => __('backend.Done_This_Invoice_closed_successfully_please_reload_this_page'), 'statusCode' => 200);

            } catch (Exception $e) {
                DB::rollback();

                return array('message' => __('backend.Something_error'), 'statusCode' => 420);
            }
        } else {
            return array('message' => __('backend.This_Inovice_does_not_exist'), 'statusCode' => 420);
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {

        $user = Auth::guard($this->guardType)->user();

        $type = app('request')->input('type');
        /*$invoices = Invoice::get();
        if (count($invoices)) {
            foreach ($invoices as $tmporder) {
                $tmporder->invoice_no 				= $tmporder->invoice_no.$tmporder->object_id;
                $tmporder->save();

            }
        }

*/
        $invoices = Invoice::orderBy('id', 'desc')->where('type', $type)
            ->orderBy('invoices.created_at', 'DESC');

        $objects = null;

        if ($type == 1) {
            $objects = Driver::where('is_active', 1)->select('drivers.*');
        } elseif ($type == 2) {
            $objects = Corporate::where('is_active', 1)->select('corporates.*')
                ->orderBy('corporates.name', 'ASC');
        }

        if ($user->is_warehouse_user) {
            if ($type == 1) {
                $objects = $objects->join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                    ->where('user_id', $user->id);
                $invoices = $invoices->join('drivers', 'drivers.id', '=', 'invoices.object_id')
                    ->join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                    ->where('user_id', $user->id);
            } elseif ($type == 2) {
                $objects = $objects->join('customers', 'customers.corporate_id', '=', 'corporates.id')
                    ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                    ->where('user_id', $user->id);
                $invoices = $invoices->join('corporates', 'corporates.id', '=', 'invoices.object_id')
                    ->join('store_user', 'store_user.store_id', '=', 'corporates.store_id')
                    ->where('user_id', $user->id);
            }
        }

        $objects = $objects->get();

        $object_id = $request->object_id;
        if (isset($object_id) && $object_id != "") {
            $invoices = $invoices->where('object_id', $object_id);
        }
        $invoices = $invoices->paginate('15');

        if ($request->ajax()) {
            return [
                'view' => view('backend.invoices.table', compact('invoices', 'type', 'objects'))->render(),
            ];
        }

        return view('backend.invoices.index', compact('invoices', 'type', 'objects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.invoices.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $invoice = new Invoice();
        $invoice->start_date = $request->input("start_date");
        $invoice->end_date = $request->input("end_date");
        $invoice->object_id = $request->input("object_id");
        $invoice->type = $request->input("type");
        $invoice->agent_id = $request->input("agent_id");
        $invoice->invoice_type = $request->input("invoice_type");
        $invoice->save();

        Flash::success(__('backend.Invoice_saved_successfully'));
        return redirect()->route('mngrAdmin.invoices.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show_old($id)
    {

        $regAcceptedOrders = Order::join('accepted_orders', 'orders.id', '=', 'accepted_orders.order_id')->
        join('drivers', 'accepted_orders.driver_id', '=', 'drivers.id')->
        join('invoice_orders', 'accepted_orders.id', '=', 'invoice_orders.accepted_order_id')->
        join('invoices', 'invoice_orders.invoice_id', '=', 'invoices.id')->
        select('orders.id', 'orders.type', 'orders.created_at', 'orders.order_price', 'orders.payment_method_id', 'orders.status as order_status', 'accepted_orders.delivery_price', 'accepted_orders.status as accepted_order_status', 'accepted_orders.received_date', 'accepted_orders.delivery_date', 'drivers.name', 'drivers.mobile')->
        where('invoices.id', '=', $id)->
        where('orders.payment_method_id', '<', 5)->
        get();

        $othAcceptedOrders = Order::join('accepted_orders', 'orders.id', '=', 'accepted_orders.order_id')->
        join('drivers', 'accepted_orders.driver_id', '=', 'drivers.id')->
        join('invoice_orders', 'accepted_orders.id', '=', 'invoice_orders.accepted_order_id')->
        join('invoices', 'invoice_orders.invoice_id', '=', 'invoices.id')->
        select('orders.id', 'orders.type', 'orders.created_at', 'orders.order_price', 'orders.payment_method_id', 'orders.status as order_status', 'accepted_orders.delivery_price', 'accepted_orders.status as accepted_order_status', 'accepted_orders.received_date', 'accepted_orders.delivery_date', 'drivers.name', 'drivers.mobile')->
        where('invoices.id', '=', $id)->
        where('orders.payment_method_id', '>', 4)->
        get();

        $AllAcceptedOrders = Order::join('accepted_orders', 'orders.id', '=', 'accepted_orders.order_id')->
        join('drivers', 'accepted_orders.driver_id', '=', 'drivers.id')->
        join('invoice_orders', 'accepted_orders.id', '=', 'invoice_orders.accepted_order_id')->
        join('invoices', 'invoice_orders.invoice_id', '=', 'invoices.id')->
        select('orders.id', 'orders.type', 'orders.created_at', 'orders.order_price', 'orders.payment_method_id', 'orders.status as order_status', 'accepted_orders.id as accepted_id', 'accepted_orders.delivery_price', 'accepted_orders.status as accepted_order_status', 'accepted_orders.received_date', 'accepted_orders.delivery_date', 'drivers.name', 'drivers.mobile')->
        where('invoices.id', '=', $id);

        $totalDeliveryFinal = $AllAcceptedOrders->sum('accepted_orders.delivery_price');

        $AllAcceptedOrders = $AllAcceptedOrders->get();


        $invoice = Invoice::findOrFail($id);

        $wallet = Wallet::where('object_id', $invoice->object_id)->first();
        $totalItem = 0;
        $totalDelivery = 0;
        $collectedDelivery = 0;
        $othtotalDelivery = 0;
        $orderDeliceredDone = 0;
        $corporate_deserved = 0;
        $ufelix_deserved = 0;

        $orderDeliceredDone = count($AllAcceptedOrders);

        if (isset($regAcceptedOrders)) {
            foreach ($regAcceptedOrders as $regAcceptedOrder) {
                if ($regAcceptedOrder->accepted_order_status == 4) {
                    $othtotalDelivery += $regAcceptedOrder->delivery_price;
                    continue;
                } else {
                    $totalItem += $regAcceptedOrder->order_price;
                    $collectedDelivery += $regAcceptedOrder->delivery_price;
                }
            }
        }
        if (isset($othAcceptedOrders)) {
            foreach ($othAcceptedOrders as $othAcceptedOrder) {
                if ($othAcceptedOrder->accepted_order_status == 4) {
                    $othtotalDelivery += $othAcceptedOrder->delivery_price;
                    continue;
                } else {
                    $totalItem += $othAcceptedOrder->order_price;
                    $othtotalDelivery += $othAcceptedOrder->delivery_price;
                }
            }
        }
        $totalDelivery = $collectedDelivery + $othtotalDelivery;
        if ($invoice->type == 3) {

            $agent = Agent::where('id', $invoice->object_id)->first();
            $commission = ($agent->profit_rate / 100) * $totalDelivery;
            $othCommission = ($agent->profit_rate / 100) * $othtotalDelivery;

        } elseif ($invoice->type == 1) {

            $driver = Driver::where('id', $invoice->object_id)->first();
            $agent = Agent::where('id', $driver->agent_id)->first();
            $commission = ($driver->profit / 100) * $totalDelivery;
            $othCommission = ($driver->profit / 100) * $othtotalDelivery;

        } else {


            $commission = (5 / 100) * $totalDelivery;
            $othCommission = (5 / 100) * $othtotalDelivery;


        }
        if ($invoice->type == 2) {
            $result = $othtotalDelivery;
            $corporate_deserved = $totalItem - $result;
            $ufelix_deserved = 0;
            if ($corporate_deserved < 0) {
                $corporate_deserved = 0;
                $ufelix_deserved = $result - $totalItem;
            }
        } else {
            $result = $totalItem + $commission;
        }
        $message = 0;
        if ($wallet == null) {
            if ($invoice->type == 2) {
                if ($result < $totalItem) {
                    $message = 0;
                } else {
                    $message = 2;
                }
            } else {
                $message = 2;
            }
        } elseif ($result > $wallet->value) {
            if ($invoice->type == 2) {
                if ($result < $totalItem) {
                    $message = 0;
                } else {
                    $message = 1;
                }
            } else {
                $message = 1;
            }
        }


        if (Auth::guard($this->guardType)->user()->role_id == 1) {

            return view('backend.invoices.show', compact('invoice', 'AllAcceptedOrders', 'totalItem', 'totalDelivery', 'collectedDelivery', 'othtotalDelivery', 'commission', 'othCommission', 'result', 'message', 'orderDeliceredDone', 'totalDeliveryFinal', 'ufelix_deserved', 'corporate_deserved'));

        } elseif (Auth::guard($this->guardType)->user()->role_id == 2) {

            return view('profile.invaice', compact('invoice', 'AllAcceptedOrders', 'totalItem', 'totalDelivery', 'collectedDelivery', 'othtotalDelivery', 'commission', 'othCommission', 'result', 'message'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $invoice = Invoice::findOrFail($id);

        return view('backend.invoices.edit', compact('invoice'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @param Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $invoice = Invoice::findOrFail($id);
        $invoice->start_date = $request->input("start_date");
        $invoice->end_date = $request->input("end_date");
        $invoice->object_id = $request->input("object_id");
        $invoice->type = $request->input("type");
        //$invoice->agent_id = $request->input("agent_id");
        //$invoice->invoice_type = $request->input("invoice_type");
        $invoice->save();
        Flash::warning(__('backend.Invoice_Updated_successfully'));
        return redirect()->route('mngrAdmin.invoices.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $invoice = Invoice::findOrFail($id);
        $invoice->delete();
        flash(__('backend.Invoice_deleted_successfully'))->error();
        return redirect()->route('mngrAdmin.invoices.index');
    }


    public function show($id, Request $request)
    {

        $invoice = Invoice::findOrFail($id);

        $pickups = PickUp::join('invoice_pickups', 'pick_ups.id', 'invoice_pickups.pickup_id')
            ->join('drivers', 'drivers.id', 'pick_ups.driver_id')
            ->whereIn('pick_ups.status', [2, 3])
            ->where('invoice_pickups.invoice_id', $id)
            ->select('pick_ups.id', 'pick_ups.pickup_number', 'pick_ups.pickup_number', 'drivers.name',
                'pick_ups.status', 'pick_ups.created_at', 'pick_ups.received_at', 'pick_ups.delivered_at', 'pick_ups.bonus_per_order',
                'pick_ups.delivery_price', 'invoice_pickups.captain_bonus', 'invoice_pickups.pickup_price', 'pick_ups.pickup_orders')
            ->withCount('pickup_orders')
            ->get();

        // this all orders that the driver has approved
        $AcceptedOrders = Order::with(['customer' => function ($query) {
            $query->with('Corporate');
        }, 'to_government'])->join('accepted_orders', 'orders.id', '=', 'accepted_orders.order_id')
            ->join('drivers', 'accepted_orders.driver_id', '=', 'drivers.id')
            ->join('invoice_orders as invOr', 'accepted_orders.id', '=', 'invOr.accepted_order_id')
            ->join('invoices', 'invOr.invoice_id', '=', 'invoices.id')
            ->join('payment_methods', 'payment_methods.id', '=', 'orders.payment_method_id')
            ->select('orders.id', 'orders.type', 'orders.created_at', 'orders.order_price', 'orders.payment_method_id', 'orders.customer_id',
                'orders.status', 'accepted_orders.id as accepted_id', 'drivers.bouns_of_delivery as driver_bonus',
                'orders.delivery_price', 'orders.overweight_cost', 'drivers.recall_price as recall_price', 'drivers.reject_price as reject_price',
                'orders.order_price as price', 'orders.received_at', 'orders.recalled_by', 'orders.warehouse_dropoff',
                'orders.delivered_at', 'drivers.name', 'drivers.mobile', 'invOr.id as invoice_order_id', 'orders.order_number', 'orders.reference_number',
                'orders.s_government_id', 'orders.r_government_id', 'orders.collected_cost', 'orders.delivery_status', 'orders.recalled_at', 'orders.rejected_at', 'orders.cancelled_at', 'orders.delivery_status',
                'orders.overload', 'orders.receiver_name', 'payment_methods.name as payment_name', 'payment_method_id', 'invOr.captain_bonus', 'invOr.fees', 'invOr.f_fees', 'invOr.e_fees',
                DB::raw("(SELECT count(*) from invoice_orders as invOrr inner join invoices as inv on invOrr.invoice_id = inv.id where invOr.accepted_order_id = invOrr.accepted_order_id and inv.type = 1 and inv.collected = 1) as paid"))
            ->where('invoices.id', '=', $id);

        if ($request->scan_collection_id) {
            $AcceptedOrders->join('scan_collection_orders', 'orders.id', '=', 'scan_collection_orders.order_id')
                ->where('scan_collection_orders.collection_id', $request->scan_collection_id);
        }


        if ($request->status) {
            if (in_array($request->status, [2, 3, 5, 8])) {
                $AcceptedOrders->where('orders.status', $request->status);
            } elseif ($request->status == 4) {
                $AcceptedOrders->where('orders.status', '4')
                    ->where(function ($query) {
                        $query->whereNotNUll('status_before_cancel')
                            ->where('status_before_cancel', '!=', 0);
                    });
            }

        } else {
            $AcceptedOrders->where(function ($query) use ($invoice) {
                if ($invoice->type == 2) {
                    $query->whereIn('orders.status', [3, 8, 5])
                        ->orWhere(function ($query) {
                            $query->where('orders.status', '4')
                                ->where(function ($query) {
                                    $query->whereNotNUll('status_before_cancel')
                                        ->where('status_before_cancel', '!=', 0);
                                });
                        });
                } else {
                    $query->whereIn('orders.status', [2, 3, 8, 5]);
                }
            });
        }

        if ($request->created_at) {
            $AcceptedOrders->whereDate('orders.created_at', '<=', $request->created_at);
        }

        if ($request->customer_id) {
            $AcceptedOrders->whereDate('orders.customer_id', $request->customer_id);
        }

        if ($request->last_update_date) {
            $AcceptedOrders->where(function ($query) use ($request, $invoice) {
                $query->whereDate('orders.delivered_at', $request->last_update_date)
                    ->orWhereDate('orders.recalled_at', $request->last_update_date)
                    ->orWhereDate('orders.rejected_at', $request->last_update_date);
                if ($invoice->type == 2) {
                    $query->orWhereDate('orders.cancelled_at', $request->last_update_date);
                }
            });
        }

        if ($request->paid == 1) {
            $AcceptedOrders->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('invoice_orders as invOrr')
                    ->join('invoices', 'invOrr.invoice_id', '=', 'invoices.id')
                    ->whereRaw('invOr.accepted_order_id = invOrr.accepted_order_id')
                    ->where('invoices.type', '=', 1)
                    ->where('invoices.collected', '=', 1);
            });
        } elseif ($request->paid == 2) {
            $AcceptedOrders->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('invoice_orders as invOrr')
                    ->join('invoices', 'invOrr.invoice_id', '=', 'invoices.id')
                    ->whereRaw('invOr.accepted_order_id = invOrr.accepted_order_id')
                    ->where('invoices.type', '=', 1)
                    ->where('invoices.collected', '=', 0);
            });
        }

        if ($request->filled('arrange')) {
            $AcceptedOrders->orderByRaw("FIELD(orders.status, 3,8,5,2)");
        }

        $AcceptedOrders->groupBy('invOr.accepted_order_id');

        if ($request->filled('limit')) {
            if ($request->limit != 'all') {
                $AcceptedOrders->limit($request->limit);
            }
        } else {
            $AcceptedOrders->limit(100);
        }

        $AllAcceptedPaginatedOrders = $AcceptedOrders->get();

        $customers = '';

        if ($invoice->type == 2) {
            $customers = Customer::where('corporate_id', $invoice->object_id)->get();
        }

        if (Auth::guard($this->guardType)->user()->role_id == 1) {
            if ($invoice->type == 1) {
                return view('backend.invoices.driver_view', compact('invoice', 'AllAcceptedPaginatedOrders', 'pickups', 'customers'));
            }

            return view('backend.invoices.corporate_view', compact('invoice', 'AllAcceptedPaginatedOrders', 'pickups', 'customers'));

        } elseif (Auth::guard($this->guardType)->user()->role_id == 2) {

            return view('profile.invaice', compact('invoice', 'AllAcceptedPaginatedOrders', 'pickups'));
        }

    } // end function show


    #reset_invoice_price
    public function reset_invoice_price(Request $request)
    {
        $invoice = Invoice::where('id', $request->invoice_id)->first();
        if (!empty($invoice)) {

            //make invoice order with default :D
            $invoice->price_type = null; // if price_tupe == null >> get price order in order table
            $invoice->save();
            return array('message' => __('backend.Return_to_Default_Saved_successfully'), 'statusCode' => 200);

        } else {
            return array('message' => __('backend.This_invoice_does_not_exist'), 'statusCode' => 420);
        }

    }

    //save price list to invoice order
    public function save_invoice_price(Request $request)
    {
        if (!empty($request->price_list) && count($request->price_list) > 0) {
            $invoice = Invoice::where('id', $request->invoice_id)->first();

            if (!empty($invoice)) {
                DB::beginTransaction();
                try {
                    //foreach
                    foreach ($request->price_list as $price_list) {
                        $order = InvoiceOrder::where('invoice_id', $request->invoice_id)->where('accepted_order_id', $price_list['order_id'])->where('id', $price_list['invoice_order_id'])->first();
                        if ($order) {
                            $order->price = $price_list['price'];
                            $order->save();
                        }
                    }
                    //make invoice order with price list :D
                    $invoice->price_type = 1;  // if price_type == 1 >> get price order in invoice_orders table
                    $invoice->save();

                    DB::commit();

                    return array('message' => __('backend.Price_List_Saved_successfully'), 'statusCode' => 200);

                } catch (Exception $e) {
                    DB::rollback();
                    return array('message' => __('backend.Something_error'), 'statusCode' => 420);
                }
            } else {
                return array('message' => __('backend.This_invoice_does_not_exist'), 'statusCode' => 420);
            }
        } else {
            return array('message' => __('backend.No_Pricing_Sent_You_must_predefine_Order_Price'), 'statusCode' => 420);
        }
    }

    #save_invoice_percent

    public function save_invoice_percent(Request $request)
    {
        $invoice = Invoice::where('id', $request->invoice_id)->first();

        if (!empty($invoice)) {
            DB::beginTransaction();
            try {
                //make invoice type percent :D
                $invoice->price_type = 2;
                $invoice->profit = $request->percent;
                $invoice->save();

                DB::commit();

                return array('message' => __('backend.Percentage_Saved_successfully'), 'statusCode' => 200);

            } catch (Exception $e) {
                DB::rollback();
                return array('message' => __('backend.Something_error'), 'statusCode' => 420);
            }
        } else {
            return array('message' => __('backend.This_invoice_does_not_exist'), 'statusCode' => 420);
        }
    }

    public function print($id, Request $request, $send_to_mail = 0)
    {

        $user = Auth::guard($this->guardType)->user();

        $invoice = Invoice::findOrFail($id);

        $pickups = PickUp::join('invoice_pickups', 'pick_ups.id', 'invoice_pickups.pickup_id')
            ->join('drivers', 'drivers.id', 'pick_ups.driver_id')
            ->whereIn('pick_ups.status', [2, 3])
            ->where('invoice_pickups.invoice_id', $id)
            ->select('pick_ups.id', 'pick_ups.pickup_number', 'pick_ups.pickup_number', 'drivers.name',
                'pick_ups.status', 'pick_ups.created_at', 'pick_ups.received_at', 'pick_ups.delivered_at', 'pick_ups.bonus_per_order',
                'pick_ups.delivery_price', 'invoice_pickups.captain_bonus', 'invoice_pickups.pickup_price', 'pick_ups.pickup_orders')
            ->withCount('pickup_orders')
            ->get();

        // this all orders that the driver has approved
        $AcceptedOrders = Order::with(['customer' => function ($query) {
            $query->with('Corporate');
        }, 'to_government'])->join('accepted_orders', 'orders.id', '=', 'accepted_orders.order_id')
            ->join('drivers', 'accepted_orders.driver_id', '=', 'drivers.id')
            ->join('invoice_orders as invOr', 'accepted_orders.id', '=', 'invOr.accepted_order_id')
            ->join('invoices', 'invOr.invoice_id', '=', 'invoices.id')
            ->join('payment_methods', 'payment_methods.id', '=', 'orders.payment_method_id')
            ->select('orders.id', 'orders.type', 'orders.created_at', 'orders.order_price', 'orders.payment_method_id', 'orders.customer_id',
                'orders.status', 'accepted_orders.id as accepted_id', 'drivers.bouns_of_delivery as driver_bonus',
                'orders.delivery_price', 'orders.overweight_cost', 'drivers.recall_price as recall_price', 'drivers.reject_price as reject_price',
                'orders.order_price as price', 'orders.received_at', 'orders.recalled_by', 'orders.warehouse_dropoff',
                'orders.delivered_at', 'drivers.name', 'drivers.mobile', 'invOr.id as invoice_order_id', 'orders.order_number', 'orders.reference_number',
                'orders.s_government_id', 'orders.r_government_id', 'orders.collected_cost', 'orders.delivery_status', 'orders.recalled_at', 'orders.rejected_at', 'orders.cancelled_at',
                'orders.overload', 'orders.receiver_name', 'payment_methods.name as payment_name', 'payment_method_id', 'invOr.captain_bonus', 'invOr.fees', 'invOr.f_fees', 'invOr.e_fees', 'orders.delivery_status',
                DB::raw("(SELECT count(*) from invoice_orders as invOrr inner join invoices as inv on invOrr.invoice_id = inv.id where invOr.accepted_order_id = invOrr.accepted_order_id and inv.type = 1 and inv.collected = 1) as paid"))
            ->where('invoices.id', '=', $id);

        if ($request->status) {
            if (in_array($request->status, [2, 3, 5, 8])) {
                $AcceptedOrders->where('orders.status', $request->status);
            } elseif ($request->status == 4) {
                $AcceptedOrders->where('orders.status', '4')
                    ->where(function ($query) {
                        $query->whereNotNUll('status_before_cancel')
                            ->where('status_before_cancel', '!=', 0);
                    });
            }

        } else {
            $AcceptedOrders->where(function ($query) use ($invoice) {
                if ($invoice->type == 2) {
                    $query->whereIn('orders.status', [3, 8, 5])
                        ->orWhere(function ($query) {
                            $query->where('orders.status', '4')
                                ->where(function ($query) {
                                    $query->whereNotNUll('status_before_cancel')
                                        ->where('status_before_cancel', '!=', 0);
                                });
                        });
                } else {
                    $query->whereIn('orders.status', [2, 3, 8, 5]);
                }
            });
        }

        if ($request->created_at) {
            $AcceptedOrders->whereDate('orders.created_at', '<=', $request->created_at);
        }

        if ($request->customer_id) {
            $AcceptedOrders->whereDate('orders.customer_id', $request->customer_id);
        }

        if ($request->last_update_date) {
            $AcceptedOrders->where(function ($query) use ($request, $invoice) {
                $query->whereDate('orders.delivered_at', $request->last_update_date)
                    ->orWhereDate('orders.recalled_at', $request->last_update_date)
                    ->orWhereDate('orders.rejected_at', $request->last_update_date);
                if ($invoice->type == 2) {
                    $query->orWhereDate('orders.cancelled_at', $request->last_update_date);
                }
            });
        }

        if ($request->paid == 1) {
            $AcceptedOrders->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('invoice_orders as invOrr')
                    ->join('invoices', 'invOrr.invoice_id', '=', 'invoices.id')
                    ->whereRaw('invOr.accepted_order_id = invOrr.accepted_order_id')
                    ->where('invoices.type', '=', 1)
                    ->where('invoices.collected', '=', 1);
            });
        } elseif ($request->paid == 2) {
            $AcceptedOrders->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('invoice_orders as invOrr')
                    ->join('invoices', 'invOrr.invoice_id', '=', 'invoices.id')
                    ->whereRaw('invOr.accepted_order_id = invOrr.accepted_order_id')
                    ->where('invoices.type', '=', 1)
                    ->where('invoices.collected', '=', 0);
            });
        }

        if ($request->filled('arrange')) {
            $AcceptedOrders->orderByRaw("FIELD(orders.status, 3,8,5,2)");
        }

        $AcceptedOrders->groupBy('invOr.accepted_order_id');

        if ($request->filled('limit')) {
            if ($request->limit != 'all') {
                $AcceptedOrders->limit($request->limit);
            }
        } else {
            $AcceptedOrders->limit(100);
        }

        $AllAcceptedPaginatedOrders = $AcceptedOrders->get();

        if ($send_to_mail && env('APP_ENV') == 'production') {
            Mail::to($invoice->owner->email)->send(new InvoiceClosed(compact('AllAcceptedPaginatedOrders', 'pickups', 'invoice', 'user')));
        } else {
            if ($invoice->type == 2) {
                return view('backend.invoices.corporate_printInvoice', compact('AllAcceptedPaginatedOrders', 'pickups', 'invoice', 'user'));
            } elseif ($invoice->type == 1 && $invoice->collected) {
                return view('backend.invoices.driver_collected_printInvoice', compact('AllAcceptedPaginatedOrders', 'pickups', 'invoice', 'user'));
            } else {
                return view('backend.invoices.driver_printInvoice', compact('AllAcceptedPaginatedOrders', 'pickups', 'invoice', 'user'));
            }
        }

    }


    /*public function showOrdersIndividuals()
    {
        // $orders = Order::join('orders', 'orders.id', '=', 'accepted_orders.order_id')->
        // join('drivers', 'accepted_orders.driver_id', '=', 'drivers.id')->
        // join('customers', 'orders.customer_id', '=', 'customers.id')->
        // select('orders.id AS order_id', 'accepted_orders.delivery_price', 'received_date', 'drivers.name AS driver_name', 'drivers.mobile AS driver_mobile', 'accepted_orders.status AS accepted_orders_status' , 'orders.type', 'orders.sender_name' ,'orders.order_price','orders.order_number' ,'orders.status AS order_status', 'orders.receiver_name' ,'orders.receiver_code', 'orders.id AS order_id', 'customers.name AS customer_name', 'customers.mobile')->
        // where(['customers.role_id' => '3','customers.corporate_id' => null
        // ])->paginate(50);
        //
        // return view('backend.invoices.orders_individuals', compact('orders'));

        // $orders = AcceptedOrder::join('customers', 'customers.id', '=', 'orders.customer_id')->
        // 										join('drivers', 'accepted_orders.driver_id', '=', 'drivers.id')->
        // 										join('orders', 'orders.id', '=', 'accepted_orders.order_id')->
        // 										select('orders.id AS order_id', 'accepted_orders.delivery_price', 'received_date', 'drivers.name AS driver_name', 'drivers.mobile AS driver_mobile', 'accepted_orders.status AS accepted_orders_status' , 'orders.type', 'orders.sender_name' ,'orders.order_price','orders.order_number' ,'orders.status AS order_status', 'orders.receiver_name' ,'orders.receiver_code', 'orders.id AS order_id', 'customers.name AS customer_name', 'customers.mobile')->
        // 										where(['customers.role_id' => 3, 'customers.corporate_id' => null])->get();
        //
        // return view('backend.invoices.orders_individuals', compact('orders'));

        // $orders = AcceptedOrder::join('orders', 'orders.id', '=', 'accepted_orders.order_id')->
        // 	join('drivers', 'accepted_orders.driver_id', '=', 'drivers.id')->
        // 	join('customers', 'orders.customer_id', '=', 'customers.id')->
        // 	join('corporates', 'customers.corporate_id', '=', 'corporates.id')->
        // 	select('accepted_orders.delivery_price', 'received_date', 'drivers.name AS driver_name', 'drivers.mobile AS driver_mobile', 'accepted_orders.status AS accepted_orders_status' , 'orders.type', 'orders.sender_name' ,'orders.order_price','orders.order_number' ,'orders.status AS order_status', 'orders.receiver_name' ,'orders.receiver_code', 'orders.id AS order_id', 'customers.name', 'customers.mobile')->
        // 	where('orders.customer_id', $id)->get();

        $orders = Order::join('customers', 'orders.customer_id', '=', 'customers.id')->
        join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')->
        join('drivers', 'accepted_orders.driver_id', '=', 'drivers.id')->
        select('accepted_orders.delivery_price', 'received_date', 'drivers.name AS driver_name', 'drivers.mobile AS driver_mobile', 'accepted_orders.status AS accepted_orders_status' , 'orders.type', 'orders.sender_name' ,'orders.order_price','orders.order_number' ,'orders.status AS order_status', 'orders.receiver_name' ,'orders.receiver_code', 'orders.id AS order_id', 'customers.name AS customer_name', 'customers.mobile AS customer_mobile')->
        where('orders.status', 3)->where(['customers.role_id' => 3, 'customers.corporate_id' => null])->paginate(15);


        return view('backend.invoices.orders_individuals', compact('orders'));



    }*/


    public function showOrdersIndividuals(Request $request)
    {

        $orders = Order::join('customers', 'orders.customer_id', '=', 'customers.id')->
        join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')->
        join('drivers', 'accepted_orders.driver_id', '=', 'drivers.id')->
        select('orders.created_at AS created_at', 'accepted_orders.delivery_price', 'received_date', 'drivers.name AS driver_name', 'drivers.mobile AS driver_mobile', 'accepted_orders.status AS accepted_orders_status', 'orders.type', 'orders.sender_name', 'orders.order_price', 'orders.order_number', 'orders.status AS order_status', 'orders.receiver_name', 'orders.receiver_code', 'orders.id AS order_id', 'customers.name AS customer_name', 'customers.mobile AS customer_mobile')->
        whereIn('orders.status', [1, 2, 3, 5])->where(['customers.role_id' => 3, 'customers.corporate_id' => null]);

        $date = app('request')->input('date');

        if (isset($date) && $date != "") {
            $orders = $orders->whereDate('orders.created_at', 'like', $date . '%');
        }

        $orders = $orders->orderBy('orders.id', 'asc')->paginate(50);

        if ($request->ajax()) {
            return [
                'view' => view('backend.invoices.table_orders_individuals', compact('orders'))->render(),
            ];
        }

        $orders = Order::join('customers', 'orders.customer_id', '=', 'customers.id')->
        join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')->
        join('drivers', 'accepted_orders.driver_id', '=', 'drivers.id')->
        select('orders.created_at AS created_at', 'accepted_orders.delivery_price', 'received_date', 'drivers.name AS driver_name', 'drivers.mobile AS driver_mobile', 'accepted_orders.status AS accepted_orders_status', 'orders.type', 'orders.sender_name', 'orders.order_price', 'orders.order_number', 'orders.status AS order_status', 'orders.receiver_name', 'orders.receiver_code', 'orders.id AS order_id', 'customers.name AS customer_name', 'customers.mobile AS customer_mobile')->
        whereIn('orders.status', [1, 2, 3, 5])->where(['customers.role_id' => 3, 'customers.corporate_id' => null])->orderBy('orders.id', 'asc')->paginate(50);

        return view('backend.invoices.orders_individuals', compact('orders'));

    }


}

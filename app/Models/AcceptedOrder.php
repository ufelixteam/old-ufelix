<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AcceptedOrder extends Model
{
    protected $guarded = ['id'];

    public function driver()
    {
        return $this->belongsTo('App\Models\Driver', 'driver_id');
    }

    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'order_id');
    }
}

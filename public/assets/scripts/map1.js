function myMap() {

  var mapCanvas = document.getElementById("map");

  var latitude = document.getElementById("latitude").value;

  var longitude = document.getElementById("longitude").value;


  var myCenter = new google.maps.LatLng('30.0668886','31.1962743'); 

  //var myCenter = new google.maps.LatLng(51.508742,-0.120850);

  var mapOptions = {center: myCenter, zoom: 14};
  var map = new google.maps.Map(mapCanvas,mapOptions);
  var marker = new google.maps.Marker({
    position: myCenter,
    draggable:true,
  });
  marker.setMap(map);
  geocodePosition(marker.getPosition());
  new google.maps.event.addListener(marker, 'dragend', function() {

    geocodePosition(marker.getPosition());
    $("#latitude").val(this.getPosition().lat());
    $("#longitude").val(this.getPosition().lng());

  });
  //var geoloccontrol = new klokantech.GeolocationControl(map, 20);
  
}
 
function geocodePosition(pos) {
  geocoder = new google.maps.Geocoder();
  geocoder.geocode({
    latLng: pos

  }, function(responses) {
    if (responses && responses.length > 0) {
      //   updateMarkerAddress(responses[0].formatted_address);
      $("#address").val(responses[0].formatted_address);
    } else {
      // updateMarkerAddress('Cannot determine address at this location.');
    }
  });
}
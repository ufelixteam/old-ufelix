<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CollectionOrder extends Model
{
    protected $guarded = ['id'];

    #is_saved used to check if collection saved products or not
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    public function listorders()
    {
        return $this->hasMany('App\Models\Order', 'collection_id');

    }

    public function processed_collection()
    {
        return $this->belongsTo('App\Models\CollectionOrder', 'processed_collection_id');
    }

    public function temporders()
    {
        return $this->hasMany('App\Models\TempOrder', 'collection_id');

    }

    public function getSavedSpanAttribute($value)
    {
        if ($this->is_saved == 1) {
            $value = "<span class='badge badge-pill label-success'>" . __('backend.saved') . "</span>";
        } else {

            $value = "<span class='badge badge-pill label-danger'>" . __('backend.Not_Saved') . "</span>";
        }
        return $value;
    }


    public function getCompletedSpanAttribute($value)
    {
        if ($this->is_saved == 1) {
            return "<span class='badge badge-pill label-info'>" . __('backend.completed') . "</span>";
        }elseif ($this->type == 1){
            return "<span class='badge badge-pill label-warning'>" . __('backend.under_processing') . "</span>";

        }

        return '';
    }

    public function getTargetSpanAttribute($value)
    {
        if ($this->uploaded_target == 1) {
            return __('backend.sheet');
        } elseif ($this->uploaded_target == 2) {
            return __('backend.dashboard');
        } elseif ($this->uploaded_target == 3) {
            return __('backend.mobile');
        } elseif ($this->uploaded_target == 4) {
            return __('backend.profile');
        } elseif ($this->uploaded_target == 5) {
            return __('backend.integration');
        }

        return '';
    }

    public function getAllowPickupAttribute()
    {

        $pickup_orders_count = Order::leftJoin('pick_up_orders', 'pick_up_orders.order_id', '=', 'orders.id')
            ->leftJoin('pick_ups', 'pick_up_orders.pick_up_id', '=', 'pick_ups.id')
            ->where('collection_id', $this->id)
            ->where(function ($query) {
                $query->orWhere('orders.status', '!=', 0)
                    ->orWhere(function ($query) {
                        $query->where('pick_up_orders.status', '!=', 3)
                            ->where('pick_ups.status', '!=', 3);
                    });
            })
            ->count();

        if ($pickup_orders_count) {
            return false;
        } else {
            return true;
        }

    }/* check if order is pickup or not */

    public function getAllowPickupStatusAttribute()
    {

        $orders_count = Order::where('collection_id', $this->id)
            ->where('orders.status', '!=', 0)
            ->count();

        if ($orders_count) {
            return 3;
        } else {
            $pickup_orders_count = Order::leftJoin('pick_up_orders', 'pick_up_orders.order_id', '=', 'orders.id')
                ->leftJoin('pick_ups', 'pick_up_orders.pick_up_id', '=', 'pick_ups.id')
                ->where('orders.collection_id', $this->id)
                ->where('pick_up_orders.status', '!=', 3)
                ->where('pick_ups.status', '!=', 3)
                ->count();

            if ($pickup_orders_count) {
                return 2;
            } else {
                return 1;
            }
        }

    }/* check if order is pickup or not */

    public function getIsOrdersPendingAttribute()
    {
        if ($this->listorders_count == $this->listorders->where('status', 0)->count()) {
            return true;
        } else {
            return false;
        }
    }
}

@extends('backend.layouts.app')
@section('css')
  <title>Vehicles Types - View</title>
@endsection
@section('header')
<div class="page-header">
        <h3>Show #{{$type->id}}</h3> {!! $type->status_span !!}
        <form action="{{ route('mngrAdmin.types.destroy', $type->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('mngrAdmin.types.edit', $type->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="form-group col-sm-6">
              <label for="nome">#</label>
              <p class="form-control-static">{{$type->id}}</p>
            </div>
            <div class="form-group col-sm-6">
               <label for="name">NAME</label>
               <p class="form-control-static">{{$type->name_en}}</p>
            </div>
            <div class="col-md-12 col-xs-12 col-sm-12">
                <a class="btn btn-link" href="{{ route('mngrAdmin.types.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
            </div>
        </div>
    </div>
@endsection

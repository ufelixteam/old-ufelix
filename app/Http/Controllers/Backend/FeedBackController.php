<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use App\Models\Review;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Feedback;
use Response;
use Flash;
use File;
use Auth;

class FeedBackController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$list = Feedback::orderBy('id', 'desc')->paginate(50);

		return view('backend.feedback.index', compact('list'));
	}



    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function reviews()
    {
        $list = Review::orderBy('id', 'desc')->paginate(50);

        return view('backend.feedback.reviews', compact('list'));
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$feedback = Feedback::findOrFail($id);

		return view('backend.feedback.show', compact('feedback'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

	}

}

<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        @if($collections->count())
            <table class="table table-condensed table-striped text-center">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('backend.collection_number')}}</th>
                    <th>{{__('backend.driver')}}</th>
                    <th>{{__('backend.scan_type')}}</th>
                    <th>{{__('backend.count_orders')}}</th>
                    <th>{{__('backend.Create_at')}}</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                @foreach($collections as $collection)
                    <tr>
                        <td>{{$collection->id}}</td>
                        <td>{{$collection->collection_number}}</td>
                        <td>{{isset($collection->driver->name) ? $collection->driver->name : '-'}}</td>
                        <td>{!!$collection->scan_type_span!!}</td>
                        <td>{{$collection->orders_count}}</td>
                        <td>{{$collection->created_at}}</td>
                        <td>
                            <a class="btn btn-xs btn-primary "
                               href="{{ URL::asset('mngrAdmin/invoices/scan-collection/'.$collection->id) }}">
                                {{__('backend.invoice')}}
                            </a>
                            <a class="btn btn-xs btn-primary " target="_blank"
                               href="{{ URL::asset('mngrAdmin/reports/scan_collection_print?scan_collection_id='.$collection->id) }}">
                                {{__('backend.print')}}
                            </a>
                            <a class="btn btn-xs btn-success " target="_blank"
                               href="{{ URL::asset('mngrAdmin/scan_excel?scan_collection_id='.$collection->id) }}">
                                {{__('backend.Excel')}}
                            </a>
                            <form
                                action="{{ URL::asset('/mngrAdmin/scan_collections/'. $collection->id) }}"
                                method="POST" style="display: inline;"
                                onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-xs btn-danger"><i
                                        class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {!! $collections->appends($_GET)->links() !!}
        @else
            <h3 class="text-center alert alert-warning">No Result Found !</h3>
        @endif
    </div>
</div>

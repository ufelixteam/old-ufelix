<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        @if(! empty($orders) && count($orders) > 0)
            <form method="post" id="myform" action="#">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <div class="table-responsive">
                    <table id="theTable" class="table table-condensed table-striped text-center"
                           style="min-height: 200px;">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('captain.captain')}}</th>
                            <th>{{__('captain.receiver_name')}}</th>
                            @if(app('request')->input('type') == 'finish_not_dropped')
                            <th>{{__('captain.address')}}</th>
                            @endif
                            <th>{{__('captain.sender_name')}}</th>
                            <th>{{__('captain.to')}}</th>
                            <th style="font-weight: bold; color: #333;">{{__('captain.order_number')}}</th>
                            <th>{{__('captain.total_cost')}}</th>
                            <th>{{__('captain.status')}}</th>
{{--                            <th style="width: 150px;">--}}
{{--                                <button type="button" id="pdf-submit"--}}
{{--                                        class="btn btn-warning btn-pdf btn-xs ">{{__('captain.pdf')}}</button>--}}
{{--                            </th>--}}

{{--                            <th></th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $i => $order)
                            <tr>
                                <td>{{($i+1)}}</td>
                                <td>{{$order->driver->name}}</td>
                                <td>{{$order->receiver_name}}</td>
                                @if(app('request')->input('type') == 'finish_not_dropped')
                                <td>{{$order->receiver_address}}</td>
                                @endif
                                <td>
                                    {{$order->sender_name}}
                                    @if($order->customer && $order->customer->Corporate)
                                        <strong style="color: #0c81bc"> {{$order->customer->Corporate->name}}</strong>
                                    @endif
                                </td>
                                <td>
                                    @if(! empty($order->to_government) )
                                        {{$order->to_government->name_en}}
                                    @endif
                                </td>
                                <td style="font-weight: bold; color: #a43;">{{$order->order_number}}</td>
                                <td>{{$order->total_price}}</td>
                                <td>{!! $order->status_details !!}</td>

{{--                                <td class="selectpdf" id="selectpdf">--}}
{{--                                    <input type="checkbox" name="select[]" value="{{$order->id}}"--}}
{{--                                           class="selectOrder"--}}
{{--                                           data-value="{{$order->id}}"/>--}}

{{--                                </td>--}}


{{--                                <td>--}}
{{--                                    <a href="{{ route('captainDashboard.orders.show', $order->id ) }}"><i--}}
{{--                                            class="fa fa-eye"></i> {{__('captain.view')}}</a>--}}
{{--                                </td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </form>
            {!! $orders->appends($_GET)->links() !!}
        @else
            <h3 class="text-center alert alert-warning">No Result Found !</h3>
        @endif
    </div>
</div>
<div id="js-form" class="js-form">
</div>

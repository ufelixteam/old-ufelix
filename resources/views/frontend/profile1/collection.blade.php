@extends('frontend.profile.layouts.app')

@section('title') @lang('website.FormCollection') @endsection
@section('css')
    <style>
        .pac-container {
            z-index: 9999 !important;
        }

    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card card-default">
                <div class="card-header">
                    <h4 class="card-title m-b-0">@lang('website.AddCollection') </h4>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="alert alert-danger" style="display:none"></div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="alert alert-success" style="display:none"></div>
                    </div>
                    <div class="container-fluid">
                        <div id="create-form" class="create-form">
                            @include('frontend.profile.collections.create_form')
                        </div>
                    </div>
                    <div class="row">
                        <div class="table-responsive">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="list">
                                    @include('frontend.profile.collections.tmptable')
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

        <div class="col-lg-12 col-md-12" style="margin-top: 30px">

            <button type="button" id="complete_later"
                    class="btn btn-info btn-rounded btn-block  @if(empty($collection) || (!empty($collection) && $collection->complete_later == 1)) hidden @endif">
                <i
                    class="fa fa-check"></i>@lang('website.send_orders')</button>

        </div>
        <div id="js-form" class="js-form"></div>

        @include('frontend.profile.collections.map')
        @include('frontend.profile.collections.map1')

        @endsection

        @section('scripts')

            <script type="text/javascript" src="{{ asset('/assets/scripts/map-order.js')}}"></script>
            <script
                src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key=AIzaSyDQiN4qc7pC9b1BRQwkqXXD28peMfWcHvw&callback=myMap"></script>
            @include('frontend.profile.collections.js')
    </div>
@endsection


<div class="row">
  <div class="col-md-12 col-xs-12 col-sm-12">
    <?php if(! empty($activityLogs) && count($activityLogs) > 0): ?>
        <table id="theTable" class="table table-condensed table-striped text-center">
          <thead>
              <tr>
                <th>#<?php echo e(__('backend.id')); ?></th>
                <th><?php echo e(__('backend.username')); ?></th>
                <th><?php echo e(__('backend.usertype')); ?></th>
                
                <th><?php echo e(__('backend.subject')); ?></th>
                <th><?php echo e(__('backend.date')); ?></th>
                <th><?php echo e(__('backend.time')); ?></th>
                <th><?php echo e(__('backend.delete')); ?></th>
                <td  class="text-right"></td>
              </tr>
          </thead>
          <tbody>
              <?php $__currentLoopData = $activityLogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $activity): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <tr>
                      <td><?php echo e($activity->id); ?></td>
                      <td><?php echo e($activity->object_name); ?></td>
                      <td>
                          <?php if($activity->object_type == 1): ?>
                              <?php if(session()->has('lang') == 'ar'): ?>
                                  مشرف
                              <?php else: ?>
                                  Admin
                              <?php endif; ?>
                          <?php elseif($activity->object_type == 2): ?>
                              <?php if(session()->has('lang') == 'ar'): ?>
                                  وكيل
                              <?php else: ?>
                                  Agent
                              <?php endif; ?>
                          <?php endif; ?>
                      </td>
                      
                      <td>
                          <?php if(session('lang') == 'ar'): ?>
                            <?php echo e($activity->subject_ar); ?>

                          <?php else: ?>
                            <?php echo e($activity->subject_en); ?>

                          <?php endif; ?>
                      </td>
                      <td><?php echo e($activity->created_at->format('d-m-Y')); ?></td>
                      <td><?php echo e($activity->created_at->format('H:i:s')); ?></td>
                      <td>
                        <?php if(permission('deleteActivityLog')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                          <form action="<?php echo e(route('mngrAdmin.activity_log.delete', $activity->id)); ?>" method="POST" style="display: inline;" onsubmit="if(confirm('<?php echo e(__('backend.msg_confirm_delete')); ?>')) { return true } else {return false };">
                              <input type="hidden" name="_method" value="DELETE">
                              <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                              <div class="btn-group pull-right" role="group" aria-label="...">
                                  <button type="submit" class="btn btn-danger btn-sm"> <?php echo e(__('backend.delete')); ?> <i class="glyphicon glyphicon-trash"></i></button>
                              </div>
                          </form>
                        <?php endif; ?>
                      </td>
                  </tr>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </tbody>
        </table>
        <?php echo $activityLogs->appends($_GET)->links(); ?>

    <?php else: ?>
      <h3 class="text-center alert alert-warning"><?php echo e(__('backend.No_result_found')); ?></h3>
    <?php endif; ?>
  </div>
</div>
<?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/activity_log/table.blade.php ENDPATH**/ ?>
<form id="order-form" method="POST" enctype="multipart/form-data" style="margin-bottom: 70px;">
    <input type="hidden" id="collection_id" name="collection_id"
           value="{{ ! empty($collection_id) ? $collection_id : '0' }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="corparate_id" id="corparate_id"
           value="{{@Auth::guard('Customer')->user()->corporate_id}}">
    <input type="hidden" id="order_type_id" name="order_type_id"
           value="{{@Auth::guard('Customer')->user()->Corporate->order_type ? @Auth::guard('Customer')->user()->Corporate->order_type : 2}}">

    <div class="row">
        <div class="col-md-6 col-xs-12">
            <div class="ribbon-wrapper card receiver-card">
                <div class="ribbon ribbon-default">Receiver Data</div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-12 col-md-12">
                            Name:
                            <div>
                                <input id="receiver_name-field" name="receiver_name" class="form-control"
                                       value="<?php echo e(old("receiver_name")); ?>" type="text" placeholder="Name">
                            </div>
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-12 col-md-12">Mobile Number:
                            <div>
                                <input class="form-control" type="number" id="receiver_mobile-field"
                                       name="receiver_mobile"
                                       onkeypress="return event.charCode >= 48" min="1"
                                       placeholder="Mobile Number">
                            </div>
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-12 col-md-12">Mobile Number 2:
                            <div>
                                <input class="form-control" type="number" id="receiver_phone-field"
                                       name="receiver_phone"
                                       onkeypress="return event.charCode >= 48" min="1"
                                       placeholder="Mobile Number 2">
                            </div>
                        </label>
                    </div>
                </div>


                <div class="form-group">
                    <div class="row">
                        <label class="col-md-12" style="position: relative;"> Receiver Address:
                            <div>

                                <input class="form-control" type="text" id="receiver_address" name="receiver_address"
                                       placeholder="Receiver Address">
                            </div>
                        </label>
                    </div>
                </div>


                <div class="form-group">
                    <div class="row mt-28">
                        <label class="col-sm-4">Government: </label>
                        <div class="col-12 col-md-4">

                            <select class=" select2 form-control r_government_id" id="r_government_id"
                                    name="r_government_id">
                                <option value="" data-display="Select"> Government</option>
                                <?php if(!empty($governments)): ?>
                                <?php $__currentLoopData = $governments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $government): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($government->id); ?>">
                                    <?php echo e($government->name_en); ?>

                                </option>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>

                            </select>
                        </div>

                        <label class="col-sm-1"> City: </label>
                        <div class="col-12 col-md-3">
                            <select class="select2 form-control r_state_id" id="r_state_id" name="r_state_id">
                                <option value=""> City</option>

                            </select>
                        </div>

                    </div>
                </div>


                <div class="form-group">
                    <div class="row" style="margin-top:50px">
                        <label class="col-sm-4"> Location (optional): </label>
                        <div class="col-12 col-md-8">
                            <div class="row">
                                <div class="col-1 col-md-1">
                                    <i class="icon-map-pin" data-toggle="modal" data-target="#map1Modal"
                                       data-whatever="@fat"
                                       style=""></i>
                                </div>
                                <div class="col-6 col-md-6 location-1st-input">
                                    <input class="form-control" type="text" name="receiver_latitude"
                                           id="receiver_latitude" placeholder="Latitude" value="30.0668886">
                                </div>
                                <div class="col-5 col-md-5 location-2nd-input">
                                    <input class="form-control" type="text" name="receiver_longitude"
                                           id="receiver_longitude" placeholder="Longitude" value="31.1962743">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-6 col-xs-12">
            <div class="ribbon-wrapper card order-card">
                <div class="ribbon ribbon-info">Order Data</div>

                <div class="form-group">

                    <div class="row">
                        <label class="col-12 col-md-12">
                            Name:
                            <div>
                                <input class="form-control " type="text" name="type" id="type-field"
                                       placeholder="Type Name">
                            </div>
                        </label>
                    </div>
                </div>

                <div class="form-group">

                    <div class="row">
                        <label class="col-12 col-md-12">
                            Reference Number:
                            <div>
                                <input class="form-control " type="text" name="reference_number"
                                       id="reference_number-field"
                                       placeholder="Reference Number">
                            </div>
                        </label>
                    </div>
                </div>

                <div
                    class="form-group @if($errors->has('delivery_type') || $errors->has('sender_subphone')) has-error @endif">
                    <div class="row">

                        <label class="col-sm-2" style="font-size: 14px;margin-top: 20px;">Delivery Type: </label>

                        <div class="col-md-4">

                            <input type="text" id="delivery_type-field" name="delivery_type"
                                   class="form-control"
                                   onkeypress="return event.charCode >= 48" min="1"
                                   value="{{ old("delivery_type") }}"/>
                            @if($errors->has("delivery_type"))
                                <span class="help-block">{{ $errors->first("delivery_type") }}</span>
                            @endif
                        </div>

                        <label class="col-sm-2" style="font-size: 14px;margin-top: 20px;">Sender Subphone: </label>

                        <div class="col-md-4">

                            <input type="text" id="sender_subphone-field" name="sender_subphone"
                                   class="form-control"
                                   value="{{ old("sender_subphone") }}"/>
                            @if($errors->has("sender_subphone"))
                                <span class="help-block">{{ $errors->first("sender_subphone") }}</span>
                            @endif
                        </div>


                    </div>
                </div>

                <div class="form-group">
                    <div class="row mt-40">
                        <label class="col-sm-2">Pick-up Center:</label>
                        <div class="col-md-4">
                            <select id="store_id-field" name="main_store" class="form-control"
                                    <?php if($stores->isEmpty()): ?> disabled <?php endif; ?>>
                                <option value="">Choose Pick-up Center</option>
                                <?php if(!empty($allStores)): ?>
                                <?php $__currentLoopData = $allStores; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $store): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option
                                    value="<?php echo e($store->id); ?>" <?php echo e($store->id == old('main_store') ? 'selected' : ''); ?> >
                                    <?php echo e($store->name); ?>

                                </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>

                            </select>
                        </div>

                        <label class="col-sm-2">Stock: </label>

                        <div class="col-md-4">
                            <select id="stock_id-field" name="store_id" class="form-control"
                                    <?php if($stores->isEmpty()): ?> disabled <?php endif; ?>>
                                <option value="">Choose Stock</option>
                                <?php if(!empty($stores)): ?>
                                <?php $__currentLoopData = $stores; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stock): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option
                                    value="<?php echo e($stock->id); ?>" <?php echo e($stock->id == old('store_id') ? 'selected' : ''); ?> >
                                    <?php echo e($stock->title); ?>

                                </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>

                            </select>

                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row mt-40">

                        <label class="col-sm-2" style="font-size: 14px;margin-top: 0px;">Quantity Order: </label>


                        <div class="col-md-4">

                            <input class="form-control" id="qty-field" type="number" name="qty" placeholder="Quantity"
                                   value="0" <?php if($stores->isEmpty()): ?> disabled <?php endif; ?>>
                            Available Quantity: <span id="available_quantity">0</span>
                        </div>


                        <label class="col-sm-2" style="font-size: 14px;margin-top: 0px;">Overweight: </label>


                        <div class="col-md-4">

                            <input type="number" required id="overload-field" name="overload" class="form-control"
                                   placeholder="Overweight"
                                   value="<?php echo e(old("overload") ? old("overload") : '0'); ?>" disabled/>
                        </div>


                    </div>
                </div>

                <div class="form-group">
                    <div class="row mt-35">

                        <label class="col-sm-2" style="font-size: 14px;">Delivery Fees: </label>

                        <div class="col-md-4">

                            <input class="form-control" type="number" id="delivery_price-field" name="delivery_price"
                                   placeholder="Delivery price" value="0" readonly>
                        </div>

                        <label class="col-sm-2" style="font-size: 14px;">Order Price : </label>
                        <div class="col-md-4">

                            <input class="form-control" id="order_price-field" type="number" name="order_price"
                                   value="0"
                                   placeholder="Order Price">
                        </div>


                    </div>
                </div>


                <div class="form-group">
                    <div class="row">


                        <label class="col-sm-2" style="font-size: 14px;margin-top: 0px;">Delivery Fees From : </label>
                        <div class="col-md-4">

                            <select id="payment_method_id-field" name="payment_method_id" class="form-control select2">
                                <option value="" data-display="Select">Choose Method</option>

                                <?php if(!empty($payments)): ?>
                                <?php $__currentLoopData = $payments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($payment->id); ?>">
                                    <?php echo e($payment->name); ?>

                                </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>

                            </select>
                        </div>


                        <label class="col-sm-2" style="font-size: 14px;margin-top: 0px;">Total Price: </label>

                        <div class="col-md-4">

                            <div class="form-control" id="total_price">0</div>
                        </div>

                    </div>
                </div>


                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-4"> Notes: </label>
                        <div class="col-md-8">

                            <textarea class="form-control" name="notes" rows="4" cols="80"
                                      style="resize: vertical; min-height: 100px"
                                      placeholder="notes..."></textarea>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="ribbon-wrapper card">
                        <div class="ribbon ribbon-default">Change Sender Address</div>

                        <div class="form-group">
                            <div class="row mt-28">
                                <label class="col-sm-4">Government: </label>
                                <div class="col-12 col-md-8">

                                    <select class=" select2 form-control s_government_id" id="s_government_id"
                                            name="s_government_id">
                                        <option value="" data-display="Select">Sender Government</option>
                                        <?php if(!empty($governments)): ?>
                                        <?php $__currentLoopData = $governments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $government): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option
                                            value="<?php echo e($government->id); ?>">
                                            <?php echo e($government->name_en); ?>

                                        </option>

                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>

                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-4"> City: </label>
                                <div class="col-12 col-md-8">
                                    <select class="select2 form-control s_state_id" id="s_state_id" name="s_state_id">
                                        <option value="">Sender City</option>

                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-4"> Sender Mobile: </label>
                                <div class="col-12 col-md-8">
                                    <input class="form-control" type="text" id="sender_mobile" name="sender_mobile"
                                           value="<?php echo e(@Auth::guard('Customer')->user()->mobile); ?>"
                                           placeholder="Mobile Number">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-4"> Sender Address: </label>
                                <div class="col-12 col-md-8" style="position: relative;">
                                    <i class="mdi mdi-map-marker-radius" data-toggle="modal" data-target="#map2Modal"
                                       data-whatever="@fat"
                                       style="text-align: center; font-size: 25px; top: 0px; position: absolute; right: 20px;"></i>
                                    <input class="form-control" type="text" id="sender_address" name="sender_address"
                                           value="<?php echo e(@Auth::guard('Customer')->user()->address); ?>"
                                           placeholder="Sender Address">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-4"> Location Latitude : </label>
                                <div class="col-12 col-md-8">
                                    <input class="form-control" type="text" name="sender_latitude" id="sender_latitude"
                                           placeholder="Latitude"
                                           value="<?php echo e(@Auth::guard('Customer')->user()->latitude); ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-4"> Location Longitude : </label>
                                <div class="col-12 col-md-8">
                                    <input class="form-control" type="text" name="sender_longitude"
                                           id="sender_longitude" placeholder="Longitude"
                                           value="<?php echo e(@Auth::guard('Customer')->user()->longitude); ?>">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <?php if(empty($collection) || (!empty($collection) && $collection->complete_later != 1)): ?>
    <button type="button" id="order-btn" class="btn btn-info"><i
            class="fa fa-check"></i><?php echo app('translator')->getFromJson('website.AddOrder'); ?></button>


    {{--        @if(!empty($collection) && $collection->complete_later != 1)--}}
    {{--            <button type="button" id="complete_later" class="btn btn-info btn-rounded btn-block"><i--}}
    {{--                    class="fa fa-check"></i>@lang('website.send_orders')</button>--}}
    {{--        @endif--}}
    <a class="btn btn-danger @if(empty($collection) || (!empty($collection) && $collection->complete_later == 1)) hidden @endif" id="cancelCollection"
    href="{{url('/customer/account/form-collections/delete/'.@$collection->id)}}"><i
        class="fa fa-check"></i>@lang('backend.Cancel')</a>
    @endif
</form>


@extends('agent.layouts.app')
@section('css')

@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i>  Edit Vehicles #{{$truck->id}}</h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <form action="{{ route('mngrAgent.trucks.update', $truck->id) }}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="agent_id" value="{{ \Auth::guard('agent')->user()->agent_id }}">


                <div class="form-group col-md-6 col-sm-6 @if($errors->has('model_name')) has-error @endif">
                       <label for="model_name-field">Model_name</label>
                    <input type="text" id="model_name-field" name="model_name" class="form-control" value="{{ is_null(old("model_name")) ? $truck->model_name : old("model_name") }}"/>
                       @if($errors->has("model_name"))
                        <span class="help-block">{{ $errors->first("model_name") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('plate_number')) has-error @endif">
                       <label for="plate_number-field">Plate_number</label>
                    <input type="text" id="plate_number-field" name="plate_number" class="form-control" value="{{ is_null(old("plate_number")) ? $truck->plate_number : old("plate_number") }}"/>
                       @if($errors->has("plate_number"))
                        <span class="help-block">{{ $errors->first("plate_number") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('chassi_number')) has-error @endif">
                       <label for="chassi_number-field">Chassi_number</label>
                    <input type="text" id="chassi_number-field" name="chassi_number" class="form-control" value="{{ is_null(old("chassi_number")) ? $truck->chassi_number : old("chassi_number") }}"/>
                       @if($errors->has("chassi_number"))
                        <span class="help-block">{{ $errors->first("chassi_number") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('year')) has-error @endif">
                       <label for="year-field">Year</label>
                  

                    {{ Form::selectYear('year',  date('Y'),1980,$truck->year ,["class"=>"form-control"]) }}
                       @if($errors->has("year"))
                        <span class="help-block">{{ $errors->first("year") }}</span>
                       @endif
                    </div>
                       <div class="form-group col-md-6 col-sm-6 @if($errors->has('status')) has-error @endif">
                       <label for="status-field">Active (Verified/Not Verified)</label>
                        <select id="status-field" name="status" class="form-control" value="{{ old("status") }}" >
                      <option value="0" {{ old("status") == "0" || $truck->status == "0" ? 'selected' : '' }}>No</option>
                      <option value="1" {{ old("status") == "1" || $truck->status == "1" ? 'selected' : '' }}>Yes</option>
                    </select>
                    </select>
                    
                       @if($errors->has("status"))
                        <span class="help-block">{{ $errors->first("status") }}</span>
                       @endif
                    </div>
                     <div class="form-group col-md-6 col-sm-6 @if($errors->has('type_id')) has-error @endif">
                       <label for="type_id-field">Truck Type</label>
                    <select id="type_id-field" name="type_id" class="form-control" >
                        @if(! empty($truck_types))
                            @foreach($truck_types as $type)
                              <option value="{{$type->id}}" {{ old("type_id") == $type->id  || $truck->type_id == $type->id  ? 'selected' : '' }}>{{$type->name}}</option>

                            @endforeach

                        @endif
                    </select>
                       @if($errors->has("type_id"))
                        <span class="help-block">{{ $errors->first("type_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('color_id')) has-error @endif">
                       <label for="color_id-field">Color</label>
                     <select id="color_id-field" name="color_id" class="form-control" >
                        @if(! empty($colors))
                            @foreach($colors as $color)
                              <option value="{{$color->id}}" {{ old("color_id") == $color->id || $truck->color_id == $color->id ? 'selected' : '' }}>{{$color->name}}</option>

                            @endforeach

                        @endif
                    </select>
                       @if($errors->has("color_id"))
                        <span class="help-block">{{ $errors->first("color_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('weight_id')) has-error @endif">
                       <label for="weight_id-field">Weight</label>
                    <select id="weight_id-field" name="weight_id" class="form-control" >
                        @if(! empty($weights))
                            @foreach($weights as $weight)
                              <option value="{{$weight->id}}" {{ old("weight_id") == $weight->id || $truck->weight_id == $weight->id? 'selected' : '' }}>{{$weight->weight}}</option>

                            @endforeach

                        @endif
                    </select>
                       @if($errors->has("weight_id"))
                        <span class="help-block">{{ $errors->first("weight_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('model_type_id')) has-error @endif">
                       <label for="model_type_id-field">Model Type</label>
                     <select id="model_type_id-field" name="model_type_id" class="form-control" >
                        @if(! empty($model_types))
                            @foreach($model_types as $model)
                              <option value="{{$model->id}}" {{ old("model_type_id") == $model->id || $truck->model_type_id == $model->id? 'selected' : '' }}>{{$type->name}}</option>

                            @endforeach

                        @endif
                    </select>
                       @if($errors->has("model_type_id"))
                        <span class="help-block">{{ $errors->first("model_type_id") }}</span>
                       @endif
                    </div>
                   
                  
                     <div class="form-group col-md-6 col-sm-6 @if($errors->has('driver_id')) has-error @endif">
                       <label for="driver_id-field">Driver</label>
                        <select id="driver_id-field" name="driver_id" class="form-control" >
                    
                        @if(! empty($drivers))
                            @foreach($drivers as $driver)
                              <option value="{{$driver->id}}" {{ old("driver_id") == $driver->id || $truck->driver_id == $driver->id ? 'selected' : '' }}>{{$driver->name}} - {{$driver->mobile}}</option>

                            @endforeach

                        @endif
                    </select>
                       @if($errors->has("driver_id"))
                        <span class="help-block">{{ $errors->first("driver_id") }}</span>
                       @endif
                    </div>


                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('size_id')) has-error @endif">
                       <label for="size_id-field">Size</label>
                   
                    <select id="size_id-field" name="size_id" class="form-control" >
                        @if(! empty($sizes))
                            @foreach($sizes as $size)
                              <option value="{{$size->id}}" {{ old("size_id") == $size->id || $truck->size_id == $size->id ? 'selected' : '' }}>{{$size->size}}</option>

                            @endforeach

                        @endif
                    </select>
                       @if($errors->has("size_id"))
                        <span class="help-block">{{ $errors->first("size_id") }}</span>
                       @endif
                    </div>
                     <div class="form-group col-md-6 col-sm-6 @if($errors->has('license_start_date')) has-error @endif">
                       <label for="license_start_date_field">License Start Date</label>
                    <input type="text" id="datepicker2" name="license_start_date" value="{{ !empty($truck->license_start_date) ? $truck->license_start_date : "" }}" class="form-control date"  />
                       @if($errors->has("license_start_date"))
                        <span class="help-block">{{ $errors->first("license_start_date") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('license_end_date')) has-error @endif">
                       <label for="license_end_date_field">License End Date</label>
                    <input type="text" id="datepicker1" name="license_end_date" class="form-control date"  value="{{ !empty($truck->license_end_date) ? $truck->license_end_date : "" }}"/>
                       @if($errors->has("license_end_date"))
                        <span class="help-block">{{ $errors->first("license_end_date") }}</span>
                       @endif
                    </div>
                      <div class="form-group col-md-6 col-sm-6 @if($errors->has('image')) has-error @endif">
                            <div id="image_upload">
                            @if($truck->image)
                                <input type="hidden" value="{{ $truck->image }}" name="image_old"
                                       id="image_old">
                                <img src="{{ asset('public/backend/images/'.$truck->image) }}"
                                     style="width:100px;height:100px">
                                <button id="cancel" class="btn btn-danger">&times;</button>
                             @endif
                            </div>
                            <label for="image-field">Vehicle Image</label>
                            <input required class="uploadPhoto btn btn-primary" type="file"
                                   name="image" id="image">
                            @if($errors->has("image"))
                                <span class="help-block">{{ $errors->first("image") }}</span>
                            @endif
                        </div>
                        <div class="form-group col-md-6 col-sm-6 @if($errors->has('license_image_front')) has-error @endif">
                            <div id="license_image_front_upload">
                                 @if($truck->license_image_front)
                                <input type="hidden" value="{{ $truck->license_image_front }}" name="license_image_front_old"
                                       id="license_image_front_old">
                                <img src="{{ asset('public/backend/images/'.$truck->license_image_front) }}"
                                     style="width:100px;height:100px">
                                <button id="cancel" class="btn btn-danger">&times;</button>
                             @endif
                            </div>
                            <label for="image-field">License Image Front</label>
                            <input required class="uploadPhoto btn btn-primary" type="file"
                                   name="license_image_front" id="license_image_front">
                            @if($errors->has("license_image_front"))
                                <span class="help-block">{{ $errors->first("license_image_front") }}</span>
                            @endif
                        </div>
                        <div class="form-group col-md-6 col-sm-6 @if($errors->has('license_image_back')) has-error @endif">
                            <div id="license_image_back_upload">
                                 @if($truck->license_image_back)
                                <input type="hidden" value="{{ $truck->license_image_back }}" name="license_image_back_old"
                                       id="license_image_back_old">
                                <img src="{{ asset('public/backend/images/'.$truck->license_image_back) }}"
                                     style="width:100px;height:100px">
                                <button id="cancel" class="btn btn-danger">&times;</button>
                             @endif
                            </div>
                            <label for="license_image_back">License Image Back</label>
                            <input required class="uploadPhoto btn btn-primary" type="file"
                                   name="license_image_back" id="license_image_back">
                            @if($errors->has("license_image_back"))
                                <span class="help-block">{{ $errors->first("license_image_back") }}</span>
                            @endif
                        </div>
                <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAgent.trucks.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function () {
            $("form[name='truck_create']").validate({
                // Specify validation rules
                rules: {
                    model_name: {
                        required: true
                    },
                    plate_number: {
                        required: true
                    },
                    license_start_date: {
                        required: true,
                        date:true
                    },
                    license_end_date: {
                        required: true,
                        date:true
                    },
                    chassi_number: {
                        required: true
                    },
                    type_id: {
                        required: true
                    },
                    year: {
                        required: true
                    },
                    color_id: {
                        required: true
                    },
                    weight_id: {
                        required: true
                    },
                    model_type_id: {
                        date:true
                    },
                    driver_id: {
                        required: true
                    },
                    size_id: {
                        required: true
                    }
                },
                // Specify validation error messages
                messages: {
                    password: {
                        required: "Please Enter Driver Password",
                        minlength: "Password must be more than 5 character"
                    },
                    model_name: {
                        required: "Please Enter Model Name"
                    },
                    plate_number: {
                        required: "Please Enter Plate Number"
                    },
                    chassi_number: {
                        required: "Please Enter Chassi Number"
                    },
                    type_id: {
                        required: "Please Enter Type"
                    },
                    year: {
                        required: "Please Enter Year"
                    },
                    color_id: {
                        required: "Please Enter Color"
                    },
                    weight_id: {
                        required: "Please Enter Weight"
                    },
                    model_type_id: {
                        required: "Please Enter Model Type"
                    },
                    driver_id: {
                        required: "Please Enter Driver"
                    },
                    size_id: {
                        required: "Please Enter Size"
                    }
                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                },

                highlight: function (element) {
                    $(element).parent().addClass('error')
                },
                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {

                    form.submit();

                }
            });
        })
    </script>
@endsection
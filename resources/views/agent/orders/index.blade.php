@extends('agent.layouts.app')
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<style>
    .jconfirm.jconfirm-modern .jconfirm-box div.jconfirm-title-c .jconfirm-icon-c {
    -webkit-transition: -webkit-transform .5s;
    transition: -webkit-transform .5s;
    transition: transform .5s;
    transition: transform .5s,-webkit-transform .5s;
    -webkit-transform: scale(0);
    transform: scale(0);
    display: block;
    margin-right: 0;
    margin-left: 0;
    margin-bottom: 10px;
    font-size: 69px;
    color: #9e1717;
}
</style>
@endsection
@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> Orders <span style="color: #5810c3;" >{{! empty($driver) ? ' of Driver:: '.$driver->name.' - '.$driver->mobile : ''}} </span>

        </h3>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            @if(count($orders) > 0)
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>RECIEVER NAME</th>
                            <th>RECIEVER MOBILE</th>
                            <th>STATUS</th>
                            <th>COST</th>
                            <th>CUSTOMER</th>
                            <th>PAYMENT</th>
                            <th class="text-right"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td>{{$order->id}}</td>
                                <td>{{$order->reciever_name}}</td>
                                <td>{{$order->reciever_mobile}}</td>
                                <td>{!! ! empty($order->accepted )  ? $order->accepted->status_span : '' !!}</td>
                               <td>{{! empty($order->accepted )  ? $order->accepted->total_price : ''}}</td>
                                <td>{{ ! empty($order->customer) ? $order->customer->name.' - '.$order->customer->mobile  : '' }}</td>
                                <td>{{! empty($order->payment_method) ?  $order->payment_method->name : ''}}</td>

                                <td class="text-right">
                                @if(! empty($order->agent_review))
                                	@if($order->agent_review->status == 1)
                                    <a class="btn btn-xs btn-primary" href="{{ route('mngrAgent.orders.show', $order->id) }}"><i class="glyphicon glyphicon-eye-open"></i>View</a>
                                    @else
                                    	{!! $order->agent_review->status_span !!}
                                    @endif
                                @else
                                   <a class="btn btn-xs btn-warning ask_view" data-id="{{$order->id}}" >Ask View</a>
                                @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $orders->render() !!}
            @else
                <h3 class="text-center alert alert-warning">No Result Found !</h3>
            @endif

        </div>
    </div>

@endsection

@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<script>
$(function(){
	$(".ask_view").on('click',function(){
		var id = $(this).data('id');
		$.ajax({
    			url:'{!! url("/mngrAgent/ask_review_order/") !!}'+'/'+id ,
    			type:'get',

		    }).done(function(data) {
		        if(data == "ok")
		        {
			     $.alert({
                  title: 'Request Review Sent!',
                  icon: 'fa fa-smile-o',
                  theme: 'modern',
                  content: 'Your request sent to Ufelix Admin successfully!',
                  buttons: {
                      info: {
                          text: 'OK',
                          btnClass: 'btn-success',
                          action: function(){
                              window.location.href=window.location.href;
                          }
                      }
                  }
              });
	           }
		    });
	});

});

</script>

@endsection

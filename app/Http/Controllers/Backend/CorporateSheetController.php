<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use App\Models\Order;
use App\Models\User;
use Auth;
use Config;
use ExcelReport;
use Flash;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use PdfReport;

//use Elibyy\TCPDF\Facades\TCPDF;
class CorporateSheetController extends Controller
{
    private $guardType;

    public function __construct(Request $request)
    {
        if ($request->is('mngrAdmin/*') || $request->is('admin/*')) {
            Config::set('auth.defaults.guard', 'admin');

        } elseif ($request->is('mngrAgent/*') || $request->is('agent/*')) {
            Config::set('auth.defaults.guard', 'agent');
        }
        $this->guardType = Config::get('auth.defaults.guard');
    }

    public function index(Request $request)
    {

        Excel::create('corporates_' . date('Y_m_d'), function ($excel) use ($request) {

            $type = 1;

            $orders = Order::with(['payment_method', 'to_government', 'to_city', 'comments' => function ($query) {
                $query->orderBy('created_at', 'DESC');
            }, 'customer' => function ($query) {
                $query->with('Corporate');
            }])
                ->join('customers', 'customers.id', '=', 'orders.customer_id')
                ->join('corporates', 'corporates.id', '=', 'customers.corporate_id')
                ->select('orders.*')
                ->withCount('comments')
                ->orderBy('orders.created_at');

            if ($request->filled('user_id')) {
                $orders = $orders->where('responsible_id', $request->user_id);
            }

            if ($request->filled('corporate_id')) {
                $orders = $orders->where('corporate_id', $request->corporate_id);
            }

            if ($request->filled('from_date')) {
                $orders = $orders->whereDate('orders.created_at', '>=', $request->from_date);
            }

            if ($request->filled('to_date')) {
                $orders = $orders->whereDate('orders.created_at', '<=', $request->to_date);
            }

            if ($request->filled('status') && is_array($request->status)) {
                $orders = $orders->whereIn('orders.status', $request->status);
            }

            $excel->setTitle('Orders');

            $excel->sheet('Orders', function ($sheet) use ($orders, $type) {
                $sheet->loadView('backend.reports.orders')->with(['orders' => $orders, 'type' => $type]);
            });

        })->download('xls');
    }

    public function show()
    {
        $user = Auth::guard($this->guardType)->user();

//        $corporates = Corporate::select('corporates.*')->orderBy('corporates.id', 'desc');

        $users = User::where('role_id', 1)->select('users.*')
            ->orderBy('users.name', 'ASC');

//        $corporates = Corporate::where('is_active', 1)->select('corporates.*')
//            ->orderBy('corporates.name', 'ASC');

        if ($user->is_warehouse_user) {

            $users = $users->join('store_user', 'store_user.store_id', '=', 'users.id')
                ->where('user_id', $user->id);

//            $corporates = $corporates->join('customers', 'customers.corporate_id', '=', 'corporates.id')
//                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
//                ->where('user_id', $user->id);

        }

        $users = $users->get();
//        $corporates = $corporates->get();

        return view('backend.reports.corporate_sheets', compact('users'));
    }

}

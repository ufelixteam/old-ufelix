<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\AcceptedOrder;
use App\Models\Agent;
use App\Models\Driver;
use App\Models\Order;
use App\Models\Truck;
use App\Models\User;
use App\Models\Role;
use App\Models\Verification;
use App\Models\AgentGovernment;
use Mail;
use Response;
use Flash;
use File;
use Auth;
use DB;
use App\Models\Country;
use App\Models\Governorate;
use App\Models\City;

class AgentController extends Controller
{

    /* **-**-*-*-*-*-* Dashboard Agent -*-*-*-*-*-*-*-edit by samar *--*-*-*-*-*-*-- */

    public function get_agent_edit_profile()
    {
        $user = Auth::guard('agent')->user();
        $agent = Agent::where('id', $user->agent_id)->first();
        return view('agent.edit-profile', compact('user', 'agent'));
    }

    public function get_agent_profile()
    {
        $user = Auth::guard('agent')->user();
        $agent = Agent::where('id', $user->agent_id)->first();
        return view('agent.profile', compact('user', 'agent'));
    }

    public function dashboard()
    {
        $user = Auth::guard('agent')->user();
        $agent = Agent::where('id', $user->agent_id)->first();
        $drivers = Driver::where('agent_id', $user->agent_id)->count();
        $busy = Driver::where('online', 2)->where('agent_id', $user->agent_id)->count();
        $onlines = Driver::where('online', 1)->where('agent_id', $user->agent_id)->count();
        $offlines = Driver::where('online', 0)->where('agent_id', $user->agent_id)->count();
        $user_info = DB::table('trucks')->
        select('vehicle_type_id', DB::raw('count(*) as total'))->
        groupBy('vehicle_type_id')->
        get();
        $orders = 0;
        $driverIds = Driver::where('agent_id', $user->agent_id)->pluck('id')->toArray();
        if (count($driverIds)) {
            $orderIds = AcceptedOrder::whereIn('driver_id', $driverIds)->pluck('id')->toArray();
            $orders = Order::whereIn('id', $orderIds)->orderBy('id', 'desc')->count();
        }
        $not_actived_drivers = Driver::where('is_active', 0)->where('agent_id', $user->agent_id)->count();
        $actived_drivers = Driver::where('is_active', 1)->where('agent_id', $user->agent_id)->count();
        return view('agent.dashboard', compact('busy', 'onlines', 'offlines', 'orders', 'drivers', 'user', 'agent', 'not_actived_drivers', 'actived_drivers'));
    }

    function check_agent_email()
    {
        $email = \Request::get("email");
        $agent = User::where('email', $email)->first();
        if ($agent) {
            echo 'false';
        } else {
            echo 'true';
        }
    }

    function check_agent_mobile()
    {
        $mobile = \Request::get("mobile");
        $agent = User::where('mobile', $mobile)->first();
        if ($agent) {
            echo 'false';
        } else {
            echo 'true';
        }
    }

    function check_agent_phone()
    {
        $phone = \Request::get("phone");
        $agent = Agent::where('phone', $phone)->first();
        if ($agent) {
            echo 'false';
        } else {
            echo 'true';
        }
    }

    // Get All Agent
    public function index(Request $request)
    {
        // $agents = Agent::orderBy('id', 'desc')->paginate('15');
        // return view('backend.agents.index', compact('agents'));

        $agents = Agent::orderBy('id', 'desc');

        #check if active search by name an mobile
        $active = app('request')->input('active');   // Search By Active
        $search = app('request')->input('search');   // Search

        if (isset($search) && $search != "") {
            $agents = $agents->where('name', 'like', '%' . $search . '%')->orWhere('mobile', 'like', '%' . $search . '%');
        }

        if (isset($active) && $active != -1) {
            $agents = $agents->where('status', $active);
        }

        $agents = $agents->paginate(15);

        if ($request->ajax()) {
            return [
                'view' => view('backend.agents.table', compact('agents'))->render(),
            ];
        }

        return view('backend.agents.index', compact('agents'));
    }

    // Add New Agent Page
    public function create()
    {
        $countries = Country::orderBy('name', 'ASC')->get();
        $governments = Governorate::orderBy('name_en', 'ASC')->get();
        return view('backend.agents.create', compact('countries', 'governments'));
    }

    // Add New Agent
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|string|max:255,name',
            'mobile' => 'required|unique:agents',
            'email' => 'required|email|unique:agents',
            'commercial_record_number' => 'required|unique:agents',
            'country_id' => 'required',
            'government_id' => 'required',
            'city_id' => 'required',
            'password' => 'required|confirmed',
        ]);

        DB::beginTransaction();
        try {
            $agent = new Agent();
            $agent->name = $request->input("name");
            $agent->mobile = $request->input("mobile");
            $agent->email = $request->input("email");
            $agent->phone = $request->input("phone");
            $agent->fax = $request->input("fax");
            $agent->country_id = $request->input("country_id");
            $agent->government_id = $request->input("government_id");
            $agent->city_id = $request->input("city_id");
            $agent->address = $request->input("address");
            $agent->commercial_record_number = $request->input("commercial_record_number");
            $agent->start_date = $request->input("start_date");
            $agent->deal_date = $request->input("deal_date");
            $agent->profit_rate = $request->input("profit_rate");
            $agent->latitude = $request->input("latitude");
            $agent->longitude = $request->input("longitude");
            $agent->status = $request->input("status");
            $agent->bonus_of_delivery = $request->input("bonus_of_delivery");
            $agent->pickup_price = $request->input("pickup_price");
            $agent->bonus_of_pickup_for_one_order = $request->input("bonus_of_pickup_for_one_order");
//            $agent->password = bcrypt($request->input("password"));


            $agent->save();

            $user = new User();
            $user->name = $request->input("name");
            $user->email = $request->input("email");
            $user->agent_id = $agent->id;
            $user->mobile = $request->input("mobile");
            $user->is_active = 1;
            $user->password = bcrypt($request->input("password"));
            $role = Role::where('name', 'agent')->first();
            $user->role_id = $role->id;
            $user->save();

            DB::commit();

            // Function Add Action In Activity Log By Samir
            addActivityLog('Add Agent ' . $agent->name, ' اضافة الوكيل ' . $agent->name, 3); // Helper Function in App\Helpers.php

            Flash::success(__('backend.Agent_saved_successfully'));
            return redirect()->route('mngrAdmin.agents.index');
        } catch (Exception $e) {
            DB::rollback();
        }
    }

    // View One Agent
    public function show($id)
    {
        $agent = Agent::findOrFail($id);
        $drivers = Driver::where('agent_id', $agent->id)->get();
        $ids = AgentGovernment::where('agent_id', $agent->id)->pluck('government_id')->toArray();
        $areas = AgentGovernment::where('agent_id', $agent->id)->get();
        if (count($ids) > 0) {
            $governments = Governorate::orderBy('name_en', 'ASC')->whereNotIn('id', $ids)->get();

        } else {
            $governments = Governorate::orderBy('name_en', 'ASC')->get();

        }

        return view('backend.agents.show', compact('agent', 'drivers', 'governments', 'areas'));
    }

    // Edit Agent Page
    public function edit($id)
    {
        $agent = Agent::findOrFail($id);
        $countries = Country::orderBy('name', 'ASC')->get();
        $governments = Governorate::orderBy('name_en', 'ASC')->get();
        $cities = City::orderBy('id', 'ASC')->get();

        return view('backend.agents.edit', compact('agent', 'countries', 'governments', 'cities'));
    }

    // Update Agent
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'required|string|max:255,name',
            'mobile' => 'required||unique:agents,mobile,' . $id,
            'email' => 'required||email|unique:agents,email,' . $id,
            'commercial_record_number' => 'required',
            'country_id' => 'required',
            'government_id' => 'required',
            'city_id' => 'required',
        ]);

        DB::beginTransaction();
        try {
            $agent = Agent::findOrFail($id);
            $agentEmail = $agent->email;
            $agent->name = $request->input("name");
            $agent->email = $request->input("email");
            $agent->mobile = $request->input("mobile");
            $agent->phone = $request->input("phone");
            $agent->fax = $request->input("fax");
            $agent->country_id = $request->input("country_id");
            $agent->government_id = $request->input("government_id");
            $agent->city_id = $request->input("city_id");
            $agent->address = $request->input("address");
            $agent->commercial_record_number = $request->input("commercial_record_number");
            $agent->start_date = $request->input("start_date");
            $agent->deal_date = $request->input("deal_date");
            $agent->profit_rate = $request->input("profit_rate");
            $agent->latitude = $request->input("latitude");
            $agent->longitude = $request->input("longitude");
            $agent->status = $request->input("status");
            $agent->bonus_of_delivery = $request->input("bonus_of_delivery");
            $agent->pickup_price = $request->input("pickup_price");
            $agent->bonus_of_pickup_for_one_order = $request->input("bonus_of_pickup_for_one_order");

            $agent->save();

            // Function Add Action In Activity Log By Samir
            addActivityLog('Edit Agent ' . $agent->name, ' تعديل الوكيل ' . $agent->name, 3); // Helper Function in App\Helpers.php

            $user = User::where('agent_id', $agent->id)->where('email', $agentEmail)->first();

            if ($user == null) {
                $user = new User();
                $user->agent_id = $agent->id;
                $role = Role::where('name', 'agent')->first();
                $user->role_id = $role->id;
            }

            $user->name = $request->input("name");
            $user->email = $request->input("email");
            $user->mobile = $request->input("mobile");

            if ($request->input("password") != "") {
                $user->password = bcrypt($request->input("password"));
            }

            $user->save();
            DB::commit();

            Flash::warning(__('backend.Agent_Updated_successfully'));
            return redirect()->route('mngrAdmin.agents.index');
        } catch (Exception $e) {
            DB::rollback();
        }
    }

    // Delete Agent
    public function destroy($id)
    {
        try {
            $agent = Agent::findOrFail($id);
            $drivers = Driver::where('agent_id', $agent->id)->get();

            if (count($drivers) > 0) { // If Found Any Driver With This Agent
                flash(__('backend.Agent_does_not_deleted_May_b_have_drivers_and_orders'))->error();
                return redirect()->route('mngrAdmin.agents.index');
            }

            if (!empty($agent)) {
                $user = User::where('agent_id', $agent->id)->delete();
                $agent->delete();

                // Function Add Action In Activity Log By Samir
                addActivityLog('Delete Agent ' . $agent->name, ' حذف الوكيل ' . $agent->name, 3); // Helper Function in App\Helpers.php

                DB::commit();
                flash(__('backend.Agent_deleted_successfully'))->error();
                return redirect()->route('mngrAdmin.agents.index');
            } else {
                flash(__('backend.Agent_does_not_exist'))->error();
                return redirect()->route('mngrAdmin.agents.index');
            }
        } catch (Exception $e) {
            DB::rollback();
        }
    }

    // Activeed And Not Activeed Agent
    public function active($id)
    {
        /*
        * status = 0 :: Not Active
        * status = 1 :: Active
        */
        $agent = Agent::findOrFail($id);
        if (!empty($agent)) {
            if ($agent->status == 0) {
                $agent->status = 1;
                // Function Add Action In Activity Log By Samir
                addActivityLog('Active Agent ' . $agent->name, ' تفعيل الوكيل ' . $agent->name, 3); // Helper Function in App\Helpers.php
            } else {
                $agent->status = 0;
                // Function Add Action In Activity Log By Samir
                addActivityLog('Unactive Agent ' . $agent->name, ' الغاء تفعيل الوكيل ' . $agent->name, 3); // Helper Function in App\Helpers.php
            }
            $agent->save();
            return $agent->status;
        }
    }

    public function add_area(Request $request)
    {

        $agent = Agent::find($request->agent_id);
        $area = Governorate::find($request->government_id);
        if ($agent && $area) {
            //
            $area = new AgentGovernment();
            $area->agent_id = $request->agent_id;
            $area->government_id = $request->government_id;
            $area->save();

            Flash::success(__('backend.Agent_Area_saved_successfully'));
            return redirect('mngrAdmin/agents/' . $request->agent_id);


        } else {
            flash(__('backend.SomeThing_Error'))->error();
            return redirect()->route('mngrAdmin.agents.index');
        }

    }

    //
    public function delete_area(Request $request)
    {

        $area = AgentGovernment::where('agent_id', $request->agent_id)->where('government_id', $request->government_id)->first();
        if (!empty($area)) {

            $area->delete();
            Flash::success(__('backend.delete_Agent_Area_saved_successfully'));
            return 'true';


        } else {
            flash(__('backend.SomeThing_Error'))->error();
            return 'false';
        }
    }/*delete_order_pickup*/

}

<div class="modal" tabindex="-1" role="dialog" id="dropModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{__('backend.drop_off')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-12 col-xs-12 col-sm-12">
                        <label class="control-label"
                               for="reason"> {{__('backend.comment')}}:</label>
                        <textarea rows="4" id="drop_comment" name="drop_comment" style="resize: vertical"
                                  class="form-control"></textarea>
                    </div>
                    <div class="form-group col-md-12 col-xs-12 col-sm-12">
                        <label class="control-label"
                               for="captain_received_date"> {{__('backend.task_type')}}:</label>

                        <div class="form-group">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="confirm_pickup" value="1"
                                       name="confirm_pickup">
                                <label class="form-check-label"
                                       for="confirm_pickup">{{__('backend.pickup')}}</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-control" type="input" value="" name="pickup_orders"
                                       id="pickup_orders"
                                       placeholder="{{__('backend.orders')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="confirm_cash_collect" value="1"
                                       name="confirm_cash_collect">
                                <label class="form-check-label"
                                       for="confirm_cash_collect">{{__('backend.cache_collect')}}</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-control" type="input" value="" name="cash_money" id="cash_money"
                                       placeholder="{{__('backend.money')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="confirm_recall_orders" value="1"
                                       name="confirm_recall_orders">
                                <label class="form-check-label"
                                       for="confirm_recall_orders">{{__('backend.recall_orders')}}</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-control" type="input" value="" name="recall_orders"
                                       id="recallOrders"
                                       placeholder="{{__('backend.orders')}}">
                            </div>
                        </div>
                        <label class="control-label"
                               for="captain_received_date"> {{__('backend.refund_collections')}}:</label>
                        <button type="button" class="btn btn-success" id="add_refund_collection">
                            {{__('backend.add')}}
                        </button>
                        <div class="form-group" id="refunWrapper">
                            <div class="form-check form-check-inline">
                                <input class="form-control refund_collections" type="text" value="" name="refund_collections[]">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-success" id="drop_button">
                    {{__('backend.save')}}
                </button>
            </div>
        </div>
    </div>
</div>

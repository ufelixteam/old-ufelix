@extends('backend.layouts.app')

@section('css')
    <title>{{__('backend.notifications')}} - {{__('backend.view')}}</title>
@endsection

@section('header')
    <div class="page-header">
        <h3>{{__('backend.show_notification')}} #{{$notification->id}}</h3>
        <form action="{{ route('mngrAdmin.notifications.destroy', $notification->id) }}" method="POST"
              style="display: inline;"
              onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
            @if(permission('editNotification')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <a class="btn btn-warning btn-group" role="group"
                   href="{{ route('mngrAdmin.notifications.edit', $notification->id) }}"><i
                        class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
            @endif

            @if(permission('deleteNotification')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <button type="submit" class="btn btn-danger"> {{__('backend.delete')}} <i
                        class="glyphicon glyphicon-trash"></i></button>
                @endif
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="form-group{{ $errors->has('type') ? ' is-invalid' : '' }} col-md-4 col-sm-6 col-xs-6">
                <label class="control-label" for="type">{{__('backend.Notification_Type')}}</label>
                <select id="type" name="type" class="form-control">
                    <option value="0">{{__('backend.Select_type')}}</option>
                    @foreach(DB::table('notification_type')->get() as $value)
                        <option
                            {{$notification->type ==  $value->id ? 'selected' : ''}} value="{{ $value->id }}">{{ $value->title }}</option>
                    @endforeach
                </select>
                @if($errors->has("type"))
                    <span class="help-block">{{ $errors->first("type") }}</span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('client_model') ? ' is-invalid' : '' }} col-md-4 col-sm-6 col-xs-6">
                <label class="control-label" for="client_model">{{__('backend.Send_to')}}</label>
                <select id="client_model" name="client_model" class="form-control">
                    <option value="0">{{__('backend.Select_Sender_type')}}</option>
                    <option
                        {{$notification->client_model ==  5 ? 'selected' : ''}} value="5">{{__('backend.all')}}</option>
                    <option
                        {{$notification->client_model ==  1 ? 'selected' : ''}} value="1">{{__('backend.captains')}}</option>
                    {{--                    <option {{$notification->client_model ==  2 ? 'selected' : ''}} value="2">{{__('backend.agents')}}</option>--}}
{{--                    <option--}}
{{--                        {{$notification->client_model ==  3 ? 'selected' : ''}} value="3">{{__('backend.individuals')}}</option>--}}
                    <option
                        {{$notification->client_model ==  4 ? 'selected' : ''}} value="4">{{__('backend.corporates')}}</option>
                </select>
                @if($errors->has("client_model"))
                    <span class="help-block">{{ $errors->first("client_model") }}</span>
                @endif
            </div>


            <div
                class="captains form-group{{ $errors->has('captains') ? ' is-invalid' : '' }} col-md-4 col-sm-6 col-xs-6"
                @if($notification->client_model ==  1) @else style="display:none;" @endif>
                <label class="control-label" for="captains">{{__('backend.captains')}}</label>
                <select name="client_id" class="form-control">
                    <option value="0">{{__('backend.all')}}</option>
                    @foreach(App\Models\Driver::where('is_active', 1)->get() as $value)
                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                    @endforeach
                </select>
                @if($errors->has("captains"))
                    <span class="help-block">{{ $errors->first("captains") }}</span>
                @endif
            </div>

            {{--            <div class="agents form-group{{ $errors->has('agents') ? ' is-invalid' : '' }} col-md-4 col-sm-6 col-xs-6" @if($notification->client_model ==  2) @else style="display:none;" @endif>--}}
            {{--                <label class="control-label" for="agents">{{__('backend.agents')}}</label>--}}
            {{--                <select name="client_id" class="form-control">--}}
            {{--                    <option value="0">{{__('backend.all')}}</option>--}}
            {{--                    @foreach(App\Models\Agent::where('status', 1)->get() as $value)--}}
            {{--                        <option value="{{ $value->id }}">{{ $value->name }}</option>--}}
            {{--                    @endforeach--}}
            {{--                </select>--}}
            {{--                @if($errors->has("agents"))--}}
            {{--                    <span class="help-block">{{ $errors->first("agents") }}</span>--}}
            {{--                @endif--}}
            {{--            </div>--}}


{{--            <div--}}
{{--                class="individuals form-group{{ $errors->has('individuals') ? ' is-invalid' : '' }} col-md-4 col-sm-6 col-xs-6"--}}
{{--                @if($notification->client_model ==  3) @else style="display:none;" @endif>--}}
{{--                <label class="control-label" for="individuals">{{__('backend.individuals')}}</label>--}}
{{--                <select name="client_id" class="form-control">--}}
{{--                    <option value="0">{{__('backend.all')}}</option>--}}
{{--                    @foreach(App\Models\Customer::where('status', 1)->get() as $value)--}}
{{--                        <option value="{{ $value->id }}">{{ $value->name }}</option>--}}
{{--                    @endforeach--}}
{{--                </select>--}}
{{--                @if($errors->has("individuals"))--}}
{{--                    <span class="help-block">{{ $errors->first("individuals") }}</span>--}}
{{--                @endif--}}
{{--            </div>--}}


            <div
                class="corporate form-group{{ $errors->has('corporate') ? ' is-invalid' : '' }} col-md-4 col-sm-6 col-xs-6"
                @if($notification->client_model ==  4) @else style="display:none;" @endif>
                <label class="control-label" for="corporate">{{__('backend.corporates')}}</label>
                <select name="client_id" class="form-control">
                    <option value="0">{{__('backend.all')}}</option>
                    @foreach(App\Models\Corporate::all() as $value)
                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                    @endforeach
                </select>
                @if($errors->has("corporate"))
                    <span class="help-block">{{ $errors->first("corporate") }}</span>
                @endif
            </div>

            <div class="form-group col-md-4 col-sm-6 col-xs-6 @if($errors->has('devices')) has-error @endif">
                <label for="title-field">{{__('backend.devices')}}</label>
                <select id="devices" name="devices" class="form-control">
                    <option value="0">{{__('backend.Select_device')}}</option>
                    <option
                        {{$notification->device_type ==  4 ? 'selected' : ''}} value="4">{{__('backend.all')}}</option>
                    <option
                        {{$notification->device_type ==  1 ? 'selected' : ''}} value="1">{{__('backend.andriod')}}</option>
                    <option
                        {{$notification->device_type ==  2 ? 'selected' : ''}} value="2">{{__('backend.ios')}}</option>
                    <option
                        {{$notification->device_type ==  3 ? 'selected' : ''}} value="3">{{__('backend.browser')}}</option>
                </select>
                @if($errors->has("devices"))
                    <span class="help-block">{{ $errors->first("devices") }}</span>
                @endif
            </div>

            <div class="form-group col-md-4 col-sm-6 col-xs-6 @if($errors->has('date')) has-error @endif">
                <label for="title-field">{{__('backend.date')}}</label>
                <input type="date" id="title-field" name="date" class="form-control" value="{{ $notification->date }}"/>
                @if($errors->has("date"))
                    <span class="help-block">{{ $errors->first("date") }}</span>
                @endif
            </div>
            <div class="form-group col-md-4 col-sm-6 col-xs-6 @if($errors->has('title')) has-error @endif">
                <label for="title-field">{{__('backend.title')}}</label>
                <input type="text" id="title-field" name="title" class="form-control"
                       value="{{$notification->title }}"/>
                @if($errors->has("title"))
                    <span class="help-block">{{ $errors->first("title") }}</span>
                @endif
            </div>

            <div class="form-group col-md-12 col-sm-12 col-xs-12 @if($errors->has('message')) has-error @endif">
                <label for="message-field">{{__('backend.message')}}</label>
                <textarea class="form-control summernote" name="message"
                          required>{{ $notification->message }}</textarea>
                @if($errors->has("message"))
                    <span class="help-block">{{ $errors->first("message") }}</span>
                @endif
            </div>

            {{--<div class="form-group">--}}
            {{--<label for="nome">#</label>--}}
            {{--<p class="form-control-static"></p>--}}
            {{--</div>--}}
            {{--<div class="form-group">--}}
            {{--<label for="title">TITLE</label>--}}
            {{--<p class="form-control-static">{{$notification->title}}</p>--}}
            {{--</div>--}}
            {{--<div class="form-group">--}}
            {{--<label for="message">MESSAGE</label>--}}
            {{--<p class="form-control-static">{{$notification->subject}}</p>--}}
            {{--</div>--}}
            {{--<div class="form-group">--}}
            {{--<label for="user_id">USER_ID</label>--}}
            {{--<p class="form-control-static">@if(isset($notification->client_id)) {{ App\Models\User::findOrFail($notification->client_id)->name }} @endif</p>--}}
            {{--</div>--}}
            {{--<div class="form-group">--}}
            {{--<label for="object_id">OBJECT_ID</label>--}}
            {{--<p class="form-control-static">{{$notification->object_id}}</p>--}}
            {{--</div>--}}
            {{--<div class="form-group">--}}
            {{--<label for="type">TYPE</label>--}}
            {{--<p class="form-control-static">{{$notification->type}}</p>--}}
            {{--</div>--}}
            {{--<div class="form-group">--}}
            {{--<label for="is_read">IS_READ</label>--}}
            {{--<p class="form-control-static">{{$notification->is_read == 1 ? 'true' : 'false' }}</p>--}}
            {{--</div>--}}
            {{----}}
            <div class="col-md-12 col-xs-12 col-sm-12">
                <a class="btn btn-link" href="{{ route('mngrAdmin.notifications.index') }}"><i
                        class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
            </div>

        </div>
    </div>

@endsection

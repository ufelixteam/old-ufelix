@extends('agent.layouts.app')
@section('header')
    <div class="page-header">
        <h3>Show #{{$truck->id}}</h3><br>{!! $truck->status_span !!}
        <form action="{{ route('mngrAdmin.trucks.destroy', $truck->id) }}" method="POST" style="display: inline;"
              onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <!--<a class="btn btn-warning btn-group" role="group"-->
                   <!--href="{{ route('mngrAdmin.trucks.edit', $truck->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>-->
                <!--<button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>-->
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="form-group col-sm-12">
            <h3 style="color: #dc931d">Vehicle Information</h3>
                <hr>
            </div>
            <div class="form-group col-sm-6">
                <label for="model_name">MODEL NAME</label>
                <p class="form-control-static">{{ ! empty($truck->model_name)?$truck->model_name:''}}</p>
            </div>
            <div class="form-group col-sm-6">
                <label for="plate_number">PLATE NUMBER</label>
                <p class="form-control-static">{{ ! empty($truck->plate_number)?$truck->plate_number:''}}</p>
            </div>
            <div class="form-group col-sm-6">
                <label for="chassi_number">CHASSI NUMBER</label>
                <p class="form-control-static">{{ ! empty($truck->chassi_number)?$truck->chassi_number:''}}</p>
            </div>
            <div class="form-group col-sm-6">
                <label for="year">YEAR</label>
                <p class="form-control-static">{{ ! empty($truck->year)?$truck->year:''}}</p>
            </div>

            <div class="form-group col-sm-6">
                <label for="type_id">TYPE</label>
                <p class="form-control-static">{{ ! empty($truck->Type->name)?$truck->Type->name:''}}</p>
            </div>
            <div class="form-group col-sm-6">
                <label for="color_id">COLOR</label>
                <p class="form-control-static">{{ ! empty($truck->Color->name)?$truck->Color->name:''}}</p>
            </div>
            <div class="form-group col-sm-6">
                <label for="model_type_id">MODEL TYPE</label>
                <p class="form-control-static">{{ ! empty($truck->Model_Type->name)?$truck->Model_Type->name:'' }}</p>
            </div>

            <div class="form-group col-sm-6">
                <label for="agent_id">AGENT</label>
                <p class="form-control-static">{{ ! empty($truck->agent->name)?$truck->agent->name:''}}</p>
            </div>
            <div class="form-group col-sm-6">
                <label for="size_id">License Start Date</label>
                <p class="form-control-static">{{ ! empty($truck->license_start_date)?$truck->license_start_date:''}}</p>
            </div>
            <div class="form-group col-sm-6">
                <label for="size_id">License End Date</label>
                <p class="form-control-static">{{ ! empty($truck->license_end_date)?$truck->license_end_date:''}}</p>
            </div>
            <div class="form-group col-sm-6">
                <label for="driving_licence_expired_date">Image</label>
                <br>
          @if($truck->image)
            <a data-toggle="lightbox" href="#" >
              <img style="height: 100px;width:100px;"
                        src="{{ asset('public/backend/images/'.$truck->image) }}"></a>
      			<div id="demoLightbox" class="lightbox fade"  tabindex="-1" role="dialog" aria-hidden="true">
         			 <div class='lightbox-dialog'>
             			 <div class='lightbox-content'>
           		       <img src="{{ asset('public/backend/images/'.$truck->image) }}">
              		</div>
          		</div>
        		</div>
          @endif
            </div>
            <div class="form-group col-sm-6">
                <label for="driving_licence_expired_date">License Image Front</label>
                <br>
                @if ($truck->license_image_front)
                <a data-toggle="lightbox" href="#" > <img
                        style="height: 100px;width:100px;"
                        src="{{ asset('public/backend/images/'.$truck->license_image_front) }}"></a>
			<div id="demoLightbox1" class="lightbox fade"  tabindex="-1" role="dialog" aria-hidden="true">
   			 <div class='lightbox-dialog'>
       			 <div class='lightbox-content'>
     		       <img src="{{ asset('public/backend/images/'.$truck->license_image_front) }}">
        		</div>
    		</div>
  		</div>
          @endif
            </div>
            <div class="form-group col-sm-6">
                <label for="driving_licence_expired_date">License Image Back</label>
                <br>
                @if ($truck->license_image_back)
                <a data-toggle="lightbox" href="#" > <img
                        style="height: 100px;width:100px;"
                        src="{{ asset('public/backend/images/'.$truck->license_image_back) }}"></a>
			<div id="demoLightbox5" class="lightbox fade"  tabindex="-1" role="dialog" aria-hidden="true">
   			 <div class='lightbox-dialog'>
       			 <div class='lightbox-content'>
     		       <img src="{{ asset('public/backend/images/'.$truck->license_image_back) }}">
        		</div>
    		</div>
  		</div>
           @endif
            </div>


        </div>
        <div>
            <div class="form-group col-sm-12">
            <h3 style="color: #dc931d">Driver Information</h3>
                <hr>
            </div>
            @if(! empty($truck->Driver))
            <div class="form-group col-sm-6">
                <label for="driver_id">NAME</label>
                <p class="form-control-static">{{ ! empty($truck->Driver->name)?$truck->Driver->name:''}}</p>
            </div>
            <div class="form-group col-sm-6">
                <label for="driver_id">EMAIL</label>
                <p class="form-control-static">{{  ! empty($truck->Driver->email)? $truck->Driver->email:'' }}</p>
            </div>
            <div class="form-group col-sm-6">
                <label for="driver_id">Mobile</label>
                <p class="form-control-static">{{ ! empty($truck->Driver->mobile)?$truck->Driver->mobile:''}}</p>
            </div>
            <div class="form-group col-sm-6">
                <label for="driver_id">Phone</label>
                <p class="form-control-static">{{ ! empty($truck->Driver->phone)?$truck->Driver->phone:''}}</p>
            </div>
            <div class="form-group col-sm-6">
                <label for="driver_id">DRIVING LICENCE EXPIRED DATE</label>
                <p class="form-control-static">{{ ! empty($truck->Driver->driving_licence_expired_date)?$truck->Driver->driving_licence_expired_date:''}}</p>
            </div>
            <div class="form-group col-sm-6">
                <label for="driver_id">ADDRESS</label>
                <p class="form-control-static">{{ ! empty($truck->Driver->address)?$truck->Driver->address:''}}</p>
            </div>
            <div class="form-group col-sm-6">
                <label for="driver_id">NATIONAL ID EXPIRE DATE</label>
                <p class="form-control-static">{{ ! empty($truck->Driver->national_id_expired_date)?$truck->Driver->national_id_expired_date:''}}</p>
            </div>
        </div>

        @else

        <h4 class="text-center">This Truck doe not assigned to driver</h4>

        @endif
        <div class="col-md-12 col-xs-12 col-sm-12">
            <a class="btn btn-link" href="{{ route('mngrAdmin.trucks.index') }}"><i
                        class="glyphicon glyphicon-backward"></i> Back</a>
        </div>

    </div>

@endsection

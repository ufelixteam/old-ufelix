<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDelay extends Model
{

    protected $table = 'order_delay';

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'created_by');
    }
}

@extends('agent.layouts.app')
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<style>
    .jconfirm.jconfirm-modern .jconfirm-box div.jconfirm-title-c .jconfirm-icon-c {
    -webkit-transition: -webkit-transform .5s;
    transition: -webkit-transform .5s;
    transition: transform .5s;
    transition: transform .5s,-webkit-transform .5s;
    -webkit-transform: scale(0);
    transform: scale(0);
    display: block;
    margin-right: 0;
    margin-left: 0;
    margin-bottom: 10px;
    font-size: 69px;
    color: #9e1717;
}
.tab-content
{
  padding-top: 20px;
}
.nav-tabs li:first-child.active a
{
  color: #fff;background-color: #337ab7;
}
.nav-tabs li:last-child.active a
{
  color: #fff;background-color: #dd4b39;
}
</style>
@endsection
@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> Pilot Orders <span style="color: #5810c3;" >{{! empty($driver) ? ' of Driver:: '.$driver->name.' - '.$driver->mobile : ''}} </span>
        </h3>
    </div>
@endsection

@section('content')

<div>
  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
      <div class="row">
          <div class="col-md-12 col-xs-12 col-sm-12">
						<form class="" action="{{ url('mngrAgent/Pilot_update') }}" method="post">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="order_id" value="{{ $order_id }}">
              <input type="hidden" name="delv_id" value="{{ $delv_id }}">
							<input type="hidden" name="acc_id" value="{{ $acc_id }}">
							<div class="form-group col-md-6 col-sm-6 col-sm-offset-2">
									 <select id="items-field" name="items" id="items" class="form-control">
												@foreach($drivers as $driver)
														<option value="{{$driver->id}}"
														>{{$driver->name}}</option>
												@endforeach
										</select>
								</div>
								<input type="submit" class="btn btn-success" value="Success">
						</form>
					</div>
      </div>
      <!-- end -->
    </div>
@endsection

@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<script>
$('#myTabs a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})
</script>

@endsection

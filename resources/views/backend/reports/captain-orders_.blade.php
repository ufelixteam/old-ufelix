<style>
    td, th {
        text-align: center;
        vertical-align: middle;
    }

</style>
<table>
    <tr>
        <th>No</th>
        <th>
            ID
        </th>
        <th>
            CREATE DATE
        </th>
        <th>
            ACCEPTED DATE
        </th>
        <th>
            RECEIVED DATE
        </th>
        <th>
            LAST STATUS DATE
        </th>
        <th>
            STATUS
        </th>
        <th>
            WAREHOUSE
        </th>
        <th>
            COLLECTED COST
        </th>
        <th>
            Corporate
        </th>
        <th>
            Customer
        </th>
        <th>
            CODE -1
        </th>
        <th>
            CODE -2
        </th>
        <th>
            RECEIVER
        </th>
        <th>
            ADDRESS
        </th>
        <th>
            PHONE
        </th>
        <th>
            GOV
        </th>
        <th>
            CITY
        </th>
        <th>
            NOTES
        </th>
        <th>
            TYPE
        </th>
        <th>
            TOTAL
        </th>
        <th>
            CLIENT
        </th>
        <th>
            UFELIX
        </th>
        <th>
            DELAY
        </th>
        <th>
            DATE
        </th>
        <th>
            DELIVERY PROBLEMS
        </th>
        <th>
            DATE
        </th>
    </tr>
    @php
        $i=1;
    @endphp
    @if(! empty($orders))
        @foreach($orders->cursor() as $order)
            @if((count($order->delivery_problems) >= count($order->order_delay)) && count($order->delivery_problems) > 0)
                @foreach($order->delivery_problems as $j => $problem)
                    @if($loop->first)
                        <tr>
                            <td>{{$i}}</td>
                            <td>
                                {{$order->id}}
                            </td>
                            <td>
                                {{$order->created_at}}
                            </td>
                            <td>
                                {{$order->accepted_at}}
                            </td>
                            <td>
                                {{$order->received_at}}
                            </td>
                            <td>
                                {{$order->last_status_date}}
                            </td>
                            <td>
                                {!!$order->status_details!!}
                            </td>
                            <td>
                                @if($order->client_dropoff == 1)
                                    Client
                                @elseif($order->warehouse_dropoff == 1)
                                    Warehouse
                                @endif
                            </td>
                            <td>
                                {{$order->collected_cost}}
                            </td>
                            <td>
                                {{isset($order->customer->Corporate->name) ? $order->customer->Corporate->name : '-'}}
                            </td>
                            <td>
                                {{isset($order->customer->name) ? $order->customer->name : '-'}}
                            </td>
                            <td>
                                {{$order->order_number}}
                            </td>
                            <td>
                                {{$order->order_no}}
                            </td>
                            <td>
                                {{$order->receiver_name}}
                            </td>
                            <td>
                                {{mb_substr(htmlentities(strip_tags($order->receiver_address)),0,150, "utf-8")}}
                            </td>
                            <td>
                                {{$order->receiver_mobile}}
                            </td>
                            <td>
                                {{isset($order->to_government->name_en) ? $order->to_government->name_en : ''}}
                            </td>
                            <td>
                                {{isset($order->to_city->name_en) ? $order->to_city->name_en : ''}}
                            </td>
                            <td>
                                {{$order->notes}}
                            </td>
                            <td>
                                {{isset($order->types->name_en) ? $order->types->name_en : ''}}
                            </td>
                            <td>
                                {{$order->total_price}}
                            </td>
                            <td>
                                {{$order->total_price - $order->delivery_price}}
                            </td>
                            <td>
                                {{$order->delivery_price}}
                            </td>
                            <td>
                                @if(isset($order->order_delay[$j]))
                                    {{$order->order_delay[$j]->delay_comment}}
                                @endif
                            </td>
                            <td>
                                @if(isset($order->order_delay[$j]))
                                    {{$order->order_delay[$j]->delay_at}}
                                @endif
                            </td>
                            <td>
                                {{mb_substr(($problem->reason ? htmlentities(strip_tags($problem->reason)) : $problem->problem->reason_en),0,150, "utf-8")}}
                            </td>
                            <td>
                                {{$problem->created_at}}
                            </td>
                        </tr>
                    @else
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                @if(isset($order->order_delay[$j]))
                                    {{$order->order_delay[$j]->delay_comment}}
                                @endif
                            </td>
                            <td>
                                @if(isset($order->order_delay[$j]))
                                    {{$order->order_delay[$j]->delay_at}}
                                @endif
                            </td>
                            <td>
                                {{mb_substr(($problem->reason ? htmlentities(strip_tags($problem->reason)) : $problem->problem->reason_en),0,150, "utf-8")}}
                            </td>
                            <td>
                                {{$problem->created_at}}
                            </td>
                        </tr>
                    @endif
                @endforeach
            @elseif(count($order->delivery_problems) < count($order->order_delay))
                @foreach($order->order_delay as $t => $order_delay)
                    @if($loop->first)
                        <tr>
                            <td>{{$i}}</td>
                            <td>
                                {{$order->id}}
                            </td>
                            <td>
                                {{$order->created_at}}
                            </td>
                            <td>
                                {{$order->accepted_at}}
                            </td>
                            <td>
                                {{$order->received_at}}
                            </td>
                            <td>
                                {{$order->last_status_date}}
                            </td>
                            <td>
                                {!!$order->status_details!!}
                            </td>
                            <td>
                                @if($order->client_dropoff == 1)
                                    Client
                                @elseif($order->warehouse_dropoff == 1)
                                    Warehouse
                                @endif
                            </td>
                            <td>
                                {{$order->collected_cost}}
                            </td>
                            <td>
                                {{isset($order->customer->Corporate->name) ? $order->customer->Corporate->name : '-'}}
                            </td>
                            <td>
                                {{isset($order->customer->name) ? $order->customer->name : '-'}}
                            </td>
                            <td>
                                {{$order->order_number}}
                            </td>
                            <td>
                                {{$order->order_no}}
                            </td>
                            <td>
                                {{$order->receiver_name}}
                            </td>
                            <td>
                                {{mb_substr(htmlentities(strip_tags($order->receiver_address)),0,150, "utf-8")}}
                            </td>
                            <td>
                                {{$order->receiver_mobile}}
                            </td>
                            <td>
                                {{isset($order->to_government->name_en) ? $order->to_government->name_en : ''}}
                            </td>
                            <td>
                                {{isset($order->to_city->name_en) ? $order->to_city->name_en : ''}}
                            </td>
                            <td>
                                {{$order->notes}}
                            </td>
                            <td>
                                {{isset($order->types->name_en) ? $order->types->name_en : ''}}
                            </td>
                            <td>
                                {{$order->total_price}}
                            </td>
                            <td>
                                {{$order->total_price - $order->delivery_price}}
                            </td>
                            <td>
                                {{$order->delivery_price}}
                            </td>
                            <td>
                                {{$order_delay->delay_comment ? $order_delay->delay_comment : ''}}
                            </td>
                            <td>
                                {{$order_delay->delay_at}}
                            </td>
                            <td>
                                @if(isset($order->delivery_problems[$t]))
                                    {{mb_substr(($order->delivery_problems[$t]->reason ? htmlentities(strip_tags($order->delivery_problems[$t]->reason)) : $order->delivery_problems[$t]->problem->reason_en),0,150, "utf-8")}}
                                @endif
                            </td>
                            <td>
                                @if(isset($order->delivery_problems[$t]))
                                    {{$order->delivery_problems[$t]->created_at}}
                                @endif
                            </td>
                        </tr>
                    @else
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                {{$order_delay->delay_comment ? $order_delay->delay_comment : ''}}
                            </td>
                            <td>
                                {{$order_delay->delay_at}}
                            </td>
                            <td>
                                @if(isset($order->delivery_problems[$t]))
                                    {{mb_substr(($order->delivery_problems[$t]->reason ? htmlentities(strip_tags($order->delivery_problems[$t]->reason)) : $order->delivery_problems[$t]->problem->reason_en),0,150, "utf-8")}}
                                @endif
                            </td>
                            <td>
                                @if(isset($order->delivery_problems[$t]))
                                    {{$order->delivery_problems[$t]->created_at}}
                                @endif
                            </td>
                        </tr>
                    @endif
                @endforeach
            @else
                <tr>
                    <td>{{$i}}</td>
                    <td>
                        {{$order->id}}
                    </td>
                    <td>
                        {{$order->created_at}}
                    </td>
                    <td>
                        {{$order->accepted_at}}
                    </td>
                    <td>
                        {{$order->received_at}}
                    </td>
                    <td>
                        {{$order->last_status_date}}
                    </td>
                    <td>
                        {!!$order->status_details!!}
                    </td>
                    <td>
                        @if($order->client_dropoff == 1)
                            Client
                        @elseif($order->warehouse_dropoff == 1)
                            Warehouse
                        @endif
                    </td>
                    <td>
                        {{$order->collected_cost}}
                    </td>
                    <td>
                        {{isset($order->customer->Corporate->name) ? $order->customer->Corporate->name : '-'}}
                    </td>
                    <td>
                        {{isset($order->customer->name) ? $order->customer->name : '-'}}
                    </td>
                    <td>
                        {{$order->order_number}}
                    </td>
                    <td>
                        {{$order->order_no}}
                    </td>
                    <td>
                        {{$order->receiver_name}}
                    </td>
                    <td>
                        {{mb_substr(htmlentities(strip_tags($order->receiver_address)),0,150, "utf-8")}}
                    </td>
                    <td>
                        {{$order->receiver_mobile}}
                    </td>
                    <td>
                        {{isset($order->to_government->name_en) ? $order->to_government->name_en : ''}}
                    </td>
                    <td>
                        {{isset($order->to_city->name_en) ? $order->to_city->name_en : ''}}
                    </td>
                    <td>
                        {{$order->notes}}
                    </td>
                    <td>
                        {{isset($order->types->name_en) ? $order->types->name_en : ''}}
                    </td>
                    <td>
                        {{$order->total_price}}
                    </td>
                    <td>
                        {{$order->total_price - $order->delivery_price}}
                    </td>
                    <td>
                        {{$order->delivery_price}}
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
            @endif

            @php
                $i++;
            @endphp
        @endforeach
    @endif
</table>

@extends('backend.layouts.app')

@section('css')
    <title>{{__('backend.customers')}} - {{__('backend.edit')}}</title>
    <link href="{{asset('assets/select2/select2.min.css')}}" rel="stylesheet">
    <style>
        .select2-container {
            width: 100% !important;
            display: block;
        }

        .input-group {
            -moz-box-shadow: none !important;
            -webkit-box-shadow: none !important;
            box-shadow: none !important;
        }

        .input-group {
            width: 100%;
        }

        .pac-container {
            z-index: 9999 !important;
        }

        .form-group {
            min-height: 94px;
            margin-bottom: 0px;
        }

        .imgDiv img {
            height: 100px;
            width: 100px;
            border-radius: 50%;
        }
    </style>
@endsection

@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i> {{__('backend.corporate_users')}}
            / {{__('backend.edit')}} {{__('backend.user')}}
            - {{$customer->name}}</h3>
    </div>
@endsection

@section('content')


    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <form action="{{ route('mngrAdmin.customers.update', $customer->id) }}" method="POST" name="customer_edit"
                  enctype="multipart/form-data">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row" style="margin-bottom: 30px;">
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('name')) has-error @endif">
                        <label for="name-field">{{__('backend.name')}}: </label>
                        <input class="form-control" type="text" name="name"
                               value="{{ !empty($customer->name)? $customer->name :  $customer->name}}">
                        @if($errors->has("name"))
                            <span class="help-block">{{ $errors->first("name") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('email')) has-error @endif">
                        <label for="role-field">{{__('backend.email')}}: </label>
                        <input class="form-control" type="email" name="email"
                               value="{{ !empty($customer->email)? $customer->email :  $customer->email}}">
                        @if($errors->has("email"))
                            <span class="help-block">{{ $errors->first("email") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('mobile')) has-error @endif">
                        <label for="type-field">{{__('backend.mobile')}}: </label>
                        <input class="form-control" type="text" name="mobile"
                               value="{{ !empty($customer->mobile)? $customer->mobile :  $customer->mobile}}"
                               maxlength="11">
                        @if($errors->has("mobile"))
                            <span class="help-block">{{ $errors->first("mobile") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('phone')) has-error @endif">
                        <label for="phone-field">{{__('backend.mobile_number2')}}: </label>
                        <input class="form-control" type="text" name="phone"
                               value="{{ !empty($customer->phone)? $customer->phone :   $customer->phone}}"
                               maxlength="11">
                        @if($errors->has("phone"))
                            <span class="help-block">{{ $errors->first("phone") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('national_id')) has-error @endif">
                        <label for="national_id-field">{{__('backend.national_id_number')}}: </label>
                        <input class="form-control" type="text" name="national_id"
                               value="{{ !empty($customer->national_id )? $customer->national_id :  $customer->national_id}}"
                               maxlength="14">
                        @if($errors->has("national_id"))
                            <span class="help-block">{{ $errors->first("national_id") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('job')) has-error @endif">
                        <label for="job-field">{{__('backend.job_name')}}: </label>
                        <input class="form-control" type="text" name="job"
                               value="{{ !empty($customer->job)? $customer->job:  $customer->job}}">
                        @if($errors->has("job"))
                            <span class="help-block">{{ $errors->first("job") }}</span>
                        @endif
                    </div>

                    {{--                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('country_id')) has-error @endif">--}}
                    {{--                        <label for="country_id-field">{{__('backend.choose_country')}}: </label>--}}
                    {{--                        <select id="country_id-field" name="country_id" class="form-control">--}}
                    {{--                            @if(! empty($countries))--}}
                    {{--                                @foreach($countries as $country)--}}
                    {{--                                    <option--}}
                    {{--                                        value="{{$country->id}}" {{ old("country_id") == $country->id  || $customer->country_id == $country->id  ? 'selected' : '' }}>{{$country->name}}</option>--}}
                    {{--                                @endforeach--}}
                    {{--                            @endif--}}
                    {{--                        </select>--}}
                    {{--                    </div>--}}
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('government_id')) has-error @endif">
                        <label for="government_id-field"> {{__('backend.government')}}: </label>
                        <select class="form-control government_id" id="government_id" name="government_id">
                            <option value="" data-display="Select">{{__('backend.government')}}</option>
                            @if(! empty($governments))
                                @foreach($governments as $government)
                                    <option
                                        value="{{$government->id}}" {{  old("government_id") == $government->id  ||  $customer->government_id == $government->id ? 'selected' : old("government_id")}}>
                                        {{$government->name_en}}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                        @if($errors->has("government_id"))
                            <span class="help-block">{{ $errors->first("government_id") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('city_id')) has-error @endif">
                        <label class="control-label" for="s_state_id">{{__('backend.city')}}</label>
                        <select class=" form-control city_id" id="city_id" name="city_id">
                            @foreach($cities as $city)
                                <option
                                    value="{{$city->id}}" {{ $city->governorate_id == $customer->government_id && $city->id == $customer->city_id ? 'selected' : old("city_id")}}>
                                    {{$city->name_en}}
                                </option>
                            @endforeach
                        </select>
                        @if($errors->has("city_id"))
                            <span class="help-block">{{ $errors->first("city_id") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('address')) has-error @endif">
                        <label for="national_id-field">{{__('backend.address')}}: </label>
                        <input class="form-control" type="text" name="address"
                               value="{{ !empty($customer->address)? $customer->address:  $customer->address}}">
                        @if($errors->has("address"))
                            <span class="help-block">{{ $errors->first("address") }}</span>
                        @endif
                    </div>

                    <div
                        class="form-group col-md-4 col-sm-4 @if($errors->has('latitude')) has-error @endif">
                        <div class=" input-group ">
                            <label for="receiver_latitude"> {{__('backend.latitude')}}: </label>
                            <input type="text" required id="latitude" name="latitude" class="form-control"
                                   value="{{ $customer->latitude ? $customer->latitude  : '30.0668886'  }}"/>
                            <span class="input-group-append">
                            <button class="input-group-text bg-transparent" type="button" data-toggle="modal"
                                    data-target="#map1Modal"> <i class="fa fa-map-marker"></i></button>
                        </span>

                            @if($errors->has("latitude"))
                                <span class="help-block">{{ $errors->first("latitude") }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('longitude')) has-error @endif">
                        <label for="longitude"> {{__('backend.longitude')}}: </label>
                        <input type="text" required id="longitude" name="longitude"
                               class="form-control"
                               value="{{ $customer->longitude ? $customer->longitude  : '31.1962743'  }}"/>
                        @if($errors->has("longitude"))
                            <span class="help-block">{{ $errors->first("longitude") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-4 col-sm-4">
                        <label for="mobile-users">{{__('backend.store')}}</label>
                        <select class="form-control select2 disabled" id="store_id" name="store_id">
                            <option value="" data-dispaly="Select">{{__('backend.store')}}</option>
                            @if(! empty($stores))
                                @foreach($stores as $store)
                                    <option value="{{$store->id}}"
                                            @if($customer->store_id == $store->id) selected @endif>{{$store->name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="form-group col-md-4 col-sm-4  @if($errors->has('password')) has-error @endif">
                        <label for="is_active-field">{{__('backend.password')}}</label>
                        <input class="form-control" type="password" name="password" value=""
                               placeholder="{{__('backend.enter_new_password')}}">
                        <span
                            class="help-block">{{__('backend.if_you_does_not_change_password_please_leave_it_empty')}}</span>
                        @if($errors->has("password"))
                            <span class="help-block">{{ $errors->first("password") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4">
                        <label for="manager-field">{{__('backend.manager')}}: </label>
                        <select id="manager-field" name="is_manager" class="form-control">
                            <option
                                value="0" {{ old("is_manager") == "0" ||  $customer->is_manager == "0" ? 'selected' : '' }}>{{__('backend.No')}}</option>
                            <option
                                value="1" {{ old("is_manager") == "1" ||  $customer->is_manager == "1" ? 'selected' : '' }}>{{__('backend.Yes')}}</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('image')) has-error @endif">
                        <div id="image_upload">
                            @if($customer->image)
                                <div class="imgDiv">
                                    <input type="hidden" value="{{ $customer->image }}" name="image_old">
                                    <img src="{{ $customer->image }}" style="width:100px;height:100px"
                                         onerror="this.src='{{asset('assets/images/user.png')}}'">
                                    <button class="btn btn-danger cancel">&times;</button>
                                </div>
                            @endif
                        </div>
                        <label for="image-field">{{__('backend.image')}}: </label>
                        <input class="uploadPhoto btn btn-primary" type="file" name="image" id="image">
                        @if($errors->has("image"))
                            <span class="help-block">{{ $errors->first("image") }}</span>
                        @endif
                    </div>
                </div>

                <hr>
                <div class="form-group col-12">
                    <h3 style="text-decoration: underline; display: inline-block">{{__('backend.task_days')}}</h3>
{{--                    <button type="button" class="btn btn-success" id="add_day"--}}
{{--                            style="display: inline-block; margin-right: 10px; margin-left: 10px">--}}
{{--                        {{__('backend.add_day')}}--}}
{{--                    </button>--}}
                </div>

                @php
                $days = $customer->task_days->pluck('collect_day')->toArray();
                @endphp

                <div class="form-group col-12">
                    <div class="form-group col-md-4 col-sm-4 day">
                        <label for="days-field">{{__('backend.add_day_by_number')}}:</label>
                        <select class="form-control select2" multiple name="days[]" id="day_number">
                            @for ($i = 1; $i <= 31; $i++)
                                <option value="{{$i}}" @if(in_array($i, $days)) selected @endif>{{$i}}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="form-group col-md-4 col-sm-4 day">
                        <label for="days-field">{{__('backend.add_day_by_name')}}:</label>
                        <select class="form-control select2" multiple name="days[]" id="day_name">
                            <option value="saturday" @if(in_array('saturday', $days)) selected @endif>{{__('backend.saturday')}}</option>
                            <option value="sunday" @if(in_array('sunday', $days)) selected @endif>{{__('backend.sunday')}}</option>
                            <option value="monday" @if(in_array('monday', $days)) selected @endif>{{__('backend.monday')}}</option>
                            <option value="tuesday" @if(in_array('tuesday', $days)) selected @endif>{{__('backend.tuesday')}}</option>
                            <option value="wednesday" @if(in_array('wednesday', $days)) selected @endif>{{__('backend.wednesday')}}</option>
                            <option value="thursday" @if(in_array('thursday', $days)) selected @endif>{{__('backend.thursday')}}</option>
                            <option value="friday" @if(in_array('friday', $days)) selected @endif>{{__('backend.friday')}}</option>
                        </select>
                    </div>

                </div>

{{--                <div class="form-group col-12" id="daysWrapper">--}}
{{--                    @foreach($customer->task_days as $key => $day)--}}
{{--                        <div class="form-group col-md-4 col-sm-4 day">--}}
{{--                            <label for="days-field">{{__('backend.day')}}#<span class="dayCounter">{{($key+1)}}</span>:--}}
{{--                                <span><i class="fa fa-trash delete_day"--}}
{{--                                         style="color: #ff0000; cursor: pointer"></i></span></label>--}}
{{--                            <select name="days[]" class="form-control">--}}
{{--                                @for ($i = 1; $i <= 31; $i++)--}}
{{--                                    <option value="{{$i}}" @if($day->collect_day== $i) selected @endif>{{$i}}</option>--}}
{{--                                @endfor--}}
{{--                            </select>--}}
{{--                        </div>--}}
{{--                    @endforeach--}}
{{--                </div>--}}

                <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
                    <button type="submit" class="btn btn-primary">{{__('backend.save_edits')}}</button>
                    <a class="btn btn-link pull-right"
                       href="{{ URL::previous() /*route('mngrAdmin.customers.index')*/ }}"><i
                            class="glyphicon glyphicon-backward"></i> {{__('backend.back')}} </a>
                </div>
            </form>
        </div>
    </div>

    @include('backend.map')

@endsection
@section('scripts')
    <script type="text/javascript" src="{{asset('/assets/scripts/map.js')}}"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key=AIzaSyDQiN4qc7pC9b1BRQwkqXXD28peMfWcHvw&callback=myMap"></script>
    <script src="{{asset('assets/select2/select2.min.js')}}"></script>

    <script type="text/javascript">
        $(function () {
            $("body").on("click", ".delete_day", function () {
                $(this).parents('.day').remove();
            });

            $("body").on("click", "#add_day", function () {
                let i = $(".day").length;
                row = `
                    <div class="form-group col-md-4 col-sm-4 day">
                        <label for="days-field">{{__('backend.day')}}#<span class="dayCounter">${(i + 1)}</span>:
                            <span><i class="fa fa-trash delete_day" style="color: #ff0000; cursor: pointer"></i></span></label>
                        <select name="days[]" class="form-control">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                        </select>
                    </div>
                        `;

                $("#daysWrapper").append(row);
            });

            $('.select2').select2();

            $("#day_number").select2({
                placeholder: "{{__('backend.day_number')}}",
                allowClear: true
            });

            $("#day_name").select2({
                placeholder: "{{__('backend.day_name')}}",
                allowClear: true
            });

            $("form[name='customer_edit']").validate({
                // Specify validation rules
                rules: {
                    email: {
                        required: true,
                        email: true,
                        // remote: {
                        //     url: "{!! url('/check-email-customer') !!}",
                        //     type: "get"
                        // }
                    },
                    mobile: {
                        required: true,
                        number: true,
                        digits: true,
                        rangelength: [11, 11],
                        // remote: {
                        //     url: "{!! url('/check-mobile-customer') !!}",
                        //     type: "get"
                        // }
                    },
                    name: {
                        required: true,
                    },
                    // country_id: {
                    //     required: true,
                    // },
                    government_id: {
                        required: true,
                    },
                    city_id: {
                        required: true,
                    },
                    latitude: {
                        number: true,
                    },
                    longitude: {
                        number: true,
                    },
                },
                // Specify validation error messages
                messages: {
                    email: {
                        required: "{{__('backend.Please_Enter_Customer_E-mail')}}",
                        email: "{{__('backend.Please_Enter_Customer_Correct_E-mail')}}",
                        // remote: jQuery.validator.format("{{__('backend.E-mail_already_exist')}}")
                    },
                    mobile: {
                        required: "{{__('backend.Please_Enter_Customer_Mobile')}}",
                        rangelength: "{{__('backend.Mobile_Must_Be11_Digits')}}",
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                        // remote: jQuery.validator.format("{{__('backend.Mobile_already_exist')}}")
                    },
                    {{--country_id: {--}}
                        {{--    required: "{{__('backend.Please_Chosse_The_Country')}}",--}}
                        {{--},--}}
                    government_id: {
                        required: "{{__('backend.Please_Chosse_The_Government')}}",
                    },
                    city_id: {
                        required: "{{__('backend.Please_Select_City')}}",
                    },
                    name: {
                        required: "{{__('backend.Please_Enter_Customer_Name')}}",
                    },
                    latitude: {
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",

                    },
                    longitude: {
                        required: "{{__('backend.Please_Enter_Longitude')}}",
                    },
                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                },

                highlight: function (element) {
                    $(element).parent().addClass('error')
                },

                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                },

                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                }
            });

            $("#government_id").on('change', function () {
                $('#city_id').html('<option value="" >Choose City</option>');
                if ($(this).val()) {
                    $.ajax({
                        url: "{{ url('/get_cities')}}" + "/" + $(this).val(),
                        type: 'get',
                        data: {},
                        success: function (data) {
                            $.each(data, function (i, content) {
                                $('#city_id').append($("<option></option>").attr("value", content.id).text(content.name_en));
                            });
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }
            });

        });
    </script>



@endsection

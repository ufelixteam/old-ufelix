<form id="order-form" method="POST" enctype="multipart/form-data" style="margin-bottom: 70px;">
    <input type="hidden" id="collection_id" name="collection_id"
           value="{{ ! empty($collection_id) ? $collection_id : '0' }}">
    <input type="hidden" id="order_id" name="order_id" value="{{ ! empty($order) ? $order->id : '0' }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="order_type_id" name="order_type_id"
           value="{{ ! empty($order) ? $order->order_type_id : '0' }}">

    <div class="row">
        <div class="col-md-6 col-xs-12">
            <div class="ribbon-wrapper card">
                <div class="ribbon ribbon-default">Receiver Data</div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-4">Name: </label>
                        <div class="col-12 col-md-8">
                            <input id="receiver_name-field" name="receiver_name" class="form-control"
                                   value="{{ ! empty($order) ? $order->receiver_name : old("receiver_name") }}"
                                   type="text" placeholder="Name">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-4">Mobile Number: </label>
                        <div class="col-12 col-md-8">
                            <input class="form-control" type="number" id="receiver_mobile-field" name="receiver_mobile"
                                   onkeypress="return event.charCode >= 48" min="1"
                                   value="{{ ! empty($order) ? $order->receiver_mobile : old("receiver_mobile") }}"
                                   placeholder="Mobile Number">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-4"> Mobile Number 2: </label>
                        <div class="col-12 col-md-8">
                            <input class="form-control" type="number" id="receiver_phone-field" name="receiver_phone"
                                   onkeypress="return event.charCode >= 48" min="1"
                                   alue="{{ ! empty($order) ? $order->receiver_phone : old("receiver_phone") }}"
                                   placeholder="Mobile Number 2">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-4">Government: </label>
                        <div class="col-12 col-md-4">

                            <select class=" select2 form-control r_government_id" id="r_government_id"
                                    name="r_government_id">
                                <option value="" data-display="Select"> Government</option>
                                @if(! empty($governments))
                                    @foreach($governments as $government)
                                        <option
                                            value="{{$government->id}}" {{ ! empty($order) && $order->r_government_id  == $government->id ? 'selected':  '' }} >
                                            {{$government->name_en}}
                                        </option>

                                    @endforeach
                                @endif

                            </select>
                            <div class="has-error hidden" id="no_fees">
                                <span class="help-block">{{__('backend.no_fees')}}</span>
                            </div>
                        </div>

                        <label class="col-sm-1"> City: </label>
                        <div class="col-12 col-md-3">
                            <select class="select2 form-control r_state_id" id="r_state_id" name="r_state_id">
                                <option value=""> City</option>

                                @if(! empty($r_cities))
                                    @foreach($r_cities as $r_city)
                                        <option
                                            value="{{$r_city->id}}" {{ ! empty($order) && $order->r_state_id  == $r_city->id ? 'selected':  '' }} >
                                            {{$r_city->name_en}}
                                        </option>

                                    @endforeach
                                @endif

                            </select>
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-4"> Receiver Address: </label>
                        <div class="col-12 col-md-8" style="position: relative;">
                            <input class="form-control" type="text" id="receiver_address" name="receiver_address"
                                   placeholder="Receiver Address"
                                   value="{{ ! empty($order) ? $order->receiver_address : old(" receiver_address") }}">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-4"> Location (optional): </label>
                        <div class="col-12 col-md-8">
                            <div class="row">
                                <div class="col-2 col-md-2">
                                    <i class="icon-map-pin" data-toggle="modal" data-target="#map1Modal"
                                       data-whatever="@fat"
                                       style=""></i>
                                </div>
                                <div class="col-5 col-md-5">
                                    <input class="form-control" type="text" name="receiver_latitude"
                                           id="receiver_latitude" placeholder="Latitude"
                                           value="{{ ! empty($order) ? $order->receiver_latitude : old(" receiver_latitude") }}">
                                </div>
                                <div class="col-5 col-md-5">
                                    <input class="form-control" type="text" name="receiver_longitude"
                                           id="receiver_longitude" placeholder="Longitude"
                                           value="{{ ! empty($order) ? $order->receiver_longitude : old(" receiver_longitude") }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-6 col-xs-6">
            <div class="ribbon-wrapper card">
                <div class="ribbon ribbon-info">Order Data</div>

                <div class="form-group">
                    <div class="row">

                        {{--                        <label class="col-sm-2"> Type: </label>--}}

                        {{--                        <div class="col-sm-4">--}}
                        {{--                            <select class=" select2 form-control" id="order_type_id" name="order_type_id">--}}
                        {{--                                <option value="" data-display="Select">Type</option>--}}
                        {{--                                @if(! empty($types))--}}
                        {{--                                    @foreach($types as $type)--}}
                        {{--                                        <option value="{{$type->id}}" {{ ! empty($order) && $order->order_type_id == $type->id ?--}}
                        {{--                                    'selected': '' }} >--}}
                        {{--                                            {{$type->name_en}}--}}
                        {{--                                        </option>--}}

                        {{--                                    @endforeach--}}
                        {{--                                @endif--}}
                        {{--                            </select>--}}
                        {{--                        </div>--}}

                        <label class="col-sm-2 "> Name: </label>

                        <div class="col-sm-10">

                            <input class="form-control " type="text" name="type" id="type-field"
                                   value="{{ ! empty($order) ? $order->type : old(" type") }}"
                                   placeholder="Type Name">
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-2">Pick-up Center:</label>
                        <div class="col-md-4">
                            <select id="store_id-field" name="main_store" class="form-control"
                                    @if($stores->isEmpty()) readonly @endif>
                                <option value="">Choose Pick-up Center</option>
                                @if(! empty($allStores))
                                    @foreach($allStores as $store)
                                        <option
                                            value="{{$store->id}}" {{ $store->id == old('main_store') || $store->id == @$order->main_store ? 'selected' : '' }} >
                                            {{$store->name}}
                                        </option>
                                    @endforeach
                                @endif

                            </select>
                        </div>

                        <label class="col-sm-2">Stock: </label>

                        <div class="col-md-4">
                            <select id="stock_id-field" name="store_id" class="form-control"
                                    @if($stores->isEmpty()) readonly @endif>
                                <option value="">Choose Stock</option>
                                @if(! empty($stores))
                                    @foreach($stores as $stock)
                                        <option
                                            value="{{$stock->id}}" {{ $stock->id == old('store_id') || $stock->id == @$order->store_id ? 'selected' : '' }} >
                                            {{$stock->title}}
                                        </option>
                                    @endforeach
                                @endif


                            </select>

                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">

                        <label class="col-sm-2" style="font-size: 14px;margin-top: 20px;">Quantity Order: </label>


                        <div class="col-md-4">

                            <input class="form-control"
                                   @if( ! empty($order->stock) ) max="{{$order->stock->available_quantity}}"
                                   @endif id="qty-field" type="number" name="qty" placeholder="Quantity"
                                   @if($stores->isEmpty()) readonly @endif value="{{ !
                                empty($order) ? $order->qty : old("qty") }}">
                            Available Quantity: <span id="available_quantity">0</span>
                        </div>


                        <label class="col-sm-2" style="font-size: 14px;margin-top: 20px;">Overweight: </label>


                        <div class="col-md-4">

                            <input type="number" required id="overload-field" name="overload" class="form-control"
                                   placeholder="Overweight"
                                   value="{{ ! empty($order) ? $order->overload : old(" overload") }}" readonly/>
                        </div>


                    </div>
                </div>

                <div class="form-group">
                    <div class="row">

                        <label class="col-sm-2" style="font-size: 14px;margin-top: 20px;">Delivery Fees: </label>

                        <div class="col-md-4">

                            <input class="form-control" type="number" id="delivery_price-field" name="delivery_price"
                                   placeholder="Delivery price"
                                   value="{{ ! empty($order) ? $order->delivery_price : old(" delivery_price") }}"
                                   readonly>
                        </div>

                        <label class="col-sm-2" style="font-size: 14px;margin-top: 20px;">Order Price : </label>
                        <div class="col-md-4">

                            <input class="form-control" id="order_price-field" type="number" name="order_price"
                                   value="{{ ! empty($order) ? $order->order_price : old(" order_price") }}"
                                   placeholder="Order Price">
                        </div>


                    </div>
                </div>


                <div class="form-group">
                    <div class="row">


                        <label class="col-sm-2" style="font-size: 14px;margin-top: 20px;">Delivery Fees From : </label>
                        <div class="col-md-4">

                            <select id="payment_method_id-field" name="payment_method_id" class="form-control select2">
                                <option value="" data-display="Select">Choose Method</option>

                                @if(! empty($payments))
                                    @foreach($payments as $payment)
                                        <option value="{{$payment->id}}" {{ ! empty($order) && $order->payment_method_id ==
                                    $payment->id ? 'selected': '' }}>
                                            {{$payment->name}}
                                        </option>
                                    @endforeach
                                @endif

                            </select>
                        </div>


                        <label class="col-sm-2" style="font-size: 14px;margin-top: 20px;">Total Price: </label>

                        <div class="col-md-4">

                            <div class="form-control" id="total_price">{{@$order->total_price}}</div>
                        </div>

                    </div>
                </div>


                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-4"> Notes: </label>
                        <div class="col-md-8">

                         <textarea class="form-control" name="notes" rows="4" cols="80"
                                   style="resize: vertical; min-height: 100px"
                                   placeholder="notes...">{{ ! empty($order) ? $order->notes : old("notes") }}</textarea>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
{{--    <div class="form-group">--}}
{{--        <div class="row text-center">--}}

{{--            <button type="button" class="btn btn-link text-center" data-toggle="modal" data-target="#exampleModal">--}}
{{--                Edit my data for this collection--}}
{{--            </button>--}}
{{--        </div>--}}
{{--    </div>--}}
<!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="ribbon-wrapper card">
                        <div class="ribbon ribbon-default">Change Sender Address</div>

                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-4">Government: </label>
                                <div class="col-12 col-md-8">

                                    <select class=" select2 form-control s_government_id" id="s_government_id"
                                            name="s_government_id">
                                        <option value="" data-display="Select">Sender Government</option>
                                        @if(! empty($governments))
                                            @foreach($governments as $government)
                                                <option
                                                    value="{{$government->id}}">
                                                    {{$government->name_en}}
                                                </option>

                                            @endforeach
                                        @endif

                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-4"> City: </label>
                                <div class="col-12 col-md-8">
                                    <select class="select2 form-control s_state_id" id="s_state_id" name="s_state_id">
                                        <option value="">Sender City</option>

                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-4"> Sender Mobile: </label>
                                <div class="col-12 col-md-8">
                                    <input class="form-control" type="text" id="sender_mobile" name="sender_mobile"
                                           value="{{@Auth::guard('Customer')->user()->mobile}}"
                                           placeholder="Mobile Number">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-4"> Sender Address: </label>
                                <div class="col-12 col-md-8" style="position: relative;">
                                    <i class="mdi mdi-map-marker-radius" data-toggle="modal" data-target="#map2Modal"
                                       data-whatever="@fat"
                                       style="text-align: center; font-size: 25px; top: 0px; position: absolute; right: 20px;"></i>
                                    <input class="form-control" type="text" id="sender_address" name="sender_address"
                                           value="{{@Auth::guard('Customer')->user()->address}}"
                                           placeholder="Sender Address">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-4"> Location Latitude : </label>
                                <div class="col-12 col-md-8">
                                    <input class="form-control" type="text" name="sender_latitude" id="sender_latitude"
                                           placeholder="Latitude"
                                           value="{{@Auth::guard('Customer')->user()->Corporate->latitude}}">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-4"> Location Longitude : </label>
                                <div class="col-12 col-md-8">
                                    <input class="form-control" type="text" name="sender_longitude"
                                           id="sender_longitude" placeholder="Longitude"
                                           value="{{@Auth::guard('Customer')->user()->Corporate->longitude}}">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <button type="button" class="btn btn-warning btn-rounded btn-block" id="order-btn">Edit Order
    </button>

    <button type="button" data-value="{{ ! empty($collection_id) ? $collection_id : '0' }}"
            class="btn btn-rounded btn-default btn-block" id="reset-btn">Reset Form
    </button>

</form>

@include('frontend.profile.collections.js', array('from_update' => true))



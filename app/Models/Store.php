<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    //
    public function governorate()
    {
        return $this->belongsTo('App\Models\Governorate', 'governorate_id');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City', 'state_id');
    }

    public function stocks()
    {
        return $this->hasMany('App\Models\Stock');
    }

    public function getStatusSpanAttribute($value)
    {
        /*
        * status = 0 :: Not Publish
        * status = 1 :: Publish
        */
        if ($this->status == 1) {
            $value = "<span class='badge badge-pill label-success'>" . __('backend.publish') . "</span>";
        } else {
            $value = "<span class='badge badge-pill label-danger'>" . __('backend.not_publish') . "</span>";
        }
        return $value;
    }

    public function getImageAttribute($value)
    {
        if ($value != "") {
            $value = asset("/api_uploades/warehouse/" . $value);
        } else {
            $value = "";
        }
        return $value;
    }

    /*pickup_default*/
    public function getPickupDefaultSpanAttribute($value)
    {
        /*
        * pickup_default = 0 :: Not
        * pickup_default = 1 :: Default
        */
        if ($this->pickup_default == 1) {
            $value = "<span class='badge badge-pill label-success' >" . __('backend.yes') . "</span>";
        } else {
            $value = "<span class='badge badge-pill label-danger'>" . __('backend.no') . "</span>";
        }
        return $value;
    }

    public function responsibles()
    {
        return $this->belongsToMany('App\Models\User', 'store_user', 'store_id', 'user_id')->withPivot('is_responsible');
    }
}

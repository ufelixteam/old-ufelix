@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.the_invoices')}}</title>
@endsection

@section('header')
	@php
		$type ="";
		if(app('request')->input('type') == 1){
    	$type = __('backend.the_driver');
    } else if(app('request')->input('type') == 2) {
    	$type = __('backend.the_corporate');
    } else if(app('request')->input('type') == 3) {
    	$type = __('backend.the_agent');
    }
	@endphp
  <div class="page-header clearfix">
    <h3>
      <i class="glyphicon glyphicon-align-justify"></i> {{ ! empty($type) ? $type : ''}} {{__('backend.invoices')}}
      <!--<a class="btn btn-success pull-right" href="{{ URL::asset('mngrAdmin/invoices/create?type='.app('request')->input('type')) }}"><i class="glyphicon glyphicon-plus"></i> Add New</a>-->
    </h3>
  </div>
@endsection

@section('content')
    <div class="row" style="margin-bottom:15px;">
        <div class="col-md-12">
            <div class="group-control col-md-3 col-sm-3">
                <input type="hidden" name="type" value="{{request()->type}}" id="type">
                <select id="object_id" name="object_id" class="form-control">
                    <option value=""> {{__('backend.choose')}}</option>
                    @foreach($objects as $object)
                        <option value="{{$object->id}}">{{$object->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="list">
        @include('backend.invoices.table')
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $("#object_id").on('change', function () {
            $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
            $.ajax({
                url: '{{URL::asset("/mngrAdmin/invoices")}}' + '?type=' + $("#type").val() + '&object_id=' + $("#object_id").val(),
                type: 'get',
                success: function (data) {
                    $('.list').html(data.view);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });
    </script>
@endsection

@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.the_orders')}} - {{__('backend.scan')}}</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">

    <style>
        td.arrowUp:before {
            content: '⇧';
            position: absolute;
            color: #FF0000FF;
            left: 0;
            font-weight: bold;
            font-size: 20px;
        }

        .form-check-inline {
            display: -webkit-inline-box;
            display: -ms-inline-flexbox;
            display: inline-flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            padding-left: 0;
            margin-right: .75rem;
        }

        .form-check-inline input {
            margin: 5px;
        }

        .toggle.ios, .toggle-on.ios, .toggle-off.ios {
            border-radius: 20px;
        }

        .toggle.ios .toggle-handle {
            border-radius: 20px;
        }

        .btn-warning {
            background-color: #e47d33;
            border-color: #e47d33;
        }

        .action_btn {
            width: 100px;
            margin: 1px;
            padding: 5px;
        }

        #delivery_problems {
            background-color: #e4cb10;
            border-color: #d6be0f;
            color: #000;
        }

        .dropdown-menu-right {
            right: auto;
            left: 0;
        }

        .table-responsive {
            overflow-y: overlay;
        }

        .tooltip-inner {
            max-width: none !important;
            width: 100% !important;
        }

        .scan-problems.table-striped > tbody > tr:nth-of-type(odd) {
            background-color: initial !important;
        }

        .popover {
            max-width: 800px !important;
        }

        .popover-title .close {
            position: relative;
            bottom: 1px;
            right: 2px;
        }

        .popover-title {
            padding: 0 !important;
            border-bottom: none !important;
        }

        .pac-container {
            z-index: 9999 !important;
        }

        .collected_cost.disabled {
            pointer-events: none;
            opacity: 0.6;
        }

        .counter {
            background-color: white;
            display: flex;
            justify-content: center;
            align-items: center;
            color: black;
            user-select: none;
            width: 145px;
            height: 145px;
            border-radius: 50%;
            margin: auto;
            border: 10px solid grey;
            font-size: 78px;
        }

        table.entries {
            width: 100%;
            border-spacing: 0px;
            margin: 10px 0;
        }

        table.entries thead.fixed {
            position: fixed;
            top: 0px;
        }

        /*.table > thead > tr > th:first-child{*/
        /*    padding-left: 5px !important;*/
        /*}*/

        #theTable thead {
            background: #6d6868;
            color: white;
        }

        .searched-past1 {
            border-top: 3px solid #6c6a6a;
            border-left: 3px solid #6c6a6a;
            border-right: 3px solid #6c6a6a
        }

        .searched-past2 {
            border-bottom: 3px solid #6c6a6a;
            border-left: 3px solid #6c6a6a;
            border-right: 3px solid #6c6a6a
        }

        .searched-current1 {
            border-top: 6px solid #74d774;
            border-left: 6px solid #74d774;
            border-right: 6px solid #74d774
        }

        .searched-current2 {
            border-bottom: 6px solid #74d774;
            border-left: 6px solid #74d774;
            border-right: 6px solid #74d774
        }

        .navbar-fixed-top {
            position: relative !important;
        }

        #wrapper #sidebar-nav, #wrapper .main {
            padding-top: 0px;
        }

        #wrapper .sidebar {
            top: 0 !important;
            padding-top: 60px !important;
        }
    </style>
@endsection
@section('header')
    <div class="page-header">
        <h4>
            <i class="glyphicon glyphicon-plus"></i> {{__('backend.scan_order')}} </h4>
    </div>
@endsection

@section('content')

    <audio id="alert-ding" src="{{asset('alert_ding.wav')}}" preload="auto"></audio>

    <div class="container-fluid">
        <div class="alert alert-success" role="alert" style="display: none">
        </div>
        <div class="row">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group col-sm-6" style="display: none">
                <label class="control-label" for="code-scan"> {{__('backend.Scanned_Code')}}</label>
                {{--                    <input type="text" id="code-scan" style="display: none">--}}
                {{--                    <textarea class="form-control" id="code-scan" rows="3" style="min-height: 100px; resize: vertical;"--}}
                {{--                              name="code"></textarea>--}}

            </div>
            <div class="col-md-3" style="padding: 0">
                <label class="control-label" for="code-scan"> {{__('backend.Scanned_Code')}}</label>
                <textarea class="form-control" id="code-scan" rows="6"
                          style="min-height: 100px; resize: vertical; margin-top: 15px"
                          name="code"></textarea>
                <div id="searchWrapper" style="display: none">
                    <input type="checkbox" name="quick_search" id="quick_search" value="1" style="display: inline;">
                    <input type="text" class="form-control" id="code-search" name="search_code"
                           style="display: inline; margin-top: 15px; width: 63%; margin-left: 5px; margin-right:5px;margin-bottom: 20px;">
                    <button type="button" class="btn btn-info" id="search-btn"
                            style="display: inline;"> {{__('backend.search')}}</button>
                </div>

                {{--                                        <select id="driver_filter" name="driver_filter" class="form-control" style="margin-bottom: 10px">--}}
                {{--                                            <option value="" data-display="Select">{{__('backend.Choose_Driver')}}</option>--}}
                {{--                                            @foreach($drivers as $driver)--}}
                {{--                                                <option value="{{$driver->id}}" data-display="Select">{{$driver->name}}</option>--}}
                {{--                                            @endforeach--}}
                {{--                                            <option value="0">{{__('backend.pending')}} </option>--}}
                {{--                                        </select>--}}
                {{--                                        <select id="status" name="status" class="form-control" style="margin-bottom: 10px">--}}
                {{--                                            <option value="" data-display="Select">{{__('backend.status')}}</option>--}}
                {{--                                            <option value="0">{{__('backend.pending')}} </option>--}}
                {{--                                            <option value="1">{{__('backend.accepted')}} </option>--}}
                {{--                                            <option value="2">{{__('backend.received')}} </option>--}}
                {{--                                            <option value="3">{{__('backend.delivered')}} </option>--}}
                {{--                                            <option value="4">{{__('backend.cancelled')}} </option>--}}
                {{--                                            <option value="5">{{__('backend.recalled')}} </option>--}}
                {{--                                        </select>--}}
                <button type="button" class="btn btn-warning" id="scan-btn"
                        style="margin-top: 15px; display: inline-block;vertical-align: baseline;"> {{__('backend.scan')}}</button>
                {{--                <div style="margin-top: 20px; display: none; margin-left: 10px;" id="exit_count">--}}
                {{--                        <span class='badge badge-pill label-success'>{{__('backend.orders') }} <span--}}
                {{--                                id="orders_count">0</span></span>--}}
                {{--                </div>--}}
                <div style="margin-top: 20px; display: none" id="accepted_count">
                        <span class='badge badge-pill label-warning'>{{__('backend.received') }} <span
                                id="received_count">0</span></span>
                    <span class='badge badge-pill label-danger'>{{__('backend.recalled') }} <span
                            id="recall_count">0</span></span>
                    <span class='badge badge-pill label-danger'>{{__('backend.rejected') }} <span
                            id="reject_count">0</span></span>
                </div>

            </div>
            <div class="col-md-3">
                <label class="control-label">{{__('backend.filter')}}</label>
                <div class="row" style="padding: 0 15px">
                    <div class="col-md-6">
                        <label class="radio"><input type="radio" name="radio_filter" value="captain_entry"
                                                    checked>{{__('backend.captain_entry')}}</label>
                        <label class="radio"><input type="radio" name="radio_filter"
                                                    value="captain_exit">{{__('backend.captain_exit')}}</label>
                        <label class="radio"><input type="radio" name="radio_filter"
                                                    value="recall_drop">{{__('backend.recall_drop')}}</label>
                        <label class="radio"><input type="radio" name="radio_filter"
                                                    value="client_drop">{{__('backend.client_drop')}}</label>
                        <label class="radio"><input type="radio" name="radio_filter"
                                                    value="quick_entry">{{__('backend.quick_entry')}}</label>

                    </div>
                    <div class="col-md-6">
                        <label class="radio"><input type="radio" name="radio_filter"
                                                    value="moved_to">{{__('backend.moved_to')}}</label>
                        <label class="radio"><input type="radio" name="radio_filter"
                                                    value="moved_dropped">{{__('backend.moved_dropped')}}</label>
                        <label class="radio"><input type="radio" name="radio_filter"
                                                    value="pending_dropped">{{__('backend.pending_dropped')}}
                        </label>
                        <label class="radio"><input type="radio" name="radio_filter"
                                                    value="pickup_dropped">{{__('backend.pickup_dropped')}}</label>
                        <label class="radio"><input type="radio" name="radio_filter"
                                                    value="all_orders">{{__('backend.all_orders')}}</label>

                    </div>
                </div>
            </div>
            <div class="col-md-4" style="padding-top: 30px">
                <div>

                    <a class="action_btn btn btn-success disabled" id="forward">
                        <i class="fa fa-eye"></i> {{__('backend.forward')}}
                    </a>
                    <a class="action_btn btn btn-success disabled" id="receive_orders">
                        <i class="fa fa-battery-half"></i> {{__('backend.Receive')}}
                    </a>
                    <a class="action_btn btn btn-success disabled" id="deliver_orders">
                        <i class="fa fa-battery"></i> {{__('backend.deliver')}}
                    </a>
                    {{--                        <a class="action_btn btn btn-info disabled" id="delay_orders">--}}
                    {{--                            <i class="fa fa-calendar"></i> {{__('backend.delay')}}--}}
                    {{--                        </a>--}}

                </div>
                <div>
                    <a class="action_btn btn btn-danger disabled" id="cancel_orders">
                        <i class="fa fa-close"></i> {{__('backend.Cancel')}}
                    </a>

                    <a class="action_btn btn btn-danger disabled" id="recall_orders">
                        <i class="fa fa-history"></i> {{__('backend.recall')}}
                    </a>

                    <a class="action_btn btn btn-danger disabled" id="reject_orders">
                        <i class="fa fa-history"></i> {{__('backend.reject')}}
                    </a>

                </div>
                <div>
                    <a class="action_btn btn btn-secondary disabled" id="dropped_orders">
                        <i class="fa fa-inbox"></i> {{__('backend.drop')}}
                    </a>
                    <a class="action_btn btn btn-danger disabled" id="order_moved_to">
                        <i class="fa fa-truck"></i> {{__('backend.move_to')}}
                    </a>
                    <a class="action_btn btn btn-danger disabled" id="moved_dropped">
                        <i class="fa fa-check"></i> {{__('backend.moved_drop')}}
                    </a>

                </div>
                <div>
                    <a class="action_btn btn btn-info disabled" id="order_comment"> {{__('backend.comment')}}</a>
                    <button type="button" id="pdf-submit" disabled
                            class="action_btn btn btn-warning">{{__('backend.print')}}</button>
                    <button type="button" id="export-submit" disabled
                            class="action_btn btn btn-warning">{{__('backend.export')}}</button>
                </div>
                <div>
                    <a class="action_btn btn btn-secondary disabled" id="drop_and_exit_again"
                       style="width: 205px"> {{__('backend.drop_and_exit_again')}}</a>
                    @if(permission('actionBack'))
                        <button type="button" id="action-back" disabled
                                class="action_btn btn btn-warning">{{__('backend.action_back')}}</button>
                    @endif
                </div>
            </div>
            <div class="col-md-2" style="padding-top: 30px">
                <main>
                    <div class="counter" id="orders_count"><span class="number">0</span></div>
                </main>
            </div>

        </div>
    </div>

    <div class="text-center" style="color:#5673ea;font-size: 54px; display: none" id="tempo"><h1
            class="fa fa-refresh fa-spin text-center"></h1>
    </div>
    <div id="print_form" style="display: none">

    </div>
    <div class="text-center list" id="table">

    </div>
    {{--    @include('backend.pickups.create_pickup_popup')--}}
    {{--    @include('backend.orders.delay_order_modal')--}}
    {{--    @include('backend.orders.delivery_problem')--}}
    @include('backend.orders.order_comment')
    @include('backend.scan.move_order_modal')
    @include('backend.scan.drop_task_modal')
    @include('backend.scan.action_back_modal')

@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('/assets/scripts/map-order.js')}}"></script>

    <script
        src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key=AIzaSyDQiN4qc7pC9b1BRQwkqXXD28peMfWcHvw&callback=myMap"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

    <script src="{{ asset('/assets/scripts/jquery-code-scanner.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <script type="text/javascript" src="{{asset('/assets/scripts/bootstrap-notify.min.js')}}"></script>

    <script>
        TableThing = function (params) {
            settings = {
                table: $('#theTable'),
                thead: []
            };

            this.fixThead = function () {
                // empty our array to begin with
                settings.thead = [];
                // loop over the first row of td's in &lt;tbody> and get the widths of individual &lt;td>'s
                $('tbody tr:first td', $('#theTable')).each(function (i, v) {
                    settings.thead.push($(v).width());
                });

                // now loop over our array setting the widths we've got to the &lt;th>'s
                for (i = 0; i < settings.thead.length; i++) {
                    $('#floatThead th:eq(' + i + ')', $('#theTable')).width(settings.thead[i]);
                }


            }
        }
        $(function () {
            var table = new TableThing();

            $(window).scroll(function () {
                $("#floatThead", $('#theTable')).width($('#theTable').width());
                var windowTop = $(window).scrollTop();
                if ($('#theTable').length && windowTop > $('#theTable').offset().top) {
                    if (!$("#floatThead", $('#theTable')).hasClass("fixed")) {
                        $("#floatThead", $('#theTable')).show().addClass("fixed");
                        table.fixThead();
                    }
                } else {
                    $("#floatThead", $('#theTable')).hide().removeClass("fixed");
                }
            });
            // $(window).scroll(function () {
            //     table.fixThead();
            // });
        });

        $.notifyDefaults({
            type: 'success',
            allow_dismiss: true
        });

        $(document).ready(function () {
            const counter = document.querySelector(".counter")
            let number = document.querySelector(".number")
            let i = 0

            function count() {
                i++
                number.innerHTML = i
            }

            counter.addEventListener("click", count);

            $('body').on('change', '.delivery_status', function () {
                let parent = $(this).parents("tr");
                let total_cost = parent.find('.total_cost');
                let collected_cost = parent.find('.collected_cost');
                collected_cost.removeClass('disabled');

                if ($(this).val() == 1) {
                    if (!collected_cost.val() || collected_cost.val() === '0') {
                        collected_cost.val(total_cost.html());
                    }
                }

                if ($(this).val() == 3) {
                    collected_cost.val(0).addClass('disabled');
                }
            });

            $(document).on('dblclick', '.delivery_status', function () {
                if (this.checked) {
                    $(this).prop('checked', false);
                }
            });

            $(document).on("paste", "input.collected_cost", function (e) {
                if (e.originalEvent.clipboardData.getData('Text').match(/[^\d+\.\d+-]/)) {
                    e.preventDefault();
                }
            });

            $(document).on("keypress", "input.collected_cost", function (e) {
                if ((e.which < 48 || e.which > 58) && e.which != 46 && e.which != 41 && e.which != 45) {
                    return false;
                }
            });
        });

        function change_status(info = {}, orders = [], customer = "") {
            let ids = [];
            let idsWithCollectedCost = [];
            let customer_id = '';
            $(".selectpdf input:checked").each(function () {
                if (!orders || orders.length === 0) {
                    ids.push($(this).val());
                }
                if (!customer) {
                    customer_id = $(this).attr('data-customer');
                }

            });

            if (orders && orders.length !== 0) {
                ids = orders
            }

            if (customer) {
                customer_id = customer
            }

            if (info['type'] == 'deliver') {
                ids.forEach(function (value, index) {
                    idsWithCollectedCost.push({
                        id: value,
                        collected_cost: $("#" + value + " .collected_cost").val(),
                        delivery_status: $("input[name='delivery_status_" + value + "']:checked").val()
                    });
                });
            }

            if (ids.length > 0) {
                info['ids'] = ids;
                info['idsWithCollectedCost'] = idsWithCollectedCost;
                info['_token'] = "{{csrf_token()}}";
                info['customer_id'] = customer_id;
                $.ajax({
                    url: "{{ url('/mngrAdmin/scan_order_action')}}",
                    type: 'post',
                    data: info,
                    success: function (data) {
                        if (info['type'] == 'forward') {
                            $("#accepted_count").show();
                            $("#received_count").html(data.received_count);
                            $("#recall_count").html(data.recall_count);
                            $("#reject_count").html(data.rejected_count);
                        }

                        if (data.ids_status) {
                            data.ids_status.forEach(function (entry) {
                                if (data.status) {
                                    $("#" + entry.id + " .status").html(entry.status);
                                    // console.log('entry ', entry);
                                }

                                if (info['type'] == 'drop_and_exit_again') {
                                    count--;
                                    // console.log('hererererre');
                                    let order_id = entry.id;
                                    $('#' + order_id).next().remove();
                                    $('#' + order_id).remove();
                                    $("#orders_count").html(count);
                                    $('.serial').each(function (i, obj) {
                                        $(this).html(++i);
                                    })
                                }
                            });
                        } else if (data.ids) {
                            data.ids.forEach(function (entry) {
                                if (data.status) {
                                    $("#" + entry + " .status").html(data.status);
                                    // console.log('entry ', entry);
                                }
                                $('#pickup_' + entry).prop({'checked': false}).attr('disabled', 'disabled'); // Unchecks &disabled it

                                if (data.driver) {
                                    $('#driver_' + entry).html(data.driver.name); // Unchecks &disabled it
                                    $('#selectOrder_' + entry).attr('data-driver', data.driver.id); // Unchecks &disabled it
                                    $('#' + entry).attr('data-driver', data.driver.id);
                                }

                                if (info['type'] == 'deliver' && ($("input[name='radio_filter']:checked").val() === 'captain_entry' || $("input[name='radio_filter']:checked").val() === 'quick_entry')) {
                                    if (!$("#" + entry + " .collected_cost").val()) {
                                        $("#" + entry + " .collected_cost").val($("#" + entry + " .total_cost").html());
                                    }

                                    if (!$('.delivery_status').is(':checked')) {
                                        $("input[name=delivery_status_" + entry + "][value=1]").attr('checked', 'checked');
                                    }
                                }

                                if (info['type'] == 'drop_and_exit_again') {
                                    count--;

                                    // console.log('hererererre');
                                    let order_id = entry;
                                    $('#' + order_id).next().remove();
                                    $('#' + order_id).remove();
                                    $("#orders_count").html(count);
                                    $('.serial').each(function (i, obj) {
                                        $(this).html(++i);
                                    })
                                }

                            });
                        }

                        if (data.result == true) {
                            $(".alert").removeClass('alert-danger').addClass('alert-success');
                        } else {
                            $(".alert").removeClass('alert-success').addClass('alert-danger');
                        }

                        ["recall_problem", "delay_problem"].forEach(function (id) {
                            // console.log(id);
                            let elm = document.getElementById(id);
                            if (elm) {
                                document.getElementById(id).checked = false;
                            }
                        });

                        window.scrollTo(0, 0);
                        $(".alert").html(data.message);
                        $(".alert").show();
                        window.setTimeout(function () {
                            $(".alert").hide();
                        }, 3000);

                        if (info['type'] == 'client_drop' && data['refund_collection_id']) {
                            $('<form action="{{ url("/mngrAdmin/print_refund/")}}" method="get" target="_blank"><input value="' + data['refund_collection_id'] + '" name="refund_collection_id"  /></form>').appendTo('body').submit();
                            $(".selectpdf input:checked").each(function () {
                                $(this).prop({'checked': false}).attr('disabled', 'disabled');
                            });
                        }
                    },
                    error: function (data) {
                        play_alert();
                        a.open();
                    }
                });
            }
        }

        function check_validation(info = {}, id = "", customer = "") {
            var ids = [];
            var customer_id = customer;
            var collected_cost = null;
            var one_row = 0;

            if (id) {
                ids.push(id);
                collected_cost = $(".collected_cost[data-id='" + id + "']").val();
                one_row = 1;
            } else {
                $(".selectpdf input:checked").each(function () {
                    ids.push($(this).val());
                });
            }

            if (ids.length > 0) {
                info['ids'] = ids;
                info['_token'] = "{{csrf_token()}}";

                $.ajax({
                    url: "{{ url('/mngrAdmin/scan_order_validation')}}",
                    type: 'post',
                    data: info,
                    success: function (data) {
                        $(".arrowUp").removeClass('arrowUp');
                        if (data.result == false) {
                            play_alert();
                            if (data.more_drivers == 1) {
                                a.content = `<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c">
                <i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">
                {{__("backend.scan_order")}}</span></div><div style="font-weight:bold;">
                <p>More than one driver has been detected</p>
                </div>`;
                            } else if (data.not_found) {
                                a.content = `<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c">
                <i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">
                {{__("backend.scan_order")}}</span></div><div style="font-weight:bold;">
                <p>Orders not found or not allowed</p>
                <pre style="white-space: pre-wrap !important;">${data.not_found}</pre>
                </div>`;
                                data.not_found.split(',').forEach(function (value, index) {
                                    $("#" + value).parents('.orderRow').children(':first-child').addClass('arrowUp');
                                });
                            }
                            a.open();
                        } else {
                            if (info['type'] == 'forward') {
                                $("#forwardModal").modal("show");
                            } else if (info['type'] == 'moved_to') {
                                $("#moveModal").modal("show");
                                $("#move_ids").val(ids);
                                // play_alert();
                                {{--$.confirm({--}}
                                {{--    title: '',--}}
                                {{--    theme: 'modern',--}}
                                {{--    class: 'danger',--}}
                                {{--    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-meh-o text-success"></i></span><span class="jconfirm-title">{{__("backend.Confirm")}}</span></div><div style="font-weight:bold;"><p>{{__("backend.moved_to_orders_confirm")}}</p></div>',--}}
                                {{--    buttons: {--}}
                                {{--        confirm: {--}}
                                {{--            text: '{{__("backend.Ok")}}',--}}
                                {{--            btnClass: 'btn-danger',--}}
                                {{--            action: function () {--}}
                                {{--                $("#moveModal").modal("show");--}}
                                {{--                $("#move_ids").val(ids);--}}
                                {{--            }--}}
                                {{--        },--}}
                                {{--        cancel: {--}}
                                {{--            text: '{{__("backend.Cancel")}}',--}}
                                {{--            action: function () {--}}
                                {{--            }--}}
                                {{--        }--}}
                                {{--    }--}}
                                {{--});--}}
                            } else if (info['type'] == 'receive') {
                                // play_alert();
                                $.confirm({
                                    title: '',
                                    theme: 'modern',
                                    class: 'danger',
                                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-meh-o text-success"></i></span><span class="jconfirm-title">{{__("backend.Confirm")}}</span></div><div style="font-weight:bold;"><p>{{__("backend.receive_orders_confirm")}}</p></div>',
                                    buttons: {
                                        confirm: {
                                            text: '{{__("backend.Ok")}}',
                                            btnClass: 'btn-danger',
                                            action: function () {
                                                change_status({
                                                    type: 'receive'
                                                }, ids, customer_id);
                                            }
                                        },
                                        cancel: {
                                            text: '{{__("backend.Cancel")}}',
                                            action: function () {
                                            }
                                        }
                                    }
                                });
                            } else if (info['type'] === 'dropped') {
                                // play_alert();
                                $.confirm({
                                    title: '',
                                    theme: 'modern',
                                    class: 'danger',
                                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-meh-o text-success"></i></span><span class="jconfirm-title">{{__("backend.Confirm")}}</span></div><div style="font-weight:bold;"><p>{{__("backend.dropped_orders_confirm")}}</p></div>',
                                    buttons: {
                                        confirm: {
                                            text: '{{__("backend.Ok")}}',
                                            btnClass: 'btn-danger',
                                            action: function () {
                                                change_status({
                                                    type: 'dropped'
                                                }, ids, customer_id);
                                            }
                                        },
                                        cancel: {
                                            text: '{{__("backend.Cancel")}}',
                                            action: function () {
                                            }
                                        }
                                    }
                                });
                            } else if (info['type'] === 'action_back') {
                                // play_alert();
                                $.confirm({
                                    title: '',
                                    theme: 'modern',
                                    class: 'danger',
                                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-meh-o text-success"></i></span><span class="jconfirm-title">{{__("backend.Confirm")}}</span></div><div style="font-weight:bold;"><p>{{__("backend.action_back_confirm")}}</p></div>',
                                    buttons: {
                                        confirm: {
                                            text: '{{__("backend.Ok")}}',
                                            btnClass: 'btn-danger',
                                            action: function () {
                                                $("#actionBackModal").modal("show");
                                            }
                                        },
                                        cancel: {
                                            text: '{{__("backend.Cancel")}}',
                                            action: function () {
                                            }
                                        }
                                    }
                                });
                            } else if (info['type'] === 'drop_and_exit_again') {
                                // play_alert();
                                $.confirm({
                                    title: '',
                                    theme: 'modern',
                                    class: 'danger',
                                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-meh-o text-success"></i></span><span class="jconfirm-title">{{__("backend.Confirm")}}</span></div><div style="font-weight:bold;"><p>{{__("backend.dropped_orders_confirm")}}</p></div>',
                                    buttons: {
                                        confirm: {
                                            text: '{{__("backend.Ok")}}',
                                            btnClass: 'btn-danger',
                                            action: function () {
                                                change_status({
                                                    type: 'drop_and_exit_again'
                                                }, ids, customer_id);
                                            }
                                        },
                                        cancel: {
                                            text: '{{__("backend.Cancel")}}',
                                            action: function () {
                                            }
                                        }
                                    }
                                });
                            } else if (info['type'] === 'recall_drop') {
                                // play_alert();
                                $.confirm({
                                    title: '',
                                    theme: 'modern',
                                    class: 'danger',
                                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-meh-o text-success"></i></span><span class="jconfirm-title">{{__("backend.Confirm")}}</span></div><div style="font-weight:bold;"><p>{{__("backend.dropped_orders_confirm")}}</p></div>',
                                    buttons: {
                                        confirm: {
                                            text: '{{__("backend.Ok")}}',
                                            btnClass: 'btn-danger',
                                            action: function () {
                                                change_status({
                                                    type: 'recall_drop'
                                                }, ids, customer_id);
                                            }
                                        },
                                        cancel: {
                                            text: '{{__("backend.Cancel")}}',
                                            action: function () {
                                            }
                                        }
                                    }
                                });
                            } else if (info['type'] === 'client_drop') {
                                // play_alert();
                                $.confirm({
                                    title: '',
                                    theme: 'modern',
                                    class: 'danger',
                                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-meh-o text-success"></i></span><span class="jconfirm-title">{{__("backend.Confirm")}}</span></div><div style="font-weight:bold;"><p>{{__("backend.dropped_orders_confirm")}}</p></div>',
                                    buttons: {
                                        confirm: {
                                            text: '{{__("backend.Ok")}}',
                                            btnClass: 'btn-danger',
                                            action: function () {
                                                change_status({
                                                    type: 'client_drop'
                                                }, ids, customer_id);
                                            }
                                        },
                                        cancel: {
                                            text: '{{__("backend.Cancel")}}',
                                            action: function () {
                                            }
                                        }
                                    }
                                });
                            } else if (info['type'] === 'moved_dropped') {
                                // play_alert();
                                $.confirm({
                                    title: '',
                                    theme: 'modern',
                                    class: 'danger',
                                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-meh-o text-success"></i></span><span class="jconfirm-title">{{__("backend.Confirm")}}</span></div><div style="font-weight:bold;"><p>{{__("backend.dropped_orders_confirm")}}</p></div>',
                                    buttons: {
                                        confirm: {
                                            text: '{{__("backend.Ok")}}',
                                            btnClass: 'btn-danger',
                                            action: function () {
                                                change_status({
                                                    type: 'moved_dropped'
                                                }, ids, customer_id);
                                            }
                                        },
                                        cancel: {
                                            text: '{{__("backend.Cancel")}}',
                                            action: function () {
                                            }
                                        }
                                    }
                                });
                            } else if (info['type'] === 'pickup_dropped') {
                                // play_alert();
                                $.confirm({
                                    title: '',
                                    theme: 'modern',
                                    class: 'danger',
                                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-meh-o text-success"></i></span><span class="jconfirm-title">{{__("backend.Confirm")}}</span></div><div style="font-weight:bold;"><p>{{__("backend.dropped_orders_confirm")}}</p></div>',
                                    buttons: {
                                        confirm: {
                                            text: '{{__("backend.Ok")}}',
                                            btnClass: 'btn-danger',
                                            action: function () {
                                                change_status({
                                                    type: 'pickup_dropped'
                                                }, ids, customer_id);
                                            }
                                        },
                                        cancel: {
                                            text: '{{__("backend.Cancel")}}',
                                            action: function () {
                                            }
                                        }
                                    }
                                });
                            } else if (info['type'] == 'deliver') {
                                // play_alert();
                                $.confirm({
                                    title: '',
                                    theme: 'modern',
                                    class: 'danger',
                                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-meh-o text-success"></i></span><span class="jconfirm-title">{{__("backend.Confirm")}}</span></div><div style="font-weight:bold;"><p>{{__("backend.delivery_orders_confirm")}}</p></div>',
                                    buttons: {
                                        confirm: {
                                            text: '{{__("backend.Ok")}}',
                                            btnClass: 'btn-danger',
                                            action: function () {
                                                change_status({
                                                    type: 'deliver'
                                                }, ids, customer_id);
                                            }
                                        },
                                        cancel: {
                                            text: '{{__("backend.Cancel")}}',
                                            action: function () {
                                            }
                                        }
                                    }
                                });
                            } else if (info['type'] == 'recall') {
                                // play_alert();
                                $.confirm({
                                    title: '',
                                    theme: 'modern',
                                    class: 'danger',
                                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-meh-o text-success"></i></span><span class="jconfirm-title">{{__("backend.Confirm")}}</span></div><div style="font-weight:bold;"><p>{{__("backend.recall_orders_confirm")}}</p></div>',
                                    buttons: {
                                        confirm: {
                                            text: '{{__("backend.Ok")}}',
                                            btnClass: 'btn-danger',
                                            action: function () {
                                                change_status({
                                                    type: 'recall'
                                                }, ids, customer_id);
                                            }
                                        },
                                        cancel: {
                                            text: '{{__("backend.Cancel")}}',
                                            action: function () {
                                            }
                                        }
                                    }
                                });
                            } else if (info['type'] == 'reject') {
                                // play_alert();
                                $.confirm({
                                    title: '',
                                    theme: 'modern',
                                    class: 'danger',
                                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-meh-o text-success"></i></span><span class="jconfirm-title">{{__("backend.Confirm")}}</span></div><div style="font-weight:bold;"><p>{{__("backend.reject_orders_confirm")}}</p></div>',
                                    buttons: {
                                        confirm: {
                                            text: '{{__("backend.Ok")}}',
                                            btnClass: 'btn-danger',
                                            action: function () {
                                                if (one_row) {
                                                    change_status({
                                                        collected_cost: collected_cost,
                                                        one_row: one_row,
                                                        type: 'reject'
                                                    }, ids, customer_id);
                                                } else {
                                                    change_status({
                                                        type: 'reject'
                                                    }, ids, customer_id);
                                                }

                                            }
                                        },
                                        cancel: {
                                            text: '{{__("backend.Cancel")}}',
                                            action: function () {
                                            }
                                        }
                                    }
                                });
                            } else if (info['type'] == 'cancel') {
                                // play_alert();
                                $.confirm({
                                    title: '',
                                    theme: 'modern',
                                    class: 'danger',
                                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-meh-o text-success"></i></span><span class="jconfirm-title">{{__("backend.Confirm")}}</span></div><div style="font-weight:bold;"><p>{{__("backend.cancel_orders_confirm")}}</p></div>',
                                    buttons: {
                                        confirm: {
                                            text: '{{__("backend.Ok")}}',
                                            btnClass: 'btn-danger',
                                            action: function () {
                                                change_status({
                                                    type: 'cancel'
                                                }, ids, customer_id);
                                            }
                                        },
                                        cancel: {
                                            text: '{{__("backend.Cancel")}}',
                                            action: function () {
                                            }
                                        }
                                    }
                                });

                            } else if (info['type'] == 'problems') {
                                var driver_id = [];
                                var orders_id = [];
                                var latitude = 0;
                                var longitude = 0;
                                $(".selectpdf input:checked").each(function () {
                                    orders_id.push($(this).val());
                                    latitude = $(this).attr('data-latitude');
                                    longitude = $(this).attr('data-longitude');
                                    // console.log(latitude, ' - ', longitude);
                                    if (driver_id.indexOf($(this).attr('data-driver')) !== -1 || driver_id.length === 0) {
                                        driver_id.push($(this).attr('data-driver'))
                                    }
                                    // console.log('driveeer', $(this).val())
                                });
                                // console.log('driver  ', driver_id.length, ' - ', orders_id.length);
                                if (driver_id.length === orders_id.length) {
                                    $("div.select_driver select").val(driver_id[0]);
                                    $(".latitude").val(latitude);
                                    $(".longitude").val(longitude);
                                } else {
                                    $("div.select_driver select").val('');
                                    $("div.latitude ").val(0);
                                    $("div.longitude ").val(0);
                                }
                                $("#problemModal").modal("show");
                            } else if (info['type'] == 'comments') {
                                var driver_id = [];
                                var orders_id = [];
                                var latitude = 0;
                                var longitude = 0;
                                $(".selectpdf input:checked").each(function () {
                                    orders_id.push($(this).val());
                                    latitude = $(this).attr('data-latitude');
                                    $('input.collected_cost').on('paste', function (event) {
                                        if (event.originalEvent.clipboardData.getData('Text').match(/[^\d]/)) {
                                            event.preventDefault();
                                        }
                                    });

                                    $("input.collected_cost").on("keypress", function (event) {
                                        if (event.which < 48 || event.which > 58) {
                                            return false;
                                        }
                                    });
                                    longitude = $(this).attr('data-longitude');
                                    // console.log(latitude, ' - ', longitude);
                                    if (driver_id.indexOf($(this).attr('data-driver')) !== -1 || driver_id.length === 0) {
                                        driver_id.push($(this).attr('data-driver'))
                                    }
                                    // console.log('driveeer', $(this).val())
                                });
                                // console.log('driver  ', driver_id.length, ' - ', orders_id.length);
                                if (driver_id.length === orders_id.length) {
                                    $("div.select_driver select").val(driver_id[0]);
                                    $(".latitude").val(latitude);
                                    $(".longitude").val(longitude);
                                } else {
                                    $("div.select_driver select").val('');
                                    $("div.latitude ").val(0);
                                    $("div.longitude ").val(0);
                                }
                                $("#commentModal").modal("show");
                            } else if (info['type'] == 'delay') {
                                $("#delayModal").modal("show");

                            }
                        }
                    },
                    error: function (data) {
                        play_alert();
                        a.open();
                    }
                });
            }
        }

        $(function () {
            $('#delay_date, #delay_at, #created_at').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                language: 'en',
                todayHighlight: true,
                startDate: new Date()
            });

            /*print*/
            $('.toggle').bootstrapToggle();

            $("#delivery_problem_id").on('change', function () {
                if ($(this).val() == 5) {
                    $("#other_reason").show();
                } else {
                    $("#reason").val("");
                    $("#other_reason").hide();
                }
            });

            $('input[type=radio][name=delay_comment]').change(function () {
                if (this.value == 'Other reason') {
                    $("#delay_reason").show();
                } else {
                    $("#del_reason").val("");
                    $("#delay_reason").hide();
                }
            });

            $('body').on('change', '.selectOrder', function (e) {
                if ($('input[name="select[]"]').is(':checked')) {
                    if ($("input[name='radio_filter']:checked").val() === 'captain_entry') {
                        $(".action_btn").not('#toggle, #forward, #order_moved_to, #moved_dropped, #drop_and_exit_again').removeClass('disabled');
                        $("#pdf-submit").prop('disabled', false);
                    } else if ($("input[name='radio_filter']:checked").val() === 'quick_entry') {
                        $(".action_btn").not('#toggle, #forward, #order_moved_to, #moved_dropped').removeClass('disabled');
                        $("#pdf-submit").prop('disabled', false);
                    } else if ($("input[name='radio_filter']:checked").val() === 'captain_exit') {
                        $("#forward, #receive_orders, #pdf-submit").removeClass('disabled');
                        $("#pdf-submit").prop('disabled', false);
                    } else if ($("input[name='radio_filter']:checked").val() === 'pending_dropped') {
                        $("#dropped_orders").removeClass('disabled');
                    } else if ($("input[name='radio_filter']:checked").val() === 'moved_dropped') {
                        $("#moved_dropped").removeClass('disabled');
                    } else if ($("input[name='radio_filter']:checked").val() === 'moved_to') {
                        let warehouses = [];
                        $(".selectOrder:checked").each(function () {
                            warehouses.push($(this).attr('data-warehouse'));
                        });

                        const allEqual = arr => arr.every(v => v === arr[0]);

                        if ($('input[name="select[]"]').is(':checked') && allEqual(warehouses)) {
                            $("#order_moved_to").removeClass('disabled');
                        } else {
                            $("#order_moved_to").addClass('disabled');
                        }
                    } else if ($("input[name='radio_filter']:checked").val() === 'pickup_dropped') {
                        $("#dropped_orders").removeClass('disabled');
                    } else if ($("input[name='radio_filter']:checked").val() === 'recall_drop') {
                        $("#dropped_orders").removeClass('disabled');
                    } else if ($("input[name='radio_filter']:checked").val() === 'client_drop') {
                        let customers = [];
                        $(".selectOrder:checked").each(function () {
                            customers.push($(this).attr('data-customer'));
                        });

                        const allEqual = arr => arr.every(v => v === arr[0]);

                        if ($('input[name="select[]"]').is(':checked') && allEqual(customers)) {
                            $("#dropped_orders").removeClass('disabled');
                        } else {
                            $("#dropped_orders").addClass('disabled');
                        }
                    } else {
                        $(".action_btn").not('#toggle, #dropped_orders').removeClass('disabled');
                        $("#pdf-submit").prop('disabled', false);
                        $("#dropped_orders").removeClass('disabled');
                    }
                    $("#action-back").prop('disabled', false);
                    $("#export-submit").prop('disabled', false);
                    $("#order_comment").prop('disabled', false);
                } else {
                    $(".action_btn").not('#toggle').addClass('disabled');
                    $("#pdf-submit").prop('disabled', true);
                    $("#export-submit").prop('disabled', true);
                }
            });

            $('body').on('change', '#agent', function (e) {
                $.ajax({
                    url: "{{ url('/mngrAdmin/getdriver_by_agents')}}" + "/" + $(this).val(),
                    type: 'get',
                    data: {},
                    success: function (data) {

                        $('#driver').html('<option value="">Chosse Driver</option>');
                        $.each(data, function (key, content) {
                            $("#driver").append($("<option></option>").attr("value", content.id).text(content.name));
                        });

                    },
                    error: function () {
                        alert('Error occured');
                    }
                });

            });

            $('body').on('click', '#saveMove', function (e) {
                // info['ids'] = $("#ids").val();
                var info = $('#moveForm').serialize();
                // console.log('info', info);
                info['_token'] = "{{csrf_token()}}";
                $.ajax({
                    url: "{{ url('mngrAdmin/move_collections')}}",
                    type: 'post',
                    data: info,
                    success: function (data) {
                        $("#moveModal").modal("hide");
                        // $("#driver_id, #cost, #store_id, #move_ids, #move_by").val("");
                        if (data.ids) {
                            data.ids.forEach(function (entry) {
                                if (data.status) {
                                    $("#" + entry + " .status").html(data.status);
                                    // console.log('entry ', entry);
                                }
                                $('#selectOrder_' + entry).prop({'checked': false}).attr('disabled', 'disabled'); // Unchecks &disabled it
                            });
                        }

                        if (data.result == true) {
                            $(".alert").removeClass('alert-danger').addClass('alert-success');
                        } else {
                            $(".alert").removeClass('alert-success').addClass('alert-danger');
                        }

                        window.scrollTo(0, 0);
                        $(".alert").html(data.message);
                        $(".alert").show();
                        window.setTimeout(function () {
                            $(".alert").hide();
                        }, 3000);
                    },
                    error: function (data) {
                        play_alert();
                        a.open();
                    }
                });
            });
            /*
                        $('body').on('change', '.delivery_status', function () {
                            // $('#save_changes_' + $(this).attr('data-id')).removeClass('disabled');
                            var self = $(this);
                            let id = $(this).attr('data-id');
                            let collected_cost = $("#collected_cost_" + id).val();
                            let delivery_status = $(this).closest("tr").find("input:checked").val();
                            $.ajax({
                                url: '{{URL::asset("/mngrAdmin/change_delivery_status")}}',
                    type: 'post',
                    data: {
                        id: id,
                        delivery_status: delivery_status,
                        collected_cost: collected_cost,
                        _token: "{{csrf_token()}}"
                    },
                    success: function (data) {
                        if (data.result === true) {
                            $(".alert").removeClass('alert-danger').addClass('alert-success');
                            if (data.status) {
                                $("#" + id + " .status").html(data.status);
                                if (data.delivery_status === 3) {
                                    $(".delivery_status_class_" + id).prop('disabled', true);
                                    $("#collected_cost_" + id).prop('disabled', true);
                                }
                            }

                        } else {
                            $(".alert").removeClass('alert-success').addClass('alert-danger');
                        }

                        window.scrollTo(0, 0);
                        $(".alert").html(data.message);
                        $(".alert").show();
                        window.setTimeout(function () {
                            $(".alert").hide();
                        }, 3000);

                    },
                    error: function (data) {
                        console.log('Error:', data);

                    }
                });
            });
*/
            /*$('body').on('input', '.collected_cost', function () {
                $('#save_changes_' + $(this).attr('data-id')).removeAttr('disabled');
            });*/
            $('body').on('click', '.save_changes', function () {
                $(".arrowUp").removeClass('arrowUp');
                let orders_scan = [];
                let all_delivery_status = [];
                let drivers = [];
                let hasNoCollectedCost = [];
                // console.log('in saveeeee', $('.collected_cost'));
                // var collected_cost = $('.collected_cost');
                /* for(var i = 0; i < collected_cost.length; i++){
                     let id = $(collected_cost[i]).attr('data-id');
                     let delivery_status = $("input:checked").$('#delivery_status_' + id).val();
                     orders_scan.push({
                         order_id: $(collected_cost[i]).attr('data-id'),
                         collected_cost: $(collected_cost[i]).val(),
                     });
                 }*/

                $("#theTable > tbody > tr.orderRow").each(function () {
                    let currentRow = $(this); //Do not search the whole HTML tree twice, use a subtree instead
                    let delivery_status = currentRow.find(".delivery_status:checked").val();

                    if (!currentRow.find(".collected_cost").val() && (delivery_status == 2 || delivery_status == 4)) {
                        hasNoCollectedCost.push(currentRow.find(".order_number").attr('id'));
                        // play_alert();
                        // a.open();
                        // return false;
                    }

                    if (drivers.indexOf(currentRow.attr('data-driver')) == -1) {
                        drivers.push(currentRow.attr('data-driver'))
                    }
                    if (!delivery_status && !currentRow.find(".label-black").length) {
                        all_delivery_status.push(currentRow.find(".order_number").attr('id'));
                    }
                    orders_scan.push({
                        id: currentRow.find(".collected_cost").attr('data-id'),
                        collected_cost: currentRow.find(".collected_cost").val(),
                        total_cost: currentRow.find(".total_cost").html(),
                        delivery_status: delivery_status
                    });
                });

                if (drivers.length > 1 || !drivers.length) {
                    play_alert();
                    a.content = `<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c">
                <i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">
                {{__("backend.scan_order")}}</span></div><div style="font-weight:bold;">
                <p>More than one driver has been detected</p>
                </div>`;
                    a.open();
                    return false;
                }

                if (hasNoCollectedCost.length) {
                    play_alert();
                    a.content = `<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c">
                <i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">
                {{__("backend.scan_order")}}</span></div><div style="font-weight:bold;">
                <p>Orders not have collected cost</p>
                <pre style="white-space: pre-wrap !important;">${hasNoCollectedCost.join(',')}</pre>
                </div>`;

                    hasNoCollectedCost.forEach(function (value, index) {
                        $("#" + value).parents('.orderRow').children(':first-child').addClass('arrowUp');
                    });

                    a.open();
                    return false;
                }

                if (all_delivery_status.length) {
                    play_alert();
                    a.content = `<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c">
                <i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">
                {{__("backend.scan_order")}}</span></div><div style="font-weight:bold;">
                <p>Orders not have delivery status</p>
                <pre style="white-space: pre-wrap !important;">${all_delivery_status.join(',')}</pre>
                </div>`;

                    all_delivery_status.forEach(function (value, index) {
                        $("#" + value).parents('.orderRow').children(':first-child').addClass('arrowUp');
                    });

                    a.open();
                    return false;
                }

                // var self = $(this);
                /*let id = $(this).attr('data-id');
                // let delivery_status = $("input:checked").$('#delivery_status_' + id).val();
                let collected_cost = $("#collected_cost_" + id).val();*/
                $.ajax({
                    url: '{{URL::asset("/mngrAdmin/save_scan_order_rows")}}',
                    type: 'post',
                    data: {
                        orders_scan: orders_scan,
                        _token: "{{csrf_token()}}",
                        driver_id: drivers[0],
                        type: $("input[name='radio_filter']:checked").val()
                    },
                    success: function (data) {
                        // console.log('data', data);
                        if (data.result === true) {
                            $(".alert").removeClass('alert-danger').addClass('alert-success');
                            if (data.data) {
                                $.each(data.data, function (index, row) {
                                    let order_id = row.id;
                                    $("#" + order_id + " .status").html(row.status);
                                    if (row.delivery_status === '3') {
                                        $(".delivery_status_class_" + order_id).prop('disabled', true);
                                        // $("#collected_cost_" + order_id).prop('disabled', true);
                                    } else if (row.delivery_status === '1' || row.delivery_status === '2' || row.delivery_status === '4') {
                                        // console.log('in  second cond');
                                        $("#delivery_status_value_3_" + order_id).prop('disabled', true);
                                    }

                                    // if (row.recall === '5') {
                                    //     $(".collected_cost_" + order_id).addClass('disabled');
                                    // }
                                });

                                if (data.scan_collection_id) {
                                    $('<form action="{{ url("/mngrAdmin/reports/scan_collection_print/")}}" method="get" target="_blank"><input value="' + data.scan_collection_id + '" name="scan_collection_id"  /></form>').appendTo('body').submit();
                                }

                            }
                            window.scrollTo(0, 0);
                            $(".alert").html(data.message);
                            $(".alert").show();
                            window.setTimeout(function () {
                                $(".alert").hide();
                            }, 3000);
                        } else {
                            //$(".alert").removeClass('alert-success').addClass('alert-danger');
                            play_alert();
                            a.open();
                        }

                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });

            });

            $('body').on('click', '#save_captain_exit', function () {
                let orders_scan = [];
                let drivers = [];

                $("#theTable > tbody > tr.orderRow").each(function () {

                    let currentRow = $(this); //Do not search the whole HTML tree twice, use a subtree instead
                    if (drivers.indexOf(currentRow.attr('data-driver')) == -1) {
                        drivers.push(currentRow.attr('data-driver'))
                    }

                    orders_scan.push(currentRow.attr('id'));
                });

                if (drivers.length > 1 || !drivers.length) {
                    play_alert();
                    a.content = `<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c">
                <i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">
                {{__("backend.scan_order")}}</span></div><div style="font-weight:bold;">
                <p>More than one driver has been detected</p>
                </div>`;
                    a.open();
                    return false;
                }

                $.ajax({
                    url: '{{URL::asset("/mngrAdmin/save_captain_exit_orders")}}',
                    type: 'post',
                    data: {orders_scan: orders_scan, _token: "{{csrf_token()}}", driver_id: drivers[0]},
                    success: function (data) {
                        if (data.result === true) {
                            $(".alert").removeClass('alert-danger').addClass('alert-success');
                            if (data.data) {
                                if (data.scan_collection_id) {
                                    $('<form action="{{ url("/mngrAdmin/reports/scan_collection_print/")}}" method="get" target="_blank"><input value="' + data.scan_collection_id + '" name="scan_collection_id"  /></form>').appendTo('body').submit();
                                }

                            }
                            window.scrollTo(0, 0);
                            $(".alert").html(data.message);
                            $(".alert").show();
                            window.setTimeout(function () {
                                $(".alert").hide();
                            }, 3000);
                        } else {
                            //$(".alert").removeClass('alert-success').addClass('alert-danger');
                            play_alert();
                            a.open();
                        }

                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });

            });

            $('body').on('click', '#removeRow', function (e) {
                e.preventDefault();
                count--;
                // console.log('hererererre');
                let order_id = $(this).attr('data-id');
                $(this).parents("tr").next().remove();
                $(this).parents("tr").remove();
                $("#orders_count").html(count);
                $('.serial').each(function (i, obj) {
                    $(this).html(++i);
                })
            });

            $('body').on('click', '#saveDelay', function (e) {
                if ($("#delay_date").val()) {
                    let ids = [];
                    $(".selectOrder:checked").each(function () {
                        ids.push($(this).val());
                    });
                    $.ajax({
                        url: "{{ url('/mngrAdmin/delay_orders')}}",
                        type: 'post',
                        data: {
                            ids: ids,
                            delay_date: $("#delay_date").val(),
                            delay_comment: $("input[name='delay_comment']:checked").val(),
                            delay_reason: $("#del_reason").val(),
                            change_status: $("input[name='change_status']:checked").val(),
                            flash: 0,
                            '_token': "{{csrf_token()}}"
                        },
                        success: function (data) {
                            if (data.ids) {
                                data.ids.forEach(function (entry) {
                                    if (data.status) {
                                        $("#" + entry + " .status").html(data.status);
                                    }
                                });
                            }

                            if (data.result == true) {
                                $(".alert").removeClass('alert-danger').addClass('alert-success');
                            } else {
                                $(".alert").removeClass('alert-success').addClass('alert-danger');
                            }

                            $("#delayModal").modal("hide");

                            window.scrollTo(0, 0);
                            $(".alert").html(data.message);
                            $(".alert").show();
                            window.setTimeout(function () {
                                $(".alert").hide();
                            }, 3000);

                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }
            });

            $('body').on('click', '#order_comment', function (e) {

                check_validation({
                    type: 'comments'
                });

            });

            $('body').on('click', '#delivery_problems', function (e) {

                check_validation({
                    type: 'problems'
                });

            });

            $('body').on('click', '#deliver_orders', function (e) {

                check_validation({
                    type: 'deliver'
                });

            });

            $('body').on('click', '#receive_orders', function (e) {
                check_validation({
                    type: 'receive'
                });
            });

            $('body').on('click', '#recall_orders', function (e) {
                check_validation({
                    type: 'recall'
                });
            });

            $('body').on('click', '#reject_orders', function (e) {
                check_validation({
                    type: 'reject'
                });
            });

            $('body').on('click', '.recallOne', function (e) {
                let id = $(this).attr('data-id');
                let customer_id = $(this).attr('data-customer');
                check_validation({
                    type: 'recall'
                }, id, customer_id);
            });

            $('body').on('click', '.rejectOne', function (e) {
                let id = $(this).attr('data-id');
                let customer_id = $(this).attr('data-customer');
                check_validation({
                    type: 'reject'
                }, id, customer_id);
            });

            $('body').on('click', '#cancel_orders', function (e) {
                check_validation({
                    type: 'cancel'
                });
            });

            $('body').on('click', '#forward', function (e) {
                check_validation({
                    type: "forward"
                });

            });

            $('body').on('click', '#dropped_orders', function (e) {
                if ($("input[name='radio_filter']:checked").val() === 'pickup_dropped') {
                    check_validation({
                        type: "pickup_dropped"
                    });
                } else if ($("input[name='radio_filter']:checked").val() === "recall_drop") {
                    check_validation({
                        type: "recall_drop"
                    });
                } else if ($("input[name='radio_filter']:checked").val() === 'client_drop') {
                    check_validation({
                        type: "client_drop"
                    });

                } else {
                    check_validation({
                        type: "dropped"
                    });

                }
            });

            $('body').on('click', '#drop_and_exit_again', function (e) {
                check_validation({
                    type: "drop_and_exit_again"
                });
            });

            $('body').on('click', '#action-back', function (e) {
                check_validation({
                    type: "action_back"
                });
            });

            $('body').on('click', '#action_back', function (e) {
                if ($("#action_status").val()) {
                    change_status({
                        type: 'action_back',
                        action_status: $("#action_status").val()
                    });

                    $("#actionBackModal").modal("hide");
                }
            });

            $('body').on('click', '.dropOne', function (e) {
                let id = $(this).attr('data-id');
                let customer_id = $(this).attr('data-customer');
                if ($("input[name='radio_filter']:checked").val() === 'pickup_dropped') {
                    check_validation({
                        type: "pickup_dropped"
                    }, id, customer_id);
                } else if ($("input[name='radio_filter']:checked").val() === "recall_drop") {
                    check_validation({
                        type: "recall_drop"
                    }, id, customer_id);
                } else if ($("input[name='radio_filter']:checked").val() === 'client_drop') {
                    check_validation({
                        type: "client_drop"
                    }, id, customer_id);

                } else {
                    check_validation({
                        type: "dropped"
                    }, id, customer_id);

                }
            });

            $('body').on('click', '#order_moved_to', function (e) {
                check_validation({
                    type: "moved_to"
                });

            });
            $('body').on('click', '#moved_dropped', function (e) {
                check_validation({
                    type: "moved_dropped"
                });

            });

            $('body').on('click', '#delay_orders', function (e) {
                check_validation({
                    type: "delay"
                });

            });

            $("#problems_form").submit(function (event) {
                event.preventDefault();

                if ($("#driver_id").val() && $("#delivery_problem_id").val()) {
                    // console.log('delay at ', $("input[name='delay_at']").val());
                    change_status({
                        type: 'problems',
                        driver_id: $("#driver_id").val(),
                        latitude: $("#receiver_latitude").val(),
                        longitude: $("#receiver_longitude").val(),
                        delivery_problem_id: $("#delivery_problem_id").val(),
                        reason: $("#reason").val(),
                        problem_radio: $("input[name='problem_radio']:checked").val() ? $("input[name='problem_radio']:checked").val() : null,
                        delay_at: $("input[name='delay_at']").val()
                    });

                    $("#problemModal").modal("hide");
                }
            });

            $("#comments_form").submit(function (event) {
                event.preventDefault();

                if ($("#comment").val()) {
                    // console.log('delay at ', $("input[name='delay_at']").val());
                    change_status({
                        type: 'comments',
                        driver_id: $("#driver_id").val(),
                        latitude: $("#receiver_latitude").val(),
                        longitude: $("#receiver_longitude").val(),
                        comment: $("#comment").val(),
                        created_at: $("input[name='created_at']").val()
                    });

                    $("#commentModal").modal("hide");
                }
            });

            $('body').on('click', '#forward_action', function (e) {
                if ($("#driver").val()) {
                    change_status({
                        type: 'forward',
                        driver_id: $("#driver").val()
                    });

                    $("#forwardModal").modal("hide");
                }
            });
            $('body').on('click', '#moved_to_action', function (e) {
                if ($("#store").val()) {
                    change_status({
                        type: 'moved_to',
                        store_id: $("#store").val()
                    });

                    $("#movedToModal").modal("hide");
                }
            });

            $('body').on('click', '.printdata', function (e) {

                var url = "{{URL::asset('/mngrAdmin/print_pdf/')}}/" + $(this).attr('data-id');
                $('<form action="' + url + '" method="get" target="_blank"></form>').appendTo('#orders').submit();
            });

        });
        $('body').on('click', '.problem_radio', function () {
            var problem_radio = $("input[name='problem_radio']:checked").val();
            // console.log('hereeee in body ', problem_radio);

            if (problem_radio == 'delay') {
                // console.log('hereeee in body if', problem_radio);

                $('.delay_at').removeClass('hidden');
            } else {
                // console.log('hereeee in body ', problem_radio);

                $('.delay_at').addClass('hidden');
            }
        });

        $('body').on('click', '#export-submit', function (e) {

            let ids = [];

            $(".selectpdf input:checked").each(function () {
                ids.push($(this).val());

            });
            if (ids.length > 0) {
                $("#print_form").html('');
                $('<form action="{{ url("/mngrAdmin/reports/export_scan_orders/")}}" method="post" target="_blank"><input value="' + ids + ' " name="ids"  />" {{csrf_field()}}"</form>').appendTo('#print_form').submit();

            }
        });

        $('body').on('click', '#pdf-submit', function (e) {

            let ids = [];

            $(".selectpdf input:checked").each(function () {
                ids.push($(this).val());

            });
            if (ids.length > 0) {
                $.ajax({
                    url: "{{ url('/mngrAdmin/scan_order_validation')}}",
                    type: 'post',
                    data: {ids: ids, '_token': "{{csrf_token()}}", type: 'print'},
                    success: function (data) {
                        $(".arrowUp").removeClass('arrowUp');
                        if (data.result == false) {
                            play_alert();
                            if (data.more_drivers == 1) {
                                a.content = `<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c">
                <i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">
                {{__("backend.scan_order")}}</span></div><div style="font-weight:bold;">
                <p>More than one driver has been detected</p>
                </div>`;
                            } else if (data.not_found) {
                                a.content = `<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c">
                <i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">
                {{__("backend.scan_order")}}</span></div><div style="font-weight:bold;">
                <p>Orders not found or not allowed</p>
                <pre style="white-space: pre-wrap !important;">${data.not_found}</pre>
                </div>`;
                                data.not_found.split(',').forEach(function (value, index) {
                                    $("#" + value).parents('.orderRow').children(':first-child').addClass('arrowUp');
                                });
                            }
                            a.open();
                        } else {
                            $("#print_form").html('');
                            $('<form action="{{ url("/mngrAdmin/scan_order_print/")}}" method="post" target="_blank"><input value="' + ids + ' " name="ids"  /><input value="' + $("#driver_filter").val() + ' " name="driver_id"  />" {{csrf_field()}}"</form>').appendTo('#print_form').submit();
                        }
                    },
                    error: function (data) {
                        play_alert();
                        a.open();
                    }
                });
            }
        });

        $('body').on('click', '#toggle', function () {

            switch ($(this).attr('data-state')) {
                case "1" :
                    $("input.selectOrder:not(:disabled)").prop('checked', true).trigger('change');
                    // $(".selectOrder").prop('checked', true).trigger('change');
                    $(this).attr('data-state', "2");
                    break;
                case "2" :
                    $("input.selectOrder:not(:disabled)").prop('checked', false).trigger('change');
                    // $(".selectOrder").prop('checked', false).trigger('change');
                    $(this).attr('data-state', "1");
                    break;
            }
        });

    </script>

    <script type="text/javascript">
        var page = 1;
        var init = 1;
        var count = 0;
        var sort_status = null;
        var rearrange = 0;
        var code_scan = "";

        $(function () {

            $('#code-scan').codeScanner({
                onScan: function ($element, code) {

                    if (!$('#quick_search').is(':checked')) {
                        code_scan = code;
                        // console.log($element.val(), code, $element.val().includes(code));
                        if (!$element.val().includes(code)) {
                            if ($element.val()) {
                                $element.val($element.val() + ',' + code);
                            } else {
                                $element.val(code);
                            }
                        } else {
                            // console.log($element.val().trim().split(code));
                            if ($("#code-scan").is(":focus") && $element.val().trim().split(code).length > 2) {
                                const str = $element.val().slice(0, parseInt('-' + code.length));
                                $element.val(str);
                            }
                        }

                        getData(page);
                    }

                }
            });

            $('#code-search').codeScanner({
                onScan: function ($element, code) {

                    if ($('#quick_search').is(':checked')) {
                        $element.val(code);

                        if (code && !$("#" + code).length && !isNaN(parseFloat(code)) && isFinite(code) && code.length % 2 == 0) {
                            code = code.substring(1);
                        }

                        if ($("#" + code).length) {

                            $('html, body').animate({
                                scrollTop: $("#" + code).offset().top - 65
                            }, "fast");

                            $(".searched-current1").addClass('searched-past1').removeClass('searched-current1');
                            $(".searched-current2").addClass('searched-past2').removeClass('searched-current2');

                            $("#" + code).parent().addClass('searched-current1');
                            $("#" + code).parent().next().addClass('searched-current2');
                        }
                        // $("#" + code).parent().css('border-top', '3px solid #6c6a6a')
                        //     .css('border-left', '3px solid #6c6a6a')
                        //     .css('border-right', '3px solid #6c6a6a');
                        // $("#" + code).parent().next().css('border-bottom', '3px solid #6c6a6a')
                        //     .css('border-left', '3px solid #6c6a6a')
                        //     .css('border-right', '3px solid #6c6a6a');
                    }

                }
            });

            $("#search-btn").on('click', function () {
                // console.log($("#" + $("#code-search").val()).position(), $("#" + $("#code-search").val()).offset(), $(window).scrollTop());
                // $('#floatThead').css('top', $("#" + $("#code-search").val()).position().left - 5);
                let code = $("#code-search").val();
                if (code && !$("#" + code).length && !isNaN(parseFloat(code)) && isFinite(code) && code.length % 2 == 0) {
                    code = code.substring(1);
                }

                if ($("#" + code).length) {
                    $('html, body').animate({
                        scrollTop: $("#" + code).offset().top - 65
                    }, "fast");
                }

                $(".searched-current1").addClass('searched-past1').removeClass('searched-current1');
                $(".searched-current2").addClass('searched-past2').removeClass('searched-current2');

                $("#" + code).parent().addClass('searched-current1');
                $("#" + code).parent().next().addClass('searched-current2');

                // $("#" + $("#code-search").val()).parent().css('border-top', '3px solid #6c6a6a')
                //     .css('border-left', '3px solid #6c6a6a')
                //     .css('border-right', '3px solid #6c6a6a');
                // $("#" + $("#code-search").val()).parent().next().css('border-bottom', '3px solid #6c6a6a')
                //     .css('border-left', '3px solid #6c6a6a')
                //     .css('border-right', '3px solid #6c6a6a');
            });

            $("#scan-btn").on('click', function () {
                // page = 1;
                // location.hash = page;
                sort_status = null;
                rearrange = 0;
                code_scan = $("#code-scan").val();
                getData(page);
            });

            // $("#status, #driver_filter").on('change', function () {
            //     init = 1;
            //     count = 0;
            //     $("#code-scan").val('');
            //     $('.list').empty().html('');
            // });

            // $(window).on('hashchange', function () {
            //     if (window.location.hash) {
            //         page = window.location.hash.replace('#', '');
            //         if (page == Number.NaN || page <= 0) {
            //             return false;
            //         } else {
            //             getData(page);
            //         }
            //     }
            // });

            $(document).on('click', '.pagination a', function (event) {
                event.preventDefault();

                $('li').removeClass('active');
                $(this).parent('li').addClass('active');

                var myurl = $(this).attr('href');
                var page = $(this).attr('href').split('page=')[1];

                //getData(page);
                location.hash = page;
            });


        });

        var open = false;
        var a = $.confirm({
            lazyOpen: true,
            animation: 'zoom',
            title: '',
            theme: 'material',
            content: `<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c">
                <i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">
                {{__("backend.scan_order")}}</span></div><div style="font-weight:bold;">
                <p>{{__("backend.bad_choice")}}</p>
                </div>`,
            type: 'red',
            onOpen: function () {
                // after the modal is displayed.
                open = true;
            },
            onClose: function () {
                // after the modal is displayed.
                open = false;
            },
        });
        $(document).ready(function () {
            $("input[name='radio_filter']").click(function () {
                // console.log('radio ', $("input[name='radio_filter']:checked").val());
                $(".action_btn").addClass('disabled');
                $('.list').html('');
                init = 1;
                count = 0;

                // if ($("input[name='radio_filter']:checked").val() == 'captain_exit') {
                //     $("#exit_count").css('display', 'inline-block');
                //     $("#orders_count").html('0');
                // } else {
                //     $("#exit_count").hide();
                // }

                $("#orders_count").html('0');

                if ($("input[name='radio_filter']:checked").val() == 'quick_entry') {
                    $("#searchWrapper").show();
                } else {
                    $("#searchWrapper").hide();
                    $("#quick_search").prop('checked', false);
                }
            });

            $(document).on("click", ".rearrange", function (e) {
                $(".action_btn").addClass('disabled');
                $('.list').html('');
                init = 1;
                count = 0;
                sort_status = $(this).attr('data-status');
                if (sort_status == 'default') {
                    rearrange = 0;
                    sort_status = null;
                } else {
                    rearrange = 1;
                }

                $("#orders_count").html('0');
                getData(page);

            });
        });

        function getData(page) {
            // console.log(page);
            $("#tempo").show();
            if (init == 1) {
                $('.list').html('');
            }

            if (open) {
                a.close();
            }

            $.ajax({
                url: '{{URL::asset("/mngrAdmin/scan_order_qrcode")}}' + '?code=' + code_scan + '&page=' + page + '&init=' + init,
                type: 'get',
                data: {
                    'filter': $("input[name='radio_filter']:checked").val(),
                    'sort_status': sort_status,
                    'rearrange': rearrange
                },
                success: function (data) {

                    $("#tempo").hide();

                    if (init == 1) {
                        if (data.count != 0) {
                            init = 2;
                            count += data.count;
                            $("#orders_count").html(count);
                        }
                        $('.list').empty().html(data.view);

                    } else {
                        if (data.count != 0) {
                            data.views.forEach(function (entry) {
                                const myEle = document.getElementById(entry.id);
                                if (!myEle) {
                                    count++;
                                    $("#orders_count").html(count);
                                    $("#tbody").append(entry.view);
                                    $("#" + entry.id + " .serial").html(count);
                                } else {
                                    play_alert();
                                }
                            });
                        }
                    }

                    $(".toggle").bootstrapToggle('destroy');
                    $(".toggle").bootstrapToggle();
                    location.hash = page;

                    if (data.count != 0) {
                        let code_val = $("#code-scan").val();
                        if (code_val) {
                            if (!code_val.includes(code_scan)) {
                                code_val += ',' + code_scan;
                                // getData(page);
                            }
                        } else {
                            // getData(page);
                            code_val = code_scan;
                        }

                        $("#code-scan").val(code_val);
                    } else {
                        // document.getElementById('alert-ding').play();
                        // play_alert();
                        // a.open();
                    }

                    if (data.not_found) {
                        play_alert();
                        a.content = `<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c">
                <i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">
                {{__("backend.scan_order")}}</span></div><div style="font-weight:bold;">
                <p>Orders not found or not allowed</p>
                <pre style="white-space: pre-wrap !important;">${data.not_found}</pre>
                </div>`;
                        a.open();
                    }

                    // $('.testtooltip').tooltip({
                    //     sanitize: true,
                    //     trigger: 'hover'
                    // });
                    $('.testtooltip').popover({
                        container: 'body'
                        // content: $('#myPopoverContent').html(),
                        // html: true,
                        // trigger: "hover"
                    });
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }

        function play_alert() {
            var snd = new Audio("{{asset('alert_ding.wav')}}");
            snd.play();
        }

        $(document).on("click", ".popover .close", function () {
            $(this).parents(".popover").popover('hide');
            $(this).parents(".popover").remove();
        });

        $(document).on("click", ".inline_comment", function (e) {
            e.preventDefault();
            var self = $(this);
            $.ajax({
                url: "{{ url('/mngrAdmin/orders/add_order_comment')}}",
                type: 'post',
                data: {
                    comment: self.next().val(),
                    order_id: self.attr('data-id'),
                    '_token': "{{csrf_token()}}"
                },
                success: function (data) {
                    self.addClass('disabled');
                    self.next().val('');
                    $.notify(data);
                    setTimeout(function () {
                        $.notifyClose('top-right');
                    }, 2000);
                }
            });
        });

        $(document).on("input", ".comment_text", function (e) {
            if ($(this).val()) {
                $(this).prev().removeClass('disabled');
            } else {
                $(this).prev().addClass('disabled');
            }
        });

        $("#add_refund_collection").click(function () {
            $("#refunWrapper").append(
                `<div class="form-check form-check-inline">
                                    <input class="form-control refund_collections" type="text" value="" name="refund_collections[]">
                                </div>`
            );
        });

        var taskId = null;
        $('body').on('click', '#drop_button', function (e) {
            let ids = [taskId];
            let refund_collections = [];

            $(".refund_collections").each(function () {
                refund_collections.push($(this).val());
            });

            let data = {
                _token: "{{csrf_token()}}",
                ids: ids,
                refund_collections: refund_collections,
                drop_comment: $("#drop_comment").val(),
                confirm_pickup: $("#confirm_pickup").is(":checked"),
                confirm_cash_collect: $("#confirm_cash_collect").is(":checked"),
                confirm_recall_orders: $("#confirm_recall_orders").is(":checked"),
                pickup_orders: $("#pickup_orders").val(),
                cash_money: $("#cash_money").val(),
                recall_orders: $("#recallOrders").val()
            };

            $("#dropModal").modal("hide");
            $.ajax({
                url: "{{ url('/mngrAdmin/dropoff_pickups/')}}",
                type: 'POST',
                data: data,
                success: function (data) {
                    $("#" + taskId + " .status").html(data.status);
                    $("#" + taskId + " .dropPickup").addClass('disabled');
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $('body').on('click', '.dropPickup', function (e) {
            taskId = $(this).attr('data-id');
            $.confirm({
                title: '',
                theme: 'modern',
                class: 'warning',
                content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-simle-o  text-warning"></i></span><span class="jconfirm-title"> {{__("backend.confirm_drop_off")}}</span></div><div style="font-weight:bold;"><p>{{__("backend.drop_off_pickups")}}</p></div>',
                buttons: {
                    confirm: {
                        text: '{{__("backend.drop_off_text_pickups")}}',
                        btnClass: 'btn-warning',
                        action: function () {
                            $("#dropModal").modal("show");
                        }
                    },
                    cancel: {
                        text: 'Cancel',
                        action: function () {
                        }
                    }
                }
            });
        });
    </script>
@endsection

@extends('frontend.layouts.app')

@section('title') @lang('website.ForgotTitle') @endsection

@section('content')


<section class="req body-in">
  <div class="container">
      <div class="col-md-6 col-md-offset-3 col-xs-12">
          <div class="req-x">
            <div class="modal-body mb-1">
                <div class="title">
                    <h3>@lang('website.ForgotTitle')</h3>
                    <span class="text-warning"><i class="fas fa-info-circle prefix"></i>@lang('website.ForgotMessage')</span>
                </div>

                <form action="{{  app()->getLocale() == 'en' ? url('/en/forgotpassword') : url('forgotpassword')}}" method="post" name="forgotForm" id="forgotForm">
                  {{csrf_field()}}
                    <div class="form-group  @if($errors->has('mobile')) has-error @endif">
                        <input type="text"  id="mobile" name="mobile" value="{{old('mobile')}}" class="form-control" placeholder="@lang('website.Mobile')" />    
                        <i class="fas fa-mobile prefix"></i>

                        @if($errors->has("mobile"))
                            <span class="help-block">{{ $errors->first("mobile") }}</span>
                        @endif                      
                     </div>

                    @include('flash::message')

                     <div class="form-group text-center">
                        <button class="btn btn-info" type="submit"> @lang('website.ForgotBtn') </button>
                    </div>
                </form>

                <div class="my-account text-center">

                    <a href="{{ app()->getLocale() == 'en' ? url('/en/login') : url('/login') }}">
                        @lang('website.ReturnLogin')
                    </a>
                </div>
            </div>
      </div>
    </div>
    <!--/.Content-->
  </div>
</section>

 @endsection

  @section('scripts')
<script src="{{asset('/assets/scripts/jquery.validate.js')}}"></script>

<script>

    $(function(){
        $("form[name='forgotForm']").validate({
            rules: {
                mobile: {
                    required: true,
                    number:true,
                    rangelength:[11,11],
                }
            },
            // Specify validation error messages
            messages: {
                mobile: {
                    required: "{{__('website.ContactMobileRequired')}}",
                    number: "{{__('website.Please_Enter_Avalid_Number')}}",
                    rangelength:"{{__('website.Mobile_Must_Be11_Digits')}}",
                }
            },

            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error)
            
            },
            highlight: function (element) {
                $(element).parent().addClass('error help-block');
                $(element).parent().find('label').addClass(' help-block');
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('error');
                $(element).parent().find('label').removeClass(' help-block');

            },
           
            submitHandler: function (form) {
                form.submit();
            }
        });
    })
  </script>
@endsection
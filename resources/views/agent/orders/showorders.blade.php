@extends('agent.layouts.app')
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<style>
    .jconfirm.jconfirm-modern .jconfirm-box div.jconfirm-title-c .jconfirm-icon-c {
    -webkit-transition: -webkit-transform .5s;
    transition: -webkit-transform .5s;
    transition: transform .5s;
    transition: transform .5s,-webkit-transform .5s;
    -webkit-transform: scale(0);
    transform: scale(0);
    display: block;
    margin-right: 0;
    margin-left: 0;
    margin-bottom: 10px;
    font-size: 69px;
    color: #9e1717;
}
.tab-content
{
  padding-top: 20px;
}
.nav-tabs li:first-child.active a
{
  color: #fff;background-color: #337ab7;
}
.nav-tabs li:last-child.active a
{
  color: #fff;background-color: #dd4b39;
}
</style>
@endsection
@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> Drivers <span style="color: #5810c3;" >{{! empty($driver) ? ' of Driver:: '.$driver->name.' - '.$driver->mobile : ''}} </span>
        </h3>
    </div>
@endsection

@section('content')

<div>

  <!-- Nav tabs -->
  <ul id="myTabs" class="nav nav-tabs nav-justified" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">current</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">finshed</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
      <div class="row">
          <div class="col-md-12 col-xs-12 col-sm-12">
              @if(count($orders) > 0)
              <table class="table table-condensed table-striped text-center">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Received Date</th>
                    <th>Order Type</th>
                    <th>Order Price</th>
                    <th>Receiver Name</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($orders as $index => $order)
                    @if($order->order_status == 1 && $order->accepted_orders_status == 0
                      ||
                        $order->order_status == 2 && $order->accepted_orders_status == 1)
                    <tr>
                      <td>{{$index+1}}</td>
                      <td>{{$order->received_date}}</td>
                      <td>{{$order->type}}</td>
                      <td>{{$order->order_price}}</td>
                      <td>{{$order->receiver_name}}</td>
                      <td>
                        @if($order->order_status == 0 )
                          <span class='badge badge-pill badge-black'>Pending</span>
                        @elseif($order->order_status == 1 && $order->accepted_orders_status == 0)
                          <span class='badge badge-pill badge-info'>Accepted</span>
                        @elseif($order->order_status == 2 && $order->accepted_orders_status == 1)
                          <span class='badge badge-pill badge-warning'>Received</span>
                        @elseif($order->order_status == 3 && $order->accepted_orders_status == 2)
                          <span class='badge badge-pill badge-success'>Delivered</span>
                        @elseif($order->order_status == 1)
                          <span class='badge badge-pill badge-danger'>Cancelled</span>
                        @elseif($order->order_status == 2)
                          <span class='badge badge-pill badge-danger'>Recalled</span>
                        @endif
                      </td>
                    </tr>
                    @else
                      @continue
                    @endif
                  @endforeach
                </tbody>
              </table>
              @else
                  <h3 class="text-center alert alert-warning">No Result Found !</h3>
              @endif
          </div>
      </div>
      <!-- end -->
    </div>
    <div role="tabpanel" class="tab-pane" id="profile">
      <table class="table table-condensed table-striped text-center">
        <thead>
          <tr>
            <th>#</th>
            <th>Received Date</th>
            <th>Order Type</th>
            <th>Order Price</th>
            <th>Receiver Name</th>
            <th>Status</th>
          </tr>
        </thead>
          <tbody>
            @foreach($orders as $index => $order)
              @if($order->order_status == 3 && $order->accepted_orders_status == 2
                ||
                  $order->order_status == 1 && $order->accepted_orders_status == 3)
              <tr>
                <td>{{$index+1}}</td>
                <td>{{$order->received_date}}</td>
                <td>{{$order->type}}</td>
                <td>{{$order->order_price}}</td>
                <td>{{$order->receiver_name}}</td>
                <td>
                  @if($order->order_status == 0 )
                    <span class='badge badge-pill badge-black'>Pending</span>
                  @elseif($order->order_status == 1 && $order->accepted_orders_status == 0)
                    <span class='badge badge-pill badge-warning'>Accepted</span>
                  @elseif($order->order_status == 2 && $order->accepted_orders_status == 1)
                    <span class='badge badge-pill badge-info'>Received</span>
                  @elseif($order->order_status == 3 && $order->accepted_orders_status == 2)
                    <span class='badge badge-pill badge-success'>Delivered</span>
                  @elseif($order->order_status == 1 && $order->accepted_orders_status == 3)
                    <span class='badge badge-pill badge-danger'>Cancelled</span>
                  @elseif($order->order_status == 2 && $order->accepted_orders_status == 4)
                    <span class='badge badge-pill badge-danger'>Recalled</span>
                  @endif
                </td>
              </tr>
              @else
              @continue
            @endif
          @endforeach
        </tbody>
    </table>
  </div>
</div>
</div>

@endsection

@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<script>
$('#myTabs a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})
$(function(){
	$(".ask_view").on('click',function(){
		var id = $(this).data('id');
		$.ajax({
    			url:'{!! url("/mngrAgent/ask_review_order/") !!}'+'/'+id ,
    			type:'get',

		    }).done(function(data) {
		        if(data == "ok")
		        {
			     $.alert({
                  title: 'Request Review Sent!',
                  icon: 'fa fa-smile-o',
                  theme: 'modern',
                  content: 'Your request sent to Ufelix Admin successfully!',
                  buttons: {
                      info: {
                          text: 'OK',
                          btnClass: 'btn-success',
                          action: function(){
                              window.location.href=window.location.href;
                          }
                      }
                  }
              });
	           }
		    });
	});

});

</script>

@endsection

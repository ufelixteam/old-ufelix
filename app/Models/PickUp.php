<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PickUp extends Model
{
    /*
        redirect :: for packup == 1 another order null or 0

    */
    protected $hidden = ['updated_at'];

    public function from_government()
    {
        return $this->belongsTo('App\Models\Governorate', 's_government_id');
    }

    public function to_government()
    {
        return $this->belongsTo('App\Models\Governorate', 'r_government_id');
    }


    public function driver()
    {
        return $this->belongsTo('App\Models\Driver', 'driver_id');
    }

    public function corporate()
    {
        return $this->belongsTo('App\Models\Corporate', 'corporate_id');
    }
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }

    public function agent()
    {
        return $this->belongsTo('App\Models\Agent', 'agent_id');
    }

    public function pickup_orders()
    {
        return $this->hasMany('App\Models\PickUpOrder', 'pick_up_id');
    }


    public function getStatusSpanAttribute()
    {
        if ($this->status == 0) {
            return "<span class='badge badge-pill label-info' style='background-color: #c466e3;''>" . __('backend.created') . "</span>";
        } else if ($this->status == 1) {
            return "<span class='badge badge-pill label-warning' style='background-color: #461e53'>" . __('backend.accepted') . "</span>";
        } else if ($this->status == 2) {
            return "<span class='badge badge-pill label-warning'>" . __('backend.collected') . "</span>";
        } else if ($this->status == 3) {
            return "<span class='badge badge-pill label-success'>" . __('backend.dropped') . "</span>";
        } else if ($this->status == 4) {
            return "<span class='badge badge-pill label-danger'>" . __('backend.cancelled') . " </span>";
        }
    }

    public function getTaskTypesAttribute()
    {
        $type = '';

        if ($this->is_pickup == 1) {
            $type .= "<span class='badge badge-pill label-info'>" . __('backend.pickup') . "</span> ";
        }
        if ($this->is_cash_collect == 1) {
            $type .= "<span class='badge badge-pill label-success'>" . __('backend.cache') . "</span> ";
        }
        if ($this->is_recall_orders == 1) {
            $type .= "<span class='badge badge-pill label-danger'>" . __('backend.recall') . "</span> ";
        }

        return $type;
    }

    public function getConfirmTaskTypesAttribute()
    {
        $type = '';

        if ($this->confirm_pickup == 1) {
            $type .= "<span class='badge badge-pill label-info'>" . __('backend.pickup') . "</span> ";
        }
        if ($this->confirm_cash_collect == 1) {
            $type .= "<span class='badge badge-pill label-success'>" . __('backend.cache') . "</span> ";
        }
        if ($this->confirm_recall_orders == 1) {
            $type .= "<span class='badge badge-pill label-danger'>" . __('backend.recall') . "</span> ";
        }

        return $type;
    }

    public function getConfirmTaskCountAttribute()
    {
        $type = '';

        if ($this->confirm_pickup == 1) {
            $type .= "<span class='badge badge-pill label-info'>" . __('backend.pickup_orders') . ': ' . $this->pickup_orders . "</span> ";
        }
        if ($this->confirm_cash_collect == 1) {
            $type .= "<span class='badge badge-pill label-success'>" . __('backend.money') . ': ' . $this->cash_money . "</span> ";
        }
        if ($this->confirm_recall_orders == 1) {
            $type .= "<span class='badge badge-pill label-danger'>" . __('backend.recall_orders') . ': ' . $this->recall_orders . "</span> ";
        }

        return $type;
    }

    public function getTotalOrderNoAttribute()
    {
        return PickUpOrder::where('pick_up_id', $this->id)->count();
    }

    public function getTotalAttribute()
    {
        return PickUpOrder::where('pick_up_id', $this->id)->sum('delivery_price') + $this->delivery_price;
    }

    public function refund_collections()
    {
        return $this->belongsToMany('App\Models\RefundCollection', 'task_refund_collections', 'task_id', 'refund_collection_id');

    }


}

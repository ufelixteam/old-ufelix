@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.governorates')}} - {{__('backend.edit')}}</title>
    <link href="{{asset('assets/scripts/taginput/tagsinput.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i>
            {{__('backend.Edit_Governorate')}} -
            @if(session()->has('lang') == 'ar')
                {{$governorate->name_ar}}
            @else
                {{$governorate->name_en}}
            @endif
        </h3>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <form action="{{ route('mngrAdmin.governorates.update', $governorate->id) }}" method="POST"
                  name='validateForm'>
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('name_en')) has-error @endif">
                        <label for="governorate-field">{{__('backend.governorate_en')}}: </label>
                        <input type="text" id="name_en-field" name="name_en" class="form-control"
                               value="{{ is_null(old("name_en")) ? $governorate->name_en : old("name_en") }}"/>
                        @if($errors->has("name_en"))
                            <span class="help-block">{{ $errors->first("name_en") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('name_ar')) has-error @endif">
                        <label for="governorate-field">{{__('backend.governorate_ar')}}: </label>
                        <input type="text" id="name_ar-field" name="name_ar" class="form-control"
                               value="{{ is_null(old("name_ar")) ? $governorate->name_ar : old("name_ar") }}"/>
                        @if($errors->has("name_ar"))
                            <span class="help-block">{{ $errors->first("name_ar") }}</span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('country_id')) has-error @endif">
                        <label for="country_id-field">{{__('backend.country')}}: </label>
                        <select id="country_id-field" name="country_id" class="form-control">
                            @if(! empty($list_countries) )
                                @foreach($list_countries as $country)
                                    <option
                                        value="{{$country->id}}" {{$governorate->country_id == $country->id ||  old("country_id") == $country->id ? 'selected' : ''}} >{{$country->name}}</option>
                                @endforeach
                            @endif
                        </select>
                        @if($errors->has("country_id"))
                            <span class="help-block">{{ $errors->first("country_id") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('status')) has-error @endif">
                        <label for="status-field">{{__('backend.status')}}: </label>
                        <select id="status-field" name="status" class="form-control">
                            <option
                                value="0" {{ old("status") == "0" ||  $governorate->status == "0" ? 'selected' : '' }}>{{__('backend.no')}}</option>
                            <option
                                value="1" {{ old("status") == "1" ||  $governorate->status == "1" ? 'selected' : '' }}>{{__('backend.yes')}}</option>
                        </select>
                        @if($errors->has("status"))
                            <span class="help-block">{{ $errors->first("status") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('area_id')) has-error @endif">
                        <label for="area_id-field">{{__('backend.area')}}: </label>
                        <select id="area_id-field" name="area_id" class="form-control">
                            @if(! empty($areas) )
                                @foreach($areas as $area)
                                    <option
                                        value="{{$area->id}}" {{$governorate->area_id == $area->id ||  old("area_id") == $area->id ? 'selected' : ''}} >{{$area->name_en}}</option>
                                @endforeach
                            @endif
                        </select>
                        @if($errors->has("area_id"))
                            <span class="help-block">{{ $errors->first("area_id") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6">
                        <label for="status-field">{{__('backend.keywords')}}: </label>
                        <input data-role="tagsinput" type="text" value="{{$governorate->keywords->pluck('keyword')->implode(',')}}" class="form-control"
                               name="keywords">
                    </div>
                </div>
                <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
                    <button type="submit" class="btn btn-primary">{{__('backend.save_edits')}}</button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.cities.index') }}"><i
                            class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('assets/scripts/taginput/tagsinput.js')}}"></script>
    <script type="text/javascript">
        $(function () {
            $("form[name='validateForm']").validate({
                // Specify validation rules
                rules: {
                    name_en: {
                        required: true,
                    },
                    name_ar: {
                        required: true,
                    },
                    country_id: {
                        required: true,
                    },
                    status: {
                        required: true,
                    },
                },
                // Specify validation error messages
                messages: {
                    name_en: {
                        required: "{{__('backend.Please_Enter_Governorate_English_Name')}}",
                    },
                    name_ar: {
                        required: "{{__('backend.Please_Enter_Governorate_Arabic_Name')}}",
                    },
                    country_id: {
                        required: "{{__('backend.Please_Enter_Country_Name')}}",
                    },
                    status: {
                        required: "{{__('backend.Please_Enter_Governorate_Status')}}",
                    },
                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                },
                highlight: function (element) {
                    $(element).parent().addClass('error')
                },
                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                }
            });
        })
    </script>
@endsection

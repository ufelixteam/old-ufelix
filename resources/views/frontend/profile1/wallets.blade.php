@extends('frontend.profile.layouts.app')
@section('title')
@lang('website.wallets')
@endsection
@section('content')

<div class="row">
    <div class="col-lg-12 col-md-12">
       <div class="card card-default">
          <div class="card-header">
             <h4 class="card-title m-b-0">@lang('website.wallets') </h4>
          </div>
          <div class="container-fluid">
             <div class="row">
             	<div class="card wallet">
                	<div class="icon-wallet"></div>
                    <span>@if(!empty($wallet->value))  {{$wallet->value}} @lang('website.LE') @else 0 @lang('website.LE') @endif</span>
                </div>
             </div>
          </div>
       </div>
    </div>
</div>

@endsection
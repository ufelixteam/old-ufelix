@extends('frontend.profile.layouts.app')
@section('title') @lang('website.invoices') @endsection


@section('content')
    <div class="row">
        <div class="col-12">
            <div class="table-responsive m-t-40">
                <h1 class="title">
                    {{__('backend.invoices')}}
                </h1>

                @if($invoices->count())
                    <table id="theTable" class="table table-condensed table-striped">
                        <thead>
                        <tr>
                            <th scope="col"> @lang('website.ID')</th>
                            <th scope="col"> @lang('website.StartDate')</th>
                            <th scope="col"> @lang('website.EndDate')</th>
                            <th scope="col"> @lang('website.Status')</th>
                            <th scope="col"> @lang('website.Actions')</th>
                        </tr>
                        </thead>
                        <tbody class="text-center">
                        @foreach($invoices as $invoice)
                            <tr>
                                <td scope="row">{{$invoice->id}}</td>
                                <td>{{$invoice->start_date}}</td>
                                <td>{{$invoice->end_date}}</td>
                                <td>
                                    {!! $invoice->invoice_type_span !!}
                                </td>
                                <td>
                                    <a href="{{ app()->getLocale() == 'en' ? url('/en/customer/account/invoices' ,$invoice->id) : url('/customer/account/invoices' ,$invoice->id)}}"
                                       class="text-inverse p-r-10" data-toggle="tooltip" title=""
                                       data-original-title="show"
                                       aria-describedby="tooltip468946">
                                        <i class="icon-eye"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    {!! $invoices->appends($_GET)->links() !!}

                @else
                    <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
                @endif
            </div>
        </div>
    </div>
@endsection

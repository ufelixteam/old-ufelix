@extends('backend.layouts.app')

@section('css')
    <title>{{__('backend.documentations')}} - {{__('backend.edit')}}</title>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <form action="{{ url('mngrAdmin/docs/update', $data->id) }}" method="POST" name='validateForm'>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group col-md-4 col-sm-6 @if($errors->has('parent_id')) has-error @endif">
                    <label for="weight-field">{{__('backend.type')}}</label>
                    <select name="parent_id" class="form-control">
                        <option value="">{{__('backend.choose_docs_type')}}</option>
                        @foreach(App\Models\DocsType::all() as $value)
                            <option {{ $data->parent_id == $value->id ? 'selected' : '' }} value="{{ $value->id }}">{{ $value->title }}</option>
                        @endforeach
                    </select>
                    @if($errors->has("parent_id"))
                        <span class="help-block">{{ $errors->first("parent_id") }}</span>
                    @endif
                </div>

                <div class="form-group col-md-4 col-sm-6 col-xs-6 @if($errors->has('title')) has-error @endif">
                    <label for="title-field">{{__('backend.title')}}</label>
                    <input type="text" id="title-field" name="title" class="form-control" value="{{ $data->title }}"/>
                    @if($errors->has("title"))
                        <span class="help-block">{{ $errors->first("title") }}</span>
                    @endif
                </div>

                <div class="form-group col-md-4 col-sm-6 col-xs-6 @if($errors->has('slug')) has-error @endif">
                    <label for="slug-field">{{__('backend.slug')}}</label>
                    <input type="text" id="slug-field" name="slug" class="form-control" value="{{ $data->slug }}" readonly/>
                    @if($errors->has("slug"))
                        <span class="help-block">{{ $errors->first("slug") }}</span>
                    @endif
                </div>

                <div class="form-group col-md-12 col-sm-12 col-xs-12 @if($errors->has('description')) has-error @endif">
                    <label for="message-field">{{__('backend.description')}}</label>
                    <textarea class="form-control summernote" name="description" required>{{ $data->description }}</textarea>
                    @if($errors->has("description"))
                        <span class="help-block">{{ $errors->first("description") }}</span>
                    @endif
                </div>

                <div class="well well-sm col-md-12 col-sm-12 col-xs-12">
                    <button type="submit" class="btn btn-primary">{{__('backend.save_edits')}}</button>
                    <a class="btn btn-link pull-right" href="{{ url('mngrAdmin/docs/index') }}">
                        <i class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}
                    </a>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.summernote').summernote({
                tabsize: 2,
                height: 300,
                maximumImageFileSize: 10048576
            });

            $("form[name='validateForm']").validate({
              // Specify validation rules
              rules: {
                parent_id: {
                  required: true,
                },
                title: {
                  required: true,
                },
                slug: {
                  required: true,
                },
                description: {
                  required: true,
                },
              },
              // Specify validation error messages
              messages: {
                parent_id: {
                  required: "{{__('backend.Please_Choose_Documentation_Type')}}",
                },
                title: {
                  required: "{{__('backend.Please_Enter_Documentation_Title')}}",
                },
                slug: {
                  required: "{{__('backend.Please_Enter_Documentation_Slug')}}",
                },
                description: {
                  required: "{{__('backend.Please_Enter_Documentation_Description')}}",
                },
              },

              errorPlacement: function(error, element) {
                  $(element).parents('.form-group').append(error)
              },

              highlight: function (element) {
                  $(element).parent().addClass('error')
              },

              unhighlight: function (element) {
                  $(element).parent().removeClass('error')
              },

              // Make sure the form is submitted to the destination defined
              // in the "action" attribute of the form when valid
              submitHandler: function(form) {
                  form.submit();
              }
            });

        });
    </script>
@endsection

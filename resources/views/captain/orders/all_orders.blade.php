@extends('captain.layouts.app')
@section('css')
    <title>{{__('captain.the_orders')}}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <style type="text/css">
        @media only screen and (max-width: 580px) {
            .hide-td {
                display: none;
            }
        }

        .box-body {
            min-height: 100px;
        }

        .stylish-input-group .input-group-addon {
            background: white !important;
        }

        .stylish-input-group .form-control {
            border-right: 0;
            box-shadow: 0 0 0;
            border-color: #ccc;
        }

        .stylish-input-group button {
            border: 0;
            background: transparent;
        }

        .toggle.ios, .toggle-on.ios, .toggle-off.ios {
            border-radius: 20px;
        }

        .toggle.ios .toggle-handle {
            border-radius: 20px;
        }
    </style>
@endsection

@section('header')
    <div class="page-header clearfix">
        <h3 style="display: inline-block; float: left">
            <i class="glyphicon glyphicon-align-justify"></i>
            Orders
        </h3>

        <a href="{{url('captainDashboard/reports/captain_report') . '?export&status=' . app('request')->input('status') . '&from_date='. app('request')->input('fromDate') . '&to_date='. app('request')->input('toDate')}}"
           class="btn btn-info" style="float: right" id="download_excel">
            {{__('captain.download_excel')}}
        </a>

    </div>
@endsection
@section('content')
    @if (session('success'))
        <div class="alert alert-success alert-block text-center">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ session('success') }}</strong>
        </div>
    @endif
    @if (session('warning'))
        <div class="alert alert-warning alert-block text-center">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ session('warning') }}</strong>
        </div>
    @endif

    @include('error')

    <div class="alert alert-danger" hidden>
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong id="errorId"></strong>
    </div>

    <form action="{{URL::asset('/captainDashboard/orders/all')}}" method="get">
        <div class="row" style="margin-bottom:15px;" id="orders">
            <div class="col-md-12">


                <div class="col-md-3 col-sm-3">
                    <input type="text" class="form-control" id="search-field" name="search"
                           value="{{ app('request')->input('search')}}"
                           placeholder="{{__('captain.search')}} ">
                </div>
{{--                <div class="group-control col-md-3 col-sm-3">--}}
{{--                    <div class='input-group date' id='datepickerFilter'>--}}
{{--                        <input type="text" id="date" name="date" class="form-control"--}}
{{--                               value="{{ app('request')->input('date')}}"/>--}}
{{--                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>--}}
{{--                    </div>--}}
{{--                </div>--}}

                <div class="col-md-3 col-sm-3 filter">
                    <div class='input-group date ' id="datepicker1">
                        <input type="text" id="fromDate" name="fromDate" class="form-control"
                               value="{{app('request')->fromDate}}"
                               placeholder="{{__('captain.From_Date')}}" autocomplete="off"/>
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 filter">
                    <div class='input-group date ' id="datepickers">
                        <input type="text" id="toDate" name="toDate" class="form-control"
                               value="{{app('request')->toDate}}"
                               placeholder="{{__('captain.To_Date')}}" autocomplete="off"/>
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-3">
                    <select id="status" name="status" class="form-control">
                        <option value="" data-display="Select">{{__('captain.status')}}</option>
{{--                        <option value="1"--}}
{{--                                @if(app('request')->input('status') == 1) selected @endif>{{__('captain.accepted')}} </option>--}}
                        <option value="2"
                                @if(app('request')->input('status') == 2) selected @endif>{{__('captain.received')}} </option>
                        <option value="3"
                                @if(app('request')->input('status') == 3) selected @endif>{{__('captain.delivered')}} </option>
{{--                        <option value="4"--}}
{{--                                @if(app('request')->input('status') == 4) selected @endif>{{__('captain.cancelled')}} </option>--}}
                        <option value="5"
                                @if(app('request')->input('status') == 5) selected @endif>{{__('captain.recalled')}} </option>
                        <option value="8"
                                @if(app('request')->input('status') == 8) selected @endif>{{__('captain.rejected')}} </option>
                    </select>
                </div>

                <div class="col-md-3 col-sm-3" style="clear: both; margin-top: 15px">
                    <button type="submit"
                            class="btn btn-primary btn-block"> {{__('captain.filter_orders')}} </button>
                </div>

            </div>
        </div>

    </form>
    <div class='list'>
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                @if(! empty($orders) && count($orders) > 0)
                    <form method="post" id="myform" action="#">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <div class="table-responsive">
                            <table id="theTable" class="table table-condensed table-striped text-center"
                                   style="min-height: 200px;">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{__('captain.captain')}}</th>
                                    <th>{{__('captain.sender_name')}}</th>
                                    <th>{{__('captain.receiver_name')}}</th>
                                    <th>{{__('captain.address')}}</th>
                                    <th>{{__('captain.to')}}</th>
                                    <th style="font-weight: bold; color: #333;">{{__('captain.order_number')}}</th>
                                    <th>{{__('captain.total_cost')}}</th>
                                    <th>{{__('captain.collected_cost')}}</th>
                                    <th>{{__('captain.status')}}</th>
{{--                                    <th style="width: 150px;">--}}
{{--                                        <button type="button" id="pdf-submit"--}}
{{--                                                class="btn btn-warning btn-pdf btn-xs ">{{__('captain.pdf')}}</button>--}}
{{--                                    </th>--}}

{{--                                    <th></th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $i => $order)
                                    <tr>
                                        <td>{{($i+1)}}</td>
                                        <td>{{$order->driver->name}}</td>
                                        <td>
                                            {{$order->sender_name}}
                                            @if($order->customer && $order->customer->Corporate)
                                                <strong style="color: #0c81bc"> {{$order->customer->Corporate->name}}</strong>
                                            @endif
                                        </td>
                                        <td>{{$order->receiver_name}}</td>
                                        <td>{{$order->receiver_address}}</td>

                                        <td>
                                            @if(! empty($order->to_government))
                                                {{$order->to_government->name_en}}
                                            @endif
                                        </td>
                                        <td style="font-weight: bold; color: #a43;">{{$order->order_number}}</td>
                                        <td>{{$order->total_price}}</td>
                                        <td>{{$order->collected_cost}}</td>
                                        <td>{!! $order->status_details !!}</td>

{{--                                        <td class="selectpdf" id="selectpdf">--}}
{{--                                            <input type="checkbox" name="select[]" value="{{$order->id}}"--}}
{{--                                                   class="selectOrder"--}}
{{--                                                   data-value="{{$order->id}}"/>--}}

{{--                                        </td>--}}


{{--                                        <td>--}}
{{--                                            <a href="{{ route('captainDashboard.orders.show', $order->id ) }}"><i--}}
{{--                                                    class="fa fa-eye"></i> {{__('captain.view')}}</a>--}}
{{--                                        </td>--}}
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </form>
                    {!! $orders->appends($_GET)->links() !!}
                @else
                    <h3 class="text-center alert alert-warning">No Result Found !</h3>
                @endif
            </div>
        </div>
        <div id="js-form" class="js-form">
        </div>

    </div>

@endsection
@section('scripts')
    {{--    @include('captain.orders.js-search')--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script>
        $(function () {
            /*print*/
            $('.toggle').bootstrapToggle();

            $('body').on('change', '.selectOrder', function (e) {
                if ($('input[name="select[]"]').is(':checked')) {
                    $("#delay_orders").removeClass('disabled');
                    $("#deliver_orders").removeClass('disabled');
                    $("#receive_orders").removeClass('disabled');
                } else {
                    $("#delay_orders").addClass('disabled');
                    $("#deliver_orders").addClass('disabled');
                    $("#receive_orders").addClass('disabled');
                }
            });

            $('body').on('click', '#saveDelay', function (e) {
                if ($("#delay_date").val()) {
                    let ids = [];
                    $(".selectOrder:checked").each(function () {
                        ids.push($(this).val());
                    });
                    $.ajax({
                        url: "{{ url('/captainDashboard/delay_orders')}}",
                        type: 'post',
                        data: {
                            ids: ids,
                            delay_date: $("#delay_date").val(),
                            delay_comment: $("#delay_comment").val(),
                            '_token': "{{csrf_token()}}"
                        },
                        success: function (data) {
                            // if (data['status'] != 'false') {
                            location.reload();
                            // }
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }
            });

            $('body').on('click', '#deliver_orders', function (e) {

                let ids = [];

                $(".selectpdf input:checked").each(function () {
                    ids.push($(this).val());

                });

                $.confirm({
                    title: '',
                    theme: 'modern',
                    class: 'danger',
                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-meh-o text-success"></i></span><span class="jconfirm-title">{{__("captain.Confirm")}}</span></div><div style="font-weight:bold;"><p>{{__("captain.delivery_orders_confirm")}}</p></div>',
                    buttons: {
                        confirm: {
                            text: '{{__("captain.Ok")}}',
                            btnClass: 'btn-danger',
                            action: function () {
                                let ids = [];
                                $(".selectOrder:checked").each(function () {
                                    ids.push($(this).val());
                                });
                                $.ajax({
                                    url: "{{ url('/captainDashboard/deliver_orders')}}",
                                    type: 'post',
                                    data: {ids: ids, '_token': "{{csrf_token()}}"},
                                    success: function (data) {
                                        // if (data['status'] != 'false') {
                                        location.reload();
                                        // }
                                    },
                                    error: function (data) {
                                        console.log('Error:', data);
                                    }
                                });
                            }
                        },
                        cancel: {
                            text: '{{__("captain.Cancel")}}',
                            action: function () {
                            }
                        }
                    }
                });

                $('body').on('click', '#receive_orders', function (e) {

                    let ids = [];

                    $(".selectpdf input:checked").each(function () {
                        ids.push($(this).val());

                    });

                    $.confirm({
                        title: '',
                        theme: 'modern',
                        class: 'danger',
                        content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-meh-o text-success"></i></span><span class="jconfirm-title">{{__("captain.Confirm")}}</span></div><div style="font-weight:bold;"><p>{{__("captain.receive_orders_confirm")}}</p></div>',
                        buttons: {
                            confirm: {
                                text: '{{__("captain.Ok")}}',
                                btnClass: 'btn-danger',
                                action: function () {
                                    let ids = [];
                                    $(".selectOrder:checked").each(function () {
                                        ids.push($(this).val());
                                    });
                                    $.ajax({
                                        url: "{{ url('/captainDashboard/receive_orders')}}",
                                        type: 'post',
                                        data: {ids: ids, '_token': "{{csrf_token()}}"},
                                        success: function (data) {
                                            // if (data['status'] != 'false') {
                                            location.reload();
                                            // }
                                        },
                                        error: function (data) {
                                            console.log('Error:', data);
                                        }
                                    });
                                }
                            },
                            cancel: {
                                text: '{{__("captain.Cancel")}}',
                                action: function () {
                                }
                            }
                        }
                    });

                });

                $('#delay_date').datepicker({
                    format: 'yyyy-mm-dd',
                    autoclose: true,
                    language: 'en',
                    todayHighlight: true,
                    startDate: new Date()
                });

                $('body').on('click', '.printdata', function (e) {

                    var url = "{{URL::asset('/captainDashboard/print_pdf/')}}/" + $(this).attr('data-id');
                    $('<form action="' + url + '" method="get" target="_blank"></form>').appendTo('#orders').submit();
                });

                $('body').on('click', '.delete-order', function (e) {

                    var id = $(this).attr('data-id');
                    $.confirm({
                        title: '',
                        theme: 'modern',
                        class: 'danger',
                        content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Confirm Delete</span></div><div style="font-weight:bold;"><p>Are You sure To Delete This Order ?</p></div>',
                        buttons: {
                            confirm: {
                                text: 'Delete Order',
                                btnClass: 'btn-danger',
                                action: function () {
                                    $.ajax({
                                        url: "{{ url('/captainDashboard/orders/')}}" + "/" + id,
                                        type: 'DELETE',
                                        data: {'_token': "{{csrf_token()}}"},
                                        success: function (data) {
                                            jQuery('.alert-danger').html('');
                                            jQuery('.alert-danger').show();
                                            if (data != 'false') {
                                                jQuery('.alert-danger').append('<p>' + data + '</p>');
                                                setTimeout(function () {
                                                    location.reload();
                                                }, 2000);
                                            } else {
                                                jQuery('.alert-danger').append('<p>Something Error</p>');
                                            }
                                            window.scrollTo({top: 0, behavior: 'smooth'});
                                        },
                                        error: function (data) {
                                            console.log('Error:', data);
                                        }
                                    });
                                }
                            },
                            cancel: {
                                text: 'Cancel',
                                action: function () {
                                }
                            }
                        }
                    });
                });

                $('body').on('click', '#packup', function (e) {

                    let ids = [];
                    $(".selectpack:checked").each(function () {
                        ids.push($(this).val());
                    });

                    let collection_id = $('.selectpackup  input:checked').attr('data-value');

                    let order_ids = $('.selectpackup  input:checked').val();

                    $.confirm({
                        title: '',
                        theme: 'modern',
                        class: 'warning',
                        content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-simle-o  text-warning"></i></span><span class="jconfirm-title">Confirm Pick Up</span></div><div style="font-weight:bold;"><p>Are You sure To Pick Up This Orders ?</p></div>',
                        buttons: {
                            confirm: {
                                text: 'Pickup Order',
                                btnClass: 'btn-warning',
                                action: function () {
                                    $.ajax({
                                        url: "{{ url('/captainDashboard/pickup_orders/')}}",
                                        type: 'POST',
                                        data: {
                                            '_token': "{{csrf_token()}}",
                                            'ids': ids,
                                            'collection_id': collection_id
                                        },
                                        success: function (data) {
                                            if (data['status'] != 'false') {
                                                $(".alert").hide();

                                                $('#orderpickup').val(data['orders']);
                                                $('#from-pick').val(data['frompick']);
                                                $('#delivery_price-field').val(data['cost']);
                                                $('#bonous-field').val(data['bonous']);

                                                $('#pickupModal').modal('show');

                                            } else {
                                                $('#errorId').html(data['message']);
                                                $(".alert").show();

                                                $('#pickupModal').modal('hide');

                                            }
                                            window.scrollTo({top: 0, behavior: 'smooth'});
                                        },
                                        error: function (data) {
                                            console.log('Error:', data);
                                        }
                                    });
                                }
                            },
                            cancel: {
                                text: 'Cancel',
                                action: function () {
                                }
                            }
                        }
                    });
                })

            });

            $('body').on('click', '#pdf-submit', function (e) {

                let ids = [];

                $(".selectpdf input:checked").each(function () {
                    ids.push($(this).val());

                });
                if (ids.length > 0) {
                    $('<form action="{{ url("/captainDashboard/print_police/")}}" method="post" ><input value="' + ids + ' " name="ids"  />" {{csrf_field()}}"</form>').appendTo('body').submit();

                } else {

                    $.confirm({
                        title: '',
                        theme: 'modern',
                        content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Print PDF</span></div><div style="font-weight:bold;"><p>{{__("captain.Must_Choose_Orders_To_Print")}}</p></div>',
                        type: 'red',
                    });
                }

            });
            $('body').on('click', '#action_back', function (e) {
                $("#exampleModal").modal("hide");

                let ids = [];
                let action_status = $('#action_status').val();

                $(".selectBack input:checked").each(function () {
                    ids.push($(this).val());

                });
                if (ids.length > 0) {

                    $.ajax({
                        type: 'post',
                        url: '{{ url("/captainDashboard/orders/action_back")}}',
                        data: {'ids': ids, 'action_status': action_status, _token: "{{csrf_token()}}"},
                        success: function (data) {
                            location.reload();
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }

                    });
                    // $('<form action="{{ url("/captainDashboard/orders/action_back/")}}" method="post" ><input value="" name="ids"  /><input value="'+action_status+'"  name="action_status" />" {{csrf_field()}}"</form>').appendTo('body').submit();

                } else {

                    $.confirm({
                        title: '',
                        theme: 'modern',
                        content: '<div class="jconfirm-title-c jconfirm-hand">' +
                            '<span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span>' +
                            '<span class="jconfirm-title">{{__('captain.action_back')}}</span></div><div style="font-weight:bold;">' +
                            '<p>{{__("captain.Must_Choose_Orders_To_Action_Back")}}</p></div>',
                        type: 'red',
                    });
                }

            });

            $('body').on('change', '.warehouse_dropoff', function (e) {
                if ($(this).is(':checked')) {
                    var self = $(this).attr('data-id');
                    $(this).bootstrapToggle('disable');
                    $("#client_dropoff_" + self).bootstrapToggle('enable');
                    $.ajax({
                        url: "{{ url('/captainDashboard/orders/warehouse_dropoff/')}}",
                        type: 'POST',
                        data: {'_token': "{{csrf_token()}}", ids: [self]},
                        success: function (data) {

                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }
            });

            $('body').on('change', '.head_office', function (e) {
                var self = $(this).attr('data-id');
                var is_checked = $(this).is(':checked');
                $.ajax({
                    url: "{{ url('/captainDashboard/orders/head_office/')}}",
                    type: 'POST',
                    data: {'_token': "{{csrf_token()}}", ids: [self], head_office: is_checked},
                    success: function (data) {

                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });

            });

            $('.client_dropoff').on('change', function (e) {
                if ($(this).is(':checked')) {
                    var self = $(this).attr('data-id');
                    $(this).bootstrapToggle('disable');
                    $.ajax({
                        url: "{{ url('/captainDashboard/orders/client_dropoff/')}}",
                        type: 'POST',
                        data: {'_token': "{{csrf_token()}}", ids: [self]},
                        success: function (data) {

                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });

                }
            });

            $("#s_government_id").on('change', function () {
                $('#s_state_id').html('<option value="" >Sender City</option>');
                if ($(this).val()) {
                    $.ajax({
                        url: "{{ url('/get_cities')}}" + "/" + $(this).val(),
                        type: 'get',
                        data: {},
                        success: function (data) {
                            $.each(data, function (i, content) {
                                $('#s_state_id').append($("<option></option>").attr("value", content.id).text(content.city_name));
                            });
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }

            });


            $("#r_government_id").on('change', function () {
                $('#r_state_id').html('<option value="" >Receiver City</option>');
                if ($(this).val()) {
                    $.ajax({
                        url: "{{ url('/get_cities')}}" + "/" + $(this).val(),
                        type: 'get',
                        data: {},
                        success: function (data) {
                            $.each(data, function (i, content) {
                                $('#r_state_id').append($("<option></option>").attr("value", content.id).text(content.city_name));
                            });
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }

            });

            $("#r_government_id").val("{{app('request')->input('r_government_id')}}").change();
            $("#s_government_id").val("{{app('request')->input('s_government_id')}}").change();


        });

        // Select All Orders function For Print
        function do_this() {
            var checkboxes = document.getElementsByName('select[]');
            var button = document.getElementById('toggle');

            if (button.value == 'select') {
                for (var i in checkboxes) {
                    checkboxes[i].checked = 'FALSE';
                }
                button.value = 'deselect'
            } else {
                for (var i in checkboxes) {
                    checkboxes[i].checked = '';
                }
                button.value = 'select';
            }
        }

    </script>
@endsection

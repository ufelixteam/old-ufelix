@extends('agent.layouts.app')
@section('css')

@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i> Edit Drivers #{{$driver->id}}</h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <form action="{{ route('mngrAgent.drivers.update', $driver->id) }}" method="POST"
                  enctype="multipart/form-data" name='driver_create'>
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="agent_id" value="{{ \Auth::guard('agent')->user()->agent_id }}">


                <div class="form-group col-md-6 col-sm-6 @if($errors->has('name')) has-error @endif">
                    <label for="name-field">Name</label>
                    <input type="text" id="name-field" name="name" class="form-control"
                           value="{{ is_null(old("name")) ? $driver->name : old("name") }}"/>
                    @if($errors->has("name"))
                        <span class="help-block">{{ $errors->first("name") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-6 col-sm-6 @if($errors->has('mobile')) has-error @endif">
                    <label for="mobile-field">Mobile</label>
                    <input type="text" id="mobile-field" name="mobile" class="form-control"
                           value="{{ is_null(old("mobile")) ? $driver->mobile : old("mobile") }}"/>
                    @if($errors->has("mobile"))
                        <span class="help-block">{{ $errors->first("mobile") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-6 col-sm-6 @if($errors->has('email')) has-error @endif">
                    <label for="email-field">E-mail</label>
                    <input type="email" id="email-field" name="email" class="form-control"
                           value="{{ is_null(old("email")) ? $driver->email : old("email") }}"/>
                    @if($errors->has("email"))
                        <span class="help-block">{{ $errors->first("email") }}</span>
                    @endif
                </div>

                <div class="form-group col-md-6 col-sm-6 @if($errors->has('phone')) has-error @endif">
                    <label for="phone-field">Phone</label>
                    <input type="text" id="phone-field" name="phone" class="form-control"
                           value="{{ is_null(old("phone")) ? $driver->phone : old("phone") }}"/>
                    @if($errors->has("phone"))
                        <span class="help-block">{{ $errors->first("phone") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-6 col-sm-6 @if($errors->has('password')) has-error @endif">
                    <label for="password-field">Password</label>
                    <input type="password" id="password-field" name="password" class="form-control" value=""/>
                    @if($errors->has("password"))
                        <span class="help-block">{{ $errors->first("password") }}</span>
                    @endif
                </div>
              
               
                <div class="form-group col-md-6 col-sm-6 @if($errors->has('latitude')) has-error @endif">
                    <label for="latitude-field">Latitude</label>
                    <input type="text" id="latitude-field" name="latitude" class="form-control"
                           value="{{ is_null(old("latitude")) ? $driver->latitude : old("latitude") }}"/>
                    @if($errors->has("latitude"))
                        <span class="help-block">{{ $errors->first("latitude") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-6 col-sm-6 @if($errors->has('longitude')) has-error @endif">
                    <label for="longitude-field">longitude</label>
                    <input type="text" id="longitude-field" name="longitude" class="form-control"
                           value="{{ is_null(old("longitude")) ? $driver->longitude : old("longitude") }}"/>
                    @if($errors->has("longitude"))
                        <span class="help-block">{{ $errors->first("longitude") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-6 col-sm-6 @if($errors->has('driving_licence_number')) has-error @endif">
                    <label for="driving_licence_number-field">Driving_licence_number</label>
                    <input type="text" id="driving_licence_number-field" name="driving_licence_number"
                           class="form-control"
                           value="{{ is_null(old("driving_licence_number")) ? $driver->driving_licence_number : old("driving_licence_number") }}"/>
                    @if($errors->has("driving_licence_number"))
                        <span class="help-block">{{ $errors->first("driving_licence_number") }}</span>
                    @endif
                </div>

                <div class="form-group col-md-6 col-sm-6 @if($errors->has('driving_licence_expired_date')) has-error @endif">
                    <label for="driving_licence_expired_date-field">Driving_licence_expired_date</label>
                    <div class='input-group date datepicker2' >
                    <input type="text" id="driving_licence_expired_date-field" name="driving_licence_expired_date"
                           class="form-control"
                           value="{{ is_null(old("driving_licence_expired_date")) ? $driver->driving_licence_expired_date : old("driving_licence_expired_date") }}"/>
                    @if($errors->has("driving_licence_expired_date"))
                        <span class="help-block">{{ $errors->first("driving_licence_expired_date") }}</span>
                    @endif
                </div></div>
                <div class="form-group col-md-6 col-sm-6 @if($errors->has('national_id_number')) has-error @endif">
                    <label for="national_id_number-field">National_id_number</label>
                    <input type="text" id="national_id_number-field" name="national_id_number" class="form-control"
                           value="{{ is_null(old("national_id_number")) ? $driver->national_id_number : old("national_id_number") }}"/>
                    @if($errors->has("national_id_number"))
                        <span class="help-block">{{ $errors->first("national_id_number") }}</span>
                    @endif
                </div>

                  <div class="form-group col-md-6 col-sm-6 @if($errors->has('national_id_expired_date')) has-error @endif">
                    <label for="national_id_expired_date-field">National_id_expired_date</label>
                    <div class='input-group date datepicker2' >
                    <input type="text" id="national_id_expired_date-field" name="national_id_expired_date"
                           class="form-control"
                           value="{{ is_null(old("national_id_expired_date")) ? $driver->national_id_expired_date : old("national_id_expired_date") }}"/>
                    @if($errors->has("national_id_expired_date"))
                        <span class="help-block">{{ $errors->first("national_id_expired_date") }}</span>
                    @endif
                    </div>
                </div>

                 <div class="form-group col-md-3 col-sm-3 @if($errors->has('status')) has-error @endif">
                    <label for="status-field">Status</label>

                    <select id="status-field" name="status" class="form-control" value="{{ old("status") }}">
                        <option value="0" {{ old("status") == "0" || $driver->status == "0" ? 'selected' : '' }}>Offline</option>
                        <option value="1" {{ old("status") == "1" || $driver->status == "1" ? 'selected' : '' }}>Online</option>
                    </select>
                    @if($errors->has("status"))
                        <span class="help-block">{{ $errors->first("status") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-3 col-sm-3 @if($errors->has('is_active')) has-error @endif">
                    <label for="is_active-field">Is Active (verified/not verified)</label>
                    <select id="is_active-field" name="is_active" class="form-control" value="{{ old("is_active") }}">
                        <option value="0" {{ old("is_active") == "0" || $driver->is_active == "0" ? 'selected' : '' }}>No</option>
                        <option value="1" {{ old("is_active") == "1" || $driver->is_active == "1" ? 'selected' : '' }}>Yes</option>
                    </select>
                    @if($errors->has("is_active"))
                        <span class="help-block">{{ $errors->first("is_active") }}</span>
                    @endif
                </div>
                 
              
               
             
                <div class="form-group col-md-3 col-sm-3 @if($errors->has('country_id')) has-error @endif">
                    <label for="country_id-field">Country</label>
                    <select id="country_id-field" name="country_id" class="form-control">

                        @if(! empty($countries))
                            @foreach($countries as $country)
                                <option value="{{$country->id}}" {{ ($country->id) == $driver->country_id ? 'selected' : '' }}
                                >{{$country->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>

                <div class="form-group col-md-3 col-sm-3 @if($errors->has('city_id')) has-error @endif">
                    <label for="city_id-field">City</label>
                    <select id="city_id-field" name="city_id" class="form-control">
                        @if(! empty($cities))
                            @foreach($cities as $city)
                                <option value="{{$city->id}}" {{ ($city->id) == $driver->city_id ? 'selected' : '' }}
                                >{{$country->name}}</option>
                            @endforeach
                        @endif
                    </select>
                    @if($errors->has("city_id"))
                        <span class="help-block">{{ $errors->first("city_id") }}</span>
                    @endif
                </div>

                  <div class="form-group col-md-12 col-sm-12 @if($errors->has('address')) has-error @endif">
                    <label for="address-field">Address</label>
                    <textarea class="form-control" id="address-field" rows="4"
                              name="address">{{ is_null(old("address")) ? $driver->address : old("address") }}</textarea>
                    @if($errors->has("address"))
                        <span class="help-block">{{ $errors->first("address") }}</span>
                    @endif
                </div>
   <div class="form-group col-md-12 col-sm-12 @if($errors->has('notes')) has-error @endif">
                    <label for="notes-field">Notes</label>
                    <textarea class="form-control" id="notes-field" rows="4"
                              name="notes">{{ is_null(old("notes")) ? $driver->notes : old("notes") }}</textarea>
                    @if($errors->has("notes"))
                        <span class="help-block">{{ $errors->first("notes") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-6 col-sm-6 @if($errors->has('driver_profile')) has-error @endif">
                    <div id="driver_profile_upload">
                        @if($driver->image)
                        <div class="imgDiv">
                            <input type="hidden" value="{{ $driver->image }}" name="driver_profile_old"
                                   id="driver_profile_old">
                            <img src="{{ asset('public/backend/images/'.$driver->image) }}"
                                 style="width:100px;height:100px">
                            <button  class=" cancel btn btn-danger">&times;</button>
                            </div>
                        @endif
                    </div>
                    <label for="image-field">Profile Image</label>
                    <input  class="uploadPhoto btn btn-primary" type="file"
                           name="driver_profile" id="driver_profile">
                    @if($errors->has("driver_profile"))
                        <span class="help-block">{{ $errors->first("driver_profile") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-6 col-sm-6 @if($errors->has('criminal_record_image')) has-error @endif">
                    <div id="criminal_record_image_upload">
                        @if($driver->criminal_record_image)
                        <div class="imgDiv">
                            <input type="hidden" value="{{ $driver->criminal_record_image }}" name="criminal_record_image_old"
                                   id="criminal_record_image_old">
                            <img src="{{ asset('public/backend/images/'.$driver->criminal_record_image) }}"
                                 style="width:100px;height:100px">
                            <button class="btn btn-danger cancel">&times;</button>
                            </div>
                        @endif
                    </div>
                    <label for="image-field">Criminal Record Image</label>
                    <input  class="uploadPhoto btn btn-primary" type="file"
                           name="criminal_record_image" id="criminal_record_image">
                    @if($errors->has("criminal_record_image"))
                        <span class="help-block">{{ $errors->first("criminal_record_image") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-6 col-sm-6 @if($errors->has('driving_licence_image')) has-error @endif">
                    <div id="driving_licence_image_upload">
                        @if($driver->driving_licence_image)
                        <div class="imgDiv">
                            <input type="hidden" value="{{ $driver->driving_licence_image }}"
                                   name="driving_licence_image_old" id="driving_licence_image_old">
                            <img src="{{ asset('public/backend/images/'.$driver->driving_licence_image) }}"
                                 style="width:100px;height:100px">
                            <button class="btn btn-danger cancel">&times;</button>
                            </div>
                        @endif
                    </div>
                    <label for="driving_licence_image-field">Driving licence image</label>
                    <input  class="uploadPhoto btn btn-primary" type="file"
                           name="driving_licence_image" id="driving_licence_image">
                    @if($errors->has("driving_licence_image"))
                        <span class="help-block">{{ $errors->first("driving_licence_image") }}</span>
                    @endif
                </div>
                
               
                <div class="form-group col-md-6 col-sm-6 @if($errors->has('national_id_image')) has-error @endif">
                    <div id="national_id_image_upload">
                        @if($driver->national_id_image)
                        <div class="imgDiv">
                            <input  type="hidden" value="{{ $driver->national_id_image }}"
                                   name="national_id_image_old"
                                   id="national_id_image_old">
                            <img src="{{ asset('public/backend/images/'.$driver->national_id_image) }}"
                                 style="width:100px;height:100px">
                            <button  class="btn btn-danger cancel">&times;</button>
                            </div>
                        @endif
                    </div>
                    <label for="national_id_image-field">National Id Image</label>
                    <input  class="uploadPhoto btn btn-primary" type="file" name="national_id_image"
                           id="national_id_image">
                    @if($errors->has("national_id_image"))
                        <span class="help-block">{{ $errors->first("national_id_image") }}</span>
                    @endif
                </div>

                <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAgent.drivers.index') }}"><i
                                class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function () {

            if(!$("#criminal_record_image_old").val()){
                $("#criminal_record_image").prop('required',true);
            }


            $("form[name='driver_create']").validate({
                // Specify validation rules
                rules: {
                    email: {
                        required: true,
                        email: true,
                        remote: {
                            url: "{!! url('/check-email-agent') !!}",
                            type: "get"
                        }
                    },
                    mobile: {
                        required: true,
                        number:true,
                        remote: {
                            url: "{!! url('/check-mobile-agent') !!}",
                            type: "get"
                        }
                    },
                    phone: {
                        required: true,
                        number:true
                    },
                    password: {
                        required: true,
                        minlength: 5
                    },
                    name: {
                        required: true
                    },
                    address: {
                        required: true
                    },

                    city_id: {
                        required: true
                    },
                    latitude: {
                        required: true,
                        number: true
                    },
                    longitude: {
                        required: true,
                        number: true
                    },
                    driving_licence_expired_date: {
                        date: true,
                        required: true
                    },
                    national_id_number: {
                        required: true,
                        number: true,
                        rangelength:[14,14]
                    },
                    driving_licence_number: {
                        required: true,
                        number: true
                    },
                    national_id_expired_date: {
                        date: true,
                        required: true
                    },
                    confirm_password: {
                        required: true,
                        equalTo: '#password-field',

                        minlength: 5
                    }
                },
                // Specify validation error messages
                messages: {
                    password: {
                        required: "Please Enter Driver Password",
                        minlength: "Password must be more than 5 character"
                    },
                    name: {
                        required: "Please Enter Driver Name",

                    },
                    address: {
                        required: "Please Enter Your Address",

                    },
                    confirm_password: "Confirm password is wrong",
                    email: {
                        required: "Please Enter Driver E-mail",
                        email: "Please Enter Driver Correct E-mail",
                        remote: jQuery.validator.format("E-mail already exist!")
                    },
                    mobile: {
                        required: "Please Enter Driver Mobile",
                        remote: jQuery.validator.format("Mobile already exist!")
                    },
                    city_id: {
                        required: "Please Select City",
                    },
                    latitude: {
                        required: "Please Enter Latitude",
                        number: "Latitude must be numbers",
                    },
                    longitude: {
                        required: "Please Enter Longitude",
                        number: "Longitude must be numbers",
                    },
                    driving_licence_expired_date: {
                        date: 'Please Enter Correct Date Format',
                        required: "Please Enter Driving Licence Expired Date",
                    },
                    national_id_number: {
                        required: "Please Enter National Id Number",
                        number: "National Id Must Be Number",
                        rangelength:'National Id Must Be 14 Digits'
                    },
                    driving_licence_number: {
                        required: "Please Enter Driving Licence Number",
                        number: "Driving Licence Must Be Number",
                    },
                    national_id_expired_date: {
                        date: 'Please Enter Correct Date Format',
                        required: "Please Enter National Id Expired Date",
                    }

                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                },

                highlight: function (element) {
                    $(element).parent().addClass('error')
                },
                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {

                    form.submit();

                }
            });
        })
    </script>
@endsection
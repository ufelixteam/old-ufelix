@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.contacts')}} - {{__('backend.add_contact')}}</title>
@endsection

@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-plus"></i> {{__('backend.contacts')}} / {{__('backend.add_contact')}} </h3>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <form action="{{ route('mngrAdmin.contacts.store') }}" method="POST" name="validateForm">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                  <div class="col-md-6 col-sm-6 form-group @if($errors->has('name')) has-error @endif">
                      <label for="name-field">{{__('backend.name')}}</label>
                      <input type="text" id="name-field" name="name" class="form-control" value="{{ old("name") }}"/>
                       @if($errors->has("name"))
                        <span class="help-block">{{ $errors->first("name") }}</span>
                       @endif
                  </div>
                  <div class="col-md-6 col-sm-6 form-group @if($errors->has('email')) has-error @endif">
                     <label for="email-field">{{__('backend.email')}}</label>
                     <input type="text" id="email-field" name="email" class="form-control" value="{{ old("email") }}"/>
                     @if($errors->has("email"))
                      <span class="help-block">{{ $errors->first("email") }}</span>
                     @endif
                   </div>
                 </div>
                 <div class="row">
                   <div class="col-md-6 col-sm-6 form-group @if($errors->has('mobile')) has-error @endif">
                       <label for="mobile-field">{{__('backend.mobile')}}</label>
                       <input type="text" id="mobile-field" name="mobile" class="form-control" value="{{ old("mobile") }}"/>
                       @if($errors->has("mobile"))
                        <span class="help-block">{{ $errors->first("mobile") }}</span>
                       @endif
                   </div>
                    <div class="col-md-6 col-sm-6 form-group @if($errors->has('title')) has-error @endif">
                       <label for="title-field">{{__('backend.title')}}</label>
                       <input type="text" id="title-field" name="title" class="form-control" value="{{ old("title") }}"/>
                       @if($errors->has("title"))
                        <span class="help-block">{{ $errors->first("title") }}</span>
                       @endif
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 col-sm-12 form-group @if($errors->has('message')) has-error @endif">
                       <label for="message-field">{{__('backend.message')}}</label>
                       <textarea class="form-control" id="message-field" rows="3" name="message">{{ old("message") }}</textarea>
                       @if($errors->has("message"))
                        <span class="help-block">{{ $errors->first("message") }}</span>
                       @endif
                    </div>
                  </div>
                  <div class="well well-sm col-md-12 col-sm-12 col-xs-12">
                      <button type="submit" class="btn btn-primary">{{__('backend.create')}}</button>
                      <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.contacts.index') }}"><i class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
                  </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
  <script type="text/javascript">
  $(function() {
    $("form[name='validateForm']").validate({
      // Specify validation rules
      rules: {
        name: {
          required: true,
        },
        email: {
          required: true,
          email: true,
        },
        mobile: {
          required: true,
          number: true,
          rangelength:[11,11],
        },
        title: {
          required: true,
        },
        message: {
          required: true,
        }
      },

      // Specify validation error messages
      messages: {
        name: {
          required: "{{__('backend.Please_Enter_The_Name')}}",
        },
        email: {
          required: "{{__('backend.Please_Enter_Email_Address')}}",
          email: "{{__('backend.Please_Enter_Correct_E-mail')}}",
        },
        mobile: {
          required: "{{__('backend.Please_Enter_Mobile_Number')}}",
          number: "{{__('backend.Please_Enter_Correct_Number')}}",
          rangelength:"{{__('backend.Mobile_Must_Be11_Digits')}}",
        },
        title: {
          required: "{{__('backend.Please_Enter_Message_Title')}}",
        },
        message: {
          required: "{{__('backend.Please_Enter_The_Message_Content')}}",
        },
      },

      errorPlacement: function(error, element) {
          $(element).parents('.form-group').append(error)
      },

      highlight: function (element) {
          $(element).parent().addClass('error')
      },

      unhighlight: function (element) {
          $(element).parent().removeClass('error')
      },

      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function(form) {
          form.submit();
      }
    });
  })
</script>
@endsection

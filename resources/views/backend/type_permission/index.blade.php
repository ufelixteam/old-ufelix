@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.type_permission')}}</title>
@endsection

@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.type_permission')}}
        </h3>
    </div>
@endsection

@section('content')
    @if(permission('addPermissionType')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
      <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
              <form action="{{ route('mngrAdmin.type_permissions.store') }}" method="POST" name='validateForm'>
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">

                  <div class="form-group col-md-12 col-sm-12 @if($errors->has('name')) has-error @endif">
                     <label for="name-field">{{__('backend.name')}}:</label>
                     <input type="text" id="name-field" name="name" class="form-control" value="{{ old("name") }}"/>
                     @if($errors->has("name"))
                      <span class="help-block">{{ $errors->first("name") }}</span>
                     @endif
                  </div>

                  <div class="well well-sm col-md-12 col-sm-12 col-xs-12">
                      <button type="submit" class="btn btn-primary">{{__('backend.create')}}</button>
                      <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.type_permissions.index') }}"><i class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
                  </div>
              </form>
          </div>
      </div>
    @endif

    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            @if($typePermissions->count())
                <table class="table table-condensed table-striped">
                    <thead>
                      <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">{{__('backend.permission_type')}}</th>
                        <th class="text-right"></th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach($typePermissions as $type)
                            <tr>
                                <td class="text-center">{{$type->id}}</td>
                                <td class="text-center">{{$type->name}}</td>
                                <td class="text-right">

                                  @if(permission('editPermissionType')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                    <a class="btn btn-xs btn-warning" data-toggle="modal" data-target="#{{$type->id}}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>

                                      <!-- Modal -->
                                      <div class="modal fade" id="{{$type->id}}" role="dialog">
                                        <div class="modal-dialog">
                                          <!-- Modal content-->
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                              <h4 class="modal-title pull-left">{{__('backend.Edit_Permission_Type')}} - <b>{{$type->name}}</b></h4>
                                            </div>
                                            <form action="{{ route('mngrAdmin.type_permissions.update', $type->id) }}" method="POST">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="_method" value="PUT">
                                                <div class="modal-body">
                                                    <input type="text" id="name-field" name="name" class="form-control" value="{{$type->name}}"/>
                                                    @if($errors->has("name"))
                                                     <span class="help-block">{{ $errors->first("name") }}</span>
                                                    @endif
                                                </div>
                                                <div class="modal-footer">
                                                  <button type="submit" class="btn btn-primary"> {{__('backend.edit')}}  </button>
                                                  <button type="button" class="btn btn-default" data-dismiss="modal"> {{__('backend.close')}}  </button>
                                                </div>
                                            </form>
                                          </div>
                                        </div>
                                      </div>
                                    @endif

                                    @if(permission('deletePermissionType')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                      <!-- <form action="{{ route('mngrAdmin.type_permissions.destroy', $type->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}} ')) { return true } else {return false };">
                                          <input type="hidden" name="_method" value="DELETE">
                                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                          <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}} </button>
                                      </form> -->
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $typePermissions->render() !!}
            @else
                <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}} </h3>
            @endif
        </div>
    </div>
@endsection

@section('scripts')
   <script type="text/javascript">
  $(function() {
    $("form[name='validateForm']").validate({
      // Specify validation rules
      rules: {
        name: {
          required: true,
        },
      },

      // Specify validation error messages
      messages: {
        name: {
          required: "{{__('backend.Please_Enter_Permission_Type')}}",
        },
      },

       errorPlacement: function(error, element) {
          $(element).parents('.form-group').append(error)
      },

      highlight: function (element) {
          $(element).parent().addClass('error')
      },

      unhighlight: function (element) {
          $(element).parent().removeClass('error')
      },
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function(form) {
          form.submit();
      }
    });
  })
</script>
@endsection

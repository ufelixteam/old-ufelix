<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RefundCollection extends Model
{

    protected $guarded = ['id'];

    public function getRefundStatusAttribute()
    {
        $orders = $this->orders()->where('is_refund', 0)->count();

        if ($orders == 0) {
            return 1;
        } else {
            return 0;
        }

    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order', 'refund_collection_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }

}

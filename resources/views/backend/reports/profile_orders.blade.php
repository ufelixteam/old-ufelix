<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">

    <style>
        td, th {
            text-align: center;
            vertical-align: middle;
        }

    </style>
</head>

<body>
<table>
    <tr>
        <th>No</th>
        <th>
            ID
        </th>
        {{--        <th>--}}
        {{--            CREATE DATE--}}
        {{--        </th>--}}
        {{--        <th>--}}
        {{--            DROPPED DATE--}}
        {{--        </th>--}}
        {{--        <th>--}}
        {{--            RECEIVED DATE--}}
        {{--        </th>--}}
        <th>
            LAST STATUS DATE
        </th>
        {{--        <th>--}}
        {{--            COMMENT--}}
        {{--        </th>--}}
        {{--        <th>--}}
        {{--            FROM DROPPED--}}
        {{--        </th>--}}
        {{--        <th>--}}
        {{--            FROM RECEIVED--}}
        {{--        </th>--}}
        <th>
            STATUS
        </th>
        <th>
            WAREHOUSE
        </th>
        <th>
            COLLECTED COST
        </th>
        @if($type == 1)
            <th>
                CORPORATE
            </th>
            <th>
                USER
            </th>
        @endif
        <th>
            Client Code
        </th>
        <th>
            Driver
        </th>
        <th>
            Captain
        </th>
        <th>
            ORDER NUMBER
        </th>
        <th>
            ORDER TYPE
        </th>
        <th>
            CODE
        </th>
        <th>
            RECEIVER NAME
        </th>
        <th>
            RECEIVER ADDRESS
        </th>
        <th>
            RECEIVER MOBILE
        </th>
        <th>
            RECEIVER MOBILE2
        </th>
        <th>
            GOV
        </th>
        <th>
            CITY
        </th>
        <th>
            NOTES
        </th>
        <th>
            TYPE
        </th>
        <th>
            TOTAL
        </th>
        <th>
            CLIENT
        </th>
        <th>
            UFELIX
        </th>
        <th>
            Ufelix Comment1
        </th>
        <th>
            Date
        </th>
        <th>
            Ufelix Comment2
        </th>
        <th>
            Date
        </th>
        <th>
            Ufelix Comment3
        </th>
        <th>
            Date
        </th>
        <th>
            Ufelix Comment4
        </th>
        <th>
            Date
        </th>
        <th>
            Ufelix Comment5
        </th>
        <th>
            Date
        </th>
        <th>
            Client Comment1
        </th>
        <th>
            Date
        </th>
        <th>
            Client Comment2
        </th>
        <th>
            Date
        </th>
        <th>
            Client Comment3
        </th>
        <th>
            Date
        </th>
        <th>
            Client Comment4
        </th>
        <th>
            Date
        </th>
        <th>
            Client Comment5
        </th>
        <th>
            Date
        </th>
    </tr>
    @php
        $i=1;
    @endphp
    @if(! empty($orders))
        @foreach($orders->cursor() as $order)
            @php
                $driver = !empty($order->oneDriver) ? $order->oneDriver->first() : null;
            @endphp
            <tr>
                <td>{{$i}}</td>
                <td>
                    {{$order->id}}
                </td>
                {{--                <td>--}}
                {{--                    {{$order->created_at ? date('Y-m-d', strtotime($order->created_at)) : ''}}--}}
                {{--                </td>--}}
                {{--                <td>--}}
                {{--                    {{$order->dropped_at ? date('Y-m-d', strtotime($order->dropped_at)) : ''}}--}}
                {{--                </td>--}}
                {{--                <td>--}}
                {{--                    {{$order->received_at ? date('Y-m-d', strtotime($order->received_at)) : ''}}--}}
                {{--                </td>--}}
                <td>
                    @if($settings)
                        {{$order->profile_last_status_date ? date('Y-m-d', strtotime($order->profile_last_status_date)) : ''}}
                    @else
                        {{$order->last_status_date ? date('Y-m-d', strtotime($order->last_status_date)) : ''}}
                    @endif
                </td>
                {{--                <td @if($order->comments_count == 0) style="background: #ff0000; color: #ffffff" @endif>--}}
                {{--                    {{$order->comments_count ? $order->comments_count : 0}}--}}
                {{--                </td>--}}
                {{--                <td>--}}
                {{--                    @if($order->dropped_at)--}}
                {{--                        @php--}}
                {{--                            $now = \Carbon\Carbon::now();--}}
                {{--                            $date = \Carbon\Carbon::parse($order->dropped_at);--}}
                {{--                            echo $date->diffInDays($now);--}}
                {{--                        @endphp--}}
                {{--                    @endif--}}
                {{--                </td>--}}
                {{--                <td>--}}
                {{--                    @if($order->received_at)--}}
                {{--                        @php--}}
                {{--                            $now = \Carbon\Carbon::now();--}}
                {{--                            $date = \Carbon\Carbon::parse($order->received_at);--}}
                {{--                            echo $date->diffInDays($now);--}}
                {{--                        @endphp--}}
                {{--                    @endif--}}
                {{--                </td>--}}
                <td>
                    {!! $settings ? $order->profile_status_details_without_refund : $order->status_details_without_refund !!}
                </td>
                <td>
                    @if($order->is_refund == 1 && (!$order->is_received || !$settings))
                        Refund
                    @elseif($order->client_dropoff == 1 && (!$order->is_received || !$settings))
                        Client
                    @elseif($order->warehouse_dropoff == 1 && (!$order->is_received || !$settings))
                        Warehouse
                    @endif
                </td>
                <td>
                    @if(!$order->is_received || !$settings)
                        {{$order->collected_cost}}
                    @endif
                </td>
                @if($type == 1)
                    <td>
                        {{@$order->customer->Corporate->name}}
                    </td>
                    <td>
                        {{$order->customer->name}}
                    </td>
                @endif
                <td>
                    {{$order->reference_number}}
                </td>
                <td>
                    @if($driver && $driver->manager_id && !empty($driver->manager))
                        {{$driver->manager->name}}
                    @elseif($driver)
                        {{$driver->name}}
                    @else
                        -
                    @endif
                </td>
                <td>
                    @if($driver && $driver->manager_id && !empty($driver->manager))
                        {{$driver->name}}
                    @else
                        -
                    @endif
                </td>
                <td>
                    {{$order->order_no}}
                </td>
                <td>
                    {{$order->type}}
                </td>
                <td>
                    {{$order->order_number}}
                </td>
                <td>
                    {{$order->receiver_name}}
                </td>
                <td>
                    {{mb_substr(htmlentities(strip_tags($order->receiver_address)),0,150, "utf-8")}}
                </td>
                <td>
                    {{$order->receiver_mobile}}
                </td>
                <td>
                    {{$order->receiver_phone}}
                </td>
                <td>
                    {{isset($order->to_government->name_en) ? $order->to_government->name_en : ''}}
                </td>
                <td>
                    {{isset($order->to_city->name_en) ? $order->to_city->name_en : ''}}
                </td>
                <td>
                    {{$order->notes}}
                </td>
                <td>
                    {{isset($order->types->name_en) ? $order->types->name_en : ''}}
                </td>
                <td>
                    {{$order->total_price}}
                </td>
                <td>
                    {{$order->total_price - $order->delivery_price}}
                </td>
                <td>
                    {{$order->delivery_price}}
                </td>
                @php
                    $ufelix_comments = $order->comments->whereIn('user_type', [1, 3])->sortByDesc('created_at')->take(5);
                    $client_comments = $order->comments->where('user_type', 2)->sortByDesc('created_at')->take(5);
                @endphp
                @foreach($ufelix_comments as $t => $order_comment)
                    <td>
                        {{$order_comment->comment ? $order_comment->comment : ''}}
                    </td>
                    <td>
                        {{$order_comment->created_at ? date('Y-m-d', strtotime($order_comment->created_at)) : ''}}
                    </td>
                @endforeach
                @php
                    $ufelixCommentCount = 5 - count($ufelix_comments)
                @endphp
                @if($ufelixCommentCount)
                    @for ($k = 0; $k < $ufelixCommentCount; $k++)
                        <td></td>
                        <td></td>
                    @endfor
                @endif
                @foreach($client_comments as $t => $order_comment)
                    <td>
                        {{$order_comment->comment ? $order_comment->comment : ''}}
                    </td>
                    <td>
                        {{$order_comment->created_at ? date('Y-m-d', strtotime($order_comment->created_at)) : ''}}
                    </td>
                @endforeach
                @php
                    $clientCommentCount = 5 - count($client_comments)
                @endphp
                @if($clientCommentCount)
                    @for ($k = 0; $k < $clientCommentCount; $k++)
                        <td></td>
                        <td></td>
            @endfor
            @endif
        @endforeach
    @endif
</table>
</body>
</html>

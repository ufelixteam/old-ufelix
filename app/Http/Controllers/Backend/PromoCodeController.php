<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\PromoCode;
use Response;
use Flash;
use File;
use Auth;

class PromoCodeController extends Controller {

	// Get All Promo Codes
	public function index()
	{
		$promo_codes = PromoCode::orderBy('id', 'desc')->paginate(50);
		return view('backend.promo_codes.index', compact('promo_codes'));
	}

	// Create Promo Code Page
	public function create()
	{
		return view('backend.promo_codes.create');
	}

	// Added New Promo Code
	public function store(Request $request)
	{
			/*
			* status = 0 :: Not Verify
			* status = 1 :: Verify
			*/
			$v = \Validator::make($request->all(), [
					'code'           => 'required',
					'no_tries'       => 'required',
					'discount'       => 'required',
					'status'         => 'required',
			]);

			if ($v->fails()) {
					return redirect()->back()->withErrors($v->errors());
			}

			$promo_code 						= new PromoCode();
			$promo_code->code 			= $request->input("code");
      $promo_code->no_tries 	= $request->input("no_tries");
      $promo_code->discount 	= $request->input("discount");
      $promo_code->status 		= $request->input("status");
			$promo_code->save();

			// Function Add Action In Activity Log By Samir
			addActivityLog('Add Promo Code ' . $promo_code->code , ' اضافة الرمز الترويجي ' . $promo_code->code, 14); // Helper Function in App\Helpers.php

      Flash::success(__('backend.PromoCode_saved_successfully'));
			return redirect()->route('mngrAdmin.promo_codes.index');
	}

	// View One Promo Code
	public function show($id)
	{
			$promo_code = PromoCode::findOrFail($id);
			return view('backend.promo_codes.show', compact('promo_code'));
	}

	// Edit Promo Code Page
	public function edit($id)
	{
		$promo_code = PromoCode::findOrFail($id);
		return view('backend.promo_codes.edit', compact('promo_code'));
	}

	// Update Promo Code
	public function update(Request $request, $id)
	{
			/*
			* status = 0 :: Not Verify
			* status = 1 :: Verify
			*/
			$v = \Validator::make($request->all(), [
					'code'           => 'required',
					'no_tries'       => 'required',
					'discount'       => 'required',
					'status'         => 'required',
			]);

			if ($v->fails()) {
					return redirect()->back()->withErrors($v->errors());
			}

			$promo_code 						= PromoCode::findOrFail($id);
			$promo_code->code 			= $request->input("code");
      $promo_code->no_tries 	= $request->input("no_tries");
      $promo_code->discount 	= $request->input("discount");
      $promo_code->status 		= $request->input("status");
			$promo_code->save();

			// Function Add Action In Activity Log By Samir
			addActivityLog('Edit Promo Code ' . $promo_code->code , ' تعديل الرمز الترويجي ' . $promo_code->code, 14); // Helper Function in App\Helpers.php

      Flash::warning(__('backend.PromoCode_Updated_successfully'));
			return redirect()->route('mngrAdmin.promo_codes.index');
	}

	// Delete Promo Code
	public function destroy($id)
	{
			$promo_code = PromoCode::findOrFail($id);
			$promo_code->delete();

			// Function Add Action In Activity Log By Samir
			addActivityLog('Delete Promo Code ' . $promo_code->code , ' حذف الرمز الترويجي ' . $promo_code->code, 14); // Helper Function in App\Helpers.php

      flash(__('backend.PromoCode_deleted_successfully'))->error();
			return redirect()->route('mngrAdmin.promo_codes.index');
	}

}

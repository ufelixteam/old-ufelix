<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleType extends Model
{
    //
    protected $hidden =['created_at','updated_at'];

    public function getStatusSpanAttribute($value)
    {
        if($this->status == 0){
            $value="<span class='badge badge-pill label-danger'>".__('backend.not_verify')."</span>";
        }else if($this->status == 1){
            $value="<span class='badge badge-pill label-success'>".__('backend.verfiy')."</span>";
        }else{
          $value = "";
        }
        return $value;
    }
}

<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Color;
use Response;
use Flash;
use File;
use Auth;

class ColorController extends Controller {

	// Get All Vehicle Colors
	public function index()
	{
		$colors = Color::orderBy('id', 'desc')->paginate(50);
		return view('backend.colors.index', compact('colors'));
	}

	// Create Vehicle Color Page
	public function create()
	{
		return view('backend.colors.create');
	}

	// Added New Vehicle Color
	public function store(Request $request)
	{
			/*
			* status = 0 :: Not Verify
			* status = 1 :: Verify
			*/

			$this->validate($request,[
				 'name'   		 			=> 'required',
				 'status'   	 			=> 'required',
			]);

			$color 					= new Color();
			$color->name 		= $request->input("name");
      $color->status 	= $request->input("status");
			$color->save();

			// Function Add Action In Activity Log By Samir
			addActivityLog('Add New Vehicle Color ' . $color->name , ' اضافة لون مركبة جديدة ' . $color->name, 24); // Helper Function in App\Helpers.php

      Flash::success(__('backend.Color_saved_successfully'));
			return redirect()->route('mngrAdmin.colors.index');
	}

	// View One Vehicle Color
	public function show($id)
	{
		$color = Color::findOrFail($id);
		return view('backend.colors.show', compact('color'));
	}

	// Edit Vehicle Color
	public function edit($id)
	{
		$color = Color::findOrFail($id);
		return view('backend.colors.edit', compact('color'));
	}

	// Update Vehicle Color
	public function update(Request $request, $id)
	{
			/*
			* status = 0 :: Not Verify
			* status = 1 :: Verify
			*/

			$this->validate($request,[
				 'name'   		 			=> 'required',
				 'status'   	 			=> 'required',
			]);

			$color 					= Color::findOrFail($id);
			$color->name 		= $request->input("name");
      $color->status 	= $request->input("status");
			$color->save();

			// Function Add Action In Activity Log By Samir
			addActivityLog('Edit Vehicle Color ' . $color->name , ' تعديل لون المركبة ' . $color->name, 24); // Helper Function in App\Helpers.php

      Flash::warning(__('backend.Color_Updated_successfully'));
			return redirect()->route('mngrAdmin.colors.index');
	}

	// Delete Vehicle Color
	public function destroy($id)
	{
			$color = Color::findOrFail($id);
			$color->delete();

			// Function Add Action In Activity Log By Samir
			addActivityLog('Delete Vehicle Color ' . $color->name , ' حذف لون المركبة ' . $color->name, 24); // Helper Function in App\Helpers.php

      flash(__('backend.Color_deleted_successfully'))->error();
			return redirect()->route('mngrAdmin.colors.index');
	}

}

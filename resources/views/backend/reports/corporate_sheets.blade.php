@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.corporates_excel')}}</title>
    <style>
        .form-check-inline {
            display: -webkit-inline-box;
            display: -ms-inline-flexbox;
            display: inline-flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            padding-left: 0;
            margin-right: .75rem;
        }

        .form-check-inline input {
            margin: 5px;
        }
    </style>
@endsection
@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="fa fa-file-excel-o"></i> {{__('backend.corporates_excel')}}

        </h3>

    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <form action="{{URL::asset('/mngrAdmin/corporate-sheets')}}" method="post" id="sort-form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group col-md-2 col-sm-6 col-xs-12">
                    <label for="from_date-field">{{__('backend.From_Date')}}</label>
                    <div class='input-group date ' id="datepicker1">
                        <input type="text" id="from_date-field" name="from_date" class="form-control"/>
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-2 col-sm-6 col-xs-12">
                    <label for="to_date-field">{{__('backend.To_Date')}}</label>
                    <div class='input-group date ' id="datepickers">
                        <input type="text" id="to_date-field" name="to_date" class="form-control"/>
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                    </div>
                </div>

                <div class="form-group col-md-2 col-sm-6 col-xs-12">
                    <label for="corporate_id">{{__('backend.user')}}</label>
                    <select class="form-control select2" id="user_id" name="user_id">
                        <option value="" data-display="Select">{{__('backend.user')}}</option>
                        @if(! empty($users))
                            @foreach($users as $user)
                                <option value="{{$user->id}}">
                                    {{$user->name}}
                                </option>
                            @endforeach
                        @endif
                    </select>
                </div>

                <div class="form-group col-md-2 col-sm-6 col-xs-12">
                    <label for="corporate_id">{{__('backend.corporate')}}</label>
                    <select class="form-control select2" id="corporate_id" name="corporate_id">
                        <option value="" data-display="Select">{{__('backend.corporate')}}</option>
                    </select>
                </div>
                {{--                <div class="form-group col-md-2 col-sm-6 col-xs-12 @if($errors->has('customer_id')) has-error @endif">--}}
                {{--                    <label for="user_id">{{__('backend.customer')}}</label>--}}
                {{--                    <select class="form-control select2 " id="customer_id" name="customer_id">--}}
                {{--                        <option value="" data-dispaly="Select">{{__('backend.customer')}}</option>--}}
                {{--                        @if(! empty($customers))--}}
                {{--                            @foreach($customers as $customer)--}}
                {{--                                <option value="{{$customer->id}}">{{$customer->name}}</option>--}}
                {{--                            @endforeach--}}
                {{--                        @endif--}}
                {{--                    </select>--}}
                {{--                    @if($errors->has("customer_id"))--}}
                {{--                        <span class="help-block">{{ $errors->first("customer_id") }}</span>--}}
                {{--                    @endif--}}
                {{--                </div>--}}

                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="user_id">{{__('backend.status')}}</label>
                    <div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="0"
                                   name="status[]">
                            <label class="form-check-label" for="inlineCheckbox1">{{__('backend.pending')}}</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="7"
                                   name="status[]">
                            <label class="form-check-label"
                                   for="inlineCheckbox2">{{__('backend.dropped')}}</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="1"
                                   name="status[]">
                            <label class="form-check-label"
                                   for="inlineCheckbox3">{{__('backend.accepted')}}</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="2"
                                   name="status[]">
                            <label class="form-check-label"
                                   for="inlineCheckbox3">{{__('backend.received')}}</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="3"
                                   name="status[]">
                            <label class="form-check-label"
                                   for="inlineCheckbox3">{{__('backend.delivered')}}</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="4"
                                   name="status[]">
                            <label class="form-check-label"
                                   for="inlineCheckbox3">{{__('backend.cancelled')}}</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="5"
                                   name="status[]">
                            <label class="form-check-label"
                                   for="inlineCheckbox3">{{__('backend.recall')}}</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="8"
                                   name="status[]">
                            <label class="form-check-label"
                                   for="inlineCheckbox3">{{__('backend.reject')}}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group col-md-2 col-sm-6 col-xs-12 ">
                    <button id="download-field" type="submit" class="btn btn-primary btn-block"
                            style="margin-top: 24px;"> {{__('backend.download')}} </button>
                </div>
            </form>
        </div>

        <div class=" row col-md-12 text-center" id="table-view">

        </div>
    </div>



@endsection

@section('scripts')
    <script>
        $('#user_id').on('change', function () {
            $('#corporate_id').children('option:not(:first)').remove();
            if ($(this).val()) {
                $.ajax({
                    url: "{{ url('/mngrAdmin/get_user_corporates/')}}" + "/" + $(this).val(),
                    type: 'get',
                    success: function (data) {
                        $.each(data, function (i, content) {
                            $('#corporate_id').append($("<option></option>").attr("value", content.id).text(content.name));
                        });
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        });
    </script>

@endsection

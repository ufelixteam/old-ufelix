@extends('backend.layouts.app')
@section('css')
  <title>Countries - View</title>
@endsection
@section('header')
<div class="page-header">
        <h3>Show Countries #{{$country->id}}</h3>
        <form action="{{ route('mngrAdmin.countries.destroy', $country->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('mngrAdmin.countries.edit', $country->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <div class="form-group col-md-6 col-xs-6 col-sm-6">
                <label for="nome">#</label>
                <p class="form-control-static">{{$country->id}}</p>
            </div>
            <div class="form-group col-md-6 col-xs-6 col-sm-6">
                     <label for="name">NAME</label>
                     <p class="form-control-static">{{$country->name}}</p>
                </div>
                    <div class="form-group col-md-6 col-xs-6 col-sm-6">
                     <label for="code">CODE</label>
                     <p class="form-control-static">{{$country->code}}</p>
                </div>
                    <div class="form-group col-md-6 col-xs-6 col-sm-6">
                     <label for="image">IMAGE</label>
                     <p><img src="{{ $country->image != "" ? $country->image : ''}}" width="50" height="50"></p>
                </div>

            <div class="col-md-12 col-xs-12 col-sm-12">
                <a class="btn btn-link" href="{{ route('mngrAdmin.countries.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
            </div>

        </div>
    </div>

@endsection

<script>

    $(document).ready(function () {
        $(window).keydown(function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });
    });
    $("#search-btn1").on('click', function () {
        // Wait icon
        $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
        $.ajax({
            url: '{{url("/captainDashboard/orders")}}' + '?from_date=' + $("#fromDate").val() + '&to_date=' + $("#toDate").val() + '&search=' + $("#search-field").val() + '&type={{$type}}' + '&driver_id=' + $("#driver_id").val(),
            type: 'get',
            data: $("#search-form").serialize(),
            success: function (data) {
                $('.list').html(data.view);
                $('.js-form').html(data.js);
                $(".toggle").bootstrapToggle('destroy')
                $(".toggle").bootstrapToggle();

                $('.testtooltip').popover({
                    container: 'body'
                    // content: $('#myPopoverContent').html(),
                    // html: true,
                    // trigger: "hover"
                });
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });

    });

    $("#fromDate").on('change', function () {
        // Wait icon
        $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
        var link = $("#download_excel").attr('href') + '&from_date=' + $("#fromDate").val() + '&to_date=' + $("#toDate").val();
        $("#download_excel").attr('href', link);
        $.ajax({
            url: '{{url("/captainDashboard/orders")}}' + '?from_date=' + $("#fromDate").val() + '&to_date=' + $("#toDate").val() + '&search=' + $("#search-field").val() + '&type={{$type}}' + '&driver_id=' + $("#driver_id").val(),
            type: 'get',
            data: $("#search-form").serialize(),
            success: function (data) {
                $('.list').html(data.view);
                $('.js-form').html(data.js);
                $(".toggle").bootstrapToggle('destroy')
                $(".toggle").bootstrapToggle();

                $('.testtooltip').popover({
                    container: 'body'
                    // content: $('#myPopoverContent').html(),
                    // html: true,
                    // trigger: "hover"
                });
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

    $("#toDate").on('change', function () {
        // Wait icon
        $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
        var link = $("#download_excel").attr('data-href') + '&from_date=' + $("#fromDate").val() + '&to_date=' + $("#toDate").val();
        $("#download_excel").attr('href', link);
        $.ajax({
            url: '{{url("/captainDashboard/orders")}}' + '?from_date=' + $("#fromDate").val() + '&to_date=' + $("#toDate").val() + '&search=' + $("#search-field").val() + '&type={{$type}}' + '&driver_id=' + $("#driver_id").val(),
            type: 'get',
            data: $("#search-form").serialize(),
            success: function (data) {
                $('.list').html(data.view);
                $('.js-form').html(data.js);
                $(".toggle").bootstrapToggle('destroy')
                $(".toggle").bootstrapToggle();

                $('.testtooltip').popover({
                    container: 'body'
                    // content: $('#myPopoverContent').html(),
                    // html: true,
                    // trigger: "hover"
                });
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

    $("#driver_id").on('change', function () {
        // Wait icon
        $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
        var link = $("#download_excel").attr('data-href') + '&from_date=' + $("#fromDate").val() + '&to_date=' + $("#toDate").val() + '&driver_id=' + $("#driver_id").val();
        $("#download_excel").attr('href', link);
        $.ajax({
            url: '{{url("/captainDashboard/orders")}}' + '?from_date=' + $("#fromDate").val() + '&to_date=' + $("#toDate").val() + '&search=' + $("#search-field").val() + '&type={{$type}}' + '&driver_id=' + $("#driver_id").val(),
            type: 'get',
            data: $("#search-form").serialize(),
            success: function (data) {
                $('.list').html(data.view);
                $('.js-form').html(data.js);
                $(".toggle").bootstrapToggle('destroy')
                $(".toggle").bootstrapToggle();

                $('.testtooltip').popover({
                    container: 'body'
                    // content: $('#myPopoverContent').html(),
                    // html: true,
                    // trigger: "hover"
                });
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

    // Select All Orders function For Print
    function do_this() {
        var checkboxes = document.getElementsByName('select[]');
        var button = document.getElementById('toggle');

        if (button.value == 'select') {
            for (var i in checkboxes) {
                checkboxes[i].checked = 'FALSE';
            }
            button.value = 'deselect'
        } else {
            for (var i in checkboxes) {
                checkboxes[i].checked = '';
            }
            button.value = 'select';
        }
    }

    function getChcked() {
        var form = document.getElementById('myform');
        var chks = form.querySelectorAll('input[type="checkbox"]');
        var checked = [];
        for (var i = 0; i < chks.length; i++) {
            if (chks[i].checked) {
                checked.push(chks[i].value)
            }
        }
        return checked;
    }

</script>

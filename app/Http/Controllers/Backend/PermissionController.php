<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\TypePermission;
use Response;
use Flash;

class PermissionController extends Controller
{

    // Get All Permissions
    public function index()
    {
        $permissions      = Permission::orderBy('id', 'desc')->paginate(50);
        $typePermission   = TypePermission::all();
        return view('backend.permissions.index', compact('permissions', 'typePermission'));
    }

    public function create()
    {
        //
    }

    // Added New Permissions
    public function store(Request $request)
    {
          $this->validate($request,[
             'name'                 =>'required',
             'type_permission_id'   => 'required'
          ]);

          $permission                       = new Permission();
          $permission->name                 = $request->input("name");
          $permission->type_permission_id   = $request->input("type_permission_id");
          $permission->save();

          // Function Add Action In Activity Log By Samir
          addActivityLog('Add New Permission ' . $permission->name , ' اضافة صلاحية جديدة ' . $permission->name, 16); // Helper Function in App\Helpers.php

          Flash::success(__('backend.Permission_saved_successfully'));
          return redirect()->route('mngrAdmin.permissions.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    // Update Permission
    public function update(Request $request, $id)
    {
        $this->validate($request,[
           'name'                 =>'required',
           'type_permission_id'   => 'required'
        ]);

        $editPermission                       = Permission::find($id);
        $editPermission->name                 = $request->input("name");
        $editPermission->type_permission_id   = $request->type_permission_id;
        $editPermission->save();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Edit Permission ' . $editPermission->name , ' تعديل الصلاحية ' . $editPermission->name, 16); // Helper Function in App\Helpers.php

        Flash::success(__('backend.Permission_Updated_successfully'));
        return redirect()->route('mngrAdmin.permissions.index');
    }

    // Delete Permission
    public function destroy($id)
    {
        $delPermission = Permission::find($id);
        $delPermission->delete();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Delete Permission ' . $delPermission->name , ' حذف الصلاحية ' . $delPermission->name, 16); // Helper Function in App\Helpers.php

        Flash::success(__('backend.Permission_Deleted_successfully'));
        return redirect()->route('mngrAdmin.permissions.index');
    }

}

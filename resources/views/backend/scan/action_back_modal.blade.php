<!-- Modal For Action back-->
<div class="modal fade" id="actionBackModal" tabindex="-1" role="dialog" aria-labelledby="actionBackModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">{{__('backend.action_back')}}</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div class="group-control col-md-10 col-sm-10">
                            <select id="action_status" name="action_status" class="form-control action_status" required>
                                <option value="" data-display="Select">{{__('backend.status')}}</option>
                                <option value="2">{{__('backend.received')}} </option>
                                <option value="3">{{__('backend.delivered')}} </option>
                                <option value="5">{{__('backend.recalled')}} </option>
                                <option value="8">{{__('backend.rejected')}} </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-6"></div>
                    <div class="col-md-12">
                        <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal"
                                style="margin-left: 15px;">Close
                        </button>
                        <button type="submit" class="btn btn-primary pull-right action_back" id="action_back"
                        >{{__('backend.action_back')}}</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

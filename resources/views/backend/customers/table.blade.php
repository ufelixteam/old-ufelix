<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        @if($customers->count())
            <table class="table table-condensed table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('backend.name')}}</th>
                    <th>{{__('backend.mobile')}}</th>
                    <th>{{__('backend.block')}}</th>
                    <th>{{__('backend.verify_email')}}</th>
                    <th>{{__('backend.verfiy_mobile')}}</th>
                    <th>{{__('backend.created_at')}}</th>
                    <th class="text-right"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($customers as $customer)
                    <tr @if(permission('showCustomer')) class='clickable-row'
                        data-href="{{ route('mngrAdmin.customers.show', $customer->id) }}" @endif>
                        <td>{{$customer->id}}</td>
                        <td>{{$customer->name}}</td>
                        <td>{{$customer->mobile}}</td>

                        <td class="exclude-td">
                            <input data-id="{{$customer->id}}" data-size="mini" class="toggle change_block"
                                   {{$customer->is_block == 1 ? 'checked' : ''}} data-onstyle="danger" type="checkbox"
                                   data-style="ios" data-on="Yes" data-off="No">
                        </td>

                        <td class="exclude-td">

                            <input data-id="{{$customer->id}}" data-size="mini" class="toggle verify_email"
                                   {{$customer->is_verify_email == 1 ? 'checked' : ''}} data-onstyle="success"
                                   type="checkbox" data-style="ios" data-on="Yes" data-off="No">
                        </td>
                        <td class="exclude-td">

                            <input data-id="{{$customer->id}}" data-size="mini" class="toggle verify_mobile"
                                   {{$customer->is_verify_mobile == 1 ? 'checked' : ''}} data-onstyle="success"
                                   type="checkbox" data-style="ios" data-on="Yes" data-off="No">

                        </td>

                        <td>{{ $customer->created_at }}</td>

                        <td class="exclude-td">

                        {{--                @if(permission('showCustomer')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->--}}
                        {{--                  <a class="btn btn-xs btn-primary" href="{{ route('mngrAdmin.customers.show', $customer->id) }}"><i class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>--}}
                        {{--                @endif--}}

                        @if(permission('editCustomer')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                            <a class="btn btn-xs btn-warning"
                               href="{{ route('mngrAdmin.customers.edit', $customer->id) }}"><i
                                    class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                            @endif

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {!! $customers->appends($_GET)->links() !!}

        @else
            <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
        @endif
    </div>
</div>

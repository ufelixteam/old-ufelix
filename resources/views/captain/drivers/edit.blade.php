@extends('captain.layouts.app')
@section('css')
    <title>{{__('captain.drivers')}} - {{__('captain.edit')}}</title>
    <style>

        .form-group {
            min-height: 94px;
            margin-bottom: 0px;
        }

        .input-group {
            -moz-box-shadow: none !important;
            -webkit-box-shadow: none !important;
            box-shadow: none !important;
        }

        .input-group {
            width: 100%;
        }
    </style>
@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i> {{__('captain.edit_driver')}} - {{$driver->name}}</h3>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <form action="{{ route('captainDashboard.drivers.update', $driver->id) }}" method="POST"
                  enctype="multipart/form-data" name="driver_edit">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group col-md-4 col-sm-4 @if($errors->has('name')) has-error @endif">
                    <label for="name-field">{{__('captain.name')}}: </label>
                    <input type="text" id="name-field" name="name" class="form-control"
                           value="{{is_null(old("name")) ? $driver->name : old("name")}}"/>
                    @if($errors->has("name"))
                        <span class="help-block">{{ $errors->first("name") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-4 col-sm-4 @if($errors->has('mobile')) has-error @endif">
                    <label for="mobile-field">{{__('captain.mobile_number')}}: </label>
                    <input type="text" id="mobile-field" name="mobile" maxlength="11" class="form-control"
                           value="{{is_null(old("mobile")) ? $driver->mobile : old("mobile")}}"/>
                    @if($errors->has("mobile"))
                        <span class="help-block">{{ $errors->first("mobile") }}</span>
                    @endif
                </div>

                <div class="form-group col-md-4 col-sm-4 @if($errors->has('password')) has-error @endif">
                    <label for="password-field">{{__('captain.new_password')}}: </label>
                    <input type="password" id="password-field" name="password" class="form-control" value=""/>
                    @if($errors->has("password"))
                        <span class="help-block">{{ $errors->first("password") }}</span>
                    @endif
                </div>

                <div class="well well-sm col-md-12 col-xs-12 col-sm-12" style="margin-top: 20px">
                    <button type="submit" id="added_driver"
                            class="btn btn-primary">{{__('captain.save_edits')}}</button>
                    <a class="btn btn-link pull-right" href="{{route('captainDashboard.drivers.index')}}"><i
                            class="glyphicon glyphicon-backward"></i>{{__('captain.back')}}</a>
                </div>


                <input type="hidden" name="driver_id" value="{{$driver->id}}">

            </form>
        </div>
    </div>

@endsection
@section('scripts')

    <script type="text/javascript">
        $(function () {

            $("form[name='driver_edit']").validate({
                rules: {
                    name: {
                        required: true,
                    },
                    mobile: {
                        required: true,
                        number: true,
                        digits: true,
                        rangelength: [11, 11],
                    },
                },

// Specify validation error messages
                messages: {
                    name: {
                        required: "{{__('captain.Please_Enter_driver_Name')}}",
                    }, "{{__('captain.Please_Enter_Correct_E-mail')}}"
                    mobile: {
                        rangelength: "{{__('captain.Mobile_Must_Be11_Digits')}}",
                        required: "{{__('captain.Please_Enter_Driver_Mobile_Number')}}",
                        number: "{{__('captain.Please_Enter_Avalid_Number')}}"

                    },

                },
                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                },
                highlight: function (element) {
                    $(element).parent().addClass('error')
                },
                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                }
            });
        })
    </script>
@endsection

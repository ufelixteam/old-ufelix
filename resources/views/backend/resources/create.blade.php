@extends('backend.layouts.app')
@section('css')
  <title>{{__('backend.resources')}} - {{__('backend.create')}}</title>
@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-plus"></i> {{__('backend.resources')}} / {{__('backend.create')}} </h3>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('mngrAdmin.resources.store') }}" method="POST" name="validateForm">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                  <div class="form-group col-md-6 col-sm-6 @if($errors->has('name')) has-error @endif">
                       <label for="name-field">{{__('backend.name')}}</label>
                       <input type="text" id="name-field" name="name" class="form-control" value="{{ old("name") }}"/>
                       @if($errors->has("name"))
                        <span class="help-block">{{ $errors->first("name") }}</span>
                       @endif
                  </div>
                  <div class="form-group col-md-6 col-sm-6 @if($errors->has('email')) has-error @endif">
                     <label for="email-field">{{__('backend.email')}}</label>
                     <input type="text" id="email-field" name="email" class="form-control" value="{{ old("email") }}"/>
                     @if($errors->has("email"))
                      <span class="help-block">{{ $errors->first("email") }}</span>
                     @endif
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-6 col-sm-6 @if($errors->has('started_at')) has-error @endif">
                     <label for="started_at-field">{{__('backend.started_at')}}</label>
                     <div class='input-group date datepicker2'>
                        <input type="text" id="started_at-field" name="started_at" class="form-control date-picker" value="{{ old("started_at") }}"/>
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                     </div>
                     @if($errors->has("started_at"))
                      <span class="help-block">{{ $errors->first("started_at") }}</span>
                     @endif
                  </div>
                  <div class="form-group col-md-6 col-sm-6 @if($errors->has('renew_at')) has-error @endif">
                     <label for="renew_at-field">{{__('backend.renew_at')}}</label>
                     <div class='input-group date datepicker2'>
                        <input type="text" id="renew_at-field" name="renew_at" class="form-control date-picker" value="{{ old("renew_at") }}"/>
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                     </div>
                     @if($errors->has("renew_at"))
                      <span class="help-block">{{ $errors->first("renew_at") }}</span>
                     @endif
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-6 col-sm-6 @if($errors->has('cost')) has-error @endif">
                     <label for="cost-field">{{__('backend.cost')}}</label>
                     <input type="text" id="cost-field" name="cost" class="form-control" value="{{ old("cost") }}"/>
                     @if($errors->has("cost"))
                      <span class="help-block">{{ $errors->first("cost") }}</span>
                     @endif
                  </div>
                  <div class="form-group col-md-6 col-sm-6 @if($errors->has('website_link')) has-error @endif">
                     <label for="website_link-field">{{__('backend.Website_Link')}}</label>
                     <input type="text" id="website_link-field" name="website_link" class="form-control" value="{{ old("website_link") }}"/>
                     @if($errors->has("website_link"))
                      <span class="help-block">{{ $errors->first("website_link") }}</span>
                     @endif
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-6 col-sm-6 @if($errors->has('username')) has-error @endif">
                     <label for="username-field">{{__('backend.username')}}</label>
                     <input type="text" id="username-field" name="username" class="form-control" value="{{ old("username") }}"/>
                     @if($errors->has("username"))
                      <span class="help-block">{{ $errors->first("username") }}</span>
                     @endif
                  </div>
                  <div class="form-group col-md-6 col-sm-6 @if($errors->has('password')) has-error @endif">
                     <label for="password-field">{{__('backend.password')}}</label>
                     <input type="text" id="password-field" name="password" class="form-control" value="" autocomplete="off"/>
                     @if($errors->has("password"))
                      <span class="help-block">{{ $errors->first("password") }}</span>
                     @endif
                  </div>
                </div
                <div class="well well-sm col-md-12 col-sm-12">
                    <button type="submit" class="btn btn-primary">{{__('backend.create')}}</button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.resources.index') }}"><i class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
   <script type="text/javascript">
  $(function() {
    $("form[name='validateForm']").validate({
      // Specify validation rules
      rules: {
        name: {
          required: true,
        },
        email: {
          required: true,
          email: true,
        },
        started_at: {
          required: true,
          date: true,
        },
        renew_at: {
          required: true,
          date: true,
        },
        cost: {
          required: true,
          number: true,
        },
        website_link: {
          required: true,
          email: true,
        },
        username: {
          required: true,
        },
        password: {
          required: true,
        },
      },
      // Specify validation error messages
      messages: {
        name: {
          required: "{{__('backend.Please_Enter_Resource_Name')}}",
        },
        email: {
          required: "{{__('backend.Please_Enter_Resource_Email')}}",
          email: "{{__('backend.Please_Enter_Correct_E-mail')}}",
        },
        started_at: {
          required: "{{__('backend.Please_Enter_Started_At')}}",
          date: "{{__('backend.Please_enter_avalid_date')}}",
        },
        renew_at: {
          required: "{{__('backend.Please_Enter_Renew_At')}}",
          date: "{{__('backend.Please_enter_avalid_date')}}",
        },
        cost: {
          required: "{{__('backend.Please_Enter_The_Cost')}}",
          number: "{{__('backend.Please_enter_avalid_price')}}",
        },
        website_link: {
          required: "{{__('backend.Please_Enter_Website_Link')}}",
          email: "{{__('backend.Please_Enter_Correct_Website_Link')}}",
        },
        username: {
          required: "{{__('backend.Please_Enter_Username')}}",
        },
        password: {
          required: "{{__('backend.Please_Enter_Password')}}",
        },
      },

      errorPlacement: function(error, element) {
          $(element).parents('.form-group').append(error)
      },
      highlight: function (element) {
          $(element).parent().addClass('error')
      },
      unhighlight: function (element) {
          $(element).parent().removeClass('error')
      },
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function(form) {
          form.submit();
      }
    });
  })
</script>
@endsection

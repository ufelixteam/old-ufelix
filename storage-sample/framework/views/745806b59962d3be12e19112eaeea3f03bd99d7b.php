<?php $__env->startSection('css'); ?>
<title><?php echo e(__('backend.stores')); ?></title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> <?php echo e(__('backend.stores')); ?>

            <?php if(permission('addStore')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
              <a class="btn btn-success pull-right" href="<?php echo e(route('mngrAdmin.stores.create')); ?>"><i class="glyphicon glyphicon-plus"></i> <?php echo e(__('backend.add_store')); ?></a>
            <?php endif; ?>
        </h3>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12">
            <?php if($stores->count()): ?>
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th><?php echo e(__('backend.id')); ?>#</th>
                            <th><?php echo e(__('backend.name')); ?></th>
                            <th><?php echo e(__('backend.mobile')); ?></th>
                            <th><?php echo e(__('backend.government')); ?></th>
                            <th><?php echo e(__('backend.status')); ?></th>
                            <th><?php echo e(__('backend.pickup_default')); ?></th>
                            <th class="text-right"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $stores; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $store): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($store->id); ?></td>
                                <td><?php echo e($store->name); ?></td>
                                <td><?php echo e($store->mobile); ?></td>
                                <td>
                                  <?php if(! empty($store->governorate)): ?>
                                    <?php if(session()->has('lang') == 'en'): ?>
                                      <?php echo e($store->governorate->name_ar); ?>

                                    <?php else: ?>
                                      <?php echo e($store->governorate->name_en); ?>

                                    <?php endif; ?>
                                  <?php else: ?>

                                  <?php endif; ?>
                                </td>
                                <td><?php echo $store->status_span; ?></td>
                                <td><?php echo $store->pickup_default_span; ?></td>
                                <td class="text-right">
                                    <?php if(permission('showStore')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                      <a class="btn btn-xs btn-primary" href="<?php echo e(route('mngrAdmin.stores.show', $store->id)); ?>"><i class="glyphicon glyphicon-eye-open"></i> <?php echo e(__('backend.view')); ?></a>
                                    <?php endif; ?>
                                    <?php if(permission('editStore')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                      <a class="btn btn-xs btn-warning" href="<?php echo e(route('mngrAdmin.stores.edit', $store->id)); ?>"><i class="glyphicon glyphicon-edit"></i> <?php echo e(__('backend.edit')); ?></a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
                <?php echo $stores->render(); ?>

            <?php else: ?>
                <h3 class="text-center alert alert-warning"><?php echo e(__('backend.No_result_found')); ?></h3>
            <?php endif; ?>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/stores/index.blade.php ENDPATH**/ ?>
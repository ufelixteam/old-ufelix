<div id="sidebar-nav" class="sidebar">
	<div class="sidebar-scroll">
		<nav>
			<ul class="nav">
				<li><a href="<?php echo e(url('mngrAdmin')); ?>" class="<?php echo e(Request::is('mngrAdmin/dashboard')  ? 'active' : ''); ?>">
                        <i class="lnr lnr-home"></i> <span><?php echo e(__('backend.dashboard')); ?></span>
                    </a>
				</li>
				<li>
					<a href="#catPages" data-toggle="collapse" class="<?php echo e(Request::is('mngrAdmin/corporates*') || Request::is('mngrAdmin/customers*') || Request::is('mngrAdmin/agents*')  ? 'active' : 'collapsed'); ?> "
                       aria-expanded="<?php echo e(Request::is('mngrAdmin/corporates*') || Request::is('mngrAdmin/customers*') || Request::is('mngrAdmin/agents*')  ? 'true' : 'false'); ?> ">
                        <i class="lnr lnr-users"></i> <span><?php echo e(__('backend.clients')); ?></span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
					<div id="catPages" class="collapse <?php echo e(Request::is('mngrAdmin/corporates*') || Request::is('mngrAdmin/customers*') || Request::is('mngrAdmin/agents*')  ? 'in' : ''); ?>

                        " aria-expanded="<?php echo e(Request::is('mngrAdmin/agents*') || Request::is('mngrAdmin/customers*') || Request::is('mngrAdmin/corporates*')  ? 'true' : 'false'); ?>">
						<ul class="nav">
                        <?php if(permission('customers')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                            <li> <a href="<?php echo URL::asset('mngrAdmin/customers'); ?>"><i class="fa fa-users"></i><span><?php echo e(__('backend.individual_clients')); ?></span></a> </li>
                        <?php endif; ?>

                        <?php if(permission('corporates')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                            <li> <a href="<?php echo URL::asset('mngrAdmin/corporates'); ?>"><i class="fa fa-building"></i><span><?php echo e(__('backend.corporates')); ?></span></a> </li>
                        <?php endif; ?>

                        <?php if(permission('agents')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                            <li> <a href="<?php echo URL::asset('mngrAdmin/agents'); ?>"><i class="fa fa-user-secret"></i><span><?php echo e(__('backend.agents')); ?></span></a> </li>
                            <?php endif; ?>

						</ul>
					</div>
				</li>

                <li>
                    <a href="#carPages" data-toggle="collapse" class="<?php echo e(Request::is('mngrAdmin/trucks*') || Request::is('mngrAdmin/drivers*') || Request::is('mngrAdmin//track_drivers/all')  ? 'active' : 'collapsed'); ?> "
                       aria-expanded="<?php echo e(Request::is('mngrAdmin/trucks*') || Request::is('mngrAdmin/drivers*') || Request::is('mngrAdmin//track_drivers/all')  ? 'true' : 'false'); ?> ">
                        <i class="lnr lnr-car"></i> <span><?php echo e(__('backend.drivers')); ?></span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                    <div id="carPages" class="collapse <?php echo e(Request::is('mngrAdmin/trucks*') || Request::is('mngrAdmin/drivers*') || Request::is('mngrAdmin//track_drivers/all')  ? 'in' : ''); ?>

                        " aria-expanded="<?php echo e(Request::is('mngrAdmin/trucks*') || Request::is('mngrAdmin/drivers*') || Request::is('mngrAdmin//track_drivers/all')  ? 'true' : 'false'); ?>">
                        <ul class="nav">
                        <?php if(permission('drivers')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                            <li> <a href="<?php echo URL::asset('mngrAdmin/drivers'); ?>"><i class="fa fa-car"></i><span><?php echo e(__('backend.drivers')); ?></span></a> </li>
                        <?php endif; ?>

                        <?php if(permission('Vehicles')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                            <li> <a href="<?php echo URL::asset('mngrAdmin/trucks'); ?>"><i class="fa fa-truck"></i><span><?php echo e(__('backend.vehicles')); ?></span></a> </li>
                        <?php endif; ?>
                        <li> <a href="<?php echo URL::asset('mngrAdmin/track_drivers/all'); ?>"><i class="fa fa-map"></i><span><?php echo e(__('backend.map')); ?></span></a> </li>
                        </ul>
                    </div>
                </li>
                <?php if(permission('wallets')): ?>
                <li>
                    <a href="#walletPages" data-toggle="collapse" class="<?php echo e(Request::is('mngrAdmin/wallets*')  ? 'active' : 'collapsed'); ?> "
                       aria-expanded="<?php echo e(Request::is('mngrAdmin/wallets*')  ? 'true' : 'false'); ?> ">
                        <i class="fa fa-money "></i> <span><?php echo e(__('backend.the_wallets')); ?></span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                    <div id="walletPages" class="collapse <?php echo e(Request::is('mngrAdmin/wallets*')  ? 'in' : ''); ?>

                        " aria-expanded="<?php echo e(Request::is('mngrAdmin/wallets*')  ? 'true' : 'false'); ?>">
                        <ul class="nav">
                            <li> <a href="<?php echo URL::asset('mngrAdmin/wallets?type=1'); ?>"><i class="fa fa-info"></i><span><?php echo e(__('backend.drivers')); ?></span></a> </li>
                            <li> <a href="<?php echo URL::asset('mngrAdmin/wallets?type=2'); ?>"><i class="fa fa-check"></i><span><?php echo e(__('backend.corporates')); ?></span></a> </li>
                            <li> <a href="<?php echo URL::asset('mngrAdmin/wallets?type=3'); ?>"><i class="fa fa-close"></i><span><?php echo e(__('backend.agents')); ?></span></a> </li>
                            <li> <a href="<?php echo URL::asset('mngrAdmin/wallets/1'); ?>"><i class="fa fa-cc"></i><span><?php echo e(__('backend.Ufelix_Account')); ?></span></a> </li>
                        </ul>
                    </div>
                </li>
                <?php endif; ?>

                <?php if(permission('Invoices')): ?>
                    <li>
                        <a href="#invoicePages" data-toggle="collapse" class="<?php echo e(Request::is('mngrAdmin/invoices*')  ? 'active' : 'collapsed'); ?> "
                           aria-expanded="<?php echo e(Request::is('mngrAdmin/invoices*')  ? 'true' : 'false'); ?> ">
                            <i class="fa fa-usd"></i> <span><?php echo e(__('backend.the_invoices')); ?></span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="invoicePages" class="collapse <?php echo e(Request::is('mngrAdmin/invoices*')  ? 'in' : ''); ?>

                            " aria-expanded="<?php echo e(Request::is('mngrAdmin/invoices*')  ? 'true' : 'false'); ?>">
                            <ul class="nav">
                                <li> <a href="<?php echo URL::asset('mngrAdmin/invoices?type=1'); ?>"><i class="fa fa-info"></i><span><?php echo e(__('backend.drivers')); ?></span></a> </li>
                                <li> <a href="<?php echo URL::asset('mngrAdmin/invoices?type=2'); ?>"><i class="fa fa-check"></i><span><?php echo e(__('backend.corporates')); ?></span></a> </li>
                                <li> <a href="<?php echo URL::asset('mngrAdmin/invoices?type=3'); ?>"><i class="fa fa-close"></i><span><?php echo e(__('backend.agents')); ?></span></a> </li>
                                <li> <a href="<?php echo URL::asset('mngrAdmin/orders/showOrders/individuals'); ?>"><i class="fa fa-users"></i><span><?php echo e(__('backend.individuals')); ?></span></a> </li>
                            </ul>
                        </div>
                    </li>
                <?php endif; ?>
                <?php if(permission('orders')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <li >
                    <a href="#orderPages" data-toggle="collapse" class="<?php echo e(Request::is('mngrAdmin/orders*')  ? 'active' : 'collapsed'); ?> "
                       aria-expanded="<?php echo e(Request::is('mngrAdmin/orders*')  ? 'true' : 'false'); ?> ">
                        <i class="fa fa-tree"></i> <span><?php echo e(__('backend.the_orders')); ?></span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                    <div id="orderPages" class="collapse <?php echo e(Request::is('mngrAdmin/orders*')  ? 'in' : ''); ?>

                        " aria-expanded="<?php echo e(Request::is('mngrAdmin/orders*')  ? 'true' : 'false'); ?>">
                        <ul class="nav">
                        <li> <a href="<?php echo URL::asset('mngrAdmin/orders?type=pending'); ?>"><i class="fa fa-info"></i><span><?php echo e(__('backend.Pending_Orders')); ?></span></a> </li>
                        <li> <a href="<?php echo URL::asset('mngrAdmin/orders?type=accept'); ?>"><i class="fa fa-check"></i><span><?php echo e(__('backend.Accepted_Orders')); ?></span></a> </li>
                        <li> <a href="<?php echo URL::asset('mngrAdmin/orders?type=receive'); ?>"><i class="fa fa-battery-half"></i><span><?php echo e(__('backend.Received_Orders')); ?></span></a> </li>
                        <li> <a href="<?php echo URL::asset('mngrAdmin/orders?type=deliver'); ?>"><i class="fa fa-battery"></i><span><?php echo e(__('backend.Delivered_Orders')); ?></span></a> </li>
                        <li> <a href="<?php echo URL::asset('mngrAdmin/orders?type=cancel'); ?>"><i class="fa fa-close"></i><span><?php echo e(__('backend.Cancelled_Orders')); ?></span></a> </li>
                        <li> <a href="<?php echo URL::asset('mngrAdmin/orders?type=recall'); ?>"><i class="fa fa-history"></i><span><?php echo e(__('backend.Recalled_Orders')); ?></span></a> </li>
                        <li> <a href="<?php echo URL::asset('mngrAdmin/orders?type=waiting'); ?>"><i class="fa fa-info"></i><span><?php echo e(__('backend.Waiting_Orders')); ?></span></a> </li>
                        
                        <li> <a href="<?php echo URL::asset('mngrAdmin/orders?type=expired_waiting'); ?>"><i class="fa fa-info"></i><span><?php echo e(__('backend.Expired_Waiting_Orders')); ?></span></a> </li>
                        <li> <a href="<?php echo URL::asset('mngrAdmin/collection_orders?type=0'); ?>"><i class="fa fa-file-excel-o"></i><span><?php echo e(__('backend.Collection_Order_Excel')); ?></span></a> </li>
                    </ul>
                    </div>
                </li>
                <?php endif; ?>

               
                <li>
                    <a href="#pickupPages" data-toggle="collapse" class="<?php echo e(Request::is('mngrAdmin/pickups*') || Request::is('mngrAdmin/pickup_price_lists*') || Request::is('mngrAdmin/pickup_stores*')  ? 'active' : 'collapsed'); ?> "
                       aria-expanded="<?php echo e(Request::is('mngrAdmin/pickups*') || Request::is('mngrAdmin/pickup_price_lists*') || Request::is('mngrAdmin/pickup_stores*')  ? 'true' : 'false'); ?> ">
                        <i class="fa fa-money "></i> <span><?php echo e(__('backend.the_PickUp')); ?></span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                    <div id="pickupPages" class="collapse <?php echo e(Request::is('mngrAdmin/pickups*')  || Request::is('mngrAdmin/pickup_price_lists*') || Request::is('mngrAdmin/pickup_stores*') ? 'in' : ''); ?>

                        " aria-expanded="<?php echo e(Request::is('mngrAdmin/pickups*') || Request::is('mngrAdmin/pickup_price_lists*') || Request::is('mngrAdmin/pickup_stores*') ? 'true' : 'false'); ?>">
                        <ul class="nav">
                            <?php if(permission('Pickups')): ?>
                           <li><a href="<?php echo URL::asset('mngrAdmin/pickups'); ?>"><i class="fa fa-info"></i><span><?php echo e(__('backend.packup_Orders')); ?></span></a> </li>
                            <?php endif; ?>


                           <?php if(permission('stores')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                            <li> <a href="<?php echo URL::asset('mngrAdmin/stores'); ?>"><i class="fa fa-check"></i><span><?php echo e(__('backend.pickup_store')); ?></span></a> </li>

                            <?php endif; ?>

                            <li> <a href="<?php echo URL::asset('mngrAdmin/pickup_price_lists'); ?>"><i class="fa fa-close"></i><span><?php echo e(__('backend.pickup_price_list')); ?></span></a> </li>

                 
                        </ul>
                    </div>
                </li>



                <?php if(permission('collectionOrders')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <li>  <a class="<?php echo e(Request::is('mngrAdmin/collection_orders*') ? 'active' : ''); ?>"
                     href="<?php echo URL::asset('mngrAdmin/collection_orders?type=1'); ?>">
                        <i class="fa fa-file-text"></i> <span><?php echo e(__('backend.Collection_Order_Form')); ?></span>
                    </a>
                </li>
                <?php endif; ?>

                <?php if(permission('scanQRCode')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <li><a class="<?php echo e(Request::is('mngrAdmin/scan_qrcode*') ? 'active' : ''); ?>"
                    href="<?php echo URL::asset('mngrAdmin/scan_qrcode'); ?>">
                        <i class="fa fa-camera"></i> <span><?php echo e(__('backend.scan')); ?></span>
                    </a>
                </li>
                <?php endif; ?>

                <?php if(permission('notifications')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                    <li>
                        <a href="#notifPages" data-toggle="collapse" class="<?php echo e(Request::is('mngrAdmin/notifications*')  ? 'active' : 'collapsed'); ?> "
                           aria-expanded="<?php echo e(Request::is('mngrAdmin/notifications*')  ? 'true' : 'false'); ?> ">
                            <i class="lnr lnr-alarm"></i> <span><?php echo e(__('backend.notifications')); ?></span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="notifPages" class="collapse <?php echo e(Request::is('mngrAdmin/notifications*')  ? 'in' : ''); ?>

                            " aria-expanded="<?php echo e(Request::is('mngrAdmin/notifications*')  ? 'true' : 'false'); ?>">
                            <ul class="nav">
                                <li> <a href="<?php echo URL::asset('mngrAdmin/notifications'); ?>"><i class="fa fa-bell-o"></i><span><?php echo e(__('backend.notifications')); ?></span></a> </li>
                                <li> <a href="<?php echo URL::asset('mngrAdmin/notifications/types'); ?>"><i class="fa fa-bell-o"></i><span><?php echo e(__('backend.Notifications_Types')); ?></span></a> </li>
                            </ul>
                        </div>
                    </li>
                <?php endif; ?>
                <li>
                    <a href="#shipmentPages" data-toggle="collapse" class="<?php echo e(Request::is('mngrAdmin/governoratePrices*') || Request::is('mngrAdmin/shipment_tools*') || Request::is('mngrAdmin/shipment_tool_orders*')  ? 'active' : 'collapsed'); ?> "
                       aria-expanded="<?php echo e(Request::is('mngrAdmin/governoratePrices*') || Request::is('mngrAdmin/shipment_tools*') || Request::is('mngrAdmin/shipment_tool_orders*')  ? 'true' : 'false'); ?> ">

                        <i class="fa fa-gavel"></i> <span><?php echo e(__('backend.shipment')); ?></span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                    <div id="shipmentPages" class="collapse <?php echo e(Request::is('mngrAdmin/governoratePrices*') || Request::is('mngrAdmin/shipment_tools*') || Request::is('mngrAdmin/shipment_tool_orders*')  ? 'in' : ''); ?>

                        " aria-expanded="<?php echo e(Request::is('mngrAdmin/governoratePrices*') || Request::is('mngrAdmin/shipment_tools*') || Request::is('mngrAdmin/shipment_tool_orders*')  ? 'true' : 'false'); ?>">
                        <ul class="nav">
                    <?php if(permission('shipmentTools')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                        <li> <a href="<?php echo URL::asset('mngrAdmin/shipment_tools'); ?>"><i class="fa fa-gavel"></i><span><?php echo e(__('backend.shipment_material')); ?></span></a> </li>
                    <?php endif; ?>

                    <?php if(permission('shipmentOrders')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                        <li> <a href="<?php echo URL::asset('mngrAdmin/shipment_tool_orders'); ?>"><i class="fa fa-envelope"></i><span><?php echo e(__('backend.shipment_material_orders')); ?></span></a> </li>
                    <?php endif; ?>

                    <?php if(permission('shipmentDestinations')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                        <li> <a href="<?php echo URL::asset('mngrAdmin/governoratePrices'); ?>"><i class="fa fa-arrow-right"></i><span><?php echo e(__('backend.shipment_destinations')); ?></span></a> </li>
                        <?php endif; ?>
                    </ul>
                    </div>
                </li>

                <li>  <a class="<?php echo e(Request::is('mngrAdmin/list_receivers*') ? 'active' : ''); ?>"  href="<?php echo url('mngrAdmin/list_receivers'); ?>">
                    <i class="fa fa-mobile"></i> <span><?php echo e(__('backend.list_receivers')); ?></span>
                    </a>
                </li>


                <li>
                    <a href="#permissionPages" data-toggle="collapse" class="<?php echo e(Request::is('mngrAdmin/admin_roles*') || Request::is('mngrAdmin/permissions*') || Request::is('mngrAdmin/type_permissions*')  ? 'active' : 'collapsed'); ?> "
                       aria-expanded="<?php echo e(Request::is('mngrAdmin/admin_roles*') || Request::is('mngrAdmin/permissions*') || Request::is('mngrAdmin/type_permissions*')  ? 'true' : 'false'); ?> ">

                        <i class="fa fa-gavel"></i> <span><?php echo e(__('backend.permissions')); ?></span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                    <div id="permissionPages" class="collapse <?php echo e(Request::is('mngrAdmin/admin_roles*') || Request::is('mngrAdmin/permissions*') || Request::is('mngrAdmin/type_permissions*')  ? 'in' : ''); ?>

                        " aria-expanded="<?php echo e(Request::is('mngrAdmin/admin_roles*') || Request::is('mngrAdmin/permissions*') || Request::is('mngrAdmin/type_permissions*')  ? 'true' : 'false'); ?>">
                        <ul class="nav">
                    <?php if(permission('permissions')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                        <li > <a href="<?php echo route('mngrAdmin.permissions.index'); ?>"><i class="fa fa-lock"></i><span><?php echo e(__('backend.permissions')); ?></span></a> </li>
                    <?php endif; ?>

                    <?php if(permission('permissionTypes')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                        <li > <a href="<?php echo route('mngrAdmin.type_permissions.index'); ?>"><i class="fa fa-lock"></i><span><?php echo e(__('backend.type_permission')); ?></span></a> </li>
                    <?php endif; ?>

                    <?php if(permission('adminRoles')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                        <li > <a href="<?php echo route('mngrAdmin.admin_roles.index'); ?>"><i class="fa fa-users"></i><span><?php echo e(__('backend.roles')); ?></span></a> </li>
                        <?php endif; ?>
                    </ul>
                    </div>
                </li>

                <?php if(permission('activityLog')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <li>  <a href="#logPages" data-toggle="collapse" class="<?php echo e(Request::is('mngrAdmin/activity_log*') || Request::is('mngrAdmin/activity_log_type*')   ? 'active' : 'collapsed'); ?> "
                          aria-expanded="<?php echo e(Request::is('mngrAdmin/activity_log*') || Request::is('mngrAdmin/activity_log_type*')  ? 'true' : 'false'); ?> ">
                        <i class="fa fa-tags"></i> <span><?php echo e(__('backend.activity_log')); ?></span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                    <div id="logPages" class="collapse <?php echo e(Request::is('mngrAdmin/activity_log*') || Request::is('mngrAdmin/activity_log_type*')  ? 'in' : ''); ?>

                        " aria-expanded="<?php echo e(Request::is('mngrAdmin/activity_log*') || Request::is('mngrAdmin/activity_log_type*')  ? 'true' : 'false'); ?>">
                        <ul class="nav">
                        <li > <a href="<?php echo route('mngrAdmin.activity_log.index'); ?>"><i class="fa fa-tag"></i></i><span><?php echo e(__('backend.activity_log')); ?></span></a> </li>
                        <li > <a href="<?php echo route('mngrAdmin.activity_log_type.index'); ?>"><i class="fa fa-tag"></i></i><span><?php echo e(__('backend.Activity_log_type')); ?></span></a> </li>
                    </ul>
                    </div>
                </li>
                <?php endif; ?>
                <li>
                    <a href="#otherPage" data-toggle="collapse" class="<?php echo e(Request::is('mngrAdmin/countries*') || Request::is('mngrAdmin/cities*') || Request::is('mngrAdmin/payment_methods*')
                  || Request::is('mngrAdmin/colors*')|| Request::is('mngrAdmin/weights*') || Request::is('mngrAdmin/types/*') ||
                   Request::is('mngrAdmin/sizes*') ||  Request::is('mngrAdmin/model_types*') || Request::is('mngrAdmin/governorates*')  ? 'active' : 'collapsed'); ?> "
                       aria-expanded="<?php echo e(Request::is('mngrAdmin/countries*') || Request::is('mngrAdmin/cities*') || Request::is('mngrAdmin/payment_methods*')
                  || Request::is('mngrAdmin/colors*')|| Request::is('mngrAdmin/weights*') || Request::is('mngrAdmin/types/*') ||
                   Request::is('mngrAdmin/sizes*') ||  Request::is('mngrAdmin/model_types*') || Request::is('mngrAdmin/governorates*')  ? 'true' : 'false'); ?> ">

                        <i class="fa fa-medkit"></i> <span><?php echo e(__('backend.others')); ?></span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                    <div id="otherPage" class="collapse <?php echo e(Request::is('mngrAdmin/countries*') || Request::is('mngrAdmin/cities*') || Request::is('mngrAdmin/payment_methods*')
                  || Request::is('mngrAdmin/colors*')|| Request::is('mngrAdmin/weights*') || Request::is('mngrAdmin/types/*') ||
                   Request::is('mngrAdmin/sizes*') ||  Request::is('mngrAdmin/model_types*') || Request::is('mngrAdmin/governorates*')  ? 'in' : ''); ?>

                        " aria-expanded="<?php echo e(Request::is('mngrAdmin/countries*') || Request::is('mngrAdmin/cities*') || Request::is('mngrAdmin/payment_methods*')
                  || Request::is('mngrAdmin/colors*')|| Request::is('mngrAdmin/weights*') || Request::is('mngrAdmin/types/*') ||
                   Request::is('mngrAdmin/sizes*') ||  Request::is('mngrAdmin/model_types*') || Request::is('mngrAdmin/governorates*')  ? 'true' : 'false'); ?>">
                        <ul class="nav">


                            <?php if(permission('stocks')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                            <li style="margin-top: 3px;">  <a class="<?php echo e(Request::is('mngrAdmin/stocks*') ? 'active' : ''); ?>"  href="<?php echo route('mngrAdmin.stocks.index'); ?>">
                                    <i class="fa fa-shopping-basket"></i> <span><?php echo e(__('backend.stocks')); ?></span>
                                </a>
                            </li>
                            <?php endif; ?>


                            <?php if(permission('promoCodes')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                            <li style="margin-top: 3px;">  <a class="<?php echo e(Request::is('mngrAdmin/promo_codes*') ? 'active' : ''); ?>"  href="<?php echo route('mngrAdmin.promo_codes.index'); ?>">
                                    <i class="fa fa-gift "></i> <span><?php echo e(__('backend.promo_codes')); ?></span>
                                </a>
                            </li>
                            <?php endif; ?>

                            <?php if(permission('androidVersions')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li style="margin-top: 3px;"> <a href="<?php echo e(url('mngrAdmin/android')); ?>"><i class="fa fa-android"></i><span><?php echo e(__('backend.Android_Versions')); ?></span></a> </li>
                            <?php endif; ?>

                            <?php if(permission('orderTypes')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li > <a href="<?php echo URL::asset('mngrAdmin/order_types'); ?>"><i class="fa fa-list-alt"></i><span><?php echo e(__('backend.Order_Types')); ?></span></a> </li>
                            <?php endif; ?>

                            <?php if(permission('resources')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li > <a href="<?php echo URL::asset('mngrAdmin/resources'); ?>"><i class="fa fa-list-alt"></i><span><?php echo e(__('backend.resources')); ?></span></a> </li>
                            <?php endif; ?>

                            <?php if(permission('vehicleModels')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li > <a href="<?php echo route('mngrAdmin.model_types.index'); ?>"><i class="fa fa-magnet"></i><span><?php echo e(__('backend.Vehicles_Model')); ?></span></a> </li>
                            <?php endif; ?>

                            <?php if(permission('vehicleTypes')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li > <a href="<?php echo e(url('mngrAdmin/vehicle')); ?>"><i class="fa fa-motorcycle"></i><span> <?php echo e(__('backend.Vehicle_Types')); ?></span></a> </li>
                            <?php endif; ?>

                            <?php if(permission('vehicleColors')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li > <a href="<?php echo route('mngrAdmin.colors.index'); ?>"><i class="fa fa-paint-brush"></i><span><?php echo e(__('backend.Vehicle_Colors')); ?></span></a> </li>
                            <?php endif; ?>

                            <?php if(permission('paymentMethods')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li > <a href="<?php echo route('mngrAdmin.payment_methods.index'); ?>"><i class="fa fa-credit-card"></i><span><?php echo e(__('backend.Payment_Methods')); ?></span></a> </li>
                            <?php endif; ?>

                            <?php if(permission('countries')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li > <a href="<?php echo route('mngrAdmin.countries.index'); ?>"><i class="fa fa-language"></i><span><?php echo e(__('backend.countries')); ?></span></a> </li>
                            <?php endif; ?>

                            <?php if(permission('governorates')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li > <a href="<?php echo route('mngrAdmin.governorates.index'); ?>"><i class="fa fa-language"></i><span><?php echo e(__('backend.governorates')); ?></span></a> </li>
                            <?php endif; ?>

                            <?php if(permission('cities')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li > <a href="<?php echo route('mngrAdmin.cities.index'); ?>"><i class="fa fa-language"></i><span><?php echo e(__('backend.cities')); ?></span></a> </li>
                            <?php endif; ?>

                            <?php if(permission('priceLists')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                            <li > <a href="<?php echo route('mngrAdmin.price_lists.index'); ?>"><i class="fa fa-money"></i><span><?php echo e(__('backend.Pricing_List')); ?></span></a> </li>
                            <?php endif; ?>

          					<li style="margin-top: 13px;"> <a href="<?php echo route('mngrAdmin.CorporateTarget.index'); ?>"><i class="fa fa-money"></i><span><?php echo e(__('backend.corporate_target')); ?></span></a> </li>

                            <?php if(permission('documentations')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li >   <a class="<?php echo e(Request::is('mngrAdmin/docs*') ? 'active' : ''); ?>"
                                  href="<?php echo URL::asset('mngrAdmin/docs'); ?>">
                                        <i class="fa fa-book"></i> <span><?php echo e(__('backend.documentations')); ?></span>
                                    </a>
                                </li>
                            <?php endif; ?>
                    </ul>
                    </div>
                </li>



                <?php if(permission('reports')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <li>  <a class="<?php echo e(Request::is('mngrAdmin/reports*') ? 'active' : ''); ?>"  href="<?php echo URL::asset('mngrAdmin/reports'); ?>">
                        <i class="fa fa-file-excel-o"></i> <span><?php echo e(__('backend.reports')); ?></span>
                    </a>
                </li>
                <?php endif; ?>

                <?php if(permission('contacts')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <li>  <a class="<?php echo e(Request::is('mngrAdmin/contacts*') ? 'active' : ''); ?>"  href="<?php echo route('mngrAdmin.contacts.index'); ?>">
                        <i class="fa fa-envelope"></i> <span><?php echo e(__('backend.Contact_Us')); ?></span>
                    </a>
                </li>
                <?php endif; ?>


                <li>
                    <a href="#websitePages" data-toggle="collapse" class="<?php echo e(Request::is('mngrAdmin/services*')  ? 'active' : 'collapsed'); ?> "
                       aria-expanded="<?php echo e(Request::is('mngrAdmin/services*') ? 'true' : 'false'); ?> ">
                        <i class="fa fa-asterisk"></i> <span><?php echo e(__('backend.website')); ?></span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>

                    <div id="websitePages" class="collapse <?php echo e(Request::is('mngrAdmin/services*')  ? 'in' : ''); ?>

                        " aria-expanded="<?php echo e(Request::is('mngrAdmin/services*')  ? 'true' : 'false'); ?>">
                        <ul class="nav">
                            <?php if(permission('services')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li > <a  class="<?php echo e(Request::is('mngrAdmin/services?type=1') ? 'active' : ''); ?>"
                                          href="<?php echo url('/mngrAdmin/services?type=1'); ?>">
                                        <i class="fa fa-glass"></i> <span><?php echo e(__('backend.services')); ?></span>
                                    </a>
                                </li>

                                <li > <a  class="<?php echo e(Request::is('mngrAdmin/services?type=2') ? 'active' : ''); ?>"
                                          href="<?php echo url('/mngrAdmin/services?type=2'); ?>">
                                        <i class="fa fa-cloud"></i> <span><?php echo e(__('backend.why_us')); ?></span>
                                    </a>
                                </li>

                                <li > <a  class="<?php echo e(Request::is('mngrAdmin/services?type=3') ? 'active' : ''); ?>"
                                          href="<?php echo url('/mngrAdmin/services?type=3'); ?>">
                                        <i class="fa fa-cloud"></i> <span><?php echo e(__('backend.slider')); ?></span>
                                    </a>
                                </li>

                                <li > <a  class="<?php echo e(Request::is('mngrAdmin/services?type=4') ? 'active' : ''); ?>"
                                          href="<?php echo url('/mngrAdmin/services?type=4'); ?>">
                                        <i class="fa fa-user"></i> <span><?php echo e(__('backend.drivers')); ?></span>
                                    </a>
                                </li>

                                <li > <a  class="<?php echo e(Request::is('mngrAdmin/services?type=5') ? 'active' : ''); ?>"
                                          href="<?php echo url('/mngrAdmin/services?type=5'); ?>">
                                        <i class="fa fa-building"></i> <span><?php echo e(__('backend.customers')); ?></span>
                                    </a>
                                </li>
                            <?php endif; ?>
                        
                        </ul>
                    </div>
                </li>

                <li>
                    <a href="#compPages" data-toggle="collapse" class="<?php echo e(Request::is('mngrAdmin/employees*') || Request::is('mngrAdmin/admins*') || Request::is('mngrAdmin/settings*')  ? 'active' : 'collapsed'); ?> "
                       aria-expanded="<?php echo e(Request::is('mngrAdmin/employees*') || Request::is('mngrAdmin/admins*') || Request::is('mngrAdmin/settings*')  ? 'true' : 'false'); ?> ">
                        <i class="lnr lnr-users"></i> <span><?php echo e(__('backend.company')); ?></span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>

                    <div id="compPages" class="collapse <?php echo e(Request::is('mngrAdmin/employees*') || Request::is('mngrAdmin/admins*') || Request::is('mngrAdmin/settings*')  ? 'in' : ''); ?>

                        " aria-expanded="<?php echo e(Request::is('mngrAdmin/settings*') || Request::is('mngrAdmin/admins*') || Request::is('mngrAdmin/employees*')  ? 'true' : 'false'); ?>">
                        <ul class="nav">
                        <?php if(permission('admins')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                            <li > <a  class="<?php echo e(Request::is('mngrAdmin/admins*') ? 'active' : ''); ?>"
                                      href="<?php echo route('mngrAdmin.admins.index'); ?>">
                                    <i class="fa fa-user"></i> <span><?php echo e(__('backend.admins')); ?></span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if(permission('editAboutUs')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                            <li> <a class="<?php echo e(Request::is('mngrAdmin/settings*') ? 'active' : ''); ?>"
                                    href="<?php echo url('mngrAdmin/settings'); ?>">
                                    <i class="lnr lnr-cog"></i> <span><?php echo e(__('backend.settings')); ?></span>
                                </a>
                            </li>
                            <?php endif; ?>

                        <?php if(permission('employees')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                            <li><a class="<?php echo e(Request::is('mngrAdmin/employees*') ? 'active' : ''); ?>"
                                 href="<?php echo route('mngrAdmin.employees.index'); ?>">
                                    <i class="fa fa-user-secret "></i> <span><?php echo e(__('backend.employees')); ?></span>
                                </a>
                            </li>
                            <?php endif; ?>

                        </ul>
                    </div>
                </li>


            </ul>
		</nav>
	</div>
</div>
<?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/layouts/sidemenu.blade.php ENDPATH**/ ?>
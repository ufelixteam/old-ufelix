@extends('frontend.profile.layouts.app')

@section('title')
    @lang('website.orderview')
@endsection
{{--@section('css')--}}

{{--    <link href="{{asset('public/profile/assets/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">--}}
{{--@endsection--}}

@section('content')

    <div class="container-fluid {{ app()->getLocale() == 'en' ? 'text-left' : 'text-right' }}">
        @include('flash::message')
    </div>
    <div class="row">

        <div class="col-12">
            <div class="">
                <div class="table-responsive m-t-40">
                    <h1 class="title">@lang('website.order_view')</h1>
                    @if(! empty($order) && $order)
                        <div class="container-fluid">

                            <div class=" {{app()->getLocale() == 'en' ? 'pull-right' : 'pull-left' }}">
                                @if($order->status == 2)
                                    <a href="{{ route('profile.orders.recall_if_dropped', $order->id) }}"
                                       class="btn btn-danger @if($order->recall_if_dropped) disabled @endif">
                                        {{__('backend.recall_if_dropped')}}
                                    </a>
                                @endif
                                @if($order->status == 5 || $order->status == 8 || ($order->status == 4 && $order->status_before_cancel != 0)|| ($order->status == 3 && $order->delivery_status == 2 && (!$order->is_received || !$settings))|| ($order->status == 3 && $order->delivery_status == 4 && (!$order->is_received || !$settings)))
                                    @if($order->warehouse_dropoff)
                                        <span class="text-info"> {{__('backend.warehouse')}}</span>
                                        <br>
                                    @endif
                                    @if($order->client_dropoff)
                                        <span class="text-warning"> {{__('backend.client')}}</span>
                                        <br>
                                    @endif
                                @endif
                                @if(!empty($order) && ($order->collected_cost || $order->collected_cost == 0) && (in_array($order->status, [5,8]) || ((!$order->is_received || !$settings) && $order->status == 3) ))
                                    <span
                                            class="text-success"> {{__('backend.collected_cost')}}: {{ $order->collected_cost ? $order->collected_cost : 0 }} </span>
                                    <br>
                                @endif
                                @if(!empty($order) && $order->delivery_status && (!$order->is_received || !$settings))
                                    <span class="text-primary"> {{__('backend.delivery_status')}}:
                                        @if ($order->status == 3 && $order->delivery_status == 1)
                                            <span
                                                    class='badge badge-pill label-success'> {{__('backend.full_delivered')}} </span>
                                        @elseif ($order->status == 3 && $order->delivery_status == 2)
                                            <span class='badge badge-pill label-black'> {{__('backend.pr')}} </span>
                                        @elseif ($order->status == 3 && $order->delivery_status == 4)
                                            <span
                                                    class='badge badge-pill label-info'> {{__('backend.replace')}}  </span>
                                        @elseif ($order->status == 5 && $order->delivery_status == 3)
                                            <span
                                                    class='badge badge-pill label-danger'> {{__('backend.recall')}}  </span>
                                        @elseif ($order->status == 8 && $order->delivery_status == 5)
                                            <span
                                                    class='badge badge-pill label-danger'> {{__('backend.reject')}}  </span>
                                        @endif
            </span>
                                    <br>
                                @endif

                                @if(!empty($order) && $order->is_refund)
                                    <span class='badge badge-pill label-refund'>{{ __('backend.refund')}} </span>
                                    <br>
                                @endif
                                @if(!empty($order) && $order->payment_status)
                                    <span class="text-primary"> {{__('backend.payment_status')}}:
                                        {!! $order->payment_status_span !!}
                                            </span>
                                    <br>
                                    <span class="text-primary"> {{__('backend.payment_date')}}:
                                        {!! $order->payment_date !!}
                                        </span>
                                    <br>
                                    <span class="text-primary"> {{__('backend.paid_amount')}}:
                                        {!! $order->residual !!}
                                        </span>
                                @endif
                            </div>

                            <div class="row" style="padding: 12px 20px;">
                                <div class="alert alert-danger" style="display:none"></div>
                                <!-- Create at: -->
                                <div class="form-group col-sm-2">
                                    <label for="receiver_name">{{__('backend.created_at')}}: </label>
                                    <p class="form-control-static"
                                       style="color: #0376d9; font-weight: bold;">@if($order->created_at){{$order->created_at}} @else {{__('backend.No_Date')}} @endif </p>
                                </div>

                            @if( in_array($order->status, [1,2,3,4,5,7,8]))
                                <!-- Warehouse Dropped at: -->
                                    <div class="form-group col-sm-3">
                                        <label for="receiver_name">{{__('backend.Warehouse_Dropped_at')}}: </label>
                                        <p class="form-control-static"
                                           style="color: #0376d9; font-weight: bold;">
                                            @if($order->warehouse_dropped_at)
                                                {{$order->warehouse_dropped_at}}
                                            @elseif($order->dropped_at)
                                                {{$order->dropped_at}}
                                            @else
                                                {{__('backend.No_Date')}}
                                            @endif
                                        </p>
                                    </div>

                                    <!-- Dropped at: -->
                                    <div class="form-group col-sm-2">
                                        <label for="reciever_mobile"> {{__('backend.Dropped_at')}}: </label>
                                        <p class="form-control-static" style="color: #0376d9; font-weight: bold;">
                                            @if( $order->dropped_at )
                                                {{$order->dropped_at}}
                                            @else
                                                {{__('backend.No_Date')}}
                                            @endif
                                        </p>
                                    </div>
                            @endif

                            @if( in_array($order->status, [1,2,3,4,5,8]))
                                <!-- Accept at: -->
                                    <div class="form-group col-sm-2">
                                        <label for="reciever_mobile"> {{__('backend.Accept_at')}}: </label>
                                        <p class="form-control-static" style="color: #0376d9; font-weight: bold;">


                                            @if( $order->accepted_at )
                                                {{$order->accepted_at}}
                                            @else {{__('backend.No_Date')}} @endif
                                        </p>
                                    </div>
                            @endif

                            @if( in_array($order->status, [2,3,4,5,8]))
                                <!-- Receiver at: -->
                                    <div class="form-group col-sm-2">
                                        <label for="receiver_name">{{__('backend.Received_at')}}: </label>
                                        <p class="form-control-static" style="color: #0376d9; font-weight: bold;">
                                            @if( $order->status != 0  )

                                                {{$order->received_at}}

                                            @else {{__('backend.No_Date')}}  @endif
                                        </p>
                                    </div>
                            @endif
                            @if( in_array($order->status, [3]) && (!$order->is_received || !$settings))
                                <!-- Deliver at: -->
                                    <div class="form-group col-sm-2">
                                        <label for="reciever_mobile"> {{__('backend.Deliver_at')}}: </label>
                                        <p class="form-control-static" style="color: #0376d9; font-weight: bold;">
                                            @if ($order->status != 0)
                                                {{$settings ? $order->delivered_date : $order->delivered_at}}
                                            @else {{__('backend.No_Date')}} @endif
                                        </p>
                                    </div>
                            @endif
                            @if( in_array($order->status, [4]))
                                <!-- Cancelled at: -->
                                    <div class="form-group col-sm-2">
                                        <label for="reciever_mobile"> {{__('backend.Cancelled_at')}}: </label>
                                        <p class="form-control-static" style="color: #0376d9; font-weight: bold;">
                                            @if ($order->cancelled_at )
                                                {{$order->cancelled_at}}
                                            @else {{__('backend.No_Date')}} @endif
                                        </p>
                                    </div>

                            @endif
                            @if( in_array($order->status, [5]))
                                <!-- Cancelled at: -->
                                    <div class="form-group col-sm-2">
                                        <label for="reciever_mobile"> {{__('backend.Recalled_at')}}: </label>
                                        <p class="form-control-static" style="color: #0376d9; font-weight: bold;">
                                            @if ($order->recalled_at )
                                                {{$order->recalled_at}}
                                            @else {{__('backend.No_Date')}} @endif
                                        </p>
                                    </div>

                                @endif

                            </div>
                        @if( in_array($order->status, [8]))
                            <!-- Cancelled at: -->
                                <div class="form-group col-sm-2">
                                    <label for="reciever_mobile"> {{__('backend.Rejected_at')}}: </label>
                                    <p class="form-control-static" style="color: #0376d9; font-weight: bold;">
                                        @if ($order->rejected_at )
                                            {{$order->rejected_at}}
                                        @else {{__('backend.No_Date')}} @endif
                                    </p>
                                </div>

                            @endif

                        </div>
                        {{--                            <div class="row" style="padding: 12px 20px;">--}}
                        {{--                                <div class="form-group col-sm-12">--}}
                        {{--                                    @if($order->delay_at)--}}
                        {{--                                        <span--}}
                        {{--                                            class="text-danger">  {{__('backend.order_delayed')}}: {{date("Y-m-d", strtotime($order->delay_at))}} </span>--}}
                        {{--                                        <br>--}}
                        {{--                                        @if($order->delay_comment)--}}
                        {{--                                            <span--}}
                        {{--                                                class="text-danger">  {{__('backend.note')}}: {{$order->delay_comment}} </span><br>--}}
                        {{--                                        @endif--}}
                        {{--                                    @endif--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        <div class="row">

                            <div class="col-md-6 col-xs-12">
                                <div class="ribbon-wrapper card">
                                    <div class="ribbon ribbon-default">Receiver Data</div>

                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-sm-4"
                                                   style="font-size: 14px;margin-top: 5px;">@lang('website.ReceiverName')
                                                : </label>
                                            <div class="col-12 col-md-8">
                                                <input disabled class="form-control" type="text"
                                                       name="customer_name" value="{{$order->receiver_name}}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;">Mobile
                                                Number: </label>
                                            <div class="col-12 col-md-8">
                                                <input disabled class="form-control" type="text"
                                                       name="customer_email" value="{{$order->receiver_mobile}}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;">Receiver
                                                Address: </label>
                                            <div class="col-12 col-md-8">
                                                <input disabled class="form-control" type="text" name=""
                                                       value="{{$order->receiver_address}}"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="ribbon-wrapper card">
                                    <div class="ribbon ribbon-primary">Driver Details</div>

                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;">Driver
                                                Name: </label>
                                            <div class="col-12 col-md-8">
                                                <input disabled class="form-control" type="text"
                                                       name="corporate_name"
                                                       value="{{ ! empty($order->accepted ) && ! empty($order->accepted->driver) ? $order->accepted->driver->name : ''}}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;">Driver
                                                Mobile: </label>
                                            <div class="col-12 col-md-8">
                                                <input disabled class="form-control" type="text"
                                                       name="corporate_email"
                                                       value="{{ ! empty($order->accepted ) && ! empty($order->accepted->driver) ? $order->accepted->driver->mobile : ''}}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;"> Driver
                                                E-Mail: </label>
                                            <div class="col-12 col-md-8">
                                                <input disabled class="form-control" type="text"
                                                       name="corporate_mobile"
                                                       value="{{! empty($order->accepted ) && ! empty($order->accepted->driver) ? $order->accepted->driver->email : ''}}">
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            {{--                                <div class="col-md-6 col-xs-12">--}}
                            {{--                                    <div class="ribbon-wrapper card">--}}
                            {{--                                        <div class="ribbon ribbon-info">Sender Data</div>--}}

                            {{--                                        <div class="form-group">--}}
                            {{--                                            <div class="row">--}}
                            {{--                                                <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;">Sender--}}
                            {{--                                                    Name: </label>--}}
                            {{--                                                <div class="col-12 col-md-8">--}}
                            {{--                                                    <input disabled class="form-control" type="text"--}}
                            {{--                                                           name="corporate_name" value="{{$order->sender_name}}">--}}
                            {{--                                                </div>--}}
                            {{--                                            </div>--}}
                            {{--                                        </div>--}}

                            {{--                                        <div class="form-group">--}}
                            {{--                                            <div class="row">--}}
                            {{--                                                <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;">Mobile--}}
                            {{--                                                    Number: </label>--}}
                            {{--                                                <div class="col-12 col-md-8">--}}
                            {{--                                                    <input disabled class="form-control" type="text"--}}
                            {{--                                                           name="corporate_email" value="{{$order->sender_mobile}}">--}}
                            {{--                                                </div>--}}
                            {{--                                            </div>--}}
                            {{--                                        </div>--}}

                            {{--                                        <div class="form-group">--}}
                            {{--                                            <div class="row">--}}
                            {{--                                                <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;">Sender--}}
                            {{--                                                    Address: </label>--}}
                            {{--                                                <div class="col-12 col-md-8">--}}
                            {{--                                                    <input disabled class="form-control" type="text" name=""--}}
                            {{--                                                           value="{{$order->sender_address}}">--}}
                            {{--                                                </div>--}}
                            {{--                                            </div>--}}
                            {{--                                        </div>--}}

                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="ribbon-wrapper card">
                                    <div class="ribbon ribbon-danger">Order Data</div>

                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;">Order
                                                Type: </label>
                                            <div class="col-12 col-md-8">
                                                <input disabled class="form-control" type="text"
                                                       name="corporate_name"
                                                       value="{{! empty($order->type) ? $order->type : '' }}">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-sm-4"
                                                   style="font-size: 14px;margin-top: 5px;">@lang('website.OrderPrice')
                                                : </label>
                                            <div class="col-12 col-md-8">
                                                <input disabled class="form-control" type="text" name="order_price"
                                                       value="{{! empty($order->order_price) ? $order->order_price : '' }}">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-sm-4"
                                                   style="font-size: 14px;margin-top: 5px;">@lang('website.OrderNumber')
                                                : </label>
                                            <div class="col-12 col-md-8">
                                                <input disabled class="form-control" type="text"
                                                       name="corporate_email" value="{{$order->order_number}}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;">Receiver
                                                Code: </label>
                                            <div class="col-12 col-md-8">
                                                <input disabled class="form-control" type="text" name=""
                                                       value="{{$order->receiver_code}}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-sm-4"
                                                   style="font-size: 14px;margin-top: 5px;">Notes: </label>
                                            <div class="col-12 col-md-8">
                                                    <textarea disabled class="form-control" rows="3"
                                                              name="corporate_mobile"
                                                              style="min-height: 150px;resize: vertical;"> {{! empty($order->notes) ? $order->notes : '-' }} </textarea>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="ribbon-wrapper card">
                                    <div class="ribbon ribbon-danger">{{__('backend.ufelix_comments')}}</div>

                                    <div class="table-responsive">
                                        @if(count($order->comments->whereIn('user_type', [1, 3])))
                                            <table id="theTable"
                                                   class="table table-condensed table-striped text-center">
                                                <thead>
                                                <tr>
                                                    <th>{{__('backend.comment')}}</th>
                                                    <th>{{__('backend.user')}}</th>
                                                    <th>{{__('backend.driver')}}</th>
                                                    <th>{{__('backend.date')}}</th>
                                                    <th>{{__('backend.location')}}</th>
                                                </tr>
                                                </thead>

                                                @foreach($order->comments->whereIn('user_type', [1, 3])->all() as $comment)
                                                    <tr>
                                                        <td>{{$comment->comment}}</td>
                                                        <td>{{!empty($comment->user) ? $comment->user->name : '-'}}</td>
                                                        <td>{{!empty($comment->driver) ? $comment->driver->name : '-'}}</td>
                                                        <td>{{date("Y-m-d", strtotime($comment->created_at))}}</td>
                                                        <td>
                                                            <a href="http://maps.google.com/?q={{$comment->latitude}},{{$comment->longitude}}"
                                                               target="_blank"><img width="15"
                                                                                    src="{{asset('profile/images/map.svg')}}"></a>
                                                        </td>
                                                    </tr>
                                                @endforeach

                                                <tbody>
                                                </tbody>
                                            </table>
                                        @else
                                            <div class="form-group col-sm-4 col-sm-offset-4 text-center">
                                                <div class="alert alert-info" style="padding-bottom: 3px;">
                                                    <h4>{{__('backend.no_order_comments')}}</h4>
                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                </div>

                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="ribbon-wrapper card">
                                    <div class="ribbon ribbon-danger">{{__('backend.client_comments')}}</div>
                                    <div class="ribbon ribbon-primary " style="right: -2px; left: auto; cursor: pointer"
                                         id="new_problem" data-toggle="modal"
                                         data-target="#commentModal">
                                        {{__('backend.add_order_comment')}}
                                    </div>
                                    <div class="table-responsive">
                                        @if(count($order->comments->whereIn('user_type', [2])))
                                            <table id="theTable"
                                                   class="table table-condensed table-striped text-center">
                                                <thead>
                                                <tr>
                                                    <th>{{__('backend.comment')}}</th>
                                                    <th>{{__('backend.user')}}</th>
                                                    <th>{{__('backend.driver')}}</th>
                                                    <th>{{__('backend.date')}}</th>
                                                    <th>{{__('backend.location')}}</th>
                                                </tr>
                                                </thead>

                                                @foreach($order->comments->whereIn('user_type', [2])->all() as $comment)
                                                    <tr>
                                                        <td>{{$comment->comment}}</td>
                                                        <td>{{!empty($comment->user) ? $comment->user->name : '-'}}</td>
                                                        <td>{{!empty($comment->driver) ? $comment->driver->name : '-'}}</td>
                                                        <td>{{date("Y-m-d", strtotime($comment->created_at))}}</td>
                                                        <td>
                                                            <a href="http://maps.google.com/?q={{$comment->latitude}},{{$comment->longitude}}"
                                                               target="_blank"><img width="15"
                                                                                    src="{{asset('profile/images/map.svg')}}"></a>
                                                        </td>
                                                    </tr>
                                                @endforeach

                                                <tbody>
                                                </tbody>
                                            </table>
                                        @else
                                            <div class="form-group col-sm-4 col-sm-offset-4 text-center">
                                                <div class="alert alert-info" style="padding-bottom: 3px;">
                                                    <h4>{{__('backend.no_order_comments')}}</h4>
                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                </div>

                            </div>
                        </div>
                </div>
                @else
                    <div class="container-fluid">

                        <div class="alert alert-danger text-center">
                            <p>
                                No Order Found
                            </p>
                        </div>

                    </div>
                @endif
            </div>
        </div>
    </div>
    </div>

    @include('frontend.profile.order_comment')
@endsection
{{--@section('scripts')--}}
{{--    <!-- Sweet-Alert  -->--}}
{{--    <script src="{{asset('public/Profile/assets/plugins/sweetalert/sweetalert.min.js') }}"></script>--}}
{{--    <script src="{{asset('public/Profile/assets/plugins/sweetalert/jquery.sweet-alert.custom.js') }}"></script>--}}


{{--@endsection--}}

@extends('backend.layouts.app')
@section('header')
<div class="page-header">
        <h3>{{__('backend.the_Service')}} #{{$service->id}}</h3>{!! $service->publish_span ?  $service->publish_span : ''  !!}
        <form action="{{ route('mngrAdmin.services.destroy', $service->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__("backend.DELETEASk")}}')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('mngrAdmin.services.edit', $service->id) }}"><i class="fa fa-edit"></i> {{__('backend.EDIT')}}</a>
                <button type="submit" class="btn btn-danger">{{__('backend.DELETE')}} <i class="fa fa-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="form-group col-md-4 col-sm-4 col-xs-12">
            <label for="image">@lang('backend.Image')</label>
            <p class="form-control-static"><img width="100%" src="{{$service->image}}" /></p>
        </div>
        <div class="form-group col-md-4 col-sm-4 col-xs-12">
             <label for="title_ar">@lang('backend.title_ar')</label>
             <p class="form-control-static">{{$service->title_ar}}</p>
        </div>
            <div class="form-group col-md-4 col-sm-4 col-xs-12">
             <label for="title_en">@lang('backend.title_en')</label>
             <p class="form-control-static">{{$service->title_en}}</p>
        </div>
     


            <div class="form-group col-md-4 col-sm-4 col-xs-12">
             <label for="small_description_ar">@lang('backend.small_description_ar')</label>
             <p class="form-control-static">{{$service->small_description_ar}}</p>
        </div>
            <div class="form-group col-md-4 col-sm-4 col-xs-12">
             <label for="small_description_en">@lang('backend.small_description_en')</label>
             <p class="form-control-static">{{$service->small_description_en}}</p>
        </div>
            <div class="form-group col-md-4 col-sm-4 col-xs-12">
             <label for="description_ar">@lang('backend.description_ar')</label>
             <p class="form-control-static">{{$service->description_ar}}</p>
        </div>
            <div class="form-group col-md-4 col-sm-4 col-xs-12">
             <label for="description_en">@lang('backend.description_en')</label>
             <p class="form-control-static">{{$service->description_en}}</p>
        </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        @include('backend.layouts.back')
        
    </div>


    </div>
</div>

@endsection
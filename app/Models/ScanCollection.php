<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScanCollection extends Model
{

    protected $guarded = ['id'];

    public function getScanTypeSpanAttribute()
    {
        if ($this->scan_type == 1) {
            return "<span class='badge badge-pill label-success'>" . __('backend.captain_entry') . "</span>";
        } elseif ($this->scan_type == 2) {
            return "<span class='badge badge-pill label-danger'>" . __('backend.captain_exit') . "</span>";
        } elseif ($this->scan_type == 3) {
            return "<span class='badge badge-pill label-warning'>" . __('backend.quick_entry') . "</span>";
        }
    }

    public function orders()
    {
        return $this->hasMany('App\Models\ScanCollectionOrder', 'collection_id');
    }

    public function allOrders()
    {
        return $this->belongsToMany('App\Models\Order', 'scan_collection_orders', 'collection_id', 'order_id');
    }

    public function driver()
    {
        return $this->belongsTo('App\Models\Driver', 'driver_id');
    }

}

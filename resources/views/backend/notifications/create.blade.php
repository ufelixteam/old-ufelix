@extends('backend.layouts.app')

@section('css')
    <title>{{__('backend.notifications')}} - {{__('backend.new')}}</title>
@endsection

@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-plus"></i> {{__('backend.New_Notification')}} </h3>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <form action="{{ route('mngrAdmin.notifications.store') }}" method="POST" name="validateForm">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div
                    class="form-group{{ $errors->has('type') ? ' is-invalid' : '' }} col-md-4 col-sm-6 col-xs-6 @if($errors->has('type')) has-error @endif">
                    <label class="control-label" for="type">{{__('backend.Notification_Type')}}</label>
                    <select id="type" name="type" class="form-control">
                        <option value="0">{{__('backend.Select_type')}}</option>
                        @foreach(DB::table('notification_type')->get() as $value)
                            <option value="{{ $value->id }}">{{ $value->title }}</option>
                        @endforeach
                    </select>
                    @if($errors->has("type"))
                        <span class="help-block">{{ $errors->first("type") }}</span>
                    @endif
                </div>

                <div
                    class="form-group{{ $errors->has('client_model') ? ' is-invalid' : '' }} col-md-4 col-sm-6 col-xs-6 @if($errors->has('client_model')) has-error @endif">
                    <label class="control-label" for="client_model">{{__('backend.Send_to')}}</label>
                    <select id="client_model" name="client_model" class="form-control">
                        <option value="0">{{__('backend.Select_Sender_type')}}</option>
                        <option value="5">{{__('backend.all')}}</option>
                        <option value="1">{{__('backend.captains')}}</option>
                        {{--                        <option value="2">{{__('backend.agents')}}</option>--}}
{{--                        <option value="3">{{__('backend.individuals')}}</option>--}}
                        <option value="4">{{__('backend.corporates')}}</option>
                    </select>
                    @if($errors->has("client_model"))
                        <span class="help-block">{{ $errors->first("client_model") }}</span>
                    @endif
                </div>

                <div
                    class="captains form-group{{ $errors->has('captains') ? ' is-invalid' : '' }} col-md-4 col-sm-6 col-xs-6"
                    style="display:none;">
                    <label class="control-label" for="captains">{{__('backend.captains')}}</label>
                    <select name="client_id" class="form-control">
                        <option value="0">{{__('backend.all')}}</option>
                        @foreach(App\Models\Driver::where('is_active', 1)->get() as $value)
                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                        @endforeach
                    </select>
                    @if($errors->has("captains"))
                        <span class="help-block">{{ $errors->first("captains") }}</span>
                    @endif
                </div>


                {{--                <div class="agents form-group{{ $errors->has('agents') ? ' is-invalid' : '' }} col-md-4 col-sm-6 col-xs-6" style="display:none;">--}}
                {{--                    <label class="control-label" for="agents">{{__('backend.agents')}}</label>--}}
                {{--                    <select name="client_id" class="form-control">--}}
                {{--                        <option value="0">{{__('backend.all')}}</option>--}}
                {{--                    @foreach(App\Models\Agent::where('status', 1)->get() as $value)--}}
                {{--                            <option value="{{ $value->id }}">{{ $value->name }}</option>--}}
                {{--                        @endforeach--}}
                {{--                    </select>--}}
                {{--                    @if($errors->has("agents"))--}}
                {{--                        <span class="help-block">{{ $errors->first("agents") }}</span>--}}
                {{--                    @endif--}}
                {{--                </div>--}}


{{--                <div--}}
{{--                    class="individuals form-group{{ $errors->has('individuals') ? ' is-invalid' : '' }} col-md-4 col-sm-6 col-xs-6"--}}
{{--                    style="display:none;">--}}
{{--                    <label class="control-label" for="individuals">{{__('backend.individuals')}}</label>--}}
{{--                    <select name="client_id" class="form-control">--}}
{{--                        <option value="0">{{__('backend.all')}}</option>--}}
{{--                        @foreach(App\Models\Customer::where('status', 1)->get() as $value)--}}
{{--                            <option value="{{ $value->id }}">{{ $value->name }}</option>--}}
{{--                        @endforeach--}}
{{--                    </select>--}}
{{--                    @if($errors->has("individuals"))--}}
{{--                        <span class="help-block">{{ $errors->first("individuals") }}</span>--}}
{{--                    @endif--}}
{{--                </div>--}}


                <div
                    class="corporate form-group{{ $errors->has('corporate') ? ' is-invalid' : '' }} col-md-4 col-sm-6 col-xs-6"
                    style="display:none;">
                    <label class="control-label" for="corporate">{{__('backend.corporates')}}</label>
                    <select name="client_id" class="form-control">
                        <option value="0">{{__('backend.all')}}</option>
                        @foreach(App\Models\Corporate::all() as $value)
                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                        @endforeach
                    </select>
                    @if($errors->has("corporate"))
                        <span class="help-block">{{ $errors->first("corporate") }}</span>
                    @endif
                </div>

                <div class="form-group col-md-4 col-sm-6 col-xs-6 @if($errors->has('devices')) has-error @endif">
                    <label for="title-field">{{__('backend.devices')}}</label>
                    <select id="devices" name="devices" class="form-control">
                        <option value="0">{{__('backend.Select_device')}}</option>
                        <option value="4">{{__('backend.all')}}</option>
                        <option value="1">{{__('backend.android')}}</option>
                        <option value="2">{{__('backend.ios')}}</option>
                        <option value="3">{{__('backend.browser')}}</option>
                    </select>
                    @if($errors->has("devices"))
                        <span class="help-block">{{ $errors->first("devices") }}</span>
                    @endif
                </div>

                <div class="form-group col-md-4 col-sm-6 col-xs-6 @if($errors->has('date')) has-error @endif">
                    <label for="title-field">{{__('backend.date')}}</label>
                    <input type="date" id="title-field" name="date" class="form-control"
                           value="{{ old("date", date('Y-m-d')) }}"/>
                    @if($errors->has("date"))
                        <span class="help-block">{{ $errors->first("date") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-8 col-sm-6 col-xs-6 @if($errors->has('title')) has-error @endif">
                    <label for="title-field">{{__('backend.title')}}</label>
                    <input type="text" id="title-field" name="title" class="form-control" value="{{ old("title") }}"/>
                    @if($errors->has("title"))
                        <span class="help-block">{{ $errors->first("title") }}</span>
                    @endif
                </div>

                <div class="form-group col-md-12 col-sm-12 col-xs-12 @if($errors->has('message')) has-error @endif">
                    <label for="message-field">{{__('backend.message')}}</label>
                    <textarea class="form-control summernote" name="message" required>{{ old("message") }}</textarea>
                    @if($errors->has("message"))
                        <span class="help-block">{{ $errors->first("message") }}</span>
                    @endif
                </div>
                <div class="well well-sm col-md-12 col-sm-12 col-xs-12">
                    <button type="submit" class="btn btn-primary">{{__('backend.send')}}</button>
                    <a class="btn btn-link pull-right" href="{{ URL::previous() }}">
                        <i class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}
                    </a>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('.summernote').summernote({
                placeholder: 'Enter your message here',
                tabsize: 2,
                height: 300
            });
        });

        $(function () {
            if ($("#type-field").val() == "1") {
                $("#id").show();
            } else {
                $("#id").hide();
            }

            $("#client_model").on("change", function () {
                if (this.value == 1) {
                    $('.captains').show();
                    $('.agents').hide();
                    $('.individuals').hide();
                    $('.corporate').hide();
                } else if (this.value == 2) {
                    $('.captains').hide();
                    $('.agents').show();
                    $('.individuals').hide();
                    $('.corporate').hide();
                } else if (this.value == 3) {
                    $('.captains').hide();
                    $('.agents').hide();
                    $('.individuals').show();
                    $('.corporate').hide();
                } else if (this.value == 4) {
                    $('.captains').hide();
                    $('.agents').hide();
                    $('.individuals').hide();
                    $('.corporate').show();
                } else {
                    $('.captains').hide();
                    $('.agents').hide();
                    $('.individuals').hide();
                    $('.corporate').hide();
                }
            });

            $("#user_id-field").keyup(function (e) {
                e.preventDefault();
                var serchKy = $('#user_id-field').val();
                if (serchKy != "") {
                    $.ajax({
                        url: "{{url('/searchCustomer/')}}" + '/' + $('#user_id-field').val(),
                        type: 'GET',
                        data: {},
                        success: function (data) {
                            var datalist = $("#searchResult");
                            var tablSearc = "";
                            datalist.empty();
                            $.each(data, function (i, content) {
                                document.getElementById('searchResult').innerHTML += "<option value='" + content.id + "'>" + content.first_name + ' - ' + content.mobile + "</option>";
                            });
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }

            });

            $("form[name='validateForm']").validate({
                // Specify validation rules
                rules: {
                    type: {
                        required: true
                    },
                    client_model: {
                        required: true
                    },
                    devices: {
                        required: true
                    },
                    date: {
                        required: true
                    },
                    title: {
                        required: true
                    },
                    message: {
                        required: true
                    },
                },

                // Specify validation error messages
                messages: {
                    type: {
                        required: "{{__('backend.Please_Enter_Model_Type_Name')}}",
                    },
                    client_model: {
                        required: "{{__('backend.Please_Enter_Model_Type_Name')}}",
                    },
                    devices: {
                        required: "{{__('backend.Please_Enter_Model_Type_Name')}}",
                    },
                    date: {
                        required: "{{__('backend.Please_Enter_Model_Type_Name')}}",
                    },
                    title: {
                        required: "{{__('backend.Please_Enter_Model_Type_Name')}}",
                    },
                    message: {
                        required: "{{__('backend.Please_Enter_Model_Type_Name')}}",
                    },
                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                },

                highlight: function (element) {
                    $(element).parent().addClass('error')
                },
                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                }
            });

        })
    </script>
@endsection

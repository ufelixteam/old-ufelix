<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        @if($tickets->count())
            <table class="table table-condensed table-striped ">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('backend.ticket_no')}}</th>
                    <th>{{__('backend.order')}}/{{__('backend.corporate')}}</th>
                    <th>{{__('backend.created_by')}}</th>
                    <th>{{__('backend.status')}}</th>
                    <th>{{__('backend.reason')}}</th>
                    <th>{{__('backend.closed_by')}}</th>
                    <th>{{__('backend.closed_at')}}</th>
                    <th>{{__('backend.created_at')}}</th>
                </thead>
                <tbody>

                @foreach($tickets as $ticket)
                    <tr class='clickable-row' data-href="{{ route('mngrAdmin.tickets.show', $ticket->id) }}">
                        <td>{{$ticket->id}}</td>
                        <td>{{$ticket->ticket_number}}</td>
                        <td class="exclude-td">
                            @if(! empty($ticket->order))
                                <a href="{{route('mngrAdmin.orders.show', $ticket->order_id)}}">{{$ticket->order->order_number}}</a>
                            @elseif(! empty($ticket->corporate))
                                <a href="{{route('mngrAdmin.corporates.show', $ticket->corporate_id)}}">{{$ticket->corporate->name}}</a>
                            @endif
                        </td>
                        <td>{!! $ticket->created_by !!}</td>
                        <td>{!! $ticket->status_span !!}</td>
                        {{--                        <td class="exclude-td">--}}
                        {{--                            <input data-id="{{$ticket->id}}" data-size="mini"--}}
                        {{--                                   class="toggle status" id="status_{{$ticket->id}}"--}}
                        {{--                                   {{$ticket->status == 1 ? 'checked disabled' : ''}} data-onstyle="success"--}}
                        {{--                                   data-offstyle="danger"--}}
                        {{--                                   type="checkbox" data-style="ios" data-on="Closed" data-off="Opened">--}}
                        {{--                        </td>--}}
                        <td>{{$ticket->reason}}</td>
                        <td>{!! $ticket->closed_by_user !!}</td>
                        <td>{{ $ticket->closed_at ? date('Y-m-d', strtotime($ticket->closed_at)) : '-' }}</td>
                        <td>{{ $ticket->created_at->format('Y-m-d') }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {!! $tickets->appends($_GET)->links() !!}
        @else
            <h3 class="text-center alert alert-warning">No Result Found !</h3>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        @if(! empty($orders) && count($orders) > 0)
            <form method="post" id="myform" action="#">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <div class="table-responsive">
                    <table id="theTable" class="table table-condensed table-striped text-center"
                           style="min-height: 200px;">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('captain.status')}}</th>
                            <th>{{__('captain.captain')}}</th>
                            <th>{{__('captain.receiver_name')}}</th>
                            <th>{{__('captain.to')}}</th>
                            <th>{{__('captain.sender_name')}}</th>
                            <th style="font-weight: bold; color: #333;">{{__('captain.total_cost')}}</th>
                            <th>{{__('captain.collected_cost')}}</th>
                            <th>{{__('captain.full_delivered')}}</th>
                            <th>{{__('captain.part_recall')}}</th>
                            <th>{{__('captain.replace')}}</th>
                            <th>{{__('captain.recall')}}</th>
                            <th>{{__('captain.reject')}}</th>
                            <th>
                                <a class="action_btn btn btn-info btn-xs" id="toggle" data-state="1"><i class="fa fa-check"></i> </a>
                                <button type="button" id="pdf-submit"
                                        class="btn btn-warning btn-pdf btn-xs "><i class="fa fa-print"></i></button>
                                @if(Auth::guard('captain')->user()->is_manager)
                                <a class="forward btn btn-success  btn-xs" id="forward">
                                    <i class="fa fa-share"></i>
                                </a>
                                    @endif
                            </th>

                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $i => $order)

                            <tr @if($order->getIsAlert()) class="firstOne" @endif>
                                <td>{{($i+1)}}</td>
                                <td @if (count($order->comments)) class="testtooltip"
                                    tabindex="0"
                                    data-html="true"
                                    data-toggle="popover"
                                    data-trigger="hover"
                                    data-placement="top"
                                    title='<button class="close" data-dismiss="alert"><i class="fa fa-close"></i></button>'
                                    data-content='{!! $order->problems_table !!}' @endif>
                                    <span class='badge badge-pill label-warning'>{{__('captain.received')}}</span>
                                    <br>
                                    <span style="font-weight: bold; color: #843747;">{{$order->order_number}}</span>
                                </td>
                                <td>
                                    {{$order->driver->name}}
                                </td>
                                <td>
                                    {{$order->receiver_name}}
                                    <br>
                                    {{$order->receiver_mobile}}
                                </td>
                                <td>
                                    @if(! empty($order->to_government)  && $order->from_government != null  )
                                        {{$order->to_government->name_en}}
                                    @endif
                                </td>
                                <td>
                                    {{$order->sender_name}}<br>
                                    {{$order->sender_mobile}}
                                </td>
                                <td style="font-weight: bold; color: #a43;"><span
                                        class="total_cost">{{$order->total_price}}</span></td>
                                <td>
                                    <input type="text"
                                           class="form-control delivery_status_class collected_cost"
                                           value="{{($order->collected_cost || $order->collected_cost == 0) ? $order->collected_cost : ($order->payment_method_id == 1 && $order->order_price == 0 ? 0 : $order->total_price) }}"
                                           data-id="{{$order->id}}"
                                           name="collected_cost"
                                           id="collected_cost_{{$order->id}}"
                                           style="width: 90px;"/>
                                </td>
                                <td>
                                    <input type="radio" name="delivery_status_{{$order->id}}" value="1"
                                           class="delivery_status delivery_status_class_{{$order->id}}"
                                           data-id="{{$order->id}}">
                                </td>
                                <td>
                                    <input type="radio" name="delivery_status_{{$order->id}}" value="2"
                                           class="delivery_status delivery_status_class_{{$order->id}}"
                                           data-id="{{$order->id}}">
                                </td>
                                <td>
                                    <input type="radio" name="delivery_status_{{$order->id}}" value="4"
                                           class="delivery_status delivery_status_class_{{$order->id}}"
                                           data-id="{{$order->id}}">
                                </td>
                                <td>
                                    <input type="radio" name="delivery_status_{{$order->id}}" value="3"
                                           class="delivery_status delivery_status_class_{{$order->id}} delivery_status_value_3"
                                           data-id="{{$order->id}}"
                                           id="delivery_status_value_3_{{$order->id}}">
                                </td>
                                <td>
                                    <input type="radio" name="delivery_status_{{$order->id}}" value="5"
                                           class="delivery_status delivery_status_class_{{$order->id}} delivery_status_value_5"
                                           data-id="{{$order->id}}"
                                           id="delivery_status_value_5_{{$order->id}}">
                                </td>

                                <td class="selectpdf" id="selectpdf">
                                    <input type="checkbox" name="select[]" value="{{$order->id}}"
                                           class="selectOrder"
                                           data-value="{{$order->id}}"/>

                                </td>
                                <td>
                                    <a href="#" class="disabled btn btn-danger save_status" data-id="{{$order->id}}">{{__('captain.save')}}</a>
                                </td>
                            </tr>

                            <tr @if($order->getIsAlert()) class="secondOne" @endif>
                                <td colspan="8" style="padding-left: 0">
                                    <input type="text" class="comment_text form-control" style="display: inline-block;width: 350px;"
                                           name="comment"/>
                                    <a href="#" class="btn btn-info disabled inline_comment"
                                       data-id="{{$order->id}}">{{__('captain.comment')}}</a>
                                </td>
                                <td colspan="8" style="padding-right: 40px;padding-top: 15px;">
                                    <strong class="pull-right">{{$order->receiver_address}}</strong>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </form>
            {!! $orders->appends($_GET)->links() !!}
        @else
            <h3 class="text-center alert alert-warning">No Result Found !</h3>
        @endif
    </div>
</div>
<div id="js-form" class="js-form">
</div>


<div class="modal fade" id="forwardModal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <p>{{__('captain.Forward_Orders_In_Agent_Or_Driver')}}</p>
                <div class="form-group">
                    <br>
                    <select class="form-control" name="driverID" id="driver">
                        <option value="">{{__('captain.Choose_Driver')}}</option>
                        @if(! empty($online_drivers))
                            @foreach($online_drivers as $driver)
                                <option value="{{$driver->id}}">{{$driver->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary"
                            id="forward_action">{{__('captain.forward')}}</button>
                    <button type="button" class="btn btn-danger"
                            data-dismiss="modal">{{__('captain.close')}}</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$("#s_state_id").on('change',function(){
  $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
    $.ajax({
      url: '<?php echo e(url("/mngrAdmin/orders")); ?>'+'?s_government_id='+$("#s_government_id").val()+'&&s_state_id='+$("#s_state_id").val()+'&&r_government_id='+$("#r_government_id").val()+'&&r_state_id='+$("#r_state_id").val()+'&&type=<?php echo e($type); ?>',
      type: 'get',
      data: {},
      success: function(data) {
        $('.list').html(data.view);
        $('.js-form').html(data.js);
      },
      error: function(data) {
        console.log('Error:', data);
      }
    });
});
    $("#r_state_id").on('change',function(){
      $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
        $.ajax({
          url: '<?php echo e(url("/mngrAdmin/orders")); ?>'+'?s_government_id='+$("#s_government_id").val()+'&&s_state_id='+$("#s_state_id").val()+'&&r_government_id='+$("#r_government_id").val()+'&&r_state_id='+$("#r_state_id").val()+'&&type=<?php echo e($type); ?>',
          type: 'get',
          data: {},
          success: function(data) {
            $('.list').html(data.view);
            $('.js-form').html(data.js);
          },
          error: function(data) {
            console.log('Error:', data);
          }
        });
    });

$("#s_government_id").on('change',function(){
      $.ajax({
        url: "<?php echo e(url('/get_cities')); ?>"+"/"+$(this).val(),
        type: 'get',
        data: {},
        success: function(data) {
          $('#s_state_id').html('<option value="" >Sender City</option>');
          $.each(data, function(i, content) {
            $('#s_state_id').append($("<option></option>").attr("value",content.id).text(content.city_name));
          });
        },
        error: function(data) {
          console.log('Error:', data);
        }
      });

      $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
        $.ajax({
          url: '<?php echo e(url("/mngrAdmin/orders")); ?>'+'?s_government_id='+$("#s_government_id").val()+'&&s_state_id='+$("#s_state_id").val()+'&&r_government_id='+$("#r_government_id").val()+'&&r_state_id='+$("#r_state_id").val()+'&&type=<?php echo e($type); ?>',
          type: 'get',
          data: {},
          success: function(data) {
            $('.list').html(data.view);
            $('.js-form').html(data.js);
          },
          error: function(data) {
            console.log('Error:', data);
          }
        });

    });



    $("#r_government_id").on('change',function(){

      $.ajax({
        url: "<?php echo e(url('/get_cities')); ?>"+"/"+$(this).val(),
        type: 'get',
        data: {},
        success: function(data) {
          $('#r_state_id').html('<option value="" >Receiver City</option>');
          $.each(data, function(i, content) {
            $('#r_state_id').append($("<option></option>").attr("value",content.id).text(content.city_name));
          });
        },
        error: function(data) {
          console.log('Error:', data);
        }
      });
      $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
        $.ajax({
          url: '<?php echo e(url("/mngrAdmin/orders")); ?>'+'?s_government_id='+$("#s_government_id").val()+'&&s_state_id='+$("#s_state_id").val()+'&&r_government_id='+$("#r_government_id").val()+'&&r_state_id='+$("#r_state_id").val()+'&&type=<?php echo e($type); ?>',
          type: 'get',
          data: {},
          success: function(data) {
            $('.list').html(data.view);
            $('.js-form').html(data.js);
          },
          error: function(data) {
            console.log('Error:', data);
          }
        });

    });

$("#date").on('change', function() {
  $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
    $.ajax({
        url: '<?php echo e(url("/mngrAdmin/orders")); ?>'+'?date='+$("#date").val(),
        type: 'get',
        success: function(data) {
            $('.list').html(data.view);
            $('.js-form').html(data.js);
            /*
            $('#theTable').DataTable({
              "pagingType": "full_numbers"
            });
            */
        },
        error: function(data) {
            console.log('Error:', data);
        }
    });
});
$(document).ready(function() {
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
});
$("#search-btn1").on('click', function() {
      // Wait icon
    $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
    $.ajax({
      url: '<?php echo e(url("/mngrAdmin/orders")); ?>'+'?search='+$("#search-field").val(),
      type: 'get',
      data: $("#search-form").serialize(),
      success: function(data) {
        $('.list').html(data.view);
        $('.js-form').html(data.js);
      },
      error: function(data) {
        console.log('Error:', data);
      }
    });

  });

  // Select All Orders function For Print
  function do_this(){
    var checkboxes  = document.getElementsByName('select[]');
    var button      = document.getElementById('toggle');

    if(button.value == 'select'){
        for (var i in checkboxes){
            checkboxes[i].checked = 'FALSE';
        }
        button.value = 'deselect'
    }else{
        for (var i in checkboxes){
            checkboxes[i].checked = '';
        }
        button.value  = 'select';
    }
  }

  function getChcked(){
      var form = document.getElementById('myform');
      var chks = form.querySelectorAll('input[type="checkbox"]');
      var checked = [];
      for(var i = 0; i < chks.length; i++){
          if(chks[i].checked){
              checked.push(chks[i].value)
          }
      }
      return checked;
  }

$('#agent').change(function() {
    $.ajax({
        url: "<?php echo e(url('/mngrAdmin/getdriver_by_agents')); ?>"+"/"+$(this).val(),
        type: 'get',
        data: {},
        success : function(data) {

          $('#driver').html('<option value="">Chosse Driver</option>');
          $.each(data, function(key,content) {
           $("#driver").append($("<option></option>").attr("value",content.id).text(content.name));
         });

        },
        error: function() {
            alert('Error occured');
        }
    });

  });


</script>
<?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/orders/js-search.blade.php ENDPATH**/ ?>
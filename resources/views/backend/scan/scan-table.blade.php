<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        <a class="btn btn-secondary rearrange" data-status="0">
            <th>{{__("backend.pending")}}</th>
        </a>
        <a class="btn btn-secondary rearrange" data-status="7">
            <th>{{__("backend.dropped")}}</th>
        </a>
        <a class="btn btn-info rearrange" data-status="1">
            <th>{{__("backend.accepted")}}</th>
        </a>
        <a class="btn btn-warning rearrange" data-status="2">
            <th>{{__("backend.received")}}</th>
        </a>
        <a class="btn btn-success rearrange" data-status="3">
            <th>{{__("backend.delivered")}}</th>
        </a>
        <a class="btn btn-danger rearrange" data-status="5">
            <th>{{__("backend.recalled")}}</th>
        </a>
        <a class="btn btn-danger rearrange" data-status="8">
            <th>{{__("backend.rejected")}}</th>
        </a>
        <a class="btn btn-secondary rearrange" data-status="default">
            <th>{{__("backend.default")}}</th>
        </a>
    </div>
    <div class="col-md-12 col-xs-12 col-sm-12">
        @if(!empty($orders) && count($orders))
            <form method="post" id="myform" action="{{ url('/mngrAdmin/forward_order') }}">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <div class="table-responsive">
                    <table id="theTable" class="entries table table-condensed table-striped text-center"
                           style="min-height: 200px;">
                        <thead>
                        <tr>
                            <th></th>
                            <th>#</th>
                            {{--                            <th>{{__("backend.id")}}</th>--}}
                            <th>{{__("backend.status")}}</th>
                            <th>{{__('backend.collection')}}</th>
                            <th>{{__('backend.receiver_name')}}</th>
                            <th>{{__('backend.sender_name')}}</th>
                            <th>{{__('backend.from')}}</th>
                            <th>{{__('backend.to')}}</th>
                            <th>{{__('backend.order_number')}}</th>
                            <th>{{__('backend.driver')}}</th>
                            <th>{{__('backend.total_price')}}</th>
                            @if ($additional_filter === 'captain_entry' || $additional_filter === 'quick_entry')
                                <th>{{__('backend.collected_cost')}}</th>
                                {{--                                <th></th>--}}
                                <th>{{__('backend.full_delivered')}}</th>
                                <th>{{__('backend.part_recall')}}</th>
                                <th>{{__('backend.replace')}}</th>
                                <th>{{__('backend.recall')}}</th>
                                <th>{{__('backend.reject')}}</th>
                            @endif
                            <th>
                                <a class="action_btn btn btn-warning" id="toggle" data-state="1"> Select</a>
                                {{--                                <div>--}}
                                {{--                                    <a class="action_btn btn btn-warning disabled" id="delivery_problems"--}}
                                {{--                                       style="width: 415px;">--}}
                                {{--                                        {{__('backend.delivery_problems')}}--}}
                                {{--                                    </a>--}}
                                {{--                                </div>--}}
                                {{--                                <div>--}}
                                {{--                                    <a class="action_btn btn btn-warning" id="toggle"--}}
                                {{--                                       data-state="1"> {{__('backend.select')}}</a>--}}

                                {{--                                    <a class="action_btn btn btn-danger disabled" id="recall_orders">--}}
                                {{--                                        <i class="fa fa-history"></i> {{__('backend.recall')}}--}}
                                {{--                                    </a>--}}

                                {{--                                    <a class="action_btn btn btn-success disabled" id="receive_orders">--}}
                                {{--                                        <i class="fa fa-battery-half"></i> {{__('backend.Receive')}}--}}
                                {{--                                    </a>--}}

                                {{--                                    <a class="action_btn btn btn-info disabled" id="delay_orders">--}}
                                {{--                                        <i class="fa fa-calendar"></i> {{__('backend.delay')}}--}}
                                {{--                                    </a>--}}

                                {{--                                </div>--}}
                                {{--                                <div>--}}
                                {{--                                    <button type="button" id="pdf-submit" disabled--}}
                                {{--                                            class="action_btn btn btn-warning">{{__('backend.print')}}</button>--}}

                                {{--                                    <a class="action_btn btn btn-danger disabled" id="cancel_orders">--}}
                                {{--                                        <i class="fa fa-close"></i> {{__('backend.Cancel')}}--}}
                                {{--                                    </a>--}}

                                {{--                                    <a class="action_btn btn btn-success disabled" id="deliver_orders">--}}
                                {{--                                        <i class="fa fa-battery"></i> {{__('backend.deliver')}}--}}
                                {{--                                    </a>--}}

                                {{--                                    <a class="action_btn btn btn-success disabled" id="forward">--}}
                                {{--                                        <i class="fa fa-eye"></i> {{__('backend.forward')}}--}}

                                {{--                                    </a>--}}
                                {{--                                </div>--}}
                                {{--                                <div>--}}
                                {{--                                    <a class="action_btn btn btn-danger disabled" id="dropped_orders">--}}
                                {{--                                        <i class="fa fa-inbox"></i> {{__('backend.drop')}}--}}
                                {{--                                    </a>--}}
                                {{--                                    <a class="action_btn btn btn-danger disabled" id="order_moved_to">--}}
                                {{--                                        <i class="fa fa-truck"></i> {{__('backend.move_to')}}--}}
                                {{--                                    </a>--}}
                                {{--                                    <a class="action_btn btn btn-danger disabled" id="moved_dropped">--}}
                                {{--                                        <i class="fa fa-check"></i> {{__('backend.moved_drop')}}--}}
                                {{--                                    </a>--}}
                                {{--                                    <button type="button" id="export-submit" disabled--}}
                                {{--                                            class="action_btn btn btn-warning">{{__('backend.export')}}</button>--}}

                                {{--                                </div>--}}
                            </th>
                            <th></th>

                        </tr>
                        </thead>
                        <thead id="floatThead" style="display: none">
                        <tr>
                            <th></th>
                            <th>#</th>
                            {{--                            <th>{{__("backend.id")}}</th>--}}
                            <th>{{__("backend.status")}}</th>
                            <th>{{__('backend.collection')}}</th>
                            <th>{{__('backend.receiver_name')}}</th>
                            <th>{{__('backend.sender_name')}}</th>
                            <th>{{__('backend.from')}}</th>
                            <th>{{__('backend.to')}}</th>
                            <th>{{__('backend.order_number')}}</th>
                            <th>{{__('backend.driver')}}</th>
                            <th>{{__('backend.total_price')}}</th>
                            @if ($additional_filter === 'captain_entry' || $additional_filter === 'quick_entry')
                                <th>{{__('backend.collected_cost')}}</th>
                                {{--                                <th></th>--}}
                                <th>{{__('backend.full_delivered')}}</th>
                                <th>{{__('backend.part_recall')}}</th>
                                <th>{{__('backend.replace')}}</th>
                                <th>{{__('backend.recall')}}</th>
                                <th>{{__('backend.reject')}}</th>
                            @endif
                            <th>
                                <a class="action_btn btn btn-warning" id="toggle" data-state="1"> Select</a>
                                {{--                                <div>--}}
                                {{--                                    <a class="action_btn btn btn-warning disabled" id="delivery_problems"--}}
                                {{--                                       style="width: 415px;">--}}
                                {{--                                        {{__('backend.delivery_problems')}}--}}
                                {{--                                    </a>--}}
                                {{--                                </div>--}}
                                {{--                                <div>--}}
                                {{--                                    <a class="action_btn btn btn-warning" id="toggle"--}}
                                {{--                                       data-state="1"> {{__('backend.select')}}</a>--}}

                                {{--                                    <a class="action_btn btn btn-danger disabled" id="recall_orders">--}}
                                {{--                                        <i class="fa fa-history"></i> {{__('backend.recall')}}--}}
                                {{--                                    </a>--}}

                                {{--                                    <a class="action_btn btn btn-success disabled" id="receive_orders">--}}
                                {{--                                        <i class="fa fa-battery-half"></i> {{__('backend.Receive')}}--}}
                                {{--                                    </a>--}}

                                {{--                                    <a class="action_btn btn btn-info disabled" id="delay_orders">--}}
                                {{--                                        <i class="fa fa-calendar"></i> {{__('backend.delay')}}--}}
                                {{--                                    </a>--}}

                                {{--                                </div>--}}
                                {{--                                <div>--}}
                                {{--                                    <button type="button" id="pdf-submit" disabled--}}
                                {{--                                            class="action_btn btn btn-warning">{{__('backend.print')}}</button>--}}

                                {{--                                    <a class="action_btn btn btn-danger disabled" id="cancel_orders">--}}
                                {{--                                        <i class="fa fa-close"></i> {{__('backend.Cancel')}}--}}
                                {{--                                    </a>--}}

                                {{--                                    <a class="action_btn btn btn-success disabled" id="deliver_orders">--}}
                                {{--                                        <i class="fa fa-battery"></i> {{__('backend.deliver')}}--}}
                                {{--                                    </a>--}}

                                {{--                                    <a class="action_btn btn btn-success disabled" id="forward">--}}
                                {{--                                        <i class="fa fa-eye"></i> {{__('backend.forward')}}--}}

                                {{--                                    </a>--}}
                                {{--                                </div>--}}
                                {{--                                <div>--}}
                                {{--                                    <a class="action_btn btn btn-danger disabled" id="dropped_orders">--}}
                                {{--                                        <i class="fa fa-inbox"></i> {{__('backend.drop')}}--}}
                                {{--                                    </a>--}}
                                {{--                                    <a class="action_btn btn btn-danger disabled" id="order_moved_to">--}}
                                {{--                                        <i class="fa fa-truck"></i> {{__('backend.move_to')}}--}}
                                {{--                                    </a>--}}
                                {{--                                    <a class="action_btn btn btn-danger disabled" id="moved_dropped">--}}
                                {{--                                        <i class="fa fa-check"></i> {{__('backend.moved_drop')}}--}}
                                {{--                                    </a>--}}
                                {{--                                    <button type="button" id="export-submit" disabled--}}
                                {{--                                            class="action_btn btn btn-warning">{{__('backend.export')}}</button>--}}

                                {{--                                </div>--}}
                            </th>
                            <th></th>

                        </tr>
                        </thead>
                        <tbody id="tbody">
                        @foreach($orders as $i => $order)
                            @include('backend.scan.scan-tr', ['order' => $order, 'serial' => ($i+1)])
                        @endforeach
                        <div class="modal fade" id="forwardModal" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        {{--                                        <div class="form-group">--}}
                                        {{--                                            <input type="hidden" class="form-control" name="orderTypeID[]"--}}
                                        {{--                                                   value="{{$orders->order_type_id}}">--}}
                                        {{--                                            <input type="hidden" class="form-control" name="governorateCostID[]"--}}
                                        {{--                                                   value="{{$orders->governorate_cost_id}}">--}}
                                        {{--                                        </div>--}}
                                        <p>{{__('backend.Forward_Orders_In_Agent_Or_Driver')}}</p>
                                        <div class="form-group">
{{--                                            <br>--}}
{{--                                            <select name="agent" id="agent" class="form-control agent" required>--}}
{{--                                                <option value="" selected>{{__('backend.Choose_Agent')}}</option>--}}
{{--                                                @if(! empty($agents))--}}
{{--                                                    @foreach($agents as $agent)--}}
{{--                                                        <option value="{{$agent->id}}">{{$agent->name}}</option>--}}
{{--                                                    @endforeach--}}
{{--                                                @endif--}}
{{--                                            </select>--}}

                                            <br>
                                            <select class="form-control" name="driverID" id="driver">
                                                <option value="">{{__('backend.Choose_Driver')}}</option>
                                                @if(! empty($online_drivers))
                                                    @foreach($online_drivers as $driver)
                                                        <option value="{{$driver->id}}">{{$driver->name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary"
                                                    id="forward_action">{{__('backend.forward')}}</button>
                                            <button type="button" class="btn btn-danger"
                                                    data-dismiss="modal">{{__('backend.close')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--
                                                <div class="modal fade" id="movedToModal" tabindex="-1" role="dialog"
                                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-body">
                                                                <div class="form-group">
                                                                    <select name="store" id="store" class="form-control store" required>
                                                                        <option value="" selected>{{__('backend.Choose_Store')}}</option>
                                                                        @if(! empty($stores))
                                                                            @foreach($stores as $store)
                                                                                <option value="{{$store->id}}">{{$store->name}}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-primary" id="moved_to_action">{{__('backend.moved_to')}}</button>
                                                                    <button type="button" class="btn btn-danger"
                                                                            data-dismiss="modal">{{__('backend.close')}}</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                        --}}
                        </tbody>
                    </table>
                </div>
                @if ($additional_filter === 'captain_entry' || $additional_filter === 'quick_entry')
                    <div class="col-md-12">
                        <button class="btn btn-success save_changes delivery_status_class" type="button" style="width: 300px"
                                id="save_changes">{{__('backend.save')}}</button>
                    </div>
                @endif

                @if ($additional_filter === 'captain_exit')
                    <div class="col-md-12">
                        <button class="btn btn-success" type="button" style="width: 300px"
                                id="save_captain_exit">{{__('backend.save')}}</button>
                    </div>
                @endif


            </form>
            {{--            {!! $orders->appends($_GET)->links() !!}--}}
        @else
            <h3 class="text-center alert alert-warning">No Result Found !</h3>
        @endif
    </div>
</div>
<div id="js-form" class="js-form">
</div>

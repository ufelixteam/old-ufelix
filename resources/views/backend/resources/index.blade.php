@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.resources')}}</title>
@endsection

@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.resources')}}
            @if(permission('addResource')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
              <a class="btn btn-success pull-right" href="{{ route('mngrAdmin.resources.create') }}"><i class="glyphicon glyphicon-plus"></i> {{__('backend.create')}}</a>
            @endif
        </h3>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($resources->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>{{__('backend.id')}}#</th>
                            <th>{{__('backend.name')}}</th>
                            <th>{{__('backend.started_at')}}</th>
                            <th>{{__('backend.renew_at')}}</th>
                            <th>{{__('backend.cost')}}</th>

                            <th class="text-right"></th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($resources as $resource)
                            <tr>
                                <td>{{$resource->id}}</td>
                                <td>{{$resource->name}}</td>
                                <td>{{$resource->started_at}}</td>
                                <td>{{$resource->renew_at}}</td>
                                <td>{{$resource->cost}}</td>
                                <td class="text-right">
                                  @if(permission('showResource')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                    <a class="btn btn-xs btn-primary" href="{{ route('mngrAdmin.resources.show', $resource->id) }}"><i class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>
                                  @endif

                                  @if(permission('editResource')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                    <a class="btn btn-xs btn-warning" href="{{ route('mngrAdmin.resources.edit', $resource->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                                  @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {!! $resources->appends($_GET)->links() !!}

            @else
                <h3 class="text-center alert alert-info">{{__('backend.No_result_found')}}</h3>
            @endif
        </div>
    </div>

@endsection

@extends('backend.layouts.app')
@section('css')
  <title>{{__('backend.vehicles')}} - {{__('backend.view')}}</title>
@endsection
@section('header')
<div class="page-header">
        <h3>{{__('backend.view')}} #{{$truck->id}}</h3> {!! $truck->status_span !!}
        <form action="{{ route('mngrAdmin.trucks.destroy', $truck->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                @if(permission('editVehicle')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                  <a class="btn btn-warning btn-group" role="group" href="{{ route('mngrAdmin.trucks.edit', $truck->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                @endif

                @if(permission('changeStatusVehicle')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                  <button id="change_status" type="button" value="{{ $truck->id }}"  class="btn btn-success">{{ $truck->status_txt }}</button>
                @endif

                @if(permission('deleteVehicle')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                  <button type="submit" class="btn btn-danger">{{__('backend.delete')}} <i class="glyphicon glyphicon-trash"></i></button>
                @endif
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <div class="form-group col-sm-6">
                <label for="model_name">{{__('backend.Model_Name')}}</label>
                <p class="form-control-static">{{ ! empty($truck->model_name)?$truck->model_name:''}}</p>
            </div>
            <div class="form-group col-sm-6">
                <label for="plate_number">{{__('backend.Plate_Number')}}</label>
                <p class="form-control-static">{{ ! empty($truck->plate_number)?$truck->plate_number:''}}</p>
            </div>
            <div class="form-group col-sm-6">
                <label for="chassi_number">{{__('backend.Chassi_Number')}}</label>
                <p class="form-control-static">{{ ! empty($truck->chassi_number)?$truck->chassi_number:''}}</p>
            </div>
            <div class="form-group col-sm-6">
                <label for="year">{{__('backend.Model_Year')}}</label>
                <p class="form-control-static">{{ ! empty($truck->year)?$truck->year:''}}</p>
            </div>

            <div class="form-group col-sm-6">
                <label for="type_id">{{__('backend.type')}}</label>
                <p class="form-control-static">{{ ! empty($truck->Type->name)?$truck->Type->name:''}}</p>
            </div>
            <div class="form-group col-sm-6">
                <label for="color_id">{{__('backend.color')}}</label>
                <p class="form-control-static">{{ ! empty($truck->Color->name)?$truck->Color->name:''}}</p>
            </div>
            <div class="form-group col-sm-6">
                <label for="model_type_id">{{__('backend.Model_Type')}}</label>
                <p class="form-control-static">{{ ! empty($truck->Model_Type->name)?$truck->Model_Type->name:'' }}</p>
            </div>

            <div class="form-group col-sm-6">
                <label for="size_id">{{__('backend.License_Start_Date')}}</label>
                <p class="form-control-static">{{ ! empty($truck->license_start_date)?$truck->license_start_date:''}}</p>
            </div>
            <div class="form-group col-sm-6">
                <label for="size_id">{{__('backend.License_End_Date')}}</label>
                <p class="form-control-static">{{ ! empty($truck->license_end_date)?$truck->license_end_date:''}}</p>
            </div>

            <div class="form-group col-sm-6">
                <label for="driving_licence_expired_date">{{__('backend.License_Image_Front')}}</label>
                <br>

                <?php if ($truck->license_image_front):?>
                <a href="{{ $truck->license_image_front }}"> <img style="height: 100px;width:100px;" src="{{ $truck->license_image_front }}"></a>
                <?php endif;?>
            </div>

            @php /*
              <div class="form-group col-sm-6">
                  <label for="driving_licence_expired_date">{{__('backend.License_Image_Back')}}</label>
                  <br>
                  <?php if ($truck->license_image_back):?>
                  <a href="{{ asset('backend/images/'.$truck->license_image_back) }}"> <img style="height: 100px;width:100px;" src="{{ asset('backend/images/'.$truck->license_image_back) }}"></a>
                  <?php endif;?>
              </div>
              <div class="form-group col-sm-6">
                  <label for="driving_licence_expired_date">{{__('backend.Vehicle_Image')}}</label>
                  <br>
                  <?php if ($truck->image):?>
                  <a href="{{ asset('backend/images/'.$truck->image) }}"> <img style="height: 100px;width:100px;" src="{{ asset('backend/images/'.$truck->image) }}"></a>
                  <?php endif;?>
              </div>
            */ @endphp

            <div>
{{--               @if (! empty($truck->Driver->agent))--}}
{{--                  <div class="col-sm-12" style="margin-top: 90px; border: 2px solid #fdb801;color: #fff;margin-bottom: 20px;background-color: #fdb801;">--}}
{{--                  <h4>{{__('backend.Belonging_Agent_Information')}}</h4>--}}
{{--                  </div>--}}
{{--                  <div class="form-group col-sm-4">--}}
{{--                   <label for="agent_id">{{__('backend.Belonging_Agent_Name')}}: </label>--}}
{{--                   <p class="form-control-static">{{$truck->Driver->agent->name}}</p>--}}
{{--                  </div>--}}
{{--                  <div class="form-group col-sm-4">--}}
{{--                   <label for="mobile">{{__('backend.Belonging_Agent_Mobile')}}: </label>--}}
{{--                   <p class="form-control-static">{{$truck->Driver->agent->mobile}}</p>--}}
{{--                  </div>--}}
{{--                  <div class="form-group col-sm-4">--}}
{{--                   <label for="phone">{{__('backend.Belonging_Agent_Mobile2')}}: </label>--}}
{{--                   <p class="form-control-static">{{$truck->Driver->agent->phone ? $truck->Driver->agent->phone : 'Not Found'}}</p>--}}
{{--                  </div>--}}
{{--              @endif--}}

              @php /*
              <div class="col-sm-12" style="margin-top: 90px; border: 2px solid #fdb801;color: #fff;margin-bottom: 20px;background-color: #fdb801;">
                  <h4>{{__('backend.Belonging_Driver_Information')}}</h4>
              </div>

              @if(! empty($truck->Driver))
              <div class="form-group col-sm-6">
                  <label for="driver_id">{{__('backend.name')}}</label>
                  <p class="form-control-static">{{ ! empty($truck->Driver->name)?$truck->Driver->name:''}}</p>
              </div>
              <div class="form-group col-sm-6">
                  <label for="driver_id">{{__('backend.email')}}</label>
                  <p class="form-control-static">{{  ! empty($truck->Driver->email)? $truck->Driver->email:'' }}</p>
              </div>
              <div class="form-group col-sm-6">
                  <label for="driver_id">{{__('backend.mobile')}}</label>
                  <p class="form-control-static">{{ ! empty($truck->Driver->mobile)?$truck->Driver->mobile:''}}</p>
              </div>
              <div class="form-group col-sm-6">
                  <label for="driver_id">{{__('backend.phone')}}</label>
                  <p class="form-control-static">{{ ! empty($truck->Driver->phone)?$truck->Driver->phone:''}}</p>
              </div>
              <div class="form-group col-sm-6">
                  <label for="driver_id"> {{__('backend.driving_licence_expired_date')}}</label>
                  <p class="form-control-static">{{ ! empty($truck->Driver->driving_licence_expired_date)?$truck->Driver->driving_licence_expired_date:''}}</p>
              </div>
              <div class="form-group col-sm-6">
                  <label for="driver_id">{{__('backend.address')}}</label>
                  <p class="form-control-static">{{ ! empty($truck->Driver->address)?$truck->Driver->address:''}}</p>
              </div>
              <div class="form-group col-sm-6">
                  <label for="driver_id">{{__('backend.national_id_expired_date')}}</label>
                  <p class="form-control-static">{{ ! empty($truck->Driver->national_id_expired_date)?$truck->Driver->national_id_expired_date:''}}</p>
              </div>
        </div>

        @else
          <h4 class="text-center">{{__('backend.This_Truck_do_not_assigned_to_driver')}}</h4>
        @endif

        */ @endphp

            <div class="col-md-12 col-xs-12 col-sm-12">
                <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.trucks.index') }}"><i class="glyphicon glyphicon-backward"></i>  {{__('backend.back')}}</a>
            </div>

        </div>
    </div>

@endsection


@section('scripts')
<script>
$(function () {
    $('#change_status').on('click', function () {
       var Status = $(this).val();
   // alert(Status);
        $.ajax({
            url: "{{ url('/mngrAdmin/change_status/'.$truck->id) }}",
            Type:"GET",
            dataType : 'json',
            success: function(response){
                if(response==0){
                    $('#change_status').html('Verify');
                }else{
                    $('#change_status').html('UnVerify');
                }
             location.reload();
            }
        });
    });
});

</script>
@endsection

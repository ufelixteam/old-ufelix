@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.employees')}} - {{__('backend.create')}}</title>
@endsection

@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-plus"></i> {{__('backend.add_employee')}} </h3>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('mngrAdmin.employees.store') }}" method="POST" enctype="multipart/form-data" name="validateForm">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                  <div class="form-group col-md-6 col-sm-6 @if($errors->has('name')) has-error @endif">
                     <label for="name-field">{{__('backend.name')}}</label>
                     <input type="text" id="name-field" name="name" class="form-control" value="{{ old("name") }}"/>
                     @if($errors->has("name"))
                      <span class="help-block">{{ $errors->first("name") }}</span>
                     @endif
                   </div>
                   <div class="form-group col-md-6 col-sm-6 @if($errors->has('email')) has-error @endif">
                       <label for="email-field">{{__('backend.email')}}</label>
                       <input type="text" id="email-field" name="email" class="form-control" value="{{ old("email") }}"/>
                       @if($errors->has("email"))
                        <span class="help-block">{{ $errors->first("email") }}</span>
                       @endif
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('mobile')) has-error @endif">
                       <label for="mobile-field">{{__('backend.mobile')}}</label>
                       <input type="text" id="mobile-field" name="mobile" class="form-control" value="{{ old("mobile") }}"/>
                       @if($errors->has("mobile"))
                        <span class="help-block">{{ $errors->first("mobile") }}</span>
                       @endif
                     </div>
                     <div class="form-group col-md-6 col-sm-6 @if($errors->has('salary')) has-error @endif">
                       <label for="salary-field">{{__('backend.salary')}}</label>
                       <input type="text" id="salary-field" name="salary" class="form-control" value="{{ old("salary") }}"/>
                       @if($errors->has("salary"))
                        <span class="help-block">{{ $errors->first("salary") }}</span>
                       @endif
                     </div>
                   </div>
                   <div class="row">
                     <div class="form-group col-md-12 col-sm-12 @if($errors->has('job_title')) has-error @endif">
                       <label for="job_title-field">{{__('backend.job_title')}}</label>
                       <input type="text" id="job_title-field" name="job_title" class="form-control" value="{{ old("job_title") }}"/>
                       @if($errors->has("job_title"))
                        <span class="help-block">{{ $errors->first("job_title") }}</span>
                       @endif
                     </div>
                   </div>
                   <div class="row">
                    <div class="form-group col-md-12 col-sm-12 @if($errors->has('bio')) has-error @endif">
                       <label for="bio-field">{{__('backend.bio')}}</label>
                       <textarea class="form-control" id="bio-field" rows="3" name="bio">{{ old("bio") }}</textarea>
                       @if($errors->has("bio"))
                        <span class="help-block">{{ $errors->first("bio") }}</span>
                       @endif
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('image')) has-error @endif">
                       <label for="image-field">{{__('backend.image')}}</label>
                       <div id="image_upload"></div>
                       <input type="file" id="image-field" name="image" class="form-control uploadPhoto" value="{{ old("image") }}"/>
                       @if($errors->has("image"))
                        <span class="help-block">{{ $errors->first("image") }}</span>
                       @endif
                    </div>
                  </div>
                  <div class="well well-sm col-md-12 col-sm-12">
                      <button type="submit" class="btn btn-primary">{{__('backend.create')}}</button>
                      <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.employees.index') }}"><i class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
                  </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
   <script type="text/javascript">
  $(function() {
    $("form[name='validateForm']").validate({
      // Specify validation rules
      rules: {
        name: {
          required: true,
        },
        email: {
          required: true,
          email: true,
        },
        mobile: {
          required: true,
          number:true,
          rangelength:[11,11],
        },
        salary: {
          required: true,
          number:true
        },
        job_title: {
          required: true,
        },
      },

      // Specify validation error messages
      messages: {
        name: {
          required: "{{__('backend.Please_Enter_Employee_Name')}}",
        },
        email: {
          required: "{{__('backend.Please_Enter_Employee_Email')}}",
          email: "{{__('backend.Please_Enter_Correct_E-mail')}}",
        },
        mobile: {
          required: "{{__('backend.Please_Enter_Mobile_Number')}}",
          number: "{{__('backend.Please_Enter_Avalid_Number')}}",
          rangelength:"{{__('backend.Mobile_Must_Be11_Digits')}}",
        },
        salary: {
          required: "{{__('backend.Please_Enter_Employee_Salary')}}",
          number: "{{__('backend.Please_Enter_Avalid_Number')}}",
        },
        job_title: {
          required: "{{__('backend.Please_Enter_Job_Title')}}",
        },
      },

       errorPlacement: function(error, element) {
          $(element).parents('.form-group').append(error)
      },

      highlight: function (element) {
          $(element).parent().addClass('error')
      },
      unhighlight: function (element) {
          $(element).parent().removeClass('error')
      },
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function(form) {
          form.submit();
      }
    });
  })
</script>
@endsection

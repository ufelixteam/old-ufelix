<!-- Start Counter -->
<section class="logistic_counter_area">
    <div class="container">
        <div class="logistic_counter clearfix">

            
            <div class="col-md-2 col-sm-6">
                <div class="single-counter">
                    <div class="icon-count"><i class="fas  fa-box-open"></i></div>
                    <span class="counter"><?php echo e(! empty($settings) && ! empty($settings['no_orders'] ) ? $settings['no_orders'] : '0'); ?></span>
                    <h4><?php echo app('translator')->getFromJson('website.no_orders'); ?> </h4>
                </div>
            </div>
            <div class="col-md-2 col-sm-6">
                <div class="single-counter">
                    <div class="icon-count"><i class="fa fa-building"></i></div>
                    <span class="counter"><?php echo e(! empty($settings) && ! empty($settings['no_corporates'] ) ? $settings['no_corporates'] : '0'); ?></span>
                    <h4><?php echo app('translator')->getFromJson('website.no_corporates'); ?> </h4>
                    
                </div>
            </div>
            <div class="col-md-2 col-sm-6">
                <div class="single-counter">
                    <div class="icon-count"><i class="fa fa-users"></i></div>
                    <span class="counter"><?php echo e(! empty($settings) && ! empty($settings['no_customers'] ) ? $settings['no_customers'] : '0'); ?></span>
                    <h4><?php echo app('translator')->getFromJson('website.no_customers'); ?> </h4>
                </div>
            </div>
            <div class="col-md-2 col-sm-6">
                <div class="single-counter">
                    <div class="icon-count"><i class="fas  fa-user-tie"></i></div>
                    <span class="counter"><?php echo e(! empty($settings) && ! empty($settings['no_agents'] ) ? $settings['no_agents'] : '0'); ?></span>
                    <h4><?php echo app('translator')->getFromJson('website.no_agents'); ?> </h4>
                </div>
            </div>
            <div class="col-md-2 col-sm-6">
                <div class="single-counter">
                    <div class="icon-count"><i class="fas  fa-truck-moving"></i></div>
                    <span class="counter"><?php echo e(! empty($settings) && ! empty($settings['no_vehicles'] ) ? $settings['no_vehicles'] : '0'); ?></span>
                    <h4><?php echo app('translator')->getFromJson('website.no_vehicles'); ?> </h4>
                </div>
            </div>
            <div class="col-md-2 col-sm-6">
                <div class="single-counter">
                    <div class="icon-count"><i class="fa fa-users"></i></div>
                    <span class="counter"><?php echo e(! empty($settings) && ! empty($settings['no_drivers'] ) ? $settings['no_drivers'] : '0'); ?></span>
                    <h4><?php echo app('translator')->getFromJson('website.no_drivers'); ?> </h4>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Counter --><?php /**PATH /home/www/ufelix/ufelix/resources/views/frontend/home/statistic.blade.php ENDPATH**/ ?>
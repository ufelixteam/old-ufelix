<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        @if($collection_orders->count())
            <table class="table table-condensed table-striped ">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('backend.Task_No')}}</th>
                    <th>{{__('backend.corporate')}}</th>
                    <th>{{__('backend.Driver')}}</th>
                    <th>{{__('backend.collection')}}</th>
                    <th>{{__('backend.notes')}}</th>
                    <th style="width: 15%;">{{__('backend.address')}}</th>
                    <th style="width: 15%;">{{__('backend.task_type')}}</th>
                    <th>#{{__('backend.orders')}}</th>

                    {{--                    <th>{{__('backend.Agent')}}</th>--}}
                    <th>{{__('backend.captain_received_date')}}</th>
                    <th>{{__('backend.Status')}}</th>
                    <th>
                        <input type="checkbox" class="form-control"
                               style="display: inline-block;vertical-align: bottom;" id="printAll">
                        <button type="button" id="pdf-submit" style="display: inline-block"
                                class="btn btn-warning btn-pdf btn-xs ">{{__('backend.print')}}</button>
                    </th>
                    <th>
                        <input type="checkbox" class="form-control"
                               style="display: inline-block;vertical-align: bottom;" id="dropAll">
                        <a class="btn btn-xs btn btn btn-success" style="display: inline-block" id="drop_off">
                            {{__('backend.drop_off')}}

                        </a>
                    </th>
                    <th>
                        <input type="checkbox" class="form-control"
                               style="display: inline-block;vertical-align: bottom;" id="forwardAll">
                        <a class="btn btn-xs btn btn btn-warning" style="display: inline-block;background-color: #461e53;" id="forward">
                            {{__('backend.forward')}}

                        </a>
                    </th>

                </tr>
                </thead>
                <tbody>

                @foreach($collection_orders as $collection)
                    <tr class='clickable-row' data-href="{{ route('mngrAdmin.pickups.show', $collection->id) }}">
                        <td>{{$collection->id}}</td>
                        <td>{{$collection->pickup_number}}</td>
                        <td>{{! empty($collection->corporate) ? $collection->corporate->name : ''}}</td>
                        <td>{{! empty($collection->driver) ? $collection->driver->name : ''}}</td>
                        <td>{{$collection->collection_id}}</td>
                        <td>{{$collection->notes}}</td>
                        <td>{{$collection->sender_address}}</td>
                        <td>{!! $collection->task_types !!}</td>
                        <td>{{$collection->is_fake_pickup ?  $collection->orders_count : $collection->total_order_no}}</td>
                        {{--                        <td>{{! empty($collection->agent) ? $collection->agent->name : ''}}</td>--}}
                        <td>{{$collection->captain_received_time ? date("Y-m-d", strtotime($collection->captain_received_time)) : ''}}</td>

                        <td>{!! $collection->status_span !!}</td>
                        <td class="selectpackup exclude-td">
                            <input type="checkbox" class="printpack" name="printpack[]"
                                   value="{{$collection->id}}"
                                   @if($collection->status == '0' || !$collection->driver_id) disabled @endif/>
                        </td>
                        <td class="selectpackup exclude-td">
                            <input type="checkbox" class="selectpack" name="selectpackup[]"
                                   value="{{$collection->id}}"
                                   @if(($collection->status == '0' && !$collection->driver_id) || $collection->status == 4 || $collection->status == 3) disabled @endif/>
                        </td>
                        <td class="selectpackup exclude-td">
                            <input type="checkbox" class="forwardpack" name="forwardpackup[]"
                                   value="{{$collection->id}}"
                                   @if($collection->status != '0' && $collection->status != '1') disabled @endif/>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {!! $collection_orders->appends($_GET)->links() !!}
        @else
            <h3 class="text-center alert alert-warning">No Result Found !</h3>
        @endif
    </div>
</div>

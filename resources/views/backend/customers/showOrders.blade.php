@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.customer')}} - {{__('backend.orders')}}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <style>
        .jconfirm.jconfirm-modern .jconfirm-box div.jconfirm-title-c .jconfirm-icon-c {
            -webkit-transition: -webkit-transform .5s;
            transition: -webkit-transform .5s;
            transition: transform .5s;
            transition: transform .5s, -webkit-transform .5s;
            -webkit-transform: scale(0);
            transform: scale(0);
            display: block;
            margin-right: 0;
            margin-left: 0;
            margin-bottom: 10px;
            font-size: 69px;
            color: #9e1717;
        }

        .nav-tabs li:first-child.active a {
            color: #fff;
            background-color: #337ab7;
        }

        .nav-tabs li:last-child.active a {
            color: #fff;
            background-color: #dd4b39;
        }
    </style>
@endsection

@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-align-justify"></i>
            {{__('backend.orders')}}
            @if($corporate)
                {{__('backend.user_of_corporate')}} - {{$corporate->name}}
            @else
                {{__('backend.user')}}
            @endif
            [ # {{$customer->id}} - {{$customer->name}} ]
        </h3>
    </div>
@endsection

@section('content')
    <div>

        <!-- Nav tabs -->
        <ul id="myTabs" class="nav nav-tabs nav-justified nav-pills" role="tablist">
            <li role="presentation" class="{{ ! empty($sort) && $sort == 'currentorders' ? 'active' : ''}}"><a
                    href="#home" aria-controls="home" role="tab" data-toggle="tab">{{__('backend.current')}}</a></li>
            <li role="presentation" class="{{ ! empty($sort) && $sort == 'orders' ? 'active' : ''}}"><a href="#profile"
                                                                                                        aria-controls="profile"
                                                                                                        role="tab"
                                                                                                        data-toggle="tab">{{__('backend.finished')}}</a>
            </li>
{{--            <li role="presentation" class="{{ ! empty($sort) && $sort == 'recall' ? 'active' : ''}}"><a href="#recall"--}}
{{--                                                                                                        aria-controls="recall"--}}
{{--                                                                                                        role="tab"--}}
{{--                                                                                                        data-toggle="tab"--}}
{{--                                                                                                        data-sort="recall"--}}
{{--                                                                                                        class="order_type">{{__('backend.recall')}}</a>--}}
{{--            </li>--}}
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane {{ ! empty($sort) && $sort == 'currentorders' ? 'active' : ''}} "
                 id="home">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        @if(count($currentorders) > 0)
                            <form action="{{URL::asset('/mngrAdmin/pdf/4/' . $id)}}" method="get" target="_blank">
                                <table class="table table-condensed table-striped text-center">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{__('backend.order_type')}}</th>
                                        <th>{{__('backend.order_number')}}</th>
                                        <th>{{__('backend.delivery_price')}}</th>
                                        <th>{{__('backend.sender_name')}}</th>
                                        <th>{{__('backend.receiver_name')}}</th>
{{--                                        <th>{{__('backend.receiver_code')}}</th>--}}
                                        <th>{{__('backend.status')}}</th>
                                        <th class="pull-right"></th>
                                        <td>
                                            <input type="button" class="btn btn-info btn-xs" id="toggle"
                                                   value="{{__('backend.select')}}" onClick="do_this()"/>
                                            <button type="submit"
                                                    class="btn btn-warning btn-xs btn-pdf">{{__('backend.pdf')}}</button>
                                        </td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($currentorders as $index => $order)
                                        <tr>
                                            <td>{{$order->id}}</td>

                                            <td>{{$order->type}}</td>
                                            <td>{{$order->order_number}}</td>
                                            <td>{{$order->delivery_price}}</td>
                                            <td>{{$order->sender_name}}</td>
                                            <td>{{$order->receiver_name}}</td>
{{--                                            <td>{{$order->receiver_code}}</td>--}}
                                            <td>
                                                {!! $order->status_span !!}

                                            </td>
                                            <td class="pull-right">
                                                <a class="btn btn-xs btn-primary"
                                                   href="{{ route('mngrAdmin.orders.show', $order->id) }}"><i
                                                        class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}
                                                </a>
                                            </td>
                                            <td>
                                                <input type="checkbox" name="select[]" value="{{$order->id}}"/>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </form>
                            {!! $currentorders->appends(['sort' => 'currentorders'])->appends($_GET)->links() !!}
                        @else
                            <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
                        @endif
                    </div>
                </div>
            </div> <!-- tab end -->
            <div role="tabpanel" class="tab-pane {{ ! empty($sort) && $sort == 'orders' ? 'active' : ''}}" id="profile">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        @if(count($orders) > 0)
                            <form action="{{URL::asset('/mngrAdmin/pdf/4/'.$id)}}" method="get" target="_blank">
                                <table class="table table-condensed table-striped text-center">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{__('backend.order_type')}}</th>
                                        <th>{{__('backend.order_number')}}</th>
                                        <th>{{__('backend.delivery_price')}}</th>
                                        <th>{{__('backend.sender_name')}}</th>
                                        <th>{{__('backend.receiver_name')}}</th>
{{--                                        <th>{{__('backend.receiver_code')}}</th>--}}
                                        <th>{{__('backend.status')}}</th>
                                        <th class="pull-right"></th>
                                        <td>
                                            <input type="button" class="btn btn-info btn-xs" id="toggle"
                                                   value="{{__('backend.select')}}" onClick="do_this()"/>
                                            <button type="submit"
                                                    class="btn btn-warning btn-xs btn-pdf">{{__('backend.pdf')}}</button>
                                        </td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orders as  $order)
                                        <tr>
                                            <td>{{$order->id}}</td>
                                            <td>{{$order->type}}</td>
                                            <td>{{$order->order_number}}</td>
                                            <td>{{$order->delivery_price}}</td>
                                            <td>{{$order->sender_name}}</td>
                                            <td>{{$order->receiver_name}}</td>
{{--                                            <td>{{$order->receiver_code}}</td>--}}
                                            <td>
                                                {!! $order->status_span !!}
                                            </td>
                                            <td class="pull-right">
                                                <a class="btn btn-xs btn-primary"
                                                   href="{{ route('mngrAdmin.orders.show', $order->id) }}"><i
                                                        class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}
                                                </a>
                                            </td>
                                            <td>
                                                <input type="checkbox" name="select[]" value="{{$order->id}}"/>
                                            </td>
                                        </tr>

                                    @endforeach
                                    </tbody>
                                </table>
                            </form>
                            {!! $orders->appends($_GET)->appends(['sort' => 'orders'])->links() !!}
                        @else
                            <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
                        @endif
                    </div>
                </div>
            </div> <!-- tab end -->
{{--            <div role="tabpanel" class="tab-pane {{ ! empty($sort) && $sort == 'recall' ? 'active' : ''}}" id="recall">--}}
{{--                <div class="row">--}}
{{--                    <div class="col-md-12 col-xs-12 col-sm-12">--}}
{{--                        @if(! empty($recalled_orders) && count($recalled_orders) > 0)--}}
{{--                            <form action="{{URL::asset('/mngrAdmin/pdf/1/'. $id)}}" method="get" target="_blank">--}}
{{--                                <table class="table table-condensed table-striped text-center">--}}
{{--                                    <thead>--}}
{{--                                    <tr>--}}
{{--                                        <th>#</th>--}}
{{--                                        <th>{{__('backend.order_type')}}</th>--}}
{{--                                        <th>{{__('backend.order_number')}}</th>--}}
{{--                                        <th>{{__('backend.order_price')}}</th>--}}
{{--                                        <th>{{__('backend.sender_name')}}</th>--}}
{{--                                        <th>{{__('backend.receiver_name')}}</th>--}}
{{--                                        <th>{{__('backend.receiver_code')}}</th>--}}
{{--                                        <th>--}}
{{--                                            <a class="btn btn-xs btn btn btn-success" id="drop_off">--}}
{{--                                                <i class="fa fa-dropbox"></i> {{__('backend.drop_off')}}--}}

{{--                                            </a>--}}
{{--                                        </th>--}}
{{--                                        <th class="pull-right"></th>--}}
{{--                                        <th>--}}
{{--                                            <input type="button" class="btn btn-info btn-xs" id="toggle"--}}
{{--                                                   value="{{__('backend.select')}}" onClick="do_this()"/>--}}
{{--                                            <button type="submit"--}}
{{--                                                    class="btn btn-warning btn-xs btn-pdf">{{__('backend.pdf')}}</button>--}}
{{--                                        </th>--}}
{{--                                    </tr>--}}
{{--                                    </thead>--}}
{{--                                    <tbody>--}}
{{--                                    @foreach($recalled_orders as  $order)--}}
{{--                                        <tr>--}}
{{--                                            <td>{{$order->id}}</td>--}}
{{--                                            <td>{{$order->type}}</td>--}}
{{--                                            <td>{{$order->order_number}}</td>--}}
{{--                                            <td>{{$order->order_price}}</td>--}}
{{--                                            <td>{{$order->sender_name}}</td>--}}
{{--                                            <td>{{$order->receiver_name}}</td>--}}
{{--                                            <td>{{$order->receiver_code}}</td>--}}
{{--                                            <td class="selectdropoff">--}}
{{--                                                <input type="checkbox" class="warehouse_dropoff"--}}
{{--                                                       name="warehouse_dropoff[]"--}}
{{--                                                       value="{{$order->id}}"--}}
{{--                                                       @if($order->warehouse_dropoff == 1) disabled--}}
{{--                                                       @endif @if($order->warehouse_dropoff == 1) checked @endif/>--}}
{{--                                            </td>--}}
{{--                                            <td class="pull-right">--}}
{{--                                                <a class="btn btn-xs btn-primary"--}}
{{--                                                   href="{{ route('mngrAdmin.orders.show', $order->id) }}"><i--}}
{{--                                                        class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}--}}
{{--                                                </a>--}}
{{--                                            </td>--}}
{{--                                            <td>--}}
{{--                                                <input type="checkbox" name="select[]" value="{{$order->id}}"/>--}}
{{--                                            </td>--}}
{{--                                        </tr>--}}
{{--                                    @endforeach--}}
{{--                                    </tbody>--}}
{{--                                </table>--}}
{{--                            </form>--}}
{{--                            {!! $orders->appends($_GET)->appends(['sort' => 'recall'])->links() !!}--}}

{{--                        @else--}}
{{--                            <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div> <!-- tab end -->--}}
        </div>

    </div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script>
        $('#myTabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        })
        $(function () {
            $(".ask_view").on('click', function () {
                var id = $(this).data('id');
                $.ajax({
                    url: '{!! url("/mngrAgent/ask_review_order/") !!}' + '/' + id,
                    type: 'get',

                }).done(function (data) {
                    if (data == "ok") {
                        $.alert({
                            title: "{{__('backend.Request_Review_Sent')}}",
                            icon: 'fa fa-smile-o',
                            theme: 'modern',
                            content: "{{__('backend.Your_request_sent_to_Ufelix_Admin_successfully')}}",
                            buttons: {
                                info: {
                                    text: 'OK',
                                    btnClass: 'btn-success',
                                    action: function () {
                                        window.location.href = window.location.href;
                                    }
                                }
                            }
                        });
                    }
                });
            });

        });

        // Select All Orders function For Print
        function do_this() {

            var checkboxes = document.getElementsByName('select[]');
            var button = document.getElementById('toggle');

            if (button.value == 'select') {
                for (var i in checkboxes) {
                    checkboxes[i].checked = 'FALSE';
                }
                button.value = 'deselect'
            } else {
                for (var i in checkboxes) {
                    checkboxes[i].checked = '';
                }
                button.value = 'select';
            }
        }

    </script>
@endsection

@extends('agent.layouts.app')
@section('css')
 
@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-plus"></i> Orders / Create </h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <form action="{{ route('mngrAgent.orders.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group col-md-6 col-sm-6 @if($errors->has('reciever_name')) has-error @endif">
                       <label for="reciever_name-field">Reciever Name</label>
                    <input type="text" id="reciever_name-field" name="reciever_name" class="form-control" value="{{ old("reciever_name") }}"/>
                       @if($errors->has("reciever_name"))
                        <span class="help-block">{{ $errors->first("reciever_name") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('reciever_mobile')) has-error @endif">
                       <label for="reciever_mobile-field">Reciever Mobile</label>
                    <input type="text" id="reciever_mobile-field" name="reciever_mobile" class="form-control" value="{{ old("reciever_mobile") }}"/>
                       @if($errors->has("reciever_mobile"))
                        <span class="help-block">{{ $errors->first("reciever_mobile") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('reciever_phone')) has-error @endif">
                       <label for="reciever_phone-field">Reciever Mobile 2 or Phone</label>
                    <input type="text" id="reciever_phone-field" name="reciever_phone" class="form-control" value="{{ old("reciever_phone") }}"/>
                       @if($errors->has("reciever_phone"))
                        <span class="help-block">{{ $errors->first("reciever_phone") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('reciever_latitude')) has-error @endif">
                       <label for="reciever_latitude-field">Reciever Latitude</label>
                    <input type="text" id="reciever_latitude-field" name="reciever_latitude" class="form-control" value="{{ old("reciever_latitude") }}"/>
                       @if($errors->has("reciever_latitude"))
                        <span class="help-block">{{ $errors->first("reciever_latitude") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('reciever_longitude')) has-error @endif">
                       <label for="reciever_longitude-field">Reciever Longitude</label>
                    <input type="text" id="reciever_longitude-field" name="reciever_longitude" class="form-control" value="{{ old("reciever_longitude") }}"/>
                       @if($errors->has("reciever_longitude"))
                        <span class="help-block">{{ $errors->first("reciever_longitude") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('reciever_address')) has-error @endif">
                       <label for="reciever_address-field">Reciever Address</label>
                    <textarea class="form-control" id="reciever_address-field" rows="4" name="reciever_address">{{ old("reciever_address") }}</textarea>
                       @if($errors->has("reciever_address"))
                        <span class="help-block">{{ $errors->first("reciever_address") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('reciever_famous_palce')) has-error @endif">
                       <label for="reciever_famous_palce-field">Reciever Famous Palce</label>
                    <textarea class="form-control" id="reciever_famous_palce-field" rows="4" name="reciever_famous_palce">{{ old("reciever_famous_palce") }}</textarea>
                       @if($errors->has("reciever_famous_palce"))
                        <span class="help-block">{{ $errors->first("reciever_famous_palce") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('sender_name')) has-error @endif">
                       <label for="sender_name-field">Sender Name</label>
                    <input type="text" id="sender_name-field" name="sender_name" class="form-control" value="{{ old("sender_name") }}"/>
                       @if($errors->has("sender_name"))
                        <span class="help-block">{{ $errors->first("sender_name") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('sender_mobile')) has-error @endif">
                       <label for="sender_mobile-field">Sender Mobile</label>
                    <input type="text" id="sender_mobile-field" name="sender_mobile" class="form-control" value="{{ old("sender_mobile") }}"/>
                       @if($errors->has("sender_mobile"))
                        <span class="help-block">{{ $errors->first("sender_mobile") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('sender_phone')) has-error @endif">
                       <label for="sender_phone-field">Sender Mobile 2 or Phone</label>
                    <input type="text" id="sender_phone-field" name="sender_phone" class="form-control" value="{{ old("sender_phone") }}"/>
                       @if($errors->has("sender_phone"))
                        <span class="help-block">{{ $errors->first("sender_phone") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('sender_latitude')) has-error @endif">
                       <label for="sender_latitude-field">Sender Latitude</label>
                    <input type="text" id="sender_latitude-field" name="sender_latitude" class="form-control" value="{{ old("sender_latitude") }}"/>
                       @if($errors->has("sender_latitude"))
                        <span class="help-block">{{ $errors->first("sender_latitude") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('sender_longitude')) has-error @endif">
                       <label for="sender_longitude-field">Sender Longitude</label>
                    <input type="text" id="sender_longitude-field" name="sender_longitude" class="form-control" value="{{ old("sender_longitude") }}"/>
                       @if($errors->has("sender_longitude"))
                        <span class="help-block">{{ $errors->first("sender_longitude") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('sender_address')) has-error @endif">
                       <label for="sender_address-field">Sender Address</label>
                    <textarea class="form-control" id="sender_address-field" rows="4" name="sender_address">{{ old("sender_address") }}</textarea>
                       @if($errors->has("sender_address"))
                        <span class="help-block">{{ $errors->first("sender_address") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('sender_famous_palce')) has-error @endif">
                       <label for="sender_famous_palce-field">Sender Famous Palce</label>
                    <textarea class="form-control" id="sender_famous_palce-field" rows="4" name="sender_famous_palce">{{ old("sender_famous_palce") }}</textarea>
                       @if($errors->has("sender_famous_palce"))
                        <span class="help-block">{{ $errors->first("sender_famous_palce") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('status')) has-error @endif">
                       <label for="status-field">Status</label>
                    <div class="btn-group" data-toggle="buttons"><label class="btn btn-primary"><input type="radio" value="true" name="status-field" id="status-field" autocomplete="off"> True</label><label class="btn btn-primary active"><input type="radio" name="status-field" value="false" id="status-field" autocomplete="off"> False</label></div>
                       @if($errors->has("status"))
                        <span class="help-block">{{ $errors->first("status") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('type')) has-error @endif">
                       <label for="type-field">Type</label>
                    <div class="btn-group" data-toggle="buttons"><label class="btn btn-primary"><input type="radio" value="true" name="type-field" id="type-field" autocomplete="off"> True</label><label class="btn btn-primary active"><input type="radio" name="type-field" value="false" id="type-field" autocomplete="off"> False</label></div>
                       @if($errors->has("type"))
                        <span class="help-block">{{ $errors->first("type") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('type_id')) has-error @endif">
                       <label for="type_id-field">Truck Type</label>
                    <select id="type_id-field" name="type_id" class="form-control" >
                        @if(! empty($truck_types))
                            @foreach($truck_types as $type)
                              <option value="{{$type->id}}" {{ old("type_id") == $type->id ? 'selected' : '' }}>{{$type->name}}</option>

                            @endforeach

                        @endif
                    </select>
                       @if($errors->has("type_id"))
                        <span class="help-block">{{ $errors->first("type_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('weight_id')) has-error @endif">
                       <label for="weight_id-field">Weight</label>
                    <select id="weight_id-field" name="weight_id" class="form-control" >
                        @if(! empty($weights))
                            @foreach($weights as $weight)
                              <option value="{{$weight->id}}" {{ old("weight_id") == $weight->id ? 'selected' : '' }}>{{$weight->weight}}</option>

                            @endforeach

                        @endif
                    </select>
                       @if($errors->has("weight_id"))
                        <span class="help-block">{{ $errors->first("weight_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('customer_id')) has-error @endif">
                       <label for="customer_id-field">Customer</label>
                    <input type="text" id="customer_id-field" name="customer_id" class="form-control" value="{{ old("customer_id") }}"/>
                       @if($errors->has("customer_id"))
                        <span class="help-block">{{ $errors->first("customer_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('payment_method_id')) has-error @endif">
                       <label for="payment_method_id-field">Payment Method</label>
                   
                   <select id="payment_method_id-field" name="payment_method_id" class="form-control" >
                        @if(! empty($payment_methods))
                            @foreach($payment_methods as $payment_method)
                              <option value="{{$payment_method->id}}" {{ old("payment_method") == $payment_method->id ? 'selected' : '' }}>{{$payment_method->name}}</option>

                            @endforeach

                        @endif
                    </select>

                       @if($errors->has("payment_method_id"))
                        <span class="help-block">{{ $errors->first("payment_method_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('size_id')) has-error @endif">
                       <label for="size_id-field">Size</label>
                   
                   <select id="size_id-field" name="size_id" class="form-control" >
                        @if(! empty($sizes))
                            @foreach($sizes as $size)
                              <option value="{{$size->id}}" {{ old("size_id") == $size->id ? 'selected' : '' }}>{{$size->size}}</option>

                            @endforeach

                        @endif
                    </select>
                       @if($errors->has("size_id"))
                        <span class="help-block">{{ $errors->first("size ") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-12 col-sm-12 @if($errors->has('notes')) has-error @endif">
                       <label for="notes-field">Notes</label>
                    <textarea class="form-control" id="notes-field" rows="4" name="notes">{{ old("notes") }}</textarea>
                       @if($errors->has("notes"))
                        <span class="help-block">{{ $errors->first("notes") }}</span>
                       @endif
                    </div>
                   
                <div class="well well-sm col-md-12 col-sm-12 col-xs-12">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAgent.orders.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  
@endsection

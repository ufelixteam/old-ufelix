<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Ufelix</title>
    <style type="text/css" media="all">
        .polica {
            --padding: 20px;
            text-transform: capitalize;
            --margin: 30px;
        }

        .head {
            border-bottom: 4px solid #343a40;
        }

        .head h4 {
            font-weight: bold;

        }

        .head button {
            border: none;
            box-shadow: none;
            outline: none;
            padding: .3rem 0.75rem !important;
        }

        .btn.focus, .btn:focus {
            border: none !important;
            box-shadow: none !important;
            outline: none !important;
        }

        #invoice-body {
            padding: 20px 10px 1px 10px;
            max-width: 100%;
            box-shadow: 1px 1px 12px #343a40;
            border-radius: 5px;
        }

        .logo-ufelix .img-thumbnail {
            border: none;
        }

        .logo-ufelix .line {
            margin-top: 0px;
            margin-bottom: .1rem;
            border-top: 2px solid #e7e9ea;
            width: 95%;
        }

        .data {
            background-color: #f80;
            height: 40px;
            color: #fff;
            line-height: 40px;
            font-weight: bold;

        }

        .order_number {
            margin-left: 14px;
            margin-top: 10px;
            margin-bottom: 40px;
        }

        .order_number ul li {
            margin-bottom: 5px;
        }

        .customer-info {
            border: 1px solid #f80;
            margin: 10px;
            width: 97%;
        }

        .customer-info > div {
            border: 2px solid #f80;
            padding: 15px;

        }

        /*#table td, #table th {*/
        /*    font-size: 13px;*/
        /*    border-top: 1px solid #ff8800;*/
        /*    text-align: center;*/
        /*}*/

        /*bootstrab class*/
        *,
        *::before,
        *::after {
            box-sizing: border-box;
        }

        html {
            font-family: sans-serif;
            line-height: 1.15;
            -webkit-text-size-adjust: 100%;
            -ms-overflow-style: scrollbar;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
        }


        article, aside, figcaption, figure, footer, header, hgroup, main, nav, section {
            display: block;
        }

        body {
            margin: 0;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, Noto Sans, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            text-align: left;
            background-color: #fff;
        }

        .list-inline {
            padding-left: 0;
            list-style: none;
        }

        .row {
            margin-right: -15px;
            margin-left: -15px;
        }

        .row-no-gutters {
            margin-right: 0;
            margin-left: 0;
        }

        .row-no-gutters [class*="col-"] {
            padding-right: 0;
            padding-left: 0;
        }

        .col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col,
        .col-auto, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm,
        .col-sm-auto, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .col-md,
        .col-md-auto, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg,
        .col-lg-auto, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl,
        .col-xl-auto {
            position: relative;
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
        }

        .img-thumbnail {
            /* padding: 0.25rem; */
            background-color: #fff;
            border: 1px solid #dee2e6;
            border-radius: 0.25rem;
            max-width: 65%;
            /* height: auto; */
            height: 85px;
            margin: 12px 0 0 38px;
        }

        .d-inline {
            display: inline !important;
        }

        h1, h2, h3, h4, h5, h6 {
            margin-top: 0;
            margin-bottom: 0.5rem;
        }

        h1, h2, h3, h4, h5, h6,
        .h1, .h2, .h3, .h4, .h5, .h6 {
            margin-bottom: 0.5rem;
            font-family: inherit;
            font-weight: 500;
            line-height: 1.3;
            color: inherit;
        }

        h1, .h1 {
            font-size: 2.5rem;
        }

        h2, .h2 {
            font-size: 2rem;
        }

        h3, .h3 {
            font-size: 1.75rem;
        }

        h4, .h4 {
            font-size: 1.2rem;
        }

        h5, .h5 {
            font-size: 1.25rem;
        }

        h6, .h6 {
            font-size: 1rem;
        }

        .text-right {
            text-align: right !important;
        }

        .text-center {
            text-align: center !important;
        }

        .table-bordered {
            border: 1px solid #000;
        }

        .table {
            background-color: transparent;
            width: 100% !important;
            margin-top: 1px !important;
            border-collapse: collapse !important;
        }

        .table-bordered td, .table-bordered th {
            border: 1px solid #000;
        }

        .table td, .table th {
            padding: 4px;
            vertical-align: top;
            /*border-top: 1px solid #000;*/
        }


        .btn-primary {
            color: #fff;
            background-color: #007bff;
            border-color: #007bff;
        }

        .btn-secondary:focus, .btn-secondary.focus {
            box-shadow: 0 0 0 0.2rem rgba(130, 138, 145, 0.5);
        }

        .btn-secondary.disabled, .btn-secondary:disabled {
            color: #fff;
            background-color: #6c757d;
            border-color: #6c757d;
        }

        .btn-secondary {
            color: #fff;
            background-color: #6c757d;
            border-color: #6c757d;
        }


        /*.table-bordered {*/
        /*    border: 1px solid #000;*/
        /*}*/


        .d-inline {
            display: inline !important;
        }

        .col-sm-1 {
            -ms-flex: 0 0 8.333333%;
            flex: 0 0 8.333333%;
            max-width: 8.333333%;
        }

        .col-sm-2 {
            -ms-flex: 0 0 16.666667%;
            flex: 0 0 16.666667%;
            max-width: 16.666667%;
        }

        .col-sm-3 {
            -ms-flex: 0 0 25%;
            flex: 0 0 25%;
            max-width: 25%;
        }

        .col-sm-4 {
            -ms-flex: 0 0 33.333333%;
            flex: 0 0 33.333333%;
            max-width: 33.333333%;
        }

        .col-sm-5 {
            -ms-flex: 0 0 41.666667%;
            flex: 0 0 41.666667%;
            max-width: 41.666667%;
        }

        .col-sm-6 {
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }

        .col-sm-7 {
            -ms-flex: 0 0 58.333333%;
            flex: 0 0 58.333333%;
            max-width: 58.333333%;
        }

        .col-sm-8 {
            -ms-flex: 0 0 66.666667%;
            flex: 0 0 66.666667%;
            max-width: 66.666667%;
        }

        .col-sm-9 {
            -ms-flex: 0 0 75%;
            flex: 0 0 75%;
            max-width: 75%;
        }

        .col-sm-10 {
            -ms-flex: 0 0 83.333333%;
            flex: 0 0 83.333333%;
            max-width: 83.333333%;
        }

        .col-sm-11 {
            -ms-flex: 0 0 91.666667%;
            flex: 0 0 91.666667%;
            max-width: 91.666667%;
        }

        .col-sm-12 {
            -ms-flex: 0 0 100%;
            flex: 0 0 100%;
            max-width: 100%;
        }

        .container {
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }

        .row {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            /* margin-right: -15px;
            margin-left: -15px; */
            margin: 0 -15px 5px;
        }

        .borderless td, .borderless th {
            border: none;
        }

    </style>
</head>
<body>
@php
    $i =0;
@endphp
@if(! empty($orders))
    @foreach($orders as $order)

        <div class="container"
             style="@if($i < (count($orders)-1)) page-break-after:always; @endif">
            <h3 class="text-center" style="font-weight: bold">Delivery Receipt</h3>
            <table style="width: 100%">
                <tbody>
                <tr>
                    <td style="width: 25%; text-align: center">
                        <img src="{{public_path().'/website/images/logo.png'}}" alt=""
                             style="max-width: 70px; height: auto">
                    </td>
                    <td style="@if(empty($order->reference_number)) width: 53%; @else width: 43%; @endif text-align: center"><h6
                            style=" font-weight: bold">{{isset($order->warehouse->name) ? $order->warehouse->name : '-'}}</h6>
                    </td>
                    <td style="@if(empty($order->reference_number)) width: 22%; @else width: 32%;  @endif text-align: center">
                        @if(!empty($order->customer->Corporate) && $order->customer->Corporate->logo)
                            <img
                                src="{{$order->customer->Corporate->logo}}" style="max-width:100px; height: auto; max-height: 70px">
                        @elseif(!empty($order->customer->Corporate->manager->first()) && $order->customer->Corporate->manager->first()->image)
                            <img
                                src="{{$order->customer->Corporate->manager->first()->image}}" style="max-width:100px; height: auto; max-height: 70px">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td style="width: 25%; text-align: center">
                        <h6 style="">
                            Code: {{ ! empty($order) ? $order->order_number : ''}}
                        </h6>
                    </td>
                    <td style="@if(empty($order->reference_number)) width: 53%; @else width: 43%; @endif text-align: center">
                        <h6 style="">
                            Date: {{ ! empty($order) ? date('d M Y', strtotime($order->created_at)) : ''}}</h6>
                    </td>
                    <td style="@if(empty($order->reference_number)) width: 22%; @else width: 32%; @endif text-align: center">
                        @if(!empty($order->reference_number))
                            <h6 style="">
                                Reference
                                Code:
                                {{ $order->reference_number}}</h6>
                        @endif
                    </td>
                </tr>
                </tbody>
            </table>

            <table class="table table-bordered">
                <thead>
                <tr>
                    <td style="border-right: none !important;font-weight: bold">Receiver Name</td>
                    <td style="border-left: none !important;">{{ ! empty($order) ? $order->receiver_name : '-'}}</td>
                    <td style="border-right: none !important;font-weight: bold">Customer Name</td>
                    <td style="border-left: none !important;">{{ ! empty($order->corporteName) ? $order->corporteName : '-'}}</td>

                </tr>

                <tr>
                    <td style="border-right: none !important;font-weight: bold">Receiver Phone</td>
                    <td style="border-left: none !important;">
                        <span>{{ ! empty($order) ? $order->receiver_mobile : '-'}}</span>
                        @if($order->receiver_phone)
                            <br>
                            <span>{{ ! empty($order) ? $order->receiver_phone : '-'}}</span>
                        @endif
                    </td>
                    <td style="border-right: none !important;font-weight: bold">Customer Phone</td>
                    <td style="border-left: none !important;">{{ ! empty($order) ? $order->sender_mobile : '-'}}</td>

                </tr>
                <tr>
                    <td style="border-right: none !important;font-weight: bold">From</td>
                    <td style="border-left: none !important; direction: rtl !important;">
                        @if(! empty($order->to_government)  && $order->to_government != null )
                            <span>{{$order->to_government->name_ar}}</span>
                        @endif
                        <img src="{{public_path().'/website/images/right-arrow.svg'}}" style="width: 15px; vertical-align: bottom">
                        @if(! empty($order->from_government)  && $order->from_government != null )
                            <span>{{$order->from_government->name_ar}}</span>
                        @endif
                    </td>
                    <td style="border-right: none !important;font-weight: bold">Sub Phone</td>
                    <td style="border-left: none !important;">{{ ! empty($order->sender_subphone) ? $order->sender_subphone : '-'}}</td>

                </tr>
                <tr>
                    <td style="border-right: none !important;font-weight: bold">Receiver Address</td>
                    <td colspan="3" style="border-left: none !important;">
                        {{--                                {{ !empty($order) ? mb_strimwidth($order->receiver_address, 0, 51) : ''}}--}}
                        {{$order->receiver_address}}
                        {{ !empty($order) && ! empty($order->to_city) ? ' - '. $order->to_city->city_name : ''}}
                        {{ !empty($order) && ! empty($order->to_government) ?  ' - '.$order->to_government->name_ar : ''}}

                    </td>
                </tr>
                <tr>
                    <td style="border-right: none !important;font-weight: bold">Type Name</td>
                    <td colspan="3"
                        style="border-left: none !important;">{{ ! empty($order->type)  ? mb_substr($order->type, 0, 180) : '-'}}</td>
                </tr>

                <tr>
                    <td style="border-right: none !important;font-weight: bold">Notes</td>
                    <td colspan="3"
                        style="border-left: none !important;">{{ ! empty($order) ? $order->notes : '-'}}</td>
                </tr>

                <tr>
                    <td style="border-right: none !important;font-weight: bold">Total Price</td>
                    <td style="border-left: none !important; border-right: none !important;">{{ ! empty($order) ? $order->total_price_policy: ''}}</td>
                    <td colspan="2" style="border-left: none !important; text-align: center">
                        <img src="data:image/png;base64,{!! DNS1D::getBarcodePNG($order->order_number, 'I25') !!}"
                             alt="barcode"/>
                        <br>
                        <span style="font-size: 12px; font-weight: bold">{{ ! empty($order->delivery_type)  ? mb_substr($order->delivery_type, 0, 150) : '-'}}</span>
                    </td>
                </tr>
                </thead>
            </table>

            <table style="width: 100%; margin-top: 1px !important;">
                <tr>
                    <td style="text-align: left">
                        <span style="font-size: 12px; font-weight: bold">www.ufelix.com</span>
                    </td>
                    <td style="text-align: right">
                        <span style="font-size: 12px; font-weight: bold">فى حالة رفض الاستلام بعد فتح الشحنة يقوم المستلم بدفع مصاريف الشحن</span>
                    </td>

                </tr>
            </table>

        </div>
        @php
            $i ++;
        @endphp
    @endforeach
@endif

</body>
</html>

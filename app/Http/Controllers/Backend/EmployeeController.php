<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use App\Http\Requests;
use App\Models\Employee;
use Illuminate\Http\Request;
use Flash;
use File;

class EmployeeController extends Controller
{

    // Get All Employee
    public function index()
    {
        $employees = Employee::orderBy('id', 'desc')->paginate(50);
        return view('backend.employees.index', compact('employees'));
    }

    // Create Employee Page
    public function create()
    {
        return view('backend.employees.create');
    }

    // Added New Employee
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'        => 'required|string|max:255',
            'email'       => 'required|email|max:255|unique:employees,email',
            'mobile'      => 'required|string|max:255|unique:employees,mobile',
            'salary'      => 'required',
            'job_title'   => 'required',
            'image'       => 'mimes:jpeg,jpg,png,gif'
        ]);

        $employee               = new Employee();
        $employee->name         = $request->input("name");
        $employee->email        = $request->input("email");
        $employee->mobile       = $request->input("mobile");
        $employee->salary       = $request->input("salary");
        $employee->job_title    = $request->input("job_title");
        $employee->bio          = $request->input("bio");
        if ($request->file("image") != "") {
            $extension          = File::extension($request->file("image")->getClientOriginalName());
            $image              = time() . "." . $extension;
            $employee->image    = $image;
            $request->file('image')->move(public_path('backend/images/employees/'), $image);
        }
        $employee->save();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Add New Employee ' . $employee->name , ' اضافة موظف جديد ' . $employee->name, 15); // Helper Function in App\Helpers.php

        Flash::success(__('backend.Employee_saved_successfully'));
        return redirect()->route('mngrAdmin.employees.index');
    }

    // View One Employee
    public function show($id)
    {
        $employee = Employee::findOrFail($id);
        return view('backend.employees.show', compact('employee'));
    }

    // Edit Employee Page
    public function edit($id)
    {
        $employee = Employee::findOrFail($id);
        return view('backend.employees.edit', compact('employee'));
    }

    // Update Employee
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'        => 'required|string|max:255',
            'email'       => 'required|email|max:255|unique:employees,email,' . $id,
            'mobile'      => 'required|string|max:255|unique:employees,mobile,' . $id,
            'salary'      => 'required',
            'job_title'   => 'required',
            'image'       => 'mimes:jpeg,jpg,png,gif',
        ]);

        $employee               = Employee::findOrFail($id);
        $employee->name         = $request->input("name");
        $employee->email        = $request->input("email");
        $employee->mobile       = $request->input("mobile");
        $employee->salary       = $request->input("salary");
        $employee->job_title    = $request->input("job_title");
        $employee->bio          = $request->input("bio");
        if ($request->file("image") != "") {
            $extension          = File::extension($request->file("image")->getClientOriginalName());
            $image              = time() . "." . $extension;
            $employee->image    = $image;
            $request->file('image')->move(public_path('backend/images/employees/'), $image);
        }
        $employee->save();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Edit Employee ' . $employee->name , ' تعديل الموظف ' . $employee->name, 15); // Helper Function in App\Helpers.php

        Flash::warning(__('backend.Employee_updated_successfully'));
        return redirect()->route('mngrAdmin.employees.index');
    }

    // Delete Employee
    public function destroy($id)
    {
        $employee = Employee::findOrFail($id);
        $employee->delete();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Delete Employee ' . $employee->name , ' حذف الموظف ' . $employee->name, 15); // Helper Function in App\Helpers.php

        flash(__('backend.Employee_deleted_successfully'))->error();
        return redirect()->route('mngrAdmin.employees.index');
    }

}

<?php

namespace App\Http\Controllers\Captain;

use App\Models\Driver;
use App\Models\Order;
use App\Models\Role;
use App\Models\Role_captain;
use App\Models\User;
use Auth;
use Config;
use File;
use Flash;
use Illuminate\Http\Request;
use Validator;

class UserController extends Controller
{
    private $guardType;

    public function __construct(Request $request)
    {
        Config::set('auth.defaults.guard', 'captain');
        $this->guardType = Config::get('auth.defaults.guard');
    }

    //Users By Roles
    public function get_users_type($key)
    {
        $role = Role::where('name', $key)->first();
        if (count($role) > 0) {
            $users = User::whereHas('roles', function ($q) use ($key) {
                $q->where('name', $key);
            })->paginate(50);
            $title = $role->display_name;
            return view('captain.users.index', compact('users', 'title', 'key'));
        } else {
            return redirect('/captainDashboard/404');
        }
    }

    // Get All Admins
    public function index()
    {
        $users = User::whereHas('roles', function ($q) {
            $q->where('name', 'user');
        })->paginate(50);
        return view('captain.users.index', compact('users'));
    }

    // Search In Admins
    public function search_users($key)
    {
        $users = User::where('mobile', 'LIKE', $key . "%")->
        orWhere('name', 'LIKE', $key . "%")->
        orWhere('email', 'LIKE', $key . "%")->
        orWhere('id', 'LIKE', $key . "%")->
        get();
        return $users;
    }

    // Create Admin Page
    public function create()
    {
        $role_captains = Role_captain::get();
        return view('captain.users.create', compact('role_captains'));
    }

    // Added New Admin
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'string|max:255',
            'email' => 'max:255|unique:users',
            'mobile' => 'required|string|max:255|unique:users',
            'password' => 'required|min:4|confirmed',
            'role_captain_id' => 'required',
        ]);

        $role = Role::where('name', 'captain')->first();
        $user = new User();
        $user->name = $request->input("name");
        $user->email = $request->input("email");
        $user->mobile = $request->input("mobile");
        $user->password = bcrypt($request->input("password"));
        $user->role_id = $role->id;
        $user->role_captain_id = $request->input("role_captain_id");
        $user->is_active = 1;
        $user->save();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Add New Admin ' . $user->name, ' اضافة مشرف جديد ' . $user->name, 31); // Helper Function in App\Helpers.php

        Flash::success(__('captain.User_added_successfully'));
        return redirect('/captainDashboard/captains');
    }

    // Block Admin
    public function block_user(Request $request)
    {
        $user = User::findOrFail($request->user_id);
        if ($user->is_active == 1) {
            $user->is_active = 0;
            $msg = __('captain.user_is_unblocked_successfully');
        } else {
            $user->is_active = 1;
            $msg = __('captain.user_is_blocked_successfully');
        }

        return response()->json([
            'data' => [
                'success' => $user->save(),
                'message' => $msg,
            ]
        ]);
    }

    // View One Admin
    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('captain.users.show', compact('user'));
    }

    // Edit Admin Page
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $role_captains = Role_captain::get();
        return view('captain.users.edit', compact('user', 'role_captains'));
    }

    // Update Admin
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'string|max:255',
            'email' => 'email|max:255|unique:users,email,' . $id,
            'mobile' => 'required|string|max:255|unique:users,mobile,' . $id,
            'role_captain_id' => 'required',
        ]);

        $user = User::findOrFail($id);
        $user->name = $request->input("name");
        $user->email = $request->input("email");
        $user->mobile = $request->input("mobile");
        $user->role_captain_id = $request->input("role_captain_id");
        if ($request->input('password') != "") {
            $user->password = bcrypt($request->input("password"));
        }
        $user->save();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Edit Admin ' . $user->name, ' تعديل المشرف ' . $user->name, 31); // Helper Function in App\Helpers.php

        Flash::warning(__('captain.User_is_updated_successfully'));

        if ($user->id == Auth::guard($this->guardType)->user()->id && $request->input('password') != "") {
            Auth::guard($this->guardType)->login($user);
        }

        return redirect('/captainDashboard/captains');
    }

    // Delete Admin
    public function destroy($id)
    {
        $user = User::where('id', $id)->first();

        if (!empty($user)) {
            $name = $user->name;
            User::where('id', $id)->delete();

            // Function Add Action In Activity Log By Samir
            addActivityLog('Delete Admin ' . $name, ' حذف المشرف ' . $name, 31); // Helper Function in App\Helpers.php

            flash(__('captain.User_deleted_successfully'))->error();
            return redirect('captainDashboard/captains');

        } else {

        }

    }


    public function logout()
    {
        if (Auth::guard($this->guardType)->user()) {
            Auth::guard($this->guardType)->logout();
            return redirect('/captainDashboard');
        } else {
            return redirect('/captainDashboard');
        }
    }

    function block($id)
    {
        $user = User::findOrFail($id);
        if ($user->is_active == 0) {
            $user->is_active = 1;

            // Function Add Action In Activity Log By Samir
            addActivityLog('Unblocked Admin ' . $user->name, ' الغاء حظر المشرف ' . $user->name, 31); // Helper Function in App\Helpers.php

        } else {
            $user->is_active = 0;

            // Function Add Action In Activity Log By Samir
            addActivityLog('Blocked Admin ' . $user->name, ' حظر المشرف ' . $user->name, 31); // Helper Function in App\Helpers.php
        }

        $user->save();
        return $user->is_active;
    }

    public function authenticate(Request $request)
    {
        //Fr3on
        $v = Validator::make($request->all(), [
            'mobile' => 'required',
            'password' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        if (Auth::guard('captain')->attempt(['mobile' => $request->mobile, 'password' => $request->password], true)) {
            if (Auth::guard('captain')->user()->is_active == 0) {

                Auth::guard('captain')->logout();
                return redirect('/captainDashboard/login')->withErrors([__('captain.You_are_blocke')])->withInput($request->except('password'));
            }
            Config::set('auth.defines.guard', 'captain');
            return redirect('/captainDashboard/dashboard');
        } else {
            Config::set('auth.defines.guard', 'captain');
            return redirect('/captainDashboard/login')->withErrors([__('captain.Email_or_Password_is_wrong')])->withInput($request->except('password'));
        }
    }

    public function authenticate_agent(Request $request)
    {
        //Fr3on
        $v = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        if (Auth::guard('agent')->attempt(['email' => $request->email, 'password' => $request->password, 'role_id' => 2], true)) {
            if (Auth::guard('agent')->user()->is_active == 0) {
                Auth::guard('agent')->logout();
                return redirect('/mngrAgent/login')->withInput(\Request::except("password"))->withErrors(['error' => __('captain.You_are_blocke')]);

            } else if (!empty(Auth::guard('agent')->user()->agent) && Auth::guard('agent')->user()->agent->status == 0) {
                Auth::guard('agent')->logout();
                return redirect('/mngrAgent/login')->withInput(\Request::except("password"))->withErrors(['error' => __('captain.You_are_Not_Verify_Please_Contact_with_captain')]);
            }
            return redirect('/mngrAgent/dashboard');
        } else {

            Config::set('auth.defines.guard', 'agent');
            return redirect('/mngrAgent/login')->withErrors(['error' => __('captain.Email_or_Password_is_wrong')]);
        }
    }

    /* **-**-*-*-*-*-* Dashboard Admin -*-*-*-*-*-*-*-edit by samar *--*-*-*-*-*-*-- */
    public function dashboard()
    {
        $user = Auth::guard($this->guardType)->user();
        $ownDrivers = Driver::where(function($query) use($user){
            $query->where('id', $user->id)
                ->orWhere('manager_id', $user->id);
        })->pluck('id')->toArray();

        $neworders = Order::join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
            ->whereIn('accepted_orders.driver_id', $ownDrivers)
            ->whereIn('orders.status', [1, 2])
            ->whereIn('accepted_orders.status', [0, 2])
            ->orderBy('orders.created_at', 'DESC')
            ->select('orders.*')
            ->groupBy('orders.id')
            ->take(5)
            ->get();
        $orders = Order::join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
            ->whereIn('accepted_orders.driver_id', $ownDrivers)
            ->where(function ($query) {
                $query->where(function ($query) {
                    $query->where('orders.status', 1)
                        ->where('accepted_orders.status', 0);
                })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 2)
                            ->where('accepted_orders.status', 2);
                    })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 3)
                            ->where('accepted_orders.status', 3);
                    })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 5)
                            ->where('accepted_orders.status', 5)
                            ->where('orders.warehouse_dropoff', 1);
                    })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 8)
                            ->where('accepted_orders.status', 8)
                            ->where('orders.warehouse_dropoff', 1);
                    })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 4)
                            ->where('accepted_orders.status', 4)
                            ->where('orders.warehouse_dropoff', 1)
                            ->where('orders.status_before_cancel', '!=', 0);
                    })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 5)
                            ->where('accepted_orders.status', 5)
                            ->where('orders.warehouse_dropoff', 0);
                    })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 8)
                            ->where('accepted_orders.status', 8)
                            ->where('orders.warehouse_dropoff', 0);
                    })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 4)
                            ->where('accepted_orders.status', 4)
                            ->where('orders.warehouse_dropoff', 0)
                            ->where('orders.status_before_cancel', '!=', 0);
                    });
            })
            ->distinct('order_id')
            ->count('order_id');

//        $orderComplete = Order::join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
//            ->where('accepted_orders.driver_id', $user->id)
//            ->where('orders.status', 3)
//            ->where('accepted_orders.status', 3)
//            ->distinct('order_id')
//            ->count('order_id');
//        $orderRunning = Order::join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
//            ->where('accepted_orders.driver_id', $user->id)
//            ->where('orders.status', 2)
//            ->where('accepted_orders.status', 2)
//            ->distinct('order_id')
//            ->count('order_id');
//        $orderAccepted = Order::join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
//            ->where('accepted_orders.driver_id', $user->id)
//            ->where('orders.status', 1)
//            ->where('accepted_orders.status', 0)
//            ->distinct('order_id')
//            ->count('order_id');
//        $orderCancel = Order::join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
//            ->where('accepted_orders.driver_id', $user->id)
//            ->where('orders.status', 4)
//            ->where('accepted_orders.status', 4)
//            ->distinct('order_id')
//            ->count('order_id');
//        $orderRecall = Order::join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
//            ->where('accepted_orders.driver_id', $user->id)
//            ->where('orders.status', 5)
//            ->where('accepted_orders.status', 5)
//            ->distinct('order_id')
//            ->count('order_id');
//        $orderReject = Order::join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
//            ->where('accepted_orders.driver_id', $user->id)
//            ->where('orders.status', 8)
//            ->where('accepted_orders.status', 8)
//            ->distinct('order_id')
//            ->count('order_id');
        $orderCurrent = Order::join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
            ->whereIn('accepted_orders.driver_id', $ownDrivers)
            ->where(function ($query) {
                $query->where(function ($query) {
                    $query->where('orders.status', 1)
                        ->where('accepted_orders.status', 0);
                })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 2)
                            ->where('accepted_orders.status', 2);
                    })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 5)
                            ->where('accepted_orders.status', 5)
                            ->where('orders.warehouse_dropoff', 0);
                    })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 8)
                            ->where('accepted_orders.status', 8)
                            ->where('orders.warehouse_dropoff', 0);
                    })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 3)
                            ->where('accepted_orders.status', 3)
                            ->where('delivery_status', 2)
                            ->where('orders.warehouse_dropoff', 1);
                    })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 3)
                            ->where('accepted_orders.status', 3)
                            ->where('delivery_status', 4)
                            ->where('orders.warehouse_dropoff', 1);
                    })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 3)
                            ->where('accepted_orders.status', 3)
                            ->where('delivery_status', 2)
                            ->where('orders.warehouse_dropoff', 0);
                    })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 3)
                            ->where('accepted_orders.status', 3)
                            ->where('delivery_status', 4)
                            ->where('orders.warehouse_dropoff', 0);
                    });
            })
            ->distinct('order_id')
            ->count('order_id');
        $orderFinish = Order::join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
            ->whereIn('accepted_orders.driver_id', $ownDrivers)
            ->where(function ($query) {
                $query->where(function ($query) {
                    $query->where('orders.status', 3)
                        ->where('accepted_orders.status', 3);
                })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 5)
                            ->where('accepted_orders.status', 5)
                            ->where('orders.warehouse_dropoff', 1);
                    })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 8)
                            ->where('accepted_orders.status', 8)
                            ->where('orders.warehouse_dropoff', 1);
                    })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 3)
                            ->where('accepted_orders.status', 3)
                            ->where('delivery_status', 2)
                            ->where('orders.warehouse_dropoff', 1);
                    })
                    ->orWhere(function ($query) {
                        $query->where('orders.status', 3)
                            ->where('accepted_orders.status', 3)
                            ->where('delivery_status', 4)
                            ->where('orders.warehouse_dropoff', 1);
                    });
            })
            ->distinct('order_id')
            ->count('order_id');

        return view('captain.dashboard', compact('orderFinish', 'orderCurrent', 'orders', 'neworders'));
    }

    public function search_user($key, $type)
    {
        $users = User::whereHas('roles', function ($q) use ($type) {
            $q->where('name', $type);
        })->where(function ($query) use ($key) {
            $query->where('mobile', 'LIKE', $key . "%");
            $query->orWhere('name', 'LIKE', $key . "%");
            $query->orWhere('email', 'LIKE', $key . "%");
            $query->orWhere('id', 'LIKE', $key . "%");
        })->get();
        return $users;
    }
}

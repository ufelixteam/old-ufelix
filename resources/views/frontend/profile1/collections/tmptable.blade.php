<table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0"
       width="100%">
    <thead>
    <tr>

        <th>Receiver Name</th>
        <th>Receiver Mobile</th>
        <th>Receiver City</th>
        @if(!empty($collection) && $collection->complete_later != 1)
            <th>action</th>
        @endif
    </tr>
    </thead>
    <tbody>

    @if(! empty($orders) && count($orders) >0  )
        @foreach($orders as $order)
            <tr>

                <td>{{$order->receiver_name}}</td>
                <td>{{$order->receiver_mobile}}</td>

                <td>{{ ! empty($order->receiver_city )? $order->receiver_city->name_en : ''  }}</td>

                @if(!empty($collection) && $collection->complete_later != 1)
                    <td><a class="btn btn-warning btn-rounded view-order btn-block"
                           data-value="{{$order->id}}">View/Edit</a></td>
                @endif
            </tr>
        @endforeach
    @endif

    </tbody>
</table>
{!! $orders->appends($_GET)->links() !!}

@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.order_cancel_requests')}}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.order_cancel_requests')}}
        </h3>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            @if(! empty($order_cancel_requests) && count($order_cancel_requests) > 0)
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <div class="table-responsive">
                    <table id="theTable" class="table table-condensed table-striped text-center"
                           style="min-height: 200px;">
                        <thead>
                        <tr>
                            <th>{{__('backend.Order_No')}}</th>
                            <th>{{__('backend.customer')}}</th>
                            <th>{{__('backend.corporate')}}</th>
                            <th>{{__('backend.Create_at')}}</th>
                            <th>{{__('backend.reason')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($order_cancel_requests as $cancel_request)
                            <tr>
                                <td>{{$cancel_request->order_id}}</td>
                                <td>{{$cancel_request->customer->name}}</td>
                                <td>{{$cancel_request->customer->corporate->name}}</td>
                                <td>{{$cancel_request->created_at}}</td>
                                <td>{{$cancel_request->reason}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {!! $order_cancel_requests->appends($_GET)->links() !!}
            @else
                <h3 class="text-center alert alert-warning">No Result Found !</h3>
            @endif
        </div>
    </div>
@endsection

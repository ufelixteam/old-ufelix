<div class="modal fade" id="forward_agentMo" tabindex="-1" role="dialog" aria-labelledby="forward_agentMo" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="forward_agentMo">{{__('backend.Forward_Order_To_Another_Agent')}}</h5>
        </div>
        <div class="modal-body">
            <form class="" action="{{ url('mngrAdmin/forward_order') }}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="select[]" value="{{ $order->id }}">

                <div class="form-group">
                    <select id="agent-field" name="agent" id="agent" class="form-control">
                        <option >{{__('backend.Choose_Agent')}}</option>
                            @if(! empty($agents))

                        @foreach($agents as $agent)

                            <option value="{{$agent->id}}"
                                    {{$agent->id == $order->agent_id ? 'selected' : ''}}>{{$agent->name}}</option>
                        @endforeach
                        @endif
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">{{__('backend.forward')}}</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">{{__('backend.close')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
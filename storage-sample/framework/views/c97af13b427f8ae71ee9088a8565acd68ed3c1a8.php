
<?php $__env->startSection('css'); ?>
<title><?php echo e(__('backend.pickup_price_list')); ?> - <?php echo e(__('backend.create')); ?></title>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('header'); ?>
  <div class="page-header">
    <h3><i class="glyphicon glyphicon-plus"></i> <?php echo e(__('backend.pickup_price_list')); ?> / <?php echo e(__('backend.create')); ?> </h3>
  </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <form action="<?php echo e(route('mngrAdmin.pickup_price_lists.store')); ?>" method="POST" name='validateForm'>
        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
        <div class="form-group col-md-6 col-sm-6 <?php if($errors->has('start_station')): ?> has-error <?php endif; ?>">
          <label for="start_station-field"><?php echo e(__('backend.from')); ?>: </label>
          <select id="start_station-field" name="start_station" class="form-control" >
          <?php if(! empty($list_governorates) ): ?>
            <option disabled selected><?php echo e(__('backend.Choose_Start')); ?>: </option>
            <?php $__currentLoopData = $list_governorates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $governorate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value="<?php echo e($governorate->id); ?>"><?php echo e($governorate->name_en); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          <?php endif; ?>
          </select>
            <?php if($errors->has("start_station")): ?>
              <span class="help-block"><?php echo e($errors->first("start_station")); ?></span>
            <?php endif; ?>
        </div>
        <div class="form-group col-md-6 col-sm-6 <?php if($errors->has('access_station')): ?> has-error <?php endif; ?>">
          <label for="access_station-field"><?php echo e(__('backend.to')); ?>: </label>
          <select id="access_station-field" name="access_station" class="form-control" >
          <?php if(! empty($list_governorates) ): ?>
          <option disabled selected><?php echo e(__('backend.Choose_End')); ?>: </option>
            <?php $__currentLoopData = $list_governorates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $governorate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value="<?php echo e($governorate->id); ?>"><?php echo e($governorate->name_en); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          <?php endif; ?>
          </select>
            <?php if($errors->has("access_station")): ?>
              <span class="help-block"><?php echo e($errors->first("access_station")); ?></span>
            <?php endif; ?>
        </div>
        <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('cost')): ?> has-error <?php endif; ?>">
          <label for="cost-field"><?php echo e(__('backend.cost')); ?>: </label>
          <input type="text" id="cost-field" name="cost" class="form-control" value="<?php echo e(old("cost")); ?>"/>
          <?php if($errors->has("cost")): ?>
            <span class="help-block"><?php echo e($errors->first("cost")); ?></span>
          <?php endif; ?>
        </div>

         <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('bonous')): ?> has-error <?php endif; ?>">
          <label for="bonous-field"><?php echo e(__('backend.the_bonous')); ?>: </label>
          <input type="text" id="bonous-field" name="bonous" class="form-control" value="<?php echo e(old("bonous")); ?>"/>
          <?php if($errors->has("bonous")): ?>
            <span class="help-block"><?php echo e($errors->first("bonous")); ?></span>
          <?php endif; ?>
        </div>
        <div class="form-group col-md-6 col-sm-6 <?php if($errors->has('status')): ?> has-error <?php endif; ?>">
          <label for="status-field" style="display: block;"><?php echo e(__('backend.status')); ?>: </label>
          <select id="status-field" name="status" class="form-control" value="<?php echo e(old("status")); ?>" >
            <option value="0" <?php echo e(old("status") == "0" ? 'selected' : ''); ?>><?php echo e(__('backend.no')); ?></option>
            <option value="1" <?php echo e(old("status") == "1" ? 'selected' : ''); ?>><?php echo e(__('backend.yes')); ?></option>
          </select>
          <?php if($errors->has("status")): ?>
            <span class="help-block"><?php echo e($errors->first("status")); ?></span>
          <?php endif; ?>
        </div>
        <div class="well well-sm col-md-12 col-sm-12 col-xs-12">
          <button type="submit" class="btn btn-primary"><?php echo e(__('backend.add_destination')); ?></button>
          <a class="btn btn-link pull-right" href="<?php echo e(route('mngrAdmin.pickup_price_lists.index')); ?>"><i class="glyphicon glyphicon-backward"></i> <?php echo e(__('backend.back')); ?></a>
        </div>
      </form>
    </div>
  </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
  $(function() {
    $("form[name='validateForm']").validate({
      // Specify validation rules
      rules: {
        start_station: {
          required: true,
        },
        access_station: {
          required: true,
        },
        cost: {
          required: true,
          number: true
        },
      },
      // Specify validation error messages
      messages: {
        start_station: {
          required: "<?php echo e(__('backend.Please_Choose_Start_Destination')); ?>",
        },
        access_station: {
          required: "<?php echo e(__('backend.Please_Choose_End_Destination')); ?>",
        },
        cost: {
          required: "<?php echo e(__('backend.Please_Enter_The_Destination_Cost')); ?>",
          number: "<?php echo e(__('backend.Please_Enter_Avalid_Number')); ?>",
        },
      },

      errorPlacement: function(error, element) {
          $(element).parents('.form-group').append(error)
      },
      highlight: function (element) {
          $(element).parent().addClass('error')
      },
      unhighlight: function (element) {
          $(element).parent().removeClass('error')
      },
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function(form) {
          form.submit();
      }
    });
  })
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/pickup_price_lists/create.blade.php ENDPATH**/ ?>
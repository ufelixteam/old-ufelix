<?php $__env->startSection('css'); ?>
  <title><?php echo app('translator')->getFromJson('backend.dashboard'); ?></title>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="panel panel-headline">
        <div class="panel-heading">
            <h3 class="panel-title"> Overview</h3>
            <p class="panel-subtitle"></p>
        </div>


        <div class="panel-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="metric">
                        <span class="icon"><i class="fa fa-users"></i></span>
                        <p>
                            <span class="number"><?php echo e(! empty($users) ? $users : '0'); ?></span>
                            <span class="title">Customers</span>
                        </p>
                    </div>
                </div>


                <div class="col-md-3">
                    <div class="metric">
                        <span class="icon"><i class="fa fa-handshake-o"></i></span>
                        <p>
                            <span class="number"><?php echo e(! empty($corporates) ? $corporates : '0'); ?></span>
                            <span class="title">Corporates</span>
                        </p>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="metric">
                        <span class="icon"><i class="lnr lnr-car"></i></span>
                        <p>
                            <span class="number"><?php echo e(! empty($drivers) ? $drivers : '0'); ?></span>
                            <span class="title">Captains</span>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="metric">
                        <span class="icon"><i class="fa fa-shopping-basket "></i></span>
                        <p>
                            <span class="number"><?php echo e(! empty($orders) ? $orders : '0'); ?></span>
                            <span class="title">Orders</span>
                        </p>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="metric">
                        <span class="icon"><i class="fa fa-envelope"></i></span>
                        <p>
                            <span class="number"><?php echo e(! empty($contacts) ? $contacts : '0'); ?></span>
                            <span class="title">Feedback</span>
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-7">
            <!-- RECENT PURCHASES -->
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Recent Individual Customers</h3>
                    <div class="right">
                        <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                        <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                    </div>
                </div>
                <div class="panel-body no-padding">
                    <table class="table table-striped">

                        <thead>
                        <tr>
                            <th width = "10%">Photo</th>
                            <th>Name</th>
                            <th>Mobile</th>
                            <th>Date </th>
                            <th>Mobile Verified</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(! empty($recentCustomers) && count($recentCustomers)>0): ?>

                            <?php $__currentLoopData = $recentCustomers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td>
                                        <?php if(! empty($user->image)): ?>
                                            <img src="<?php echo e($user->image); ?>"  width = "100%" >
                                        <?php else: ?>
                                            <img src="<?php echo e(URL::asset('/public/images/user.svg')); ?>" width = "50%">
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <a class="users-list-name" href="<?php echo e(URL::to('/mngrAdmin/customers')); ?>/<?php echo e($user->id); ?>"> <?php echo e($user->name); ?></a>
                                    </td>
                                    <td>
                                        <?php echo e($user->mobile); ?>

                                    </td>
                                    <td>
                                        <span class="users-list-date"><?php echo e(date('d-M', strtotime($user->created_at))); ?></span>
                                    </td>
                                    <td>
                                        <?php echo $user->mobile_verify_span; ?>

                                    </td>

                                </tr>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        <?php else: ?>
                            <tr>
                                <td colspan="4">
                                    <p style="padding: 8px 0 0 10px;">
                                        No Individual Customer registered
                                    </p>
                                </td>
                            </tr>
                        <?php endif; ?>

                        </tbody>
                    </table>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Last 24 hours</span></div>
                        <div class="col-md-6 text-right"><a href="<?php echo e(url('/mngrAdmin/customers')); ?>" class="btn btn-primary">View All Individual Customers</a></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-5">
            <!-- TASKS -->
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Overall Orders</h3>
                    <div class="right">
                        <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                        <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                    </div>
                </div>
                <div class="panel-body">
                    <ul class="list-unstyled task-list">
                        <?php if($orders > 0): ?>
                        <li>
                            <p>Cancelled Order <span class="label-percent"><?php echo e($orderCancel); ?></b>/<?php echo e($orders); ?></span></p>
                            <div class="progress progress-xs">
                                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?php echo e($orderCancel); ?>" aria-valuemin="0"
                                     aria-valuemax="<?php echo e($orders); ?>" style="width:<?php echo e(($orderCancel/$orders) *100); ?>%">
                                    <span class="sr-only"><?php echo e($orderCancel); ?></b>/<?php echo e($orders); ?> Complete</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <p>Completed Orders <span class="label-percent"><?php echo e($orderComplete); ?></b>/<?php echo e($orders); ?> </span></p>
                            <div class="progress progress-xs">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo e($orderComplete); ?>" aria-valuemin="0" aria-valuemax="<?php echo e($orders); ?>"
                                     style="width: <?php echo e(($orderComplete/$orders) *100); ?>%">
                                    <span class="sr-only"><?php echo e($orderComplete); ?></b>/<?php echo e($orders); ?> Complete</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <p>Pending Order <span class="label-percent"><?php echo e($orderPending); ?></b>/<?php echo e($orders); ?></span></p>
                            <div class="progress progress-xs">
                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="<?php echo e($orderPending); ?>" aria-valuemin="0"
                                     aria-valuemax="<?php echo e($orders); ?>" style="width: <?php echo e(($orderPending/$orders) *100); ?>%">
                                    <span class="sr-only"><?php echo e($orderPending); ?> </b>/<?php echo e($orders); ?> Complete</span>
                                </div>
                            </div>
                        </li>

                        <li>
                            <p>Accepted <span class="label-percent"><?php echo e($orderAccepted); ?></b>/<?php echo e($orders); ?></span></p>
                            <div class="progress progress-xs">
                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?php echo e($orderAccepted); ?>" aria-valuemin="0"
                                     aria-valuemax="<?php echo e($orders); ?>" style="width: <?php echo e(($orderAccepted/$orders) *100); ?>%">
                                    <span class="sr-only"><?php echo e($orderAccepted); ?></b>/<?php echo e($orders); ?>% Complete</span>
                                </div>
                            </div>
                        </li>

                        <li>
                            <p>Received <span class="label-percent"><?php echo e($orderRunning); ?></b>/<?php echo e($orders); ?></span></p>
                            <div class="progress progress-xs">
                                <div class="progress-bar progress-bar-default" role="progressbar" aria-valuenow="<?php echo e($orderRunning); ?>" aria-valuemin="0"
                                     aria-valuemax="<?php echo e($orders); ?>" style="width: <?php echo e(($orderRunning/$orders) *100); ?>%">
                                    <span class="sr-only"><?php echo e($orderRunning); ?></b>/<?php echo e($orders); ?>% Complete</span>
                                </div>
                            </div>
                        </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
            <!-- END TASKS -->
        </div>




        <div class="row">
            <div class="col-md-7">
                <!-- RECENT PURCHASES -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Recent Corporate</h3>
                        <div class="right">
                            <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                            <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                        </div>
                    </div>
                    <div class="panel-body no-padding">
                        <table class="table table-striped">

                            <thead>
                            <tr>
                                <th>Photo</th>
                                <th>Name</th>
                                <th>Date &amp; Time</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(! empty($recentCorporates) && count($recentCorporates)>0): ?>

                                <?php $__currentLoopData = $recentCorporates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td>
                                            <?php if(! empty($user->image)): ?>
                                                <img src="<?php echo e($user->image); ?>" width = "50%">
                                            <?php else: ?>
                                                <img src="<?php echo e(URL::asset('/public/images/user.svg')); ?>" width = "50%">
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <a class="users-list-name" href="<?php echo e(URL::to('/admin/sellers')); ?>/<?php echo e($user->id); ?>"> <?php echo e($user->name); ?></a>
                                        </td>
                                        <td>
                                            <span class="sellers-list-date"><?php echo e(date('d-M', strtotime($user->created_at))); ?></span>
                                        </td>
                                        <td>
                                            <?php echo $user->active_span; ?>

                                        </td>

                                    </tr>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            <?php else: ?>
                                <tr>
                                    <td colspan="4">
                                        <p style="padding: 8px 0 0 10px;">
                                            No Corporates registered
                                        </p>
                                    </td>
                                </tr>
                            <?php endif; ?>

                            </tbody>
                        </table>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Last 24 hours</span></div>
                            <div class="col-md-6 text-right"><a href="<?php echo e(url('/mngrAdmin/corporates')); ?>" class="btn btn-primary">View All Corporates</a></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-5">
                <!-- TASKS -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">New Orders</h3>
                        <div class="right">
                            <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                            <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Receiver</th>
                                    <th>Sender</th>
                                    <th>Created At</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if(! empty($neworders) && count($neworders)>0): ?>
                                    <?php $__currentLoopData = $neworders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                        <tr>
                                            <td><a href="<?php echo e(URL::to('/mngrAdmin/orders')); ?>/<?php echo e($order->id); ?>" data-toggle="tooltip" data-placement="bottom" title="Go to detail"><?php echo e($order->id); ?></a></td>
                                            <td><?php echo e($order->receiver_name); ?> </br> <?php echo e($order->receiver_mobile); ?> </td>
                                            <td><?php echo e($order->sender_name); ?> </br> <?php echo e($order->sender_mobile); ?> </td>

                                            <td> <?php echo e($order->created_at); ?> </td>
                                            <td>
                                                <?php echo $order->status_span; ?>

                                            </td>
                                        </tr>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                <?php else: ?>
                                    <tr>
                                        <td colspan="4" class="text-center">No Order Added</td>

                                    </tr>
                                <?php endif; ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Last 24 hours</span></div>
                            <div class="col-md-6 text-right"><a href="<?php echo e(url('/mngrAdmin/orders??type=pending')); ?>" class="btn btn-primary">View Pending Orders</a></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/dashboard.blade.php ENDPATH**/ ?>
<div class="row">
  <div class="col-md-12 col-xs-12 col-sm-12">
    @if(count($orders) > 0)
        <table id="theTable" class="table table-condensed table-striped text-center">
          <thead>
              <tr>
                <th>#</th>
                <th>{{__('backend.customer')}}</th>
                <th>{{__('backend.receiver_name')}}</th>
                <th>{{__('backend.order_type')}}</th>
                <th>{{__('backend.order_number')}}</th>
{{--                <th>{{__('backend.receiver_code')}}</th>--}}
                <th>{{__('backend.Delivery_Price')}}</th>
                <th>{{__('backend.created_at')}}</th>
                <th>{{__('backend.status')}}</th>
                <th class="pull-right"></th>
              </tr>
          </thead>
          <tbody>
            @foreach($orders as $index => $order)
              <tr>
                <td>{{$order->order_id}}</td>
                <td>{{$order->customer_name}}</td>
                <td>{{$order->receiver_name}}</td>
                <td>{{$order->type}}</td>
                <td>{{$order->order_number}}</td>
{{--                <td>{{$order->receiver_code}}</td>--}}
                <td>{{$order->delivery_price}}</td>
                <td>{{$order->created_at->format('d-m-Y')}}</td>
                {{-- {{date('30-M-Y')}} --}}
                <td>
                  @if($order->order_status == 1)
                      <span class='badge badge-pill label-info'>{{__('backend.accepted')}}</span>
                  @elseif($order->order_status == 2)
                      <span class='badge badge-pill label-warning'>{{__('backend.received')}}</span>
                  @elseif($order->order_status == 3)
                      <span class='badge badge-pill label-success'>{{__('backend.delivered')}}</span>
                  @elseif($order->order_status == 5)
                      <span class='badge badge-pill label-danger'>{{__('backend.recalled')}}</span>
                  @endif
                </td>
                <td class="pull-right">
                  <a class="btn btn-xs btn-primary" href="{{ route('mngrAdmin.orders.show', $order->order_id) }}"><i class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}} </a>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
        {!! $orders->appends($_GET)->links() !!}
    @else
      <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
    @endif
  </div>
</div>

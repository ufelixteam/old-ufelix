<?php $__env->startSection('css'); ?>
<title><?php echo e(__('backend.Order_Collection')); ?></title>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('header'); ?>
<div class="page-header clearfix">
  <h3>
    <i class="glyphicon glyphicon-align-justify"></i> <?php echo e(__('backend.Collection_Orders')); ?> <?php if(! empty($type) && $type ==1 ): ?> <?php echo e(__('backend.form')); ?> <?php else: ?> <?php echo e(__('backend.excel')); ?> <?php endif; ?>
    <?php if(! empty($type) && $type ==1 ): ?>
    <a class="btn btn-success pull-right" href="<?php echo e(route('mngrAdmin.orders.create')); ?>"><i class="glyphicon glyphicon-plus"></i> <?php echo e(__('backend.add_Collection')); ?></a>

    <?php else: ?>
    <a class="btn btn-success pull-right" href="<?php echo e(route('mngrAdmin.collection_orders.create')); ?>"><i class="glyphicon glyphicon-plus"></i> <?php echo e(__('backend.add_Collection')); ?></a>
    <a class="btn btn-success pull-right" style="margin-right: 30px;" href="https://ufelix.com:3000/readdata" target="_blank"> <?php echo e(__('backend.Fillter_Page')); ?></a>
    <a class="btn btn-success pull-right" style="margin-right: 30px;" href="https://ufelix.com:3000/" target="_blank"> <?php echo e(__('backend.Upload_After_Check')); ?></a>
    <?php endif; ?>
</h3>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<?php if(! empty($type) && $type ==1 ): ?>

<div class="row" style="margin-bottom:15px;">
  <div class="col-md-12">

    <div class="group-control col-md-3 col-sm-3">
      <select id="saved" name="saved" class="form-control">
        <option value="-1"> <?php echo e(__('backend.Sort_By_Saved')); ?></option>
        <option value="0"><?php echo e(__('backend.Not_Saved')); ?></option>
        <option value="1"><?php echo e(__('backend.saved')); ?></option>
      </select>
    </div>
  </div>
</div>
<?php endif; ?>
  <div class="list">
  <?php echo $__env->make('backend.collection_orders.table', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">

  $("#saved").on('change', function() {
  $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
    $.ajax({
        url: '<?php echo e(URL::asset("/mngrAdmin/collection_orders?type=1")); ?>'+'&&saved='+$("#saved").val(),
        type: 'get',
        success: function(data) {
            $('.list').html(data.view);
        },
        error: function(data) {
            console.log('Error:', data);
        }
    });
});


  $('#downloadStatus').on('click', function() {
    var _token = $('#_token');
    var id = $('#id');
    var status = $('#status');
    var file_name = $('#file_name');
    $.ajax({
      type:post,
      url:'mngrAdmin\\collection_orders\\downloadStatus',
      data: {
        _token: _token.val(),
        id: id.val(),
        status: status.val(),
        file_name: file_name.val(),
      },
      success: function(Response){
        $('#downloadStatus').removeClass('btn-primary').addClass('btn-warning');
      },
      error: function() {
        alert("<?php echo e(__('backend.Something_went_Wrong_please_try_again_later')); ?>");
      }
    });
  });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/collection_orders/index.blade.php ENDPATH**/ ?>
@extends('backend.layouts.app')
@section('header')
<div class="page-header">
        <h3>'{{__('backend.view')}}' - #{{$city->id}}</h3> {!! $city->status_span !!}
        <form action="{{ route('mngrAdmin.pickup_price_lists.destroy', $city->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
              @if(permission('editShipmentDestinations')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <a class="btn btn-warning btn-group" role="group" href="{{ route('mngrAdmin.pickup_price_lists.edit', $city->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
              @endif
              @if(permission('deleteShipmentDestinations')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <button type="submit" class="btn btn-danger"> {{__('backend.delete')}} <i class="glyphicon glyphicon-trash"></i></button>
              @endif
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <div class="form-group">
                <label for="nome">#</label>
                <p class="form-control-static"></p>
            </div>
            <div class="form-group col-sm-6">
                     <label for="city">CITY</label>
                     <p class="form-control-static">{{$city->city}}</p>
                </div>
                    <div class="form-group col-sm-6">
                     <label for="code">CODE</label>
                     <p class="form-control-static">{{$city->code}}</p>
                </div>
                    <div class="form-group col-sm-6">
                     <label for="country_id">COUNTRY</label>
                     <p class="form-control-static">{{ ! empty($city->country) ?  $city->country->name : ''}}</p>
                </div>


            <div class="col-md-12 col-xs-12 col-sm-12">
                <a class="btn btn-link" href="{{ route('mngrAdmin.pickup_price_lists.index') }}"><i class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
            </div>

        </div>
    </div>

@endsection

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/sms/{l}/{m}', 'FrontController@SendSMSCode');

Route::get('/test/{l}/{m}', 'FrontController@test');

Route::get('/landing', function () {
    return view('frontend.landing.index');
});

Route::get('/', 'FrontController@index')->name('home');
Route::get('/login', 'FrontController@get_login')->name('login');
Route::post('/login', 'FrontController@login')->name('post_login');
Route::get('/about', 'FrontController@about')->name('about');
Route::get('/services', 'FrontController@services')->name('service');
Route::get('/captain', 'FrontController@captain')->name('captain');
Route::get('/customer', 'FrontController@customer')->name('customer');
Route::get('/tracking', 'FrontController@tracking')->name('tracking');
Route::get('/register', 'FrontController@get_register')->name('register');
Route::post('/register', 'FrontController@register')->name('post_register');
Route::get('/contact', 'FrontController@contact')->name('contact');
Route::post('/contact', 'FrontController@contact_post')->name('contact_post');
Route::get('/terms', 'FrontController@terms')->name('terms');
Route::get('/forgotpassword', 'FrontController@get_forgotpassword')->name('forgotpassword');
Route::post('/forgotpassword', 'FrontController@forgotpassword')->name('forgotPassword');
Route::get('/active_account/{k}', 'FrontController@get_active_account')->name('active_account');
Route::post('/active_account', 'FrontController@active_account')->name('post_active_account');
Route::post('/forgotpasswordcode', 'FrontController@forgotpasswordcode')->name('forgotpasswordcode');
Route::post('/send_active_code', 'FrontController@send_active_code')->name('send_active_code');
Route::post('/resetpassword', 'FrontController@resetpassword')->name('resetpassword');
Route::post('/tracking', 'FrontController@search_tracking')->name('search_tracking');

Route::group(['prefix' => '/customer/account', 'as' => 'profile.', 'middleware' => 'customer'], function () {
    Route::get('/', 'FrontController@dashboard')->name('dashboard');
    Route::get('orders', 'FrontController@orders')->name('orders');
    Route::get('orders/{id}', 'FrontController@orderview')->name('orderview');
    Route::get('orders/{id}/edit', 'FrontController@order_edit_without_lang')->name('order_edit');
    Route::put('orders/{id}', 'FrontController@order_update')->name('order_update');
    Route::get('invoices/', 'FrontController@indexprofilewebsite');

    Route::get("reports/daily_report", "FrontController@profile_daily_report");
    Route::post("reports/daily_report", "Backend\ReportController@profile_daily_report");
    Route::post("orders/add_order_comment", "FrontController@addOrderComment");

    Route::get("invoices", "FrontController@indexInvoice")->name('invoices.index');
    Route::get("invoices/{id}", "FrontController@showInvoice")->name('invoices.show');
    Route::get("print/invoices/{id}", "FrontController@printInvoice");

    Route::get("refund_collections", "FrontController@indexRefund")->name('refund_collections.index');
    Route::any("print_refund", "Backend\ReportController@print_refund")->name("orders.print_refund");
    Route::any("refund_excel", "Backend\ReportController@refund_excel")->name("orders.refund_excel");

    Route::get('upload-excel', 'FrontController@getupdate');
    Route::post('upload-excel', 'FrontController@upload');
    Route::get('process-sheet/{id}', 'FrontController@process');
    Route::post("handle_sheet", "FrontController@handle_sheet");
    Route::post("export_sheet", "FrontController@export_sheet");
    Route::get('order-form', 'FrontController@order_form');
    Route::post('complete_collection/{id}', 'FrontController@complete_collection');
    Route::get('form-collections', 'FrontController@order_form_list')->name('collectionOrders');
    Route::any('form-collections/delete/{id}', 'FrontController@delete_collection')->name('collectionOrders.delete');
    Route::get('wallets', 'FrontController@profilewallet');
    Route::post('cancel_orders', 'FrontController@cancel_orders')->name('cancel_orders');
    Route::post('cancel_orders_request', 'FrontController@cancel_orders_request')->name('cancel_orders_request');
//    Route::get('wallets/', function () {
//        return view('frontend.profile.under_processing');
//    });
    Route::post('tmporders_create', 'FrontController@tmporders_create');
    #tmporders_update_view
    Route::get('tmporders_update_view/{key}', 'FrontController@tmporders_update_view');
    #tmporders_reset_view
    Route::get('tmporders_reset_view/{key}', 'FrontController@tmporders_reset_view');
    Route::get('/recall_if_dropped/{id}', 'FrontController@recall_if_dropped')->name('orders.recall_if_dropped');
    Route::get('logout', 'FrontController@logout');
    Route::get('changepassword', 'FrontController@get_changepassword');
    Route::post('changepassword', 'FrontController@changepassword');
    Route::get('notifications', 'FrontController@notifications');
    Route::get('notifications/delete', 'FrontController@delete_notification')->name('notifications.delete');

    Route::post('edit-profile', 'FrontController@edit_profile')->name('edit-profile');

    Route::get('get_cost/{key}/{receiver}', 'Backend\OrderController@get_cost_sender_receiver');

    Route::any("print_police", "Backend\ReportController@print_police");

    Route::get("customers/details/{id}", "CustomerController@details")->name('customers.details');
    Route::get("customers", "CustomerController@index")->name('customers.index');
    Route::get("customers/create", "CustomerController@create")->name('customers.create');
    Route::post("customers", "CustomerController@store")->name('customers.store');
    Route::get("customers/{id}", "CustomerController@show")->name('customers.show');
    Route::get("customers/{id}/edit", "CustomerController@customer_edit_without_lang")->name('customers.edit');
    Route::put("customers/{id}/update", "CustomerController@update")->name('customers.update');
    Route::delete("customers/{id}", "CustomerController@destroy")->name('customers.destroy');

    Route::get("tasks/cancel_pickup/{key}", "TaskOrderController@cancel_pickup");
    Route::get("tasks/details/{id}", "TaskOrderController@details")->name('tasks.details');
    Route::get("tasks", "TaskOrderController@index")->name('tasks.index');
    Route::get("tasks/create", "TaskOrderController@create")->name('tasks.create');
    Route::post("tasks", "TaskOrderController@store")->name('tasks.store');
    Route::get("tasks/{id}", "TaskOrderController@show")->name('tasks.show');
    Route::get("tasks/{id}/edit", "TaskOrderController@customer_edit_without_lang")->name('tasks.edit');
    Route::put("tasks/{id}/update", "TaskOrderController@update")->name('tasks.update');
    Route::delete("tasks/{id}", "TaskOrderController@destroy")->name('tasks.destroy');
});


Route::group(['prefix' => '{locale}', 'where' => ['locale' => '[a-zA-Z]{2}'], 'middleware' => 'setlocale'], function () {
    Route::get('/', 'FrontController@index')->name('home');
    Route::get('/login', 'FrontController@get_login')->name('login');
    Route::post('/login', 'FrontController@login')->name('post_login');
    Route::get('/about', 'FrontController@about')->name('about');
    Route::get('/services', 'FrontController@services')->name('service');
    Route::get('/captain', 'FrontController@captain')->name('captain');
    Route::get('/customer', 'FrontController@customer')->name('customer');
    Route::get('/tracking', 'FrontController@tracking')->name('tracking');
    Route::get('/register', 'FrontController@get_register')->name('register');
    Route::post('/register', 'FrontController@register')->name('post_register');
    Route::get('/contact', 'FrontController@contact')->name('contact');
    Route::post('/contact', 'FrontController@contact_post')->name('contact_post');
    Route::get('/terms', 'FrontController@terms')->name('terms');
    Route::get('/forgotpassword', 'FrontController@get_forgotpassword')->name('forgotpassword');
    Route::post('/forgotpassword', 'FrontController@forgotpassword')->name('forgotPassword');
    Route::get('/active_account/{k}', 'FrontController@get_active_account')->name('active_account');
    Route::post('/active_account', 'FrontController@active_account')->name('post_active_account');
    Route::post('/forgotpasswordcode', 'FrontController@forgotpasswordcode')->name('forgotpasswordcode');
    Route::post('/send_active_code', 'FrontController@send_active_code')->name('send_active_code');
    Route::post('/resetpassword', 'FrontController@resetpassword')->name('resetpassword');
    Route::post('/tracking', 'FrontController@search_tracking')->name('search_tracking');


    Route::group(['prefix' => '/customer/account', 'as' => 'profile.', 'middleware' => 'customer'], function () {
        Route::get("customers/details/{id}", "CustomerController@details")->name('customers.details');
        Route::get("customers", "CustomerController@index")->name('customers.index');
        Route::get("customers/create", "CustomerController@create")->name('customers.create');
        Route::post("customers", "CustomerController@store")->name('customers.store');
        Route::get("customers/{id}", "CustomerController@show")->name('customers.show');
        Route::get("customers/{id}/edit", "CustomerController@edit")->name('customers.edit');
        Route::put("customers/{id}/update", "CustomerController@update")->name('customers.update');
        Route::delete("customers/{id}", "CustomerController@destroy")->name('customers.destroy');
        Route::get("reports/daily_report", "FrontController@profile_daily_report");
        Route::get("tasks/cancel_pickup/{key}", "TaskOrderController@cancel_pickup");
        Route::get("tasks/details/{id}", "TaskOrderController@details")->name('customers.details');
        Route::get("tasks", "TaskOrderController@index")->name('tasks.index');
        Route::get("tasks/create", "TaskOrderController@create")->name('tasks.create');
        Route::post("tasks", "TaskOrderController@store")->name('tasks.store');
        Route::get("tasks/{id}", "TaskOrderController@show")->name('tasks.show');
        Route::get("tasks/{id}/edit", "TaskOrderController@edit")->name('tasks.edit');
        Route::put("tasks/{id}/update", "TaskOrderController@update")->name('tasks.update');
        Route::delete("tasks/{id}", "TaskOrderController@destroy")->name('tasks.destroy');

        Route::get('/', 'FrontController@dashboard')->name('dashboard');
        Route::get('orders', 'FrontController@orders')->name('orders');
        Route::get('orders/{id}', 'FrontController@orderview')->name('orderview');
        Route::get('orders/{id}/edit', 'FrontController@order_edit')->name('order_edit');
        Route::get("invoices", "FrontController@indexInvoice")->name('invoices.index');
        Route::get("invoices/{id}", "FrontController@showInvoice")->name('invoices.show');
        Route::get("print/invoices/{id}", "FrontController@printInvoice");

        Route::get("refund_collections", "FrontController@indexRefund")->name('refund_collections.index');
        Route::any("print_refund", "Backend\ReportController@print_refund")->name("orders.print_refund");
        Route::any("refund_excel", "Backend\ReportController@refund_excel")->name("orders.refund_excel");

        Route::get('upload-excel', 'FrontController@getupdate');
        Route::post('upload-excel', 'FrontController@upload');
        Route::get('process-sheet/{id}', 'FrontController@process');
        Route::post("handle_sheet", "FrontController@handle_sheet");
        Route::post("export_sheet", "FrontController@export_sheet");

        Route::get('order-form', 'FrontController@order_form');
        Route::post('complete_collection/{id}', 'FrontController@complete_collection');
        Route::get('form-collections', 'FrontController@order_form_list')->name('collectionOrders');
        Route::any('form-collections/delete/{id}', 'FrontController@delete_collection')->name('collectionOrders.delete');
        Route::get('wallets', 'FrontController@profilewallet');
//        Route::get('wallets/', function () {
//            return view('frontend.profile.under_processing');
//        });

        Route::any("print_police", "Backend\ReportController@print_police");

        Route::post('tmporders_create', 'FrontController@tmporders_create');

        #tmporders_update_view
        Route::get('tmporders_update_view/{key}', 'FrontController@tmporders_update_view');
        #tmporders_reset_view
        Route::get('tmporders_reset_view/{key}', 'FrontController@tmporders_reset_view');
        Route::get('logout', 'FrontController@logout');
        Route::get('changepassword', 'FrontController@get_changepassword');
        Route::post('changepassword', 'FrontController@changepassword');

        Route::get('notifications', 'FrontController@notifications');
        Route::get('notifications/delete', 'FrontController@delete_notification')->name('notifications.delete');;

        Route::post('edit-profile', 'FrontController@edit_profile')->name('edit-profile');


    });

});


Route::get('lang/{locale}', function ($locale) {
    Session::put('lang', $locale);
    return redirect()->back();
});

Route::get("check-email-agent", "Backend\AgentController@check_agent_email");
Route::get("check-mobile-agent", "Backend\AgentController@check_agent_mobile");
Route::get("check-phone-agent", "Backend\AgentController@check_agent_phone");

Route::get("check-email-corporate", "Backend\CorprateController@check_corporate_email");
Route::get("check-mobile-corporate", "Backend\CorprateController@check_corporate_mobile");

Route::get("check-email-customer", "Backend\CustomerController@check_customer_email");
Route::get("check-mobile-customer", "Backend\CustomerController@check_customer_mobile");

// By Mahmoud
Route::get("get_cities/{key}", "Backend\CityController@get_cities");

/*-*-*-*-*-*-*-*-*--*--*---** Agent Panel URL *---*-*--*-*-*-*-*-*-*-*-*-*-**/
Route::group(['prefix' => 'mngrAgent', 'as' => 'mngrAgent.', 'namespace' => 'Backend'], function () {

    Config::set('auth.defines.guard', 'agent');

    Route::post('/login', 'UserController@authenticate_agent');

    Route::get('/', function () {
        Config::set('auth.defines.guard', 'agent');
        return redirect('/mngrAgent/login');
    });
    Route::get('/login', function () {
        Config::set('auth.defines.guard', 'agent');
        if (Auth::guard('agent')->user()) {
            return redirect('/mngrAgent/dashboard');
        } else {
            return view('agent.login');
        }
    });


    Route::group(['middleware' => 'agent'], function () {

        Config::set('auth.defines.guard', 'agent');
        Route::resource("drivers", "DriverController", ['only' => ['index', 'show', 'create', 'store']]);
        Route::get('all_orders', 'OrderController@orders_agent_index')->name('mngrAgent/all_orders');
        Route::get('invoices/{type}', 'InvoiceController@invoicee')->name('invoices');
        Route::get('invoices/show/{id}', 'InvoiceController@shwoInvoiceid')->name('shwoInvoiceid');
        Route::get('invoices/driveres/show/{id}', 'InvoiceController@mydrivereshow')->name('mydrivereshow');
        Route::get('Pilot_Orders', 'OrderController@Pilot_Orders')->name('Pilot_Orders');
        Route::get('showpilotdriver/{driver_id}/{order_id}/{delv_id}/{acc_id}', 'OrderController@showpilotdriver')->name('showpilotdriver');
        Route::post('Pilot_update', 'OrderController@pilotupdata')->name('pilotupdata');
        #ask_review_order
        Route::get('/ask_review_order/{key}', 'OrderViewController@ask_review_order');

        Route::get('/dashboard', 'AgentController@dashboard');
        Route::get('/403', function () {
            Config::set('auth.defines.guard', 'agent');
            return view('agent.403');
        });
        Route::get('/404', function () {
            Config::set('auth.defines.guard', 'agent');
            return view('agent.404');
        });
        Route::post('/logout', 'UserController@logout');

        Route::resource("shipment_tool_orders", "ShipmentToolOrderController");
        #list_suggested
        Route::get("list_suggested", "OrderController@list_suggested");
        Route::resource("trucks", "TruckController", ['only' => ['index', 'show', 'create', 'store']]);
        Route::resource("agent_invoices", "AgentInvoiceController", ['only' => ['index', 'show']]);
        Route::resource("agents", "AgentController");

        Route::get("orders/ShowOrders/{type}/{id}", "OrderController@ShowOrders")->name("orders.showOrders");

        Route::resource("orders", "OrderController");
        #orders_agent_pending
        Route::get("pending_orders", "OrderController@orders_agent_pending");
        Route::get("waiting_orders", "OrderController@orders_agent_waiting");

        Route::get("list_by_drivers/{key}", "OrderController@list_by_drivers");
        Route::get("new_shippeng_field", "ShipmentToolOrderController@new_shippeng_field");
        Route::get('/track_drivers/{key}', 'DriverController@track_drivers');
        Route::get('/profile', 'AgentController@get_agent_profile');
        Route::get('/edit-profile', 'AgentController@get_agent_edit_profile');
        Route::put('/edit-profile', 'AgentController@post_agent_edit_profile');
        Route::post('/del/{id}', 'DriverController@del');
        Route::get("trucks/driversTrucks/{id}", "TruckController@DriverVehicle")->name('trucks.DriverVehicle');
        Route::post("users/addfirebaseToken", "UserController@firebaseToken")->name("users.addfirebaseToken");
        Route::resource("users", "UserController");

        Route::post('send_to_other_driver', 'OrderController@send_to_other_driver');

    });
});
/* *-*-*-*-*-*-*-*-*-*-*-*-* ADMIN PANEL URL -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
Route::group(['prefix' => 'mngrAdmin', 'as' => 'mngrAdmin.', 'namespace' => 'Backend'], function () {
    Config::set('auth.defines.guard', 'admin');
// Admin
    Route::post('/login', 'UserController@authenticate');

    Route::get('/', function () {
        return redirect('/mngrAdmin/login');
    });

    Route::get('/login', function () {
        if (Auth::guard('admin')->user()) {
            return redirect('/mngrAdmin/dashboard');
        }
        return view('backend.login');
    });

    Route::group(['middleware' => 'admin'], function () {
        Route::get('/403', function () {
            return view('backend.403');
        });
        Route::get('/404', function () {
            return view('backend.404');
        });
        Route::post('/logout', 'UserController@logout');
        Route::get('/track_drivers/{key}', 'DriverController@track_drivers');

        // Route::resource("shipment_tools" 		,"ShipmentToolController");
        Route::get("shipment_tools", "ShipmentToolController@index")->name('shipment_tools.index')->middleware('Permition:shipmentTools');
        Route::get("shipment_tools/create", "ShipmentToolController@create")->name('shipment_tools.create')->middleware('Permition:addShipmentTools');
        Route::post("shipment_tools", "ShipmentToolController@store")->name('shipment_tools.store')->middleware('Permition:addShipmentTools');
        Route::get("shipment_tools/{id}", "ShipmentToolController@show")->name('shipment_tools.show')->middleware('Permition:showShipmentTools');
        Route::get("shipment_tools/{id}/edit", "ShipmentToolController@edit")->name('shipment_tools.edit')->middleware('Permition:editShipmentTools');
        Route::put("shipment_tools/{id}/update", "ShipmentToolController@update")->name('shipment_tools.update')->middleware('Permition:editShipmentTools');
        Route::delete("shipment_tools/{id}", "ShipmentToolController@destroy")->name('shipment_tools.destroy')->middleware('Permition:deleteShipmentTools');

        // Route::resource("shipment_tool_orders" 	,"ShipmentToolOrderController");
        Route::get("shipment_tool_orders", "ShipmentToolOrderController@index")->name('shipment_tool_orders.index')->middleware('Permition:shipmentOrders');
        Route::get("shipment_tool_orders/create", "ShipmentToolOrderController@create")->name('shipment_tool_orders.create')->middleware('Permition:addShipmentOrders');
        Route::post("shipment_tool_orders", "ShipmentToolOrderController@store")->name('shipment_tool_orders.store')->middleware('Permition:addShipmentOrders');
        Route::get("shipment_tool_orders/{id}", "ShipmentToolOrderController@show")->name('shipment_tool_orders.show')->middleware('Permition:showShipmentOrders');
        Route::get("shipment_tool_orders/{id}/edit", "ShipmentToolOrderController@edit")->name('shipment_tool_orders.edit')->middleware('Permition:editShipmentOrders');
        Route::put("shipment_tool_orders/{id}/update", "ShipmentToolOrderController@update")->name('shipment_tool_orders.update')->middleware('Permition:editShipmentOrders');
        Route::delete("shipment_tool_orders/{id}", "ShipmentToolOrderController@destroy")->name('shipment_tool_orders.destroy')->middleware('Permition:deleteShipmentOrders');

        // Route::resource("trucks", "TruckController");
        Route::get("trucks", "TruckController@index")->name('trucks.index')->middleware('Permition:Vehicles');
        Route::get("trucks/create", "TruckController@create")->name('trucks.create')->middleware('Permition:addVehicle');
        Route::post("trucks", "TruckController@store")->name('trucks.store')->middleware('Permition:addVehicle');
        Route::get("trucks/{id}", "TruckController@show")->name('trucks.show')->middleware('Permition:showVehicle');
        Route::get("trucks/{id}/edit", "TruckController@edit")->name('trucks.edit')->middleware('Permition:editVehicle');
        Route::put("trucks/{id}/update", "TruckController@update")->name('trucks.update')->middleware('Permition:editVehicle');
        Route::delete("trucks/{id}", "TruckController@destroy")->name('trucks.destroy')->middleware('Permition:deleteVehicle');
        Route::get("change_status/{key}", "TruckController@change_status")->middleware('Permition:changeStatusVehicle');   // change_status

        // Route::resource("drivers", "DriverController");
        Route::get("drivers/{id}/download_papers", "DriverController@download_papers")->name('drivers.download_papers')->middleware('Permition:showDriver');
        Route::get("drivers", "DriverController@index")->name('drivers.index')->middleware('Permition:drivers');
        Route::get("drivers/create", "DriverController@create")->name('drivers.create')->middleware('Permition:addDriver');
        Route::post("drivers", "DriverController@store")->name('drivers.store')->middleware('Permition:addDriver');
        Route::get("drivers/{id}", "DriverController@show")->name('drivers.show')->middleware('Permition:showDriver');
        Route::get("drivers/{id}/edit", "DriverController@edit")->name('drivers.edit')->middleware('Permition:editDriver');
        Route::put("drivers/{id}/update", "DriverController@update")->name('drivers.update')->middleware('Permition:editDriver');
        // Route::delete("drivers/{id}", "DriverController@destroy")->name('drivers.destroy')->middleware('Permition:deleteDriver');
        Route::post('/del/{id}', 'DriverController@del')->middleware('Permition:deleteDriver');
        Route::get("driver/active/{id}", "DriverController@active")->middleware('Permition:activeDriver');
        Route::get("driver/block/{id}", "DriverController@block")->middleware('Permition:blockedDriver');
        Route::get("check_driver_has_vehicle/{id}", "DriverController@checkHasVehicle");

        // Route::resource("agents", "AgentController");
        Route::get("agents", "AgentController@index")->name('agents.index')->middleware('Permition:agents');
        Route::get("agents/create", "AgentController@create")->name('agents.create')->middleware('Permition:addAgent');
        Route::post("agents", "AgentController@store")->name('agents.store')->middleware('Permition:addAgent');
        Route::get("agents/{id}", "AgentController@show")->name('agents.show')->middleware('Permition:showAgent');
        Route::get("agents/{id}/edit", "AgentController@edit")->name('agents.edit')->middleware('Permition:editAgent');
        Route::put("agents/{id}/update", "AgentController@update")->name('agents.update')->middleware('Permition:editAgent');
        Route::delete("agents/{id}", "AgentController@destroy")->name('agents.destroy')->middleware('Permition:deleteAgent');
        Route::get('agents/showdriver/{id}', 'DriverController@showdriver')->middleware('Permition:showDriverAgent');   // tab agent button dirver
        Route::get("agents/active/{id}", "AgentController@active")->middleware('Permition:agents');

        // Route::resource("cities", "CityController");
        Route::get("cities", "CityController@index")->name('cities.index')->middleware('Permition:cities');
        Route::get("cities/create", "CityController@create")->name('cities.create')->middleware('Permition:addCitie');
        Route::post("cities", "CityController@store")->name('cities.store')->middleware('Permition:addCitie');
        Route::get("cities/{id}", "CityController@show")->name('cities.show')->middleware('Permition:cities');
        Route::get("cities/{id}/edit", "CityController@edit")->name('cities.edit')->middleware('Permition:editCitie');
        Route::put("cities/{id}/update", "CityController@update")->name('cities.update')->middleware('Permition:editCitie');
        Route::delete("cities/{id}", "CityController@destroy")->name('cities.destroy')->middleware('Permition:deleteCitie');

        // Route::resource("governorates", "governorateController
        Route::get("governorates", "governorateController@index")->name('governorates.index')->middleware('Permition:governorates');
        Route::get("governorates/create", "governorateController@create")->name('governorates.create')->middleware('Permition:addGovernorate');
        Route::post("governorates", "governorateController@store")->name('governorates.store')->middleware('Permition:addGovernorate');
        Route::get("governorates/{id}", "governorateController@show")->name('governorates.show')->middleware('Permition:governorates');
        Route::get("governorates/{id}/edit", "governorateController@edit")->name('governorates.edit')->middleware('Permition:editGovernorate');
        Route::put("governorates/{id}/update", "governorateController@update")->name('governorates.update')->middleware('Permition:editGovernorate');
        Route::delete("governorates/{id}", "governorateController@destroy")->name('governorates.destroy')->middleware('Permition:deleteGovernorate');

        // Route::resource("colors", "ColorController");
        Route::get("colors", "ColorController@index")->name('colors.index')->middleware('Permition:vehicleColors');
        Route::get("colors/create", "ColorController@create")->name('colors.create')->middleware('Permition:addVehicleColor');
        Route::post("colors", "ColorController@store")->name('colors.store')->middleware('Permition:addGovernorate');
        Route::get("colors/{id}", "ColorController@show")->name('colors.show')->middleware('Permition:vehicleColors');
        Route::get("colors/{id}/edit", "ColorController@edit")->name('colors.edit')->middleware('Permition:editVehicleColor');
        Route::put("colors/{id}/update", "ColorController@update")->name('colors.update')->middleware('Permition:editVehicleColor');
        Route::delete("colors/{id}", "ColorController@destroy")->name('colors.destroy')->middleware('Permition:deleteVehicleColor');

        // Route::resource("users", "UserController");
        // Route::resource("admins", "AdminController");
        Route::get("admins", "AdminController@index")->name('admins.index')->middleware('Permition:admins');
        Route::get("users/create", "UserController@create")->name('users.create')->middleware('Permition:addAdmin');
        Route::post("users", "UserController@store")->name('users.store')->middleware('Permition:addAdmin');
        Route::get("users/{id}", "UserController@show")->name('users.show')->middleware('Permition:showAdmin');
        Route::get("users/{id}/edit", "UserController@edit")->name('users.edit')->middleware('Permition:editAdmin');
        Route::put("users/{id}/update", "UserController@update")->name('users.update')->middleware('Permition:editAdmin');
        Route::delete("users/{id}", "UserController@destroy")->name('users.destroy')->middleware('Permition:deleteAdmin');
        Route::get("user/block/{id}", "UserController@block")->middleware('Permition:blockedAdmin');

        // Route::resource("countries", "CountryController");
        Route::get("countries", "CountryController@index")->name('countries.index')->middleware('Permition:countries');
        Route::get("countries/create", "CountryController@create")->name('countries.create')->middleware('Permition:addCountrie');
        Route::post("countries", "CountryController@store")->name('countries.store')->middleware('Permition:addCountrie');
        Route::get("countries/{id}", "CountryController@show")->name('countries.show')->middleware('Permition:countries');
        Route::get("countries/{id}/edit", "CountryController@edit")->name('countries.edit')->middleware('Permition:editCountrie');
        Route::put("countries/{id}/update", "CountryController@update")->name('countries.update')->middleware('Permition:editCountrie');
        Route::delete("countries/{id}", "CountryController@destroy")->name('countries.destroy')->middleware('Permition:deleteCountrie');

        // Route::resource("customers", "CustomerController");
        Route::get("customers/details/{id}", "CustomerController@details")->name('customers.details')->middleware('Permition:customers');
        Route::get("customers", "CustomerController@index")->name('customers.index')->middleware('Permition:customers');
        Route::get("customers/create", "CustomerController@create")->name('customers.create')->middleware('Permition:addCustomer');
        Route::post("customers", "CustomerController@store")->name('customers.store')->middleware('Permition:addCustomer');
        Route::get("customers/{id}", "CustomerController@show")->name('customers.show')->middleware('Permition:showCustomer');
        Route::get("customers/{id}/edit", "CustomerController@edit")->name('customers.edit')->middleware('Permition:editCustomer');
        Route::put("customers/{id}/update", "CustomerController@update")->name('customers.update')->middleware('Permition:editCustomer');
        Route::delete("customers/{id}", "CustomerController@destroy")->name('customers.destroy')->middleware('Permition:deleteCustomer');
        Route::get("customer/block/{id}", "CustomerController@block")->middleware('Permition:blockedCustomer');
        Route::get("customer/status/{id}", "CustomerController@status")->middleware('Permition:changeStatus');

        Route::get("ticket_rules", "TicketRuleController@index")->name('corporates.index')->middleware('Permition:tickets');
        Route::post("change-active-ticket-rules", "TicketRuleController@change_active")->middleware('Permition:tickets');

        // Route::resource("corporates", "CorprateController");
        Route::get("corporates/order_details/{id}", "CorprateController@order_details")->name('corporates.details')->middleware('Permition:corporates');
        Route::get("corporates", "CorprateController@index")->name('corporates.index')->middleware('Permition:corporates');
        Route::get("corporates/create", "CorprateController@create")->name('corporates.create')->middleware('Permition:addCorporate');
        Route::post("corporates", "CorprateController@store")->name('corporates.store')->middleware('Permition:addCorporate');
        Route::get("corporates/{id}", "CorprateController@show")->name('corporates.show')->middleware('Permition:showCorporate');
        Route::get("corporates/{id}/edit", "CorprateController@edit")->name('corporates.edit')->middleware('Permition:editCorporate');
        Route::put("corporates/{id}/update", "CorprateController@update")->name('corporates.update')->middleware('Permition:editCorporate');
        Route::delete("corporates/{id}", "CorprateController@destroy")->name('corporates.destroy')->middleware('Permition:deleteCorporate');
        Route::get("corporate/active/{id}", "CorprateController@active")->middleware('Permition:activeCorporate');
        Route::get("get_corporate_customers/{id}", "CorprateController@get_corporate_customers")->middleware('Permition:showCorporate');
        Route::get("get_user_corporates/{id}", "CorprateController@get_user_corporates")->middleware('Permition:showCorporate');

        Route::get("corporatePrices/generate", "CorporatePriceController@generate")->name('corporatePrices.generate')->middleware('Permition:shipmentDestinations');
        Route::post("corporatePrices/save_cost", "CorporatePriceController@save_cost")->name('corporatePrices.save_cost')->middleware('Permition:editShipmentDestinations');
        Route::get("corporatePrices", "CorporatePriceController@index")->name('corporatePrices.index')->middleware('Permition:shipmentDestinations');
        Route::get("corporatePrices/create", "CorporatePriceController@create")->name('corporatePrices.create')->middleware('Permition:addShipmentDestinations');
        Route::post("corporatePrices", "CorporatePriceController@store")->name('corporatePrices.store')->middleware('Permition:addShipmentDestinations');
        Route::get("corporatePrices/{id}", "CorporatePriceController@show")->name('corporatePrices.show')->middleware('Permition:showShipmentDestinations');
        Route::get("corporatePrices/{id}/edit", "CorporatePriceController@edit")->name('corporatePrices.edit')->middleware('Permition:editShipmentDestinations');
        Route::put("corporatePrices/{id}/update", "CorporatePriceController@update")->name('corporatePrices.update')->middleware('Permition:editShipmentDestinations');
        Route::delete("corporatePrices/{id}", "CorporatePriceController@destroy")->name('corporatePrices.destroy')->middleware('Permition:deleteShipmentDestinations');

        Route::get("customerPrices/generate", "CustomerPriceController@generate")->name('customerPrices.generate')->middleware('Permition:shipmentDestinations');
        Route::post("customerPrices/save_cost", "CustomerPriceController@save_cost")->name('customerPrices.save_cost')->middleware('Permition:editShipmentDestinations');
        Route::get("customerPrices", "CustomerPriceController@index")->name('customerPrices.index')->middleware('Permition:shipmentDestinations');
        Route::get("customerPrices/create", "CustomerPriceController@create")->name('customerPrices.create')->middleware('Permition:addShipmentDestinations');
        Route::post("customerPrices", "CustomerPriceController@store")->name('customerPrices.store')->middleware('Permition:addShipmentDestinations');
        Route::get("customerPrices/{id}", "CustomerPriceController@show")->name('customerPrices.show')->middleware('Permition:showShipmentDestinations');
        Route::get("customerPrices/{id}/edit", "CustomerPriceController@edit")->name('customerPrices.edit')->middleware('Permition:editShipmentDestinations');
        Route::put("customerPrices/{id}/update", "CustomerPriceController@update")->name('customerPrices.update')->middleware('Permition:editShipmentDestinations');
        Route::delete("customerPrices/{id}", "CustomerPriceController@destroy")->name('customerPrices.destroy')->middleware('Permition:deleteShipmentDestinations');


        // Route::resource("corporate_targets", "CorporateTargetController");
        Route::get("corporate_targets", "CorporateTargetController@index")->name('CorporateTarget.index');
        Route::get("corporate_targets/create", "CorporateTargetController@create")->name('CorporateTarget.create');
        Route::post("corporate_targets", "CorporateTargetController@store")->name('CorporateTarget.store');
        Route::get("corporate_targets/{id}", "CorporateTargetController@show")->name('CorporateTarget.show');
        Route::get("corporate_targets/{id}/edit", "CorporateTargetController@edit")->name('CorporateTarget.edit');
        Route::put("corporate_targets/{id}/update", "CorporateTargetController@update")->name('CorporateTarget.update');
        Route::delete("corporate_targets/{id}", "CorporateTargetController@destroy")->name('CorporateTarget.destroy');

        // Route::resource("wallets", "WalletController");
        Route::get("wallets", "WalletController@index")->name('wallets.index')->middleware('Permition:wallets');
        Route::get("wallets/create", "WalletController@create")->name('wallets.create')->middleware('Permition:addWallet');
        Route::post("wallets", "WalletController@store")->name('wallets.store')->middleware('Permition:addWallet');
        Route::get("wallets/{id}", "WalletController@show")->name('wallets.show')->middleware('Permition:viewLogs');
        Route::get("wallets/{id}/edit", "WalletController@edit")->name('wallets.edit')->middleware('Permition:editWallet');
        Route::put("wallets/{id}/update", "WalletController@update")->name('wallets.update')->middleware('Permition:editWallet');
        Route::delete("wallets/{id}", "WalletController@destroy")->name('wallets.destroy')->middleware('Permition:deleteWallet');
        Route::put('sub_wallet', 'WalletController@sub_wallet')->middleware('Permition:decrementWallet');
        Route::put('add_wallet', 'WalletController@add_wallet')->middleware('Permition:incrementWallet');

        // Route::resource("payment_methods", "PaymentMethodController");
        Route::get("payment_methods", "PaymentMethodController@index")->name('payment_methods.index')->middleware('Permition:paymentMethods');
        Route::get("payment_methods/create", "PaymentMethodController@create")->name('payment_methods.create')->middleware('Permition:addPaymentMethod');
        Route::post("payment_methods", "PaymentMethodController@store")->name('payment_methods.store')->middleware('Permition:addPaymentMethod');
        Route::get("payment_methods/{id}", "PaymentMethodController@show")->name('payment_methods.show')->middleware('Permition:showPaymentMethod');
        Route::get("payment_methods/{id}/edit", "PaymentMethodController@edit")->name('payment_methods.edit')->middleware('Permition:editPaymentMethod');
        Route::put("payment_methods/{id}/update", "PaymentMethodController@update")->name('payment_methods.update')->middleware('Permition:editPaymentMethod');
        Route::delete("payment_methods/{id}", "PaymentMethodController@destroy")->name('payment_methods.destroy')->middleware('Permition:deletePaymentMethod');

        // Route::resource("promo_codes", "PromoCodeController");
        Route::get("promo_codes", "PromoCodeController@index")->name('promo_codes.index')->middleware('Permition:promoCodes');
        Route::get("promo_codes/create", "PromoCodeController@create")->name('promo_codes.create')->middleware('Permition:addPromeCode');
        Route::post("promo_codes", "PromoCodeController@store")->name('promo_codes.store')->middleware('Permition:addPromeCode');
        Route::get("promo_codes/{id}", "PromoCodeController@show")->name('promo_codes.show')->middleware('Permition:showPromoCode');
        Route::get("promo_codes/{id}/edit", "PromoCodeController@edit")->name('promo_codes.edit')->middleware('Permition:editPromoCode');
        Route::put("promo_codes/{id}/update", "PromoCodeController@update")->name('promo_codes.update')->middleware('Permition:editPromoCode');
        Route::delete("promo_codes/{id}", "PromoCodeController@destroy")->name('promo_codes.destroy')->middleware('Permition:deletePromoCode');

        // Route::resource("model_types", "ModelTypeController");
        Route::get("model_types", "ModelTypeController@index")->name('model_types.index')->middleware('Permition:vehicleModels');
        Route::get("model_types/create", "ModelTypeController@create")->name('model_types.create')->middleware('Permition:addVehicleModel');
        Route::post("model_types", "ModelTypeController@store")->name('model_types.store')->middleware('Permition:addVehicleModel');
        Route::get("model_types/{id}", "ModelTypeController@show")->name('model_types.show')->middleware('Permition:showVehicleModel');
        Route::get("model_types/{id}/edit", "ModelTypeController@edit")->name('model_types.edit')->middleware('Permition:editVehicleModel');
        Route::put("model_types/{id}/update", "ModelTypeController@update")->name('model_types.update')->middleware('Permition:editVehicleModel');
        Route::delete("model_types/{id}", "ModelTypeController@destroy")->name('model_types.destroy')->middleware('Permition:deleteVehicleModel');

        // Route::resource("invoices","InvoiceController");
        Route::get("invoices/scan-collection/{id}", "InvoiceController@scanInvoice")->name('invoices.index')->middleware('Permition:Invoices');
        Route::get("invoices", "InvoiceController@index")->name('invoices.index')->middleware('Permition:Invoices');
        Route::get("invoices/create", "InvoiceController@create")->name('invoices.create')->middleware('Permition:addInvoice');
        Route::post("invoices", "InvoiceController@store")->name('invoices.store')->middleware('Permition:addInvoice');
        Route::get("invoices/{id}", "InvoiceController@show")->name('invoices.show')->middleware('Permition:showInvoice');
        Route::get("invoices/{id}/edit", "InvoiceController@edit")->name('invoices.edit')->middleware('Permition:editInvoice');
        Route::put("invoices/{id}/update", "InvoiceController@update")->name('invoices.update')->middleware('Permition:editInvoice');
        Route::delete("invoices/{id}", "InvoiceController@destroy")->name('invoices.destroy')->middleware('Permition:deleteInvoce');
        Route::post("invoices/confirm", "InvoiceController@send_verify_code")->name('invoices.send_verify_code')->middleware('Permition:payInvoice');
        Route::post('transfer_orders', 'InvoiceController@transfer_orders')->middleware('Permition:transferOrders');
        Route::post("invoices/Verify", "InvoiceController@verify_phone")->name('invoices.verify_phone')->middleware('Permition:verifyInvoice');
        Route::post("invoices/Transact", "InvoiceController@Transaction")->name('invoices.Transaction')->middleware('Permition:transactionInvoices');
        Route::post("invoices/collect/{id}", "InvoiceController@collect")->name('invoices.collect')->middleware('Permition:transactionInvoices');
        Route::post("invoices/Transfere/{id}", "InvoiceController@Transfere")->name('invoices.Transfere')->middleware('Permition:transactionInvoices');
        Route::get("print/invoice/{id}", "InvoiceController@print")->middleware('Permition:printInvoice');

        // Route::resource("price_lists", "PriceListController");
        Route::get("price_lists", "PriceListController@index")->name('price_lists.index')->middleware('Permition:priceLists');
        Route::get("price_lists/create", "PriceListController@create")->name('price_lists.create')->middleware('Permition:addPriceList');
        Route::post("price_lists", "PriceListController@store")->name('price_lists.store')->middleware('Permition:addPriceList');
        Route::get("price_lists/{id}", "PriceListController@show")->name('price_lists.show')->middleware('Permition:showPriceList');
        Route::get("price_lists/{id}/edit", "PriceListController@edit")->name('price_lists.edit')->middleware('Permition:edditPriceList');
        Route::put("price_lists/{id}/update", "PriceListController@update")->name('price_lists.update')->middleware('Permition:edditPriceList');
        Route::delete("price_lists/{id}", "PriceListController@destroy")->name('price_lists.destroy')->middleware('Permition:deletePriceList');

        // Route::resource("employees", "EmployeeController");
        Route::get("employees", "EmployeeController@index")->name('employees.index')->middleware('Permition:employees');
        Route::get("employees/create", "EmployeeController@create")->name('employees.create')->middleware('Permition:addEmployee');
        Route::post("employees", "EmployeeController@store")->name('employees.store')->middleware('Permition:addEmployee');
        Route::get("employees/{id}", "EmployeeController@show")->name('employees.show')->middleware('Permition:showEmployee');
        Route::get("employees/{id}/edit", "EmployeeController@edit")->name('employees.edit')->middleware('Permition:editEmployee');
        Route::put("employees/{id}/update", "EmployeeController@update")->name('employees.update')->middleware('Permition:editEmployee');
        Route::delete("employees/{id}", "EmployeeController@destroy")->name('employees.destroy')->middleware('Permition:deleteEmployee');

        // Route::resource("resources", "ResourceController");
        Route::get("resources", "ResourceController@index")->name('resources.index')->middleware('Permition:resources');
        Route::get("resources/create", "ResourceController@create")->name('resources.create')->middleware('Permition:addResource');
        Route::post("resources", "ResourceController@store")->name('resources.store')->middleware('Permition:addResource');
        Route::get("resources/{id}", "ResourceController@show")->name('resources.show')->middleware('Permition:showResource');
        Route::get("resources/{id}/edit", "ResourceController@edit")->name('resources.edit')->middleware('Permition:editResource');
        Route::put("resources/{id}/update", "ResourceController@update")->name('resources.update')->middleware('Permition:editResource');
        Route::delete("resources/{id}", "ResourceController@destroy")->name('resources.destroy')->middleware('Permition:deleteResource');

        // Route::resource("stores","StoreController");
        Route::get("stores/stores_pdf", "StoreController@stores_pdf")->name('stores.stores_pdf')->middleware('Permition:showStore');
        Route::get("stores", "StoreController@index")->name('stores.index')->middleware('Permition:stores');
        Route::get("stores/create", "StoreController@create")->name('stores.create')->middleware('Permition:addStore');
        Route::post("stores", "StoreController@store")->name('stores.store')->middleware('Permition:addStore');
        Route::get("stores/{id}", "StoreController@show")->name('stores.show')->middleware('Permition:showStore');
        Route::get("stores/{id}/edit", "StoreController@edit")->name('stores.edit')->middleware('Permition:editStore');
        Route::post("stores/save_responsible", "StoreController@save_responsible")->name('stores.save_responsible')->middleware('Permition:editStore');
        Route::put("stores/{id}/update", "StoreController@update")->name('stores.update')->middleware('Permition:editStore');
        Route::delete("stores/{id}", "StoreController@destroy")->name('stores.destroy')->middleware('Permition:deleteStore');
        Route::get("stores/download_pdf/{id}", "StoreController@download_pdf")->name('stores.download_pdf')->middleware('Permition:showStore');

        Route::post("tickets/{id}/add_comment", "TicketController@add_comment")->name("tickets.add_comment")->middleware('Permition:editTickets');
        Route::get("tickets/{id}/change_status", "TicketController@change_status")->name("tickets.change_status")->middleware('Permition:editTickets');
        Route::get("tickets/jobs/generate", "TicketController@generate_tickets")->name("tickets.generate_tickets")->middleware('Permition:editTickets');
        Route::get("tickets/jobs/close", "TicketController@close_tickets")->name("tickets.close_tickets")->middleware('Permition:editTickets');
        Route::post("tickets/close_ticket", "TicketController@close_ticket")->name("orders.close_ticket")->middleware('Permition:editTickets');
        Route::resource("tickets", "TicketController");
        Route::get("tickets", "TicketController@index")->name('tickets.index')->middleware('Permition:tickets');
        Route::post("tickets", "TicketController@store")->name('tickets.store')->middleware('Permition:addTickets');
        Route::get("tickets/{id}", "TicketController@show")->name('tickets.show')->middleware('Permition:showTickets');
        Route::get("tickets/{id}/edit", "TicketController@edit")->name('tickets.edit')->middleware('Permition:editTickets');
        Route::put("tickets/{id}/update", "TicketController@update")->name('tickets.update')->middleware('Permition:editTickets');
        Route::delete("tickets/{id}", "TicketController@destroy")->name('tickets.destroy')->middleware('Permition:deleteTickets');

        // Route::resource("stocks","StockController");
        Route::get("stocks", "StockController@index")->name('stocks.index')->middleware('Permition:stocks');
        Route::get("stocks/create/{warehouse_id?}", "StockController@create")->name('stocks.create')->middleware('Permition:addStocks');
        Route::post("stocks", "StockController@store")->name('stocks.store')->middleware('Permition:addStocks');
        Route::get("stocks/{id}", "StockController@show")->name('stocks.show')->middleware('Permition:showStocks');
        Route::get("stocks/{id}/edit", "StockController@edit")->name('stocks.edit')->middleware('Permition:editStocks');
        Route::put("stocks/{id}/update", "StockController@update")->name('stocks.update')->middleware('Permition:editStocks');
        Route::delete("stocks/{id}", "StockController@destroy")->name('stocks.destroy')->middleware('Permition:deleteStocks');
        Route::post("stocks/save_actual_pieces", "StockController@save_actual_pieces")->name('stocks.save_actual_pieces')->middleware('Permition:editStocks');

        // Route::resource("permissions", "PermissionController");
        Route::get("permissions", "PermissionController@index")->name('permissions.index')->middleware('Permition:permissions');
        Route::get("permissions/create", "PermissionController@create")->name('permissions.create')->middleware('Permition:permissions');
        Route::post("permissions", "PermissionController@store")->name('permissions.store')->middleware('Permition:addPermission');
        Route::get("permissions/{id}", "PermissionController@show")->name('permissions.show')->middleware('Permition:permissions');
        Route::get("permissions/{id}/edit", "PermissionController@edit")->name('permissions.edit')->middleware('Permition:permissions');
        Route::put("permissions/{id}/update", "PermissionController@update")->name('permissions.update')->middleware('Permition:editPermission');
        Route::delete("permissions/{id}", "PermissionController@destroy")->name('permissions.destroy')->middleware('Permition:deletePermission');

        // Route::resource("type_permissions", "TypePermissionController");
        Route::get("type_permissions", "TypePermissionController@index")->name('type_permissions.index')->middleware('Permition:permissionTypes');
        Route::get("type_permissions/create", "TypePermissionController@create")->name('type_permissions.create')->middleware('Permition:permissionTypes');
        Route::post("type_permissions", "TypePermissionController@store")->name('type_permissions.store')->middleware('Permition:addPermissionType');
        Route::get("type_permissions/{id}", "TypePermissionController@show")->name('type_permissions.show')->middleware('Permition:permissionTypes');
        Route::get("type_permissions/{id}/edit", "TypePermissionController@edit")->name('type_permissions.edit')->middleware('Permition:permissionTypes');
        Route::put("type_permissions/{id}/update", "TypePermissionController@update")->name('type_permissions.update')->middleware('Permition:editPermissionType');
        Route::delete("type_permissions/{id}", "TypePermissionController@destroy")->name('type_permissions.destroy')->middleware('Permition:deletePermissionType');

        // Route::resource("admin_roles", "AdminRoleController");
        Route::get("get_role_users/{key}", "AdminRoleController@get_users");
        Route::get("admin_roles", "AdminRoleController@index")->name('admin_roles.index')->middleware('Permition:adminRoles');
        Route::get("admin_roles/create", "AdminRoleController@create")->name('admin_roles.create')->middleware('Permition:addAdminRole');
        Route::post("admin_roles", "AdminRoleController@store")->name('admin_roles.store')->middleware('Permition:addAdminRole');
        Route::get("admin_roles/{id}", "AdminRoleController@show")->name('admin_roles.show')->middleware('Permition:showAdminRole');
        Route::get("admin_roles/{id}/edit", "AdminRoleController@edit")->name('admin_roles.edit')->middleware('Permition:editAdminRole');
        Route::put("admin_roles/{id}/update", "AdminRoleController@update")->name('admin_roles.update')->middleware('Permition:editAdminRole');
        Route::delete("admin_roles/{id}", "AdminRoleController@destroy")->name('admin_roles.destroy')->middleware('Permition:adminRoles');

        // Route::resource("contacts", "ContactController");
        Route::get("contacts", "ContactController@index")->name('contacts.index')->middleware('Permition:contacts');
        Route::get("contacts/create", "ContactController@create")->name('contacts.create')->middleware('Permition:addContact');
        Route::post("contacts", "ContactController@store")->name('contacts.store')->middleware('Permition:addContact');
        Route::get("contacts/{id}", "ContactController@show")->name('contacts.show')->middleware('Permition:showContact');
        Route::get("contacts/{id}/edit", "ContactController@edit")->name('contacts.edit')->middleware('Permition:editContact');
        Route::put("contacts/{id}/update", "ContactController@update")->name('contacts.update')->middleware('Permition:editContact');
        Route::delete("contacts/{id}", "ContactController@destroy")->name('contacts.destroy')->middleware('Permition:deleteContact');

        //Fr3on Mobile app customer feedback display
        Route::get("feedback/index", "FeedBackController@index");
        Route::get("reviews/index", "FeedBackController@reviews");

        //Fr3on Technical Documentation
        Route::get("docs", "DocsController@index")->middleware('Permition:documentations');
        Route::get("docs/create", "DocsController@create")->middleware('Permition:addDocumentation');
        Route::post("docs/store", "DocsController@store")->middleware('Permition:addDocumentation');
        Route::get("docs/edit/{id}", "DocsController@show")->middleware('Permition:editDocumentation');
        Route::post("docs/update/{id}", "DocsController@update")->middleware('Permition:editDocumentation');
        Route::delete("docs/destroy/{id}", "DocsController@destroy")->middleware('Permition:deleteDocumentation');
        Route::get("docs/create-type", "DocsController@create")->middleware('Permition:addTypeDocumentation');
        Route::post("docs/store-type", "DocsController@store_type")->middleware('Permition:addTypeDocumentation');

        // Route::resource('settings', 'SettingController');
        Route::get('settings', 'SettingController@index')->middleware('Permition:editAboutUs');
        Route::put("setting_update", "SettingController@setting_update")->middleware('Permition:editAboutUs');

        Route::get('/corporate-sheets', 'CorporateSheetController@show')->middleware('Permition:reports');
        Route::post('/corporate-sheets', 'CorporateSheetController@index')->middleware('Permition:reports');

        Route::get('driver-sheets', 'DriverSheetController@show')->middleware('Permition:reports');
        Route::post('/driver-sheets', 'DriverSheetController@index')->middleware('Permition:reports');
        Route::get('driver-sheets/daily-report', 'DriverSheetController@dailyReport')->middleware('Permition:reports');
        Route::post('/driver-sheets/daily-report', 'DriverSheetController@dailyReportIndex')->middleware('Permition:reports');

        Route::get('/reports', 'ReportController@show')->middleware('Permition:reports');
        Route::post('/reports', 'ReportController@index')->middleware('Permition:reports');
        Route::get("reports/daily_report", "ReportController@daily_report")->middleware('Permition:reports');
        Route::get("reports/captain_report", "ReportController@captain_report")->middleware('Permition:reports');
        Route::post("reports/corporate_list", "ReportController@corporate_list")->middleware('Permition:reports');
        Route::post("reports/upload_sheet", "ReportController@upload_sheet")->middleware('Permition:addOrder');
        Route::post("reports/handle_sheet", "ReportController@handle_sheet")->middleware('Permition:addOrder');
        Route::post("reports/export_sheet", "ReportController@export_sheet")->middleware('Permition:addOrder');
        Route::post("reports/update_status", "ReportController@update_status")->middleware('Permition:editOrder');
        Route::post("reports/upload_invoice", "ReportController@upload_invoice")->middleware('Permition:editOrder');
        Route::post("reports/update_delivery_date", "ReportController@update_delivery_date")->middleware('Permition:editOrder');
        Route::any("reports/scan_collection_print", "ReportController@scan_collection_print")->middleware('Permition:reports');
        Route::post("reports/export_scan_orders", "ReportController@export_scan_orders")->middleware('Permition:reports');
        Route::get("reports/recall_warehouse", "ReportController@recall_warehouse")->middleware('Permition:reports');
        Route::get("reports/add_recall_problem", "ReportController@recallDeliveryProblem")->middleware('Permition:reports');
        Route::any("print_police", "ReportController@print_police")->name("orders.print_police");
        Route::any("print_refund", "ReportController@print_refund")->name("orders.print_refund");
        Route::any("refund_excel", "ReportController@refund_excel")->name("orders.refund_excel");
        Route::any("scan_excel", "ReportController@scan_excel")->name("orders.scan_excel");
        Route::any("move_excel", "ReportController@move_excel")->name("orders.move_excel");
        Route::any("warehouse_excel", "ReportController@warehouse_excel")->name("orders.warehouse_excel");
        Route::any("corporates_report", "ReportController@corporates_report")->name("orders.corporates_report");
        Route::any("update_collected_cost", "ReportController@updateCollectedCost")->name("orders.update_collected_cost");
        Route::any("update_refund", "ReportController@updateRefund")->name("orders.update_refund");
        Route::any("update_recall", "ReportController@updateRecall")->name("orders.update_recall");
        Route::any("update_cost", "ReportController@updateCost")->name("orders.update_cost");
        Route::any("update_status", "ReportController@updateStatus")->name("orders.update_status");

        // Vehicle Types
        Route::get('vehicle', 'vehicleController@index')->middleware('Permition:vehicleTypes');
        Route::get('vehicle/create', 'vehicleController@create')->middleware('Permition:addVehicleType');
        Route::post('vehicle/create', 'vehicleController@store')->middleware('Permition:addVehicleType');
        Route::get('vehicle/delete/{id}', 'vehicleController@destroy')->middleware('Permition:deleteVehicleType');
        Route::get('vehicle/edit/{id}', 'vehicleController@edit')->middleware('Permition:editVehicleType');
        Route::post('vehicle/edit/{id}', 'vehicleController@update')->middleware('Permition:editVehicleType');

        // andriod virsoin
        Route::get('/android', 'versionsController@index')->middleware('Permition:androidVersions');
        Route::get('/android/create', 'versionsController@create')->middleware('Permition:addAndroidVersion');
        Route::post('/android/create/add', 'versionsController@add')->middleware('Permition:addAndroidVersion');
        Route::get('/android/edit/{id}', 'versionsController@edit')->middleware('Permition:editAndroidVersion');
        Route::post('/android/update/', 'versionsController@uprow')->middleware('Permition:editAndroidVersion');
        Route::post('/android/del/{key}', 'versionsController@delete')->middleware('Permition:deleteAndroidVersion');

        // orders type
        Route::get('/order_types', 'OrderTypesController@index')->name('types.index')->middleware('Permition:orderTypes');
        Route::get('/order_types/create', 'OrderTypesController@create')->middleware('Permition:addOrderType');
        Route::post('/order_types/add', 'OrderTypesController@add')->middleware('Permition:addOrderType');
        Route::get('/order_types/edit/{id}', 'OrderTypesController@edit')->middleware('Permition:editaddOrderType');
        Route::post('/order_types/edit/{id}', 'OrderTypesController@save_edit')->middleware('Permition:editaddOrderType');
        Route::post('/order_types/delete/{id}', 'OrderTypesController@delete')->middleware('Permition:deleteaddOrderType');

        //  Forward One Order To Driver Or Send Order To Other Driver
        Route::post('orders/send_to_other_driver', 'OrderController@send_to_other_driver')->middleware('Permition:forwordOneOrder');

        //  Forward Multi Orders To Agent Driver Or Send Orders To Other Agent Or Driver
        Route::post('/forward_order', 'OrderController@forward')->middleware('Permition:forworMultiOrder');
        Route::post('/orders/drop_orders/all', 'OrderController@drop_bulk_orders')->middleware('Permition:editOrder');
        Route::get('/orders/drop_orders/{id}', 'OrderController@drop_orders')->middleware('Permition:editOrder');
        Route::post('/orders/send_to_client', 'OrderController@send_to_client')->middleware('Permition:editOrder');
        Route::post('/delay_orders', 'OrderController@delay_orders')->middleware('Permition:editOrder');
        Route::post('/scan_validation', 'OrderController@scan_validation')->middleware('Permition:editOrder');
        Route::post('/scan_action', 'OrderController@scan_action')->middleware('Permition:editOrder');

        Route::post('/scan_order_validation', 'ScanController@scan_validation')->middleware('Permition:editOrder');
        Route::post('/scan_order_action', 'ScanController@scan_action')->middleware('Permition:editOrder');
        Route::post('/save_scan_order_rows', 'ScanController@save_scan_order_rows')->middleware('Permition:editOrder');
        Route::post('/save_captain_exit_orders', 'ScanController@save_captain_exit_orders')->middleware('Permition:editOrder');

        Route::post('/deliver_orders', 'OrderController@deliver_orders')->middleware('Permition:editOrder');
        Route::post('/receive_orders', 'OrderController@receive_orders')->middleware('Permition:editOrder');
        Route::get('/recall_if_dropped/{id}', 'OrderController@recall_if_dropped')->name('orders.recall_if_dropped')->middleware('Permition:editOrder');
        Route::get("orders", "OrderController@index")->name('orders.index')->middleware('Permition:orders');
        Route::get("order_log", "OrderController@order_log")->name('orders.index')->middleware('Permition:orders');
        Route::get("orders/all", "OrderController@all_orders")->name('orders.all_orders')->middleware('Permition:orders');
        Route::get("orders/create", "OrderController@create")->name('orders.create')->middleware('Permition:addOrder');
        Route::post("orders", "OrderController@store")->name('orders.store')->middleware('Permition:addOrder');
        Route::get("orders/{id}", "OrderController@show")->name('orders.show')->middleware('Permition:showOrder');
        Route::get("orders/{id}/edit", "OrderController@edit")->name('orders.edit')->middleware('Permition:editOrder');
        Route::put("orders/{id}/update", "OrderController@update")->name('orders.update')->middleware('Permition:editOrder');
        Route::delete("orders/{id}", "OrderController@destroy")->name('orders.destroy')->middleware('Permition:deleteOrder');
        Route::get("orders/showOrders/{type}/{id}", "OrderController@showOrders")->name("orders.showOrders")->middleware('Permition:showOrdersMember');
        Route::get("orders/showOrders/individuals", "InvoiceController@showOrdersIndividuals")->name("orders.individuals")->middleware('Permition:showOrdersMember');
        Route::post("orders/warehouse_dropoff", "OrderController@warehouse_dropoff")->name("orders.warehouse_dropoff")->middleware('Permition:orders');
        Route::post("orders/head_office", "OrderController@head_office")->name("orders.head_office")->middleware('Permition:orders');
        Route::post("orders/client_dropoff", "OrderController@client_dropoff")->name("orders.client_dropoff")->middleware('Permition:orders');
        Route::post("orders/add_problem", "OrderController@add_problem")->name("orders.add_problem")->middleware('Permition:orders');
        Route::delete("orders/delete_problem/{id}", "OrderController@delete_problem")->name("orders.delete_problem")->middleware('Permition:orders');
        Route::post("orders/add_order_comment", "OrderController@addOrderComment")->name("orders.add_order_comment")->middleware('Permition:orders');
        Route::delete("orders/delete_order_comment/{id}", "OrderController@deleteOrderComment")->name("orders.delete_order_comment")->middleware('Permition:orders');
        Route::post("orders/action_back", "OrderController@action_back")->name("orders.action_back")->middleware('Permition:orders');

        Route::get("order_cancel_requests", "OrderCancelRequestController@index");

        Route::post("collection_orders/delete_collection", "CollectionOrderController@delete_collection")->name('collection_orders.delete_collection')->middleware('Permition:collectionOrders');
        Route::get("collection_orders/print/{id}", "CollectionOrderController@print_collection")->name('collection_orders.print')->middleware('Permition:collectionOrders');
        Route::get("collection_orders", "CollectionOrderController@index")->name('collection_orders.index')->middleware('Permition:collectionOrders');
        Route::get("collection_orders/create", "CollectionOrderController@create")->name('collection_orders.create')->middleware('Permition:addCollectionOrders');
        Route::post("collection_orders", "CollectionOrderController@store")->name('collection_orders.store')->middleware('Permition:addCollectionOrders');
        Route::get("collection_orders/{id}", "CollectionOrderController@show")->name('collection_orders.show')->middleware('Permition:showCollectionOrders');
        Route::get("collection_orders/{id}/edit", "CollectionOrderController@edit")->name('collection_orders.edit')->middleware('Permition:editCollectionOrders');
        Route::put("collection_orders/{id}/update", "CollectionOrderController@update")->name('collection_orders.update')->middleware('Permition:editCollectionOrders');
        Route::delete("collection_orders/{id}", "CollectionOrderController@destroy")->name('collection_orders.destroy')->middleware('Permition:deleteCollectionOrders');

        Route::any('/move_collections/drop_orders', 'MoveCollectionController@drop_orders')->middleware('Permition:editMoveCollections');
        Route::get("move_collections", "MoveCollectionController@index")->name('move_collections.index')->middleware('Permition:moveCollections');
        Route::get("move_collections/create", "MoveCollectionController@create")->name('move_collections.create')->middleware('Permition:addMoveCollections');
        Route::post("move_collections", "MoveCollectionController@store")->name('move_collections.store')->middleware('Permition:addMoveCollections');
        Route::get("move_collections/{id}", "MoveCollectionController@show")->name('move_collections.show')->middleware('Permition:showMoveCollections');
        Route::get("move_collections/{id}/edit", "MoveCollectionController@edit")->name('move_collections.edit')->middleware('Permition:editMoveCollections');
        Route::put("move_collections/{id}/update", "MoveCollectionController@update")->name('move_collections.update')->middleware('Permition:editMoveCollections');
        Route::delete("move_collections/{id}", "MoveCollectionController@destroy")->name('move_collections.destroy')->middleware('Permition:deleteMoveCollections');

        Route::any('/refund_collections/refund_orders', 'RefundCollectionController@refund_orders')->middleware('Permition:recallOrder');
        Route::get("refund_collections", "RefundCollectionController@index")->name('refund_collections.index')->middleware('Permition:recallOrder');

        Route::get("scan_collections", "ScanCollectionController@index")->name('scan_collections.index')->middleware('Permition:scanQRCode');
        Route::delete("scan_collections/{id}", "ScanCollectionController@destroy")->name('scan_collections.destroy')->middleware('Permition:scanQRCode');

        Route::post("ForwardOrders", "OrderController@cancel_forward_to_another_captain")->middleware('Permition:forwordOneOrder');

        // Route::resource("governoratePrices", "governoratePriceController");
        Route::post("governoratePrices/save_cost", "governoratePriceController@save_cost")->name('governoratePrices.save_cost')->middleware('Permition:editShipmentDestinations');
        Route::get("governoratePrices", "governoratePriceController@index")->name('governoratePrices.index')->middleware('Permition:shipmentDestinations');
        Route::get("governoratePrices/create", "governoratePriceController@create")->name('governoratePrices.create')->middleware('Permition:addShipmentDestinations');
        Route::post("governoratePrices", "governoratePriceController@store")->name('governoratePrices.store')->middleware('Permition:addShipmentDestinations');
        Route::get("governoratePrices/{id}", "governoratePriceController@show")->name('governoratePrices.show')->middleware('Permition:showShipmentDestinations');
        Route::get("governoratePrices/{id}/edit", "governoratePriceController@edit")->name('governoratePrices.edit')->middleware('Permition:editShipmentDestinations');
        Route::put("governoratePrices/{id}/update", "governoratePriceController@update")->name('governoratePrices.update')->middleware('Permition:editShipmentDestinations');
        Route::delete("governoratePrices/{id}", "governoratePriceController@destroy")->name('governoratePrices.destroy')->middleware('Permition:deleteShipmentDestinations');

        // tmporders_create
        Route::post('tmporders_create', 'OrderController@tmporders_create')->middleware('Permition:addOrder');
        Route::post('save_collection', 'OrderController@save_collection')->middleware('Permition:addOrder');

        // Route::resource("order_logs", "OrderLogController");
        Route::get("order_logs", "OrderLogController@index")->name('order_logs.index')->middleware('Permition:ordersLogs');
        Route::get("order_logs/create", "OrderLogController@create")->name('order_logs.create')->middleware('Permition:addOrderLogs');
        Route::post("order_logs", "OrderLogController@store")->name('order_logs.store')->middleware('Permition:addOrderLogs');
        Route::get("order_logs/{id}", "OrderLogController@show")->name('order_logs.show')->middleware('Permition:showOrderLogs');
        Route::get("order_logs/{id}/edit", "OrderLogController@edit")->name('order_logs.edit')->middleware('Permition:editOrderLogs');
        Route::put("order_logs/{id}/update", "OrderLogController@update")->name('order_logs.update')->middleware('Permition:editOrderLogs');
        Route::delete("order_logs/{id}", "OrderLogController@destroy")->name('order_logs.destroy')->middleware('Permition:deleteOrderLogs');

        // Route::resource("notifications", "NotificationController");
        Route::get("notifications/types", "NotificationController@viewTypes")->name('notifications.types')->middleware('Permition:notificationTypes');
        Route::post("notifications/new-type", "NotificationController@AddType")->middleware('Permition:addNotificationType');
        Route::get("notifications/types/show/{id}", "NotificationController@viewType")->middleware('Permition:showNotificationType');
        Route::post("notifications/update-type/{id}", "NotificationController@UpdateType")->middleware('Permition:editNotificationType');
        Route::delete("notifications/types/destroy/{id}", "NotificationController@DestroyType")->middleware('Permition:deleteNotificationType');

        Route::get("notifications", "NotificationController@index")->name('notifications.index')->middleware('Permition:notifications');
        Route::get("notifications/create", "NotificationController@create")->name('notifications.create')->middleware('Permition:addNotification');
        Route::post("notifications", "NotificationController@store")->name('notifications.store')->middleware('Permition:addNotification');
        Route::get("notifications/{id}", "NotificationController@show")->name('notifications.show')->middleware('Permition:showNotification');
        Route::get("notifications/{id}/edit", "NotificationController@edit")->name('notifications.edit')->middleware('Permition:editNotification');
        Route::put("notifications/{id}/update", "NotificationController@update")->name('notifications.update')->middleware('Permition:editNotification');
        Route::delete("notifications/{id}", "NotificationController@destroy")->name('notifications.destroy')->middleware('Permition:deleteNotification');

        // Activity Log
        Route::get('activity_log', 'ActitivyLogController@index')->name('activity_log.index')->middleware('Permition:activityLog');
        Route::delete('activity_log/{id}', 'ActitivyLogController@destroy')->name('activity_log.delete')->middleware('Permition:deleteActivityLog');

        // Activity Log Type
        Route::get("activity_log_type", "ActivityTypeController@index")->name('activity_log_type.index')->middleware('Permition:activityLogTypes');
        Route::post("activity_log_type", "ActivityTypeController@store")->name('activity_log_type.store')->middleware('Permition:addActivityLogTypes');
        Route::put("activity_log_type/{id}/update", "ActivityTypeController@update")->name('activity_log_type.update')->middleware('Permition:editActivityLogTypes');
        Route::delete("activity_log_type/{id}", "ActivityTypeController@destroy")->name('activity_log_type.destroy')->middleware('Permition:deleteActivityLogTypes');

        ///////////////////////////////////////////////////////////////////////////////////////////////////////

        Route::get('filter_collection', 'WalletController@filter_collection');
        Route::post('filter_collection', 'WalletController@filter_collection');
        Route::get("orders/cancelOrder/{id}", "OrderController@cancelOrder")->name("orders.cancelOrder");
        Route::get("new_shippeng_field", "ShipmentToolOrderController@new_shippeng_field");
        Route::get("order_views/active/{id}", "OrderViewController@active");
        Route::get('/dashboard', 'UserController@dashboard');
        //Route::get("pdf/{type}", "OrderController@pdf");
        Route::get("/pdf/{type}/{id}", "OrderController@pdf");
        Route::post("/scan_print", "OrderController@scan_print");

        Route::post("/scan_order_print", "ScanController@scan_print");
        Route::post("/change_delivery_status", "ScanController@change_delivery_status");
        Route::post("/save_collected_cost", "ScanController@save_collected_cost");

        Route::get('Pilot_Orders', 'OrderController@Pilot_Orders')->name('Pilot_Orders');
        Route::get('orders/showpilotdriver/{driver_id}/{order_id}/{delv_id}/{acc_id}', 'OrderController@showpilotdriver')->name('showpilotdriver');
        #get get_customers by corparte id
        Route::get('get_customers/{key}', 'CustomerController@get_customers');
        #tmporders_update_view
        Route::get('tmporders_update_view/{key}', 'OrderController@tmporders_update_view');
        Route::post("orders/pilotUpdate", "OrderController@pilotupdata")->name("orders.pilotUpdate");
        Route::post("users/addfirebaseToken", "UserController@firebaseToken")->name("users.addfirebaseToken");
        Route::post('orders/Pilot_update', 'OrderController@pilotupdata');
        #close_wallet
        Route::post('close_wallet', 'WalletController@close_wallet')->middleware('Permition:closeWallet');
        Route::post("collection_orders/downloadStatus", "CollectionOrderController@downloadStatus")->name("collection_orders.downloadStatus");
        Route::post('option/fetch', 'CollectionOrderController@fetch'); // fetch data in Collection order


        Route::post("orders/recallOrder", "OrderController@recallOrder")->name("orders.recallOrder")->middleware('Permition:recallOrder');
        Route::post("send_cofirm_recall_sms", "OrderController@send_cofirm_recall_sms")->middleware('Permition:sendCofirmRecallSms');
        #cancel_order by client
        Route::post('/cancel_order_client', 'OrderController@cancel_order_client')->middleware('Permition:cancelClientOrder');
        Route::post('/cancel_order_admin', 'OrderController@cancel_order_admin')->middleware('Permition:cancelAdminOrder');
        Route::get('/forward_recall/{key}', 'OrderController@forward_recall_view')->middleware('Permition:forwardRecallOrder');
        Route::post('/forward_recall/{key}', 'OrderController@forward_recall')->middleware('Permition:forwardRecallOrder');

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        #save_invoice_price
        Route::post('save_invoice_price', 'InvoiceController@save_invoice_price')->middleware('Permition:saveInvoicePrice');

        #save_invoice_percent
        Route::post('save_invoice_percent', 'InvoiceController@save_invoice_percent')->middleware('Permition:saveInvoicePercent');

        #reset_invoice_price
        Route::post('reset_invoice_price', 'InvoiceController@reset_invoice_price')->middleware('Permition:resetInvoicePrice');

        # get_stock_by_corporate
        Route::get('get_stock_by_corporate/{key}', 'OrderController@get_stock_by_corporate');

        #getdriver_by_agents
        Route::get('getdriver_by_agents/{key}', 'DriverController@getdriver_by_agents');

        #get_cost_sender_receiver
        Route::get('get_cost/{key}/{receiver}', 'OrderController@get_cost_sender_receiver');
        #get_cost_sender_receiver pickup
        Route::get('get_cost_pickup/{key}/{receiver}', 'PickupPriceListController@get_cost_sender_receiver');


        Route::get('/scan_qrcode', 'OrderController@scan_page')->middleware('Permition:scanQRCode');
        Route::get('/scan_order_qrcode', 'ScanController@scan_page')->middleware('Permition:scanQRCode');

        Route::get('/print_pdf/{key}', 'OrderController@print_order_pdf_byid')->middleware('Permition:printOneOrder');

        Route::post('/delete_tmporder', 'OrderController@delete_tmporder')->middleware('Permition:deleteTmporder');

        #admin_notifications
        Route::get('/admin_notifications', 'NotificationController@admin_notifications')->middleware('Permition:notifications');

        Route::post('/pickup_orders', 'PickupController@pickup_orders');

        Route::post('/dropoff_pickups', 'PickupController@dropoff_pickups');

        #tmporders_reset_view
        Route::get('tmporders_reset_view/{key}', 'OrderController@tmporders_reset_view');

        Route::get("print/collection_orders/{id}/{type}", "OrderController@printCollection")->middleware('Permition:printCollectionOrders');


        // Route::resource("stocks","PickupController");
        Route::post("pickups/customer", "PickupController@customer")->middleware('Permition:pickups');
        Route::post("pickups/print", "PickupController@pdf")->middleware('Permition:pickups');
        Route::get("pickups", "PickupController@index")->name('pickups.index')->middleware('Permition:pickups');
        Route::get("pickups/create", "PickupController@create")->name('pickups.create')->middleware('Permition:addpickups');
        Route::post("pickups/create/fake", "PickupController@create_fake_pickup")->name('pickups.create_fake')->middleware('Permition:addpickups');

        Route::post("pickups", "PickupController@store")->name('pickups.store')->middleware('Permition:addpickups');
        Route::get("pickups/{id}", "PickupController@show")->name('pickups.show')->middleware('Permition:showpickups');
        Route::get("pickups/{id}/edit", "PickupController@edit")->name('pickups.edit')->middleware('Permition:editpickups');
        Route::put("pickups/{id}/update", "PickupController@update")->name('pickups.update')->middleware('Permition:editpickups');
        Route::delete("pickups/{id}", "PickupController@destroy")->name('pickups.destroy')->middleware('Permition:deletepickups');

        Route::get("get_cities/{key}", "CityController@get_cities");
        Route::get("get_store/{key}", "StoreController@get_store");

        Route::get("check_stock_capacity/{id}", "StockController@check_stock_capacity");

        #forward_pickup
        Route::post("forward_pickup", "PickupController@forward_pickup");
        Route::post("forward_pickup_agent", "PickupController@forward_pickup_agent");

        Route::get("print_pickup_pdf/{key}", "PickupController@print_pickup");
        Route::post("update_order_pickup", "PickupController@update_order_pickup");
        Route::delete("delete_order_pickup", "PickupController@delete_order_pickup");

        #
        Route::get("receive_pickup/{key}", "PickupController@receive_pickup");
        Route::get("cancel_pickup/{key}", "PickupController@cancel_pickup");

        Route::get("pickup_price_lists", "PickupPriceListController@index")->name('pickup_price_lists.index')->middleware('Permition:PickupPriceList');
        Route::get("pickup_price_lists/create", "PickupPriceListController@create")->name('pickup_price_lists.create')->middleware('Permition:addPickupPriceList');
        Route::post("pickup_price_lists", "PickupPriceListController@store")->name('pickup_price_lists.store')->middleware('Permition:addPickupPriceList');
        Route::get("pickup_price_lists/{id}", "PickupPriceListController@show")->name('pickup_price_lists.show')->middleware('Permition:showPickupPriceList');
        Route::get("pickup_price_lists/{id}/edit", "PickupPriceListController@edit")->name('pickup_price_lists.edit')->middleware('Permition:editPickupPriceList');
        Route::put("pickup_price_lists/{id}/update", "PickupPriceListController@update")->name('pickup_price_lists.update')->middleware('Permition:editPickupPriceList');
        Route::delete("pickup_price_lists/{id}", "PickupPriceListController@destroy")->name('pickup_price_lists.destroy')->middleware('Permition:deletePickupPriceList');

        //verify-mobile-customer
        Route::post("verify-mobile-customer", "CustomerController@change_verify_mobile");
        Route::post("verify-email-customer", "CustomerController@change_verify_email");
        Route::post("change-block-customer", "CustomerController@change_block");
        Route::post("change-active-corporate", "CorprateController@change_active");
        Route::post("change-slider-corporate", "CorprateController@change_slider");

        Route::post("verify-mobile-driver", "DriverController@change_verify_mobile");
        Route::post("change-verify-driver", "DriverController@change_verify_account");
        Route::post("change-block-driver", "DriverController@change_block");

        Route::post("verify-email-driver", "DriverController@change_verify_email");

        Route::post("add_area_agent", "AgentController@add_area");
        Route::delete("delete_area_agent", "AgentController@delete_area");
        Route::get("list_receivers", "OrderController@list_receivers");
        // Route::resource("services", "ServiceController");
        Route::get("services", "ServiceController@index")->name('services.index')->middleware('Permition:services');
        Route::get("services/create", "ServiceController@create")->name('services.create')->middleware('Permition:addService');
        Route::post("services", "ServiceController@store")->name('services.store')->middleware('Permition:addService');
        Route::get("services/{id}", "ServiceController@show")->name('services.show')->middleware('Permition:showService');
        Route::get("services/{id}/edit", "ServiceController@edit")->name('services.edit')->middleware('Permition:editService');
        Route::put("services/{id}/update", "ServiceController@update")->name('services.update')->middleware('Permition:editService');
        Route::delete("services/{id}", "ServiceController@destroy")->name('services.destroy')->middleware('Permition:deleteService');


        //Agent_Area
        Route::get("check-email-driver", "DriverController@check_driver_email");
        Route::get("check-mobile-driver", "DriverController@check_driver_mobile");
        Route::get("check-phone-driver", "DriverController@check_driver_phone");

    });

});

/* *-*-*-*-*-*-*-*-*-*-*-*-* Captain Dashboard URL -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
Route::group(['prefix' => 'captainDashboard', 'as' => 'captainDashboard.', 'namespace' => 'Captain'], function () {
    Config::set('auth.defines.guard', 'captain');
// Admin
    Route::post('/login', 'UserController@authenticate');

    Route::get('/', function () {
        return redirect('/captainDashboard/login');
    });

    Route::get('/login', function () {
        if (Auth::guard('captain')->user()) {
            return redirect('/captainDashboard/dashboard');
        }
        return view('captain.login');
    });

    Route::group(['middleware' => 'captain'], function () {
        Route::get('/403', function () {
            return view('captain.403');
        });
        Route::get('/404', function () {
            return view('captain.404');
        });
        Route::post('/logout', 'UserController@logout');

        Route::get('/dashboard', 'UserController@dashboard');

        Route::get("orders", "OrderController@index")->name('orders.index');
        Route::get("orders/all", "OrderController@all_orders")->name('orders.all_orders');
        Route::get("orders/{id}", "OrderController@show")->name('orders.show');
        Route::get('/print_pdf/{key}', 'OrderController@print_order_pdf_byid');
        Route::post("orders/add_order_comment", "OrderController@addOrderComment");
        Route::post("orders/save_status", "OrderController@saveStatus");
        Route::post("/scan_order_print", "OrderController@scan_print");
        Route::post('/forward_orders', 'OrderController@forward');

        Route::any("print_police", "ReportController@print_police")->name("orders.print_police");
        Route::get("reports/captain_report", "ReportController@captain_report");
        Route::post("reports/update_status", "ReportController@update_status");

        Route::get("drivers/{id}/download_papers", "DriverController@download_papers")->name('drivers.download_papers');
        Route::get("drivers", "DriverController@index")->name('drivers.index');
        Route::get("drivers/create", "DriverController@create")->name('drivers.create');
        Route::get("check-mobile-driver", "DriverController@check_driver_mobile");
        Route::get("drivers/{id}", "DriverController@show")->name('drivers.show');
        Route::post("drivers", "DriverController@store")->name('drivers.store');
        Route::get("drivers/{id}/edit", "DriverController@edit")->name('drivers.edit');
        Route::put("drivers/{id}/update", "DriverController@update")->name('drivers.update');
        Route::post("change-verify-driver", "DriverController@change_verify_account");

        Route::get('/scan_order_qrcode', 'ScanController@scan_page');
        Route::post('/scan_order_validation', 'ScanController@scan_validation');
        Route::post('/scan_order_action', 'ScanController@scan_action');
    });

});

//\URL::forceScheme('https');

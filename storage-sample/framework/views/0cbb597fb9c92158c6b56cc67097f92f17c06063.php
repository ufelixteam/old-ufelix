
<?php $__env->startSection('css'); ?>
  <title><?php echo e(__('backend.the_pickups')); ?> - <?php echo e(__('backend.edit')); ?></title>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('header'); ?>
  <div class="page-header">
    <h3><i class="glyphicon glyphicon-edit"></i>  <?php echo e(__('backend.Edit_PickUp')); ?> #<?php echo e($pickup->id); ?></h3>
  </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  <?php echo $__env->make('error', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
      <form action="<?php echo e(route('mngrAdmin.pickups.update', $pickup->id)); ?>" method="POST">
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">


          <div class="col-md-12 col-sm-12 col-xs-12" style="border: 2px solid #607d8b;margin-bottom: 15px;">
              <h4 style="color: #607d8b;font-weight: bold">Receiver Data</h4>

                  <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('store_id')): ?> has-error <?php endif; ?>">
                    <label for="store_id"><?php echo e(__('backend.droped_pickup_store')); ?>:</label>
                    <select id="store_id" name="store_id" class="form-control" >
                        <option value="Null" ><?php echo e(__('backend.Choose_Store')); ?></option>
                        <?php if(! empty($allStores)): ?>
                            <?php $__currentLoopData = $allStores; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $store): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($store->id); ?>" <?php echo e(! empty($pickup) && $store->id == $pickup->id ? 'selected' : ''); ?> >
                                    <?php echo e($store->name); ?>

                                </option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>

                    </select>
                    <?php if($errors->has("store_id")): ?>
                        <span class="help-block"><?php echo e($errors->first("store_id")); ?></span>
                    <?php endif; ?>
                </div>

              <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('receiver_name')): ?> has-error <?php endif; ?>">
                  <label for="receiver_name-field"> Name: </label>
                  <input type="text" id="receiver_name-field" name="receiver_name" class="form-control" value="<?php echo e(! empty($pickup) ? $pickup->receiver_name : old("receiver_name")); ?>"/>
                  <?php if($errors->has("receiver_name")): ?>
                      <span class="help-block"><?php echo e($errors->first("receiver_name")); ?></span>
                  <?php endif; ?>
              </div>
              <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('receiver_mobile')): ?> has-error <?php endif; ?>">
                  <label for="reciever_mobile-field"> Mobile: </label>
                  <input type="text" id="receiver_mobile-field" name="receiver_mobile" class="form-control" value="<?php echo e(! empty($pickup) ? $pickup->receiver_mobile : old("receiver_mobile")); ?>"/>
                  <?php if($errors->has("receiver_mobile")): ?>
                      <span class="help-block"><?php echo e($errors->first("receiver_mobile")); ?></span>
                  <?php endif; ?>
              </div>
            
              <div class="form-group col-md-3 col-sm-3">
                  <label class="control-label" for="r_government_id"> Government</label>

                  <select  class="form-control r_government_id" id="r_government_id"  name="r_government_id">
                      <option value="Null" data-display="Select">Receiver Government</option>
                      <?php if(! empty($app_governments)): ?>
                          <?php $__currentLoopData = $app_governments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $government): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($government->id); ?>"  <?php echo e(! empty($pickup) && $pickup->r_government_id  == $government->id ? 'selected':  ''); ?> >
                                  <?php echo e($government->name_en); ?>

                              </option>

                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php endif; ?>

                  </select>

              </div>

              <div class="form-group col-md-2 col-sm-2">
                  <label class="control-label" for="r_state_id"> City</label>

                  <select  class=" form-control r_state_id" id="r_state_id"  name="r_state_id" >
                      <?php if(! empty($r_cities)): ?>

                          <?php $__currentLoopData = $r_cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r_city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($r_city->id); ?>" <?php echo e(! empty($pickup) && $pickup->r_state_id  == $r_city->id ? 'selected':  ''); ?> >
                                  <?php echo e($r_city->name_en); ?>

                              </option>

                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php endif; ?>

                  </select>

              </div>
              <div class="form-group col-md-2 col-sm-2 <?php if($errors->has('receiver_latitude')): ?> has-error <?php endif; ?>">
                  <label for="receiver_latitude"> Latitude: </label>
                  <input type="text" id="receiver_latitude" name="receiver_latitude" class="form-control" value="<?php echo e(! empty($pickup) ? $pickup->receiver_latitude :  '30.0668886'); ?>"/>
                  <?php if($errors->has("receiver_latitude")): ?>
                      <span class="help-block"><?php echo e($errors->first("receiver_latitude")); ?></span>
                  <?php endif; ?>
              </div>
              <div class="form-group col-md-2 col-sm-2 <?php if($errors->has('receiver_longitude')): ?> has-error <?php endif; ?>">
                  <label for="receiver_longitude"> Longitude: </label>
                  <input type="text" id="receiver_longitude" name="receiver_longitude" class="form-control" value="<?php echo e(! empty($pickup) ? $pickup->receiver_longitude :  '31.1962743'); ?>"/>
                  <?php if($errors->has("receiver_longitude")): ?>
                      <span class="help-block"><?php echo e($errors->first("receiver_longitude")); ?></span>
                  <?php endif; ?>
              </div>


              <div class="form-group input-group col-md-6 col-sm-6 <?php if($errors->has('receiver_address')): ?> has-error <?php endif; ?>">
                  <label for="receiver_address">Receiver Address: </label>

                  <input type="text" id="receiver_address" name="receiver_address" class="form-control" value="<?php echo e(! empty($pickup) ? $pickup->receiver_address : old("receiver_address")); ?>"/>

                  <span class="input-group-append">
                  <button class="input-group-text bg-transparent" type="button" data-toggle="modal" data-target="#map1Modal"> <i class="fa fa-map-marker"></i></button>
                </span>

                  <?php if($errors->has("receiver_address")): ?>
                      <span class="help-block"><?php echo e($errors->first("receiver_address")); ?></span>
                  <?php endif; ?>
              </div>

          </div>

          <div class="col-md-12 col-sm-12 col-xs-12" style="border: 2px solid #607d8b;margin-bottom: 15px;">
              <h4 style="color: #607d8b;font-weight: bold">Sender Data</h4>

              <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('sender_name')): ?> has-error <?php endif; ?>">
                  <label for="sender_name-field">Name: </label>
                  <input type="text" id="sender_name-field" name="sender_name" class="form-control" value="<?php echo e(! empty($pickup) ? $pickup->sender_name : old("sender_name")); ?>"/>
                  <?php if($errors->has("sender_name")): ?>
                      <span class="help-block"><?php echo e($errors->first("sender_name")); ?></span>
                  <?php endif; ?>
              </div>

              <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('sender_mobile')): ?> has-error <?php endif; ?>">
                  <label for="sender_mobile-field">Mobile: </label>
                  <input type="text" id="sender_mobile-field" name="sender_mobile" class="form-control" value="<?php echo e(! empty($pickup) ? $pickup->sender_mobile : old("sender_mobile")); ?>"/>
                  <?php if($errors->has("sender_mobile")): ?>
                      <span class="help-block"><?php echo e($errors->first("sender_mobile")); ?></span>
                  <?php endif; ?>
              </div>

              <div class="form-group col-md-3 col-sm-3">
                  <label class="control-label" for="s_government_id">Government</label>

                  <select  class="form-control s_government_id" id="s_government_id"  name="s_government_id">
                      <option value="Null" data-display="Select">Government</option>
                      <?php if(! empty($app_governments)): ?>
                          <?php $__currentLoopData = $app_governments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $government): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($government->id); ?>" <?php echo e(! empty($pickup) && $pickup->s_government_id  == $government->id ? 'selected':  ''); ?> >
                                  <?php echo e($government->name_en); ?>

                              </option>

                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php endif; ?>

                  </select>

              </div>

              <div class="form-group col-md-2 col-sm-2">
                  <label class="control-label" for="s_state_id">City</label>

                  <select  class=" form-control s_state_id" id="s_state_id"  name="s_state_id" >
                      <?php if(! empty($s_cities)): ?>

                          <?php $__currentLoopData = $s_cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $s_city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($s_city->id); ?>" <?php echo e(! empty($pickup) && $pickup->s_state_id  == $s_city->id ? 'selected':  ''); ?> >
                                  <?php echo e($s_city->name_en); ?>

                              </option>

                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php endif; ?>

                  </select>

              </div>



              <div class="form-group col-md-2 col-sm-2 <?php if($errors->has('sender_latitude')): ?> has-error <?php endif; ?>">
                  <label for="sender_latitude">Latitude: </label>
                  <input type="text" id="sender_latitude" name="sender_latitude" class="form-control" value="<?php echo e(! empty($pickup) ? $pickup->sender_latitude : '30.0668886'); ?>"/>
                  <?php if($errors->has("sender_latitude")): ?>
                      <span class="help-block"><?php echo e($errors->first("sender_latitude")); ?></span>
                  <?php endif; ?>
              </div>
              <div class="form-group col-md-2 col-sm-2 <?php if($errors->has('sender_longitude')): ?> has-error <?php endif; ?>">
                  <label for="sender_longitude">Longitude: </label>
                  <input type="text" id="sender_longitude" name="sender_longitude" class="form-control" value="<?php echo e(! empty($pickup) ? $pickup->sender_longitude : '31.1962743'); ?>"/>
                  <?php if($errors->has("sender_longitude")): ?>
                      <span class="help-block"><?php echo e($errors->first("sender_longitude")); ?></span>
                  <?php endif; ?>
              </div>

              <div class="form-group input-group col-md-6 col-sm-6 <?php if($errors->has('sender_address')): ?> has-error <?php endif; ?>">
                  <label for="sender_address">Sender Address: </label>
                  <input type="text" id="sender_address" name="sender_address" class="form-control" value="<?php echo e(! empty($pickup) ? $pickup->sender_address : old("sender_address")); ?>"/>
                  <span class="input-group-append">
                  <button class="input-group-text bg-transparent" type="button" data-toggle="modal" data-target="#map2Modal"> <i class="fa fa-map-marker"></i></button>
                </span>
                  <?php if($errors->has("sender_address")): ?>
                      <span class="help-block"><?php echo e($errors->first("sender_address")); ?></span>
                  <?php endif; ?>
              </div>

          </div>


          <div class="col-md-12 col-sm-12 col-xs-12" style="border: 2px solid #607d8b;margin-bottom: 15px;">
              <h4 style="color: #607d8b;font-weight: bold">Order Data</h4>

              <div class="form-group col-md-2 col-sm-2 <?php if($errors->has('agent_id')): ?> has-error <?php endif; ?>">
                  <label for="agent_id-field">Agent </label>

                  <select id="agent_id-field" name="agent_id" class="form-control" >
                      <option value="Null" >Choose Agent</option>
                      <?php if(! empty($agents)): ?>
                          <?php $__currentLoopData = $agents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $agent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($agent->id); ?>" <?php echo e(! empty($pickup) && $pickup->agent_id  == $agent->id ? 'selected':  ''); ?> >
                                  <?php echo e($agent->name); ?>

                              </option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php endif; ?>

                  </select>
                  <?php if($errors->has("agent_id")): ?>
                      <span class="help-block"><?php echo e($errors->first("agent_id")); ?></span>
                  <?php endif; ?>
              </div>

              <div class="form-group col-md-2 col-sm-2">
                  <label class="control-label" for="driver_id">Driver</label>

                  <select  class=" form-control driver_id" id="driver_id"  name="driver_id" >
                      <option value="Null" >Choose Drivers</option>
                      <?php if(! empty($online_drivers)): ?>
                          <?php $__currentLoopData = $online_drivers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $driver): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($driver->id); ?>" <?php echo e(! empty($pickup) && $pickup->driver_id  == $driver->id ? 'selected':  ''); ?>>
                                  <?php echo e($driver->name); ?>

                              </option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php endif; ?>

                  </select>

              </div>
            

              <div class="form-group col-md-2 col-sm-3 <?php if($errors->has('delivery_price')): ?> has-error <?php endif; ?>">
                  <label for="delivery_price-field">Delivery Price: </label>
                  <input type="text" required class="form-control" id="delivery_price-field" name="delivery_price" value="<?php echo e(! empty($pickup) ? $pickup->delivery_price : old("delivery_price")); ?>" >
                  <?php if($errors->has("delivery_price")): ?>
                      <span class="help-block"><?php echo e($errors->first("delivery_price")); ?></span>
                  <?php endif; ?>
              </div>

                 <div class="form-group col-md-2 col-sm-2 <?php if($errors->has('bonous')): ?> has-error <?php endif; ?>">
            <label for="bonous-field"><?php echo e(__('backend.the_bonous')); ?>: </label>
            <input type="number" required class="form-control" id="bonous-field" name="bonous" value="<?php echo e(old('bonous')  ? old('bonous')  : '0'); ?>" >
            <?php if($errors->has("bonous")): ?>
              <span class="help-block"><?php echo e($errors->first("bonous")); ?></span>
            <?php endif; ?>
          </div>
              <div class="group-control col-md-2 col-sm-2">
              <label class="control-label" for="captain_received_date"> <?php echo e(__('backend.captain_received_date')); ?>:</label>

                <div class='input-group date' id='datepickerFilter' >
                  <input type="text" id="datepicker" name="captain_received_date" class="form-control" value="<?php echo e(! empty($pickup)  && $pickup->captain_received_time != '' ? Carbon\Carbon::parse($pickup->captain_received_time)->format('Y-m-d') : ''); ?>"/>
                  <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                </div>
              </div>


               <div class="group-control col-md-2 col-sm-2">
                <label class="control-label" for="driver_id"> <?php echo e(__('backend.captain_received_time')); ?>:</label>

                <div class='input-group date' id='datepickerFilter' >
                  <input type="time"  name="captain_received_time" class="form-control" value="<?php echo e(! empty($pickup) && $pickup->captain_received_time != '' ? Carbon\Carbon::parse($pickup->captain_received_time)->format('H:i') : ''); ?>"/>
                  <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                </div>
              </div>


          </div>

        <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
          <button type="submit" class="btn btn-primary"><?php echo e(__('backend.save_edits')); ?></button>
          <a class="btn btn-link pull-right" href="<?php echo e(route('mngrAdmin.pickups.index')); ?>"><i class="glyphicon glyphicon-backward"></i> <?php echo e(__('backend.back')); ?></a>
        </div>
      </form>
    </div>
  </div>

  <?php echo $__env->make('backend.dialog-map2', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript" src="<?php echo e(asset('/assets/scripts/pickup-order.js')); ?>"></script>

    
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQiN4qc7pC9b1BRQwkqXXD28peMfWcHvw&&callback=myMap"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <?php echo $__env->make('backend.pickups.js', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/pickups/edit.blade.php ENDPATH**/ ?>
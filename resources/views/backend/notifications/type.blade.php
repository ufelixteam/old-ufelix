@extends('backend.layouts.app')

@section('css')
    <title>{{__('backend.Notifications_Types')}}</title>
@endsection

@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-plus"></i>{{__('backend.Notifications_Types')}}</h3>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            @if(Request::is('mngrAdmin/notifications/types/show/*') && isset($type))
              @if(permission('deleteNotificationType')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <form action="{{ url('mngrAdmin/notifications/types/destroy', $type->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="btn-group pull-right" role="group" aria-label="...">
                        <button type="submit" class="btn btn-danger">{{__('backend.delete')}} <i class="glyphicon glyphicon-trash"></i></button>
                    </div>
                </form>
              @endif
                <form action="{{ url('mngrAdmin/notifications/update-type', $type->id) }}" method="POST" name="validateForm">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group col-md-12 col-sm-6 col-xs-6 @if($errors->has('title')) has-error @endif">
                        <label for="title-field">{{__('backend.title')}}</label>
                        <input type="text" id="title-field" name="title" class="form-control" value="{{ $type->title }}"/>
                        @if($errors->has("title"))
                            <span class="help-block">{{ $errors->first("title") }}</span>
                        @endif
                    </div>
                    <div class="well well-sm col-md-12 col-sm-12 col-xs-12">
                      @if(permission('editNotificationType')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                        <button type="submit" class="btn btn-primary">{{__('backend.save_edits')}}</button>
                      @endif
                        <a class="btn btn-link pull-right" href="{{ URL::previous() }}">
                          <i class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}
                        </a>
                    </div>
                </form>
            @else
              @if(permission('addNotificationType')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <form action="{{ url('mngrAdmin/notifications/new-type') }}" method="POST" name="validateForm">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group col-md-12 col-sm-6 col-xs-6 @if($errors->has('title')) has-error @endif">
                        <label for="title-field">{{__('backend.title')}}</label>
                        <input type="text" id="title-field" name="title" class="form-control" value="{{ old("title") }}"/>
                        @if($errors->has("title"))
                            <span class="help-block">{{ $errors->first("title") }}</span>
                        @endif
                    </div>
                    <div class="well well-sm col-md-12 col-sm-12 col-xs-12">
                        <button type="submit" class="btn btn-primary">{{__('backend.save')}}</button>
                        <a class="btn btn-link pull-right" href="{{ URL::previous() }}"><i class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
                    </div>
                </form>
                @endif
            @endif
        </div>
    </div>
    @if(Request::is('mngrAdmin/notifications/types') && isset($types))
    <table class="table table-condensed table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>{{__('backend.title')}}</th>
            <th class="text-right"></th>
        </tr>
        </thead>

        <tbody>
        @foreach($types as $value)
            <tr>
                <td>{{$value->id}}</td>
                <td>{{$value->title}}</td>
                <td class="text-right">
                  @if(permission('showNotificationType')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                      <a class="btn btn-xs btn-primary" href="{{ url('mngrAdmin/notifications/types/show', $value->id) }}"><i class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>
                  @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {!! $types->render() !!}
    @endif

@endsection

@section('scripts')
  <script type="text/javascript">
  $(function() {
    $("form[name='validateForm']").validate({
      // Specify validation rules
      rules: {
        title: {
          required: true,
        },
      },
      // Specify validation error messages
      messages: {
        title: {
          required: "{{__('backend.Please_Enter_Notifaction_Type')}}",
        },
      },

      errorPlacement: function(error, element) {
          $(element).parents('.form-group').append(error)
      },

      highlight: function (element) {
          $(element).parent().addClass('error')
      },

      unhighlight: function (element) {
          $(element).parent().removeClass('error')
      },

      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function(form) {
          form.submit();
      }
    });
  })
</script>
@endsection

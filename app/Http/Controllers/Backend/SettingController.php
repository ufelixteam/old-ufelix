<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use App\Http\Requests;
use App\Models\Setting;
use Flash;
use Illuminate\Http\Request;

class SettingController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $settings = Setting::orderBy('id', 'ASC')->get();

        return view('backend.settings.index', compact('settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.settings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $settings = Setting::orderBy('id', 'ASC')->get();
        foreach ($settings as $setting) {
            if ($request->input($setting->key)) {
                $setting->value = $request->input($setting->key);
                $setting->save();
            }
        }
        return redirect()->route('mngrAdmin.settings.index')->with('message', __('backend.Data_created_successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $setting = Setting::findOrFail($id);

        return view('backend.settings.show', compact('setting'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $setting = Setting::findOrFail($id);

        return view('backend.settings.edit', compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @param Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $setting = Setting::findOrFail($id);

        $setting->key = $request->input("key");
        $setting->value = $request->input("value");

        $setting->save();

        return redirect()->route('mngrAdmin.settings.index')->with('message', __('backend.Data_updated_successfully'));
    }

    #setting_update
    public function setting_update(Request $request)
    {

        $settings = Setting::orderBy('id', 'ASC')->get();
        foreach ($settings as $setting) { // start foreach all data in setting

            if ($request->input($setting->key)) { // check key

                if ($setting->display == "terms" || $setting->options == '-1') { // if ==  terms

                    $setting->value = $request->input($setting->key);
                    $setting->value_ar = $request->input($setting->key . "_ar");

                    $setting->save();

                } else {  // terms or not terms becouse update arabic value & english value

                    $setting->value = $request->input($setting->key);
                    $setting->save();


                } // end conditions terms

            } elseif ($setting->key == 'disable_real_date') {
                $setting->value = $request->input($setting->key) ? $request->input($setting->key) : 0;
                $setting->save();
            }

        } //end foreach

        // Function Add Action In Activity Log By Samir
        $msg_en = 'Edit Settings';
        $msg_ar = 'تعديل الاعدادات';
        addActivityLog($msg_en, $msg_ar, 32); // Helper Function in App\Helpers.php
        //Flash::success(__('backend.Data_updated_successfully'));

        return redirect('mngrAdmin/settings')->with('message', __('backend.Data_updated_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $setting = Setting::findOrFail($id);
        $setting->delete();

        return redirect()->route('mngrAdmin.settings.index')->with('message', __('backend.Data_deleted_successfully'));
    }

}

<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Contact;
use Response;
use Flash;
use File;
use Auth;

class ContactController extends Controller {

	// Get All Contacts
	public function index()
	{
		$contacts = Contact::orderBy('id', 'desc')->paginate(50);
		return view('backend.contacts.index', compact('contacts'));
	}

	// Create Contact Page
	public function create()
	{
		return view('backend.contacts.create');
	}

	// Added New Contact
	public function store(Request $request)
	{

			$this->validate($request, [
					'name' 				=> 'required',
					'email' 			=> 'required',
					'title' 			=> 'required',
					'email' 			=> 'required|email',
					'message'			=> 'required',
			]);

			$contact 						= new Contact();
			$contact->name	 		= $request->input("name");
      $contact->email 		= $request->input("email");
      $contact->mobile 		= $request->input("mobile");
      $contact->title 		= $request->input("title");
      $contact->message 	= $request->input("message");
			$contact->save();

			// Function Add Action In Activity Log By Samir
			addActivityLog('Add New Contact #ID ' . $contact->id , ' اضافة اتصال جديد #رقم ' . $contact->id, 30); // Helper Function in App\Helpers.php

      Flash::success(__('backend.Contact_saved_successfully'));
			return redirect()->route('mngrAdmin.contacts.index');
	}

	// View One Contact
	public function show($id)
	{
		$contact = Contact::findOrFail($id);
		return view('backend.contacts.show', compact('contact'));
	}

	// Edit Contact Page
	public function edit($id)
	{
		$contact = Contact::findOrFail($id);
		return view('backend.contacts.edit', compact('contact'));
	}

	// Update Contact
	public function update(Request $request, $id)
	{

			$this->validate($request, [
					'name' 				=> 'required',
					'email' 			=> 'required',
					'title' 			=> 'required',
					'email' 			=> 'required|email',
					'message'			=> 'required',
			]);

			$contact 						= Contact::findOrFail($id);
			$contact->name 			= $request->input("name");
      $contact->email 		= $request->input("email");
      $contact->mobile 		= $request->input("mobile");
      $contact->title 		= $request->input("title");
      $contact->message 	= $request->input("message");
			$contact->save();

			// Function Add Action In Activity Log By Samir
			addActivityLog('Edit Contact #ID ' . $contact->id , ' تعديل الاتصال #رقم  ' . $contact->id, 30); // Helper Function in App\Helpers.php

      Flash::warning(__('backend.Contact_Updated_successfully'));
			return redirect()->route('mngrAdmin.contacts.index');
	}

	// Delete Contact
	public function destroy($id)
	{
			$contact = Contact::findOrFail($id);
			$contact->delete();

			// Function Add Action In Activity Log By Samir
			addActivityLog('Delete Contact #ID ' . $contact->id , ' حذف الاتصال #رقم  ' . $contact->id, 30); // Helper Function in App\Helpers.php

			flash(__('backend.Contact_deleted_successfully'))->error();
			return redirect()->route('mngrAdmin.contacts.index');
	}

}

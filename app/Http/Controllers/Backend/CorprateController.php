<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use App\Models\City;
use App\Models\Corporate;
use App\Models\CorporateTarget;
use App\Models\Customer;
use App\Models\Governorate;
use App\Models\Order;
use App\Models\OrderType;
use App\Models\Store;
use App\Models\User;
use Auth;
use DB;
use ExcelReport;
use File;
use Flash;
use Illuminate\Http\Request;
use Response;

class CorprateController extends Controller
{

    private $guardType;

    public function __construct(Request $request)
    {
        if ($request->is('mngrAdmin/*') || $request->is('admin/*')) {
            \Config::set('auth.defaults.guard', 'admin');

        } elseif ($request->is('mngrAgent/*') || $request->is('agent/*')) {
            \Config::set('auth.defaults.guard', 'agent');
        }
        $this->guardType = \Config::get('auth.defaults.guard');
    }

    function check_corporate_email()
    {
        $email = \Request::get("email");
        $corporate = Corporate::where('email', $email)->first();
        if (!empty($corporate)) {
            echo 'false'; // If Not Found Corporte Email
        } else {
            echo 'true';  // If Found Corporte Email
        }
    }

    function check_corporate_mobile()
    {
        $mobile = \Request::get("mobile");
        $corporate = Corporate::where('mobile', $mobile)->first();
        if (!empty($corporate)) {
            echo 'false'; // If Not Found Corporte Mobile
        } else {
            echo 'true';  // If Found Corporte Mobile
        }
    }

    // Get All Corporte
    public function index(Request $request)
    {
        // $corporates = Corporate::orderBy('id', 'desc')->paginate('10');
        // return view('backend.corporates.index', compact('corporates'));
        $user = Auth::guard($this->guardType)->user();

        $users = User::where('role_id', 1)->where('is_active', 1)->select('users.*');
        $stores = Store::where('status', 1)->select('stores.*');

        if ($user->is_warehouse_user) {
            $corporates = Corporate::with('customers')->join('customers', 'customers.corporate_id', '=', 'corporates.id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id)
                ->select('corporates.*')
                ->groupBy('corporates.id')
                ->orderBy('corporates.id', 'desc');

            $stores = $stores->join('store_user', 'store_user.store_id', '=', 'stores.id')
                ->where('user_id', $user->id);
            $users = $users->join('store_user', 'store_user.user_id', '=', 'users.id')
                ->where('store_user.user_id', $user->id);

        }else{
            $corporates = Corporate::with('customers')->join('customers', 'customers.corporate_id', '=', 'corporates.id')
                ->select('corporates.*')
                ->groupBy('corporates.id')
                ->orderBy('corporates.id', 'desc');
        }

        $stores = $stores->get();
        $users = $users->get();

//        $corporates = Corporate::orderBy('corporates.id', 'desc');

        #check if active search by name an mobile
        $active = app('request')->input('active');   // Search By Active
        $search = app('request')->input('search');   // Search

        if (isset($search) && $search != "") {

            $corporates = $corporates->where(function ($query) use($search){
               $query->where('corporates.name', 'like', '%' . $search . '%')
                   ->orWhere('corporates.phone', 'like', '%' . $search . '%')
                   ->orWhere('corporates.mobile', 'like', '%' . $search . '%')
                   ->orWhere('customers.name', 'like', '%' . $search . '%')
                   ->orWhere('customers.mobile', 'like', '%' . $search . '%')
                   ->orWhere('customers.phone', 'like', '%' . $search . '%');
            });
        }

        if (isset($active) && $active != -1) {
            $corporates = $corporates->where('is_active', $active);
        }

        if (app('request')->filled('store_id')) {
            $corporates = $corporates->where('store_id', app('request')->input('store_id'));
        }

        if (app('request')->filled('responsible_id')) {
            $corporates = $corporates->where('responsible_id', app('request')->input('responsible_id'));
        }

        $corporates = $corporates->paginate(50);

        if ($request->ajax()) {
            return [
                'view' => view('backend.corporates.table', compact('corporates'))->render(),
            ];
        }

        return view('backend.corporates.index', compact('corporates', 'users', 'stores'));

    }

    // Added New Corporate Page
    public function create()
    {
        $governments = Governorate::orderBy('name_en', 'ASC')->get();
        $types = OrderType::where('status', 1)->orderBy('id', 'asc')->get();
        $stores = Store::where('status', 1)->select('stores.*');
        $users = User::where('role_id', 1)->where('is_active', 1)->select('users.*');

        $user = Auth::guard($this->guardType)->user();

        if ($user->is_warehouse_user) {
            $stores = $stores->join('store_user', 'store_user.store_id', '=', 'stores.id')
                ->where('user_id', $user->id);

            $users = $users->join('store_user', 'store_user.user_id', '=', 'users.id')
                ->where('store_user.user_id', $user->id);
        }

        $stores = $stores->get();
        $users = $users->get();

        return view('backend.corporates.create', compact('governments', 'types', 'stores', 'users'));
    }

    // Add New Corporte
    public function store(Request $request)
    {

        $request->validate([
            // Corporate
            'name' => 'required',
            'email' => 'required|unique:corporates',
//            'field' => 'required',
            'order_type' => 'required',
            'shipping_type' => 'required',
            'mobile' => 'required|unique:corporates',
            'government_id' => 'required',
            'city_id' => 'required',
            'address' => 'required',
//            'latitude' => 'required',
//            'longitude' => 'required',
            'commercial_record_number' => 'required',
            'is_active' => 'required',
            'commercial_record_image' => 'required|mimes:jpeg,png,jpg,gif,pdf,doc',
            'logo' => 'mimes:jpeg,png,jpg,gif',

            'customer_name' => 'required',
            'customer_email' => 'required|unique:customers,email',
            'customer_mobile' => 'required|unique:customers,mobile',
            'customer_address' => 'required',
            'customer_password' => 'required',
            'customer_government_id' => 'required',
            'customer_city_id' => 'required',
            'customer_image' => 'mimes:jpeg,png,jpg,gif',
            'customer_latitude' => 'numeric',
            'customer_longitude' => 'numeric',
//            'responsible' => 'required',
        ]);

        // Added New Corporate
        DB::beginTransaction();
        try {
            $corprate = new Corporate();
            $corprate->name = $request->input("name");
            $corprate->email = $request->input("email");
            $corprate->responsible_id = $request->input("responsible_id");
//            $corprate->field = $request->input("field");
            $corprate->order_type = $request->input("order_type");
            $corprate->other_order_type = $request->input("other_order_type");
            $corprate->shipping_type = $request->input("shipping_type");
            $corprate->mobile = $request->input("mobile");
            $corprate->phone = $request->input("phone");
            $corprate->company_website = $request->input("company_website");
            $corprate->government_id = $request->input("government_id");
            $corprate->city_id = $request->input("city_id");
            $corprate->address = $request->input("address");
            $corprate->store_id = $request->input("store_id");
//            $corprate->latitude = $request->input("latitude");
//            $corprate->longitude = $request->input("longitude");

            $corprate->transfer_method = $request->input("transfer_method");
            $corprate->bank_name = $request->input("bank_name");
            $corprate->bank_account = $request->input("bank_account");
            $corprate->mobile_transfer = $request->input("mobile_transfer");

            $corprate->commercial_record_number = $request->input("commercial_record_number");
            $corprate->is_active = $request->input("is_active");
            $corprate->notes = $request->input("notes");
            $corprate->account_type = 3;
            $counter = 0;
            if ($request->file("commercial_record_image") != "") {
                $counter++;
                $extension = File::extension($request->file("commercial_record_image")->getClientOriginalName());
                $image = 'corprate-' . time() . $counter . "." . $extension;
                $corprate->commercial_record_image = $image;
                $request->file("commercial_record_image")->move(public_path('api_uploades/client/corporate/commercial/'), $image);
            }
            if ($request->file("logo") != "") {
                $counter++;
                $extension = File::extension($request->file("logo")->getClientOriginalName());
                $image = 'logo-' . time() . $counter . "." . $extension;
                $corprate->logo = $image;
                $request->file("logo")->move(public_path('api_uploades/client/corporate/commercial/'), $image);
            }
            $corprate->save();


            $customer = new Customer();
            $customer->name = $request->input("customer_name");
            $customer->email = $request->input("customer_email");
            $customer->mobile = $request->input("customer_mobile");
            $customer->phone = $request->input("customer_phone");
            $customer->password = $request->input("customer_password");
            $customer->address = $request->input("customer_address");
            $customer->government_id = $request->input("customer_government_id");
            $customer->city_id = $request->input("customer_city_id");
            $customer->national_id = $request->input("customer_national_id");
            $customer->latitude = $request->input("customer_latitude");
            $customer->longitude = $request->input("customer_longitude");
            $customer->store_id = $request->input("customer_store_id");
            $customer->job = $request->input("customer_job");
            $customer->is_block = 0; // is_block  = 0 :: not block
            $customer->role_id = 4; // role_id   = 5 :: Corporate Customer

            $customer->corporate_id = $corprate->id;


            if ($request->hasFile('customer_image')) {
                $imgExt = $request->file('customer_image')->getClientOriginalExtension();
                $image = 'customer-' . time() . "." . $imgExt;
                $request->file("customer_image")->move(public_path('api_uploades/client/profile/'), $image);
            } else {
                $image = 'default.png';
            }
            $customer->image = $image;
            $customer->save();


            DB::commit();

            // Function Add Action In Activity Log By Samir
            addActivityLog('Add Corporate ' . $corprate->name, ' اضافة شركة ' . $corprate->name, 2); // Helper Function in App\Helpers.php

            Flash::success(__('backend.Corporate_saved_successfully'));
            return redirect()->route('mngrAdmin.corporates.index');
       } catch (Exception $e) {
            DB::rollback();
            Flash::error(__('backend.Corporate_not_Saved_Something_Error'));
            return redirect('/mngrAdmin/corporates');
        }


    }

    // View One Corporte
    public function show($id)
    {
        $user = Auth::guard($this->guardType)->user();

//        $img_url = public_path() . '\api_uploades\client\corporate\commercial\\';
        $corprate = Corporate::with(['customers' => function ($query) use ($user) {

            $query->select('customers.*')
                ->withCount(['pendingOrders', 'droppedOrders', 'acceptedOrders', 'receivedOrders', 'deliveredOrders', 'recalledOrders',
                    'rejectedOrders', 'cancelledOrders']);
            if ($user->is_warehouse_user) {
                $query->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                    ->where('user_id', $user->id);
            }

        }])->findOrFail($id);

//        $pendingCount = Order::join('customers', 'customers.id', '=', 'orders.customer_id')
//            ->where("corporate_id", $id)
//            ->where("orders.status", 0)->count();
//
//        $droppedCount = Order::join('customers', 'customers.id', '=', 'orders.customer_id')
//            ->where("corporate_id", $id)
//            ->where("orders.status", 7)->count();
//
//        $acceptedCount = Order::join('customers', 'customers.id', '=', 'orders.customer_id')
//            ->where("corporate_id", $id)
//            ->where("orders.status", 1)->count();
//
//        $receivedCount = Order::join('customers', 'customers.id', '=', 'orders.customer_id')
//            ->where("corporate_id", $id)
//            ->where("orders.status", 2)->count();
//
//        $deliveredCount = Order::join('customers', 'customers.id', '=', 'orders.customer_id')
//            ->where("corporate_id", $id)
//            ->where("orders.status", 3)->count();
//
//        $recalledCount = Order::join('customers', 'customers.id', '=', 'orders.customer_id')
//            ->where("corporate_id", $id)
//            ->where("orders.status", 5)->count();
//
//        $cancelledCount = Order::join('customers', 'customers.id', '=', 'orders.customer_id')
//            ->where("corporate_id", $id)
//            ->where("orders.status", 4)->count();

//        $c_targets = CorporateTarget::where('corporate_id', $id)->orderBy('id', 'desc')->get(); // Corporate Targets For This Corporate
        return view('backend.corporates.show', compact('corprate'));
    }

    // Edit Corporte Page
    public function edit($id)
    {
        $corprate = Corporate::findOrFail($id);
        $governments = Governorate::orderBy('name_en', 'ASC')->get();
        $cities = City::orderBy('id', 'ASC')->get();
        $types = OrderType::where('status', 1)->orderBy('id', 'asc')->get();
        $c_targets = CorporateTarget::where('corporate_id', $id)->orderBy('id', 'desc')->get(); // Corporate Targets For This Corporate

        $stores = Store::where('status', 1);
        $users = User::where('role_id', 1)->where('is_active', 1)->select('users.*');

        $user = Auth::guard($this->guardType)->user();

        if ($user->is_warehouse_user) {
            $stores = $stores->join('store_user', 'store_user.store_id', '=', 'stores.id')
                ->where('user_id', $user->id);
            $users = $users->join('store_user', 'store_user.user_id', '=', 'users.id')
                ->where('store_user.user_id', $user->id);
        }

        $stores = $stores->get();
        $users = $users->get();

        return view('backend.corporates.edit', compact('corprate', 'governments', 'cities', 'c_targets', 'types', 'stores', 'users'));
    }

    // Edit Corporte
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:corporates,email,' . $id,
            //            'field' => 'required',
            'order_type' => 'required',
            'shipping_type' => 'required',
            'mobile' => 'required|unique:corporates,mobile,' . $id,
            'government_id' => 'required',
            'city_id' => 'required',
            'address' => 'required',
//            'latitude' => 'required',
//            'longitude' => 'required',
            'commercial_record_number' => 'required',
            'is_active' => 'required',
            'commercial_record_image' => 'required_without:commercial_record_image_old|mimes:jpeg,png,jpg,gif,pdf,doc',
            // 'corporate_target_id'         => 'required',
            'logo' => 'mimes:jpeg,png,jpg,gif',
//            'responsible' => 'required',

        ]);


        $corprate = Corporate::findOrFail($id);
        $corprate->name = $request->input("name");
        $corprate->email = $request->input("email");
        $corprate->responsible_id = $request->input("responsible_id");
//        $corprate->field = $request->input("field");
        $corprate->order_type = $request->input("order_type");
        $corprate->other_order_type = $request->input("other_order_type");
        $corprate->shipping_type = $request->input("shipping_type");
        $corprate->mobile = $request->input("mobile");
        $corprate->phone = $request->input("phone");
        $corprate->company_website = $request->input("company_website");
        $corprate->government_id = $request->input("government_id");
        $corprate->city_id = $request->input("city_id");
        $corprate->address = $request->input("address");
        $corprate->store_id = $request->input("store_id");
//        $corprate->latitude = $request->input("latitude");
//        $corprate->longitude = $request->input("longitude");
        $corprate->commercial_record_number = $request->input("commercial_record_number");

        $corprate->transfer_method = $request->input("transfer_method");
        $corprate->bank_name = $request->input("bank_name");
        $corprate->bank_account = $request->input("bank_account");
        $corprate->mobile_transfer = $request->input("mobile_transfer");

        // $corprate->corporate_target_id            = $request->input("corporate_target_id");
        $corprate->is_active = $request->input("is_active");
        $corprate->notes = $request->input("notes");

        $counter = 0;

        if (!$request->logo_old) {
            $corprate->logo = '';
        }

        if (!$request->commercial_record_image_old) {
            $corprate->commercial_record_image = '';
        }

        if ($request->file("commercial_record_image") != "") {
            $counter++;
            $extension = File::extension($request->file("commercial_record_image")->getClientOriginalName());
            $image = 'corprate-' . time() . $counter . "." . $extension;
            $corprate->commercial_record_image = $image;
            $request->file("commercial_record_image")->move(public_path('api_uploades/client/corporate/commercial/'), $image);
        } elseif (!$request->input("commercial_record_image_old")) {
            flash(__('backend.Please_Enter_Commercial_Record_Image'))->error();
            return redirect()->back();
        }

        if ($request->file("logo") != "") {
            $counter++;
            $extension = File::extension($request->file("logo")->getClientOriginalName());
            $image = 'logo-' . time() . $counter . "." . $extension;
            $corprate->logo = $image;
            $request->file("logo")->move(public_path('api_uploades/client/corporate/commercial/'), $image);
        }

        $corprate->save();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Edit Corporate ' . $corprate->name, ' تعديل شركة ' . $corprate->name, 2); // Helper Function in App\Helpers.php

        Flash::warning(__('backend.Corporate_Updated_successfully'));

        return redirect()->route('mngrAdmin.corporates.index');
    }

    // Delete Corporate
    public function destroy($id)
    {
        $orders = Order::join('customers', 'orders.customer_id', '=', 'customers.id')->
        join('corporates', 'customers.corporate_id', '=', 'corporates.id')->
        where('corporates.id', '=', $id)->
        first();
        if ($orders == null) {  // If Not Found Any Orders By This Corporate
            $corporate = Corporate::findOrFail($id); // Get Corporate ID
            $customers = Customer::where('corporate_id', $corporate->id)->get(); // Get All Customer With This Corporate
            $customersIds[] = 0;
            for ($i = 0; $i < count($customers); $i++) {
                $customersIds[$i] = $customers[$i]->id;
            }

            $customersdel = Customer::whereIn('id', $customersIds)->delete(); // Delete Customers
            $corporate->delete(); // Delete Corporate

            // Function Add Action In Activity Log By Samir
            addActivityLog('Delete Corporate ' . $corporate->name, ' حذف شركة ' . $corporate->name, 2); // Helper Function in App\Helpers.php

            flash(__('backend.Corprate_deleted_successfully'))->error();

            return redirect()->route('mngrAdmin.corporates.index');
        } else {
            flash(__('backend.Can_not_delete_Corporate_because_of_Orders_dependanices'))->error();
            return redirect()->back();
        }
    }

    // get corporate details for order creation
    public function order_details($id, Request $request)
    {
        $user = Auth::guard($this->guardType)->user();

        return Corporate::with(['customers' => function ($query) use ($user) {

            $query->select('customers.id', 'corporate_id', 'name', 'mobile', 'government_id', 'city_id', 'address', 'latitude', 'longitude');
            if ($user->is_warehouse_user) {
                $query->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                    ->where('user_id', $user->id);
            }

        }])
            ->where('corporates.id', $id)
            ->select('corporates.id', 'order_type')
            ->first();
    }


    // Activeed And Not Activeed Corporate
    public function active($id)
    {
        /*
        * is_active = 0 :: Not Active
        * is_active = 1 :: Active
        */
        $corporate = Corporate::findOrFail($id);
        if (!empty($corporate)) {
            if ($corporate->is_active == 0) {
                $corporate->is_active = 1;
                // Function Add Action In Activity Log By Samir
                addActivityLog('Active Corporate ' . $corporate->name, ' تفعيل شركة ' . $corporate->name, 2); // Helper Function in App\Helpers.php
            } else {
                $corporate->is_active = 0;
                // Function Add Action In Activity Log By Samir
                addActivityLog('Unactive Corporate ' . $corporate->name, ' الغاء تفعيل شركة ' . $corporate->name, 2); // Helper Function in App\Helpers.php
            }
            $corporate->save();
            return $corporate->is_active;
        }
    }

    function change_active(Request $request)
    {
        $id = $request->id;
        $customer = Corporate::findOrFail($id);
        if ($customer != null) {
            if ($customer->is_active == 0) {
                $customer->is_active = 1;
            } else {
                $customer->is_active = 0;
            }
            $customer->save();

            return $customer->is_active;
        } else {
            return 'error';
        }

    }

    //in_slider

    function change_slider(Request $request)
    {
        $id = $request->id;
        $customer = Corporate::findOrFail($id);
        if ($customer != null) {
            if ($customer->in_slider == 0) {
                $customer->in_slider = 1;
            } else {
                $customer->in_slider = 0;
            }
            $customer->save();

            return $customer->in_slider;
        } else {
            return 'error';
        }

    }

    public function get_corporate_customers($id)
    {
        $user = Auth::guard($this->guardType)->user();

        $customers = Customer::where('corporate_id', $id);

        if ($user->is_warehouse_user) {
            $customers->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id);
        }

        return $customers->select('customers.*')->get();

    }

    public function get_user_corporates($id)
    {
//        $user = Auth::guard($this->guardType)->user();

        $corporates = Corporate::where('responsible_id', $id);

//        if ($user->is_warehouse_user) {
//            $corporates->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
//                ->where('user_id', $user->id);
//        }

        return $corporates->select('corporates.*')->get();

    }
}

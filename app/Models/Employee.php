<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    public function getImageAttribute($value)
    {
        if($value != ""){
            $value=asset("/backend/images/employees/".$value);
        }else{
          $value = "";
        }
        return $value;
    }
}

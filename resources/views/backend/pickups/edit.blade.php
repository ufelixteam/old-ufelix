@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.task_orders')}} - {{__('backend.edit')}}</title>
    <link href="{{asset('assets/select2/select2.min.css')}}" rel="stylesheet">
    <style>
        .input-group {
            -moz-box-shadow: none !important;
            -webkit-box-shadow: none !important;
            box-shadow: none !important;
        }

        .input-group[class*=col-] {
            padding-right: inherit !important;
            padding-left: inherit !important;
            float: inherit;
        }

        .pac-container {
            z-index: 9999 !important;
        }

        .select2-container {
            width: 100% !important;
            display: block;
        }

        #validateForm .form-group {
            min-height: 84px;
        }

        .form-check-inline {
            display: -webkit-inline-box;
            display: -ms-inline-flexbox;
            display: inline-flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            padding-left: 0;
            margin-right: .75rem;
        }

        .form-check-inline input {
            margin: 5px;
        }

    </style>
@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit_task')}} #{{$pickup->id}}</h3>
    </div>
@endsection

@section('content')
    @include('error')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <form action="{{ route('mngrAdmin.pickups.update', $pickup->id) }}" method="POST"
                  name="validateForm" id="validateForm">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <input type="hidden" id="sender_name" name="sender_name" value="{{$pickup->sender_name}}"/>
                <input type="hidden" id="sender_mobile-field" name="sender_mobile" value="{{$pickup->sender_mobile}}"/>
                <input type="hidden" id="s_government_id" name="s_government_id" value="{{$pickup->s_government_id}}"/>
                <input type="hidden" id="s_state_id" name="s_state_id" value="{{$pickup->s_state_id}}"/>
                <input type="hidden" id="sender_latitude" name="sender_latitude" value="{{$pickup->sender_latitude}}"/>
                <input type="hidden" id="sender_longitude" name="sender_longitude"
                       value="{{$pickup->sender_longitude}}"/>
                <input type="hidden" id="sender_address" name="sender_address" value="{{$pickup->sender_address}}"/>

                <div class="row" style="margin-bottom:15px;">

                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('corporate_id')) has-error @endif">
                        <label for="corporate_id">{{__('backend.corporate')}}</label>
                        <select class="form-control select2" id="corporate_id" name="corporate_id">
                            <option value="" data-display="Select">{{__('backend.corporate')}}</option>
                            @if(! empty($corporates))
                                @foreach($corporates as $corporate)
                                    <option value="{{$corporate->id}}"
                                            @if($pickup->corporate_id == $corporate->id) selected @endif>
                                        {{$corporate->name}}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                        @if($errors->has("corporate_id"))
                            <span class="help-block">{{ $errors->first("corporate_id") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('customer_id')) has-error @endif">
                        <label for="user_id">{{__('backend.customer')}}</label>
                        <select class="form-control select2 disabled" id="customer_id" name="customer_id">
                            <option value="" data-dispaly="Select">{{__('backend.customer')}}</option>
                            @if(! empty($customers))
                                @foreach($customers as $customer)
                                    <option value="{{$customer->id}}"
                                            @if($pickup->customer_id == $customer->id) selected @endif>{{$customer->name}}</option>
                                @endforeach
                            @endif
                        </select>
                        @if($errors->has("customer_id"))
                            <span class="help-block">{{ $errors->first("customer_id") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-3 col-sm-3">
                        <label class="control-label" for="driver_id">{{__('backend.driver')}}</label>
                        <select class="form-control select2 driver_id" id="pickup_driver_id" name="driver_id">
                            <option value="">Choose Drivers</option>
                            @if(! empty($drivers))
                                @foreach($drivers as $driver)
                                    <option
                                        value="{{$driver->id}}" @if($pickup->driver_id == $driver->id) selected @endif>
                                        {{$driver->name}}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('delivery_price')) has-error @endif">
                        <label for="delivery_price-field">Delivery Price: </label>
                        <input type="number" class="form-control" id="delivery_price-field" name="delivery_price"
                               value="{{  $pickup->delivery_price}}">
                        @if($errors->has("delivery_price"))
                            <span class="help-block">{{ $errors->first("delivery_price") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('bonus_per_order')) has-error @endif">
                        <label for="bonous-field">{{__('backend.the_bonous')}}: </label>
                        <input type="number" class="form-control" id="bonous-field" name="bonus_per_order"
                               value="{{$pickup->bonus_per_order}}">
                        @if($errors->has("bonus_per_order"))
                            <span class="help-block">{{ $errors->first("bonus_per_order") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-3 col-sm-3">
                        <label class="control-label"
                               for="captain_received_date"> {{__('backend.captain_received_date')}}:</label>

                        <div class='input-group date' id='datepickerFilter'>
                            <input type="text" id="datepicker" name="captain_received_date" class="form-control" @if(!permission('editCaptainDate')) readonly @endif
                                   value="{{$pickup->captain_received_time ? date("Y-m-d", strtotime($pickup->captain_received_time)) : ''}}"/>
                            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>

                    <div class="form-group col-md-4 col-sm-4">
                        <label class="control-label"
                               for="captain_received_date"> {{__('backend.task_type')}}:</label>

                        <div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="1"
                                       @if($pickup->is_pickup == 1) checked @endif
                                       name="is_pickup">
                                <label class="form-check-label" for="inlineCheckbox1">{{__('backend.pickup')}}</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="1"
                                       @if($pickup->is_cash_collect == 1) checked @endif
                                       name="is_cash_collect">
                                <label class="form-check-label"
                                       for="inlineCheckbox2">{{__('backend.cache_collect')}}</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="1"
                                       @if($pickup->is_recall_orders == 1) checked @endif
                                       name="is_recall_orders">
                                <label class="form-check-label"
                                       for="inlineCheckbox3">{{__('backend.recall_orders')}}</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-md-6 col-sm-6">
                        <label class="control-label"
                               for="captain_received_date"> {{__('backend.notes')}}:</label>
                        <textarea class="form-control" rows="3" style="resize: vertical;" name="notes">{{  $pickup->notes }}</textarea>
                    </div>

                    <div class="form-group col-md-6 col-sm-6">
                        <label class="control-label"
                               for="captain_received_date"> {{__('backend.task_confirm_type')}}:</label>

                        <div class="">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="confirm_pickup" value="1"
                                       name="confirm_pickup" @if($pickup->confirm_pickup == 1) checked @endif>
                                <label class="form-check-label"
                                       for="confirm_pickup">{{__('backend.pickup')}}</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-control" type="input" value="{{$pickup->pickup_orders}}" name="pickup_orders" id="pickup_orders"
                                       placeholder="{{__('backend.orders')}}">
                            </div>
                        </div>
                        <div class="">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="confirm_cash_collect" value="1"
                                       name="confirm_cash_collect" @if($pickup->confirm_cash_collect == 1) checked @endif>
                                <label class="form-check-label"
                                       for="confirm_cash_collect">{{__('backend.cache_collect')}}</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-control" type="input" value="{{$pickup->cash_money}}" name="cash_money" id="cash_money"
                                       placeholder="{{__('backend.money')}}">
                            </div>
                        </div>
                        <div class="">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="confirm_recall_orders" value="1"
                                       name="confirm_recall_orders" @if($pickup->confirm_recall_orders == 1) checked @endif>
                                <label class="form-check-label"
                                       for="confirm_recall_orders">{{__('backend.recall_orders')}}</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-control" type="input" value="{{$pickup->recall_orders}}" name="recall_orders" id="recall_orders"
                                       placeholder="{{__('backend.orders')}}">
                            </div>
                        </div>
                    </div>

                    {{--                <div class="col-md-12 col-sm-12 col-xs-12" style="border: 2px solid #607d8b;margin-bottom: 15px;">--}}
                    {{--                    <h4 style="color: #607d8b;font-weight: bold">Receiver Data</h4>--}}

                    {{--                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('store_id')) has-error @endif">--}}
                    {{--                        <label for="store_id">{{__('backend.dropped_pickup_store')}}:</label>--}}
                    {{--                        <select id="store_id" name="store_id" class="form-control">--}}
                    {{--                            <option value="">{{__('backend.Choose_Store')}}</option>--}}
                    {{--                            @if(! empty($allStores))--}}
                    {{--                                @foreach($allStores as $store)--}}
                    {{--                                    <option--}}
                    {{--                                        value="{{$store->id}}" {{ ! empty($pickup) && $store->id == $pickup->store_id ? 'selected' : '' }} >--}}
                    {{--                                        {{$store->name}}--}}
                    {{--                                    </option>--}}
                    {{--                                @endforeach--}}
                    {{--                            @endif--}}

                    {{--                        </select>--}}
                    {{--                        @if($errors->has("store_id"))--}}
                    {{--                            <span class="help-block">{{ $errors->first("store_id") }}</span>--}}
                    {{--                        @endif--}}
                    {{--                    </div>--}}

                    {{--                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('receiver_name')) has-error @endif">--}}
                    {{--                        <label for="receiver_name-field"> Name: </label>--}}
                    {{--                        <input type="text" id="receiver_name-field" name="receiver_name" class="form-control"--}}
                    {{--                               value="{{ ! empty($pickup) ? $pickup->receiver_name : old("receiver_name") }}"/>--}}
                    {{--                        @if($errors->has("receiver_name"))--}}
                    {{--                            <span class="help-block">{{ $errors->first("receiver_name") }}</span>--}}
                    {{--                        @endif--}}
                    {{--                    </div>--}}
                    {{--                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('receiver_mobile')) has-error @endif">--}}
                    {{--                        <label for="reciever_mobile-field"> Mobile: </label>--}}
                    {{--                        <input type="text" id="receiver_mobile-field" name="receiver_mobile" class="form-control"--}}
                    {{--                               value="{{ ! empty($pickup) ? $pickup->receiver_mobile : old("receiver_mobile") }}"/>--}}
                    {{--                        @if($errors->has("receiver_mobile"))--}}
                    {{--                            <span class="help-block">{{ $errors->first("receiver_mobile") }}</span>--}}
                    {{--                        @endif--}}
                    {{--                    </div>--}}

                    {{--                    <div class="form-group col-md-3 col-sm-3">--}}
                    {{--                        <label class="control-label" for="r_government_id"> Government</label>--}}

                    {{--                        <select class="form-control r_government_id" id="r_government_id" name="r_government_id">--}}
                    {{--                            <option value="" data-display="Select">Receiver Government</option>--}}
                    {{--                            @if(! empty($app_governments))--}}
                    {{--                                @foreach($app_governments as $government)--}}
                    {{--                                    <option--}}
                    {{--                                        value="{{$government->id}}" {{ ! empty($pickup) && $pickup->r_government_id  == $government->id ? 'selected':  '' }} >--}}
                    {{--                                        {{$government->name_en}}--}}
                    {{--                                    </option>--}}

                    {{--                                @endforeach--}}
                    {{--                            @endif--}}

                    {{--                        </select>--}}

                    {{--                    </div>--}}

                    {{--                    <div class="form-group col-md-2 col-sm-2">--}}
                    {{--                        <label class="control-label" for="r_state_id"> City</label>--}}

                    {{--                        <select class=" form-control r_state_id" id="r_state_id" name="r_state_id">--}}
                    {{--                            @if(! empty($r_cities))--}}

                    {{--                                @foreach($r_cities as $r_city)--}}
                    {{--                                    <option--}}
                    {{--                                        value="{{$r_city->id}}" {{ ! empty($pickup) && $pickup->r_state_id  == $r_city->id ? 'selected':  '' }} >--}}
                    {{--                                        {{$r_city->name_en}}--}}
                    {{--                                    </option>--}}

                    {{--                                @endforeach--}}
                    {{--                            @endif--}}

                    {{--                        </select>--}}

                    {{--                    </div>--}}
                    {{--                    <div--}}
                    {{--                        class="form-group input-group  col-md-2 col-sm-2 @if($errors->has('receiver_latitude')) has-error @endif">--}}
                    {{--                        <label for="receiver_latitude"> Latitude: </label>--}}
                    {{--                        <input type="text" id="receiver_latitude" name="receiver_latitude" class="form-control"--}}
                    {{--                               value="{{ ! empty($pickup) ? $pickup->receiver_latitude :  '30.0668886'  }}"/>--}}
                    {{--                        <span class="input-group-append">--}}
                    {{--                  <button class="input-group-text bg-transparent" type="button" data-toggle="modal"--}}
                    {{--                          data-target="#map1Modal"> <i class="fa fa-map-marker"></i></button>--}}
                    {{--                </span>--}}
                    {{--                        @if($errors->has("receiver_latitude"))--}}
                    {{--                            <span class="help-block">{{ $errors->first("receiver_latitude") }}</span>--}}
                    {{--                        @endif--}}
                    {{--                    </div>--}}
                    {{--                    <div class="form-group col-md-2 col-sm-2 @if($errors->has('receiver_longitude')) has-error @endif">--}}
                    {{--                        <label for="receiver_longitude"> Longitude: </label>--}}
                    {{--                        <input type="text" id="receiver_longitude" name="receiver_longitude" class="form-control"--}}
                    {{--                               value="{{ ! empty($pickup) ? $pickup->receiver_longitude :  '31.1962743'  }}"/>--}}
                    {{--                        @if($errors->has("receiver_longitude"))--}}
                    {{--                            <span class="help-block">{{ $errors->first("receiver_longitude") }}</span>--}}
                    {{--                        @endif--}}
                    {{--                    </div>--}}


                    {{--                    <div--}}
                    {{--                        class="form-group col-md-6 col-sm-6 @if($errors->has('receiver_address')) has-error @endif">--}}
                    {{--                        <label for="receiver_address">Receiver Address: </label>--}}

                    {{--                        <input type="text" id="receiver_address" name="receiver_address" class="form-control"--}}
                    {{--                               value="{{ ! empty($pickup) ? $pickup->receiver_address : old("receiver_address") }}"/>--}}


                    {{--                        @if($errors->has("receiver_address"))--}}
                    {{--                            <span class="help-block">{{ $errors->first("receiver_address") }}</span>--}}
                    {{--                        @endif--}}
                    {{--                    </div>--}}

                    {{--                </div>--}}

                    {{--                <div class="col-md-12 col-sm-12 col-xs-12" style="border: 2px solid #607d8b;margin-bottom: 15px;">--}}
                    {{--                    <h4 style="color: #607d8b;font-weight: bold">Sender Data</h4>--}}

                    {{--                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('sender_name')) has-error @endif">--}}
                    {{--                        <label for="sender_name-field">Name: </label>--}}
                    {{--                        <input type="text" id="sender_name-field" name="sender_name" class="form-control"--}}
                    {{--                               value="{{ ! empty($pickup) ? $pickup->sender_name : old("sender_name") }}"/>--}}
                    {{--                        @if($errors->has("sender_name"))--}}
                    {{--                            <span class="help-block">{{ $errors->first("sender_name") }}</span>--}}
                    {{--                        @endif--}}
                    {{--                    </div>--}}

                    {{--                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('sender_mobile')) has-error @endif">--}}
                    {{--                        <label for="sender_mobile-field">Mobile: </label>--}}
                    {{--                        <input type="text" id="sender_mobile-field" name="sender_mobile" class="form-control"--}}
                    {{--                               value="{{ ! empty($pickup) ? $pickup->sender_mobile : old("sender_mobile") }}"/>--}}
                    {{--                        @if($errors->has("sender_mobile"))--}}
                    {{--                            <span class="help-block">{{ $errors->first("sender_mobile") }}</span>--}}
                    {{--                        @endif--}}
                    {{--                    </div>--}}

                    {{--                    <div class="form-group col-md-3 col-sm-3">--}}
                    {{--                        <label class="control-label" for="s_government_id">Government</label>--}}

                    {{--                        <select class="form-control s_government_id" id="s_government_id" name="s_government_id">--}}
                    {{--                            <option value="" data-display="Select">Government</option>--}}
                    {{--                            @if(! empty($app_governments))--}}
                    {{--                                @foreach($app_governments as $government)--}}
                    {{--                                    <option--}}
                    {{--                                        value="{{$government->id}}" {{ ! empty($pickup) && $pickup->s_government_id  == $government->id ? 'selected':  '' }} >--}}
                    {{--                                        {{$government->name_en}}--}}
                    {{--                                    </option>--}}

                    {{--                                @endforeach--}}
                    {{--                            @endif--}}

                    {{--                        </select>--}}

                    {{--                    </div>--}}

                    {{--                    <div class="form-group col-md-2 col-sm-2">--}}
                    {{--                        <label class="control-label" for="s_state_id">City</label>--}}

                    {{--                        <select class=" form-control s_state_id" id="s_state_id" name="s_state_id">--}}
                    {{--                            @if(! empty($s_cities))--}}

                    {{--                                @foreach($s_cities as $s_city)--}}
                    {{--                                    <option--}}
                    {{--                                        value="{{$s_city->id}}" {{ ! empty($pickup) && $pickup->s_state_id  == $s_city->id ? 'selected':  '' }} >--}}
                    {{--                                        {{$s_city->name_en}}--}}
                    {{--                                    </option>--}}

                    {{--                                @endforeach--}}
                    {{--                            @endif--}}

                    {{--                        </select>--}}

                    {{--                    </div>--}}


                    {{--                    <div--}}
                    {{--                        class="form-group input-group  col-md-2 col-sm-2 @if($errors->has('sender_latitude')) has-error @endif">--}}
                    {{--                        <label for="sender_latitude">Latitude: </label>--}}
                    {{--                        <input type="text" id="sender_latitude" name="sender_latitude" class="form-control"--}}
                    {{--                               value="{{ ! empty($pickup) ? $pickup->sender_latitude : '30.0668886' }}"/>--}}
                    {{--                        <span class="input-group-append">--}}
                    {{--                  <button class="input-group-text bg-transparent" type="button" data-toggle="modal"--}}
                    {{--                          data-target="#map2Modal"> <i class="fa fa-map-marker"></i></button>--}}
                    {{--                </span>--}}
                    {{--                        @if($errors->has("sender_latitude"))--}}
                    {{--                            <span class="help-block">{{ $errors->first("sender_latitude") }}</span>--}}
                    {{--                        @endif--}}
                    {{--                    </div>--}}
                    {{--                    <div class="form-group col-md-2 col-sm-2 @if($errors->has('sender_longitude')) has-error @endif">--}}
                    {{--                        <label for="sender_longitude">Longitude: </label>--}}
                    {{--                        <input type="text" id="sender_longitude" name="sender_longitude" class="form-control"--}}
                    {{--                               value="{{ ! empty($pickup) ? $pickup->sender_longitude : '31.1962743' }}"/>--}}
                    {{--                        @if($errors->has("sender_longitude"))--}}
                    {{--                            <span class="help-block">{{ $errors->first("sender_longitude") }}</span>--}}
                    {{--                        @endif--}}
                    {{--                    </div>--}}

                    {{--                    <div--}}
                    {{--                        class="form-group col-md-6 col-sm-6 @if($errors->has('sender_address')) has-error @endif">--}}
                    {{--                        <label for="sender_address">Sender Address: </label>--}}
                    {{--                        <input type="text" id="sender_address" name="sender_address" class="form-control"--}}
                    {{--                               value="{{ ! empty($pickup) ? $pickup->sender_address : old("sender_address") }}"/>--}}

                    {{--                        @if($errors->has("sender_address"))--}}
                    {{--                            <span class="help-block">{{ $errors->first("sender_address") }}</span>--}}
                    {{--                        @endif--}}
                    {{--                    </div>--}}

                    {{--                </div>--}}


                    {{--                <div class="col-md-12 col-sm-12 col-xs-12" style="border: 2px solid #607d8b;margin-bottom: 15px;">--}}
                    {{--                    <h4 style="color: #607d8b;font-weight: bold">Order Data</h4>--}}

                    {{--                    <div class="form-group col-md-2 col-sm-2 @if($errors->has('agent_id')) has-error @endif">--}}
                    {{--                        <label for="agent_id-field">Agent </label>--}}

                    {{--                        <select id="agent_id-field" name="agent_id" class="form-control">--}}
                    {{--                            <option value="">Choose Agent</option>--}}
                    {{--                            @if(! empty($agents))--}}
                    {{--                                @foreach($agents as $agent)--}}
                    {{--                                    <option--}}
                    {{--                                        value="{{$agent->id}}" {{ ! empty($pickup) && $pickup->agent_id  == $agent->id ? 'selected':  '' }} >--}}
                    {{--                                        {{$agent->name}}--}}
                    {{--                                    </option>--}}
                    {{--                                @endforeach--}}
                    {{--                            @endif--}}

                    {{--                        </select>--}}
                    {{--                        @if($errors->has("agent_id"))--}}
                    {{--                            <span class="help-block">{{ $errors->first("agent_id") }}</span>--}}
                    {{--                        @endif--}}
                    {{--                    </div>--}}

                    {{--                    <div class="form-group col-md-2 col-sm-2">--}}
                    {{--                        <label class="control-label" for="driver_id">Driver</label>--}}

                    {{--                        <select class=" form-control driver_id" id="driver_id" name="driver_id">--}}
                    {{--                            <option value="">Choose Drivers</option>--}}
                    {{--                            @if(! empty($online_drivers))--}}
                    {{--                                @foreach($online_drivers as $driver)--}}
                    {{--                                    <option--}}
                    {{--                                        value="{{$driver->id}}" {{ ! empty($pickup) && $pickup->driver_id  == $driver->id ? 'selected':  '' }}>--}}
                    {{--                                        {{$driver->name}}--}}
                    {{--                                    </option>--}}
                    {{--                                @endforeach--}}
                    {{--                            @endif--}}

                    {{--                        </select>--}}

                    {{--                    </div>--}}


                    {{--                    <div class="form-group col-md-2 col-sm-3 @if($errors->has('delivery_price')) has-error @endif">--}}
                    {{--                        <label for="delivery_price-field">Delivery Price: </label>--}}
                    {{--                        <input type="text" required class="form-control" id="delivery_price-field" name="delivery_price"--}}
                    {{--                               value="{{  ! empty($pickup) ? $pickup->delivery_price : old("delivery_price") }}">--}}
                    {{--                        @if($errors->has("delivery_price"))--}}
                    {{--                            <span class="help-block">{{ $errors->first("delivery_price") }}</span>--}}
                    {{--                        @endif--}}
                    {{--                    </div>--}}

                    {{--                    <div class="form-group col-md-2 col-sm-2 @if($errors->has('bonus_per_order')) has-error @endif">--}}
                    {{--                        <label for="bonous-field">{{__('backend.the_bonous')}}: </label>--}}
                    {{--                        <input type="number" required class="form-control" id="bonous-field" name="bonus_per_order"--}}
                    {{--                               value="{{ ! empty($pickup) ? $pickup->bonus_per_order : old("bonus_per_order")}}">--}}
                    {{--                        @if($errors->has("bonus_per_order"))--}}
                    {{--                            <span class="help-block">{{ $errors->first("bonus_per_order") }}</span>--}}
                    {{--                        @endif--}}
                    {{--                    </div>--}}
                    {{--                    <div class="group-control col-md-2 col-sm-2">--}}
                    {{--                        <label class="control-label"--}}
                    {{--                               for="captain_received_date"> {{__('backend.captain_received_date')}}:</label>--}}

                    {{--                        <div class='input-group date' id='datepickerFilter'>--}}
                    {{--                            <input type="text" id="datepicker" name="captain_received_date" class="form-control"--}}
                    {{--                                   value="{{  ! empty($pickup)  && $pickup->captain_received_time != '' ? Carbon\Carbon::parse($pickup->captain_received_time)->format('Y-m-d') : ''}}"/>--}}
                    {{--                            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}


                    {{--                    <div class="group-control col-md-2 col-sm-2">--}}
                    {{--                        <label class="control-label" for="driver_id"> {{__('backend.captain_received_time')}}:</label>--}}

                    {{--                        <div class='input-group date' id='datepickerFilter'>--}}
                    {{--                            <input type="time" name="captain_received_time" class="form-control"--}}
                    {{--                                   value="{{  ! empty($pickup) && $pickup->captain_received_time != '' ? Carbon\Carbon::parse($pickup->captain_received_time)->format('H:i') : ''}}"/>--}}
                    {{--                            <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}


                    {{--                </div>--}}

                    <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
                        <button type="submit" class="btn btn-primary">{{__('backend.save_edits')}}</button>
                        <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.pickups.index') }}"><i
                                class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    @include('backend.dialog-map1')
    @include('backend.dialog-map2')

@endsection
@section('scripts')
    <script type="text/javascript" src="{{asset('/assets/scripts/map-order.js')}}"></script>

    <script
        src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key=AIzaSyDQiN4qc7pC9b1BRQwkqXXD28peMfWcHvw&callback=myMap"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    {{--    @include('backend.pickups.js')--}}
    <script src="{{asset('assets/select2/select2.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.select2').select2();

            $("form[name='validateForm']").validate({
                // Specify validation rules
                rules: {
                    corporate_id: {
                        required: true,
                    },
                    customer_id: {
                        required: true,
                    },
                    captain_received_date: {
                        required: true,
                    }
                },
                // Specify validation error messages
                messages: {
                    corporate_id: {
                        required: "{{__('backend.Please_Choose_Corporate')}}",
                    },
                    customer_id: {
                        required: "{{__('backend.Please_Choose_Customer')}}",
                    },
                    file: {
                        required: "{{__('backend.Please_Enter_Collect_Date')}}",
                    }
                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                },
                highlight: function (element) {
                    $(element).parent().addClass('error')
                },
                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                }
            });

            var drivers_bonus = {};
            var drivers_fees = {};
            @foreach($drivers as $driver)
                drivers_bonus[{{$driver->id}}] = {{$driver->bouns_of_pickup_for_one_order ? $driver->bouns_of_pickup_for_one_order : 0}};
            drivers_fees[{{$driver->id}}] = {{$driver->pickup_price ? $driver->pickup_price : 0}};
            @endforeach

            $("#pickup_driver_id").on('change', function (e) {
                const bonus = $(this).val() ? drivers_bonus[$(this).val()] : 0;
                const fees = $(this).val() ? drivers_fees[$(this).val()] : 0;
                $("#bonous-field").val(bonus);
                $("#delivery_price-field").val(fees);
            });

            $('#corporate_id').on('change', function () {
                $.ajax({
                    url: "{{ url('/mngrAdmin/get_corporate_customers/')}}" + "/" + $(this).val(),
                    type: 'get',
                    success: function (data) {
                        console.log('dataaaa', data);
                        $('#customer_id').children('option:not(:first)').remove();
                        $('#customer_id').removeClass('disabled');

                        $.each(data, function (i, content) {
                            $('#customer_id').append($("<option></option>").attr("value", content.id).text(content.name));
                        });

                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            });

            $('#customer_id').on('change', function () {
                $.ajax({
                    url: "{{ url('/mngrAdmin/customers/details/')}}" + "/" + $(this).val(),
                    type: 'get',
                    data: {},
                    success: function (data) {
                        $('#sender_name').val(data.name);
                        $('#sender_mobile-field').val(data.mobile);
                        $('#s_government_id').val(data.government_id);
                        $('#s_state_id').val(data.government_id);
                        $('#sender_address').val(data.address);
                        $('#sender_latitude').val(data.latitude);
                        $('#sender_longitude').val(data.longitude);
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            });

        });
    </script>
@endsection

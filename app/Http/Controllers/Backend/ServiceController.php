<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Service;
use Response;
use Flash;
use File;
use Auth;

class ServiceController extends Controller {



	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$title = '';
		$services = Service::orderBy('id', 'desc');

		$type             = app('request')->input('type');
		if (isset($type) && $type != -1) {
            $services = $services->where('type', $type);
        }


		if ($type == 1) {
			$title = __('backend.services');
		}elseif ($type == 2) {
			$title = __('backend.why_us');
		}elseif ($type == 3) {
			$title = __('backend.slider');
		}elseif ($type == 4) {
			$title = __('backend.drivers');
		}elseif ($type == 5) {
			$title = __('backend.customers');
		}
		$services = $services->paginate(50);

		return view('backend.services.index', compact('services','type','title'));


	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$title = "";

		$type             = app('request')->input('type');
		if ($type == 1) {
			$title = __('backend.services');
		}elseif ($type == 2) {
			$title = __('backend.why_us');
		}elseif ($type == 3) {
			$title = __('backend.slider');
		}elseif ($type == 4) {
			$title = __('backend.drivers');
		}elseif ($type == 5) {
			$title = __('backend.customers');
		}
		return view('backend.services.create',compact('type','title'));

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{

		$this->validate($request, [
            'title_ar'       => 'required|string|max:255',
            'title_en'       => 'required|string|max:255',
            'image'         => 'mimes:jpeg,jpg,png,gif'
        ]);
		$service 					= new Service();

		$service->title_ar 			= $request->input("title_ar");
        $service->title_en 			= $request->input("title_en");
        $service->description_ar 	= $request->input("description_ar");
        $service->description_en 	= $request->input("description_en");

        $service->small_description_ar 	= $request->input("small_description_ar");
        $service->small_description_en 	= $request->input("small_description_en");
        $service->is_publish 				= $request->input("is_publish");
        $service->type 				= $request->input("type");

        if ($request->file("image") != "") {
            $extension          = File::extension($request->file("image")->getClientOriginalName());
            $image              = time() . "." . $extension;
            $service->image    	= $image;
            $request->file('image')->move(public_path('images/services/'), $image);
        }

		$service->save();

        Flash::success(__('backend.Saved_service_successfully'));

		return redirect('mngrAdmin/services?type='.$service->type );

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$service = Service::findOrFail($id);

		return view('backend.services.show',compact('service'));

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$service = Service::findOrFail($id);
		$type = $service->type;
		$title = "";
		if ($type == 1) {
			$title = __('backend.services');
		}elseif ($type == 2) {
			$title = __('backend.why_us');
		}elseif ($type == 3) {
			$title = __('backend.slider');
		}elseif ($type == 4) {
			$title = __('backend.drivers');
		}elseif ($type == 5) {
			$title = __('backend.customers');
		}
        return view('backend.services.edit', compact('service','type','title'));


	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{

		$this->validate($request, [
            'title_ar'       		=> 'required|string|max:255',
            'title_en'       		=> 'required|string|max:255',
            'image'         		=> 'mimes:jpeg,jpg,png,gif'
        ]);
		$service 					= Service::findOrFail($id);

		$service->title_ar 			= $request->input("title_ar");
        $service->title_en 			= $request->input("title_en");
        $service->description_ar 	= $request->input("description_ar");
        $service->description_en 	= $request->input("description_en");

        $service->small_description_ar 	= $request->input("small_description_ar");
        $service->small_description_en 	= $request->input("small_description_en");
        $service->is_publish 				= $request->input("is_publish");

        if ($request->file("image") != "") {
            $extension          = File::extension($request->file("image")->getClientOriginalName());
            $image              = time() . "." . $extension;
            $service->image    	= $image;
            $request->file('image')->move(public_path('images/services/'), $image);
        }

		$service->save();

        Flash::warning(__('backend.Updated_service_successfully'));

		return redirect('mngrAdmin/services?type='.$service->type );

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$service = Service::findOrFail($id);
		$type = $service->type;
		$service->delete();

        flash( __('backend.Deleted_service_successfully'))->error();

		return redirect('mngrAdmin/services?type='.$type );

	}

    /*
     * change_publish
     * */
    function change_publish(Request $request){
        $id = $request->id;
        $service = Service::findOrFail($id);
        if ($service != null) {
            if( $service->is_publish == 0)
            {
                $service->is_publish = 1;
            }else{
                $service->is_publish =0;
            }
            $service->save();

            return $service->is_publish;
        }else{
            return 'error';
        }

    }

}

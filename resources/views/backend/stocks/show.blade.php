@extends('backend.layouts.app')
@section('header')
<div class="page-header">
        <h3>
            {{__('backend.Show_Stock')}} - {{$stock->title}}
        </h3>
        <form action="{{ route('mngrAdmin.stocks.destroy', $stock->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
              @if(permission('editStocks')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <a class="btn btn-warning btn-group" role="group" href="{{ route('mngrAdmin.stocks.edit', $stock->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
              @endif
              @if(permission('deleteStocks')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <button type="submit" class="btn btn-danger"> {{__('backend.delete')}} <i class="glyphicon glyphicon-trash"></i></button>
              @endif
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group col-md-3">
                     <label for="name">{{__('backend.title')}}</label>
                     <p class="form-control-static">{{$stock->title}}</p>
                </div>

                <div class="form-group col-md-3">
                     <label for="name">{{__('backend.quantity')}}</label>
                     <p class="form-control-static">{{$stock->qty}}</p>
                </div>

                <div class="form-group col-md-3">
                    <label for="name">{{__('backend.used_quantity')}}</label>
                    <p class="form-control-static">{{$stock->used_qty}}</p>
                </div>

                <div class="form-group col-md-3">
                     <label for="latitude">{{__('backend.store')}}</label>
                     <p class="form-control-static">{{ ! empty($stock->store) ? $stock->store->name : ' Not Found ' }}</p>
                </div>

                <div class="form-group col-md-3">
                     <label for="longitude">{{__('backend.corporate')}}</label>
                     <p class="form-control-static">{{ ! empty($stock->corporate) ? $stock->corporate->name : ' Not Found ' }}</p>
                </div>

                <div class="form-group col-md-6">
                     <label for="address">{{__('backend.description')}}</label>
                     <p class="form-control-static">{{$stock->description}}</p>
                </div>

            </form>
            <div class="col-md-12">

                <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.stocks.index') }}"><i class="glyphicon glyphicon-backward"></i>  {{__('backend.back')}}</a>

            </div>

        </div>
    </div>

@endsection

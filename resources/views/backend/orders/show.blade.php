@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.the_orders')}} - {{__('backend.view')}}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <style>
        .input-group {
            -moz-box-shadow: none !important;
            -webkit-box-shadow: none !important;
            box-shadow: none !important;
        }

        .input-group[class*=col-] {
            padding-right: inherit !important;
            padding-left: inherit !important;
            float: inherit;
        }

        .pac-container {
            z-index: 9999 !important;
        }
    </style>
@endsection
@section('header')
    <div class="page-header">
        <div class="pull-left">
            <h3>{{__('backend.Order_No')}} #{{$order->id}}</h3>

            {!! $order->status_span !!} Payment
            : {!! ! empty($order->payment_method ) ? '<span class="badge badge-pill label-success">'.$order->payment_method->name.'</span>' : '' !!}
            Total Price:

            {{ ! empty($order) ? $order->total_price: ''}}
            {{--        @if(! empty($order) && $order->payment_method_id == 1)--}}
            {{--            {{ ! empty($order) ?  $order->delivery_price + ($order->overload * 5) : ''}}--}}

            {{--        @elseif(! empty($order) && ($order->payment_method_id == 3 || $order->payment_method_id == 4 ))--}}
            {{--            {{ ! empty($order) ? $order->order_price + $order->delivery_price + ($order->overload * 5): ''}}--}}
            {{--        @elseif(! empty($order) && $order->payment_method_id == 6)--}}
            {{--            {{ ! empty($order) ? $order->order_price: ''}}--}}
            {{--        @elseif(! empty($order) && $order->payment_method_id == 5)--}}
            {{--            0--}}
            {{--        @endif--}}

            <br>
            <br>
            <span class="text-success"> Order Price: {{ ! empty($order) ? $order->order_price: ''}} </span> <br>
            <span class="text-primary"> Delivery Price: {{ ! empty($order) ? $order->delivery_price: ''}} </span><br>
            <span class="text-warning"> Total Price: {{ ! empty($order) ? $order->total_price: ''}} </span><br>
            {{--        @if($order->delay_at)--}}
            {{--            <span--}}
            {{--                class="text-danger">  {{__('backend.order_delayed')}}: {{date("Y-m-d", strtotime($order->delay_at))}} </span>--}}
            {{--            <br>--}}
            {{--            @if($order->delay_comment)--}}
            {{--                <span--}}
            {{--                    class="text-danger">  {{__('backend.note')}}: {{$order->delay_comment}} </span><br>--}}
            {{--            @endif--}}
            {{--        @endif--}}
        </div>
        <div class="pull-right" style="margin-top: 45px">
            @if($order->status == 2)
                <a href="{{ route('mngrAdmin.orders.recall_if_dropped', $order->id) }}"
                   class="btn btn-danger @if($order->recall_if_dropped) disabled @endif">
                    {{__('backend.recall_if_dropped')}}
                </a>
            @endif
            @if($order->status == 5 || $order->status == 8 || ($order->status == 4 && $order->status_before_cancel != 0)|| ($order->status == 3 && $order->delivery_status == 2)|| ($order->status == 3 && $order->delivery_status == 4))
                @if($order->warehouse_dropoff)
                    <span class="text-info"> {{__('backend.warehouse')}}</span>
                    <br>
                @endif
                @if($order->client_dropoff)
                    <span class="text-warning"> {{__('backend.client')}}</span>
                    <br>
                @endif
            @endif
            @if(!empty($order) && ($order->collected_cost || $order->collected_cost == 0) && in_array($order->status, [3,5,8]) )
                <span
                        class="text-success"> {{__('backend.collected_cost')}}: {{ $order->collected_cost ? $order->collected_cost : 0 }} </span>
                <br>
            @endif
            @if(!empty($order) && $order->delivery_status)
                <span class="text-primary"> {{__('backend.delivery_status')}}:
                    @if ($order->status == 3 && $order->delivery_status == 1)
                        <span class='badge badge-pill label-success'> {{__('backend.full_delivered')}} </span>
                    @elseif ($order->status == 3 && $order->delivery_status == 2)
                        <span class='badge badge-pill label-black'> {{__('backend.pr')}} </span>
                    @elseif ($order->status == 3 && $order->delivery_status == 4)
                        <span class='badge badge-pill label-info'> {{__('backend.replace')}}  </span>
                    @elseif ($order->status == 5 && $order->delivery_status == 3)
                        <span class='badge badge-pill label-danger'> {{__('backend.recall')}}  </span>
                    @elseif ($order->status == 8 && $order->delivery_status == 5)
                        <span class='badge badge-pill label-danger'> {{__('backend.reject')}}  </span>
                    @endif
            </span>
                <br>
            @endif

            @if(!empty($order) && $order->is_refund)
                <span class='badge badge-pill label-refund'>{{ __('backend.refund')}} </span>
                <br>
            @endif
            @if(!empty($order) && $order->payment_status)
                <span class="text-primary"> {{__('backend.payment_status')}}:
                    {!! $order->payment_status_span !!}
                        </span>
                <br>
                <span class="text-primary"> {{__('backend.payment_date')}}:
                    {!! $order->payment_date !!}
                    </span>
                <br>
                <span class="text-primary"> {{__('backend.paid_amount')}}:
                    {!! $order->residual !!}
                    </span>
            @endif
        </div>
        <div class="clearfix"></div>
        <div class="dropdown  pull-right">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="true">
                <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
            </button>
            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                @if ( $order->status != 3 || ($order->status == 3 && !$order->is_paid) )
                    <li>
                        <a href="{{ route('mngrAdmin.orders.edit', $order->id) }}"><i
                                    class="fa fa-edit"></i> {{__('backend.edit')}}</a>
                    </li>
                @endif
                <li>
                    <a href="{{ URL::asset('/mngrAdmin/print_pdf/'.$order->id)}}" target="_blank"
                       data-id="{{$order->id}}"><i class="fa fa-print"></i> {{__('backend.pdf')}}</a>
                </li>
                {{--                @if($order->status == 2 && isset($order->accepted->status) && $order->accepted->status != 4)--}}
                {{--                    <li>--}}
                {{--                        <a href="{{route('mngrAdmin.orders.cancelOrder', $order->id)}}"><i--}}
                {{--                                class="fa fa-remove"></i> {{__('backend.cancelled')}}</a>--}}
                {{--                    </li>--}}
                {{--                @endif--}}
                {{--                @if($order->status == 0 || $order->status == 7 )--}}
                {{--                    <li>--}}
                {{--                        <a id="forward_agent" data-toggle="modal" data-target="#forward_agentMo"><i--}}
                {{--                                class="fa fa-user-secret" aria-hidden="true"></i> {{__('backend.Change_Agent')}}</a>--}}
                {{--                    </li>--}}
                {{--                @endif--}}
                @if( ($order->status < 3  || $order->status == 6 || $order->status == 7 ) && $order->is_pickup == "0" && $order->status != 0 )
                    <li>
                        <a id="forward" data-toggle="modal" data-target="#exampleModal"> <i class="fa fa-share"
                                                                                            aria-hidden="true"></i> @if($order->status != 0 && $order->status != 7 && $order->status != 6 )  {{__('backend.Cancel_and')}} @else {{__('backend.forward')}} @endif
                        </a>
                    </li>
                @endif
                @if(in_array($order->status, [0, 1, 7]) && $order->is_pickup == "0" )
                    <li>
                        <a id="cancelOrderClient" data-id="{{$order->id}}"><i class="fa fa-times"
                                                                              aria-hidden="true"></i>{{__('backend.Cancel_Order_Client_Required')}}
                        </a>
                    </li>
                @endif

                @if(in_array($order->status, [0, 1, 7]) && $order->is_pickup == "0" )
                    <li>
                        <a id="cancelOrderByAdmin" data-id="{{$order->id}}"><i class="fa fa-times"
                                                                               aria-hidden="true"></i>{{__('backend.Cancel_Order_Admin_Required')}}
                        </a>
                    </li>
                @endif

                {{--                @if($order->status == 5 )--}}
                {{--                    <li>--}}
                {{--                        <a href="{{ url('/mngrAdmin/forward_recall/'.$order->id) }}"><i--}}
                {{--                                class="fa fa-share"></i> {{__('backend.edit_forward')}}</a>--}}
                {{--                    </li>--}}
                {{--                @endif--}}

                @if($order->status == 2 && isset($order->accepted->status) && $order->accepted->status != 4)
                    <li>
                        <a data-toggle="modal" data-target="#recallModal">
                            <i class="fa fa-history"></i> {{__('backend.recall')}}</a>
                    </li>
                @endif
                {{--href="{{route('mngrAdmin.orders.recallOrder', $order->id)}}" id="recallModal"--}}
                {{--@if($order->status != 1)--}}
                {{--<form action="{{route('mngrAdmin.orders.destroy', $order->id)}}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">--}}
                {{--<input type="hidden" name="_method" value="DELETE">--}}
                {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
                {{--<li>--}}
                {{--<a type="submit">{{__('backend.delete')}} <i class="fa fa-trash"></i></a>--}}
                {{--</li>--}}
                {{--</form>--}}
                {{--@endif--}}
                @if($order->status == 6 || $order->status == 0)

                    <li>
                        <a data-id="{{$order->id}}" id="delete-order"><i
                                    class="fa fa-trash"></i> {{__('backend.delete')}}</a>
                    </li>
                @endif

            </ul>
        </div>

    </div>

    <div class="row" style="padding: 12px 20px;">
        <div class="alert alert-danger" style="display:none"></div>
        <!-- Create at: -->
        <div class="form-group col-sm-3">
            <label for="receiver_name">{{__('backend.Create_at')}}: </label>
            <p class="form-control-static"
               style="color: #0376d9; font-weight: bold;">@if($order->created_at){{$order->created_at}} @else {{__('backend.No_Date')}} @endif </p>
        </div>

        <!-- Warehouse Dropped at: -->
        <div class="form-group col-sm-3">
            <label for="receiver_name">{{__('backend.Warehouse_Dropped_at')}}: </label>
            <p class="form-control-static"
               style="color: #0376d9; font-weight: bold;">
                @if($order->warehouse_dropped_at)
                    {{$order->warehouse_dropped_at}}
                @elseif($order->dropped_at)
                    {{$order->dropped_at}}
                @else
                    {{__('backend.No_Date')}}
                @endif
            </p>
        </div>

        <!-- Dropped at: -->
        <div class="form-group col-sm-3">
            <label for="receiver_name">{{__('backend.Dropped_at')}}: </label>
            <p class="form-control-static"
               style="color: #0376d9; font-weight: bold;">@if($order->dropped_at){{$order->dropped_at}} @else {{__('backend.No_Date')}} @endif </p>
        </div>

        <!-- Accept at: -->
        <div class="form-group col-sm-3">
            <label for="reciever_mobile"> {{__('backend.Accept_at')}}: </label>
            <p class="form-control-static" style="color: #0376d9; font-weight: bold;">
                @if( $order->status != 0 && $order->status != 7 )
                    {{$order->accepted_at}}
                @else {{__('backend.No_Date')}} @endif
            </p>
        </div>

        <!-- Receiver at: -->
        <div class="form-group col-sm-3">
            <label for="receiver_name">{{__('backend.Received_at')}}: </label>
            <p class="form-control-static" style="color: #0376d9; font-weight: bold;">
                @if( $order->status != 0  )
                    {{$order->received_at}}
                @else {{__('backend.No_Date')}}  @endif
            </p>
        </div>

        <!-- Deliver at: -->
        <div class="form-group col-sm-2">
            <label for="reciever_mobile"> {{__('backend.Deliver_at')}}: </label>
            <p class="form-control-static" style="color: #0376d9; font-weight: bold;">
                @if ($order->status != 0 )
                    {{$order->delivered_at}}
                @else {{__('backend.No_Date')}} @endif
            </p>
        </div>

        <!-- Recalled at: -->
        <div class="form-group col-sm-2">
            <label for="reciever_mobile"> {{__('backend.Recalled_at')}}: </label>
            <p class="form-control-static" style="color: #0376d9; font-weight: bold;">
                @if ($order->status != 0 )
                    {{$order->recalled_at}}
                @else {{__('backend.No_Date')}} @endif
            </p>
        </div>

        <!-- Rejected at: -->
        <div class="form-group col-sm-2">
            <label for="reciever_mobile"> {{__('backend.Rejected_at')}}: </label>
            <p class="form-control-static" style="color: #0376d9; font-weight: bold;">
                @if ($order->status != 0 )
                    {{$order->rejected_at}}
                @else {{__('backend.No_Date')}} @endif
            </p>
        </div>


    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="col-sm-12"
                 style="border: 2px solid #607d8b;color: #fff;margin-bottom: 20px;background-color: #607d8b;">
                <h4>{{__('backend.Receiver_Data')}}</h4>
            </div>
            <div class="form-group col-sm-3">
                <label for="receiver_name">{{__('backend.receiver_name')}}: </label>
                <p class="form-control-static">{{$order->receiver_name}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="reciever_mobile">{{__('backend.mobile_number')}}: </label>
                <p class="form-control-static">{{$order->receiver_mobile}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="reciever_phone">{{__('backend.mobile_number2')}}: </label>
                <p class="form-control-static">{{$order->receiver_phone}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="reciever_address">{{__('backend.Receiver_Address')}}:</label>
                <p class="form-control-static">{{$order->receiver_address}}</p>
            </div>
            <div class="form-group col-sm-12">
                <div class="row">
                    <div class="col-sm-12"><h4><strong>{{__('backend.Customer_Account')}}</strong></h4></div>
                    <div class="col-sm-3"><strong>{{__('backend.name')}}
                            : </strong>{{! empty($order->customer) ? $order->customer->name : '' }}</div>
                    <div class="col-sm-3"><strong>{{__('backend.email')}}
                            : </strong>{{! empty($order->customer) ? $order->customer->email : '' }}</div>
                    <div class="col-sm-3"><strong>{{__('backend.mobile_number')}}
                            : </strong>{{! empty($order->customer) ? $order->customer->mobile : '' }}</div>
                </div>
            </div>

            <div class="col-sm-12"
                 style="border: 2px solid #607d8b;color: #fff;margin-bottom: 20px;background-color: #607d8b;">
                <h4>{{__('backend.Sender_Data')}}</h4>
            </div>
            <div class="form-group col-sm-3">
                <label for="sender_name">{{__('backend.sender_name')}}: </label>
                <p class="form-control-static">{{$order->sender_name}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="sender_mobile">{{__('backend.mobile_number')}}: </label>
                <p class="form-control-static">{{$order->sender_mobile}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="sender_phone">{{__('backend.mobile_number2')}}:</label>
                <p class="form-control-static">{{$order->sender_phone}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="sender_address">{{__('backend.Sender_Address')}}: </label>
                <p class="form-control-static">{{$order->sender_address}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="sender_address">{{__('backend.reference_number')}}: </label>
                <p class="form-control-static">{{$order->reference_number}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="sender_address">{{__('backend.delivery_type')}}: </label>
                <p class="form-control-static">{{$order->delivery_type}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="sender_address">{{__('backend.sender_subphone')}}: </label>
                <p class="form-control-static">{{$order->sender_subphone}}</p>
            </div>

            <div class="col-sm-12"
                 style="border: 2px solid #607d8b;color: #fff;margin-bottom: 20px;background-color: #607d8b;">
                <h4>{{__('backend.Package_Data')}}</h4>
            </div>

            <div class="form-group col-sm-3">
                <label for="notes">{{__('backend.Image')}}: </label>
                <p class="form-control-static"><img src="{{! empty($order->image) ? $order->image : '#' }}"
                                                    onerror="this.src='{{asset('assets/images/image.png')}}'"
                                                    style="border-radius: 50%;width: 100px;height: 100px;"></p>
            </div>
            <div class="form-group col-sm-3">
                <label for="type_id">{{__('backend.order_type')}}: </label>
                <p class="form-control-static">{{! empty($order->type) ? $order->type : '' }}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="order_number">{{__('backend.order_number')}}: </label>
                <p class="form-control-static">{{$order->order_number}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="notes">{{__('backend.notes')}}: </label>
                <p class="form-control-static">{{! empty($order->notes) ? $order->notes : '-' }}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="order_number">{{__('backend.receiver_code')}}: </label>
                <p class="form-control-static">{{$order->receiver_code}}</p>
            </div>


            <div class="col-sm-12"
                 style="border: 2px solid #607d8b;color: #fff;margin-bottom: 20px;background-color: #607d8b;">
                <h4>{{__('backend.Driver_Data')}}</h4>
            </div>
            @if(! empty($order->accepted ) && ! empty($order->accepted->driver))
                <div class="form-group col-sm-3">
                    <label for="sender_name">{{__('backend.Driver_Name')}}: </label>
                    <p class="form-control-static">{{ ! empty($order->accepted ) && ! empty($order->accepted->driver) ? $order->accepted->driver->name : ''}}</p>
                </div>
                <div class="form-group col-sm-3">
                    <label for="sender_mobile">{{__('backend.mobile_number')}}: </label>
                    <p class="form-control-static">{{ ! empty($order->accepted ) && ! empty($order->accepted->driver) ? $order->accepted->driver->mobile : ''}}</p>
                </div>
                <div class="form-group col-sm-3">
                    <label for="email">{{__('backend.email')}}: </label>
                    <p class="form-control-static">{{! empty($order->accepted ) && ! empty($order->accepted->driver) ? $order->accepted->driver->email : ''}}</p>
                </div>
                <div class="form-group col-sm-3">
                    <label for="sender_address">{{__('backend.Driver_Address')}}: </label>
                    <p class="form-control-static">{{  ! empty($order->accepted ) && ! empty($order->accepted->driver) ? $order->accepted->driver->address : ''}}</p>
                </div>
            @else
                <div class="form-group col-sm-4 col-sm-offset-4 text-center">
                    <div class="alert alert-info" style="padding-bottom: 3px;">
                        <h4>{{__('backend.Order_is_not_Accepted_yet')}}</h4>
                    </div>
                </div>
            @endif

            <div class="col-sm-12"
                 style="border: 2px solid #607d8b;color: #fff;margin-bottom: 20px;background-color: #607d8b;">
                <h4>
                    {{__('backend.ufelix_comments')}}
                    @if(permission('addOrderComment') && $order->status > 0)
                        <button type="submit" class="btn btn-primary" style="float: right;margin-top: -7px;"
                                id="new_problem" data-toggle="modal"
                                data-target="#commentModal">
                            <i class="fa fa-plus"></i> {{__('backend.add_order_comment')}}
                        </button>
                    @endif
                </h4>

            </div>
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="table-responsive">
                    @if(count($order->comments->whereIn('user_type', [1, 3])))
                        <table id="theTable" class="table table-condensed table-striped text-center">
                            <thead>
                            <tr>
                                <th>{{__('backend.comment')}}</th>
                                <th>{{__('backend.user')}}</th>
                                <th>{{__('backend.driver')}}</th>
                                <th>{{__('backend.date')}}</th>
                                <th>{{__('backend.location')}}</th>
                                <th></th>
                            </tr>
                            </thead>

                            @foreach($order->comments->whereIn('user_type', [1, 3])->all() as $comment)
                                <tr>
                                    <td>{{$comment->comment}}</td>
                                    <td>{{!empty($comment->user) ? $comment->user->name : '-'}}</td>
                                    <td>{{!empty($comment->driver) ? $comment->driver->name : '-'}}</td>
                                    <td>{{date("Y-m-d", strtotime($comment->created_at))}}</td>
                                    <td>
                                        <a href="http://maps.google.com/?q={{$comment->latitude}},{{$comment->longitude}}"
                                           target="_blank"><i class="fa fa-map-marker"></i></a></td>
                                    <td>
                                        @if(permission('deleteOrderComment'))
                                            <form
                                                    action="{{ route('mngrAdmin.orders.delete_order_comment', $comment->id) }}"
                                                    method="POST" style="display: inline;"
                                                    onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <button type="submit" class="btn btn-xs btn-danger"><i
                                                            class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}
                                                </button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach

                            <tbody>
                            </tbody>
                        </table>
                    @else
                        <div class="form-group col-sm-4 col-sm-offset-4 text-center">
                            <div class="alert alert-info" style="padding-bottom: 3px;">
                                <h4>{{__('backend.no_order_comments')}}</h4>
                            </div>
                        </div>
                    @endif
                </div>
            </div>

            <div class="col-sm-12"
                 style="border: 2px solid #607d8b;color: #fff;margin-bottom: 20px;background-color: #607d8b;">
                <h4>
                    {{__('backend.client_comments')}}
                </h4>

            </div>
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="table-responsive">
                    @if(count($order->comments->whereIn('user_type', [2])))
                        <table id="theTable" class="table table-condensed table-striped text-center">
                            <thead>
                            <tr>
                                <th>{{__('backend.comment')}}</th>
                                <th>{{__('backend.user')}}</th>
                                <th>{{__('backend.driver')}}</th>
                                <th>{{__('backend.date')}}</th>
                                <th>{{__('backend.location')}}</th>
                                <th></th>
                            </tr>
                            </thead>

                            @foreach($order->comments->whereIn('user_type', [2])->all() as $comment)
                                <tr>
                                    <td>{{$comment->comment}}</td>
                                    <td>{{!empty($comment->user) ? $comment->user->name : '-'}}</td>
                                    <td>{{!empty($comment->driver) ? $comment->driver->name : '-'}}</td>
                                    <td>{{date("Y-m-d", strtotime($comment->created_at))}}</td>
                                    <td>
                                        <a href="http://maps.google.com/?q={{$comment->latitude}},{{$comment->longitude}}"
                                           target="_blank"><i class="fa fa-map-marker"></i></a></td>
                                    <td>
                                        @if(permission('deleteOrderComment'))
                                            <form
                                                    action="{{ route('mngrAdmin.orders.delete_order_comment', $comment->id) }}"
                                                    method="POST" style="display: inline;"
                                                    onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <button type="submit" class="btn btn-xs btn-danger"><i
                                                            class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}
                                                </button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach

                            <tbody>
                            </tbody>
                        </table>
                    @else
                        <div class="form-group col-sm-4 col-sm-offset-4 text-center">
                            <div class="alert alert-info" style="padding-bottom: 3px;">
                                <h4>{{__('backend.no_order_comments')}}</h4>
                            </div>
                        </div>
                    @endif
                </div>
            </div>

            <div class="col-sm-12"
                 style="border: 2px solid #607d8b;color: #fff;margin-bottom: 20px;background-color: #607d8b;">
                <h4>
                    {{__('backend.order_log')}}
                </h4>

            </div>
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="table-responsive">
                    @if(count($order->logs))
                        <table id="theTable" class="table table-condensed table-striped text-center">
                            <thead>
                            <tr>
                                <th>{{__('backend.subject')}}</th>
                                <th>{{__('backend.created_by')}}</th>
                                <th>{{__('backend.description')}}</th>
                                <th>{{__('backend.created_at')}}</th>
                            </tr>
                            </thead>

                            @foreach($order->logs as $log)
                                <tr>
                                    <td>{{$log->log_name}}</td>
                                    <td>{{@$log->causer->name}}</td>
                                    <td>{{$log->description}}</td>
                                    <td>{{date("Y-m-d H:i:s", strtotime($log->created_at))}}</td>
                                </tr>
                            @endforeach

                            <tbody>
                            </tbody>
                        </table>
                    @else
                        <div class="form-group col-sm-4 col-sm-offset-4 text-center">
                            <div class="alert alert-info" style="padding-bottom: 3px;">
                                <h4>{{__('backend.no_log')}}</h4>
                            </div>
                        </div>
                    @endif
                </div>
            </div>

            <div class="col-md-12 col-xs-12 col-sm-12">
                <a class="btn btn-link pull-right" href="{{URL::previous()}}"><i
                            class="fa fa-backward"></i> {{__('backend.back')}}</a>

            </div>

        </div>
    </div>

    @include('backend.orders.recall_modal')
    @include('backend.orders.driver_forward_modal')
    {{--    @include('backend.orders.agent_forward_modal')--}}
    {{--    @include('backend.orders.delivery_problem')--}}
    @include('backend.orders.order_comment')
@endsection

@section('scripts')

    <script type="text/javascript" src="{{asset('/assets/scripts/map-order.js')}}"></script>

    <script
            src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key=AIzaSyDQiN4qc7pC9b1BRQwkqXXD28peMfWcHvw&callback=myMap"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

    <script>

        $('#delay_at').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            language: 'en',
            todayHighlight: true,
            startDate: new Date()
        });

        $('#created_at').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            language: 'en',
            todayHighlight: true
        });

        $('body').on('click', '.problem_radio', function () {
            var problem_radio = $("input[name='problem_radio']:checked").val();
            console.log('hereeee in body ', problem_radio);

            if (problem_radio == 'delay') {
                console.log('hereeee in body if', problem_radio);

                $('.delay_at').removeClass('hidden');
            } else {
                console.log('hereeee in body ', problem_radio);

                $('.delay_at').addClass('hidden');
            }
        });

        $("#delivery_problem_id").on('change', function () {
            if ($(this).val() == 5) {
                $("#other_reason").show();
            } else {
                $("#reason").val("");
                $("#other_reason").hide();
            }
        });

        /*Cancel-order*/
        $("#cancelOrderByAdmin").on('click', function () {
            var id = $(this).attr('data-id');
            $.confirm({
                title: '',
                theme: 'modern',
                class: 'danger',
                content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Confirm Cancel</span></div><div style="font-weight:bold;"><p>Are You want To Cancel This Order ? According To Admin Wanted</p></div>',
                buttons: {
                    confirm: {
                        text: 'Cancel Order',
                        btnClass: 'btn-blue',
                        action: function () {
                            $.ajax({
                                url: "{{ url('/mngrAdmin/cancel_order_admin')}}",
                                type: 'post',
                                data: {'_token': "{{csrf_token()}}", 'id': id},
                                success: function (data) {
                                    jQuery('.alert-danger').html('');
                                    jQuery('.alert-danger').show();
                                    if (data != 'false') {
                                        jQuery('.alert-danger').append('<p>' + data + '</p>');
                                        // setTimeout(function () {
                                        location.reload();
                                        // }, 2000);
                                    } else {
                                        jQuery('.alert-danger').append('<p>Something Error</p>');
                                    }
                                    window.scrollTo({top: 0, behavior: 'smooth'});
                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                            });
                        }
                    },
                    cancel: {
                        text: 'Cancel',
                        action: function () {
                        }
                    }
                }
            });
        });
        $("#cancelOrderClient").on('click', function () {
            var id = $(this).attr('data-id');
            $.confirm({
                title: '',
                theme: 'modern',
                class: 'danger',
                content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Confirm Cancel</span></div><div style="font-weight:bold;"><p>Are You want To Cancel This Order ? According To Client Wanted</p></div>',
                buttons: {
                    confirm: {
                        text: 'Cancel Order',
                        btnClass: 'btn-blue',
                        action: function () {
                            $.ajax({
                                url: "{{ url('/mngrAdmin/cancel_order_client')}}",
                                type: 'post',
                                data: {'_token': "{{csrf_token()}}", 'id': id},
                                success: function (data) {
                                    jQuery('.alert-danger').html('');
                                    jQuery('.alert-danger').show();
                                    if (data != 'false') {
                                        jQuery('.alert-danger').append('<p>' + data + '</p>');
                                        // setTimeout(function () {
                                        location.reload();
                                        // }, 2000);
                                    } else {
                                        jQuery('.alert-danger').append('<p>Something Error</p>');
                                    }
                                    window.scrollTo({top: 0, behavior: 'smooth'});
                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                            });
                        }
                    },
                    cancel: {
                        text: 'Cancel',
                        action: function () {
                        }
                    }
                }
            });
        });

        $(function () {
            {{--$("#recall_by").on('change', function () {--}}
            {{--    if ($(this).val() > 1) {--}}
            {{--        $('#password_div').hide();--}}
            {{--        $('#password').attr('readonly', true);--}}
            {{--        $('#code_div').show();--}}
            {{--        $('#code').attr('readonly', false);--}}
            {{--        $("#send_div").show();--}}
            {{--    } else {--}}
            {{--        $('#password_div').show();--}}
            {{--        $('#password').attr('readonly', false);--}}

            {{--        $('#code_div').hide();--}}
            {{--        $('#code').attr('readonly', true);--}}
            {{--        $("#send_div").hide();--}}
            {{--    }--}}
            {{--});--}}
            {{--$("#send_sms").on('click', function () {--}}
            {{--    $.ajax({--}}
            {{--        url: "{{ url('/mngrAdmin/send_cofirm_recall_sms')}}",--}}
            {{--        type: 'post',--}}
            {{--        data: $("#recallForm").serialize(),--}}
            {{--        success: function (data) {--}}
            {{--            jQuery('.danger').html('');--}}
            {{--            jQuery('.danger').show();--}}
            {{--            jQuery('.success').html('');--}}
            {{--            jQuery('.success').show();--}}
            {{--            console.log(data)--}}
            {{--            if (data != 'false') {--}}
            {{--                jQuery('.success').append('<p>Sent Succefully</p>');--}}
            {{--                // setTimeout(function() {--}}
            {{--                //     location.reload();--}}
            {{--                // }, 2000);--}}
            {{--            } else {--}}
            {{--                jQuery('.danger').append('<p>Something Error</p>');--}}
            {{--            }--}}
            {{--        },--}}
            {{--        error: function (data) {--}}
            {{--            console.log('Error:', data);--}}
            {{--        }--}}
            {{--    });--}}
            {{--});--}}
            $("#send_recall").on('click', function () {
                $.ajax({
                    url: "{{ url('/mngrAdmin/orders/recallOrder')}}",
                    type: 'post',
                    data: $("#recallForm").serialize(),
                    success: function (data) {
                        jQuery('.danger').html('');
                        jQuery('.danger').show();
                        jQuery('.success').html('');
                        jQuery('.success').show();
                        console.log(data);
                        if (data != 'false') {
                            jQuery('.success').append('<p>Sent Successfully</p>');
                            // setTimeout(function () {
                            location.reload();
                            // }, 2000);
                        } else {
                            jQuery('.danger').append('<p>Something Error</p>');
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            });
            $("#delete-order").on('click', function () {
                var id = $(this).attr('data-id');
                $.confirm({
                    title: '',
                    theme: 'modern',
                    class: 'danger',
                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Confirm Delete</span></div><div style="font-weight:bold;"><p>Are You sure To Delete This Order ?</p></div>',
                    buttons: {
                        confirm: {
                            text: 'Delete Order',
                            btnClass: 'btn-danger',
                            action: function () {
                                $.ajax({
                                    url: "{{ url('/mngrAdmin/orders/')}}" + "/" + id,
                                    type: 'DELETE',
                                    data: {'_token': "{{csrf_token()}}"},
                                    success: function (data) {

                                        if (data != 'false') {

                                            // setTimeout(function () {
                                            location.reload();
                                            window.location.href = "{{ url('/mngrAdmin/orders?type=')}}" + data;
                                            // }, 2000);
                                        } else {
                                            jQuery('.alert-danger').append('<p>Something Error</p>');
                                        }
                                        window.scrollTo({top: 0, behavior: 'smooth'});
                                    },
                                    error: function (data) {
                                        console.log('Error:', data);
                                    }
                                });
                            }
                        },
                        cancel: {
                            text: 'Cancel',
                            action: function () {
                            }
                        }
                    }
                });
            })

        });

    </script>

@endsection

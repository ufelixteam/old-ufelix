<?php
namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller\Backend;
use Illuminate\Http\Request;
use App\Models\versions;
class versionsController extends Controller
{

    // Get All Versions
    public function index()
    {
      $versions = versions::paginate(15);
      return view('backend.android_varsion.index' ,compact('versions'));
    }

    // Added New Version
    public function add(Request $request)
    {
      /*
      * status = 0 :: Not Updated
      * status = 1 :: Updated
      */
      $this->validate($request,[
        'version'    =>'required',
        'platform'   =>'required',
        'status'     =>'required',
      ]);

      $versions = versions::insert([
        'version'    => $request->version,
        'platform'   => $request->platform,
        'status'     => $request->status
      ]);

      // Function Add Action In Activity Log By Samir
      addActivityLog('Add New Android Version ' . $request->version , ' اضافة اصدار اندرويد جديد ' . $request->version, 19); // Helper Function in App\Helpers.php

      return redirect('mngrAdmin/android');
    }

    // Create Android Page
    public function create()
    {
      return view('backend.android_varsion.create');
    }

    // Edit Version
    public function edit($id)
    {
      $idver = versions::findOrFail($id);
      if ($idver) {
        $versions     = versions::where('id' , $id)->first();
        $versionsall  = versions::get();
        return view('backend.android_varsion.edit' ,compact('versions','versionsall' ,'id'));
      } else {
        return redirect('mngrAdmin/android');
      }
    }

    public function uprow(Request $request)
    {
      /*
      * status = 0 :: Not Updated
      * status = 1 :: Updated
      */
      $this->validate($request,[
        'version'    =>'required',
        'platform'   =>'required',
        'status'     =>'required',
      ]);

      $versions = versions::where('id' , $request->id)->update([
        'version'   => $request->version,
        'platform'  => $request->platform,
        'status'    => $request->status
      ]);

      // Function Add Action In Activity Log By Samir
      addActivityLog('Edit Android Version ' . $request->version , ' تعديل اصدار الاندرويد ' . $request->version, 19); // Helper Function in App\Helpers.php

      return redirect('mngrAdmin/android');
    }

    // Delete Version
    public function delete(Request $request,$key)
    {
      $deletee = versions::findOrFail($key);
      $deletee->delete();

      // Function Add Action In Activity Log By Samir
      addActivityLog('Delete Android Version ' . $deletee->version , ' حذف اصدار الاندرويد ' . $deletee->version, 19); // Helper Function in App\Helpers.php

      return redirect('mngrAdmin/android');
    }

}

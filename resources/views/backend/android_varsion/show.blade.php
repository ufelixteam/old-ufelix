@extends('backend.layouts.app')
@section('css')
  <title>Pay. Methods - View</title>
@endsection
@section('header')
<div class="page-header">
        <h3>Show #{{$payment_method->id}}</h3>{!! $payment_method->status_span !!}
        <form action="{{ route('mngrAdmin.payment_methods.destroy', $payment_method->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('mngrAdmin.payment_methods.edit', $payment_method->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <div class="form-group col-sm-6">
                <label for="nome">#</label>
                <p class="form-control-static">{{$payment_method->id}}</p>
            </div>
            <div class="form-group col-sm-6">
                     <label for="name">NAME</label>
                     <p class="form-control-static">{{$payment_method->name}}</p>
                </div>

            <div class="col-md-12 col-xs-12 col-sm-12">
                <a class="btn btn-link" href="{{ route('mngrAdmin.payment_methods.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
            </div>

        </div>
    </div>

@endsection

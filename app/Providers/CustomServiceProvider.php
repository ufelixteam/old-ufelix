<?php

namespace App\Providers;

use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Support\ServiceProvider;
use View;

class CustomServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(ViewFactory $view)
    {
        $view->composer('backend.*', 'App\Http\ViewComposers\GlobalComposer');
        $view->composer('frontend.*', 'App\Http\ViewComposers\ProfileComposer');
        $view->composer('frontend.profile.layouts.header', 'App\Http\ViewComposers\ProfileNotificationsComposer');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

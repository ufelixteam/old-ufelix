@extends('frontend.layouts.app')
@section('title') @lang('website.Contact') @endsection
@section('content')
    <!-- Start DetailsContact -->
    <section class="details-contact-p">
        <div class="container">
            <div class="col-xs-12 title-cont-g">
                <h1>@lang('website.Contact')</h1>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="block-cont">
                    <div class="icon-cont">
                        <i class="fa fa-map-marker-alt"></i>
                    </div>
                    <div class="details-cont">
                        <h1>@lang('website.Address') </h1>
                        <p class="pre-wrap"> @if(! empty($app_about)  && ! empty($app_about['address'] ) )  {{ app()->getLocale() == 'en' ? $app_about['address'] : $app_about['address_ar'] }} @endif</p>


                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="block-cont">
                    <div class="icon-cont">
                        <i class="fa fa-phone"></i>
                    </div>
                    <div class="details-cont">
                        <h1>@lang('website.Mobile')</h1>
                        <p class="pre-wrap">@if(! empty($app_about)  && ! empty($app_about['telephone'] ) )  {{  $app_about['telephone']  }} @endif</p>

                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="block-cont">
                    <div class="icon-cont">
                        <i class="fa fa-envelope"></i>
                    </div>
                    <div class="details-cont">
                        <h1>@lang('website.Email')</h1>
                        <p class="pre-wrap">@if(! empty($app_about)  && ! empty($app_about['support_email'] ) )  {{  $app_about['support_email']  }} @endif</p>


                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End DetailsContact -->
    <!-- Start App1 -->
    <section class="app body-in">
        <div class="container">
            <div class="col-md-6 col-xs-12">
                @include('flash::message')

                <form action="{{url('/contact')}}" method="post" name="contactForm">
                    {{ csrf_field() }}
                    <div class="form-group @if($errors->has('name')) has-error @endif">
                        <input type="text" name="name" value="{{ old('name')}}" placeholder="@lang('website.Name')"
                               class="form-control"/>
                        <i class="fa fa-user"></i>
                        @if($errors->has("name"))
                            <span class="help-block">{{ $errors->first("name") }}</span>
                        @endif
                    </div>
                    <div class="form-group @if($errors->has('company')) has-error @endif">
                        <input type="text" name="company" value="{{ old('company')}}"
                               placeholder="@lang('website.Company')" class="form-control"/>
                        <i class="fa fa-building"></i>
                        @if($errors->has("company"))
                            <span class="help-block">{{ $errors->first("company") }}</span>
                        @endif
                    </div>
                    <div class="form-group @if($errors->has('mobile')) has-error @endif">
                        <input type="text" name="mobile" value="{{ old('mobile')}}"
                               placeholder="@lang('website.Mobile')" class="form-control"/>
                        <i class="fa fa-phone"></i>
                        @if($errors->has("mobile"))
                            <span class="help-block">{{ $errors->first("mobile") }}</span>
                        @endif
                    </div>
                    <div class="form-group @if($errors->has('email')) has-error @endif">
                        <input type="email" name="email" value="{{ old('email')}}" placeholder="@lang('website.Email')"
                               class="form-control"/>
                        <i class="fa fa-envelope"></i>
                        @if($errors->has("email"))
                            <span class="help-block">{{ $errors->first("email") }}</span>
                        @endif
                    </div>
                    <div class="form-group @if($errors->has('message')) has-error @endif">
                        <textarea name="message" value="{{ old('message')}}" placeholder="@lang('website.Message')"
                                  class="form-control">{{ old('message')}}</textarea>
                        <i class="fa fa-envelope"></i>
                        @if($errors->has("message"))
                            <span class="help-block">{{ $errors->first("message") }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! app('captcha')->display($attributes = [], $options = []) !!}
                        @if($errors->has("message"))
                            <span class="help-block">{{ $errors->first("g-recaptcha-response") }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-style" value="@lang('website.Send')"/>
                    </div>
                </form>
            </div>

            <div class="col-md-6 col-xs-12">
                <div class="title-s">
                    @lang('website.Vision')
                </div>
                <div class="text-single">
                    <p class="pre-wrap">
                        @if(! empty($app_about)  && ! empty($app_about['vision'] ) )  {{ app()->getLocale() == 'en' ? $app_about['vision'] : $app_about['vision_ar'] }} @endif
                    </p>
                </div>

            </div>
        </div>
    </section>
    <!-- End App1 -->
    <section class="map-f">
        <div class="mapf">
            <div id="map"></div>
        </div>
    </section>

@endsection


@section('scripts')
    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDlPZ7McHvDLruA0OGRUNZSI4sryV_gQxg&libraries=places"></script>

    <script src="{{asset('/assets/scripts/jquery.validate.js')}}"></script>

    <script>
        $(function () {
            $("form[name='contactForm']").validate({
                rules: {
                    name: {
                        required: true,
                    },
                    mobile: {
                        required: true,
                        number: true,
                        rangelength: [11, 11],
                    },
                    message: {
                        required: true,
                    }

                },
                // Specify validation error messages
                messages: {
                    name: {
                        required: "{{__('website.ContactNameRequired')}}",
                    },
                    mobile: {
                        required: "{{__('website.ContactMobileRequired')}}",
                        number: "{{__('website.Please_Enter_Avalid_Number')}}",
                        rangelength: "{{__('website.Mobile_Must_Be11_Digits')}}",
                    },
                    message: {
                        required: "{{__('website.ContactMessageRequire')}}",
                    }
                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                    $(element).parent().addClass('error');
                    $(element).parent().find('label').addClass(' help-block');

                },
                highlight: function (element) {
                    $(element).parent().addClass('error')
                    $(element).parent().find('label').addClass(' help-block');

                },
                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                    $(element).parent().find('label').addClass(' help-block');

                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                }
            });

            var markers = [
                ['1', 27.1770325, 31.1669084],
                ['2', 26.5604565, 31.6630057],
                ['3', 25.694937, 32.6244472]
            ];

            function initialize() {

                var center = {lat: 26.8348758, lng: 26.37987},
                    map = new google.maps.Map(document.getElementById('map'), {
                        disableDefaultUI: true,
                        center: center,
                        zoom: 20
                    });

                var Markers = [];

                var iconNormal = 'https://webdesign.danols.com/static/template/images/icons/light/pin_map_icon&48.png',
                    iconSelected = 'https://webdesign.danols.com/static/template/images/icons/light/pin_map_icon&48.png',
                    bounds = new google.maps.LatLngBounds();

                function setMarkers(map) {
                    for (var i = 0; i < markers.length; i++) {
                        var marker = markers[i],
                            myLatLng = new google.maps.LatLng(marker[1], marker[2]),
                            eachMarker = new google.maps.Marker({
                                record_id: i,
                                position: myLatLng,
                                map: map,
                                animation: google.maps.Animation.DROP,
                                icon: iconNormal,
                                title: marker[0]
                            });
                        //var selectedMarker;
                        bounds.extend(myLatLng);
                        Markers.push(eachMarker);


                        function changeMarker(record_id) {
                            for (i in Markers) {
                                Markers[i].setIcon(iconNormal);
                                Markers[record_id].setIcon(iconSelected);
                            }
                        }
                    }
                }

                map.fitBounds(bounds);
                setMarkers(map);

            }

            google.maps.event.addDomListener(window, 'load', initialize);

        });

    </script>
@endsection

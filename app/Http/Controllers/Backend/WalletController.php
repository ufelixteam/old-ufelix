<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use App\Http\Requests;
use App\Models\Agent;
use App\Models\Corporate;
use App\Models\Driver;
use App\Models\Wallet;
use App\Models\WalletLog;
use Auth;
use File;
use Flash;
use Illuminate\Http\Request;
use Response;

class WalletController extends Controller
{

    private $guardType;

    public function __construct(Request $request)
    {
        if ($request->is('mngrAdmin/*') || $request->is('admin/*')) {
            \Config::set('auth.defaults.guard', 'admin');

        } elseif ($request->is('mngrAgent/*') || $request->is('agent/*')) {
            \Config::set('auth.defaults.guard', 'agent');
        }
        $this->guardType = \Config::get('auth.defaults.guard');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function profilewallet()
    {
        $id = auth()->guard('Customer')->user()->corporate->id;
        $wallet = Wallet::where('object_id', $id)->where('type', 2)->first();
        return view('profile.wallet', compact('wallet'));
    }

    public function check_user_wallet()
    {
        $object_id = \Request::get("object_id");
        $type = \Request::get("type");
        $id = \Request::get("id");
        $wallet = Wallet::where('object_id', $object_id)->where('type', '=', $type)->first();

        if ($wallet) {
            echo 'false';
        } else {
            echo 'true';
        }
    }


    public function index(Request $request)
    {
        $user = Auth::guard($this->guardType)->user();
        $type = app('request')->input('type');

        $header = "";
        if ($type == 1) {
            $header = __('backend.drivers');

        } elseif ($type == 2) {
            $header = __('backend.corporates');

        } elseif ($type == 3) {
            $header = __('backend.agents');
        }
        $wallets = Wallet::orderBy('id', 'desc')->paginate('15');

        $objects = null;

        if (isset($type) && in_array($type, array(1, 2, 3))) {
            $wallets = Wallet::where('type', $type)->orderBy('wallets.id', 'desc');
            if ($type == 1) {
                $objects = Driver::where('is_active', 1)->select('drivers.*');
            } elseif ($type == 2) {
                $objects = Corporate::where('is_active', 1)->select('corporates.*')
                    ->orderBy('corporates.name', 'ASC');
            }
            if ($user->is_warehouse_user) {
                if ($type == 1) {
                    $objects = $objects->join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                        ->where('user_id', $user->id);
                    $wallets = $wallets->join('drivers', 'drivers.id', '=', 'wallets.object_id')
                        ->join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                        ->where('user_id', $user->id);
                } elseif ($type == 2) {
                    $objects = $objects->join('customers', 'customers.corporate_id', '=', 'corporates.id')
                        ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                        ->where('user_id', $user->id);
                    $wallets = $wallets->join('corporates', 'corporates.id', '=', 'wallets.object_id')
                        ->join('store_user', 'store_user.store_id', '=', 'corporates.store_id')
                        ->where('user_id', $user->id);
                }
            }

            $objects = $objects->get();

            $object_id = $request->object_id;
            if (isset($object_id) && $object_id != "") {
                $wallets = $wallets->where('object_id', $object_id);
            }

            $wallets = $wallets->paginate('15');
        }

        if ($request->ajax()) {
            return [
                'view' => view('backend.wallets.table', compact('wallets', 'header', 'type', 'objects'))->render(),
            ];
        }

        return view('backend.wallets.index', compact('wallets', 'header', 'type', 'objects'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $type = app('request')->input('type');
        $user = Auth::guard($this->guardType)->user();
        if ($type == 1) {
            $header = 'Drivers';
            $users = Driver::select('drivers.*')->orderBy('drivers.id', 'desc');

            if ($user->is_warehouse_user) {

                $users = $users->join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                    ->where('user_id', $user->id);
            }

            $users = $users->get();

        } elseif ($type == 2) {
            $header = __('backend.corporates');
            $users = Corporate::select('corporates.*')->orderBy('corporates.id', 'desc');

            if ($user->is_warehouse_user) {

                $users = $users->join('customers', 'customers.corporate_id', '=', 'corporates.id')
                    ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                    ->where('user_id', $user->id);
            }

            $users = $users->get();
        } elseif ($type == 3) {
            $header = __('backend.agents');
            $users = Agent::orderBy('id', 'desc')->get();
        }
        return view('backend.wallets.create', compact('type', 'users', 'header'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $type = $request->input("type");

        if ($type == 1) {
            $user = Driver::orderBy('id', 'desc')->first();
        } elseif ($type == 2) {
            $user = Corporate::orderBy('id', 'desc')->first();
        } elseif ($type == 3) {
            $user = Agent::orderBy('id', 'desc')->first();
        }

        if ($user == null) {

            if ($type == 1) {
                flash(__('backend.Wallet_driver_not_found'))->error();
            } elseif ($type == 2) {
                flash(__('backend.Wallet_corporate_not_found'))->error();
            } elseif ($type == 3) {
                flash(__('backend.Wallet_agent_not_found'))->error();
            }
            return redirect('/mngrAdmin/wallets/create?type=' . $type);
        } else {
            $wallet = new Wallet();
            $wallet->value = $request->input("value");
            $wallet->object_id = $request->input("object_id");
            $wallet->type = $request->input("type");
            $wallet->status = $request->input("status");

            $wallet->save();

            if ($type == 1) {
                $driverName = Driver::where('id', $wallet->object_id)->value('name');
                // Function Add Action In Activity Log By Samir
                addActivityLog('Add Wallet For Driver ' . $driverName, ' اضافة محفظة للكابتن ' . $driverName, 5); // Helper Function in App\Helpers.php
            } elseif ($type == 2) {
                $corporateName = Corporate::where('id', $wallet->object_id)->value('name');
                // Function Add Action In Activity Log By Samir
                addActivityLog('Add Wallet For Corporate ' . $corporateName, ' اضافة محفظة لشركة ' . $corporateName, 5); // Helper Function in App\Helpers.php
            } elseif ($type == 3) {
                $agentName = Agent::where('id', $wallet->object_id)->value('name');
                // Function Add Action In Activity Log By Samir
                addActivityLog('Add Wallet For Agent ' . $agentName, ' اضافة محفظة للوكيل ' . $agentName, 5); // Helper Function in App\Helpers.php
            }

            Flash::success(__('backend.Wallet_saved_successfully'));
            return redirect('/mngrAdmin/wallets?type=' . $type);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $wallet = Wallet::findOrFail($id);
        $user = null;
        if (!empty($wallet)) {
            $type = $wallet->type;

            if ($type == 1) {
                $user = Driver::where('id', $wallet->object_id)->first();
            } elseif ($type == 2) {
                $user = Corporate::where('id', $wallet->object_id)->first();
            } elseif ($type == 3) {
                $user = Agent::where('id', $wallet->object_id)->first();
            }

            // dd($user);die();
            if ($user == null) {

                if ($type == 1) {
                    flash(__('backend.Wallet_driver_not_found'))->error();
                } elseif ($type == 2) {
                    flash(__('backend.Wallet_corporate_not_found'))->error();
                } elseif ($type == 3) {
                    flash(__('backend.Wallet_agent_not_found'))->error();
                }
                return redirect('/mngrAdmin/wallets?type=' . $type);
            }

            $wallet->logs = WalletLog::where('wallet_id', $wallet->id)->orderBy('created_at', 'DESC')->paginate(50);
            return view('backend.wallets.show', compact('wallet'));
        } else {
            flash(__('backend.Wallet_does_not_deleted_Something_error'))->error();

            return redirect()->back();

        }

        return view('backend.wallets.show', compact('wallet'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $wallet = Wallet::findOrFail($id);
        $type = app('request')->input('type');

        if (!empty($wallet)) {
            $user = Auth::guard($this->guardType)->user();
            if ($type == 1) {
                $header = 'Drivers';
                $users = Driver::select('drivers.*')->orderBy('drivers.id', 'desc');

                if ($user->is_warehouse_user) {

                    $users = $users->join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                        ->where('user_id', $user->id);
                }

                $users = $users->get();
            } elseif ($type == 2) {
                $header = 'Corporates';
                $users = Corporate::select('corporates.*')->orderBy('corporates.id', 'desc');

                if ($user->is_warehouse_user) {

                    $users = $users->join('customers', 'customers.corporate_id', '=', 'corporates.id')
                        ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                        ->where('user_id', $user->id);
                }

                $users = $users->get();

            } elseif ($type == 3) {
                $header = 'Agents';
                $users = Agent::orderBy('id', 'desc')->get();
            }
            return view('backend.wallets.edit', compact('wallet', 'type', 'users', 'header'));
        } else {

            flash(__('backend.Something_error_Wallet_does_not_exist'))->error();

            return redirect('/mngrAdmin/wallets?type=' . $type);

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @param Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $wallet = Wallet::findOrFail($id);
        if (!empty($wallet)) {

            $wallet->value = $request->input("value");
            $wallet->status = $request->input("status");
            $wallet->save();
            Flash::warning(__('backend.Wallet_Updated_successfully'));
            return redirect('/mngrAdmin/wallets?type=' . $wallet->type);

        } else {
            flash(__('backend.Something_error_Wallet_does_not_exist'))->error();
            return redirect()->back();


        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {

        $type = app('request')->input('type');
        $wallet = Wallet::findOrFail($id);
        $type = $wallet->type;
        if (!empty($wallet)) {
            $wallet_logs = WalletLog::where('wallet_id', $id)->get();
            if (count($wallet_logs) > 0) {
                flash(__('backend.Wallet_does_not_deleted_Wallet_have_some_operation'))->error();
                return redirect('mngrAdmin/wallets/' . $id . '?type' . $type);
            }
            $wallet->delete();

            if ($type == 1) {
                $driverName = Driver::where('id', $wallet->object_id)->value('name');
                // Function Add Action In Activity Log By Samir
                addActivityLog('Delete Wallet Driver ' . $driverName, ' حذف محفظة الكابتن ' . $driverName, 5); // Helper Function in App\Helpers.php
            } elseif ($type == 2) {
                $corporateName = Corporate::where('id', $wallet->object_id)->value('name');
                // Function Add Action In Activity Log By Samir
                addActivityLog('Delete Wallet Corporate ' . $corporateName, ' حذف محفظة شركة ' . $corporateName, 5); // Helper Function in App\Helpers.php
            } elseif ($type == 3) {
                $agentName = Agent::where('id', $wallet->object_id)->value('name');
                // Function Add Action In Activity Log By Samir
                addActivityLog('Delete Wallet Agent ' . $agentName, ' حذف محفظة الوكيل ' . $agentName, 5); // Helper Function in App\Helpers.php
            }

            flash(__('backend.Wallet_deleted_successfully'))->error();

        } else {
            flash(__('backend.Wallet_does_not_deleted_Something_error'))->error();

        }

        return redirect('mngrAdmin/wallets?type=' . $type);
    }


    # add money to wallet
    public function add_wallet(Request $request)
    {

        $type = $request->input("type");
        $wallet = Wallet::where('id', $request->input("wallet_id"))->first();

        if ($wallet && $request->input("value") > 0) {
            $log = new WalletLog();
            $log->value = $request->input("value");
            $log->wallet_id = $request->input("wallet_id");
//            $log->type = 2;
            $log->reason_en = $request->input("reason");
            $log->reason_ar = $request->input("reason");
            $log->save();

            if ($type == 1) {
                $driverName = Driver::where('id', $wallet->object_id)->value('name');
                // Function Add Action In Activity Log By Samir
                addActivityLog('Add Amount ' . $log->value . ' EL To Wallet Driver ' . $driverName, ' اضافة مبلغ ' . $log->value . ' جنية داخل محفظة الكابتن ' . $driverName, 5); // Helper Function in App\Helpers.php
            } elseif ($type == 2) {
                $corporateName = Corporate::where('id', $wallet->object_id)->value('name');
                // Function Add Action In Activity Log By Samir
                addActivityLog('Add Amount ' . $log->value . ' EL To Wallet Corporate ' . $corporateName, ' اضافة مبلغ ' . $log->value . ' جنية داخل محفظة شركة ' . $corporateName, 5); // Helper Function in App\Helpers.php
            } elseif ($type == 3) {
                $agentName = Agent::where('id', $wallet->object_id)->value('name');
                // Function Add Action In Activity Log By Samir
                addActivityLog('Add Amount ' . $log->value . ' EL To Wallet Agent ' . $agentName, ' اضافة مبلغ ' . $log->value . ' جنية داخل محفظة الوكيل ' . $agentName, 5); // Helper Function in App\Helpers.php
            }

            Flash::success(__('backend.Money_added_To_Wallet_successfully'));
            return redirect('/mngrAdmin/wallets/' . $wallet->id);

        } else {
            flash(__('backend.Wallet_does_not_exist_Something_error'))->error();
            return redirect('mngrAdmin/wallets?type=' . $type);
        }
    }


    public function sub_wallet(Request $request)
    {

        $type = $request->input("type");
        $wallet = Wallet::where('id', $request->input("wallet_id"))->first();

        if ($wallet) {
            $log = new WalletLog();
            $log->value = $request->input("value") * -1;
            $log->wallet_id = $request->input("wallet_id");
//            $log->type = 2;
            $log->reason_en = $request->input("reason");
            $log->reason_ar = $request->input("reason");
            $log->save();
            if ($type == 1) {
                $driverName = Driver::where('id', $wallet->object_id)->value('name');
                // Function Add Action In Activity Log By Samir
                addActivityLog('Withdrawal Amount ' . $log->value . ' EL To Wallet Driver ' . $driverName, ' سحب مبلغ ' . $log->value . ' جنية من محفظة الكابتن ' . $driverName, 5); // Helper Function in App\Helpers.php
            } elseif ($type == 2) {
                $corporateName = Corporate::where('id', $wallet->object_id)->value('name');
                // Function Add Action In Activity Log By Samir
                addActivityLog('Withdrawal Amount ' . $log->value . ' EL To Wallet Corporate ' . $corporateName, ' سحب مبلغ ' . $log->value . ' جنية من محفظة شركة ' . $corporateName, 5); // Helper Function in App\Helpers.php
            } elseif ($type == 3) {
                $agentName = Agent::where('id', $wallet->object_id)->value('name');
                // Function Add Action In Activity Log By Samir
                addActivityLog('Withdrawal Amount ' . $log->value . ' EL To Wallet Agent ' . $agentName, ' سحب مبلغ ' . $log->value . ' جنية من محفظة الوكيل ' . $agentName, 5); // Helper Function in App\Helpers.php
            }

            Flash::success(__('backend.Money_withdrawal_To_Wallet_successfully'));
            return redirect('/mngrAdmin/wallets/' . $wallet->id);

        } else {
            flash(__('backend.Wallet_does_not_exist_Something_error'))->error();

            return redirect('mngrAdmin/wallets?type=' . $type);

        }

    }


    public function close_wallet(Request $request)
    {
        $wallet = Wallet::where('id', $request->wallet_id)->first();
        if ($wallet) {
            if ($wallet->status == 0) {
                $wallet->status = 1;

                if ($wallet->type == 1) {
                    $driverName = Driver::where('id', $wallet->object_id)->value('name');
                    // Function Add Action In Activity Log By Samir
                    addActivityLog('Open Wallet Driver ' . $driverName, ' فتح محفظة الكابتن ' . $driverName, 5); // Helper Function in App\Helpers.php
                } elseif ($wallet->type == 2) {
                    $corporateName = Corporate::where('id', $wallet->object_id)->value('name');
                    // Function Add Action In Activity Log By Samir
                    addActivityLog('Open Wallet Corporate ' . $corporateName, ' فتح محفظة شركة ' . $corporateName, 5); // Helper Function in App\Helpers.php
                } elseif ($wallet->type == 3) {
                    $agentName = Agent::where('id', $wallet->object_id)->value('name');
                    // Function Add Action In Activity Log By Samir
                    addActivityLog('Open Wallet Agent ' . $agentName, ' فتح محفظة الوكيل ' . $agentName, 5); // Helper Function in App\Helpers.php
                }

            } else {
                $wallet->status = 0;

                if ($wallet->type == 1) {
                    $driverName = Driver::where('id', $wallet->object_id)->value('name');
                    // Function Add Action In Activity Log By Samir
                    addActivityLog('Close Wallet Driver ' . $driverName, 'اغلاق محفظة الكابتن ' . $driverName, 5); // Helper Function in App\Helpers.php
                } elseif ($wallet->type == 2) {
                    $corporateName = Corporate::where('id', $wallet->object_id)->value('name');
                    // Function Add Action In Activity Log By Samir
                    addActivityLog('Close Wallet Corporate ' . $corporateName, 'اغلاق محفظة شركة ' . $corporateName, 5); // Helper Function in App\Helpers.php
                } elseif ($wallet->type == 3) {
                    $agentName = Agent::where('id', $wallet->object_id)->value('name');
                    // Function Add Action In Activity Log By Samir
                    addActivityLog('Close Wallet Agent ' . $agentName, 'اغلاق محفظة الوكيل ' . $agentName, 5); // Helper Function in App\Helpers.php
                }

            }
            $wallet->save();
            return $wallet->status;
        } else {
            return 'error';
        }
    }

}

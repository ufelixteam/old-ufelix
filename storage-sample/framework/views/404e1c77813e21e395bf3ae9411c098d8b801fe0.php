<?php $__env->startSection('css'); ?>
  <title><?php echo e(__('backend.the_orders')); ?> - <?php echo e(__('backend.add_order')); ?></title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('header'); ?>
  <div class="page-header">
    <h4>
      <i class="glyphicon glyphicon-plus"></i> <?php echo e(__('backend.Create_Orders_Collection')); ?></h4>
  </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

  <div class="row" >
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="alert alert-danger" style="display:none"></div>

      </div>
      <div id="create-form" class="create-form">
      <?php echo $__env->make('backend.orders.create-form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    </div>

    <div id="js-form" class="js-form">
    </div>

  </div>
          <?php echo $__env->make('backend.dialog-map1', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
          <?php echo $__env->make('backend.dialog-map2', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

  <div class="row" style="margin-top:30px;">
    <div class="col-md-12 col-sm-12 col-xs-12">
       <div class="alert alert-success" style="display:none"></div>
    <div class="table-responsive">

        <div class="list">
          <?php echo $__env->make('backend.orders.tmptable', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        </div>
      </div>
  </div>
  </div>



<div class="row" style="margin-top:20px;">
  <div  class="col-md-12 col-sm-12 col-xs-12"  >

      <form action="<?php echo e(URL::asset('mngrAdmin/save_collection')); ?>" method="post" >
        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
        <input type="hidden" name="collection_id" id="saved_collection_id" value="<?php echo e(! empty($collection_id) ? $collection_id : '0'); ?>">
        
        <button type="submit" class="btn btn-success btn-block" id="saved" <?php if(! ( ! empty($collection_id) && ! empty($orders) ) ): ?> style="display: none;"   <?php endif; ?>>Save Collection To Orders</button>

      </form>

  </div>
  <div class="col-md-12 col-sm-12 col-xs-12">


  <a class="btn btn-link pull-right" href="<?php echo e(route('mngrAdmin.orders.index')); ?>"><i class="glyphicon glyphicon-backward"></i> <?php echo e(__('backend.back')); ?></a>
  </div>
</div>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('scripts'); ?>
<script type="text/javascript" src="<?php echo e(asset('/assets/scripts/map-order.js')); ?>"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQiN4qc7pC9b1BRQwkqXXD28peMfWcHvw&&callback=myMap"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<?php echo $__env->make('backend.orders.js', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/orders/create.blade.php ENDPATH**/ ?>
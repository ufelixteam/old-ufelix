
<?php $__env->startSection('css'); ?>
  <title><?php echo e(__('backend.corporates')); ?></title>
  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <style>
        .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20px; }
        .toggle.ios .toggle-handle { border-radius: 20px; }
    
    @media  only screen and (max-width: 580px) {
    .hide-td{
        display: none;
    }
    }
    .box-body {
         min-height: 100px;
    }
    .stylish-input-group .input-group-addon {
        background: white !important;
    }
    .stylish-input-group .form-control {
        border-right:0;
        box-shadow:0 0 0;
        border-color:#ccc;
    }
    .stylish-input-group button {
        border:0;
        background:transparent;
    }
  </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('header'); ?>
<div class="page-header clearfix">
  <h3>
    <i class="glyphicon glyphicon-align-justify"></i> <?php echo e(__('backend.corporates')); ?>

    <?php if(permission('addCorporate')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
      <a class="btn btn-success pull-right" href="<?php echo e(route('mngrAdmin.corporates.create')); ?>"><i class="glyphicon glyphicon-plus"></i> <?php echo e(__('backend.add_corporate')); ?></a>
    <?php endif; ?>
  </h3>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="row" style="margin-bottom:15px;">
  <div class="col-md-12">
    <div class="col-md-6 col-sm-6">
      <form action="<?php echo e(URL::asset('/mngrAdmin/corporates')); ?>" method="get" id="search-form" />
       	<div id="imaginary_container">
          <div class="input-group stylish-input-group">
            <input type="text" class="form-control" id="search-field"  name="search" placeholder="<?php echo e(__('backend.search_by_name_or_mobile')); ?>" >
              <span class="input-group-addon">
                <button type="button" id="search-btn1">
                  <span class="glyphicon glyphicon-search"></span>
                </button>
              </span>
            </div>
          </div>
   	  </form>
    </div>
    <div class="group-control col-md-6 col-sm-6">
       <select id="active-field" name="active" class="form-control">
           <option value="-1"><?php echo e(__('backend.sort_by_active')); ?></option>
           <option value="1"><?php echo e(__('backend.active')); ?></option>
           <option value="0"><?php echo e(__('backend.not_activated')); ?></option>
       </select>
    </div>
    </div>
</div>

<div class= "list">
  <?php echo $__env->make('backend.corporates.table', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<script>


$('.toggle').bootstrapToggle();

  $("#search-btn1").on('click', function() {
    // Wait icon
    $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
    $.ajax({
        url: '<?php echo e(URL::asset("/mngrAdmin/corporates")); ?>'+'?search='+$(this).val()+'&&active='+$("#active-field").val(),
        type: 'get',
        data: $("#search-form").serialize(),
        success: function(data) {
            $('.list').html(data.view);
            // $('#theTable').DataTable({
            //   "pagingType": "full_numbers"
            // });
        },
        error: function(data) {
            console.log('Error:', data);
        }
    });
  });

  $("#active-field").on('change', function() {
  	$('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
      $.ajax({
          url: '<?php echo e(URL::asset("/mngrAdmin/corporates")); ?>'+'?active='+$("#active-field").val()+'&&search='+$("#search-field").val(),
          type: 'get',
          success: function(data) {
              $('.list').html(data.view);
              // $('#theTable').DataTable({
              //   "pagingType": "full_numbers"
              // });
          },
          error: function(data) {
              console.log('Error:', data);
          }
      });
  });

  $('.change_active').on('change',function(){
    let id = $(this).attr('data-id');
    $.ajax({
        url: '<?php echo e(URL::asset("/mngrAdmin/change-active-corporate")); ?>',
        type: 'post',
        data:{id:id,_token:"<?php echo e(csrf_token()); ?>"},
        success: function(data) {

        },
        error: function(data) {
            console.log('Error:', data);
        }
    });
  });

   $('.change_slider').on('change',function(){
    let id = $(this).attr('data-id');
    $.ajax({
        url: '<?php echo e(URL::asset("/mngrAdmin/change-slider-corporate")); ?>',
        type: 'post',
        data:{id:id,_token:"<?php echo e(csrf_token()); ?>"},
        success: function(data) {

        },
        error: function(data) {
            console.log('Error:', data);
        }
    });
  });



</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/corporates/index.blade.php ENDPATH**/ ?>
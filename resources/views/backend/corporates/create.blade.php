@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.corporates')}} - {{__('backend.add_corporate')}}</title>
    <link href="{{asset('assets/select2/select2.min.css')}}" rel="stylesheet">
    <style>
        .select2-container {
            width: 100% !important;
            display: block;
        }

        .error_user {
            display: inline-block;
            color: rgb(255, 255, 255);
            background-color: #9c3737;
            width: 100%;
            padding: 5px;
            margin-top: 3px;
            border-radius: 2px;
            text-align: center;
            font-weight: 500;
        }

        .form-group {
            min-height: 94px;
            margin-bottom: 0px;
        }

        .input-group {
            width: 100%;
        }

        .pac-container {
            z-index: 9999 !important;
        }
    </style>
@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-plus"></i>{{__('backend.corporates')}} / {{__('backend.add_corporate')}}</h3>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <form action="{{ route('mngrAdmin.corporates.store') }}" method="POST" name="corporate_create"
                  enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('name')) has-error @endif">
                        <label for="name-field">{{__('backend.name')}}: </label>
                        <input type="text" id="name-field" name="name" class="form-control" value="{{old("name")}}"/>
                        @if($errors->has("name"))
                            <span class="help-block">{{ $errors->first("name") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('email')) has-error @endif">
                        <label for="email-field">{{__('backend.email')}}: </label>
                        <input type="text" id="email-field" name="email" class="form-control" value="{{old("email")}}"/>
                        @if($errors->has("email"))
                            <span class="help-block">{{ $errors->first("email") }}</span>
                        @endif
                    </div>

                    {{--                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('field')) has-error @endif">--}}
                    {{--                        <label for="corporate-work-field">{{__('backend.corporate_work_field')}}: </label>--}}
                    {{--                        <input type="text" id="corporate-work-field" name="field" class="form-control"--}}
                    {{--                               value="{{old("field")}}"/>--}}
                    {{--                        @if($errors->has("field"))--}}
                    {{--                            <span class="help-block">{{ $errors->first("field") }}</span>--}}
                    {{--                        @endif--}}
                    {{--                    </div>--}}

                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('responsible')) has-error @endif">
                        <label for="company_website-field">{{__('backend.responsible')}}: </label>
                        <input type="text" id="responsible-field" name="responsible" class="form-control"
                               value="{{old("responsible")}}"/>
                        @if($errors->has("responsible"))
                            <span class="help-block">{{ $errors->first("responsible") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('responsible_id')) has-error @endif">
                        <label for="responsible_id">{{__('backend.responsible')}}: </label>
                        {{--                    <input type="text" id="responsible-field" name="responsible" class="form-control"--}}
                        {{--                           value="{{ !empty($corprate->responsible)?$corprate->responsible:old('responsible') }}"/>--}}
                        <select class=" form-control" id="responsible_id" name="responsible_id">
                            <option value="">{{__('backend.responsible')}}</option>
                            @foreach($users as $user)
                                <option value="{{$user->id}}" {{$user->id == old("responsible_id") ? 'selected' : ''}}>
                                    {{$user->name}}
                                </option>
                            @endforeach
                        </select>
                        @if($errors->has("responsible_id"))
                            <span class="help-block">{{ $errors->first("responsible_id") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('order_type')) has-error @endif">
                        <label class="control-label">{{__('backend.order_type')}}</label>
                        <select class=" form-control" id="order_type" name="order_type">
                            @foreach($types as $type)
                                <option
                                    value="{{$type->id}}" {{$type->id == old("order_type") ? 'selected' : ''}}>
                                    {{$type->name_en}}
                                </option>
                            @endforeach
                        </select>
                        @if($errors->has("order_type"))
                            <span class="help-block">{{ $errors->first("order_type") }}</span>
                        @endif
                    </div>
                    <div id="other_type_order"
                         class="form-group col-md-4 col-sm-4 @if($errors->has('other_order_type')) has-error @endif"
                         style="display: none">
                        <label class="control-label">{{__('backend.other_order_type')}}</label>
                        <input type="text" id="other_order_type-field" name="other_order_type" class="form-control"
                               value="logistics"/>
                        @if($errors->has("other_order_type"))
                            <span class="help-block">{{ $errors->first("other_order_type") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('shipping_type')) has-error @endif">
                        <label class="control-label">{{__('backend.shipping_type')}}</label>
                        <select class=" form-control" id="shipping_type" name="shipping_type">
                            <option
                                value="1" {{ old("shipping_type") == 1 ? 'selected' : ''}}>
                                {{__('backend.n_shipping')}}
                            </option>
                            <option
                                value="2" {{ old("shipping_type") == 2 ? 'selected' : ''}}>
                                {{__('backend.rt_shipping')}}
                            </option>
                        </select>
                        @if($errors->has("shipping_type"))
                            <span class="help-block">{{ $errors->first("shipping_type") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('mobile')) has-error @endif">
                        <label for="mobile-field">{{__('backend.mobile_number')}}: </label>
                        <input type="text" id="mobile-field" maxlength="11" name="mobile" class="form-control"
                               value="{{old("mobile")}}"/>
                        @if($errors->has("mobile"))
                            <span class="help-block">{{ $errors->first("mobile") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('phone')) has-error @endif">
                        <label for="phone-field">{{__('backend.mobile_number2')}}: </label>
                        <input type="text" id="phone-field" name="phone" maxlength="11" class="form-control"
                               value="{{old("phone")}}"/>
                        @if($errors->has("phone"))
                            <span class="help-block">{{ $errors->first("phone") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('company_website')) has-error @endif">
                        <label for="company_website-field">{{__('backend.Officail_site')}}: </label>
                        <input type="email" id="company_website-field" name="company_website" class="form-control"
                               value="{{old("company_website")}}"/>
                        @if($errors->has("company_website"))
                            <span class="help-block">{{ $errors->first("company_website") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('government_id')) has-error @endif">
                        <label for="government_id-field"> {{__('backend.government')}}: </label>
                        <select class="form-control government_id" id="government_id" name="government_id">
                            <option value="" data-display="Select">{{__('backend.government')}}</option>
                            @if(! empty($governments))
                                @foreach($governments as $government)
                                    <option value="{{$government->id}}">
                                        {{$government->name_en}}
                                    </option>

                                @endforeach
                            @endif
                        </select>
                        @if($errors->has("government_id"))
                            <span class="help-block">{{ $errors->first("government_id") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4">
                        <label class="control-label" for="s_state_id">{{__('backend.city')}}</label>
                        <select class=" form-control city_id" id="city_id" name="city_id">
                            <option value="">{{__('backend.city')}}</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('address')) has-error @endif">
                        <label for="address-field">{{__('backend.address')}}: </label>
                        <input type="text" id="address-field" name="address" class="form-control"
                               value="{{old("address")}}"/>
                        @if($errors->has("address"))
                            <span class="help-block">{{ $errors->first("address") }}</span>
                        @endif
                    </div>

                    {{--                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('latitude')) has-error @endif">--}}
                    {{--                        <label for="latitude-field">{{__('backend.latitude')}}</label>--}}
                    {{--                        <input type="text" id="latitude-field" name="latitude" class="form-control"--}}
                    {{--                               value="{{old("latitude")}}"/>--}}
                    {{--                        @if($errors->has("latitude"))--}}
                    {{--                            <span class="help-block">{{ $errors->first("latitude") }}</span>--}}
                    {{--                        @endif--}}
                    {{--                    </div>--}}
                    {{--                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('longitude')) has-error @endif">--}}
                    {{--                        <label for="longitude-field">{{__('backend.longitude')}}</label>--}}
                    {{--                        <input type="text" id="longitude-field" name="longitude" class="form-control"--}}
                    {{--                               value="{{old("longitude")}}"/>--}}
                    {{--                        @if($errors->has("longitude"))--}}
                    {{--                            <span class="help-block">{{ $errors->first("longitude") }}</span>--}}
                    {{--                        @endif--}}
                    {{--                    </div>--}}
                    <div
                        class="form-group col-md-4 col-sm-4 @if($errors->has('commercial_record_number')) has-error @endif">
                        <label for="commercial-field">{{__('backend.commercial_record_number')}}: </label>
                        <input type="text" id="commercial_record_number-field" name="commercial_record_number"
                               class="form-control" value="{{ old('commercial_record_number') }}"/>
                        @if($errors->has("commercial_record_number"))
                            <span class="help-block">{{ $errors->first("commercial_record_number") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('is_active')) has-error @endif">
                        <label for="is_active-field"> {{__('backend.active')}}: </label>
                        <select id="is_active-field" name="is_active" class="form-control" value="{{old("is_active")}}">
                            <option value="0">{{__('backend.no')}}</option>
                            <option value="1">{{__('backend.yes')}}</option>
                        </select>
                        @if($errors->has("is_active"))
                            <span class="help-block">{{ $errors->first("is_active") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-4 col-sm-4">
                        <label for="mobile-users">{{__('backend.store')}}</label>
                        <select class="form-control select2 disabled" id="responsible" name="responsible">
                            <option value="" data-dispaly="Select">{{__('backend.store')}}</option>
                            @if(! empty($stores))
                                @foreach($stores as $store)
                                    <option value="{{$store->id}}">{{$store->name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('transfer_method')) has-error @endif">
                        <label for="transfer_method">{{__('backend.transfer_method')}}</label>
                        <select id="transfer_method" name="transfer_method" class="form-control">
                            <option value="" data-display="Select">{{__('backend.choose_transfer_method')}}</option>
                            <option
                                value="1" {{ old("transfer_method") == 1 ? 'selected' : '' }}>{{__('backend.bank')}}</option>
                            <option
                                value="2" {{ old("transfer_method") == 2 ? 'selected' : '' }}>{{__('backend.mobicash')}}</option>
                            <option
                                value="3" {{ old("transfer_method") == 3 ? 'selected' : '' }}>{{__('backend.cash_with_captain')}}</option>
                        </select>
                        @if($errors->has("transfer_method"))
                            <span class="help-block">{{ $errors->first("transfer_method") }}</span>
                        @endif
                    </div>

                    <div
                        class="form-group col-md-4 col-sm-4 {{ old("transfer_method") != 1 ? 'hidden' : '' }} @if($errors->has('bank_name')) has-error @endif">
                        <label for="bank_name">{{__('backend.bank_name')}}</label>
                        <input type="text" id="bank_name" name="bank_name" class="form-control"
                               value="{{old("bank_name")}}"/>
                        @if($errors->has("bank_name"))
                            <span class="help-block">{{ $errors->first("bank_name") }}</span>
                        @endif
                    </div>

                    <div
                        class="form-group col-md-4 col-sm-4 {{ old("transfer_method") != 1 ? 'hidden' : '' }} @if($errors->has('bank_account')) has-error @endif">
                        <label for="bank_name">{{__('backend.bank_account')}}</label>
                        <input type="text" id="bank_account" name="bank_account" class="form-control"
                               value="{{old("bank_account")}}"/>
                        @if($errors->has("bank_account"))
                            <span class="help-block">{{ $errors->first("bank_account") }}</span>
                        @endif
                    </div>

                    <div
                        class="form-group col-md-4 col-sm-4 {{ old("transfer_method") != 2 ? 'hidden' : '' }} @if($errors->has('mobile_transfer')) has-error @endif">
                        <label for="mobile_transfer">{{__('backend.mobile_number')}}</label>
                        <input type="text" id="mobile_transfer" name="mobile_transfer" class="form-control"
                               value="{{old("mobile_transfer")}}"/>
                        @if($errors->has("mobile_transfer"))
                            <span class="help-block">{{ $errors->first("mobile_transfer") }}</span>
                        @endif
                    </div>

                    <div
                        class="form-group col-md-4 col-sm-4 @if($errors->has('commercial_record_image')) has-error @endif">
                        <div id="commercial_record_image_upload"></div>
                        <label for="commercial_record_image-field">{{__('backend.ContractForm')}}</label>
                        <input class="uploadPhoto btn btn-primary" type="file" name="commercial_record_image"
                               id="commercial_record_image">
                        @if($errors->has("commercial_record_image"))
                            <span class="help-block">{{ $errors->first("commercial_record_image") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('logo')) has-error @endif">
                        <div id="logo_upload"></div>
                        <label for="logo-field">{{__('backend.logo')}}</label>
                        <input class="uploadPhoto btn btn-primary" type="file" name="logo" id="logo">
                        @if($errors->has("logo"))
                            <span class="help-block">{{ $errors->first("logo") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-12 col-sm-12 col-xs-12 @if($errors->has('notes')) has-error @endif">
                        <label for="notes-field">@lang('backend.ContractNote')</label>
                        <textarea class="form-control" id="notes-field" rows="6"
                                  name="notes">{{ old("notes") }}</textarea>
                        @if($errors->has("notes"))
                            <span class="help-block">{{ $errors->first("notes") }}</span>
                        @endif
                    </div>


                    <div class="col-md-12 col-sm-12">
                        <h3 style="text-decoration: underline;margin-bottom: 20px;"
                            for="driver-has-vehicle-field">{{__('backend.Click_To_Add_Client_For_This_Corporate')}}</h3>

                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('customer_name')) has-error @endif">
                        <label for="name-field">{{__('backend.username')}}: </label>
                        <input type="text" id="user-name-field" name="customer_name" class="form-control"
                               value="{{ old("customer_name") }}"/>
                        @if($errors->has("customer_name"))
                            <span class="help-block">{{ $errors->first("customer_name") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('customer_mobile')) has-error @endif">
                        <label for="mobile-field">{{__('backend.mobile_number')}}: </label>
                        <input type="text" id="user-mobile-field" name="customer_mobile" class="form-control"
                               maxlength="11" value="{{ old("customer_mobile") }}"/>
                        @if($errors->has("customer_mobile"))
                            <span class="help-block">{{ $errors->first("customer_mobile") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('customer_phone')) has-error @endif">
                        <label for="phone-field">{{__('backend.mobile_number2')}}: </label>
                        <input type="text" id="user-phone-field" name="customer_phone" class="form-control"
                               maxlength="11" value="{{ old("customer_phone") }}"/>
                        @if($errors->has("phone"))
                            <span class="help-block">{{ $errors->first("phone") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('customer_email')) has-error @endif">
                        <label for="email-field">{{__('backend.email')}}: </label>
                        <input type="text" id="user-email-field" name="customer_email" class="form-control"
                               value="{{ old("customer_email") }}"/>
                        @if($errors->has("customer_email"))
                            <span class="help-block">{{ $errors->first("customer_email") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('customer_password')) has-error @endif">
                        <label for="password-field">{{__('backend.password')}}: </label>
                        <input type="password" id="user-password-field" name="customer_password" class="form-control"/>
                        @if($errors->has("customer_password"))
                            <span class="help-block">{{ $errors->first("customer_password") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('confirm_password')) has-error @endif">
                        <label for="confirm_password-field">{{__('backend.confirm_password')}}: </label>
                        <input type="password" id="user-confirm_password-field" name="customer_confirm_password"
                               class="form-control"/>
                        @if($errors->has("confirm_password"))
                            <span class="help-block">{{ $errors->first("confirm_password") }}</span>
                        @endif
                    </div>

                    <div
                        class="form-group col-md-4 col-sm-4 @if($errors->has('customer_government_id')) has-error @endif">
                        <label for="government_id-field"> {{__('backend.government')}}: </label>
                        <select class="form-control customer_government_id" id="customer_government_id"
                                name="customer_government_id">
                            <option value="" data-display="Select">{{__('backend.government')}}</option>
                            @if(! empty($governments))
                                @foreach($governments as $government)
                                    <option value="{{$government->id}}">
                                        {{$government->name_en}}
                                    </option>

                                @endforeach
                            @endif
                        </select>
                        @if($errors->has("customer_government_id"))
                            <span class="help-block">{{ $errors->first("customer_government_id") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4">
                        <label class="control-label" for="s_state_id">{{__('backend.city')}}</label>
                        <select class=" form-control customer_city_id" id="customer_city_id" name="customer_city_id">
                            <option value="">{{__('backend.city')}}</option>
                        </select>
                    </div>


                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('customer_address')) has-error @endif">
                        <label for="address-field">{{__('backend.address')}}: </label>
                        <input type="text" id="user-address-field" name="customer_address" class="form-control"
                               value="{{old("customer_address")}}"/>
                        @if($errors->has("customer_address"))
                            <span class="help-block">{{ $errors->first("customer_address") }}</span>
                        @endif
                    </div>

                    <div
                        class="form-group col-md-4 col-sm-4 @if($errors->has('customer_latitude')) has-error @endif">
                        <div class=" input-group ">
                            <label for="receiver_latitude"> {{__('backend.latitude')}}: </label>
                            <input type="text" required id="latitude" name="customer_latitude" class="form-control"
                                   value="{{ old("customer_latitude") ? old("customer_latitude")  : '30.0668886'  }}"/>
                            <span class="input-group-append">
                            <button class="input-group-text bg-transparent" type="button" data-toggle="modal"
                                    data-target="#map1Modal"> <i class="fa fa-map-marker"></i></button>
                        </span>

                            @if($errors->has("customer_latitude"))
                                <span class="help-block">{{ $errors->first("customer_latitude") }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('customer_longitude')) has-error @endif">
                        <label for="longitude"> {{__('backend.longitude')}}: </label>
                        <input type="text" required id="longitude" name="customer_longitude"
                               class="form-control"
                               value="{{ old("customer_longitude") ? old("customer_longitude") : '31.1962743'  }}"/>
                        @if($errors->has("customer_longitude"))
                            <span class="help-block">{{ $errors->first("customer_longitude") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('customer_job')) has-error @endif">
                        <label for="job-field">{{__('backend.job_name')}}:</label>
                        <input type="text" id="user-job-field" name="customer_job" class="form-control"
                               value="{{old("customer_job")}}"/>
                        @if($errors->has("customer_job"))
                            <span class="help-block">{{ $errors->first("customer_job") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-4 col-sm-4">
                        <label for="mobile-users">{{__('backend.store')}}</label>
                        <select class="form-control select2 disabled" id="customer_store_id" name="customer_store_id">
                            <option value="" data-dispaly="Select">{{__('backend.store')}}</option>
                            @if(! empty($stores))
                                @foreach($stores as $store)
                                    <option value="{{$store->id}}">{{$store->name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('national_id')) has-error @endif">
                        <label for="user-national_id-field">{{__('backend.national_id_number')}}:</label>
                        <input type="text" id="national_id-field" maxlength="14" name="customer_national_id"
                               class="form-control" value="{{old("national_id")}}"/>
                        @if($errors->has("national_id"))
                            <span class="help-block">{{ $errors->first("national_id") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('image')) has-error @endif">
                        <div id="customer_image_upload"></div>
                        <label for="user-image-field">{{__('backend.upload_image')}}: </label>
                        <input class="uploadPhoto btn btn-primary" type="file" name="customer_image" id="user-image">
                        @if($errors->has("image"))
                            <span class="help-block">{{ $errors->first("image") }}</span>
                        @endif
                    </div>

                    <div class="well well-sm col-md-12 col-sm-12 col-xs-12">
                        <button type="submit" id="added_user"
                                class="btn btn-primary"> {{__('backend.add_corporate')}} </button>
                        <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.corporates.index') }}"><i
                                class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
                    </div>
            </form>
        </div>
    </div>

    @include('backend.map')
@endsection
@section('scripts')
    <script type="text/javascript" src="{{asset('/assets/scripts/map.js')}}"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key=AIzaSyDQiN4qc7pC9b1BRQwkqXXD28peMfWcHvw&callback=myMap"></script>
    <script src="{{asset('assets/select2/select2.min.js')}}"></script>

    <script type="text/javascript">
        $(function () {
            $('.select2').select2();
            $("form[name='corporate_create']").validate({
                // Specify validation rules
                rules: {
                    name: {
                        required: true,
                    },
                    email: {
                        required: true,
                        email: true,
                        remote: {
                            url: "{!! url('/check-email-corporate') !!}",
                            type: "get"
                        },
                    },
                    // field: {
                    //     required: true,
                    // },
                    order_type: {
                        required: true,
                    },
                    shipping_type: {
                        required: true,
                    },
                    mobile: {
                        required: true,
                        number: true,
                        rangelength: [11, 11],
                        // pattern: [0-9],
                        // digits:true,
                        remote: {
                            url: "{!! url('/check-mobile-corporate') !!}",
                            type: "get"
                        },
                    },

                    company_website: {
                        email: true,
                    },
                    government_id: {
                        required: true,
                    },
                    city_id: {
                        required: true,
                    },
                    customer_government_id: {
                        required: true,
                    },
                    customer_city_id: {
                        required: true,
                    },
                    customer_address: {
                        required: true,
                    },
                    customer_mobile: {
                        required: true,
                    },
                    customer_email: {
                        required: true,
                    },
                    customer_name: {
                        required: true,
                    },
                    address: {
                        required: true,
                    },
                    customer_password: {
                        required: true,
                        minlength: 5,
                    },
                    customer_confirm_password: {
                        required: true,
                        equalTo: '#user-password-field',
                        minlength: 5,
                    },
                    // latitude: {
                    //     required: true,
                    //     number: true,
                    // },
                    // longitude: {
                    //     required: true,
                    //     number: true,
                    // },
                    customer_latitude: {
                        number: true,
                    },
                    customer_longitude: {
                        number: true,
                    },
                    commercial_record_number: {
                        required: true,
                        number: true,
                    },
                    commercial_record_image: {
                        required: true,
                    },
                    // corporate_has_user: {
                    //     required: true,
                    // },
                    // corporate_has_client: {
                    //     required: true,
                    // },
                },
                // Specify validation error messages
                messages: {
                    name: {
                        required: "{{__('backend.Please_Enter_Corporate_Name')}}",
                    },
                    email: {
                        required: "{{__('backend.Please_Enter_Corporate_E-mail')}}",
                        email: "{{__('backend.Please_Enter_Correct_E-mail')}}",
                        remote: jQuery.validator.format("{{__('backend.E-mail_already_exist')}}"),
                    },
                    order_type: {
                        required: "{{__('backend.Please_Enter_Order_Type')}}"
                    },
                    shipping_type: {
                        required: "{{__('backend.Please_Enter_Shipping_Type')}}"
                    },
                    {{--field: {--}}
                        {{--    required: "{{__('backend.Please_Enter_Work_field')}}",--}}
                        {{--},--}}
                    mobile: {
                        required: "{{__('backend.Please_Enter_Corporate_Mobile')}}",
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                        rangelength: "{{__('backend.Mobile_Must_Be11_Digits')}}",
                        remote: jQuery.validator.format("{{__('backend.Mobile_already_exist')}}"),
                    },
                    company_website: {
                        email: "{{__('backend.Please_Enter_Correct_E-mail')}}",
                    },
                    government_id: {
                        required: "{{__('backend.Please_Chosse_The_Government')}}",
                    },
                    city_id: {
                        required: "{{__('backend.Please_Select_City')}}",
                    },
                    customer_government_id: {
                        required: "{{__('backend.Please_Chosse_The_Government')}}",
                    },
                    customer_city_id: {
                        required: "{{__('backend.Please_Select_City')}}",
                    },
                    address: {
                        required: "{{__('backend.Please_Enter_The_Address')}}",
                    },
                    {{--latitude: {--}}
                        {{--    required: "{{__('backend.Please_Enter_Latitude')}}",--}}
                        {{--    number: "{{__('backend.Please_Enter_Avalid_Number')}}",--}}

                        {{--},--}}
                        {{--longitude: {--}}
                        {{--    number: "{{__('backend.Please_Enter_Avalid_Number')}}",--}}
                        {{--    required: "{{__('backend.Please_Enter_Longitude')}}",--}}
                        {{--},--}}
                    customer_latitude: {
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",

                    },
                    customer_longitude: {
                        required: "{{__('backend.Please_Enter_Longitude')}}",
                    },
                    commercial_record_number: {
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                        required: "{{__('backend.Please_Enter_Commercial_Record_Number')}}",
                    },
                    commercial_record_image: {
                        required: "{{__('backend.Please_Enter_Commercial_Record_Image')}}",
                    },
                    customer_name: {
                        required: "{{__('backend.Please_Enter_Customer_Name')}}",
                    },
                    customer_email: {
                        required: "{{__('backend.Please_Enter_Customer_E-mail')}}",
                        email: "{{__('backend.Please_Enter_Correct_E-mail')}}",
                        remote: jQuery.validator.format("{{__('backend.E-mail_already_exist')}}"),
                    },
                    customer_address: {
                        required: "{{__('backend.Please_Enter_The_Address')}}",
                    },
                    customer_mobile: {
                        required: "{{__('backend.Please_Enter_Customer_Mobile')}}",
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                        rangelength: "{{__('backend.Mobile_Must_Be11_Digits')}}",
                        remote: jQuery.validator.format("{{__('backend.Mobile_already_exist')}}"),
                    },
                    customer_password: {
                        required: "{{__('backend.Please_Enter_Customer_Password')}}",
                        minlength: "{{__('backend.Password_must_be_more_than5_character')}}"
                    },
                    customer_confirm_password: {
                        required: "{{__('backend.Confirm_password_is_wrong')}}",
                        equalTo: "{{__('backend.Confirm_password_is_wrong')}}",
                    },
                    {{--corporate_has_user: {--}}
                    {{--    required: "{{__('backend.Please_Choose_If_you_need_Add_Client_or_Not')}}",--}}
                    {{--},--}}
                    // corporate_has_client: {
                    //     required: "{{__('backend.Please_Choose_If_you_need_Add_Client_or_Not')}}",
                    // },

                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                },

                highlight: function (element) {
                    $(element).parent().addClass('error')
                },
                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                }
            });


            // if($( "#corporate_has_client" ).val() == 1) {
            //   $('#add_client').css('display', 'block');
            // } else {
            //   $('#add_client').css('display', 'none');
            // }
            //
            // $( "#corporate_has_client" ).change(function() {
            //   if( $(this).val() == 1) {
            //     $('#add_client').css('display', 'block');
            //   } else {
            //     $('#add_client').css('display', 'none');
            //   }
            // });

            $("#transfer_method").on('change', function () {
                if ($(this).val() == 1) {
                    $("#bank_name").parent().removeClass('hidden');
                    $("#bank_account").parent().removeClass('hidden');
                    $("#mobile_transfer").parent().addClass('hidden');
                } else if ($(this).val() == 2) {
                    $("#bank_name").parent().addClass('hidden');
                    $("#bank_account").parent().addClass('hidden');
                    $("#mobile_transfer").parent().removeClass('hidden');
                } else {
                    $("#bank_name").parent().addClass('hidden');
                    $("#bank_account").parent().addClass('hidden');
                    $("#mobile_transfer").parent().addClass('hidden');
                }
            });

            $("#government_id").on('change', function () {
                $('#city_id').html('<option value="" >Choose City</option>');
                if ($(this).val()) {
                    $.ajax({
                        url: "{{ url('/get_cities')}}" + "/" + $(this).val(),
                        type: 'get',
                        data: {},
                        success: function (data) {
                            $.each(data, function (i, content) {
                                $('#city_id').append($("<option></option>").attr("value", content.id).text(content.name_en));
                            });
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }
            });

            $("#order_type").on('change', function () {
                if ($(this).val() == 5) {
                    $("#other_type_order").show();
                } else {
                    $("#other_order_type-field").val('');
                    $("#other_type_order").hide();
                }
            });

            $("#customer_government_id").on('change', function () {
                $('#customer_city_id').html('<option value="" >Choose City</option>');
                if ($(this).val()) {
                    $.ajax({
                        url: "{{ url('/get_cities')}}" + "/" + $(this).val(),
                        type: 'get',
                        data: {},
                        success: function (data) {
                            $.each(data, function (i, content) {
                                $('#customer_city_id').append($("<option></option>").attr("value", content.id).text(content.name_en));
                            });
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }
            });


            //
            // if ($("#corporate-has-user").val() == 1) {
            //     $('#data-user').show();
            // } else {
            //     $('#data-user').hide();
            // }
            //
            // $("#corporate-has-user").change(function () {
            //     if ($(this).val() == 1) {
            //
            //         $('#data-user').show();
            //
            //
            //     } else {
            //         $('#data-user').hide();
            //     }
            // });

        })
    </script>
@endsection

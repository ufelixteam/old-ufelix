@extends('frontend.profile.layouts.app')

@section('title') @lang('website.Orders') @endsection
@section('css')
    <title>{{__('backend.corporates')}}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <style>
        input[type=checkbox], input[type=radio] {
            width: 18px;
            height: 18px;
        }

        .printdata {
            cursor: pointer;
        }

        .selectAll-button {
            color: #fff;
            background-color: #5bc0de;
            border-color: #46b8da;

        }

        .submit-button {
            color: #fff;
            background-color: #0376d9;
        }

        .cancel-button {
            background-color: #ff5900;
            border-color: #ed5301;
        }

        .button-padding {
            padding: 5px 14px;
        }
    </style>
@endsection
@section('content')
    <div class="flash-message"></div>

    <div class="alert alert-danger" hidden>
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong id="errorId"></strong>
    </div>
    @if (session('success') || !empty($success))
        <div class="alert alert-success alert-block text-center">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ !empty($success) ? $success : session('success') }}</strong>
        </div>
    @endif
    @if (session('warning'))
        <div class="alert alert-warning alert-block text-center">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ session('warning') }}</strong>
        </div>
    @endif

    {{-- @if (session('success'))
         <div class="alert alert-success alert-block text-center">
             <button type="button" class="close" data-dismiss="alert">×</button>
             <strong>{{ session('success') }}</strong>
         </div>
     @endif
     @if (session('warning'))
         <div class="alert alert-warning alert-block text-center">
             <button type="button" class="close" data-dismiss="alert">×</button>
             <strong>{{ session('warning') }}</strong>
         </div>
     @endif--}}

    <!-- Modal For Cancel Orders-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">{{__('website.add_cancel_comment')}}</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="reason">{{__('website.comment')}}</label>
                            <textarea class="form-control" id="reason" name="reason" rows="3"></textarea>
                        </div>
                        <div class="col-6"></div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal"
                                    style="margin-left: 15px;">Close
                            </button>
                            <button type="submit" class="btn btn-primary pull-right cancel-order submit-button"
                                    id="cancel-order" data-value="dropped"
                            >{{__('website.submit')}}</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @if(isset($success))
        <div class="col-md-12 col-sm-12 col-xs-12" @if(app()->getLocale() != 'en') style="text-align: right" @endif>
            <div class="alert alert-success">{{$success}}</div>
        </div>
    @endif

    @if (session('success'))
        <div class="col-md-12 col-sm-12 col-xs-12" @if(app()->getLocale() != 'en') style="text-align: right" @endif>
            <div class="alert alert-success">{{session('success')}}</div>
        </div>
    @endif

    <div class="row">
        <div class="col-12">
            <div class="">
                <div class="table-responsive m-t-40">
                    <h1 class="title">
                        @if(app('request')->input('type') == 'pending')
                            @lang('website.pending_orders')
                        @elseif(app('request')->input('type') == 'accept')
                            @lang('website.accepted_orders')
                        @elseif(app('request')->input('type') == 'receive')
                            @lang('website.received_orders')
                        @elseif(app('request')->input('type') == 'deliver')
                            @lang('website.delivered_orders')
                        @elseif(app('request')->input('type') == 'cancel')
                            @lang('website.cancelled_orders')
                        @elseif(app('request')->input('type') == 'recall')
                            @lang('website.recalled_orders')
                        @elseif(app('request')->input('type') == 'waiting')
                            @lang('website.waiting_orders')
                        @elseif(app('request')->input('type') == 'dropped')
                            @lang('website.dropped_orders')
                        @elseif(app('request')->input('type') == 'refund')
                            @lang('website.refund_orders')
                        @elseif(app('request')->input('type') == 'all')
                            @lang('backend.all_orders')
                        @elseif(app('request')->input('collection'))
                            @lang('website.collection_orders')
                        @endif
                    </h1>
                    <form
                            action="{{ app()->getLocale() == 'en' ? url('/en/customer/account/orders') : url('/customer/account/orders') }}"
                            method="get">
                        <input type="hidden" name="type" value="{{ app('request')->input('type')}}">
                        <div class="row" style="margin-bottom:15px;" id="orders">
                            <div class="col-md-12" style="padding: 0">
                                <div class="group-control col-md-3 col-sm-3">
                                    <input type="text" class="form-control" id="search-field" name="search"
                                           value="{{ app('request')->input('search')}}"
                                           placeholder="{{__('backend.search')}} ">
                                </div>

                                <div class="group-control col-md-3 col-sm-3">
                                    <select id="r_government_id" name="r_government_id" class="form-control">
                                        <option value=""
                                                data-display="Select">{{__('backend.Receiver_Government')}}</option>
                                        @if(! empty($app_governments))
                                            @foreach($app_governments as $government)
                                                <option value="{{$government->id}}"
                                                        @if(app('request')->input('r_government_id') == $government->id) selected @endif>
                                                    {{$government->name_en}}
                                                </option>

                                            @endforeach
                                        @endif

                                    </select>
                                </div>

                                <div class="group-control col-md-2 col-sm-2">
                                    <button type="submit" style="padding: 8px;"
                                            class="btn btn-primary btn-block"> {{__('backend.filter_orders')}} </button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <form method="post" id="myform" action="{{ url('/customer/account/print_police') }}"
                          target="_blank">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <table id="example23" class="display nowrap table table-hover table-striped table-bordered"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th class="text-center">@lang('website.ID')</th>
                                <th class="text-center">@lang('website.collection_number')</th>
                                <th class="text-center">@lang('website.receiver_name')</th>
                                <th class="text-center">@lang('website.order_name')</th>
                                <th class="text-center">@lang('website.from')</th>
                                <th class="text-center">@lang('website.to')</th>
                                <th class="text-center">@lang('website.sender_code')</th>
                                {{--                                <th class="text-center">@lang('website.receiver_code')</th>--}}
                                @if($customer->is_manager)
                                    <th class="text-center">@lang('website.client')</th>
                                @endif
                                @if(!app('request')->input('collection'))
                                    <th class="text-center">@lang('website.status')</th>
                                @endif
                                @if(in_array(request()->type, ['refund']))
                                    <th class="text-center">@lang('backend.payment_status')</th>
                                    <th class="text-center">@lang('backend.payment_date')</th>
                                    <th class="text-center">@lang('backend.paid_amount')</th>
                                @endif
                                <th>
                                    <input type="checkbox" name="checkall" id="checkall" class="md-check">

                                    <a class="text-inverse p-r-10 printdata" id="pdf-print"><i
                                                class="icon-print"></i></a>
                                </th>
                                <th></th>
                                @if(app('request')->input('type') == 'pending' | app('request')->input('type') == 'dropped')
                                    <th>
                                        <input type="button" class="btn selectAll-button btn-xs button-padding"
                                               id="toggle"
                                               value="{{__('website.select_all')}}" onClick="do_this()"/>
                                        @if(app('request')->input('type') == 'pending')
                                            <button type="button" id="cancel-order" data-value="pending"
                                                    class="btn cancel-button  btn-xs button-padding cancel-order">
                                                {{__('website.cancel_order')}}</button>
                                        @endif
                                        @if(app('request')->input('type') == 'dropped')
                                            <button type="button" id="cancel-submit"
                                                    class="btn cancel-button  btn-xs button-padding"
                                                    data-toggle="modal" data-target="#exampleModal">
                                                {{__('website.cancel_order_request')}}</button>
                                        @endif


                                    </th>
                                @endif

                            </tr>
                            </thead>
                            <tbody>
                            @if (! empty($orders) )
                                @foreach ($orders as $order)
                                    <tr>
                                        <td class="text-center">{{ $order->order_no }}</td>
                                        <td class="text-center">{{ $order->collection_id ? $order->collection_id : '-' }}</td>
                                        <td class="text-center">{{ $order->receiver_name }}</td>
                                        <td class="text-center">{{ $order->type ? $order->type : '-'}}</td>
                                        <td class="text-center">
                                            @if(! empty($order->from_government)  && $order->from_government != null )
                                                {{$order->from_government->name_en}}
                                            @endif
                                        </td>

                                        <td class="text-center">
                                            @if(! empty($order->to_government)  && $order->from_government != null  )
                                                {{$order->to_government->name_en}}
                                            @endif
                                        </td>
                                        <td class="text-center">{{$order->order_number}}</td>
                                        {{--                                        <td class="text-center">{{$order->receiver_code}}</td>--}}
                                        @if($customer->is_manager)
                                            <td class="text-center">{{$order->customer->name}}</td>
                                        @endif
                                        @if(!app('request')->input('collection'))
                                            <td class="text-center">
                                                {!! $order->profile_status_details !!}
                                            </td>
                                        @endif
                                        @if(in_array(request()->type, ['refund']))
                                            <th class="text-center">{!! $order->payment_status_span !!}</th>
                                            <th class="text-center">{!! $order->payment_date !!}</th>
                                            <th class="text-center">{!! $order->paid_amount !!}</th>
                                        @endif
                                        <td class="selectpdf text-center">
                                            <input type="checkbox" name="ids[]" id="checkbox2_{{$order->id}}"
                                                   value="{{$order->id}}"></td>
                                        <td>
                                            <a href="{{ app()->getLocale() == 'en'  ? route('profile.orders', ['locale' =>'en','id' =>$order->id] )  : url('/customer/account/orders?id='.$order->id ) }}"
                                               class="text-inverse p-r-10" data-toggle="tooltip" title=""
                                               data-original-title="show" aria-describedby="tooltip468946">
                                                <i class="icon-eye"></i>
                                            </a>

                                            @if(app('request')->input('type') == 'pending')
                                                <a href="{{ app()->getLocale() == 'en'  ? route('profile.order_edit', ['locale' =>'en','id' =>$order->id] )  : url('/customer/account/orders/'.$order->id.'/edit' ) }}"
                                                   class="text-inverse p-r-10" data-toggle="tooltip" title=""
                                                   data-original-title="show" aria-describedby="tooltip468946">
                                                    <i class="icon-edit"></i>
                                                </a>
                                            @endif

                                            <a class="text-inverse p-r-10 printdata" data-id="{{$order->id}}"
                                               href="{{ url('/customer/account/print_police?ids='.$order->id) }}"><i
                                                        class="icon-print"></i></a>
                                        </td>
                                        @if(app('request')->input('type') == 'pending' | app('request')->input('type') == 'dropped')
                                            <td class="exclude-td selectOrders text-center" id="selectOrders">
                                                <input type="checkbox" name="select[]" value="{{$order->id}}"
                                                       class="selectOrder"
                                                       data-value="{{$order->id}}"/>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            @endif

                            </tbody>
                        </table>
                    </form>
                    @if (! empty($orders) )
                        {!! $orders->appends($_GET)->links() !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

    <script>

        // Select All Orders function For Cancel
        function do_this() {
            var checkboxes = document.getElementsByName('select[]');
            var button = document.getElementById('toggle');

            if (button.value === '{{__('website.select_all')}}') {
                for (var i in checkboxes) {
                    checkboxes[i].checked = true;
                }
                button.value = '{{__('website.deselect_all')}}'
            } else {
                for (var i in checkboxes) {
                    checkboxes[i].checked = false;
                }
                button.value = '{{__('website.select_all')}}';
            }
        }

        $('.cancel-order').on('click', function () {
            $("#exampleModal").modal("hide");
            let selected_order_ids = [];
            let checkboxes = document.getElementsByName('select[]');
            var _token = $('#_token');
            var order_type = $(this).attr('data-value');
            console.log('orders type', order_type);

            for (let i in checkboxes) {
                if (checkboxes[i].checked === true) {
                    selected_order_ids.push(checkboxes[i].value);
                }
            }
            if (selected_order_ids.length > 0) {
                if (order_type === 'pending') {
                    $.ajax({
                        url: '{{ route('profile.cancel_orders') }}',
                        type: 'post',
                        data: {
                            _token: "{{ csrf_token() }}",
                            'orders_to_cancel': selected_order_ids,
                        },
                        success: function (data) {
                            location.reload();
                            var successAlert = '<div class="alert alert-success alert-block text-center">' +
                                '            <button type="button" class="close" data-dismiss="alert">×</button>' +
                                '            <strong>{{__('website.cancelled_orders_success')}}</strong></div>';
                            $('div.flash-message').html(successAlert);

                        },
                        error: function (data) {

                        }

                    });
                } else if (order_type === 'dropped') {
                    let reason = $('#reason').val();
                    console.log('drooppppep', reason);
                    $.ajax({
                        url: '{{ route('profile.cancel_orders_request') }}',
                        type: 'post',
                        data: {
                            _token: "{{ csrf_token() }}",
                            'orders_ids_to_cancel': selected_order_ids,
                            'reason': reason,
                        },
                        success: function (data) {
                            location.reload();
                            var successAlert = '<div class="alert alert-success alert-block text-center">' +
                                '            <button type="button" class="close" data-dismiss="alert">×</button>' +
                                '            <strong>{{__('website.cancelled_orders_success')}}</strong></div>';
                            $('div.flash-message').html(successAlert);

                        },
                        error: function (data) {
                            let warning = ' <button type="button" class="close" data-dismiss="alert">×</button>' +
                                '            <strong>{{__('website.')}}</strong>';
                            $('div.flash-message').html(warning);

                        }

                    });
                }


            } else {
                $.confirm({
                    title: '',
                    theme: 'modern',
                    content: '<div class="jconfirm-title-c jconfirm-hand">' +
                        '<span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span>' +
                        '<span class="jconfirm-title">{{__('website.cancel_order')}}</span></div>' +
                        '<div style="font-weight:bold;"><p>{{__("website.must_choose_order_to_cancel")}}</p></div>',
                    type: 'red',
                });
            }


        });


    </script>
@endsection

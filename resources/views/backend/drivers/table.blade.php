<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        @if($drivers->count())
            <table class="table table-condensed table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('backend.name')}}</th>
                    <th>{{__('backend.mobile')}}</th>
                    <th>{{__('backend.agent')}}</th>

                    <th>{{__('backend.vehicle')}}</th>
                    <th>{{__('backend.accept_request')}}</th>
{{--                    <th>{{__('backend.agent')}}</th>--}}
                    <th>{{__('backend.active')}}</th>
                    <th>{{__('backend.block')}}</th>
                    <th>{{__('backend.verfiy_mobile')}}</th>
                    <th>{{__('backend.verify_email')}}</th>

                <!-- <th>{{__('backend.status')}}</th> -->


                    <th class="text-right"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($drivers as $driver)
                    <tr>
                        <td>

                            <span class=" {{ ! empty($driver->device_active) ? 'text-success' : 'text-danger' }}"
                                  style="font-weight: bold"><i class='fa fa-circle' aria-hidden='true'></i></span>
                            {{$driver->id}}</td>
                        <td>{{$driver->name}}</td>
                        <td>{{$driver->mobile}}</td>
                        <td>{{!empty($driver->manager) ? $driver->manager->name : '-'}}</td>

                        <td>
                            @if ($driver->driver_has_vehicle != 0)
                                <span class='text-success'><i class='fa fa-truck fa-lg' aria-hidden='true'></i></span>

                            @else
                                <span class=' text-danger'><i class='fa fa-times' aria-hidden='true'></i></span>
                            @endif
                        </td>

                        <td>{!! $driver->status_request_span !!}</td>

{{--                        <td>{{! empty($driver->agent) ?  $driver->agent->name : ''}}</td>--}}


                        <td><input data-id="{{$driver->id}}" data-size="mini" class="toggle change_verify"
                                   {{$driver->is_active == 1 ? 'checked' : ''}} data-onstyle="success" type="checkbox"
                                   data-style="ios" data-on="Yes" data-off="No">
                        </td>
                        <td><input data-id="{{$driver->id}}" data-size="mini" class="toggle change_block"
                                   {{$driver->is_block == 1 ? 'checked' : ''}} data-onstyle="danger" type="checkbox"
                                   data-style="ios" data-on="Yes" data-off="No">

                        </td>
                        <td>

                            <input data-id="{{$driver->id}}" data-size="mini" class="toggle verify_mobile"
                                   {{$driver->is_verify_mobile == 1 ? 'checked' : ''}} data-onstyle="success"
                                   type="checkbox" data-style="ios" data-on="Yes" data-off="No">

                        </td>
                        <td>

                            <input data-id="{{$driver->id}}" data-size="mini" class="toggle verify_email"
                                   {{$driver->is_verify_email == 1 ? 'checked' : ''}} data-onstyle="success"
                                   type="checkbox" data-style="ios" data-on="Yes" data-off="No">
                        </td>

                    <!-- <td>{!! $driver->status_span !!}</td> -->


                        <td class="text-right">

                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                @if(permission('showOrdersMember')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                    <li><a href="{{url('mngrAdmin/orders/showOrders') . '/1' . '/' . $driver->id}}"><i
                                                class="glyphicon glyphicon-list"></i> {{__('backend.orders')}}</a></li>
                                @endif

                                @if(permission('showDriver')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                    <li><a href="{{ route('mngrAdmin.drivers.show', $driver->id) }}"><i
                                                class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>
                                    </li>
                                @endif

                                @if(permission('editDriver')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                    <li><a href="{{ route('mngrAdmin.drivers.edit', $driver->id) }}"><i
                                                class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a></li>
                                    @endif


                                </ul>
                            </div>
                        </td>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {!! $drivers->appends($_GET)->links() !!}

        @else
            <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
        @endif
    </div>
</div>

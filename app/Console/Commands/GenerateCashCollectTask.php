<?php

namespace App\Console\Commands;

use App\Models\CustomerTaskDay;
use App\Models\PickUp;
use Carbon\Carbon;
use Illuminate\Console\Command;

class GenerateCashCollectTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cashCollectTask:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate cash collect task automatically';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $day = Carbon::now()->format('j');
        $dayName = strtolower(Carbon::now()->format('l'));

        $days = CustomerTaskDay::with('customer')
            ->whereIn('collect_day', [$day, $dayName])
            ->groupBy('customer_id')
            ->get();

        foreach ($days as $day) {
            $pickup = new PickUp();

            $pickup->corporate_id = $day->customer->corporate_id;
            $pickup->customer_id = $day->customer_id;

            $pickup->delivery_price = 0;
            $pickup->bonus_per_order = 0;
            $pickup->is_fake_pickup = 1;
            $pickup->orders_count = 0;

            $pickup->sender_name = $day->customer->name;
            $pickup->sender_mobile = $day->customer->mobile;
            $pickup->sender_latitude = $day->customer->latitude;
            $pickup->sender_longitude = $day->customer->longitude;
            $pickup->sender_address = $day->customer->address;
            $pickup->s_government_id = $day->customer->government_id;

            $pickup->is_pickup = 0;
            $pickup->is_cash_collect = 1;
            $pickup->is_recall_orders = 0;

            $pickup->captain_received_time = date('Y-m-d');

            $pickup->notes = "";

            $number = substr(str_shuffle(str_repeat((time() * 1000), 5)), 0, 10);
            $pickup->pickup_number = $number;
            $pickup->save();
        }
    }
}

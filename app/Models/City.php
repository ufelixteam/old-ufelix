<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{

    public function Governorate() {
        return $this->belongsTo('App\Models\Governorate','governorate_id');
    }

    public function getStatusSpanAttribute($value) {
        /*
        *	status = 0 :: Not Verify
        *	status = 1 :: Verify
        */
        if($this->status == 1){
            $value="<span class='badge badge-pill label-success'>".__('backend.verfiy')."</span>";
        }else{
          $value="<span class='badge badge-pill label-danger'>".__('backend.not_verify')."</span>";
        }
        return $value;
    }

}

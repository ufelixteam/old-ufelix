@extends('backend.layouts.app')
@section('header')
<div class="page-header">
        <h3>Show #{{$order_log->id}}</h3>
        <form action="{{ route('mngrAdmin.order_logs.destroy', $order_log->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('mngrAdmin.order_logs.edit', $order_log->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <div class="form-group">
                <label for="nome">#</label>
                <p class="form-control-static"></p>
            </div>
            <div class="form-group col-sm-6">
                     <label for="driver_id">DRIVER_ID</label>
                     <p class="form-control-static">{{$order_log->driver_id}}</p>
                </div>
                    <div class="form-group col-sm-6">
                     <label for="order_id">ORDER_ID</label>
                     <p class="form-control-static">{{$order_log->order_id}}</p>
                </div>
                    <div class="form-group col-sm-6">
                     <label for="status">STATUS</label>
                     <p class="form-control-static">{{$order_log->status}}</p>
                </div>
          
            <div class="col-md-12 col-xs-12 col-sm-12">
                <a class="btn btn-link" href="{{ route('mngrAdmin.order_logs.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
            </div>

        </div>
    </div>

@endsection
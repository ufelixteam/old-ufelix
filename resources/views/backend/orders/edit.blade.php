@extends('backend.layouts.app')
@section('css')
    <style>
        .input-group {
            -moz-box-shadow: none !important;
            -webkit-box-shadow: none !important;
            box-shadow: none !important;
        }

        .input-group[class*=col-] {
            padding-right: inherit !important;
            padding-left: inherit !important;
            float: inherit;
        }

        .pac-container {
            z-index: 9999 !important;
        }
    </style>
    <title>{{__('backend.the_orders')}} - {{__('backend.edit')}}</title>
@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i> {{__('backend.Edit_Order')}} #{{$order->id}}</h3>
    </div>
@endsection

@section('content')
    @include('error')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <form action="{{ route('mngrAdmin.orders.update', $order->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="col-md-12 col-sm-12 col-xs-12" style="border: 2px solid #ea2;margin-bottom: 15px;">
                    <h4 style="color: #ea2;font-weight: bold">Sender Data</h4>

                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('corparate_id')) has-error @endif">
                        <label for="corparate_id-field">Corporate</label>


                        <select class="form-control" id="corparate_id" name="corparate_id" disabled>
                            <option value="" data-display="Select">Corparate</option>
                            @if(! empty($Corporate_ids))
                                @foreach($Corporate_ids as $corparate_id)
                                    <option
                                        value="{{$corparate_id->id}}" {{ ! empty($order) && $order->corporate_id  == $corparate_id->id ? 'selected':  '' }} >
                                        {{ $corparate_id->id.' - ' .$corparate_id->name}}
                                    </option>

                                @endforeach
                            @endif

                        </select>

                        @if($errors->has("corparate_id"))
                            <span class="help-block">{{ $errors->first("corparate_id") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-2 col-sm-2 @if($errors->has('customer_id')) has-error @endif">
                        <label for="customer_id-field">Customer </label>
                        <select class=" form-control" id="customer_id" name="customer_id" disabled>
                            @if(! empty($customers))
                                @foreach($customers as $customer)
                                    <option
                                        {{ ! empty($order) && $order->customer_id  == $customer->id ? 'selected':  '' }}   value="{{$customer->id}}">
                                        {{$customer->name}} {{$customer->id}}
                                    </option>
                                @endforeach
                            @endif


                        </select>
                        @if($errors->has("customer_id"))
                            <span class="help-block">{{ $errors->first("customer_id") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('sender_name')) has-error @endif hidden">
                        <label for="sender_name-field">Name: </label>
                        <input type="text" id="sender_name-field" name="sender_name" class="form-control"
                               value="{{ ! empty($order) ? $order->sender_name : old("sender_name") }}"/>
                        @if($errors->has("sender_name"))
                            <span class="help-block">{{ $errors->first("sender_name") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-2 col-sm-2 @if($errors->has('sender_mobile')) has-error @endif">
                        <label for="sender_mobile-field">Mobile: </label>
                        <input type="text" id="sender_mobile-field" name="sender_mobile" class="form-control"
                               value="{{ ! empty($order) ? $order->sender_mobile : old("sender_mobile") }}"/>
                        @if($errors->has("sender_mobile"))
                            <span class="help-block">{{ $errors->first("sender_mobile") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-2 col-sm-2 @if($errors->has('sender_phone')) has-error @endif">
                        <label for="sender_phone-field">Mobile 2: </label>
                        <input type="text" id="sender_phone-field" name="sender_phone" class="form-control"
                               value="{{ ! empty($order) ? $order->sender_phone : old("sender_phone") }}"/>
                        @if($errors->has("sender_phone"))
                            <span class="help-block">{{ $errors->first("sender_phone") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-3 col-sm-3">
                        <label class="control-label" for="s_government_id">Government</label>

                        <select class="form-control s_government_id" id="s_government_id" name="s_government_id"
                                data-id="{{ ! empty($order) ? $order->s_government_id : '' }}">
                            <option value="" data-display="Select">Government</option>
                            @if(! empty($governments))
                                @foreach($governments as $government)
                                    <option
                                        value="{{$government->id}}" {{ ! empty($order) && $order->s_government_id  == $government->id ? 'selected':  '' }} >
                                        {{$government->name_en}}
                                    </option>

                                @endforeach
                            @endif

                        </select>

                    </div>

                    <div class="form-group col-md-2 col-sm-2">
                        <label class="control-label" for="s_state_id">City</label>

                        <select class=" form-control s_state_id" id="s_state_id" name="s_state_id"
                                data-id="{{ ! empty($order) ? $order->s_state_id : '' }}">
                            @if(! empty($s_cities))

                                @foreach($s_cities as $s_city)
                                    <option
                                        value="{{$s_city->id}}" {{ ! empty($order) && $order->s_state_id  == $s_city->id ? 'selected':  '' }} >
                                        {{$s_city->name_en}}
                                    </option>

                                @endforeach
                            @endif

                        </select>

                    </div>


                    <div
                        class="form-group input-group col-md-2 col-sm-2 @if($errors->has('sender_latitude')) has-error @endif">
                        <label for="sender_latitude">Latitude: </label>
                        <input type="text" id="sender_latitude" name="sender_latitude" class="form-control"
                               value="{{ ! empty($order) ? $order->sender_latitude : '30.0668886' }}"/>
                        <span class="input-group-append">
                      <button class="input-group-text bg-transparent" type="button" data-toggle="modal"
                              data-target="#map2Modal"> <i class="fa fa-map-marker"></i></button>
                                    </span>
                        @if($errors->has("sender_latitude"))
                            <span class="help-block">{{ $errors->first("sender_latitude") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-2 col-sm-2 @if($errors->has('sender_longitude')) has-error @endif">
                        <label for="sender_longitude">Longitude: </label>
                        <input type="text" id="sender_longitude" name="sender_longitude" class="form-control"
                               value="{{ ! empty($order) ? $order->sender_longitude : '31.1962743' }}"/>
                        @if($errors->has("sender_longitude"))
                            <span class="help-block">{{ $errors->first("sender_longitude") }}</span>
                        @endif
                    </div>
                    <div
                        class="form-group input-group col-md-6 col-sm-6 @if($errors->has('sender_address')) has-error @endif">
                        <label for="sender_address">Sender Address: </label>
                        <input type="text" id="sender_address" name="sender_address" class="form-control"
                               value="{{ ! empty($order) ? $order->sender_address : old("sender_address") }}"/>
                        @if($errors->has("sender_address"))
                            <span class="help-block">{{ $errors->first("sender_address") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('reference_number')) has-error @endif" style="clear: both">
                        <label for="reference_number-field"> Reference Number: </label>
                        <input type="text" id="reference_number-field" name="reference_number" class="form-control"
                               value="{{ ! empty($order) ? $order->reference_number : old("reference_number") }}"/>
                        @if($errors->has("reference_number"))
                            <span class="help-block">{{ $errors->first("reference_number") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('delivery_type')) has-error @endif">
                        <label for="delivery_type-field"> Delivery Type: </label>
                        <input type="text" id="delivery_type-field" name="delivery_type" class="form-control"
                               value="{{ ! empty($order) ? $order->delivery_type : old("delivery_type") }}"/>
                        @if($errors->has("delivery_type"))
                            <span class="help-block">{{ $errors->first("delivery_type") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('sender_subphone')) has-error @endif">
                        <label for="sender_subphone-field"> Sender Subphone: </label>
                        <input type="text" id="sender_subphone-field" name="sender_subphone" class="form-control"
                               value="{{ ! empty($order) ? $order->sender_subphone : old("sender_subphone") }}"/>
                        @if($errors->has("sender_subphone"))
                            <span class="help-block">{{ $errors->first("sender_subphone") }}</span>
                        @endif
                    </div>

                </div>


                <div class="col-md-12 col-sm-12 col-xs-12" style="border: 2px solid #ea2;margin-bottom: 15px;">
                    <h4 style="color: #ea2;font-weight: bold">Receiver Data</h4>

                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('receiver_name')) has-error @endif">
                        <label for="receiver_name-field"> Name: </label>
                        <input type="text" id="receiver_name-field" name="receiver_name" class="form-control"
                               value="{{ ! empty($order) ? $order->receiver_name : old("receiver_name") }}"/>
                        @if($errors->has("receiver_name"))
                            <span class="help-block">{{ $errors->first("receiver_name") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('receiver_mobile')) has-error @endif">
                        <label for="reciever_mobile-field"> Mobile: </label>
                        <input type="text" id="receiver_mobile-field" name="receiver_mobile" class="form-control"
                               value="{{ ! empty($order) ? $order->receiver_mobile : old("receiver_mobile") }}"/>
                        @if($errors->has("receiver_mobile"))
                            <span class="help-block">{{ $errors->first("receiver_mobile") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-3 col-sm-3 @if($errors->has('receiver_phone')) has-error @endif">
                        <label for="receiver_phone-field"> Mobile 2:</label>
                        <input type="text" id="receiver_phone-field" name="receiver_phone" class="form-control"
                               value="{{ ! empty($order) ? $order->receiver_phone : old("receiver_phone") }}"/>
                        @if($errors->has("receiver_phone"))
                            <span class="help-block">{{ $errors->first("receiver_phone") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-3 col-sm-3">
                        <label class="control-label" for="r_government_id"> Government</label>

                        <select class="form-control r_government_id" id="r_government_id" name="r_government_id">
                            <option value="" data-display="Select">Receiver Government</option>
                            @if(! empty($governments))
                                @foreach($governments as $government)
                                    <option
                                        value="{{$government->id}}" {{ ! empty($order) && $order->r_government_id  == $government->id ? 'selected':  '' }} >
                                        {{$government->name_en}}
                                    </option>

                                @endforeach
                            @endif

                        </select>
                        <div class="has-error hidden" id="no_fees">
                            <span class="help-block">{{__('backend.no_fees')}}</span>
                        </div>
                    </div>

                    <div class="form-group col-md-2 col-sm-2">
                        <label class="control-label" for="r_state_id"> City</label>

                        <select class=" form-control r_state_id" id="r_state_id" name="r_state_id">
                            @if(! empty($r_cities))

                                @foreach($r_cities as $r_city)
                                    <option
                                        value="{{$r_city->id}}" {{ ! empty($order) && $order->r_state_id  == $r_city->id ? 'selected':  '' }} >
                                        {{$r_city->name_en}}
                                    </option>

                                @endforeach
                            @endif

                        </select>

                    </div>
                    <div
                        class="form-group input-group col-md-2 col-sm-2 @if($errors->has('receiver_latitude')) has-error @endif">
                        <label for="receiver_latitude"> Latitude: </label>
                        <input type="text" id="receiver_latitude" name="receiver_latitude" class="form-control"
                               value="{{ ! empty($order) ? $order->receiver_latitude :  '30.0668886'  }}"/>
                        <span class="input-group-append">
                  <button class="input-group-text bg-transparent" type="button" data-toggle="modal"
                          data-target="#map1Modal"> <i class="fa fa-map-marker"></i></button>
                </span>
                        @if($errors->has("receiver_latitude"))
                            <span class="help-block">{{ $errors->first("receiver_latitude") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-2 col-sm-2 @if($errors->has('receiver_longitude')) has-error @endif">
                        <label for="receiver_longitude"> Longitude: </label>
                        <input type="text" id="receiver_longitude" name="receiver_longitude" class="form-control"
                               value="{{ ! empty($order) ? $order->receiver_longitude :  '31.1962743'  }}"/>
                        @if($errors->has("receiver_longitude"))
                            <span class="help-block">{{ $errors->first("receiver_longitude") }}</span>
                        @endif
                    </div>


                    <div
                        class="form-group col-md-6 col-sm-6 @if($errors->has('receiver_address')) has-error @endif">
                        <label for="receiver_address">Receiver Address: </label>

                        <input type="text" id="receiver_address" name="receiver_address" class="form-control"
                               value="{{ ! empty($order) ? $order->receiver_address : old("receiver_address") }}"/>

                        @if($errors->has("receiver_address"))
                            <span class="help-block">{{ $errors->first("receiver_address") }}</span>
                        @endif
                    </div>

                </div>


                <div class="col-md-12 col-sm-12 col-xs-12" style="border: 2px solid #ea2;margin-bottom: 15px;">
                    <h4 style="color: #ea2;font-weight: bold">Order Data</h4>


                    {{--                    <div class="form-group col-md-2 col-sm-2 @if($errors->has('type')) has-error @endif"--}}
                    {{--                         style="min-height:80px">--}}
                    {{--                        <label for="type-field">Order Type</label>--}}


                    {{--                        <select class="form-control" id="order_type_id" name="order_type_id">--}}
                    {{--                            <option value="" data-display="Select">Type</option>--}}
                    {{--                            @if(! empty($types))--}}
                    {{--                                @foreach($types as $type)--}}
                    {{--                                    <option--}}
                    {{--                                        value="{{$type->id}}" {{ ! empty($order) && $order->order_type_id  == $type->id ? 'selected':  '' }} >--}}
                    {{--                                        {{$type->name_en}}--}}
                    {{--                                    </option>--}}

                    {{--                                @endforeach--}}
                    {{--                            @endif--}}

                    {{--                        </select>--}}

                    {{--                        @if($errors->has("type"))--}}
                    {{--                            <span class="help-block">{{ $errors->first("type") }}</span>--}}
                    {{--                        @endif--}}
                    {{--                    </div>--}}

                    <div class="form-group col-md-2 col-sm-2 @if($errors->has('type')) has-error @endif"
                         style="min-height:80px">
                        <label for="type-field">Type Name: </label>
                        <input type="text" id="type-field" name="type" class="form-control"
                               value="{{ ! empty($order) ? $order->type : old("type") }}"/>
                        @if($errors->has("type"))
                            <span class="help-block">{{ $errors->first("type") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-2 col-sm-2 @if($errors->has('main_store')) has-error @endif"
                         style="min-height:80px">
                        <label for="store_id-field">{{__('backend.store')}}:</label>
                        <select id="store_id-field" name="main_store" class="form-control">
                            <option value="0">{{__('backend.Choose_Store')}}</option>
                            @if(! empty($allStores))
                                @foreach($allStores as $store)
                                    <option
                                        value="{{$store->id}}" {{ $store->id == $order->main_store ? 'selected' : '' }} >
                                        {{$store->name}}
                                    </option>
                                @endforeach
                            @endif

                        </select>
                        @if($errors->has("main_store"))
                            <span class="help-block">{{ $errors->first("main_store") }}</span>
                        @endif
                    </div>


                    <div class="form-group col-md-2 col-sm-2 @if($errors->has('stock_id')) has-error @endif"
                         style="min-height:80px">
                        <label for="stock_id-field">Stock </label>
                        <select id="stock_id-field" name="store_id" class="form-control">
                            <option value="0">Choose Stock</option>
                            @if(! empty($stores))
                                @foreach($stores as $stock)
                                    <option
                                        value="{{$stock->id}}" {{ $stock->id == $order->store_id ? 'selected' : '' }} >
                                        {{$stock->title}}
                                    </option>
                                @endforeach
                            @endif

                        </select>
                        @if($errors->has("stock_id"))
                            <span class="help-block">{{ $errors->first("stock_id") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-2 col-sm-2 @if($errors->has('qty')) has-error @endif"
                         style="min-height:80px">
                        <label for="qty-field">Quantity Order </label>
                        <input type="number" id="qty-field" @if( ! empty($order->stock) ) max="{{$order->stock->qty}}"
                               @endif name="qty" class="form-control"
                               value="{{ ! empty($order) ? $order->qty : old("qty") }}"
                               @if(!empty($stores)) disabled @endif/>
                        {{__('backend.available_quantity')}}: <span id="available_quantity">0</span>
                        @if($errors->has("qty"))
                            <span class="help-block">{{ $errors->first("qty") }}</span>
                        @endif
                    </div>


                    <div class="form-group col-md-2 col-sm-2 @if($errors->has('overload')) has-error @endif"
                         style="min-height:80px">
                        <label for="overload-field">Overload </label>
                        <input type="number" id="overload-field" name="overload" class="form-control"
                               value="{{ ! empty($order) ? $order->overload : old("overload") }}"/>
                        @if($errors->has("overload"))
                            <span class="help-block">{{ $errors->first("overload") }}</span>
                        @endif
                    </div>


{{--                    <div class="form-group col-md-2 col-sm-2 @if($errors->has('agent_id')) has-error @endif"--}}
{{--                         style="min-height:80px">--}}
{{--                        <label for="agent_id-field">Agent </label>--}}

{{--                        <select id="agent_id-field" name="agent_id" class="form-control" disabled>--}}
{{--                            <option value="">Choose Agent</option>--}}
{{--                            @if(! empty($agents))--}}
{{--                                @foreach($agents as $agent)--}}
{{--                                    <option--}}
{{--                                        value="{{$agent->id}}" {{ ! empty($order) && $order->agent_id  == $agent->id ? 'selected':  '' }} >--}}
{{--                                        {{$agent->name}}--}}
{{--                                    </option>--}}
{{--                                @endforeach--}}
{{--                            @endif--}}

{{--                        </select>--}}
{{--                        @if($errors->has("agent_id"))--}}
{{--                            <span class="help-block">{{ $errors->first("agent_id") }}</span>--}}
{{--                        @endif--}}
{{--                    </div>--}}

                    <div class="form-group col-md-2 col-sm-2" style="min-height:80px">
                        <label class="control-label" for="driver_id">Driver</label>

                        <select class=" form-control driver_id" id="driver_id" name="forward_driver_id" disabled>
                            <option value="">Choose Drivers</option>
                            @if(! empty($drivers))
                                @foreach($drivers as $driver)
                                    <option
                                        value="{{$driver->id}}" {{ ! empty($order) && $order->forward_driver_id  == $driver->id ? 'selected':  '' }}>
                                        {{$driver->name}}
                                    </option>
                                @endforeach
                            @endif

                        </select>

                    </div>

                    <div class="form-group col-md-2 col-sm-2 @if($errors->has('delivery_price')) has-error @endif"
                         style="min-height:80px">
                        <label for="delivery_price-field">Delivery Fees: </label>
                        <input type="number" required class="form-control" id="delivery_price-field"
                               name="delivery_price"
                               value="{{  ! empty($order) ? $order->delivery_price : old("delivery_price") }}">
                        @if($errors->has("delivery_price"))
                            <span class="help-block">{{ $errors->first("delivery_price") }}</span>
                        @endif

                    </div>

                    <div class="form-group col-md-2 col-sm-2 @if($errors->has('order_price')) has-error @endif"
                         style="min-height:80px">
                        <label for="order_price-field">Order Price: </label>
                        <input type="number" class="form-control" id="order_price-field" name="order_price"
                               value="{{ ! empty($order) ? $order->order_price : old("order_price") }}">
                        @if($errors->has("order_price"))
                            <span class="help-block">{{ $errors->first("order_price") }}</span>
                        @endif
                    </div>


                    <div class="form-group col-md-2 col-sm-2 @if($errors->has('payment_method_id')) has-error @endif"
                         style="min-height:80px">
                        <label for="payment_method_id-field">Delivery Fees From:</label>
                        <select id="payment_method_id-field" name="payment_method_id" class="form-control">
                            <option value="" data-display="Select">Choose Method</option>

                            @if(! empty($payments))
                                @foreach($payments as $payment)
                                    <option
                                        value="{{$payment->id}}" {{ ! empty($order) && $order->payment_method_id  == $payment->id ? 'selected':  '' }} >
                                        {{$payment->name}}
                                    </option>
                                @endforeach
                            @endif

                        </select>
                        @if($errors->has("payment_method_id"))
                            <span class="help-block">{{ $errors->first("payment_method_id") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md2 col-sm-2 " style="min-height:80px">
                        <label for="payment_method_id-field"> {{__('backend.Total_Price')}}:</label>
                        <div class="form-control" id="total_price">{{$order->total_price}}</div>
                    </div>

                    <div class="form-group col-md-12 col-sm-12 @if($errors->has('notes')) has-error @endif"
                         style="min-height:80px">
                        <label for="notes-field">Notes: </label>

                        <input type="text" id="notes-field" name="notes" class="form-control"
                               value="{{ ! empty($order) ? $order->notes : old("notes") }}"/>

                        @if($errors->has("notes"))
                            <span class="help-block">{{ $errors->first("notes") }}</span>
                        @endif
                    </div>

                </div>

                <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
                    <button type="submit" class="btn btn-primary">{{__('backend.save_edits')}}</button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.orders.index') }}"><i
                            class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
                </div>
            </form>
        </div>
    </div>
    @include('backend.dialog-map1')
    @include('backend.dialog-map2')

@endsection
@section('scripts')
    <script type="text/javascript" src="{{asset('/assets/scripts/map-order.js')}}"></script>

    <script
        src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key=AIzaSyDQiN4qc7pC9b1BRQwkqXXD28peMfWcHvw&callback=myMap"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    @include('backend.orders.js')
@endsection

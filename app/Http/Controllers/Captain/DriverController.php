<?php namespace App\Http\Controllers\Captain;

use App\Http\Controllers\Controller\Backend;
use App\Mail\Verified;
use App\Models\Driver;
use App\Models\Truck;
use App\Models\Verification;
use Auth;
use Config;
use DB;
use File;
use Flash;
use Illuminate\Http\Request;
use Mail;
use Mapper;
use move;
use Response;
use Storage;

class DriverController extends Controller
{
    private $guardType;

    public function __construct(Request $request)
    {
        Config::set('auth.defaults.guard', 'captain');
        $this->guardType = Config::get('auth.defaults.guard');

    }

    // Get All Driver
    public function index(Request $request)
    {
        $user = Auth::guard($this->guardType)->user();


        $drivers = Driver::where('manager_id', $user->id)->orderBy('drivers.id', 'desc');

        $active = app('request')->input('active');
        $search = app('request')->input('search');
        if (isset($search) && $search != "") {
            $drivers = $drivers->where('name', 'like', '%' . $search . '%')->
            orWhere('mobile', 'like', '%' . $search . '%')->
            orWhere('national_id_number', 'like', '%' . $search . '%');
        }

        if (isset($active) && $active != -1) {
            if ($active == 0) { // Not Active
                $drivers = $drivers->where(function ($query) use ($active) {
                    $query->where('is_active', $active);
                    $query->orWhere('is_active', NULL);
                });
            } else { // Active
                $drivers = $drivers->where('is_active', $active);
            }
        }

        $drivers = $drivers->paginate(15);

        if ($request->ajax()) {
            return [
                'view' => view('captain.drivers.table', compact('drivers'))->render(),
            ];
        }
        return view('captain.drivers.index', compact('drivers'));

    }

    // View One Driver
    public function show($id)
    {
        $driver = Driver::findOrFail($id);
        $truck = Truck::where('driver_id', $driver->id)->first();
        $user = Auth::guard($this->guardType)->user();
        return view('captain.drivers.show', compact('driver'));
    }

    function download_papers($id, Request $request)
    {
        $driver = \DB::table('drivers')->where('id', $id)->first();

        $files = [];

        $image = public_path("/api_uploades/driver/profile/" . $driver->image);
        if ($driver->image && \File::exists($image) && \File::size($image) > 0) {
            $files[] = $image;
        }

        $criminal_record_image_front = public_path("/api_uploades/driver/cre_record/" . $driver->criminal_record_image_front);
        if ($driver->criminal_record_image_front && \File::exists($criminal_record_image_front) && \File::size($criminal_record_image_front) > 0) {
            $files[] = $criminal_record_image_front;
        }

        $driving_licence_image_front = public_path("/api_uploades/driver/driver_licence/" . $driver->driving_licence_image_front);
        if ($driver->driving_licence_image_front && \File::exists($driving_licence_image_front) && \File::size($driving_licence_image_front) > 0) {
            $files[] = $driving_licence_image_front;
        }

        $national_id_image_front = public_path("/api_uploades/driver/national_id_front/" . $driver->national_id_image_front);
        if ($driver->national_id_image_front && \File::exists($national_id_image_front) && \File::size($national_id_image_front) > 0) {
            $files[] = $national_id_image_front;
        }

        if (count($files)) {
            $file = public_path('api_uploades/driver/papers/driver_papers_' . $driver->id . '.zip');

            \Zipper::make($file)->add($files)->close();
            return response()->download($file, 'driver_papers_' . $driver->id . '.zip');
        }

    }

    // Added Driver Page
    public function create()
    {
        return view('captain.drivers.create');
    }

    // Add New Driver
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'mobile' => 'required|unique:drivers',
            'password' => 'required|min:5',
            'confirm_password' => 'required_with:password|same:password|min:5',
        ]);

        DB::beginTransaction();
        try {
            $driver = new Driver();
            $driver->name = $request->input("name");
            $driver->mobile = $request->input("mobile");
            $driver->password = $request->input("password");
            $driver->is_active = 1;
            $driver->is_verify_mobile = 1;
            $driver->manager_id = $user = Auth::guard($this->guardType)->user()->id;

            $driver->save();

            DB::commit();

            // start email function
            Flash::success(__('captain.Driver_saved_successfully'));

            return redirect()->route('captainDashboard.drivers.index');

        } catch (Exception $e) {
            DB::rollback();
            Flash::error(__('captain.Corporate_not_Saved_Something_Error'));

            return redirect()->route('captainDashboard.drivers.index');

        }

    }

    // Edit Driver Page
    public function edit($id)
    {
        $driver = Driver::findOrFail($id);
        return view('captain.drivers.edit', compact('driver'));
    }

    // Edit Driver
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'required',
            'mobile' => 'required|unique:drivers,mobile,' . $id,
        ]);

        $driver = Driver::findOrFail($id);
        $driver->name = $request->input("name");
        $driver->email = $request->input("email");

        if ($request->input("password") != "") {
            $driver->password = $request->input("password");
        }

        $driver->save();

        Flash::warning(__('captain.Driver_Updated_successfully'));
        return redirect()->route('captainDashboard.drivers.index');
    }

    function check_driver_mobile()
    {
        $mobile = \Request::get("mobile");
        $driver = Driver::where('mobile', $mobile)->first();
        if ($driver) {
            echo 'false';
        } else {
            echo 'true';
        }
    }

    function change_verify_account(Request $request)
    {
        $id = $request->id;
        $driver = Driver::findOrFail($id);
        if ($driver != null) {
            if ($driver->is_active == 0) {
                $driver->is_active = 1;
            } else {
                $driver->is_active = 0;
            }
            $driver->save();

            return $driver->is_active;
        } else {
            return 'error';
        }

    }
}

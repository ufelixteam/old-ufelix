<?php

namespace App\Http\ViewComposers;

use App\Models\Device;
use App\Models\Governorate;
use App\Models\Notification;
use App\Models\Setting;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class ProfileNotificationsComposer
{

    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        if (Auth::guard('Customer')->user() && (app('request')->is('customer/*', 'en/customer/*', 'ar/customer/*'))) {
            $count_notification = Notification::where('is_read', 0)->where('client_model', '2')->where('client_id', Auth::guard('Customer')->user()->id)->count();
            $app_notification = Notification::where('is_read', 0)->where('client_model', '2')->where('client_id', Auth::guard('Customer')->user()->id)->take(5)->get();
            $view->with('count_notification', $count_notification);
            $view->with('app_notification', $app_notification);
        }
    }
}

?>

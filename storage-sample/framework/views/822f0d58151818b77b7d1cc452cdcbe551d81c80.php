<div class="modal fade" id="pickupModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create Pick Up</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?php echo e(URL::asset('/mngrAdmin/pickups')); ?>" method="POST" id="pickup-form" class="order-form">


                    <div class="modal-body  " style="min-height: 420px">

                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        <input type="hidden" name="from" id="from-pick" value="0">
                        <input type="hidden" name="orderpickup[]" id="orderpickup" value="[]">

                        <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('store_id')): ?> has-error <?php endif; ?>">
                            <label for="store_id"><?php echo e(__('backend.droped_pickup_store')); ?>:</label>
                            <select id="store_id" name="store_id" class="form-control">
                                <option value="Null"><?php echo e(__('backend.Choose_Store')); ?></option>
                                <?php if(! empty($allStores)): ?>
                                    <?php $__currentLoopData = $allStores; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $store): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option
                                            value="<?php echo e($store->id); ?>" <?php echo e(! empty($pickup_store) && $store->id == $pickup_store->id ? 'selected' : ''); ?> >
                                            <?php echo e($store->name); ?>

                                        </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>

                            </select>
                            <?php if($errors->has("store_id")): ?>
                                <span class="help-block"><?php echo e($errors->first("store_id")); ?></span>
                            <?php endif; ?>
                        </div>
                        <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('receiver_name')): ?> has-error <?php endif; ?>">
                            <label for="receiver_name-field"> <?php echo e(__('backend.name')); ?>: </label>
                            <input type="text" required id="reciever_name-field" name="receiver_name"
                                   class="form-control"
                                   value=" <?php echo e(! empty($pickup_store) ? $pickup_store->name : ''); ?>"/>
                            <?php if($errors->has("receiver_name")): ?>
                                <span class="help-block"><?php echo e($errors->first("receiver_name")); ?></span>
                            <?php endif; ?>
                        </div>
                        <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('receiver_mobile')): ?> has-error <?php endif; ?>">
                            <label for="reciever_mobile-field"> <?php echo e(__('backend.mobile_number')); ?>: </label>
                            <input type="text" required id="receiver_mobile-field" name="receiver_mobile"
                                   class="form-control"
                                   value="<?php echo e(! empty($pickup_store) ? $pickup_store->mobile : ''); ?>"/>
                            <?php if($errors->has("receiver_mobile")): ?>
                                <span class="help-block"><?php echo e($errors->first("receiver_mobile")); ?></span>
                            <?php endif; ?>
                        </div>

                        <div class="form-group col-md-3 col-sm-3">
                            <label class="control-label" for="r_government_id"> <?php echo e(__('backend.government')); ?>:</label>

                            <select class="form-control pick_r_government_id" id="pick_r_government_id"
                                    name="r_government_id">
                                <option value="Null"
                                        data-display="Select"><?php echo e(__('backend.Receiver_Government')); ?></option>
                                <?php if(! empty($app_governments)): ?>
                                    <?php $__currentLoopData = $app_governments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $government): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option
                                            value="<?php echo e($government->id); ?>">
                                            <?php echo e($government->name_en); ?>

                                        </option>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>

                            </select>

                        </div>

                        <div class="form-group col-md-3 col-sm-3">
                            <label class="control-label" for="r_state_id"> <?php echo e(__('backend.city')); ?>:</label>

                            <select class=" form-control " id="pick_r_state_id" name="r_state_id">
                                <option value="Null"><?php echo e(__('backend.Receiver_City')); ?></option>

                            </select>

                        </div>
                        <div
                            class="form-group col-md-3 col-sm-3 <?php if($errors->has('receiver_latitude')): ?> has-error <?php endif; ?>">
                            <label for="receiver_latitude"> <?php echo e(__('backend.latitude')); ?>: </label>
                            <input type="text" required id="receiver_latitude" name="receiver_latitude"
                                   class="form-control"
                                   value="<?php echo e(! empty($pickup_store) ? $pickup_store->latitude : ''); ?>"/>
                            <?php if($errors->has("receiver_latitude")): ?>
                                <span class="help-block"><?php echo e($errors->first("receiver_latitude")); ?></span>
                            <?php endif; ?>
                        </div>
                        <div
                            class="form-group col-md-3 col-sm-3 <?php if($errors->has('receiver_longitude')): ?> has-error <?php endif; ?>">
                            <label for="reciever_longitude"> <?php echo e(__('backend.longitude')); ?>: </label>
                            <input type="text" required id="receiver_longitude" name="receiver_longitude"
                                   class="form-control"
                                   value="<?php echo e(! empty($pickup_store) ? $pickup_store->longitude : ''); ?>"/>
                            <?php if($errors->has("receiver_longitude")): ?>
                                <span class="help-block"><?php echo e($errors->first("receiver_longitude")); ?></span>
                            <?php endif; ?>
                        </div>


                        <div
                            class="form-group col-md-12 col-sm-12 <?php if($errors->has('receiver_address')): ?> has-error <?php endif; ?>">
                            <label for="reciever_address"><?php echo e(__('backend.Receiver_Address')); ?>: </label>
                            <input type="text" required id="receiver_address" name="receiver_address"
                                   class="form-control"
                                   value="<?php echo e(! empty($pickup_store) ? $pickup_store->address : ''); ?>"/>
                            <?php if($errors->has("receiver_address")): ?>
                                <span class="help-block"><?php echo e($errors->first("receiver_address")); ?></span>
                            <?php endif; ?>
                        </div>

                        <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('delivery_price')): ?> has-error <?php endif; ?>">
                            <label for="delivery_price-field"><?php echo e(__('backend.Delivery_Price')); ?>: </label>
                            <input type="number" required class="form-control" id="delivery_price-field"
                                   name="delivery_price"
                                   value="<?php echo e(old('delivery_price')  ? old('delivery_price')  : '0'); ?>">
                            <?php if($errors->has("delivery_price")): ?>
                                <span class="help-block"><?php echo e($errors->first("delivery_price")); ?></span>
                            <?php endif; ?>
                        </div>

                        <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('bonous')): ?> has-error <?php endif; ?>">
                            <label for="bonous-field"><?php echo e(__('backend.the_bonous')); ?>: </label>
                            <input type="number" required class="form-control" id="bonous-field" name="bonus_per_order"
                                   value="<?php echo e(old('bonous')  ? old('bonous')  : '0'); ?>">
                            <?php if($errors->has("bonous")): ?>
                                <span class="help-block"><?php echo e($errors->first("bonous")); ?></span>
                            <?php endif; ?>
                        </div>

                        <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('agent_id')): ?> has-error <?php endif; ?>">
                            <label for="agent_id-field"> <?php echo e(__('backend.agent')); ?>: </label>
                            <select id="pick_agent_id" name="agent_id" class="form-control">
                                <option value="Null"> <?php echo e(__('backend.Choose_Agent')); ?></option>
                                <?php if(! empty($agents)): ?>
                                    <?php $__currentLoopData = $agents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $agent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($agent->id); ?>">
                                            <?php echo e($agent->name); ?>

                                        </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>

                            </select>
                            <?php if($errors->has("agent_id")): ?>
                                <span class="help-block"><?php echo e($errors->first("agent_id")); ?></span>
                            <?php endif; ?>
                        </div>

                        <div class="form-group col-md-4 col-sm-4">
                            <label class="control-label" for="driver_id"> <?php echo e(__('backend.driver')); ?>:</label>

                            <select class=" form-control driver_id" id="pick_driver_id" name="driver_id">
                                <option value="Null"> <?php echo e(__('backend.Choose_Driver')); ?></option>
                                <?php if(! empty($drivers)): ?>

                                    <?php $__currentLoopData = $drivers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $driver1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($driver1->id); ?>">
                                            <?php echo e($driver1->name); ?>

                                        </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                <?php endif; ?>

                            </select>

                        </div>

                        <div class="group-control col-md-3 col-sm-3">
                            <label class="control-label"
                                   for="captain_received_date"> <?php echo e(__('backend.captain_received_date')); ?>:</label>

                            <div class='input-group date' id='datepickerFilter'>
                                <input type="date" id="date" name="captain_received_date" class="form-control" value=""
                                       required="required" data-date-format="YYYY-MM-DD"/>
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>


                        <div class="group-control col-md-3 col-sm-3">
                            <label class="control-label" for="driver_id"> <?php echo e(__('backend.captain_received_time')); ?>

                                :</label>

                            <div class='input-group date' id='datepickerFilter'>
                                <input type="time" name="captain_received_time" class="form-control" value=""
                                       required="required"/>
                                <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                            </div>
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="savePickup">Save</button>
                    </div>

                </form>
            </div>


        </div>
    </div>
</div>

<script src="<?php echo e(asset('/assets/vendor/jquery/jquery.min.js')); ?>"></script>

<script>

    $(document).ready(function () {
        var state_id = "<?php echo e($pickup_store->state_id); ?>";
        var from_store = true;
        var drivers_bonus = {};

        $("#savePickup").on('click', function () {
            var data = $("#pickup-form").serialize();
            $.ajax({
                url: "<?php echo e(url('/mngrAdmin/pickups')); ?>",
                type: 'post',
                data: data,
                success: function (data) {
                    if (data['status'] != 'false') {
                        $('#pickup-form')[0].reset();
                        $('#pickupModal').modal('hide');
                        window.location.href = '<?php echo e(url("/mngrAdmin/pickups")); ?>'
                    } else {
                        $('#errorId').html(data['message']);
                        $(".alert").show();
                        $('#pickupModal').modal('hide');
                    }

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $("#pick_agent_id").on('change', function () {
            $.ajax({
                url: "<?php echo e(url('/mngrAdmin/getdriver_by_agents')); ?>" + "/" + $(this).val(),
                type: 'get',
                data: {},
                success: function (data) {
                    drivers_bonus = {};

                    $('#pick_driver_id').html('<option value="" >Choose Drivers</option>');
                    $.each(data, function (i, content) {
                        $('#pick_driver_id').append($("<option></option>").attr("value", content.id).text(content.name));
                        drivers_bonus[content.id] = content.bouns_of_pickup_for_one_order ? content.bouns_of_pickup_for_one_order : 0;
                    });
                    console.log(drivers_bonus);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $("#pick_r_government_id").on('change', function () {
            console.log('Hereeeeeeee');
            $.ajax({
                url: "<?php echo e(url('/mngrAdmin/get_cities')); ?>" + "/" + $(this).val(),
                type: 'get',
                data: {},
                success: function (data) {
                    $('#pick_r_state_id').html('<option value="" >Sender City</option>');
                    $.each(data, function (i, content) {
                        $('#pick_r_state_id').append($("<option></option>").attr("value", content.id).text(content.city_name));
                    });
                    if (from_store) {
                        console.log('gffc');
                        $("#pick_r_state_id").val(state_id);
                        from_store = false;
                    }
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

            /* get price list*/
            $.ajax({
                url: "<?php echo e(url('/mngrAdmin/get_cost_pickup')); ?>" + "/" + $("#from-pick").val() + "/" + $(this).val(),
                type: 'get',
                data: {},
                success: function (data) {
                    if (data != "0") {
                        $('#delivery_price-field').val(data.cost);
                        $('#bonous-field').val(data.bonous);

                    }

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

        });

        $("#store_id").on('change', function () {
            from_store = true;
            $.ajax({
                url: "<?php echo e(url('/mngrAdmin/get_store')); ?>" + "/" + $(this).val(),
                type: 'get',
                data: {},
                success: function (data) {
                    $('#reciever_name-field').val(data.name);
                    $('#receiver_mobile-field').val(data.mobile);
                    $('#receiver_latitude').val(data.latitude);
                    $('#receiver_longitude').val(data.longitude);
                    $('#receiver_address').val(data.address);
                    $("#pick_r_government_id").val(data.governorate_id).trigger('change');
                    state_id = data.state_id;
                    // $("div.pick_r_government_id > select > option[value=" + data.governorate_id + "]").attr("selected", true);

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

        });

        $("#pick_driver_id").on('change', function (e) {
            const bonus = $(this).val() ? drivers_bonus[$(this).val()] : 0;
            $("#bonous-field").val(bonus);
        });

        $("#pick_r_government_id").val("<?php echo e($pickup_store->governorate_id); ?>").trigger('change');
    });

</script>

<?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/pickups/create_pickup_popup.blade.php ENDPATH**/ ?>
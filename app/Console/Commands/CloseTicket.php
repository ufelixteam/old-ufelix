<?php

namespace App\Console\Commands;

use App\Models\Governorate;
use App\Models\Order;
use App\Models\Ticket;
use App\Models\TicketRule;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CloseTicket extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ticket:close';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Close tickets generated automatically';

    protected $types = [];
    protected $gizaID;
    protected $cairoID;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->types = TicketRule::pluck('name', 'type')->toArray();

        $this->gizaID = Governorate::where('name_en', 'Giza')->value('id');
        $this->cairoID = Governorate::where('name_en', 'Cairo')->value('id');

        $tickets = Ticket::where('status', 0)
            ->where('is_manual', 0)
            ->whereNotNull('type')
            ->get();

        foreach ($tickets as $ticket) {
            $order = null;

            if ($ticket->type == 1) {
                $order = $this->rule1($ticket);
            } else if ($ticket->type == 2) {
                $order = $this->rule2($ticket);
            } else if ($ticket->type == 3) {
                $order = $this->rule3($ticket);
            } else if ($ticket->type == 4) {
                $order = $this->rule4($ticket);
            } else if ($ticket->type == 5) {
                $order = $this->rule5($ticket);
            } else if ($ticket->type == 6) {
                $order = $this->rule6($ticket);
            } else if ($ticket->type == 7) {
                $order = $this->rule7($ticket);
            } else if ($ticket->type == 8) {
                $order = $this->rule8($ticket);
            } else if ($ticket->type == 9) {
                $order = $this->rule9($ticket);
            } else if ($ticket->type == 10) {
                $order = $this->rule10($ticket);
            } else if ($ticket->type == 11) {
                $order = $this->rule11($ticket);
            }

            if (!$order) {
                $ticket->status = 1;
                $ticket->closed_at = Carbon::now();
                $ticket->save();
            }
        }
    }

    public function rule1($ticket)
    {
        return Order::where('status', 0)
            ->whereDate('created_at', '<', Carbon::now()->subDays(2)->toDateString())
            ->where('id', $ticket->order_id)
            ->first();
    }

    public function rule2($ticket)
    {
        return Order::where('status', 7)
            ->whereDate('dropped_at', '<', Carbon::now()->subDays(1)->toDateString())
            ->doesnthave('drivers')
            ->where('id', $ticket->order_id)
            ->first();
    }

    public function rule3($ticket)
    {
        return Order::where('status', 7)
            ->doesnthave('order_delay')
            ->doesnthave('delivery_problems')
            ->has('drivers')
            ->where('id', $ticket->order_id)
            ->first();
    }

    public function rule4($ticket)
    {
        return Order::where('status', 7)
            ->whereNotIn('r_government_id', [$this->gizaID, $this->cairoID])
            ->has('drivers')
            ->where('id', $ticket->order_id)
            ->first();
    }

    public function rule5($ticket)
    {
        $gizaID = $this->gizaID;
        $cairoID = $this->cairoID;

        return Order::where('status', 2)
            ->whereDate('received_at', '<', Carbon::now()->subDays(1)->toDateString())
            ->where(function ($query) use ($gizaID, $cairoID) {
                $query->where('r_government_id', $gizaID)
                    ->orWhere('r_government_id', $cairoID);
            })
            ->where('id', $ticket->order_id)
            ->first();
    }

    public function rule6($ticket)
    {
        return Order::where('status', 2)
            ->whereDate('received_at', '<', Carbon::now()->subDays(4)->toDateString())
            ->whereNotIn('r_government_id', [$this->gizaID, $this->cairoID])
            ->where('id', $ticket->order_id)
            ->first();
    }

    public function rule7($ticket)
    {
        return Order::where('status', 2)
            ->whereDate('received_at', '<=', Carbon::now()->subDays(1)->toDateString())
            ->where(function ($query) {
                $query->has('order_delay')
                    ->orHas('delivery_problems');
            })
            ->where('id', $ticket->order_id)
            ->first();
    }

    public function rule8($ticket)
    {
        return Order::where('status', 3)
            ->whereNull('delivery_status')
            ->where('id', $ticket->order_id)
            ->first();
    }

    public function rule9($ticket)
    {
        return Order::where('status', 5)
            ->whereDate('recalled_at', '<=', Carbon::now()->subDays(1)->toDateString())
            ->where(function ($query) {
                $query->where('warehouse_dropoff', 0)
                    ->orWhereNull('warehouse_dropoff');
            })
            ->where('id', $ticket->order_id)
            ->first();
    }

    public function rule10($ticket)
    {
        return Order::where('status', 5)
            ->whereDate('recalled_at', '<=', Carbon::now()->subDays(1)->toDateString())
            ->where('warehouse_dropoff', 1)
            ->where(function ($query) {
                $query->where('client_dropoff', 0)
                    ->orWhereNull('client_dropoff');
            })
            ->where('id', $ticket->order_id)
            ->first();
    }

    public function rule11($ticket)
    {
        return Order::where('status', 5)
            ->whereDate('recalled_at', '<=', Carbon::now()->subDays(1)->toDateString())
            ->where('warehouse_dropoff', 1)
            ->where('client_dropoff', 1)
            ->where('is_refund', 0)
            ->whereNotNull('refund_collection_id')
            ->where('id', $ticket->order_id)
            ->first();
    }
}

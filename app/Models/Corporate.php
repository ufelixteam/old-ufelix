<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Corporate extends Model
{
    public function getActiveSpanAttribute()
    {
        /*
        * is_active = 0 :: Not Active
        * is_active = 1 :: Active
        */
        if ($this->is_active == 1) {
            $value = "<span class='badge badge-pill label-success'>" . __('backend.active') . "</span>";
        } else {
            $value = "<span class='badge badge-pill label-danger'>" . __('backend.not_activated') . "</span>";
        }
        return $value;
    }

    public function getTransferMethodSpanAttribute()
    {

        if ($this->transfer_method == 1) {
            $value = __('backend.bank');
        } elseif ($this->transfer_method == 2) {
            $value = __('backend.mobicash');
        } elseif ($this->transfer_method == 3) {
            $value = __('backend.cash_with_captain');
        } elseif ($this->transfer_method == 4) {
            $value = __('backend.cash_in_office');
        }

        return $value;
    }

    public function getActiveTxtAttribute($value)
    {
        /*
        * is_active = 0 :: Not Active
        * is_active = 1 :: Active
        */
        if ($this->is_active == 0 || $this->is_active != 1) {
            $value = __('backend.active');
        } elseif ($this->is_active == 1) {
            $value = __('backend.not_activated');
        } else {
            $value = "";
        }
        return $value;
    }

    public function getCommercialRecordImageAttribute($value)
    {
        if ($value != "") {
            $value = asset("/api_uploades/client/corporate/commercial/" . $value);
        } else {
            $value = "";
        }
        return $value;
    }


    public function getLogoAttribute($value)
    {
        if ($value != "") {
            if (\File::exists(public_path("/api_uploades/client/corporate/commercial/" . $value))) {
                $value = asset("/api_uploades/client/corporate/commercial/" . $value);
            } else {
                $value = "";
            }
        } else {
            $value = "";
        }

        return $value;
    }

    public function getTypeOfShippingAttribute()
    {
        return $this->shipping_type == 1 ? __('backend.n_shipping') : __('backend.rt_shipping');
    }

    public function getTypeOfAccountAttribute()
    {

        $type = '';

        if ($this->account_type == 1) {
            $type = __('backend.mobile_app');
        } elseif ($this->account_type == 2) {
            $type = __('backend.website');
        } elseif ($this->account_type == 3) {
            $type = __('backend.dashboard');
        }

        return $type;
    }

    public function getIsSomethingWrongAttribute()
    {

        $result = false;

        if (!$this->responsible_id) {
            $result = true;
        }

        foreach ($this->customers as $customer) {
            if (!$customer->store_id) {
                $result = true;
            } elseif (!$customer->government_id) {
                $result = true;
            }
        }

        return $result;
    }

    public function prices()
    {
        return $this->hasMany('App\Models\CorporatePrice');
    }

    public function customers()
    {
        return $this->hasMany('App\Models\Customer');
    }

    public function manager()
    {
        return $this->hasMany('App\Models\Customer')->where('is_manager', 1/**/);
    }

    function orders()
    {
        return $this->hasManyThrough('App\Models\Order', 'App\Models\Customer');
    }

    public function wallet()
    {
        return $this->hasOne('App\Models\Wallet', 'object_id');
    }

    public function getLinkAttribute()
    {

        $url = url('/mngrAdmin/corporates/' . $this->id);
        return "<a href=" . $url . " />" . $this->name . "</a>";
    }

    public function type()
    {
        return $this->belongsTo('App\Models\OrderType', 'order_type');
    }

    public function responsible_user()
    {
        return $this->belongsTo('App\Models\User', 'responsible_id');
    }

    public function government()
    {
        return $this->belongsTo('App\Models\Governorate', 'government_id');
    }

    public function store()
    {
        return $this->belongsTo('App\Models\Store', 'store_id');
    }

    public static function boot()
    {

        parent::boot();

        Corporate::updated(function ($model) {
            if (($model->is_active != $model->getOriginal('is_active')) && $model->is_active == 1 && !$model->wallet) {
                Wallet::insert([
                    'type' => 2,
                    'object_id' => $model->id,
                    'status' => 1
                ]);
            }
        });

        Corporate::created(function ($model) {
            if ($model->is_active == 1) {
                Wallet::insert([
                    'type' => 2,
                    'object_id' => $model->id,
                    'status' => 1
                ]);
            }
        });

    }

}

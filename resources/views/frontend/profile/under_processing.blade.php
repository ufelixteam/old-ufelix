@extends('frontend.profile.layouts.app')
@section('title')
    @lang('website.wallets')
@endsection
@section('content')

    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card card-default">
                <div class="card-header">
                    <h4 class="card-title m-b-0">@lang('website.under_updating')... </h4>
                </div>
            </div>
        </div>
    </div>

@endsection

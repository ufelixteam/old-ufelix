<div class="row">
    <form>
        <div class="row" style="margin-bottom: 50px">
            <div class="col-md-3">
                <div id="imaginary_container">
                    <div class="input-group stylish-input-group">
                        <input type="text" class="form-control" id="search-field" name="search" value="{{app('request')->input('search')}}"
                               placeholder="{{__('backend.search')}} ">
                        <span class="input-group-addon">
                      <button type="submit" id="search-btn1">
                        <span class="glyphicon glyphicon-search"></span>
                      </button>
                    </span>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <select name="confirmed" class="form-control">
                    <option value="">{{__('backend.confirmed')}}</option>
                    <option value="2" @if(request()->confirmed == 2) selected @endif>{{__('backend.Yes')}}</option>
                    <option value="1" @if(request()->confirmed == 1) selected @endif>{{__('backend.No')}}</option>
                </select>
            </div>
            <div class="col-md-3">
                <div class="input-daterange input-group" id="datepicker">
                    <input type="text" class="form-control" name="start"
                           value="{{app('request')->input('start')}}" autocomplete="off"/>
                    <span class="input-group-addon">{{__('backend.to')}}</span>
                    <input type="text" class="form-control" name="end"
                           value="{{app('request')->input('end')}}" autocomplete="off"/>
                </div>
            </div>
            <div class="col-md-3">
                <button class="btn btn-success">
                    <i class="fa fa-filter"></i>
                    {{__('backend.filter')}}
                </button>
            </div>
        </div>
    </form>

    <div class="col-md-12 col-xs-12 col-sm-12">
        @if($collections->count())
            <table class="table table-condensed table-striped text-center">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('backend.serial_code')}}</th>
                    <th>{{__('backend.customer')}}</th>
                    <th>{{__('backend.corporate')}}</th>
                    <th>{{__('backend.count_orders')}}</th>
                    <th>{{__('backend.Create_at')}}</th>
                    <th>
                        <input type="checkbox" class="form-control"
                               style="display: inline-block;vertical-align: bottom;" id="printAll">
                        <a class="btn btn-xs btn btn btn-info" style="display: inline-block"
                           id="print_refund">
                            <i class="fa fa-print"></i> {{__('backend.excel')}}

                        </a>
                    </th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                @foreach($collections as $collection)
                    <tr>
                        <td>{{$collection->id}}</td>
                        <td>{{$collection->serial_code}}</td>
                        <td>{{isset($collection->customer->name) ? $collection->customer->name : '-'}}</td>
                        <td>{{isset($collection->customer->Corporate->name) ? $collection->customer->Corporate->name : '-'}}</td>
                        <td>{{$collection->orders_count}}</td>
                        <td>{{$collection->created_at}}</td>
                        <td class="selectrefund">
                        <input type="checkbox" class="selectprint" name="selectprint[]"
                               value="{{$collection->id}}"/>
                        </td>
                        <td>
                            <a class="btn btn-xs btn-primary "
                               href="{{ URL::asset('mngrAdmin/orders?refund_collection='.$collection->id) }}">
                                {{__('backend.Orders_List')}}
                            </a>
                            <a class="btn btn-xs btn-primary " target="_blank"
                               href="{{ URL::asset('mngrAdmin/print_refund?refund_collection_id='.$collection->id) }}">
                                {{__('backend.print')}}
                            </a>
{{--                            <a class="btn btn-xs btn-success " target="_blank"--}}
{{--                               href="{{ URL::asset('mngrAdmin/refund_excel?refund_collection_id='.$collection->id) }}">--}}
{{--                                {{__('backend.Excel')}}--}}
{{--                            </a>--}}
                            @if(!$collection->refund_status)
                                <a class="btn btn-xs btn-info "
                                   href="{{ URL::asset('mngrAdmin/refund_collections/refund_orders?collection_id='.$collection->id) }}">
                                    {{__('backend.confirm_drop_orders')}}
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {!! $collections->appends($_GET)->links() !!}
        @else
            <h3 class="text-center alert alert-warning">No Result Found !</h3>
        @endif
    </div>
</div>

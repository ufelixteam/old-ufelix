<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Customer;
use App\Models\Device;
use App\Models\Driver;
use App\Models\Notification;
use Auth;
use Carbon\Carbon;
use DB;
use File;
use Flash;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use Illuminate\Http\Request;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use Response;
use Validator;
use FCM;

class NotificationController extends Controller
{

    public function __construct(Request $request)
    {
        if ($request->is('mngrAdmin/*') || $request->is('admin/*')) {
            \Config::set('auth.defaults.guard', 'admin');
        } elseif ($request->is('mngrAgent/*') || $request->is('agent/*')) {
            \Config::set('auth.defaults.guard', 'agent');
        }
        $this->guardType = \Config::get('auth.defaults.guard');

    }

    // Get All Notifactions
    public function index()
    {
        $user = Auth::guard($this->guardType)->user();


        if ($user->is_warehouse_user) {
            $drivers = Notification::join("drivers", 'drivers.id', '=', 'notifications.client_id')
                ->join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                ->where('user_id', $user->id)
                ->where("client_model", 1)
                ->select("notifications.*");

            $customers = Notification::join("customers", 'customers.id', '=', 'notifications.client_id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id)
                ->where("client_model", 2)
                ->select("notifications.*");

            $users = Notification::join("users", 'users.id', '=', 'notifications.client_id')
                ->join('store_user', 'store_user.user_id', '=', 'users.id')
                ->where('user_id', $user->id)
                ->where("client_model", 3)
                ->select("notifications.*");

            $notifications = $drivers->union($customers)->union($users)->paginate(50);

        } else {
            $notifications = Notification::orderBy('id', 'desc')->paginate(50);
        }

        return view('backend.notifications.index', compact('notifications'));
    }

    // Create Notifaction Page
    public function create()
    {
        return view('backend.notifications.create');
    }

    // Added New Notifaction
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'title' => 'required',
            'message' => 'required',
            'type' => 'required',
            'client_model' => 'required|not_in:0',
            'devices' => 'required|not_in:0',
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        $notification = new Notification();
        $notification->title_ar = $request->input("title");
        $notification->message_ar = $request->input("message");
        $notification->title_en = $request->input("title");
        $notification->message_en = $request->input("message");
        $notification->date = $request->input("date");
        $notification->read_at = null;
        $notification->is_read = 0;
        $notification->published_by = Auth::user()->id;
        $notification->object_id = $request->input("object_id");
        $notification->type = $request->input("type");
        $notification->device_type = $request->input("devices");
        $notification->client_id = $request->input("client_id");
        $notification->client_model = $request->input("client_model");
        $notification->save();

        /////////////////////////////////////////////////////////////////////////////////////////////////////////// Add Activity Log By Samir
        if ($notification->client_model == 1) {
            $msg_en = 'Send Notification To Drivers #No ' . $notification->id;
            $msg_ar = ' ارسال اشعار الي الكباتن رقم ' . $notification->id;
        } elseif ($notification->client_model == 2) {
            $msg_en = 'Send Notification To Agents ' . $notification->id;
            $msg_ar = ' ارسال اشعار الي الوكلاء رقم ' . $notification->id;
        } elseif ($notification->client_model == 3) {
            $msg_en = 'Send Notification To Individuals ' . $notification->id;
            $msg_ar = ' ارسال اشعار الي العملاء رقم ' . $notification->id;
        } elseif ($notification->client_model == 4) {
            $msg_en = 'Send Notification To Corporates ' . $notification->id;
            $msg_ar = ' ارسال اشعار الي الشركات رقم ' . $notification->id;
        } else {
            $msg_en = 'Send Notification To All ' . $notification->id;
            $msg_ar = ' ارسال اشعار الي الكل رقم ' . $notification->id;
        }
        addActivityLog($msg_en, $msg_ar, 8); // Helper Function in App\Helpers.php
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        if ($notification->save()) {
            // Get Tokens Of Users By Tpe
//            $socket = [];
            $drivers= null;
            $customers= null;
            $client_model = $request->client_model;
            if ($request->client_model == 1 || $request->client_model == 5) {
                $client_model = 1;
                $drivers = Driver::with('devices')->get();
            }

            if (!empty($drivers)) {

                foreach ($drivers as $driver) {
                    $notification = new Notification();
                    $notification->title_ar = $request->input("title");
                    $notification->message_ar = $request->input("message");
                    $notification->title_en = $request->input("title");
                    $notification->message_en = $request->input("message");
                    $notification->date = $request->input("date");
                    $notification->read_at = null;
                    $notification->is_read = 0;
                    $notification->published_by = Auth::user()->id;
                    $notification->object_id = $request->input("object_id");
                    $notification->type = $request->input("type");
                    $notification->device_type = $request->input("devices");
                    $notification->client_id = $driver->id;
                    $notification->client_model = $client_model;
                    $notification->save();

                    $this->send_notification($driver, $request->input("title"), $request->input("message"));
                }
            }

            if ($request->client_model == 4 || $request->client_model == 5) {
                $client_model = 4;
                $customers = Customer::with('devices')->get();
            }

            if (!empty($customers)) {

                foreach ($customers as $customer) {
                    $notification = new Notification();
                    $notification->title_ar = $request->input("title");
                    $notification->message_ar = $request->input("message");
                    $notification->title_en = $request->input("title");
                    $notification->message_en = $request->input("message");
                    $notification->date = $request->input("date");
                    $notification->read_at = null;
                    $notification->is_read = 0;
                    $notification->published_by = Auth::user()->id;
                    $notification->object_id = $request->input("object_id");
                    $notification->type = $request->input("type");
                    $notification->device_type = $request->input("devices");
                    $notification->client_id = $customer->id;
                    $notification->client_model = $client_model;
                    $notification->save();

                    $this->send_notification($customer, $request->input("title"), $request->input("message"));
                }
            }
//
//            if ($request->client_model == 1) {
//                $users = Driver::with('devices')->where('is_active', 1)->get();
//                foreach ($users as $user) {
//                    $this->send_notification($user, $request->input("title"), $request->input("message"));
//                }
//
////                foreach (Driver::where('is_active', 1)->get() as $value) {
////                    $socket = $this->get_user_tokens_by_type($value->id, $notification->client_model);
////                    if (count($socket) > 0) {
////                        $pushed = $this->push_notify($socket, $request->devices, [
////                            'notification_type' => $request->type,
////                            'notification_type_title' => DB::table('notification_type')->where('id', $request->type)->value('title'),
////                            'title' => $request->input("title"),
////                            'message' => $request->input("message"),
////                            'date' => $request->date]);
////                    }
//////                    UserNotification::insert(['object_id' => $value->id, 'type' => $notification->client_model, 'notification_id' => $notification->id, 'is_read' => 0, 'status' => 0]);
////
////                }
//            } elseif ($request->client_model == 2) {
//                foreach (Agent::where('status', 1)->get() as $value) {
//                    $socket = $this->get_user_tokens_by_type($value->id, $notification->client_model);
//                    if (count($socket) > 0) {
//                        $pushed = $this->push_notify($socket, $request->devices, [
//                            'notification_type' => $request->type,
//                            'notification_type_title' => DB::table('notification_type')->
//                            where('id', $request->type)->
//                            value('title'), 'title' => $request->
//                            input("title"), 'message' => $request->
//                            input("message"), 'date' => $request->date]);
//                    }
//                    UserNotification::insert([
//                        'object_id' => $value->id,
//                        'type' => $notification->client_model,
//                        'notification_id' => $notification->id,
//                        'is_read' => 0,
//                        'status' => 0
//                    ]);
//                }
//            } elseif ($request->client_model == 3) {
//                foreach (Customer::where('is_active', 1)->get() as $value) {
//                    $socket = $this->get_user_tokens_by_type($value->id, $notification->client_model);
//                    if (count($socket) > 0) {
//                        $pushed = $this->push_notify($socket, $request->devices, [
//                            'notification_type' => $request->type,
//                            'notification_type_title' => DB::table('notification_type')->
//                            where('id', $request->type)->
//                            value('title'), 'title' => $request->
//                            input("title"), 'message' => $request->
//                            input("message"), 'date' => $request->date]);
//                    }
//                    UserNotification::insert([
//                        'object_id' => $value->id,
//                        'type' => $notification->client_model,
//                        'notification_id' => $notification->id,
//                        'is_read' => 0,
//                        'status' => 0
//                    ]);
//                }
//            } elseif ($request->client_model == 4) {
//                $users = Customer::with('devices')->join('corporates', 'corporates.id', '=', 'customers.corporate_id')
//                    ->where('corporates.is_active', 1)->get();
//                foreach ($users as $user) {
//                    $this->send_notification($user, $request->input("title"), $request->input("message"));
//                }
////                foreach (Corporate::where('is_active', 1)->get() as $value) {
////                    $socket = $this->get_user_tokens_by_type($value->id, $notification->client_model);
////                    if (count($socket) > 0) {
////                        $pushed = $this->push_notify($socket, $request->devices, [
////                            'notification_type' => $request->type,
////                            'notification_type_title' => DB::table('notification_type')->
////                            where('id', $request->type)->
////                            value('title'), 'title' => $request->
////                            input("title"), 'message' => $request->
////                            input("message"), 'date' => $request->date]);
////                    }
////                }
//            } elseif ($request->client_model == 5) {
//                foreach (Driver::where('is_active', 1)->get() as $value) {
//                    $socket = $this->get_user_tokens_by_type($value->id, $notification->client_model);
//                    if (count($socket) > 0) {
//                        $pushed = $this->push_notify($socket, $request->devices, [
//                            'notification_type' => $request->type,
//                            'notification_type_title' => DB::table('notification_type')->
//                            where('id', $request->type)->
//                            value('title'), 'title' => $request->
//                            input("title"), 'message' => $request->
//                            input("message"), 'date' => $request->date]);
//                    }
//                    UserNotification::insert([
//                        'object_id' => $value->id,
//                        'type' => $notification->client_model,
//                        'notification_id' => $notification->id,
//                        'is_read' => 0,
//                        'status' => 0
//                    ]);
//                }
//
////                foreach (Agent::where('status', 1)->get() as $value) {
////                    $socket = $this->get_user_tokens_by_type($value->id, $notification->client_model);
////
////                    if (count($socket) > 0) {
////                        $pushed = $this->push_notify($socket, $request->devices, [
////                            'notification_type' => $request->type,
////                            'notification_type_title' => DB::table('notification_type')->
////                            where('id', $request->type)->
////                            value('title'), 'title' => $request->
////                            input("title"), 'message' => $request->
////                            input("message"), 'date' => $request->date]);
////                    }
////                    UserNotification::insert([
////                        'object_id' => $value->id,
////                        'type' => $notification->client_model,
////                        'notification_id' => $notification->id,
////                        'is_read' => 0,
////                        'status' => 0
////                    ]);
////                }
//
//                foreach (Customer::where('is_active', 1)->get() as $value) {
//                    $socket = $this->get_user_tokens_by_type($value->id, $notification->client_model);
//
//                    if (count($socket) > 0) {
//                        $pushed = $this->push_notify($socket, $request->devices, [
//                            'notification_type' => $request->type,
//                            'notification_type_title' => DB::table('notification_type')->
//                            where('id', $request->type)->
//                            value('title'), 'title' => $request->
//                            input("title"), 'message' => $request->
//                            input("message"), 'date' => $request->date]);
//                    }
//                    UserNotification::insert([
//                        'object_id' => $value->id,
//                        'type' => $notification->client_model,
//                        'notification_id' => $notification->id,
//                        'is_read' => 0,
//                        'status' => 0
//                    ]);
//                }
//
////                foreach (Corporate::where('is_active', 1)->get() as $value) {
////                    $socket = $this->get_user_tokens_by_type($value->id, $notification->client_model);
////
////                    if (count($socket) > 0) {
////                        $pushed = $this->push_notify($socket, $request->devices, [
////                            'notification_type' => $request->type,
////                            'notification_type_title' => DB::table('notification_type')->
////                            where('id', $request->type)->
////                            value('title'), 'title' => $request->
////                            input("title"), 'message' => $request->
////                            input("message"), 'date' => $request->date]);
////                    }
////                    UserNotification::insert([
////                        'object_id' => $value->id,
////                        'type' => $notification->client_model,
////                        'notification_id' => $notification->id,
////                        'is_read' => 0,
////                        'status' => 0
////                    ]);
////                }
//            } else {
//                $socket = $this->get_user_tokens_by_type($notification->client_id, $notification->client_model);
//
//                if (count($socket) > 0) {
//                    $pushed = $this->push_notify($socket, $request->devices, [
//                        'notification_type' => $request->type,
//                        'notification_type_title' => DB::table('notification_type')->
//                        where('id', $request->type)->
//                        value('title'), 'title' => $request->
//                        input("title"), 'message' => $request->
//                        input("message"), 'date' => $request->date]);
//                }
//                UserNotification::insert([
//                    'object_id' => $notification->client_id,
//                    'type' => $notification->client_model,
//                    'notification_id' => $notification->id,
//                    'is_read' => 0,
//                    'status' => 0
//                ]);
//            }
//
        }

        Flash::success(__('backend.Notification_saved_successfully'));
        return redirect()->route('mngrAdmin.notifications.index');
    }

    // View One Notifaction

    public function get_user_tokens_by_type($id, $type)
    {
        # 1 for driver , 2 customer , 3 client
        return Device::where('object_id', $id)->where('object_type', $type)->pluck('socket_token')->toArray();
    }

    // Edit Notifaction Page

    public function push_notify($socket, $devices, $message)
    {

        $url = 'http://ufelix.com:3004/sendNotification';
        $ch = curl_init($url);
        # Setup request to send json via POST.


        $payload = json_encode(
            [
                [
                    'token' => $socket,
                    'message' => $message
                ]
            ]
        );

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        # Return response instead of printing.
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        # Send request.
        $result = curl_exec($ch);
        curl_close($ch);
        # Print response.
        //  dd($result) ;die();
        return $result;

    }

    // Update Notifaction

    public function show($id)
    {
        $notification = Notification::findOrFail($id);
        return view('backend.notifications.show', compact('notification'));
    }

    // Delete Notifaction

    public function edit($id)
    {
        $notification = Notification::findOrFail($id);
        return view('backend.notifications.edit', compact('notification'));
    }


    #push notify and save it in database

    public function update(Request $request, $id)
    {

        $v = Validator::make($request->all(), [
            'title' => 'required',
            'message' => 'required',
            'type' => 'required',
            'client_model' => 'required|not_in:0',
            'devices' => 'required|not_in:0',
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        $notification = Notification::findOrFail($id);
        $notification->title = $request->input("title");
        $notification->subject = $request->input("message");
        $notification->read_at = null;
        $notification->is_read = $request->input("is_read");
        $notification->object_id = $request->input("object_id");
        $notification->type = $request->input("type");
        $notification->client_id = $request->input("client_id");
        $notification->client_model = $request->input("client_model");
        $notification->save();

        /////////////////////////////////////////////////////////////////////////////////////////////////////////// Add Activity Log By Samir
        if ($notification->client_model == 1) {
            $msg_en = 'Edit Notification Drivers No# ' . $notification->id;
            $msg_ar = ' تعديل اشعار الكباتن رقم ' . $notification->id;
        } elseif ($notification->client_model == 2) {
            $msg_en = 'Edit Notification Agents No# ' . $notification->id;
            $msg_ar = ' تعديل اشعار الوكلاء رقم ' . $notification->id;
        } elseif ($notification->client_model == 3) {
            $msg_en = 'Edit Notification Individuals No# ' . $notification->id;
            $msg_ar = ' تعديل اشعار العملاء رقم ' . $notification->id;
        } elseif ($notification->client_model == 4) {
            $msg_en = 'Edit Notification Corporates No# ' . $notification->id;
            $msg_ar = ' تعديل اشعار الشركات رقم ' . $notification->id;
        } else {
            $msg_en = 'Edit Notification All' . $notification->id;
            $msg_ar = ' تعديل اشعار الكل ' . $notification->id;
        }
        addActivityLog($msg_en, $msg_ar, 8); // Helper Function in App\Helpers.php
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        Flash::warning(__('backend.Notification_Updated_successfully'));
        return redirect()->route('mngrAdmin.notifications.index');
    }

    //** push notification to server **/

    public function destroy($id)
    {
        $notification = Notification::findOrFail($id);
        $notification->delete();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Delete Notifaction #No ' . $notification->id, ' حذف الاشعار رقم ' . $notification->id, 8); // Helper Function in App\Helpers.php

        flash(__('backend.Notification_deleted_successfully'))->error();
        return redirect()->route('mngrAdmin.notifications.index');
    }

    public function new_notify($data)
    {
        /**
         * Notes
         * client_id :: driver id or customer id or admin id
         * client_model :: if client id is driver client_model = 1 , customer client_model = 2 , admin client_model =3
         * object_id :: if you want more data such as id of orders ,...
         */

        $notification = new Notification();
        $notification->title_ar = $data['title'];
        $notification->title_en = $data['title'];
        $notification->message_ar = $data['message'];
        $notification->message_en = $data['message'];
        $notification->read_at = null;
        $notification->is_read = 0;
        $notification->object_id = $data['object_id'];
        $notification->type = $data['type'];
        $notification->client_id = $data['client_id'];
        $notification->client_model = $data['client_model'];
        $notification->date = Carbon::now();
        $type = "2";

        if ($data['client_model'] == "1") {
            $type = "2";
        } else if ($data['client_model'] == "2") {
            $type = "1";

        } else {
            $type = "3";

        }

        if ($notification->save()) {
            #get tokens of users by tpe
            $socket = [];
            $socket = $this->get_user_tokens_by_type($data['client_id'], $type);

            $pushed = 'false';

            if (count($socket) > 0) {

                $tokens = [];
                if (count($socket) > 0) {
                    foreach ($socket as $token) {
                        array_push($tokens, ['socket_id' => $token, 'type' => $type]);
                    }
                    $pushed = $this->push_notify($tokens, $type, $notification);

                }
            }

            return $pushed;
        } else {
            return "false";
        }
    }

    public function test_push($id, $type)
    {
        return $this->push_notify($id, "Helllllwwwwwwwwwwwwwwwlllllllozzzz");
    }


    #get admin notifications
    public function admin_notifications()
    {
        $user = Auth::guard('admin')->user();
        $notifications = Notification::where('is_read', 0)->
        where('client_model', '3')->
        where('client_id', $user->id)->
        orderBy('created_at', 'DESC')->
        paginate(12);
        return view('backend.notifications.user_list', compact('notifications'));
    }


    public function AddType(Request $request)
    {
        $v = Validator::make($request->all(), [
            'title' => 'required',
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        //return $request->all();
        DB::table('notification_type')->insert(['title' => $request->title, 'created_at' => date('Y-m-d h:i:s')]);

        // Function Add Action In Activity Log By Samir
        addActivityLog('Add Notification Type ' . $request->title, ' اضافة نوع اشعار جديد ' . $request->title, 8); // Helper Function in App\Helpers.php

        flash(__('backend.Notification_Type_created_successfully'))->info();
        return redirect('mngrAdmin/notifications/types');
    }

    public function viewTypes(Request $request)
    {
        $types = DB::table('notification_type')->paginate(50);
        return view('backend.notifications.type', compact('types'));
    }


    public function viewType(Request $request, $id)
    {
        $type = DB::table('notification_type')->where('id', $id)->first();
        return view('backend.notifications.type', compact('type'));
    }

    public function UpdateType(Request $request, $id)
    {

        $v = Validator::make($request->all(), [
            'title' => 'required',
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        DB::table('notification_type')->where('id', $id)->update(['title' => $request->title, 'updated_at' => date('Y-m-d h:i:s')]);

        // Function Add Action In Activity Log By Samir
        addActivityLog('Edit Notification Type ' . $request->title, ' تعديل نوع الاشعار ' . $request->title, 8); // Helper Function in App\Helpers.php

        flash(__('backend.Notification_Type_updated_successfully'))->info();
        return redirect('mngrAdmin/notifications/types');
    }

    public function DestroyType(Request $request, $id)
    {

        $notification = DB::table('notification_type')->where('id', $id)->value('title');

        DB::table('notification_type')->where('id', $id)->delete();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Delete Notification Type ' . $notification, ' حذف نوع الاشعار ' . $notification, 8); // Helper Function in App\Helpers.php

        flash(__('backend.Notification_Type_Deleted_successfully'))->info();
        return redirect('mngrAdmin/notifications/types');
    }

    public function send_notification($user, $title, $message)
    {
        if (!empty($user->devices)) {

            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60 * 20);
            $optionBuilder->setPriority('high');
            $optionBuilder->setContentAvailable(true);

            $notificationBuilder = new PayloadNotificationBuilder($title);
            $notificationBuilder->setBody($message)
                ->setSound('default')
                ->setBadge(1);

            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData([
                'body' => $title,
                'title' => $message,
                'badge' => 1,
                'sound' => 'default',
            ]);

            $option = $optionBuilder->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();

            foreach ($user->devices as $device) {
                $token = $device->fcm_token;
                if($token){
                    $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
                }
            }
        }
    }
}

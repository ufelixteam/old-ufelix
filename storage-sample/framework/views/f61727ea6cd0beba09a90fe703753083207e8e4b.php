<?php $__env->startSection('css'); ?>
  <title><?php echo e(__('backend.the_orders')); ?> - <?php echo e(__('backend.scan')); ?></title>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('header'); ?>
  <div class="page-header">
    <h4>
      <i class="glyphicon glyphicon-plus"></i> <?php echo e(__('backend.scan')); ?> </h4>
  </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="container">

  <div class="row">
    <form class="form-horizontal  col-sm-12 text-center" id="scan-form">
      <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

        <div class="form-group col-sm-6 text-center">
            <label class="control-label" for="code-scan"> <?php echo e(__('backend.Scanned_Code')); ?></label>

                <input class="form-control" id="code-scan" type="text"  name="code" />

        </div>
        <div class="col-md-1 col-md-1 col-md-1">
            <label for="sort_by-field">. . . </label>
            <button type="button"  class="btn btn-warning" id="scan-btn"> <?php echo e(__('backend.scanned')); ?></button>

        </div>
    </form>
    </div>
</div>
<div class = "text-center list" id="table">

</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

 <script src="<?php echo e(asset('/assets/scripts/jquery-code-scanner.js')); ?>"></script>

 <script type="text/javascript">
    $(function(){
        $('#code-scan').codeScanner({
            onScan: function ($element, code) {
             //   console.log(code);
             $("#code-scan").val(barcode);
            }
        });
        $("#scan-btn").on('click',function(){
        	$('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
		    $.ajax({
		        url: '<?php echo e(URL::asset("/mngrAdmin/scan_qrcode")); ?>'+'?code='+$("#code-scan").val(),
		        type: 'get',
		        success: function(data) {
		            $('.list').html(data.view);
		        },
		        error: function(data) {
		            console.log('Error:', data);
		        }
		    });
        });

    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/orders/scan.blade.php ENDPATH**/ ?>
<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TypePermission;
use Flash;

class TypePermissionController extends Controller
{

    // Get All Permission Type
    public function index()
    {
        $typePermissions = TypePermission::orderBy('id', 'desc')->paginate(50);
        return view('backend.type_permission.index', compact('typePermissions'));
    }

    public function create()
    {
      //
    }

    // Added New Permission Type
    public function store(Request $request)
    {
      $this->validate($request,[
         'name'=>'required',
      ]);

      $typePermissions          = new TypePermission();
      $typePermissions->name    = $request->input("name");
      $typePermissions->save();

      // Function Add Action In Activity Log By Samir
      addActivityLog('Add New Permission Type ' . $typePermissions->name , ' اضافة نوع صلاحية جديدة ' . $typePermissions->name, 17); // Helper Function in App\Helpers.php

      Flash::success(__('backend.Type_Permission_saved_successfully'));
      return redirect()->route('mngrAdmin.type_permissions.index');
    }

    public function show($id)
    {
        //
    }

    function edit($id)
    {
      //
    }

    // Update Permission Type
    public function update(Request $request, $id)
    {
      $this->validate($request,[
         'name'=>'required',
      ]);

      $editTypePermissions          = TypePermission::find($id);
      $editTypePermissions->name    = $request->input("name");
      $editTypePermissions->save();

      // Function Add Action In Activity Log By Samir
      addActivityLog('Edit Permission Type ' . $editTypePermissions->name , ' تعديل نوع الصلاحية ' . $editTypePermissions->name, 17); // Helper Function in App\Helpers.php

      Flash::success(__('backend.Type_Permission_Updated_successfully'));
      return redirect()->route('mngrAdmin.type_permissions.index');
    }

    // Delete Permission Type
    public function destroy($id)
    {
        $delTypePermission = TypePermission::find($id);
        $delTypePermission->delete();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Delete Permission Type ' . $delTypePermission->name , ' حذف نوع الصلاحية ' . $delTypePermission->name, 17); // Helper Function in App\Helpers.php

        Flash::success(__('backend.Type_Permission_Deleted_successfully'));
        return redirect()->route('mngrAdmin.type_permissions.index');
    }

}

@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.Pricing_List')}} - {{__('backend.edit')}}</title>
@endsection

@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> {{__('backend.Edit_Price_List')}} - #{{$price_list->id}}</h1>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('mngrAdmin.price_lists.update', $price_list->id) }}" method="POST" name="validateForm">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group col-md-6 col-sm-12 @if($errors->has('target')) has-error @endif">
                       <label for="target-field">{{__('backend.target')}}</label>
                      <input type="number" id="target-field" name="target" class="form-control" value="{{ is_null(old("target")) ? $price_list->target : old("target") }}"/>
                       @if($errors->has("target"))
                        <span class="help-block">{{ $errors->first("target") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-12 @if($errors->has('discount_percentage')) has-error @endif">
                       <label for="discount_percentage-field">{{__('backend.discount_percentage')}}</label>
                       <div class='input-group  '>
                          <input type="text" id="discount_percentage-field" name="discount_percentage" class="form-control" value="{{ is_null(old("discount_percentage")) ? $price_list->discount_percentage : old("discount_percentage") }}"/>
                          <div class="input-group-addon">
                            <i class="fa fa-percent"></i>
                          </div>
                       </div>
                       @if($errors->has("discount_percentage"))
                        <span class="help-block">{{ $errors->first("discount_percentage") }}</span>
                       @endif
                    </div>
                <div class="well well-sm col-md-12 col-sm-12">
                    <button type="submit" class="btn btn-primary">{{__('backend.save_edits')}}</button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.price_lists.index') }}"><i class="glyphicon glyphicon-backward"></i>  {{__('backend.back')}}</a>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
   <script type="text/javascript">
  $(function() {
    $("form[name='validateForm']").validate({
      // Specify validation rules
      rules: {
        target: {
          required: true,
          number: true,
        },
        discount_percentage: {
          required: true,
          number: true,
        },
      },

      // Specify validation error messages
      messages: {
        target: {
          required: "{{__('backend.Please_Enter_The_Target')}}",
          number: "{{__('backend.Please_Enter_avalid_Target')}}",
        },
        discount_percentage: {
          required: "{{__('backend.Please_Enter_discount_percentage')}}",
          number: "{{__('backend.Please_Enter_avalid_discount_percentage')}}",
        },
      },

      errorPlacement: function(error, element) {
          $(element).parents('.form-group').append(error)
      },

      highlight: function (element) {
          $(element).parent().addClass('error')
      },

      unhighlight: function (element) {
          $(element).parent().removeClass('error')
      },
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function(form) {
          form.submit();
      }
    });
  })
</script>
@endsection

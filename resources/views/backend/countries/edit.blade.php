@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.countries')}} - {{__('backend.edit')}}</title>
@endsection

@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i> {{__('backend.Edit_Countries')}} - {{$country->name}}</h3>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <form action="{{ route('mngrAdmin.countries.update', $country->id) }}" method="POST" enctype="multipart/form-data" name="validateForm">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                <div class="col-md-6 form-group @if($errors->has('name')) has-error @endif">
                     <label for="name-field">{{__('backend.name')}}</label>
                     <input type="text" id="name-field" name="name" class="form-control" value="{{ is_null(old("name")) ? $country->name : old("name") }}"/>
                     @if($errors->has("name"))
                      <span class="help-block">{{ $errors->first("name") }}</span>
                     @endif
                  </div>
                  <div class="col-md-6 form-group @if($errors->has('code')) has-error @endif">
                     <label for="code-field">{{__('backend.code')}}</label>
                     <input type="text" id="code-field" name="code" class="form-control" value="{{ is_null(old("code")) ? $country->code : old("code") }}"/>
                     @if($errors->has("code"))
                      <span class="help-block">{{ $errors->first("code") }}</span>
                     @endif
                  </div>
                </div>
                <div class="form-group @if($errors->has('image')) has-error @endif">
                   <label for="image-field">{{__('backend.c_image')}}</label>
                   <input type="file" id="image-field" name="image" class="form-control"/>
                   @if($errors->has("image"))
                    <span class="help-block">{{ $errors->first("image") }}</span>
                   @endif
                </div>
                <div class="col-md-12 col-xs-12 text-center">
                    <img src="{{ is_null(old("image")) ? $country->image : old("image") }}" id="image-preview" width="50%">
                </div>
                <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
                    <button type="submit" class="btn btn-primary">{{__('backend.save_edits')}}</button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.countries.index') }}"><i class="glyphicon glyphicon-backward"></i>  {{__('backend.back')}}</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script type="text/javascript">
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('#image-preview').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
    }

    $(function(){
      $("#image-field").change(function() {
        readURL(this);
      });
    });

    $(function() {
    $("form[name='validateForm']").validate({
      // Specify validation rules
      rules: {
        name: {
          required: true,
        },
         code: {
          required: true,
        },

      },
      // Specify validation error messages
      messages: {
        name: {
          required: "{{__('backend.Please_Enter_Country_Name')}}",
        },
         code: {
          required: "{{__('backend.Please_Enter_Code_of_Country')}}",
        },
      },

       errorPlacement: function(error, element) {
          $(element).parents('.form-group').append(error)
      },

      highlight: function (element) {
          $(element).parent().addClass('error')
      },

      unhighlight: function (element) {
          $(element).parent().removeClass('error')
      },
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function(form) {
          form.submit();
      }
    });
  })
  </script>
@endsection

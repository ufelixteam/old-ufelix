@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.drivers')}} - {{__('backend.edit')}}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <link href="{{asset('assets/select2/select2.min.css')}}" rel="stylesheet">
    <style>
        .select2-container {
            width: 100% !important;
            display: block;
        }

        .error_vehicle {
            display: inline-block;
            color: rgb(255, 255, 255);
            background-color: #9c3737;
            width: 100%;
            padding: 5px;
            margin-top: 3px;
            border-radius: 2px;
            text-align: center;
            font-weight: 500;
        }

        .form-group {
            min-height: 94px;
            margin-bottom: 0px;
        }

        .imgDiv img {
            height: 100px;
            width: 100px;
            border-radius: 50%;
        }

        .input-group {
            -moz-box-shadow: none !important;
            -webkit-box-shadow: none !important;
            box-shadow: none !important;
        }

        .input-group {
            width: 100%;
        }

        .pac-container {
            z-index: 9999 !important;
        }
    </style>
@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit_driver')}} - {{$driver->name}}</h3>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <form action="{{ route('mngrAdmin.drivers.update', $driver->id) }}" method="POST"
                  enctype="multipart/form-data" name="driver_edit">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group col-md-4 col-sm-4 @if($errors->has('name')) has-error @endif">
                    <label for="name-field">{{__('backend.name')}}: </label>
                    <input type="text" id="name-field" name="name" class="form-control"
                           value="{{is_null(old("name")) ? $driver->name : old("name")}}"/>
                    @if($errors->has("name"))
                        <span class="help-block">{{ $errors->first("name") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-4 col-sm-4 @if($errors->has('email')) has-error @endif">
                    <label for="email-field">{{__('backend.email')}}: </label>
                    <input type="email" id="email-field" name="email" class="form-control"
                           value="{{is_null(old("email")) ? $driver->email : old("email")}}"/>
                    @if($errors->has("email"))
                        <span class="help-block">{{ $errors->first("email") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-4 col-sm-4 @if($errors->has('mobile')) has-error @endif">
                    <label for="mobile-field">{{__('backend.mobile_number')}}: </label>
                    <input type="text" id="mobile-field" name="mobile" maxlength="11" class="form-control"
                           value="{{is_null(old("mobile")) ? $driver->mobile : old("mobile")}}"/>
                    @if($errors->has("mobile"))
                        <span class="help-block">{{ $errors->first("mobile") }}</span>
                    @endif
                </div>

                {{--                <div class="form-group col-md-4 col-sm-4 @if($errors->has('phone')) has-error @endif">--}}
                {{--                    <label for="phone-field">{{__('backend.mobile_number2')}}: </label>--}}
                {{--                    <input type="text" id="phone-field" name="phone" maxlength="11" class="form-control"--}}
                {{--                           value="{{is_null(old("phone")) ? $driver->phone : old("phone")}}"/>--}}
                {{--                    @if($errors->has("phone"))--}}
                {{--                        <span class="help-block">{{ $errors->first("phone") }}</span>--}}
                {{--                    @endif--}}
                {{--                </div>--}}
                <div class="form-group col-md-4 col-sm-4 @if($errors->has('password')) has-error @endif">
                    <label for="password-field">{{__('backend.new_password')}}: </label>
                    <input type="password" id="password-field" name="password" class="form-control" value=""/>
                    @if($errors->has("password"))
                        <span class="help-block">{{ $errors->first("password") }}</span>
                    @endif
                </div>
                {{--                <div class="form-group col-md-4 col-sm-4 @if($errors->has('country_id')) has-error @endif">--}}
                {{--                    <label for="country_id-field">{{__('backend.choose_country')}}: </label>--}}
                {{--                    <select id="country_id-field" name="country_id" class="form-control">--}}
                {{--                        @if(! empty($countries))--}}
                {{--                            @foreach($countries as $country)--}}
                {{--                                <option--}}
                {{--                                    value="{{$country->id}}" {{ ($country->id) == $driver->country_id ? 'selected' : '' }}--}}
                {{--                                >{{$country->name}}</option>--}}
                {{--                            @endforeach--}}
                {{--                        @endif--}}
                {{--                    </select>--}}
                {{--                </div>--}}

                <div class="form-group col-md-4 col-sm-4 @if($errors->has('government_id')) has-error @endif">
                    <label for="government_id-field"> {{__('backend.government')}}: </label>
                    <select class="form-control government_id" id="government_id" name="government_id">
                        <option value="" data-display="Select">{{__('backend.government')}}</option>
                        @if(! empty($governments))
                            @foreach($governments as $government)
                                <option
                                    value="{{$government->id}}" {{$driver->government_id == $government->id ? 'selected' : ''}}>
                                    {{$government->name_en}}
                                </option>
                            @endforeach
                        @endif
                    </select>
                    @if($errors->has("government_id"))
                        <span class="help-block">{{ $errors->first("government_id") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-4 col-sm-4 @if($errors->has('city_id')) has-error @endif">
                    <label class="control-label" for="s_state_id">{{__('backend.city')}}</label>
                    <select class=" form-control city_id" id="city_id" name="city_id">
                        @foreach($cities as $city)
                            <option
                                value="{{$city->id}}" {{$city->governorate_id == $driver->government_id && $city->id == $driver->city_id ? 'selected' : ''}}>
                                {{$city->name_en}}
                            </option>
                        @endforeach
                    </select>
                    @if($errors->has("city_id"))
                        <span class="help-block">{{ $errors->first("city_id") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-4 col-sm-4 @if($errors->has('address')) has-error @endif">
                    <label for="address-field">{{__('backend.address')}}: </label>
                    <input type="text" id="address-field" name="address" class="form-control"
                           value="{{is_null(old("address")) ? $driver->address : old("address")}}"/>
                    @if($errors->has("address"))
                        <span class="help-block">{{ $errors->first("address") }}</span>
                    @endif
                </div>

                <div class="form-group col-md-4 col-sm-4 @if($errors->has('is_active')) has-error @endif">
                    <label for="is_active-field">{{__('backend.isactive_verified_notverified')}}:</label>
                    <select id="is_active-field" name="is_active" class="form-control" value="{{ old("is_active") }}">
                        <option
                            value="0" {{ old("is_active") == "0" || $driver->is_active == "0" ? 'selected' : '' }}>{{__('backend.no')}}</option>
                        <option
                            value="1" {{ old("is_active") == "1" || $driver->is_active == "1" ? 'selected' : '' }}>{{__('backend.yes')}}</option>
                    </select>
                    @if($errors->has("is_active"))
                        <span class="help-block">{{ $errors->first("is_active") }}</span>
                    @endif
                </div>

                <div class="form-group col-md-4 col-sm-4">
                    <label for="mobile-users">{{__('backend.store')}}</label>
                    <select class="form-control select2 disabled" id="store_id" name="store_id">
                        <option value="" data-dispaly="Select">{{__('backend.store')}}</option>
                        @if(! empty($stores))
                            @foreach($stores as $store)
                                <option value="{{$store->id}}"
                                        @if($driver->store_id == $store->id) selected @endif>{{$store->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>

                <div class="form-group col-md-4 col-sm-4">
                    <label for="manager-field">{{__('backend.is_manager')}}: </label>
                    <select name="is_manager" class="form-control" id="is_manager">
                        <option
                            value="0" {{ old("is_manager") == "0" ||  $driver->is_manager == "0" ? 'selected' : '' }}>{{__('backend.No')}}</option>
                        <option
                            value="1" {{ old("is_manager") == "1" ||  $driver->is_manager == "1" ? 'selected' : '' }}>{{__('backend.Yes')}}</option>
                    </select>
                </div>

                <div class="form-group col-md-4 col-sm-4">
                    <label for="mobile-users">{{__('backend.manager')}}</label>
                    <select class="form-control select2" id="manager_id" name="manager_id" {{ old("is_manager") == "1" ||  $driver->is_manager == "1" ? 'disabled' : ''}}>
                        <option value="" data-dispaly="Select">{{__('backend.manager')}}</option>
                        @if(! empty($drivers))
                            @foreach($drivers as $one)
                                <option value="{{$one->id}}"
                                        @if($driver->manager_id == $one->id) selected @endif>{{$one->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>

{{--                <div class="form-group col-md-4 col-sm-4 @if($errors->has('agent_id')) has-error @endif">--}}
{{--                    <label for="agent_id-field">{{__('backend.choose_beloning_agent')}}: </label>--}}
{{--                    <select id="agent_id-field" name="agent_id"--}}
{{--                            class="form-control" {{ $driver->has_orders > 0 ? 'disabled' : ''}}>--}}
{{--                        @if(! empty($agents))--}}
{{--                            @foreach($agents as $agent)--}}
{{--                                <option--}}
{{--                                    value="{{ $agent->id }}" {{ ($agent->id) == $driver->agent_id ? 'selected' : '' }}--}}
{{--                                >{{$agent->name}}</option>--}}
{{--                            @endforeach--}}
{{--                        @endif--}}
{{--                    </select>--}}
{{--                    @if($errors->has("agent_id"))--}}
{{--                        <span class="help-block">{{ $errors->first("agent_id") }}</span>--}}
{{--                    @endif--}}
{{--                </div>--}}
                <div class="form-group col-md-4 col-sm-4 @if($errors->has('notes')) has-error @endif">
                    <label for="notes-field">{{__('backend.notes')}}: </label>
                    <input type="text" id="notes-field" name="notes" class="form-control"
                           value="{{is_null(old("notes")) ? $driver->notes : old("notes")}}"/>
                    @if($errors->has("notes"))
                        <span class="help-block">{{ $errors->first("notes") }}</span>
                    @endif
                </div>


                <div class="form-group  col-md-4 col-sm-4 @if($errors->has('basic_salary')) has-error @endif">
                    <label for="basic-salary-field">{{__('backend.basic_salary')}}: </label>
                    <input type="number" id="basic-salary-field" min="0" name="basic_salary" class="form-control"
                           value="{{ $driver->basic_salary ? $driver->basic_salary : '0' }}"/>
                    <input type="hidden" value="{{$driver->basic_salary}}" name="old_basic_salary"/>
                    @if($errors->has("basic_salary"))
                        <span class="help-block">{{ $errors->first("basic_salary") }}</span>
                    @endif
                </div>
                <div
                    class="form-group  col-md-4 col-sm-4 @if($errors->has('bouns_of_delivery')) has-error @endif">
                    <label for="bouns-of-delivery-field">{{__('backend.bonus_of_delivery')}}: </label>
                    <input type="number" id="bouns-of-delivery-field" min="0" name="bouns_of_delivery"
                           class="form-control"
                           value="{{ $driver->bouns_of_delivery ? $driver->bouns_of_delivery : '0' }}"/>
                    <input type="hidden" value="{{$driver->bouns_of_delivery}}" name="old_bouns_of_delivery"/>
                    @if($errors->has("bouns_of_delivery"))
                        <span class="help-block">{{ $errors->first("bouns_of_delivery") }}</span>
                    @endif
                </div>

                <div class="form-group  col-md-4 col-sm-4 @if($errors->has('recall_price')) has-error @endif">
                    <label for="recall-price-field">{{__('backend.recall_price')}}: </label>
                    <input type="number" id="recall-price-field" min="0" name="recall_price" class="form-control"
                           value="{{ $driver->recall_price ? $driver->recall_price : '0' }}"/>
                    <input type="hidden" value="{{$driver->recall_price}}" name="old_recall_price"/>
                    @if($errors->has("recall_price"))
                        <span class="help-block">{{ $errors->first("recall_price") }}</span>
                    @endif
                </div>

                <div class="form-group  col-md-4 col-sm-4 @if($errors->has('reject_price')) has-error @endif">
                    <label for="reject-price-field">{{__('backend.reject_price')}}: </label>
                    <input type="number" id="reject-price-field" min="0" name="reject_price" class="form-control"
                           value="{{ $driver->reject_price ? $driver->reject_price : '0' }}"/>
                    <input type="hidden" value="{{$driver->reject_price}}" name="old_reject_price"/>
                    @if($errors->has("reject_price"))
                        <span class="help-block">{{ $errors->first("reject_price") }}</span>
                    @endif
                </div>

                <div class="form-group  col-md-4 col-sm-4 @if($errors->has('pickup_price')) has-error @endif">
                    <label for="pickup-price-field">{{__('backend.pickup_price')}}: </label>
                    <input type="number" id="pickup-price-field" min="0" name="pickup_price" class="form-control"
                           value="{{ $driver->pickup_price ? $driver->pickup_price : '0' }}"/>
                    <input type="hidden" value="{{$driver->pickup_price}}" name="old_pickup_price"/>
                    @if($errors->has("pickup_price"))
                        <span class="help-block">{{ $errors->first("pickup_price") }}</span>
                    @endif
                </div>

                <div
                    class="form-group  col-md-4 col-sm-4 @if($errors->has('bouns_of_pickup_for_one_order')) has-error @endif">
                    <label for="bouns-of-pickup-for-one-order-field">{{__('backend.bonus_of_pickup_for_one_order')}}
                        : </label>
                    <input type="number" id="bouns-of-pickup-for-one-order-field" min="0"
                           name="bouns_of_pickup_for_one_order" class="form-control"
                           value="{{ $driver->bouns_of_pickup_for_one_order ? $driver->bouns_of_pickup_for_one_order : '0' }}"/>
                    <input type="hidden" value="{{$driver->bouns_of_pickup_for_one_order}}"
                           name="old_bouns_of_pickup_for_one_order"/>
                    @if($errors->has("bouns_of_pickup_for_one_order"))
                        <span class="help-block">{{ $errors->first("bouns_of_pickup_for_one_order") }}</span>
                    @endif
                </div>
                <div class="form-group  col-md-4 col-sm-4 @if($errors->has('profit')) has-error @endif">
                    <label for="profit-field">{{__('backend.profit_rate')}}: </label>
                    <input type="number" min="0" id="profit-field" name="profit" class="form-control"
                           value="{{ $driver->profit ? $driver->profit : '0' }}"/>
                    <input type="hidden" value="{{$driver->profit}}" name="old_profit"/>
                    @if($errors->has("profit"))
                        <span class="help-block">{{ $errors->first("profit") }}</span>
                    @endif
                </div>

                <div class="form-group col-md-4 col-sm-4 @if($errors->has('national_id_number')) has-error @endif">
                    <label for="national_id_number-field">{{__('backend.national_id_number')}}: </label>
                    <input type="text" id="national_id_number-field" maxlength="14" name="national_id_number"
                           class="form-control"
                           value="{{is_null(old("national_id_number")) ? $driver->national_id_number : old("national_id_number") }}"/>
                    @if($errors->has("national_id_number"))
                        <span class="help-block">{{ $errors->first("national_id_number") }}</span>
                    @endif
                </div>
                <div
                    class="form-group col-md-4 col-sm-4 @if($errors->has('national_id_expired_date')) has-error @endif">
                    <label for="national_id_expired_date-field">{{__('backend.national_id_expired_date')}}: </label>
                    <div class='input-group date datepicker2'>
                        <input type="text" id="national_id_expired_date-field" name="national_id_expired_date"
                               class="form-control"
                               value="{{is_null(old("national_id_expired_date")) ? $driver->national_id_expired_date : old("national_id_expired_date")}}"/>
                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                    </div>
                    @if($errors->has("national_id_expired_date"))
                        <span class="help-block">{{ $errors->first("national_id_expired_date") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-4 col-sm-4 @if($errors->has('driver_has_vehicle')) has-error @endif">
                    <label for="driver-has-vehicle-field">{{__('backend.driver_has_vehicle')}}:</label>
                    <select id="driver-has-vehicle-field" name="driver_has_vehicle" class="form-control"
                            value="{{old("driver_has_vehicle")}}">
                        <option value="NULL">{{__('backend.choose_yes_or_no')}}</option>
                        <option value="0" {{ ! empty($driver->truck) ? '' : 'selected' }}>{{__('backend.no')}}</option>
                        <option value="1" {{ ! empty($driver->truck) ? 'selected' : '' }}>{{__('backend.yes')}}</option>
                    </select>
                    @if($errors->has("driver_has_vehicle"))
                        <span class="help-block">{{ $errors->first("driver_has_vehicle") }}</span>
                    @endif
                </div>

                <div class="form-group col-md-4 col-sm-4 @if($errors->has('shipping_types')) has-error @endif">
                    <label for="driver-has-vehicle-field">{{__('backend.shipping_type')}}:</label>
                    <div class="">
                        <label class="checkbox-inline">
                            <input type="checkbox" value="1" name="shipping_types[]"
                                   @if(in_array(1, $shipping_types)) checked @endif>{{__('backend.n_shipping')}}
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" value="2" name="shipping_types[]"
                                   @if(in_array(2, $shipping_types)) checked @endif>{{__('backend.rt_b2c_shipping')}}
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" value="3" name="shipping_types[]"
                                   @if(in_array(3, $shipping_types)) checked @endif>{{__('backend.rt_c2c_shipping')}}
                        </label>
                    </div>
                    @if($errors->has("shipping_types"))
                        <span class="help-block">{{ $errors->first("shipping_types") }}</span>
                    @endif
                </div>

                <div class="form-group col-md-4 col-sm-4 @if($errors->has('auto_close_invoice')) has-error @endif">
                    <label for="auto_close_invoice-field">{{__('backend.auto_close_invoice')}}:</label>
                    <select id="auto_close_invoicee-field" name="auto_close_invoice" class="form-control" value="{{ old("auto_close_invoice") }}">
                        <option
                            value="0" {{ old("auto_close_invoice") == "0" || $driver->auto_close_invoice == "0" ? 'selected' : '' }}>{{__('backend.no')}}</option>
                        <option
                            value="1" {{ old("auto_close_invoice") == "1" || $driver->auto_close_invoice == "1" ? 'selected' : '' }}>{{__('backend.yes')}}</option>
                    </select>
                    @if($errors->has("auto_close_invoice"))
                        <span class="help-block">{{ $errors->first("auto_close_invoice") }}</span>
                    @endif
                </div>

                <div class="form-group col-md-4 col-sm-4 @if($errors->has('transfer_method')) has-error @endif">
                    <label for="transfer_method">{{__('backend.transfer_method')}}</label>
                    <select id="transfer_method" name="transfer_method" class="form-control">
                        <option value="" data-display="Select">{{__('backend.choose_transfer_method')}}</option>
                        <option
                            value="1" {{ $driver->transfer_method == 1 ? 'selected' : '' }}>{{__('backend.bank')}}</option>
                        <option
                            value="2" {{ $driver->transfer_method == 2 ? 'selected' : '' }}>{{__('backend.mobicash')}}</option>
                        <option
                            value="4" {{ $driver->transfer_method == 4 ? 'selected' : '' }}>{{__('backend.cash_in_office')}}</option>
                    </select>
                    @if($errors->has("transfer_method"))
                        <span class="help-block">{{ $errors->first("transfer_method") }}</span>
                    @endif
                </div>

                <div
                    class="form-group col-md-4 col-sm-4 {{ $driver->transfer_method != 1 ? 'hidden' : '' }} @if($errors->has('bank_name')) has-error @endif">
                    <label for="bank_name">{{__('backend.bank_name')}}</label>
                    <input type="text" id="bank_name" name="bank_name" class="form-control"
                           value="{{$driver->bank_name}}"/>
                    @if($errors->has("bank_name"))
                        <span class="help-block">{{ $errors->first("bank_name") }}</span>
                    @endif
                </div>

                <div
                    class="form-group col-md-4 col-sm-4 {{ $driver->transfer_method != 1 ? 'hidden' : '' }} @if($errors->has('bank_account')) has-error @endif">
                    <label for="bank_name">{{__('backend.bank_account')}}</label>
                    <input type="text" id="bank_account" name="bank_account" class="form-control"
                           value="{{$driver->bank_account}}"/>
                    @if($errors->has("bank_account"))
                        <span class="help-block">{{ $errors->first("bank_account") }}</span>
                    @endif
                </div>

                <div
                    class="form-group col-md-4 col-sm-4 {{ $driver->transfer_method != 2 ? 'hidden' : '' }} @if($errors->has('mobile_transfer')) has-error @endif">
                    <label for="mobile_transfer">{{__('backend.mobile_number')}}</label>
                    <input type="text" id="mobile_transfer" name="mobile_transfer" class="form-control"
                           value="{{$driver->mobile_transfer}}"/>
                    @if($errors->has("mobile_transfer"))
                        <span class="help-block">{{ $errors->first("mobile_transfer") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-4 col-sm-4 @if($errors->has('latitude')) has-error @endif">
                    <div class=" input-group ">
                        <label for="receiver_latitude"> {{__('backend.latitude')}}: </label>
                        <input type="text" required id="latitude" name="latitude" class="form-control"
                               value="{{$driver->latitude}}"/>
                        <span class="input-group-append">
                            <button class="input-group-text bg-transparent" type="button" data-toggle="modal"
                                    data-target="#map1Modal"> <i class="fa fa-map-marker"></i></button>
                        </span>

                        @if($errors->has("latitude"))
                            <span class="help-block">{{ $errors->first("latitude") }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group col-md-4 col-sm-4 @if($errors->has('longitude')) has-error @endif">
                    <label for="longitude"> {{__('backend.longitude')}}: </label>
                    <input type="text" required id="longitude" name="longitude"
                           class="form-control"
                           value="{{$driver->longitude}}"/>
                    @if($errors->has("longitude"))
                        <span class="help-block">{{ $errors->first("longitude") }}</span>
                    @endif
                </div>


                <div class="row col-md-12">
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('driver_profile')) has-error @endif">
                        <div id="driver_profile_upload">
                            @if($driver->image)
                                <div class="imgDiv">
                                    <input type="hidden" value="{{ $driver->image }}" name="driver_profile_old">
                                    <img src="{{ $driver->image }}" style="width:100px;height:100px"
                                         onerror="this.src='{{asset('assets/images/user.png')}}'">
                                    <button class="btn btn-danger cancel">&times;</button>
                                </div>
                            @endif
                        </div>
                        <label for="driver_profile-field">{{__('backend.profile_image')}}: </label>
                        <input class="uploadPhoto btn btn-primary" type="file" name="driver_profile"
                               id="driver_profile">
                        @if($errors->has("driver_profile"))
                            <span class="help-block">{{ $errors->first("driver_profile") }}</span>
                        @endif
                    </div>
                    <div
                        class="form-group col-md-4 col-sm-4 @if($errors->has('national_id_image_front')) has-error @endif">
                        <div id="national_id_image_front_upload">
                            @if($driver->national_id_image_front)
                                <div class="imgDiv">
                                    <input type="hidden" value="{{ $driver->national_id_image_front}}"
                                           name="national_id_image_front_old">
                                    <img src="{{ $driver->national_id_image_front }}" style="width:100px;height:100px"
                                         onerror="this.src='{{asset('assets/images/image.png')}}'">
                                    <button class="btn btn-danger cancel">&times;</button>
                                </div>
                            @endif
                        </div>
                        <label for="national_id_image_front-field">{{__('backend.national_id_image_front')}}:</label>
                        <input class="uploadPhoto btn btn-primary" type="file" name="national_id_image_front"
                               id="national_id_image_front">
                        @if($errors->has("national_id_image_front"))
                            <span class="help-block">{{ $errors->first("national_id_image_front") }}</span>
                        @endif
                    </div>
                    <div
                        class="form-group col-md-4 col-sm-4 @if($errors->has('criminal_record_image_front')) has-error @endif">
                        <div id="criminal_record_image_front_upload">
                            @if($driver->criminal_record_image_front)
                                <div class="imgDiv">
                                    <input type="hidden" value="{{ $driver->criminal_record_image_front}}"
                                           name="criminal_record_image_front_old">
                                    <img src="{{ $driver->criminal_record_image_front }}"
                                         style="width:100px;height:100px"
                                         onerror="this.src='{{asset('assets/images/image.png')}}'">
                                    <button class="btn btn-danger cancel">&times;</button>
                                </div>
                            @endif
                        </div>
                        <label for="criminal_record_image_front-field">{{__('backend.criminal_record_image')}}: </label>
                        <input class="uploadPhoto btn btn-primary" type="file" name="criminal_record_image_front"
                               id="criminal_record_image_front">
                        @if($errors->has("criminal_record_image_front"))
                            <span class="help-block">{{ $errors->first("criminal_record_image_front") }}</span>
                        @endif
                    </div>

                    <div class="has_vechile" {{! empty($driver->truck) ?  '' : 'hidden' }}>
                        <input type="hidden" name="vehicle_id"
                               value="{{! empty($driver->truck) ? $driver->truck->id : '0'}}"/>
                        <div class="form-group col-md-6 col-sm-6 @if($errors->has('model_name')) has-error @endif">
                            <label for="model_name-field">{{__('backend.Model_Name')}}: </label>
                            <input type="text" id="model_name-field" name="model_name" class="form-control"
                                   value="{{ ! empty($driver->truck)? $driver->truck->model_name : old("model_name") }}"/>
                            @if($errors->has("model_name"))
                                <span class="help-block">{{ $errors->first("model_name") }}</span>
                            @endif
                        </div>
                        <div class="form-group col-md-6 col-sm-6 @if($errors->has('plate_number')) has-error @endif">
                            <label for="plate_number-field">{{__('backend.Plate_Number')}}: </label>
                            <input type="text" id="plate_number-field" name="plate_number" class="form-control"
                                   value="{{ ! empty($driver->truck) ? $driver->truck->plate_number : old("plate_number") }}"/>
                            @if($errors->has("plate_number"))
                                <span class="help-block">{{ $errors->first("plate_number") }}</span>
                            @endif
                        </div>

                        <div class="form-group col-md-6 col-sm-6 @if($errors->has('chassi_number')) has-error @endif">
                            <label for="chassi_number-field">{{__('backend.Chassi_Number')}}: </label>
                            <input type="text" id="chassi_number-field" name="chassi_number" class="form-control"
                                   value="{{ ! empty($driver->truck) ? $driver->truck->chassi_number : old("chassi_number") }}"/>
                            @if($errors->has("chassi_number"))
                                <span class="help-block">{{ $errors->first("chassi_number") }}</span>
                            @endif
                        </div>
                        <div class="form-group col-md-6 col-sm-6 @if($errors->has('year')) has-error @endif">
                            <label for="year-field">{{__('backend.Model_Year')}}: </label>
                            {{ Form::selectYear('year',  date('Y'),1980,null,["class"=>"form-control", "id"=>"year-field"]) }}
                            @if($errors->has("year"))
                                <span class="help-block">{{ $errors->first("year") }}</span>
                            @endif
                        </div>

                        <div class="form-group col-md-4 col-sm-4 @if($errors->has('type_id')) has-error @endif">
                            <label for="vehicle_type_id-field">{{__('backend.Vehicle_Type')}}: </label>
                            <select id="vehicle_type_id-field" name="vehicle_type_id" class="form-control">

                                @if(! empty($truck_types))
                                    <option value="NULL">{{__('backend.Vehicle_Type')}}:</option>

                                    @foreach($truck_types as $type)
                                        <option
                                            value="{{$type->id}}" {{ old("vehicle_type_id") == $type->id  || (! empty($driver->truck) && $driver->truck->vehicle_type_id == $type->id)  ? 'selected' : '' }}>{{$type->name_en}}</option>
                                    @endforeach
                                @endif
                            </select>
                            @if($errors->has("vehicle_type_id"))
                                <span class="help-block">{{ $errors->first("vehicle_type_id") }}</span>
                            @endif
                        </div>
                        <div class="form-group col-md-4 col-sm-4 @if($errors->has('model_type_id')) has-error @endif">
                            <label for="model_type_id-field">{{__('backend.Model_Type')}}: </label>

                            <select id="model_type_id-field" name="model_type_id" class="form-control">

                                @if(! empty($model_types))
                                    <option value="NULL">{{__('backend.Model_Type')}}:</option>

                                    @foreach($model_types as $model)
                                        <option
                                            value="{{$model->id}}" {{ old("model_type_id") == $model->id || (! empty($driver->truck) && $driver->truck->model_type_id == $model->id ) ? 'selected' : '' }}>{{$model->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                            @if($errors->has("model_type_id"))
                                <span class="help-block">{{ $errors->first("model_type_id") }}</span>
                            @endif
                        </div>

                        <div class="form-group col-md-4 col-sm-4 @if($errors->has('color_id')) has-error @endif">
                            <label for="color_id-field">{{__('backend.color')}}: </label>

                            <select id="color_id-field" name="color_id" class="form-control">
                                <option value="NULL">{{__('backend.color')}}:</option>

                                @if(! empty($colors))
                                    @foreach($colors as $color)
                                        <option
                                            value="{{$color->id}}" {{ ! empty($driver->truck) && $driver->truck->color_id == $color->id ? 'selected' : '' }}>{{$color->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                            @if($errors->has("color_id"))
                                <span class="help-block">{{ $errors->first("color_id") }}</span>
                            @endif
                        </div>

                        <div
                            class="form-group col-md-6 col-sm-6 @if($errors->has('driving_licence_expired_date')) has-error @endif">
                            <label
                                for="driving_licence_expired_date-field">{{__('backend.drivier_licence_expiration_date')}}
                                : </label>
                            <div class='input-group date datepicker2'>
                                <input type="text" id="driving_licence_expired_date-field"
                                       name="driving_licence_expired_date"
                                       class="form-control"
                                       value="{{is_null(old("driving_licence_expired_date")) ? $driver->driving_licence_expired_date : old("driving_licence_expired_date")}}"/>
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                            </div>
                            @if($errors->has("driving_licence_expired_date"))
                                <span class="help-block">{{ $errors->first("driving_licence_expired_date") }}</span>
                            @endif
                        </div>
                        <div
                            class="form-group col-md-6 col-sm-6 @if($errors->has('license_end_date')) has-error @endif">
                            <label for="license_end_date_field">{{__('backend.License_End_Date')}}: </label>
                            <div class='input-group date datepicker2'>
                                <input type="text" name="license_end_date" id="license_end_date_field"
                                       class="form-control date"
                                       value="{{ ! empty($driver->truck) && ! empty($driver->truck->license_end_date) ? $driver->truck->license_end_date : "" }}"/>

                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                            </div>
                            @if($errors->has("license_end_date"))
                                <span class="help-block">{{ $errors->first("license_end_date") }}</span>
                            @endif
                        </div>

                        <div
                            class="form-group col-md-6 col-sm-6 @if($errors->has('driving_licence_image_front')) has-error @endif">
                            <div id="driving_licence_image_front_upload">
                                @if($driver->driving_licence_image_front)
                                    <div class="imgDiv">
                                        <input type="hidden" value="{{ $driver->driving_licence_image_front}}"
                                               name="driving_licence_image_front_old">
                                        <img src="{{$driver->driving_licence_image_front}}"
                                             style="width:100px;height:100px"
                                             onerror="this.src='{{asset('assets/images/image.png')}}'">
                                        <button class="btn btn-danger cancel">&times;</button>
                                    </div>
                                @endif
                            </div>
                            <label for="driving_licence_image_front-field">{{__('backend.drivier_licence_image_front')}}
                                : </label>
                            <input class="uploadPhoto btn btn-primary" type="file" name="driving_licence_image_front"
                                   id="driving_licence_image_front">
                            @if($errors->has("driving_licence_image_front"))
                                <span class="help-block">{{ $errors->first("driving_licence_image_front") }}</span>
                            @endif
                        </div>
                        <div
                            class="form-group col-md-6 col-sm-6 @if($errors->has('license_image_front')) has-error @endif">
                            <div id="license_image_front_upload">
                                @if(! empty($driver->truck) && $driver->truck->license_image_front)
                                    <div class="imgDiv">
                                        <input type="hidden"
                                               value="{{ ! empty($driver->truck) && $driver->truck->license_image_front ? $driver->truck->license_image_front : '' }}"
                                               name="license_image_front_old" id="license_image_front_old">
                                        <img src="{{ $driver->truck->license_image_front }}"
                                             style="width:100px;height:100px"
                                             onerror="this.src='{{asset('assets/images/image.png')}}'">
                                        <button class="btn btn-danger cancel">&times;</button>
                                    </div>
                                @endif
                            </div>
                            <label for="image-field">{{__('backend.License_Image_Front')}}</label>
                            <input class="uploadPhoto btn btn-primary" type="file" name="license_image_front"
                                   id="license_image_front">
                            @if($errors->has("license_image_front"))
                                <span class="help-block">{{ $errors->first("license_image_front") }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="well well-sm col-md-12 col-xs-12 col-sm-12" style="margin-top: 20px">
                    <button type="submit" id="added_driver"
                            class="btn btn-primary">{{__('backend.save_edits')}}</button>
                    <a class="btn btn-link pull-right" href="{{route('mngrAdmin.drivers.index')}}"><i
                            class="glyphicon glyphicon-backward"></i>{{__('backend.back')}}</a>
                </div>


                <input type="hidden" name="driver_id" value="{{$driver->id}}">

            </form>
        </div>
    </div>

    @include('backend.map')
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

    <script type="text/javascript" src="{{asset('/assets/scripts/map.js')}}"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key=AIzaSyDQiN4qc7pC9b1BRQwkqXXD28peMfWcHvw&callback=myMap"></script>

    <script src="{{asset('assets/select2/select2.min.js')}}"></script>

    <script type="text/javascript">
        $(function () {
            $("#is_manager").change(function () {
                if($(this).val() == 1){
                    // $("#manager_id").select2("readonly", true);
                    $('#manager_id').val(""); // Select the option with a value of '1'
                    $('#manager_id').prop('disabled', true); // Select the option with a value of '1'
                    // $("#manager_id").val("").prop('readonly', true);
                }else{
                    $('#manager_id').prop('disabled', false);
                }

                $('#manager_id').trigger('change'); // Notify any JS components that the value changed

            });

            $('.select2').select2();
            $("#transfer_method").on('change', function () {
                if ($(this).val() == 1) {
                    $("#bank_name").parent().removeClass('hidden');
                    $("#bank_account").parent().removeClass('hidden');
                    $("#mobile_transfer").parent().addClass('hidden');
                } else if ($(this).val() == 2) {
                    $("#bank_name").parent().addClass('hidden');
                    $("#bank_account").parent().addClass('hidden');
                    $("#mobile_transfer").parent().removeClass('hidden');
                } else {
                    $("#bank_name").parent().addClass('hidden');
                    $("#bank_account").parent().addClass('hidden');
                    $("#mobile_transfer").parent().addClass('hidden');
                }
            });

            $("form[name='driver_edit']").validate({
// Specify validation rules
                rules: {
                    name: {
                        required: true,
                    },
                    email: {
                        required: true,
                        email: true,

                    },
                    mobile: {
                        required: true,
                        number: true,
                        digits: true,
                        rangelength: [11, 11],
                    },
                    // phone: {
                    //     number: true,
                    //     rangelength: [11, 11],
                    //
                    // },
                    // password: {
                    //     required: true,
                    //     minlength: 5
                    // },
                    // confirm_password: {
                    //     required: true,
                    //     equalTo: '#password-field',
                    //     minlength: 5
                    // },
                    // country_id: {
                    //     required: true,
                    // },
                    government_id: {
                        required: true,
                    },
                    city_id: {
                        required: true,
                    },
                    address: {
                        required: true,
                    },
                    // agent_id: {
                    //     required: true,
                    // },
                    national_id_number: {
                        required: true,
                        number: true,
                        rangelength: [14, 14],
                    },
                    national_id_expired_date: {
                        date: true,
                        //  required: true,
                    },
                    profit: {
                        //required: true,
                        number: true,
                    },
                    basic_salary: {
                        //required: true,
                        number: true,
                    },
                    bouns_of_delivery: {
                        // required: true,
                        number: true,
                    },
                    recall_price: {
                        // required: true,
                        number: true,
                    },
                    reject_price: {
                        // required: true,
                        number: true,
                    },
                    pickup_price: {
                        // required: true,
                        number: true,
                    },
                    bouns_of_pickup_for_one_order: {
                        // required: true,
                        number: true,
                    },
                    driver_has_vehicle: {
                        required: true,
                    },
                    'shipping_types[]': {
                        required: true,
                        minlength: 1
                    }
                    // driver_profile: {
                    //     required: true,
                    // },
                    // national_id_image_front: {
                    //     required: true,
                    // },
                    // criminal_record_image_front: {
                    //     required: true,
                    // },
                    // driving_licence_image_front: {
                    //     required: true,
                    // },
                    // driving_licence_image_back: {
                    //     required: true,
                    // },
                    // national_id_image_back: {
                    //     required: true,
                    // },
                },

// Specify validation error messages
                messages: {
                    name: {
                        required: "{{__('backend.Please_Enter_driver_Name')}}",
                    },
                    email: {
                        required: "{{__('backend.Please_Enter_Driver_E-mail')}}",
                        email: "{{__('backend.Please_Enter_Correct_E-mail')}}"
                    },
                    mobile: {
                        rangelength: "{{__('backend.Mobile_Must_Be11_Digits')}}",
                        required: "{{__('backend.Please_Enter_Driver_Mobile_Number')}}",
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}"

                    },
                    {{--phone: {--}}
                        {{--    number: "{{__('backend.Please_Enter_Avalid_Number')}}",--}}
                        {{--    rangelength: "{{__('backend.Mobile_Must_Be11_Digits')}}"--}}
                        {{--},--}}
                        {{--password: {--}}
                        {{--    required: "{{__('backend.Please_Enter_Driver_Password')}}",--}}
                        {{--    minlength: "{{__('backend.Password_must_be_more_than5_character')}}"--}}
                        {{--},--}}
                        {{--confirm_password: {--}}
                        {{--    required: "{{__('backend.Confirm_password_is_wrong')}}",--}}
                        {{--    equalTo: "{{__('backend.Confirm_password_is_wrong')}}",--}}
                        {{--},--}}
                        {{--country_id: {--}}
                        {{--    required: "{{__('backend.Please_Chosse_The_Country')}}",--}}
                        {{--},--}}
                    government_id: {
                        required: "{{__('backend.Please_Chosse_The_Government')}}",
                    },
                    city_id: {
                        required: "{{__('backend.Please_Select_City')}}",
                    },
                    address: {
                        required: "{{__('backend.Please_Enter_Driver_Address')}}",
                    },
                    latitude: {
                        required: "{{__('backend.Please_Enter_Latitude')}}",
                        number: "{{__('backend.Latitude_must_be_numbers')}}",
                    },
                    longitude: {
                        required: "{{__('backend.Please_Enter_Longitude')}}",
                        number: "{{__('backend.Longitude_must_be_numbers')}}",
                    },
                    {{--agent_id: {--}}
                    {{--    required: "{{__('backend.Please_Select_Agent')}}",--}}
                    {{--},--}}
                    national_id_number: {
                        required: "{{__('backend.Please_Enter_National_Id_Number')}}",
                        number: "{{__('backend.National_Id_Must_Be_Number')}}",
                        rangelength: "{{__('backend.National_Id_Must_Be14_Digits')}}",
                    },
                    national_id_expired_date: {
                        date: "{{__('backend.Please_Enter_Correct_Date_Format')}}",
                        required: "{{__('backend.Please_Enter_National_Id_Expired_Date')}}",
                    },
                    profit: {
                        required: "{{__('backend.Please_Enter_Profit')}}",
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                    },
                    basic_salary: {
                        required: "{{__('backend.Please_Enter_Basic_Salary')}}",
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                    },
                    bouns_of_delivery: {
                        // required: "{{__('backend.Please_Enter_Bonus_Of_Delivery')}}",
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                    },
                    recall_price: {
                        // required: "{{__('backend.Please_Enter_Recall_Price')}}",
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                    },
                    reject_price: {
                        // required: "{{__('backend.Please_Enter_Reject_Price')}}",
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                    },
                    pickup_price: {
                        // required: "{{__('backend.Please_Enter_Pickup_Price')}}",
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                    },
                    bouns_of_pickup_for_one_order: {
                        // required: "{{__('backend.Please_Enter_Bonus_Of_Pickup_For_One_Order')}}",
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                    },
                    driver_has_vehicle: {
                        required: "{{__('backend.Please_Choose_If_Driver_Has_Vehicle_or_No')}}"
                    },
                    driver_profile: {
                        required: "{{__('backend.Please_Enter_Driver_Profile_Image')}}"
                    },
                    national_id_image_front: {
                        required: "{{__('backend.Please_Enter_drivier_national_id_image_front')}}"
                    },
                    criminal_record_image_front: {
                        required: "{{__('backend.please_enter_criminal_record_image_driver')}}"
                    },
                    'shipping_types[]': {
                        required: "{{__('backend.Please_Choose_One_Shipping_Type')}}",
                        minlength: "{{__('backend.Please_Choose_One_Shipping_Type')}}"
                    }
                },
                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                },
                highlight: function (element) {
                    $(element).parent().addClass('error')
                },
                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                }
            });
            $('#add_vehicle').css('display', 'none');

            $("#driver-has-vehicle-field").on('change', function () {
                if ($(this).val() == 1) {

                    $('.has_vechile').show();


                    // $('#model_name-field', '#plate_number-field', '#chassi_number-field', '#year-field', '#vehicle_type_id-field', '#model_type_id-field', '#color_id-field', '#driving_licence_expired_date-field', '#license_end_date_field', '#driving_licence_image_front', '#license_image_front').blur();

                    $(".done_create_vehicle").click(function (e) {

                        var modelNameError = true,
                            plateNumberError = true,
                            chassiNumberError = true,
                            yearError = true,
                            vehicleTypeError = true,
                            modelTypeError = true,
                            colorError = true,
                            drivingLicenceDateError = true,
                            licenseEndDateError = true,
                            drivingLicenceImageError = true,
                            licenseImageError = true;

                        if ($('#model_name-field').val() == '') {
                            modelNameError = true;
                        } else {
                            modelNameError = false;
                        }

                        if ($('#plate_number-field').val() == '') {
                            plateNumberError = true;
                        } else {
                            plateNumberError = false;
                        }

                        if ($('#chassi_number-field').val() == '') {
                            chassiNumberError = true;
                        } else {
                            chassiNumberError = false;
                        }

                        if ($('#year-field').val() == '') {
                            yearError = true;
                        } else {
                            yearError = false;
                        }

                        if ($('#vehicle_type_id-field').val() == '') {
                            vehicleTypeError = true;
                        } else {
                            vehicleTypeError = false;
                        }

                        if ($('#model_type_id-field').val() == '') {
                            modelTypeError = true;
                        } else {
                            modelTypeError = false;
                        }

                        if ($('#color_id-field').val() == '') {
                            colorError = true;
                        } else {
                            colorError = false;
                        }

                        if ($('#driving_licence_expired_date-field').val() == '') {
                            drivingLicenceDateError = true;
                        } else {
                            drivingLicenceDateError = false;
                        }

                        if ($('#license_end_date_field').val() == '') {
                            licenseEndDateError = true;
                        } else {
                            licenseEndDateError = false;
                        }

                        if ($('#driving_licence_image_front').val() == '') {
                            drivingLicenceImageError = true;
                        } else {
                            drivingLicenceImageError = false;
                        }

                        if ($('#license_image_front').val() == '') {
                            licenseImageError = true;
                        } else {
                            licenseImageError = false;
                        }

                        if (modelNameError === true || plateNumberError === true || chassiNumberError === true || yearError === true || vehicleTypeError === true || modelTypeError === true || colorError === true || drivingLicenceDateError === true || licenseEndDateError === true || drivingLicenceImageError === true || licenseImageError === true) {
                            // console.log('error');
                            $('#added_driver').attr('disabled', true);
                            e.preventDefault();
                            $('.error_vehicle').css('display', 'inline-block');
                        } else {
                            // console.log('not error');
                            $('#added_driver').attr('disabled', false);
                            $('.error_vehicle').css('display', 'none');
                        }

                    });

                } else {
                    $('.has_vechile').hide();

                }
            });

            $("#government_id").on('change', function () {
                $('#city_id').html('<option value="" >Choose City</option>');
                if ($(this).val()) {
                    $.ajax({
                        url: "{{ url('/get_cities')}}" + "/" + $(this).val(),
                        type: 'get',
                        data: {},
                        success: function (data) {
                            $.each(data, function (i, content) {
                                $('#city_id').append($("<option></option>").attr("value", content.id).text(content.name_en));
                            });
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }
            });

            $('#driver-has-vehicle-field').change(function () {
                if ($(this).val() == 0) {
                    $.ajax({
                        url: "{{ url('/mngrAdmin/check_driver_has_vehicle/' . $driver->id)}}",
                        type: 'get',
                        data: {},
                        success: function (data) {
                            if (data == 1) {
                                $.alert({
                                    title: '',
                                    theme: 'modern',
                                    class: 'danger',
                                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">{{__("backend.attention")}}</span></div><div style="font-weight:bold;"><p> {{__("backend.If_It_Chosen_That_It_Does_Not_Have_a_Vehicle_its_Vehicle_Will_Be_Deleted")}} </p><p class="text-primary"></p></div>',
                                });
                                $('.has_vechile').hide();
                            }
                        },
                        error: function () {
                            alert('Error occured');
                        }
                    });
                }
            });
        })
    </script>
@endsection

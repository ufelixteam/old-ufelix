@extends('captain.layouts.app')
@section('css')
    <title>{{__('captain.driver')}} - {{__('captain.orders')}}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <style>
        .jconfirm.jconfirm-modern .jconfirm-box div.jconfirm-title-c .jconfirm-icon-c {
            -webkit-transition: -webkit-transform .5s;
            transition: -webkit-transform .5s;
            transition: transform .5s;
            transition: transform .5s, -webkit-transform .5s;
            -webkit-transform: scale(0);
            transform: scale(0);
            display: block;
            margin-right: 0;
            margin-left: 0;
            margin-bottom: 10px;
            font-size: 69px;
            color: #9e1717;
        }

        .nav-tabs li:first-child.active a {
            color: #fff;
            background-color: #337ab7;
        }

        .nav-tabs li:last-child.active a {
            color: #fff;
            background-color: #dd4b39;
        }
    </style>
@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-align-justify"></i>
            {{__('captain.orders_of_driver')}} [ # {{$driver->id}} - {{$driver->name}} ]
        </h3>
    </div>
@endsection

@section('content')
    <div>
        <form>
            <input type="hidden" name="sort" id="sort" value="{{app('request')->input('sort', 'currentorders')}}">
            <div class="row" style="margin-bottom: 50px">
                <div class="col-md-3">
                    <div class='input-group input-group-lg date' id='datepickerFilter'>
                        <input type="text" id="received_at" name="received_at" class="form-control"
                               value="{{app('request')->input('received_at')}}" autocomplete="off"
                               placeholder="{{__('captain.Received_at')}}"/>
                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-daterange input-group input-group-lg" id="datepicker">
                        <input type="text" class="input-sm form-control" name="start"
                               value="{{app('request')->input('start')}}" autocomplete="off"/>
                        <span class="input-group-addon">{{__('captain.to')}}</span>
                        <input type="text" class="input-sm form-control" name="end"
                               value="{{app('request')->input('end')}}" autocomplete="off"/>
                    </div>
                </div>
                <div class="col-md-5">
                    <button class="btn btn-success btn-lg">
                        <i class="fa fa-filter"></i>
                        {{__('captain.add_filter')}}
                    </button>
                </div>
            </div>
        </form>
        <!-- Nav tabs -->
        <ul id="myTabs" class="nav nav-tabs nav-justified nav-pills" role="tablist">
            <li role="presentation" class="{{ ! empty($sort) && $sort == 'currentorders' ? 'active' : ''}}"><a
                    href="#home" aria-controls="home" role="tab" data-toggle="tab" data-sort="currentorders"
                    class="order_type">{{__('captain.current')}}</a></li>
            <li role="presentation" class="{{ ! empty($sort) && $sort == 'orders' ? 'active' : ''}}"><a href="#profile"
                                                                                                        aria-controls="profile"
                                                                                                        role="tab"
                                                                                                        data-toggle="tab"
                                                                                                        data-sort="orders"
                                                                                                        class="order_type">{{__('captain.finished')}}</a>
{{--            <li role="presentation" class="{{ ! empty($sort) && $sort == 'recall' ? 'active' : ''}}"><a href="#recall"--}}
{{--                                                                                                        aria-controls="recall"--}}
{{--                                                                                                        role="tab"--}}
{{--                                                                                                        data-toggle="tab"--}}
{{--                                                                                                        data-sort="recall"--}}
{{--                                                                                                        class="order_type">{{__('captain.recall')}}</a>--}}
{{--            </li>--}}
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane {{ ! empty($sort) && $sort == 'currentorders' ? 'active' : ''}} "
                 id="home">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        @if(! empty($orders) &&  count($currentorders) > 0)
                            <form action="{{URL::asset('/captainDashboard/pdf/1/'. $id)}}" method="get" target="_blank">
                                <table class="table table-condensed table-striped text-center">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{__('captain.order_type')}}</th>
                                        <th>{{__('captain.order_number')}}</th>
                                        <th>{{__('captain.order_price')}}</th>
                                        <th>{{__('captain.sender_name')}}</th>
                                        <th>{{__('captain.receiver_name')}}</th>
                                        <th>{{__('captain.received_date')}}</th>
                                        <th>{{__('captain.status')}}</th>
                                        <th></th>
                                        <th>
                                            <input type="button" class="btn btn-info btn-xs" id="toggle"
                                                   value="{{__('captain.select')}}" onClick="do_this()"/>
                                            <button type="submit"
                                                    class="btn btn-warning btn-xs btn-pdf">{{__('captain.pdf')}}</button>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($currentorders as $order)

                                        <tr class="clickable-row"
                                            data-href="{{ route('captainDashboard.orders.show', $order->id) }}">

                                            <td>{{$order->id}}</td>
                                            <td>{{$order->type}}</td>
                                            <td>{{$order->order_number}}</td>
                                            <td>{{$order->order_price}}</td>
                                            <td>{{$order->sender_name}}</td>
                                            <td>{{$order->receiver_name}}</td>
                                            <td>{{$order->received_at ? date('Y-m-d H:i:s', strtotime($order->received_at)) : '-'}}</td>
                                            <td>
                                                {!! $order->status_span !!}
                                            </td>
                                            <td class="exclude-td">
                                                @if($order->status == 1)
                                                    <button type="button" class="btn btn-info btn-xs receive_order"
                                                            data-id="{{$order->id}}">
                                                        {{__('captain.Receive')}}
                                                    </button>
                                                @elseif($order->status == 2)
                                                    <button type="button" class="btn btn-success btn-xs deliver_order"
                                                            data-id="{{$order->id}}">
                                                        {{__('captain.deliver')}}
                                                    </button>
                                                @endif
                                            </td>
                                            {{--                                            <td class="pull-right">--}}
                                            {{--                                                <a class="btn btn-xs btn-primary"--}}
                                            {{--                                                   href="{{ route('captainDashboard.orders.show', $order->id) }}"><i--}}
                                            {{--                                                        class="glyphicon glyphicon-eye-open"></i> {{__('captain.view')}}--}}
                                            {{--                                                </a>--}}
                                            {{--                                            </td>--}}
                                            <td class="exclude-td">
                                                <input type="checkbox" name="select[]" value="{{$order->id}}"/>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </form>
                            {!! $currentorders->appends(['sort' => 'currentorders'])->appends($_GET)->links() !!}

                        @else
                            <h3 class="text-center alert alert-warning">{{__('captain.No_result_found')}}</h3>
                        @endif

                    </div>
                </div>
            </div> <!-- tab end -->


            <div role="tabpanel" class="tab-pane {{ ! empty($sort) && $sort == 'orders' ? 'active' : ''}}" id="profile">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        @if(! empty($orders) && count($orders) > 0)
                            <form action="{{URL::asset('/captainDashboard/pdf/1/'. $id)}}" method="get" target="_blank">
                                <table class="table table-condensed table-striped text-center">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{__('captain.order_type')}}</th>
                                        <th>{{__('captain.order_number')}}</th>
                                        <th>{{__('captain.order_price')}}</th>
                                        <th>{{__('captain.sender_name')}}</th>
                                        <th>{{__('captain.receiver_name')}}</th>
{{--                                        <th>{{__('captain.receiver_code')}}</th>--}}
                                        <th>{{__('captain.status')}}</th>
                                        {{--                                        <th class="pull-right"></th>--}}
                                        <th>
                                            <input type="button" class="btn btn-info btn-xs" id="toggle"
                                                   value="{{__('captain.select')}}" onClick="do_this()"/>
                                            <button type="submit"
                                                    class="btn btn-warning btn-xs btn-pdf">{{__('captain.pdf')}}</button>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orders as  $order)
                                        <tr class="clickable-row"
                                            data-href="{{ route('captainDashboard.orders.show', $order->id) }}">
                                            <td>{{$order->id}}</td>
                                            <td>{{$order->type}}</td>
                                            <td>{{$order->order_number}}</td>
                                            <td>{{$order->order_price}}</td>
                                            <td>{{$order->sender_name}}</td>
                                            <td>{{$order->receiver_name}}</td>
{{--                                            <td>{{$order->receiver_code}}</td>--}}
                                            <td>
                                                {!! $order->status_span !!}
                                            </td>
                                            {{--                                            <td class="pull-right">--}}
                                            {{--                                                <a class="btn btn-xs btn-primary"--}}
                                            {{--                                                   href="{{ route('captainDashboard.orders.show', $order->id) }}"><i--}}
                                            {{--                                                        class="glyphicon glyphicon-eye-open"></i> {{__('captain.view')}}--}}
                                            {{--                                                </a>--}}
                                            {{--                                            </td>--}}
                                            <td class="exclude-td">
                                                <input type="checkbox" name="select[]" value="{{$order->id}}"/>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </form>
                            {!! $orders->appends($_GET)->appends(['sort' => 'orders'])->links() !!}

                        @else
                            <h3 class="text-center alert alert-warning">{{__('captain.No_result_found')}}</h3>
                        @endif
                    </div>
                </div>
            </div> <!-- tab end -->

{{--            <div role="tabpanel" class="tab-pane {{ ! empty($sort) && $sort == 'recall' ? 'active' : ''}}" id="recall">--}}
{{--                <div class="row">--}}
{{--                    <div class="col-md-12 col-xs-12 col-sm-12">--}}
{{--                        @if(! empty($recalled_orders) && count($recalled_orders) > 0)--}}
{{--                            <form action="{{URL::asset('/captainDashboard/pdf/1/'. $id)}}" method="get" target="_blank">--}}
{{--                                <table class="table table-condensed table-striped text-center">--}}
{{--                                    <thead>--}}
{{--                                    <tr>--}}
{{--                                        <th>#</th>--}}
{{--                                        <th>{{__('captain.order_type')}}</th>--}}
{{--                                        <th>{{__('captain.order_number')}}</th>--}}
{{--                                        <th>{{__('captain.order_price')}}</th>--}}
{{--                                        <th>{{__('captain.sender_name')}}</th>--}}
{{--                                        <th>{{__('captain.receiver_name')}}</th>--}}
{{--                                        <th>{{__('captain.receiver_code')}}</th>--}}
{{--                                        <th>--}}
{{--                                            <a class="btn btn-xs btn btn btn-success" id="drop_off">--}}
{{--                                                <i class="fa fa-dropbox"></i> {{__('captain.drop_off')}}--}}

{{--                                            </a>--}}
{{--                                        </th>--}}
{{--                                        --}}{{--                                        <th class="pull-right"></th>--}}
{{--                                        <th>--}}
{{--                                            <input type="button" class="btn btn-info btn-xs" id="toggle"--}}
{{--                                                   value="{{__('captain.select')}}" onClick="do_this()"/>--}}
{{--                                            <button type="submit"--}}
{{--                                                    class="btn btn-warning btn-xs btn-pdf">{{__('captain.pdf')}}</button>--}}
{{--                                        </th>--}}
{{--                                    </tr>--}}
{{--                                    </thead>--}}
{{--                                    <tbody>--}}
{{--                                    @foreach($recalled_orders as  $order)--}}
{{--                                        <tr class="clickable-row"--}}
{{--                                            data-href="{{ route('captainDashboard.orders.show', $order->id) }}">--}}
{{--                                            <td>{{$order->id}}</td>--}}
{{--                                            <td>{{$order->type}}</td>--}}
{{--                                            <td>{{$order->order_number}}</td>--}}
{{--                                            <td>{{$order->order_price}}</td>--}}
{{--                                            <td>{{$order->sender_name}}</td>--}}
{{--                                            <td>{{$order->receiver_name}}</td>--}}
{{--                                            <td>{{$order->receiver_code}}</td>--}}
{{--                                            <td class="selectdropoff exclude-td">--}}
{{--                                                <input type="checkbox" class="warehouse_dropoff"--}}
{{--                                                       name="warehouse_dropoff[]"--}}
{{--                                                       value="{{$order->id}}"--}}
{{--                                                       @if($order->warehouse_dropoff == 1) disabled--}}
{{--                                                       @endif @if($order->warehouse_dropoff == 1) checked @endif/>--}}
{{--                                            </td>--}}
{{--                                            --}}{{--                                            <td class="pull-right">--}}
{{--                                            --}}{{--                                                <a class="btn btn-xs btn-primary"--}}
{{--                                            --}}{{--                                                   href="{{ route('captainDashboard.orders.show', $order->id) }}"><i--}}
{{--                                            --}}{{--                                                        class="glyphicon glyphicon-eye-open"></i> {{__('captain.view')}}--}}
{{--                                            --}}{{--                                                </a>--}}
{{--                                            --}}{{--                                            </td>--}}
{{--                                            <td class="exclude-td">--}}
{{--                                                <input type="checkbox" name="select[]" value="{{$order->id}}"/>--}}
{{--                                            </td>--}}
{{--                                        </tr>--}}
{{--                                    @endforeach--}}
{{--                                    </tbody>--}}
{{--                                </table>--}}
{{--                            </form>--}}
{{--                            {!! $recalled_orders->appends($_GET)->appends(['sort' => 'recall'])->links() !!}--}}

{{--                        @else--}}
{{--                            <h3 class="text-center alert alert-warning">{{__('captain.No_result_found')}}</h3>--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div> <!-- tab end -->--}}
        </div>

    </div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script>
        $('.input-daterange').datepicker({});

        $('.order_type').click(function (e) {
            $("#sort").val($(this).attr('data-sort'))
        });

        $('#myTabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        })
        $(function () {
            $(".ask_view").on('click', function () {
                var id = $(this).attr('data-id');
                $.ajax({
                    url: '{!! url("/captainDashboard/ask_review_order/") !!}' + '/' + id,
                    type: 'get',

                }).done(function (data) {
                    if (data == "ok") {
                        $.alert({
                            title: "{{__('captain.Request_Review_Sent')}}",
                            icon: 'fa fa-smile-o',
                            theme: 'modern',
                            content: "{{__('captain.Your_request_sent_to_Ufelix_Admin_successfully')}}",
                            buttons: {
                                info: {
                                    text: 'OK',
                                    btnClass: 'btn-success',
                                    action: function () {
                                        window.location.href = window.location.href;
                                    }
                                }
                            }
                        });
                    }
                });
            });

        });


        // Select All Orders function For Print
        function do_this() {

            var checkboxes = document.getElementsByName('select[]');
            var button = document.getElementById('toggle');

            if (button.value == 'select') {
                for (var i in checkboxes) {
                    checkboxes[i].checked = 'FALSE';
                }
                button.value = 'deselect'
            } else {
                for (var i in checkboxes) {
                    checkboxes[i].checked = '';
                }
                button.value = 'select';
            }
        }

        $("#drop_off").on('click', function () {

            let ids = [];
            $(".warehouse_dropoff:checked").not(":disabled").each(function () {
                ids.push($(this).val());
            });

            if (ids.length == 0) {
                return false;
            }

            $.confirm({
                title: '',
                theme: 'modern',
                class: 'warning',
                content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-simle-o  text-warning"></i></span><span class="jconfirm-title"> {{__("captain.confirm_drop_off")}}</span></div><div style="font-weight:bold;"><p>{{__("captain.drop_off_orders")}}</p></div>',
                buttons: {
                    confirm: {
                        text: '{{__("captain.drop_off_text")}}',
                        btnClass: 'btn-warning',
                        action: function () {
                            $.ajax({
                                url: "{{ url('/captainDashboard/orders/warehouse_dropoff/')}}",
                                type: 'POST',
                                data: {'_token': "{{csrf_token()}}", 'ids': ids},
                                success: function (data) {
                                    location.reload();
                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                            });
                        }
                    },
                    cancel: {
                        text: 'Cancel',
                        action: function () {
                        }
                    }
                }
            });
        });


        $('body').on('click', '.receive_order', function (e) {

            let ids = [$(this).attr('data-id')];

            $.ajax({
                url: "{{ url('/captainDashboard/receive_orders')}}",
                type: 'post',
                data: {ids: ids, '_token': "{{csrf_token()}}"},
                success: function (data) {
                    // if (data['status'] != 'false') {
                    location.reload();
                    // }
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $('body').on('click', '.deliver_order', function (e) {
            let ids = [$(this).attr('data-id')];

            $.ajax({
                url: "{{ url('/captainDashboard/deliver_orders')}}",
                type: 'post',
                data: {ids: ids, '_token': "{{csrf_token()}}"},
                success: function (data) {
                    // if (data['status'] != 'false') {
                    location.reload();
                    // }
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

        });

    </script>
@endsection

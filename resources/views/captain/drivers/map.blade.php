@extends('captain.layouts.app')
@section('css')
    <title>{{__('captain.map')}}</title>
@endsection
@section('content')
    <div class="main_content_container">
        <div class="main_container main_menu_open page_content">
            <h1 class="heading_title">{{__('captain.Track_Drivers')}}</h1>
            <div style="margin-top:20px;"></div>
            <div class="row">
                <div class="col-md-12 col-xs-6 ">
                    <div style="height:500px;">
                        <?php
                        if (!empty($online_drivers) && count($online_drivers) > 0) {
                            Mapper::map($online_drivers[0]->device_active->latitude, $online_drivers[0]->device_active->longitude, ['center' => false, 'marker' => false])->informationWindow($online_drivers[0]->device_active->latitude, $online_drivers[0]->device_active->longitude, "Driver Name:  {$online_drivers[0]->name} - Mobile: {$online_drivers[0]->mobile}", ['title' => 'Online', 'icon' => [
                                'path' => 'M365.027,44.5c-30-29.667-66.333-44.5-109-44.5s-79,14.833-109,44.5s-45,65.5-45,107.5    c0,25.333,12.833,67.667,38.5,127c25.667,59.334,51.333,113.334,77,162s38.5,72.334,38.5,71c4-7.334,9.5-17.334,16.5-30    s19.333-36.5,37-71.5s33.167-67.166,46.5-96.5c13.334-29.332,25.667-59.667,37-91s17-55,17-71    C410.027,110,395.027,74.167,365.027,44.5z M289.027,184c-9.333,9.333-20.5,14-33.5,14c-13,0-24.167-4.667-33.5-14    s-14-20.5-14-33.5s4.667-24,14-33s20.5-13.5,33.5-13.5c13,0,24.167,4.5,33.5,13.5s14,20,14,33S298.36,174.667,289.027,184z',
                                'fillColor' => '#62d233',
                                'fillOpacity' => 0.8,
                                'scale' => 0.08,
                                'strokeColor' => '#006400',
                                'strokeWeight' => 2
                            ]])->rectangle([['latitude' => $online_drivers[0]->device_active->latitude, 'longitude' => $online_drivers[0]->device_active->longitude], ['latitude' => $online_drivers[0]->device_active->latitude, 'longitude' => $online_drivers[0]->device_active->longitude]], ['strokeColor' => '#ea234a', 'strokeOpacity' => 0.1, 'strokeWeight' => 2, 'fillColor' => '#FFFFFF']);

                            for ($i = 1; $i < count($online_drivers); $i++) {
                                Mapper::informationWindow($online_drivers[$i]->device_active->latitude, $online_drivers[$i]->device_active->longitude, "Driver Name:  {$online_drivers[$i]->name} - Mobile: {$online_drivers[$i]->mobile}", ['title' => 'Online', 'icon' => [
                                    'path' => 'M365.027,44.5c-30-29.667-66.333-44.5-109-44.5s-79,14.833-109,44.5s-45,65.5-45,107.5    c0,25.333,12.833,67.667,38.5,127c25.667,59.334,51.333,113.334,77,162s38.5,72.334,38.5,71c4-7.334,9.5-17.334,16.5-30    s19.333-36.5,37-71.5s33.167-67.166,46.5-96.5c13.334-29.332,25.667-59.667,37-91s17-55,17-71    C410.027,110,395.027,74.167,365.027,44.5z M289.027,184c-9.333,9.333-20.5,14-33.5,14c-13,0-24.167-4.667-33.5-14    s-14-20.5-14-33.5s4.667-24,14-33s20.5-13.5,33.5-13.5c13,0,24.167,4.5,33.5,13.5s14,20,14,33S298.36,174.667,289.027,184z',
                                    'fillColor' => '#62d233',
                                    'fillOpacity' => 0.8,
                                    'scale' => 0.08,
                                    'strokeColor' => '#006400',
                                    'strokeWeight' => 1.5
                                ]])->rectangle([['latitude' => $online_drivers[$i]->device_active->latitude, 'longitude' => $online_drivers[$i]->device_active->longitude], ['latitude' => $online_drivers[$i]->device_active->latitude, 'longitude' => $online_drivers[$i]->device_active->longitude]], ['strokeColor' => '#ea234a', 'strokeOpacity' => 0.1, 'strokeWeight' => 2, 'fillColor' => '#FFFFFF']);
                            }
                        }

//                        if (!empty($offline_drivers) && count($offline_drivers) > 0) {
//                            if (!empty($online_drivers) && count($online_drivers) > 0) {
//                                for ($i = 0; $i < count($offline_drivers); $i++) {
//                                    Mapper::informationWindow($offline_drivers[$i]->device->latitude, $offline_drivers[$i]->device->longitude, "Driver Name:  {$offline_drivers[$i]->name} - Mobile: {$offline_drivers[$i]->mobile}", ['title' => 'Offline', 'icon' => [
//                                        'path' => 'M365.027,44.5c-30-29.667-66.333-44.5-109-44.5s-79,14.833-109,44.5s-45,65.5-45,107.5    c0,25.333,12.833,67.667,38.5,127c25.667,59.334,51.333,113.334,77,162s38.5,72.334,38.5,71c4-7.334,9.5-17.334,16.5-30    s19.333-36.5,37-71.5s33.167-67.166,46.5-96.5c13.334-29.332,25.667-59.667,37-91s17-55,17-71    C410.027,110,395.027,74.167,365.027,44.5z M289.027,184c-9.333,9.333-20.5,14-33.5,14c-13,0-24.167-4.667-33.5-14    s-14-20.5-14-33.5s4.667-24,14-33s20.5-13.5,33.5-13.5c13,0,24.167,4.5,33.5,13.5s14,20,14,33S298.36,174.667,289.027,184z',
//                                        'fillColor' => '#808080',
//                                        'fillOpacity' => 0.8,
//                                        'scale' => 0.08,
//                                        'strokeColor' => '##808080',
//                                        'strokeWeight' => 1.5
//                                    ]])->rectangle([['latitude' => $offline_drivers[$i]->device->latitude, 'longitude' => $offline_drivers[$i]->device->longitude], ['latitude' => $offline_drivers[$i]->device->latitude, 'longitude' => $offline_drivers[$i]->device->longitude]], ['strokeColor' => '#ea234a', 'strokeOpacity' => 0.1, 'strokeWeight' => 2, 'fillColor' => '#FFFFFF']);
//                                }
//
//                            } else {
//                                Mapper::map($offline_drivers[0]->latitude, $offline_drivers[0]->longitude, ['center' => false, 'marker' => false])->informationWindow($offline_drivers[0]->latitude, $offline_drivers[0]->longitude, "Driver Name:  {$offline_drivers[0]->name} - Mobile: {$offline_drivers[0]->mobile}", ['title' => 'Offline', 'icon' => [
//                                    'path' => 'M365.027,44.5c-30-29.667-66.333-44.5-109-44.5s-79,14.833-109,44.5s-45,65.5-45,107.5    c0,25.333,12.833,67.667,38.5,127c25.667,59.334,51.333,113.334,77,162s38.5,72.334,38.5,71c4-7.334,9.5-17.334,16.5-30    s19.333-36.5,37-71.5s33.167-67.166,46.5-96.5c13.334-29.332,25.667-59.667,37-91s17-55,17-71    C410.027,110,395.027,74.167,365.027,44.5z M289.027,184c-9.333,9.333-20.5,14-33.5,14c-13,0-24.167-4.667-33.5-14    s-14-20.5-14-33.5s4.667-24,14-33s20.5-13.5,33.5-13.5c13,0,24.167,4.5,33.5,13.5s14,20,14,33S298.36,174.667,289.027,184z',
//                                    'fillColor' => '#808080',
//                                    'fillOpacity' => 0.8,
//                                    'scale' => 0.08,
//                                    'strokeColor' => '##808080',
//                                    'strokeWeight' => 1.5
//                                ]])->rectangle([['latitude' => $offline_drivers[0]->latitude, 'longitude' => $offline_drivers[0]->longitude], ['latitude' => $offline_drivers[0]->latitude, 'longitude' => $offline_drivers[0]->longitude]], ['strokeColor' => '#ea234a', 'strokeOpacity' => 0.1, 'strokeWeight' => 2, 'fillColor' => '#FFFFFF']);
//                                for ($i = 1; $i < count($offline_drivers); $i++) {
//                                    Mapper::informationWindow($offline_drivers[$i]->device->latitude, $offline_drivers[$i]->device->longitude, "Driver Name:  {$offline_drivers[$i]->name} - Mobile: {$offline_drivers[$i]->mobile}", ['title' => 'Offline', 'icon' => [
//                                        'path' => 'M365.027,44.5c-30-29.667-66.333-44.5-109-44.5s-79,14.833-109,44.5s-45,65.5-45,107.5    c0,25.333,12.833,67.667,38.5,127c25.667,59.334,51.333,113.334,77,162s38.5,72.334,38.5,71c4-7.334,9.5-17.334,16.5-30    s19.333-36.5,37-71.5s33.167-67.166,46.5-96.5c13.334-29.332,25.667-59.667,37-91s17-55,17-71    C410.027,110,395.027,74.167,365.027,44.5z M289.027,184c-9.333,9.333-20.5,14-33.5,14c-13,0-24.167-4.667-33.5-14    s-14-20.5-14-33.5s4.667-24,14-33s20.5-13.5,33.5-13.5c13,0,24.167,4.5,33.5,13.5s14,20,14,33S298.36,174.667,289.027,184z',
//                                        'fillColor' => '#808080',
//                                        'fillOpacity' => 0.8,
//                                        'scale' => 0.08,
//                                        'strokeColor' => '##808080',
//                                        'strokeWeight' => 1.5
//                                    ]])->rectangle([['latitude' => $offline_drivers[$i]->device->latitude, 'longitude' => $offline_drivers[$i]->device->longitude], ['latitude' => $offline_drivers[$i]->device->latitude, 'longitude' => $offline_drivers[$i]->device->longitude]], ['strokeColor' => '#ea234a', 'strokeOpacity' => 0.1, 'strokeWeight' => 2, 'fillColor' => '#FFFFFF']);
//                                }
//                            }
//                        }
                        /*if (! empty($busy_drivers) && count($busy_drivers) > 0) {
                            for($i=0;$i<count($busy_drivers);$i++){
                               Mapper::map($busy_drivers[$i]->latitude,$busy_drivers[$i]->longitude)->informationWindow($busy_drivers[$i]->latitude,$busy_drivers[$i]->longitude,"Driver Name:  {$busy_drivers[$i]->name} - Mobile: {$busy_drivers[$i]->mobile}", [ 'title' => 'Busy','icon' => [
                                    'path' => 'M365.027,44.5c-30-29.667-66.333-44.5-109-44.5s-79,14.833-109,44.5s-45,65.5-45,107.5    c0,25.333,12.833,67.667,38.5,127c25.667,59.334,51.333,113.334,77,162s38.5,72.334,38.5,71c4-7.334,9.5-17.334,16.5-30    s19.333-36.5,37-71.5s33.167-67.166,46.5-96.5c13.334-29.332,25.667-59.667,37-91s17-55,17-71    C410.027,110,395.027,74.167,365.027,44.5z M289.027,184c-9.333,9.333-20.5,14-33.5,14c-13,0-24.167-4.667-33.5-14    s-14-20.5-14-33.5s4.667-24,14-33s20.5-13.5,33.5-13.5c13,0,24.167,4.5,33.5,13.5s14,20,14,33S298.36,174.667,289.027,184z',
                                    'fillColor' => 'orange',
                                    'fillOpacity' => 0.8,
                                    'scale' => 0.08,
                                    'strokeColor' => 'orange',
                                    'strokeWeight' => 2
                                ]])->rectangle([['latitude' => $busy_drivers[$i]->latitude, 'longitude' => $busy_drivers[$i]->longitude], ['latitude' => $busy_drivers[$i]->latitude, 'longitude' => $busy_drivers[$i]->longitude]], ['strokeColor' => '#ea234a', 'strokeOpacity' => 0.1, 'strokeWeight' => 2, 'fillColor' => '#FFFFFF']);
                            }
                        }*/

                        $map = Mapper::render();
                        echo $map;
                        ?>
                    </div> <!-- end map div -->
                </div> <!-- end column -->
            </div> <!-- end row -->
            <hr>
            <div class="row" style="margin-top: 20px;">
                <div class="col-md-12">
                    <table class="table table-striped table-hover text-center">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('captain.name')}}</th>
                            <th>{{__('captain.mobile')}}</th>
                            <th>{{__('captain.email')}}</th>
                            <th>{{__('captain.status')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($online_drivers as $indix => $driver)
                            @if(! empty($driver->device_active))
                                <tr style="background-color: #c1e3ff;    font-weight: bold;" class="text-primary">
                            @else
                                <tr>
                                    @endif
                                    <td>{{$indix+1}}</td>
                                    <td>{{$driver->name}}</td>
                                    <td>{{$driver->mobile}}</td>
                                    <td>{{$driver->email}}</td>
                                    <td>{!! $driver->status_span !!}</td>
                                </tr>
                                @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        setTimeout(function() {
            location.reload();
        }, 30000);
    </script>
@endsection

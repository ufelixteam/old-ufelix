<!-- **** Modal **** -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{__('backend.Forward_Order')}}</h5>
            </div>
            <div class="modal-body">
                <form class="" action="{{ url('mngrAdmin/ForwardOrders') }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="driver_id"
                               value="{{ ! empty($order->accepted) && ! empty($order->accepted->driver) ? $order->accepted->driver->id : '' }}">
                        <input type="hidden" class="form-control" name="order_id" value="{{$order->id}}">
                        <input type="hidden" class="form-control" name="accepted_id"
                               value="{{ ! empty($order->accepted) ? $order->accepted->id : '' }}">
                        <input type="hidden" class="form-control" name="governorate_cost_id"
                               value="{{ $order->governorate_cost_id }}">
                    </div>

                    <div class="form-group">
                        <label>{{__('backend.choose_driver')}}:</label>
                        <select id="items-field" name="items" id="items" class="form-control">
                            @if(! empty($online_drivers))

                                @foreach($online_drivers as $driver)
                                    <option value="{{$driver->id}}">{{$driver->name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="form-group">
                        <label>
                            <input type="checkbox" value="1" name="forward_fees"
                                   checked style="vertical-align: bottom;">
                            <span>{{__('backend.remove_from_old_invoices')}}</span>
                        </label>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">{{__('backend.forward')}}</button>
                        <button type="button" class="btn btn-danger"
                                data-dismiss="modal">{{__('backend.close')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php


namespace App\Http\Controllers\Backend;


use App\Events\OrderUpdated;
use App\Models\AcceptedOrder;
use App\Models\Agent;
use App\Models\DeliveryProblem;
use App\Models\Driver;
use App\Models\Invoice;
use App\Models\InvoiceOrder;
use App\Models\MoveCollection;
use App\Models\MoveCollectionOrder;
use App\Models\Order;
use App\Models\OrderComment;
use App\Models\OrderDeliveryProblem;
use App\Models\PickUp;
use App\Models\RefundCollection;
use App\Models\ScanCollection;
use App\Models\ScanCollectionOrder;
use App\Models\Store;
use Auth;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Http\Request;
use PDF;

class ScanController extends Controller
{

    private $guardType;

    public function __construct(Request $request)
    {
        if ($request->is('mngrAdmin/*') || $request->is('admin/*')) {
            Config::set('auth.defaults.guard', 'admin');

        } elseif ($request->is('mngrAgent/*') || $request->is('agent/*')) {
            Config::set('auth.defaults.guard', 'agent');
        }
        $this->guardType = Config::get('auth.defaults.guard');
    }

    public function scan_print(Request $request)
    {
        $ids = explode(',', app('request')->input('ids'));
        if (is_array($ids) && count($ids)) {
            $ids_ordered = implode(',', $ids);

            $driver = Driver::find($request->driver_id);
            $orders = Order::with(['customer' => function ($query) {
                $query->with('corporate');
            }])->whereIn('id', $ids)->orderByRaw("FIELD(id, $ids_ordered)")->get();

            $pdf = PDF::loadView('backend.scan.pdf-scan', compact('orders', 'driver'), [], ['format' => 'A4-L']);
            return $pdf->stream(time());

        }
    }

    public function scan_validation(Request $request)
    {
        $data = ['result' => true];

        $ids = app('request')->input('ids');
        if (is_array($ids) && count($ids)) {
            if (app('request')->input('type') == 'print') {
                $orders = Order::join('accepted_orders', 'orders.id', '=', 'accepted_orders.order_id')
                    ->whereIn('orders.id', $ids)
                    ->whereIn('orders.status', [1, 2])
                    ->whereIn('accepted_orders.status', [0, 2])
                    ->select('orders.*', 'driver_id')
                    ->get()
                    ->unique();

                $drivers = $orders->groupBy('driver_id')->map(function ($drivers) {
                    return $drivers->count();
                });

                if (count($orders) != count($ids) || count($drivers) > 1) {
                    $data['result'] = false;
                    if (count($drivers) > 1) {
                        $data['more_drivers'] = 1;
                    } elseif (count($orders) != count($ids)) {
                        $ordersNotFound = array_diff($ids, $orders->pluck('id')->toArray());
                        if (count($ordersNotFound)) {
                            $ordersNotFound = Order::whereIn('id', $ordersNotFound)->pluck('order_number')->toArray();
                            $data['not_found'] = implode(',', $ordersNotFound);
                        }
                    }
                }
            } elseif (app('request')->input('type') == 'forward') {
                $orders = Order::where('orders.status', 7)
                    ->whereIn('orders.id', $ids)
                    ->get();

                if (count($orders) != count($ids)) {
                    $data['result'] = false;
                    $ordersNotFound = array_diff($ids, $orders->pluck('id')->toArray());
                    if (count($ordersNotFound)) {
                        $ordersNotFound = Order::whereIn('id', $ordersNotFound)->pluck('order_number')->toArray();
                        $data['not_found'] = implode(',', $ordersNotFound);
                    }
                }
            } elseif (app('request')->input('type') == 'action_back') {
                $orders = Order::whereIn('orders.status', [3, 5, 8])
                    ->whereIn('orders.id', $ids)
                    ->get();

                if (count($orders) != count($ids)) {
                    $data['result'] = false;
                    $ordersNotFound = array_diff($ids, $orders->pluck('id')->toArray());
                    if (count($ordersNotFound)) {
                        $ordersNotFound = Order::whereIn('id', $ordersNotFound)->pluck('order_number')->toArray();
                        $data['not_found'] = implode(',', $ordersNotFound);
                    }
                }
            } elseif (app('request')->input('type') == 'receive') {
                $orders = Order::where('orders.status', 1)
                    ->whereIn('orders.id', $ids)
                    ->get();

                if (count($orders) != count($ids)) {
                    $data['result'] = false;
                    $ordersNotFound = array_diff($ids, $orders->pluck('id')->toArray());
                    if (count($ordersNotFound)) {
                        $ordersNotFound = Order::whereIn('id', $ordersNotFound)->pluck('order_number')->toArray();
                        $data['not_found'] = implode(',', $ordersNotFound);
                    }
                }
            } elseif (app('request')->input('type') == 'problems') {
                $orders = Order::where('orders.status', 2)
                    ->whereIn('orders.id', $ids)
                    ->get();

                if (count($orders) != count($ids)) {
                    $data['result'] = false;
                    $ordersNotFound = array_diff($ids, $orders->pluck('id')->toArray());
                    if (count($ordersNotFound)) {
                        $ordersNotFound = Order::whereIn('id', $ordersNotFound)->pluck('order_number')->toArray();
                        $data['not_found'] = implode(',', $ordersNotFound);
                    }
                }
            } elseif (app('request')->input('type') == 'deliver') {
                $orders = Order::whereIn('orders.status', [2, 3])
                    ->whereIn('orders.id', $ids)
                    ->get();

                if (count($orders) != count($ids)) {
                    $data['result'] = false;
                    $ordersNotFound = array_diff($ids, $orders->pluck('id')->toArray());
                    if (count($ordersNotFound)) {
                        $ordersNotFound = Order::whereIn('id', $ordersNotFound)->pluck('order_number')->toArray();
                        $data['not_found'] = implode(',', $ordersNotFound);
                    }
                }
            } elseif (app('request')->input('type') == 'recall') {
                $orders = Order::where('orders.status', 2)
                    ->whereIn('orders.id', $ids)
                    ->get();

                if (count($orders) != count($ids)) {
                    $data['result'] = false;
                    $ordersNotFound = array_diff($ids, $orders->pluck('id')->toArray());
                    if (count($ordersNotFound)) {
                        $ordersNotFound = Order::whereIn('id', $ordersNotFound)->pluck('order_number')->toArray();
                        $data['not_found'] = implode(',', $ordersNotFound);
                    }
                }
            } elseif (app('request')->input('type') == 'reject') {
                $orders = Order::where('orders.status', 2)
                    ->whereIn('orders.id', $ids)
                    ->get();

                if (count($orders) != count($ids)) {
                    $data['result'] = false;
                    $ordersNotFound = array_diff($ids, $orders->pluck('id')->toArray());
                    if (count($ordersNotFound)) {
                        $ordersNotFound = Order::whereIn('id', $ordersNotFound)->pluck('order_number')->toArray();
                        $data['not_found'] = implode(',', $ordersNotFound);
                    }
                }
            } elseif (app('request')->input('type') == 'cancel') {
                $orders = Order::whereIn('orders.status', [7, 1, 2, 0])
                    ->whereIn('orders.id', $ids)
                    ->get();

                if (count($orders) != count($ids)) {
                    $data['result'] = false;
                    $ordersNotFound = array_diff($ids, $orders->pluck('id')->toArray());
                    if (count($ordersNotFound)) {
                        $ordersNotFound = Order::whereIn('id', $ordersNotFound)->pluck('order_number')->toArray();
                        $data['not_found'] = implode(',', $ordersNotFound);
                    }
                }
            } elseif (app('request')->input('type') == 'delay') {
                $orders = Order::whereIn('orders.status', [7, 1, 2])
                    ->whereIn('orders.id', $ids)
                    ->get();

                if (count($orders) != count($ids)) {
                    $data['result'] = false;
                    $ordersNotFound = array_diff($ids, $orders->pluck('id')->toArray());
                    if (count($ordersNotFound)) {
                        $ordersNotFound = Order::whereIn('id', $ordersNotFound)->pluck('order_number')->toArray();
                        $data['not_found'] = implode(',', $ordersNotFound);
                    }
                }
            } elseif (app('request')->input('type') == 'dropped') {
                $orders = Order::whereIn('orders.status', [0, 1, 2])
                    ->whereIn('orders.id', $ids)
                    ->get();

                if (count($orders) != count($ids)) {
                    $data['result'] = false;
                    $ordersNotFound = array_diff($ids, $orders->pluck('id')->toArray());
                    if (count($ordersNotFound)) {
                        $ordersNotFound = Order::whereIn('id', $ordersNotFound)->pluck('order_number')->toArray();
                        $data['not_found'] = implode(',', $ordersNotFound);
                    }
                }
            } elseif (app('request')->input('type') == 'drop_and_exit_again') {
                $orders = Order::whereIn('orders.status', [0, 1, 2])
                    ->whereIn('orders.id', $ids)
                    ->get();

                if (count($orders) != count($ids)) {
                    $data['result'] = false;
                    $ordersNotFound = array_diff($ids, $orders->pluck('id')->toArray());
                    if (count($ordersNotFound)) {
                        $ordersNotFound = Order::whereIn('id', $ordersNotFound)->pluck('order_number')->toArray();
                        $data['not_found'] = implode(',', $ordersNotFound);
                    }
                }
            } elseif (app('request')->input('type') == 'pickup_dropped') {
                $pickups = PickUp::where('status', '!=', 3)
                    ->whereIn('id', $ids)
                    ->get();

                if (count($pickups) != count($ids)) {
                    $data['result'] = false;
                    $ordersNotFound = array_diff($ids, $pickups->pluck('id')->toArray());
                    if (count($ordersNotFound)) {
                        $ordersNotFound = PickUp::whereIn('id', $ordersNotFound)->pluck('pickup_number')->toArray();
                        $data['not_found'] = implode(',', $ordersNotFound);
                    }
                }
            } elseif (app('request')->input('type') == 'moved_to') {
                $orders = Order::where(function ($query) {
                    $query->where('status', 7)
                        ->orWhere(function ($query) {
                            $query->where('status', 5)
                                ->where('warehouse_dropoff', 1)
                                ->where('client_dropoff', 0);
                        })
                        ->orWhere(function ($query) {
                            $query->where('status', 8)
                                ->where('warehouse_dropoff', 1)
                                ->where('client_dropoff', 0);
                        })
                        ->orWhere(function ($query) {
                            $query->where('status', 4)
                                ->where('warehouse_dropoff', 1)
                                ->where('client_dropoff', 0)
                                ->where('status_before_cancel', '!=', 0);
                        });
                })
                    ->whereNull('move_collection_id')
                    ->whereIn('orders.id', $ids)
                    ->get();

                if (count($orders) != count($ids)) {
                    $data['result'] = false;
                    $ordersNotFound = array_diff($ids, $orders->pluck('id')->toArray());
                    if (count($ordersNotFound)) {
                        $ordersNotFound = Order::whereIn('id', $ordersNotFound)->pluck('order_number')->toArray();
                        $data['not_found'] = implode(',', $ordersNotFound);
                    }
                }
            } elseif (app('request')->input('type') == 'recall_drop') {
                $orders = Order::where(function ($query) {
                    $query->where('status', 5)
                        ->orWhere('status', 8)
                        ->orWhere(function ($query) {
                            $query->where('status', 3)
                                ->where('delivery_status', 2);
                        })
                        ->orWhere(function ($query) {
                            $query->where('status', 3)
                                ->where('delivery_status', 4);
                        })
                        ->orWhere(function ($query) {
                            $query->where('status', 4)
                                ->where('status_before_cancel', '!=', 0);
                        });
                })
                    ->where(function ($query) {
                        $query->where('warehouse_dropoff', 0)
                            ->orWhereNull('warehouse_dropoff');
                    })
                    ->where(function ($query) {
                        $query->where('client_dropoff', 0)
                            ->orWhereNull('client_dropoff');
                    })
                    ->whereIn('orders.id', $ids)
                    ->get();

                if (count($orders) != count($ids)) {
                    $data['result'] = false;
                    $ordersNotFound = array_diff($ids, $orders->pluck('id')->toArray());
                    if (count($ordersNotFound)) {
                        $ordersNotFound = Order::whereIn('id', $ordersNotFound)->pluck('order_number')->toArray();
                        $data['not_found'] = implode(',', $ordersNotFound);
                    }
                }
            } elseif (app('request')->input('type') == 'client_drop') {
                $orders = Order::where(function ($query) {
                    $query->where('status', 5)
                        ->orWhere('status', 8)
                        ->orWhere(function ($query) {
                            $query->where('status', 3)
                                ->where('delivery_status', 2);
                        })
                        ->orWhere(function ($query) {
                            $query->where('status', 3)
                                ->where('delivery_status', 4);
                        })
                        ->orWhere(function ($query) {
                            $query->where('status', 4)
                                ->where('status_before_cancel', '!=', 0);
                        });
                })
                    ->where('warehouse_dropoff', 1)
                    ->where(function ($query) {
                        $query->where('client_dropoff', 0)
                            ->orWhereNull('client_dropoff');
                    })
                    ->whereIn('orders.id', $ids)
                    ->get();

                if (count($orders) != count($ids)) {
                    $data['result'] = false;
                    $ordersNotFound = array_diff($ids, $orders->pluck('id')->toArray());
                    if (count($ordersNotFound)) {
                        $ordersNotFound = Order::whereIn('id', $ordersNotFound)->pluck('order_number')->toArray();
                        $data['not_found'] = implode(',', $ordersNotFound);
                    }
                }
            } elseif (app('request')->input('type') == 'moved_dropped') {
                $orders = Order::where(function ($query) {
                    $query->where('status', 7)
                        ->Orwhere('status', 5)
                        ->orWhere('status', 8)
                        ->orWhere(function ($query) {
                            $query->where('status', 4)
                                ->where('status_before_cancel', '!=', 0);
                        });
                })
                    ->whereNotNull('move_collection_id')
                    ->whereIn('orders.id', $ids)
                    ->get();

                if (count($orders) != count($ids)) {
                    $data['result'] = false;
                    $ordersNotFound = array_diff($ids, $orders->pluck('id')->toArray());
                    if (count($ordersNotFound)) {
                        $ordersNotFound = Order::whereIn('id', $ordersNotFound)->pluck('order_number')->toArray();
                        $data['not_found'] = implode(',', $ordersNotFound);
                    }
                }
            }
        }

        return $data;
    }

    public function scan_action(Request $request)
    {
        $data = ['ids' => []];

        $ids = app('request')->input('ids');
        if (is_array($ids) && count($ids)) {
//            if (app('request')->input('type') == 'moved_to' && app('request')->input('store_id')) {
//                DB::beginTransaction();
//                $store_id = app('request')->input('store_id');
//                try {
//                    Order::whereIn('id', $ids)->update(['store_id' => $store_id]);
//                    event(new OrderUpdated($ids, Auth::guard($this->guardType)->user()->id, 1, 'moved_to', ['store_id' => $store_id]));
//                    $data['ids'] = $ids;
//                    $data['result'] = true;
//                    $data['status'] = "<span class='badge badge-pill label-black'>" . __('backend.dropped') . "</span>";
//                    $data['message'] = __('backend.orders_moved_to');
//                    DB::commit();
//
//                } catch (Exception $e) {
//                    DB::rollback();
//                    $data['result'] = false;
//                    $data['message'] = __('backend.Something_Error');
//                }
//            }
            if (app('request')->input('type') == 'forward' && app('request')->input('driver_id')) {
                DB::beginTransaction();
                try {
//                    Order::whereIn('id', $ids)->update(['status' => 1, 'accepted_at' => Carbon::now()]);
                    foreach ($ids as $key => $value) {
                        //update order status to be 1 when it assign to driver
                        $order = Order::find($value);
                        $order->status = 1;
                        $order->accepted_at = Carbon::now();
                        $order->save();

//                        $accepted_order = new AcceptedOrder();
//                        $accepted_order->order_id = $order->id;
//                        $accepted_order->driver_id = app('request')->input('driver_id');
//                        $accepted_order->delivery_price = $order->delivery_price;
//                        $accepted_order->status = 0;
//                        $accepted_order->save();

                        AcceptedOrder::create([
                            'order_id' => $order->id,
                            'driver_id' => app('request')->input('driver_id'),
                            'status' => 0,
                            'delivery_price' => $order->delivery_price
                        ]);

//                        AcceptedOrder::firstOrCreate(
//                            [
//                                'order_id' => $order->id,
//                                'driver_id' => app('request')->input('driver_id'),
//                                'status' => 0
//                            ],
//                            ['delivery_price' => $order->delivery_price]
//                        );
                    }

                    event(new OrderUpdated($ids, Auth::guard($this->guardType)->user()->id, 1, 'forward', ['driver_id' => app('request')->input('driver_id')]));

                    $data['ids'] = $ids;
                    $data['result'] = true;
                    $data['status'] = "<span class='badge badge-pill label-info'>" . __('backend.accepted') . "</span>";
                    $data['message'] = __('backend.Order_Routed_saved_successfully');

                    $data['received_count'] = Order::join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
                        ->where('orders.status', 2)
                        ->where('accepted_orders.status', 2)
                        ->where('driver_id', app('request')->input('driver_id'))
//                        ->groupBy('orders.id')
                        ->count();
                    $data['recall_count'] = Order::join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
                        ->where('orders.status', 5)
                        ->where('accepted_orders.status', 5)
                        ->where('driver_id', app('request')->input('driver_id'))
//                        ->groupBy('orders.id')
                        ->count();
                    $data['reject_count'] = Order::join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
                        ->where('orders.status', 8)
                        ->where('accepted_orders.status', 8)
                        ->where('driver_id', app('request')->input('driver_id'))
//                        ->groupBy('orders.id')
                        ->count();

                    $data['driver'] = Driver::find(app('request')->input('driver_id'));

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    $data['result'] = false;
                    $data['message'] = __('backend.Something_Error');
                }
            } elseif (app('request')->input('type') == 'receive') {
                DB::beginTransaction();
                try {
                    $accepted_orders = AcceptedOrder::with(['driver', 'order' => function ($query) {
                        $query->with(['customer']);
                    }])->whereIn('order_id', $ids)->where('status', 0)->get();

                    foreach ($accepted_orders as $accepted_order) {
                        $type = 1;
                        if ($accepted_order->driver->agent_id != 1 && $accepted_order->driver->agent_id != null) {
                            $type = 3;
                        }

//                        $invoiceCaptain = Invoice::firstOrCreate(
//                            ['object_id' => $accepted_order->driver->id, 'type' => $type, 'status' => 0],
//                            [
//                                'invoice_no' => uniqid(),
//                                'object_id' => $accepted_order->driver->id,
//                                'type' => $type,
//                                'status' => 0,
//                                'start_at' => date("Y-m-d"),
//                                'start_date' => date("Y-m-d H:i:s")
//                            ]
//                        );
//
//                        InvoiceOrder::create([
//                            'accepted_order_id' => $accepted_order->id,
//                            'invoice_id' => $invoiceCaptain->id,
//                            'price' => $accepted_order->order->delivery_price,
//                        ]);
//
//                        $invoice = Invoice::firstOrCreate(
//                            ['object_id' => $accepted_order->order->customer->corporate_id, 'type' => 2, 'status' => 0],
//                            [
//                                'invoice_no' => uniqid(),
//                                'object_id' => $accepted_order->order->customer->corporate_id,
//                                'type' => 2,
//                                'status' => 0,
//                                'start_at' => date("Y-m-d"),
//                                'start_date' => date("Y-m-d H:i:s")
//                            ]
//                        );
//
//                        InvoiceOrder::create([
//                            'accepted_order_id' => $accepted_order->id,
//                            'invoice_id' => $invoice->id,
//                            'price' => $accepted_order->order->delivery_price,
//                        ]);

                    }

                    AcceptedOrder::whereIn('order_id', $ids)->where('status', 0)->update(['status' => 2, 'old_status' => 2, 'received_date' => date("Y-m-d H:i:s")]);
                    Order::whereIn('id', $ids)->update(['status' => 2, 'received_at' => date("Y-m-d H:i:s")]);
                    event(new OrderUpdated($ids, Auth::guard($this->guardType)->user()->id, 1, 'receive_orders'));
//                    $orders->each(function ($item, $key) {
//                        $item->update(['status' => 2, 'received_at' => date("Y-m-d H:i:s")]);
//                    });
//                    foreach ($orders as $order) {
//                        $order->update(['status' => 2, 'received_at' => date("Y-m-d H:i:s")]);
//                    }
//                    foreach ($ids as $id) {
//                        Order::find($id)->update(['status' => 2, 'received_at' => date("Y-m-d H:i:s")]);
//                    }

                    $data['driver'] = $accepted_orders->first()->driver;
                    $data['ids'] = $ids;
                    $data['result'] = true;
                    $data['status'] = "<span class='badge badge-pill label-warning'>" . __('backend.received') . "</span>";
                    $data['message'] = __('backend.orders_received');

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    $data['result'] = false;
                    $data['message'] = __('backend.Something_Error');
                }
            } elseif (app('request')->input('type') == 'deliver') {
                DB::beginTransaction();
                try {

                    AcceptedOrder::whereIn('order_id', $ids)->where('status', 2)->update(['status' => 3, 'delivery_date' => date("Y-m-d H:i:s")]);
                    Order::whereIn('id', $ids)->where('status', 2)->update(['status' => 3, 'delivered_at' => date("Y-m-d H:i:s")]);

                    $tableName = Order::getModel()->getTable();

                    $multipleData = array_filter(app('request')->input('idsWithCollectedCost'), function ($value) {
                        return (!isset($value['delivery_status']) || in_array($value['delivery_status'], [1, 2, 4]));
                    });

                    if ($tableName && !empty($multipleData)) {

                        // column or fields to update
                        $updateColumn = ['id', 'collected_cost', 'delivery_status']; //array_keys($multipleData[0]);
                        $referenceColumn = $updateColumn[0]; //e.g id
                        unset($updateColumn[0]);
                        $whereIn = "";

                        $q = "UPDATE " . $tableName . " SET ";
                        foreach ($updateColumn as $uColumn) {
                            $q .= $uColumn . " = CASE ";

                            foreach ($multipleData as $data) {
                                $q .= "WHEN " . $referenceColumn . " = " . $data[$referenceColumn] . " THEN '" . (!empty($data[$uColumn]) ? $data[$uColumn] : $uColumn) . "' ";
                            }
                            $q .= "ELSE " . $uColumn . " END, ";
                        }
                        foreach ($multipleData as $data) {
                            $whereIn .= "'" . $data[$referenceColumn] . "', ";
                        }
                        $q = rtrim($q, ", ") . " WHERE " . $referenceColumn . " IN (" . rtrim($whereIn, ', ') . ")";

                        // Update
                        \DB::update(\DB::raw($q));

                    }

                    $idsWithCollectedCost = $request->idsWithCollectedCost;
                    $idsWithCollectedCostCollection = collect($request->idsWithCollectedCost);
                    $fullDeliveryIds = $idsWithCollectedCostCollection->where('delivery_status', 1)->pluck('id')->toArray();
                    $partDeliveryIds = $idsWithCollectedCostCollection->where('delivery_status', 2)->pluck('id')->toArray();
                    $replaceDeliveryIds = $idsWithCollectedCostCollection->where('delivery_status', 4)->pluck('id')->toArray();
//                    $rejectDeliveryIds = $idsWithCollectedCostCollection->where('delivery_status', 3)->pluck('id')->toArray();
//                    $recallDeliveryIds = $idsWithCollectedCostCollection->where('delivery_status', 5)->pluck('id')->toArray();
                    $deliveryIds = array_values(array_map(function ($value) {
                        return $value['id'];
                    }, array_filter($idsWithCollectedCost, function ($value) {
                        return !isset($value['delivery_status']);
                    })));

                    event(new OrderUpdated($deliveryIds, Auth::guard($this->guardType)->user()->id, 1, 'deliver'));
                    event(new OrderUpdated($fullDeliveryIds, Auth::guard($this->guardType)->user()->id, 1, 'deliver'));
                    event(new OrderUpdated($partDeliveryIds, Auth::guard($this->guardType)->user()->id, 1, 'part_delivered'));
                    event(new OrderUpdated($replaceDeliveryIds, Auth::guard($this->guardType)->user()->id, 1, 'replace'));
//                    event(new OrderUpdated($rejectDeliveryIds, Auth::guard($this->guardType)->user()->id, 1, 'reject'));
//                    event(new OrderUpdated($recallDeliveryIds, Auth::guard($this->guardType)->user()->id, 1, 'recall', ['recalled_by' => 2]));

                    $data['ids'] = $ids;
                    $data['result'] = true;
                    $data['status'] = "<span class='badge badge-pill label-success'>" . __('backend.delivered') . "</span>";
                    $data['message'] = __('backend.orders_delivered');

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    $data['result'] = false;
                    $data['message'] = __('backend.Something_Error');
                }
            } elseif (app('request')->input('type') == 'recall') {
                DB::beginTransaction();
                try {
                    $dataUpdated = ['status' => 5, 'recalled_at' => date("Y-m-d H:i:s"), 'recalled_by' => 2];
//                    if(app('request')->has('one_row')){
//                        $dataUpdated['collected_cost'] = app('request')->input('collected_cost');
//                    }
                    AcceptedOrder::whereIn('order_id', $ids)->where('status', 2)->update(['status' => 5]);
                    Order::whereIn('id', $ids)->update($dataUpdated);
                    event(new OrderUpdated($ids, Auth::guard($this->guardType)->user()->id, 1, 'recall', ['recalled_by' => 2]));

                    $data['ids'] = $ids;
                    $data['result'] = true;
                    $data['status'] = "<span class='badge badge-pill label-danger'>" . __('backend.recalled') . "</span>";
                    $data['message'] = __('backend.orders_recalled');

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    $data['result'] = false;
                    $data['message'] = __('backend.Something_Error');
                }
            } elseif (app('request')->input('type') == 'action_back') {
                DB::beginTransaction();
                try {
                    $status = $request->action_status;
                    $orders = Order::whereIn('id', $ids)->get();
                    foreach ($orders as $order) {
                        if ($status == 2) {
                            if ($order->status == 3) {
                                AcceptedOrder::where('order_id', $order->id)
                                    ->where('status', 3)
                                    ->update(['status' => 2, 'delivery_date' => null]);
                                Order::where('id', $order->id)
                                    ->update(['status' => 2, 'delivered_at' => null, 'delivery_status' => null, 'collected_cost' => null]);
                            } elseif ($order->status == 5) {
                                AcceptedOrder::where('order_id', $order->id)
                                    ->where('status', 5)
                                    ->update(['status' => 2]);
                                Order::where('id', $order->id)
                                    ->update(['status' => 2, 'recalled_at' => null, 'recalled_by' => null, 'delivery_status' => null, 'collected_cost' => null]);
                            } elseif ($order->status == 8) {
                                AcceptedOrder::where('order_id', $order->id)
                                    ->where('status', 8)
                                    ->update(['status' => 2]);
                                Order::where('id', $order->id)
                                    ->update(['status' => 2, 'rejected_at' => null, 'delivery_status' => null, 'collected_cost' => null]);
                            }
                            $data['status'] = "<span class='badge badge-pill label-warning'>" . __('backend.received') . "</span>";
                            event(new OrderUpdated([$order->id], Auth::guard($this->guardType)->user()->id, 1, 'receive_orders'), ['update_reason' => 'action_back']);
                        } elseif ($status == 3) {
                            if ($order->status == 5) {
                                AcceptedOrder::where('order_id', $order->id)
                                    ->where('status', 5)
                                    ->update(['status' => 3, 'delivery_date' => \DB::raw('received_date')]);
                                Order::where('id', $order->id)
                                    ->update(['status' => 3, 'recalled_at' => null, 'recalled_by' => null,
                                        'received_at' => \DB::raw('accepted_at'), 'delivered_at' => \DB::raw('accepted_at'),
                                        'delivery_status' => null, 'collected_cost' => null]);
                            } elseif ($order->status == 8) {
                                AcceptedOrder::where('order_id', $order->id)
                                    ->where('status', 8)
                                    ->update(['status' => 3, 'delivery_date' => \DB::raw('received_date')]);
                                Order::where('id', $order->id)
                                    ->update(['status' => 3, 'rejected_at' => null,
                                        'received_at' => \DB::raw('accepted_at'), 'delivered_at' => \DB::raw('accepted_at'),
                                        'delivery_status' => null, 'collected_cost' => null]);
                            }
                            $data['status'] = "<span class='badge badge-pill label-success'>" . __('backend.delivered') . "</span>";
                            event(new OrderUpdated([$order->id], Auth::guard($this->guardType)->user()->id, 1, 'deliver'), ['update_reason' => 'action_back']);
                        } elseif ($status == 5) {
                            if ($order->status == 3) {
                                AcceptedOrder::where('order_id', $order->id)
                                    ->where('status', 3)
                                    ->update(['status' => 5]);
                                Order::where('id', $order->id)
                                    ->update(['status' => 5, 'recalled_at' => \DB::raw('delivered_at'), 'recalled_by' => 2,
                                        'delivered_at' => null, 'delivery_status' => null, 'collected_cost' => null]);
                            }
                            $data['status'] = "<span class='badge badge-pill label-danger'>" . __('backend.recalled') . "</span>";
                            event(new OrderUpdated([$order->id], Auth::guard($this->guardType)->user()->id, 1, 'recall', ['update_reason' => 'action_back', 'recalled_by' => 2]));
                        } elseif ($status == 8) {
                            if ($order->status == 3) {
                                AcceptedOrder::where('order_id', $order->id)
                                    ->where('status', 3)
                                    ->update(['status' => 8]);
                                Order::where('id', $order->id)
                                    ->update(['status' => 8, 'rejected_at' => \DB::raw('delivered_at'),
                                        'delivered_at' => null, 'delivery_status' => null, 'collected_cost' => null]);
                            }
                            $data['status'] = "<span class='badge badge-pill label-danger'>" . __('backend.rejected') . "</span>";
                            event(new OrderUpdated([$order->id], Auth::guard($this->guardType)->user()->id, 1, 'reject', ['update_reason' => 'action_back']));
                        }
                    }

                    $data['ids'] = $ids;
                    $data['result'] = true;
                    $data['message'] = __('backend.orders_recalled');

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    $data['result'] = false;
                    $data['message'] = __('backend.Something_Error');
                }
            } elseif (app('request')->input('type') == 'reject') {
                DB::beginTransaction();
                try {
                    $dataUpdated = ['status' => 8, 'rejected_at' => date("Y-m-d H:i:s")];
                    if (app('request')->has('one_row')) {
                        $dataUpdated['collected_cost'] = app('request')->input('collected_cost');
                    }
                    AcceptedOrder::whereIn('order_id', $ids)->where('status', 2)->update(['status' => 8]);
                    Order::whereIn('id', $ids)->update($dataUpdated);
                    event(new OrderUpdated($ids, Auth::guard($this->guardType)->user()->id, 1, 'reject'));

                    $data['ids'] = $ids;
                    $data['result'] = true;
                    $data['status'] = "<span class='badge badge-pill label-danger'>" . __('backend.rejected') . "</span>";
                    $data['message'] = __('backend.orders_rejected');

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    $data['result'] = false;
                    $data['message'] = __('backend.Something_Error');
                }
            } elseif (app('request')->input('type') == 'cancel') {
                DB::beginTransaction();
                try {
                    $cancelled_ids = [];
                    $recalled_ids = [];
                    $data['ids_status'] = [];
                    foreach ($ids as $id) {
                        $order = Order::where('id', $id)->first();
                        $orderReceivedCount = AcceptedOrder::where('order_id', $id)->where('old_status', 2)->count();
                        if (($order->status == 2 && $orderReceivedCount >= 2) || (in_array($order->status, [1, 7]) && $orderReceivedCount >= 1)) {
                            $recalled_ids[] = $id;
                            $acceptedOrder = AcceptedOrder::where('order_id', $id)->orderBy('created_at', 'DESC')->first();
                            $acceptedOrder->update(['status' => 5]);
                            Order::find($id)
                                ->update(['status' => 5, 'recalled_at' => date("Y-m-d H:i:s"), 'recalled_by' => 2,
                                    'warehouse_dropoff' => 1, 'warehouse_dropoff_date' => date("Y-m-d H:i:s")]);
                            $data['ids_status'][] = ['id' => $id, 'status' => "<span class='badge badge-pill label-danger'>" . __('backend.recalled') . "</span>"];
                        } else {
                            $cancelled_ids[] = $id;
                            AcceptedOrder::where('order_id', $id)->whereIn('status', [0, 2])
                                ->update(['status' => 4, 'cancelled_by' => 1, 'cancelled_at' => date("Y-m-d H:i:s")]);
                            $status_before_cancel = $order->status;
                            Order::find($id)
                                ->update(['status_before_cancel' => $status_before_cancel, 'status' => '4', 'cancelled_by' => '2', 'cancelled_at' => Carbon::now(),
                                    'warehouse_dropoff' => 1, 'warehouse_dropoff_date' => date("Y-m-d H:i:s")]);
                            $data['ids_status'][] = ['id' => $id, 'status' => "<span class='badge badge-pill label-danger'>" . __('backend.cancelled') . "</span>"];
                        }

                    }

//                    AcceptedOrder::whereIn('order_id', $ids)->whereIn('status', [0, 2])->update(['status' => 4, 'cancelled_by' => 1, 'cancelled_at' => date("Y-m-d H:i:s")]);
//
//                    foreach ($ids as $id) {
//                        $order = Order::find($id);
//                        $order->status_before_cancel = $order->status;
//                        $order->status = 4;
//                        $order->cancelled_by = 2;
//                        $order->cancelled_at = date("Y-m-d H:i:s");
//                        $order->save();
//                    }

                    event(new OrderUpdated($cancelled_ids, Auth::guard($this->guardType)->user()->id, 1, 'cancel', ['cancelled_by' => 2]));
                    event(new OrderUpdated($recalled_ids, Auth::guard($this->guardType)->user()->id, 1, 'recall', ['recalled_by' => 2]));

                    $data['ids'] = $ids;
                    $data['result'] = true;
                    $data['status'] = "<span class='badge badge-pill label-danger'>" . __('backend.cancelled') . "</span>";
                    $data['message'] = __('backend.orders_cancelled');

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    $data['result'] = false;
                    $data['message'] = __('backend.Something_Error');
                }
            } elseif (app('request')->input('type') == 'dropped') {
                DB::beginTransaction();
                try {
                    $data['ids_status'] = [];

                    $orderReceivedToRecallIds = Order::whereIn('id', $ids)->where('recall_if_dropped', 1)->where('status', 2)->pluck('id')->toArray();
                    $orderReceivedNotToRecallIds = array_diff($ids, $orderReceivedToRecallIds);

                    Order::whereIn('id', $orderReceivedNotToRecallIds)
                        ->where('status', 0)
                        ->whereNull('warehouse_dropped_at')
                        ->update(['warehouse_dropped_at' => date("Y-m-d H:i:s")]);

                    AcceptedOrder::whereIn('order_id', $orderReceivedNotToRecallIds)->whereIn('status', [0, 2, 3, 5, 8])->update(['status' => 4, 'cancelled_by' => 1, 'cancelled_at' => date("Y-m-d H:i:s")]);
                    Order::whereIn('id', $orderReceivedNotToRecallIds)->update([
                        'status' => 7,
                        'accepted_at' => null,
                        'captain_received_time' => null,
                        'delivery_status' => null,
                        'warehouse_dropoff' => 0,
                        'warehouse_dropoff_date' => null,
                        'client_dropoff_date' => null,
                        'client_dropoff' => 0,
                        'received_at' => null,
                        'delivered_at' => null,
                        'recalled_at' => null,
                        'rejected_at' => null,
                        'recalled_by' => null,
                        'dropped_at' => date("Y-m-d H:i:s")
                    ]);

                    $collection = collect($orderReceivedNotToRecallIds);
                    foreach ($collection->chunk(100) as $items) {
                        event(new OrderUpdated($items->toArray(), Auth::guard($this->guardType)->user()->id, 1, 'dropped'));
                    }

                    AcceptedOrder::whereIn('order_id', $orderReceivedToRecallIds)->where('status', 2)->update(['status' => 5]);
                    Order::whereIn('id', $orderReceivedToRecallIds)->update(['status' => 5, 'recalled_at' => date("Y-m-d H:i:s"), 'recalled_by' => 2]);
                    event(new OrderUpdated($orderReceivedToRecallIds, Auth::guard($this->guardType)->user()->id, 1, 'recall', ['recalled_by' => 2]));

                    foreach ($orderReceivedToRecallIds as $orderReceivedToRecallId) {
                        $data['ids_status'][] = ['id' => $orderReceivedToRecallId, 'status' => "<span class='badge badge-pill label-danger'>" . __('backend.recalled') . "</span>"];
                    }

                    foreach ($orderReceivedNotToRecallIds as $orderReceivedNotToRecallId) {
                        $data['ids_status'][] = ['id' => $orderReceivedNotToRecallId, 'status' => "<span class='badge badge-pill label-black'>" . __('backend.dropped') . "</span>"];
                    }

                    $data['ids'] = $ids;
                    $data['result'] = true;
                    $data['status'] = "<span class='badge badge-pill label-black'>" . __('backend.dropped') . "</span>";
                    $data['message'] = __('backend.orders_dropped');

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    $data['result'] = false;
                    $data['message'] = __('backend.Something_Error');
                }
            } elseif (app('request')->input('type') == 'drop_and_exit_again') {
                DB::beginTransaction();
                try {
                    $driver_id = AcceptedOrder::where('order_id', $ids[0])->whereIn('status', [0, 2, 3, 5, 8])->value('driver_id');
                    AcceptedOrder::whereIn('order_id', $ids)->whereIn('status', [0, 2, 3, 5, 8])->update(['status' => 4, 'cancelled_by' => 1, 'cancelled_at' => date("Y-m-d H:i:s")]);
                    Order::whereIn('id', $ids)->update([
                        'status' => 7,
                        'accepted_at' => null,
                        'captain_received_time' => null,
                        'delivery_status' => null,
                        'warehouse_dropoff' => 0,
                        'warehouse_dropoff_date' => null,
                        'client_dropoff_date' => null,
                        'client_dropoff' => 0,
                        'received_at' => null,
                        'delivered_at' => null,
                        'recalled_at' => null,
                        'rejected_at' => null,
                        'recalled_by' => null,
                        'dropped_at' => date("Y-m-d H:i:s")
                    ]);
                    event(new OrderUpdated($ids, Auth::guard($this->guardType)->user()->id, 1, 'dropped'));

                    $data['ids'] = $ids;
                    $data['result'] = true;
                    $data['status'] = "<span class='badge badge-pill label-black'>" . __('backend.dropped') . "</span>";
                    $data['message'] = __('backend.orders_dropped');

                    foreach ($ids as $key => $value) {
                        //update order status to be 1 when it assign to driver
                        $order = Order::find($value);
                        $order->status = 1;
                        $order->accepted_at = Carbon::now();
                        $order->save();

                        AcceptedOrder::create([
                            'order_id' => $order->id,
                            'driver_id' => $driver_id,
                            'status' => 0,
                            'delivery_price' => $order->delivery_price
                        ]);

//                        AcceptedOrder::firstOrCreate(
//                            [
//                                'order_id' => $order->id,
//                                'driver_id' => $driver_id,
//                                'status' => 0
//                            ],
//                            ['delivery_price' => $order->delivery_price]
//                        );
                    }

                    event(new OrderUpdated($ids, Auth::guard($this->guardType)->user()->id, 1, 'forward', ['driver_id' => $driver_id]));


                    $accepted_orders = AcceptedOrder::with(['driver', 'order' => function ($query) {
                        $query->with(['customer']);
                    }])->whereIn('order_id', $ids)->where('status', 0)->get();

                    foreach ($accepted_orders as $accepted_order) {
                        $type = 1;
                        if ($accepted_order->driver->agent_id != 1 && $accepted_order->driver->agent_id != null) {
                            $type = 3;
                        }

//                        $invoiceCaptain = Invoice::firstOrCreate(
//                            ['object_id' => $accepted_order->driver->id, 'type' => $type, 'status' => 0],
//                            [
//                                'invoice_no' => uniqid(),
//                                'object_id' => $accepted_order->driver->id,
//                                'type' => $type,
//                                'status' => 0,
//                                'start_at' => date("Y-m-d"),
//                                'start_date' => date("Y-m-d H:i:s")
//                            ]
//                        );
//
//                        InvoiceOrder::create([
//                            'accepted_order_id' => $accepted_order->id,
//                            'invoice_id' => $invoiceCaptain->id,
//                            'price' => $accepted_order->order->delivery_price,
//                        ]);
//
//                        $invoice = Invoice::firstOrCreate(
//                            ['object_id' => $accepted_order->order->customer->corporate_id, 'type' => 2, 'status' => 0],
//                            [
//                                'invoice_no' => uniqid(),
//                                'object_id' => $accepted_order->order->customer->corporate_id,
//                                'type' => 2,
//                                'status' => 0,
//                                'start_at' => date("Y-m-d"),
//                                'start_date' => date("Y-m-d H:i:s")
//                            ]
//                        );
//
//                        InvoiceOrder::create([
//                            'accepted_order_id' => $accepted_order->id,
//                            'invoice_id' => $invoice->id,
//                            'price' => $accepted_order->order->delivery_price,
//                        ]);

                    }

                    AcceptedOrder::whereIn('order_id', $ids)->where('status', 0)->update(['status' => 2, 'old_status' => 2, 'received_date' => date("Y-m-d H:i:s")]);
                    Order::whereIn('id', $ids)->update(['status' => 2, 'received_at' => date("Y-m-d H:i:s")]);
                    event(new OrderUpdated($ids, Auth::guard($this->guardType)->user()->id, 1, 'receive_orders'));

                    $this->save_captain_exit_orders($request, $ids, $driver_id);

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    $data['result'] = false;
                    $data['message'] = __('backend.Something_Error');
                }
            } elseif (app('request')->input('type') == 'pickup_dropped') {
                DB::beginTransaction();
                try {
                    // call drop_pickup from pickupController
                    PickupController::drop_pickups($ids);

                    $data['ids'] = $ids;
                    $data['result'] = true;
                    $data['status'] = "<span class='badge badge-pill label-black'>" . __('backend.dropped') . "</span>";
                    $data['message'] = __('backend.drop_off_package');

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    $data['result'] = false;
                    $data['message'] = __('backend.Something_Error');
                }
            } elseif (app('request')->input('type') == 'recall_drop') {
                DB::beginTransaction();
                try {
                    Order::whereIn('id', $ids)->update(['warehouse_dropoff' => 1, 'warehouse_dropoff_date' => date("Y-m-d H:i:s")]);
                    event(new OrderUpdated($ids, Auth::guard($this->guardType)->user()->id, 1, 'recall_drop'));

                    $data['ids'] = $ids;
                    $data['result'] = true;
                    $data['message'] = __('backend.drop_off_package');

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    $data['result'] = false;
                    $data['message'] = __('backend.Something_Error');
                }
            } elseif (app('request')->input('type') == 'client_drop') {
                DB::beginTransaction();
                try {
                    $customer_id = $request->customer_id;
                    $serial_code = $this->randomNumber(2);

                    $start_date = Order::whereIn('id', $ids)->min('created_at');
                    $end_date = Order::whereIn('id', $ids)->max('created_at');

                    $refund_collection = RefundCollection::create([
                        'customer_id' => $customer_id,
                        'serial_code' => $serial_code,
                        'start_date' => date('Y-m-d', strtotime($start_date)),
                        'end_date' => date('Y-m-d', strtotime($end_date)),
                    ]);

                    Order::whereIn('id', $ids)->update([
                        'refund_collection_id' => $refund_collection->id,
                        'client_dropoff' => 1,
                        'client_dropoff_date' => date('Y-m-d H:i:s')
                    ]);
                    event(new OrderUpdated($ids, Auth::guard($this->guardType)->user()->id, 1, 'client_drop'));

                    $data['refund_collection_id'] = $refund_collection->id;
                    $data['ids'] = $ids;
                    $data['result'] = true;
                    $data['message'] = __('backend.drop_off_package');

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    $data['result'] = false;
                    $data['message'] = __('backend.Something_Error');
                }
            } elseif (app('request')->input('type') == 'moved_dropped') {
                DB::beginTransaction();
                try {
                    $stores = [];
                    foreach ($ids as $id) {
                        $collection_id = Order::where('id', $id)->pluck('move_collection_id')->first();
                        $collection = MoveCollection::find($collection_id);
                        MoveCollectionOrder::where('order_id', $id)->where('collection_id', $collection_id)->update(['dropped' => 1, 'dropped_at' => Carbon::now()]);
                        Order::where('id', $id)->update(['moved_in' => $collection->store_id, 'moved_at' => Carbon::now(), 'move_collection_id' => null]);
                        $stores[$collection->store_id][] = $id;
                    }

                    foreach ($stores as $key => $order_ids) {
                        event(new OrderUpdated($order_ids, Auth::guard($this->guardType)->user()->id, 1, 'moved_dropped', ['store_id' => $key]));
                    }

                    $data['ids'] = $ids;
                    $data['result'] = true;
                    $data['message'] = __('backend.drop_off_package');

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    $data['result'] = false;
                    $data['message'] = __('backend.Something_Error');
                }
            } elseif (app('request')->input('type') == 'comments') {
                DB::beginTransaction();
                try {
                    $comments = $request->only('order_id', 'latitude', 'longitude', 'driver_id', 'comment', 'created_at');
                    if (!$comments['created_at']) {
                        unset($comments['created_at']);
                    }
                    $comments['user_id'] = Auth::guard($this->guardType)->user()->id;

                    foreach ($ids as $key => $value) {
                        $comments['order_id'] = $value;

                        OrderComment::create($comments);

                        event(new OrderUpdated([$value],
                            Auth::guard($this->guardType)->user()->id,
                            1,
                            'comments',
                            ['comment' => $request->get('comment')]
                        ));
                    }

                    $data['ids'] = $ids;
                    $data['result'] = true;
                    $data['message'] = __('backend.add_order_comment_successfully');

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    $data['result'] = false;
                    $data['message'] = __('backend.Something_Error');
                }
            } elseif (app('request')->input('type') == 'problems') {
                DB::beginTransaction();
                try {
                    $before_6_hours = date("Y-m-d H:i:s", strtotime('-6 hours'));
                    $problems_count = OrderDeliveryProblem::whereIn('order_id', $ids)
                        ->where('created_at', '>', $before_6_hours)
                        ->orderBy('created_at', 'DESC')
                        ->count();

                    if ($problems_count) {
                        $data['result'] = false;
                        $data['message'] = __('backend.cannot_send_delivery_problem');

                        return $data;
                    }

                    $problems = $request->only('latitude', 'longitude', 'driver_id', 'delivery_problem_id', 'reason', 'delay_at');
                    $problems['created_by'] = Auth::guard($this->guardType)->user()->id;
                    $problem_radio = $request->problem_radio;

                    foreach ($ids as $key => $value) {
                        $problem = OrderDeliveryProblem::where('order_id', $value)
                            ->where('created_at', '>', $before_6_hours)
                            ->orderBy('created_at', 'DESC')
                            ->first();

                        if ($problem) {
                            $data['result'] = false;
                            $data['message'] = __('backend.cannot_send_delivery_problem');

                            return $data;
                        } else {
                            $problems['order_id'] = $value;

                            OrderDeliveryProblem::create($problems);

                            event(new OrderUpdated([$value],
                                Auth::guard($this->guardType)->user()->id,
                                1,
                                'problems',
                                ['reason' => $request->get('reason'), 'problem_id' => $request->get('delivery_problem_id')]
                            ));

                            $problem_count = OrderDeliveryProblem::where('order_id', $value)->count();
                            if ($problem_count >= 5 || $problem_radio === 'recall') {
                                AcceptedOrder::where('order_id', $value)
                                    ->where('status', 2)
                                    ->update(['status' => 5]);
                                Order::where('id', $value)
                                    ->where('status', 2)
                                    ->update(['status' => 5, 'recalled_at' => date("Y-m-d H:i:s"), 'recalled_by' => 2]);
                                event(new OrderUpdated([$value], Auth::guard($this->guardType)->user()->id, 1, 'recall', ['recalled_by' => 2]));
                                $data['status'] = "<span class='badge badge-pill label-danger'>" . __('backend.recalled') . "</span>";
                            } else if ($problem_radio === 'delay') {
                                AcceptedOrder::where('order_id', $value)
                                    ->where('status', 2)
                                    ->update(['status' => 4, 'cancelled_by' => 1, 'cancelled_at' => date("Y-m-d H:i:s")]);
                                Order::where('id', $value)
                                    ->where('status', 2)
                                    ->update(['status' => 7, 'delay_at' => $request->delay_at]);
                                event(new OrderUpdated([$value], Auth::guard($this->guardType)->user()->id, 1, 'dropped'));
                                $data['status'] = "<span class='badge badge-pill label-black'>" . __('backend.dropped') . "</span>";
                            }
                        }
                    }

                    $data['ids'] = $ids;
                    $data['result'] = true;
                    $data['message'] = __('backend.add_problem_successfully');

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    $data['result'] = false;
                    $data['message'] = __('backend.Something_Error');
                }
            }
        }

        return $data;
    }

    public function pickups($code)
    {
        $user = Auth::guard($this->guardType)->user();

        $pickups = PickUp::with('corporate', 'driver')
            ->where('pick_ups.status', '!=', 3)
            ->whereIn('pickup_number', $code);

        if ($user->is_warehouse_user) {
            $pickups = $pickups->join('customers', 'customers.id', '=', 'pick_ups.customer_id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id);
        }

        $pickup = $pickups->select('pick_ups.*')->first();

        $data = [
            'count' => ($pickup ? 1 : 0),
            'id' => ($pickup ? $pickup->id : 0)
        ];

        if (app('request')->input('init') == 1) {
            $data['view'] = view('backend.scan.pickup-scan-table', compact('pickup'))->render();
        } else {
            $data['view'] = view('backend.scan.pickup-scan-tr', compact('pickup'))->render();
        }

        return $data;
    }

    public function scan_page(Request $request)
    {
        $user = Auth::guard($this->guardType)->user();

        $code = [];
        $additional_filter = null;
        if (app('request')->has('code') && app('request')->input('code')) {
            $code = array_map('trim', explode(',', app('request')->input('code')));

            if (app('request')->has('filter') && app('request')->input('filter')) {
                $additional_filter = app('request')->input('filter');
            }

        }

        /** type to display any order  */
        $orders = null;

        if (!empty($code) && isset($code)) {
            if ($additional_filter === 'quick_entry') {
                $orders = Order::join('scan_collection_orders', 'scan_collection_orders.order_id', '=', 'orders.id')
                    ->join('scan_collections', 'scan_collection_orders.collection_id', '=', 'scan_collections.id')
                    ->where('scan_type', 2)
                    ->whereIn('status', [1, 2, 3, 5, 8])
                    ->where(function ($query) use ($code) {
                        $query->whereIn('collection_number', $code);
                        foreach ($code as $item) {
                            if (is_numeric($item) && strlen($item) % 2 == 0) {
                                $query->orWhere('collection_number', '=', substr($item, 1));
                            }
                        }
                    });
            } elseif ($additional_filter === 'captain_entry') {
                $orders = Order::whereIn('status', [1, 2, 3, 5, 8]);
            } else if ($additional_filter === 'captain_exit') {
                $orders = Order::whereIn('status', [1, 2, 7]);
            } else if ($additional_filter === 'pending_dropped') {
                $orders = Order::where('status', 0);
            } else if ($additional_filter === 'moved_to') {
                $orders = Order::where(function ($query) {
                    $query->where('status', 7)
                        ->orWhere(function ($query) {
                            $query->where('status', 5)
                                ->where('warehouse_dropoff', 1)
                                ->where('client_dropoff', 0);
                        })
                        ->orWhere(function ($query) {
                            $query->where('status', 8)
                                ->where('warehouse_dropoff', 1)
                                ->where('client_dropoff', 0);
                        })
                        ->orWhere(function ($query) {
                            $query->where('status', 4)
                                ->where('warehouse_dropoff', 1)
                                ->where('client_dropoff', 0)
                                ->where('status_before_cancel', '!=', 0);
                        });
                })
                    ->whereNull('move_collection_id');
            } else if ($additional_filter === 'moved_dropped') {
                $orders = Order::where(function ($query) {
                    $query->where('status', 7)
                        ->Orwhere('status', 5)
                        ->Orwhere('status', 8)
                        ->orWhere(function ($query) {
                            $query->where('status', 4)
                                ->where('status_before_cancel', '!=', 0);
                        });
                })
                    ->whereNotNull('move_collection_id');
            } else if ($additional_filter === 'recall_drop') {
                $orders = Order::where(function ($query) {
                    $query->where('status', 5)
                        ->Orwhere('status', 8)
                        ->orWhere(function ($query) {
                            $query->where('status', 3)
                                ->where('delivery_status', 2);
                        })
                        ->orWhere(function ($query) {
                            $query->where('status', 3)
                                ->where('delivery_status', 4);
                        })
                        ->orWhere(function ($query) {
                            $query->where('status', 4)
                                ->where('status_before_cancel', '!=', 0);
                        });
                })
                    ->where(function ($query) {
                        $query->where('warehouse_dropoff', 0)
                            ->orWhereNull('warehouse_dropoff');
                    })
                    ->where(function ($query) {
                        $query->where('client_dropoff', 0)
                            ->orWhereNull('client_dropoff');
                    });
            } else if ($additional_filter === 'client_drop') {
                $orders = Order::where(function ($query) {
                    $query->where('status', 5)
                        ->orWhere('status', 8)
                        ->orWhere(function ($query) {
                            $query->where('status', 3)
                                ->where('delivery_status', 2);
                        })
                        ->orWhere(function ($query) {
                            $query->where('status', 3)
                                ->where('delivery_status', 4);
                        })
                        ->orWhere(function ($query) {
                            $query->where('status', 4)
                                ->where('status_before_cancel', '!=', 0);
                        });
                })
                    ->where('warehouse_dropoff', 1)
                    ->where('client_dropoff', 0);
            } else if ($additional_filter === 'pickup_dropped') {
                return $this->pickups($code);
            } else {
                $orders = new Order;
            }

            if ($user->is_warehouse_user) {
                if ($additional_filter === 'moved_dropped') {
                    $orders = $orders->join('move_collections', 'move_collections.id', '=', 'orders.move_collection_id')
                        ->join('store_user', 'store_user.store_id', '=', 'move_collections.store_id')
                        ->where('user_id', $user->id);
                } else {
                    $orders = $orders->join('store_user', 'store_user.store_id', '=', 'orders.moved_in')
                        ->where('user_id', $user->id);
                }
            }

            if ($additional_filter !== 'quick_entry') {
                $orders = $orders->where(function ($query) use ($code) {
                    $query->whereIn('order_number', $code)
                        ->orWhereIn('order_no', $code);
                    foreach ($code as $item) {
                        if (is_numeric($item) && strlen($item) % 2 == 0) {
                            $query->orWhere('order_number', '=', substr($item, 1));
                        }
                    }
                });
            }

            if ($request->rearrange) {
                $sort_status = (int)$request->sort_status;
                $allStatus = [0, 1, 2, 3, 4, 5, 7, 8];
                if (($key = array_search($sort_status, $allStatus, true)) !== false) {
                    unset($allStatus[$key]);
                    array_unshift($allStatus, $sort_status);
                }

                $allStatus = implode($allStatus, ',');

                $orders->orderByRaw("FIELD(orders.status, $allStatus)");
            }
            $orders = $orders->with('comments', 'to_government', 'from_government')
                ->where('is_refund', '=', 0)
                ->withCount('cancel_requests')
                ->select('orders.*')
                ->get();

        }

        $agents = Agent::get();
        $stores = Store::get();

        $online_drivers = Driver::where('is_active', 1)->select('drivers.*')->orderBy('drivers.name', 'ASC');

        if ($user->is_warehouse_user) {
            $online_drivers = $online_drivers->join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                ->where('user_id', $user->id);
        }

        $online_drivers = $online_drivers->get();

        /** if this ajax request mean that scan button pressed */
        if ($request->ajax()) {

            $orderIDs = (!empty($orders) && count($orders) ? $orders->pluck('order_number')->toArray() : []);

            if ($additional_filter == 'quick_entry') {
                $scanCollections = ScanCollection::where('scan_type', 2)
                    ->whereIn('collection_number', $code)
                    ->pluck('collection_number', 'id')
                    ->toArray();
                $ordersNotFound = array_diff($code, $scanCollections);
            } else {
                foreach ($code as $key => $item) {
                    $newItem = '';
                    if (is_numeric($item) && strlen($item) % 2 == 0) {
                        $newItem = substr($item, 1);
                    }

                    if (!in_array($item, $orderIDs, true) && in_array($newItem, $orderIDs)) {
                        $code[$key] = $newItem;
                    }
                }

                $ordersNotFound = array_diff($code, array_values($orderIDs));
            }

            $data = [
                'count' => !empty($orderIDs) ? count($orderIDs) : [],
                'ids' => array_keys($orderIDs),
                'not_found' => implode(',', $ordersNotFound)
            ];

            if (app('request')->input('init') == 1) {
                $data['view'] = view('backend.scan.scan-table', compact('orders', 'agents', 'stores', 'additional_filter', 'online_drivers'))->render();
            } else {
                if (!empty($orders) && count($orders)) {
                    foreach ($orders as $order) {
                        $data['views'][] = [
                            'view' => view('backend.scan.scan-tr', compact('order', 'agents', 'stores', 'additional_filter', 'online_drivers'))->render(),
                            'id' => $order->id
                        ];
                    }
                }
            }

            return $data;
        }

        $problems = DeliveryProblem::get();

        // display scan page if request get from browser not ajax
        return view('backend.scan.scan', compact('online_drivers', 'problems', 'stores'));
    }/*scan_page*/

    public function change_delivery_status(Request $request)
    {
        $orderData['delivery_status'] = $request->delivery_status;
        $orderData['collected_cost'] = $request->collected_cost;
        if ($request->delivery_status == 1 || $request->delivery_status == 2 || $request->delivery_status == 4) {
            $orderData['status'] = 3;
            $orderData['delivered_at'] = date("Y-m-d H:i:s");
            AcceptedOrder::where('order_id', $request->id)
                ->where('status', 2)
                ->update(['status' => 3, 'delivery_date' => date("Y-m-d H:i:s")]);

            $data['status'] = "<span class='badge badge-pill label-success'>" . __('backend.delivered') . "</span>";

        } elseif ($request->delivery_status == 3) {
            $orderData['status'] = 5;
            $orderData['recalled_at'] = date("Y-m-d H:i:s");
            $orderData['recalled_by'] = 2;
            AcceptedOrder::where('order_id', $request->id)
                ->where('status', 2)
                ->update(['status' => 5]);
            $data['status'] = "<span class='badge badge-pill label-danger'>" . __('backend.recalled') . "</span>";
            $data['delivery_status'] = 3;

        } elseif ($request->delivery_status == 5) {
            $orderData['status'] = 8;
            $orderData['rejected_at'] = date("Y-m-d H:i:s");
            AcceptedOrder::where('order_id', $request->id)
                ->where('status', 2)
                ->update(['status' => 8]);
            $data['status'] = "<span class='badge badge-pill label-danger'>" . __('backend.rejected') . "</span>";
            $data['delivery_status'] = 3;

        }

        Order::where('id', $request->id)->update($orderData);

        $data['ids'] = $request->id;
        $data['result'] = true;
        $data['message'] = __('backend.changes_saved_successfully');

        return $data;
    }

    public function save_collected_cost(Request $request)
    {
        Order::where('id', $request->id)->update(['collected_cost' => $request->collected_cost]);
        $data['ids'] = $request->id;
        $data['result'] = true;
        $data['message'] = __('backend.changes_saved_successfully');
        return $data;
    }

    public function save_scan_order_rows(Request $request)
    {
        $orderIds = [];
        $orders_scan = $request->orders_scan;
        $final_data = [];
        $final_data['data'] = [];
        if (empty($orders_scan)) {
            $final_data['result'] = false;
            $final_data['message'] = __('backend.no_changes_detected');
            return $final_data;
        }

//        $ids = array_column($orders_scan, 'id');
//        $ids = array_map('intval', $ids);
//        $orders = Order::join('accepted_orders', 'orders.id', '=', 'accepted_orders.order_id')
//            ->whereIn('orders.id', $ids)
//            ->whereIn('orders.status', [2, 3, 5])
//            ->whereIn('accepted_orders.status', [2, 3, 5])
//            ->select('orders.*', 'driver_id')
//            ->get()
//            ->unique();
//
//        $drivers = $orders->groupBy('driver_id')->map(function ($drivers) {
//            return $drivers->count();
//        });
//
//        if (count($orders) != count($ids) || count($drivers) > 1) {
//            $final_data['result'] = false;
//            return $final_data;
//        }

        $scan_collection = ScanCollection::create([
            "driver_id" => $request->driver_id,
            "scan_type" => ($request->type == 'quick_entry' ? 3 : 1),
            "collection_number" => $this->randomNumber(2)
        ]);
        $final_data['scan_collection_id'] = $scan_collection->id;

        foreach ($orders_scan as $order) {
            $status = '';
            $orderObj = Order::where('id', $order['id'])->first();

            $orderData = null;
            $data = null;

            if (isset($order['collected_cost'])) {
                $orderData['collected_cost'] = $order['collected_cost'];
            }

            if (isset($order['delivery_status'])) {
                $orderData['delivery_status'] = $order['delivery_status'];
                if ($order['delivery_status'] == 1 || $order['delivery_status'] == 2 || $order['delivery_status'] == 4) {
//                    if ($order['delivery_status'] == 2 || $order['delivery_status'] == 4) {
//                        $orderData['warehouse_dropoff'] = 1;
//                        $orderData['warehouse_dropoff_date'] = date("Y-m-d H:i:s");;
//                    }
                    $orderData['status'] = 3;

                    switch ($order['delivery_status']) {
                        case 1:
                            $status = 'deliver';
                            break;
                        case 2:
                            $status = 'part_delivered';
                            break;
                        case 4:
                            $status = 'replace';
                            break;
                        default:
                            $status = "";
                    }

                    $orderIds[$status][] = $order['id'];
                    $orderData['delivered_at'] = date("Y-m-d H:i:s");
                    if (($orderObj->status == 7 && !$order['delivery_status']) || ($orderObj->status == 5 && !in_array($order['delivery_status'], [1, 2, 4])) || ($orderObj->status == 3 && $order['delivery_status'] != 3) || $orderObj->status == 2) {
                        AcceptedOrder::where('order_id', $order['id'])
                            ->where('status', 2)
                            ->update(['status' => 3, 'delivery_date' => date("Y-m-d H:i:s")]);
                    }

                    $data['status'] = "<span class='badge badge-pill label-success'>" . __('backend.delivered') . "</span>";

                } elseif ($order['delivery_status'] == 3) {
                    $orderData['status'] = 5;
//                    $orderData['warehouse_dropoff'] = 1;
//                    $orderData['warehouse_dropoff_date'] = date("Y-m-d H:i:s");
                    $orderIds['recall'][] = $order['id'];
                    $status = 'recall';
                    $orderData['recalled_at'] = date("Y-m-d H:i:s");
                    $orderData['recalled_by'] = 2;
                    if (($orderObj->status == 7 && !$order['delivery_status']) || ($orderObj->status == 5 && !in_array($order['delivery_status'], [1, 2, 4])) || ($orderObj->status == 3 && $order['delivery_status'] != 3) || $orderObj->status == 2) {
                        AcceptedOrder::where('order_id', $order['id'])
                            ->where('status', 2)
                            ->update(['status' => 5]);
                    }
                    $data['status'] = "<span class='badge badge-pill label-danger'>" . __('backend.recalled') . "</span>";
//                    $data['recall'] = 5;
                } elseif ($order['delivery_status'] == 5) {
                    $orderData['status'] = 8;
//                    $orderData['warehouse_dropoff'] = 1;
//                    $orderData['warehouse_dropoff_date'] = date("Y-m-d H:i:s");
                    $orderIds['reject'][] = $order['id'];
                    $status = 'reject';
                    $orderData['rejected_at'] = date("Y-m-d H:i:s");
                    if (($orderObj->status == 8 && !in_array($order['delivery_status'], [1, 2, 4])) || ($orderObj->status == 3 && $order['delivery_status'] != 3) || $orderObj->status == 2) {
                        AcceptedOrder::where('order_id', $order['id'])
                            ->where('status', 2)
                            ->update(['status' => 8]);
                    }
                    $data['status'] = "<span class='badge badge-pill label-danger'>" . __('backend.rejected') . "</span>";
//                    $data['recall'] = 5;
                }
            }

            ScanCollectionOrder::create([
                "order_id" => $order['id'],
                "collection_id" => $scan_collection->id
            ]);

            if ($orderObj->status == 7) {
                $orderData['collected_cost'] = 0;
            }

            if (isset($order['delivery_status']) && $order['delivery_status'] == 5 && !$order['collected_cost']) {
                $orderData['collected_cost'] = 0;
            }

            if (isset($order['delivery_status']) && $order['delivery_status'] == 1 && $order['collected_cost'] == "") {
                $orderData['collected_cost'] = $order['total_cost'];
            }

            if ((isset($orderData['collected_cost']) && $orderObj->collected_cost == $orderData['collected_cost']) && (isset($orderData['delivery_status']) && $orderObj->delivery_status == $orderData['delivery_status'])) {
                if (isset($orderIds[$status])) {
                    $logOrders = $orderIds[$status];
                    if (($key = array_search($orderObj->id, $logOrders)) !== false) {
                        unset($logOrders[$key]);
                    }
                    $orderIds[$status] = $logOrders;
                }
            }

            if (($orderObj->status == 7 && !isset($order['delivery_status'])) || ($orderObj->status == 5 && !in_array($order['delivery_status'], [1, 2, 4])) || ($orderObj->status == 8 && !in_array($order['delivery_status'], [1, 2, 4])) || ($orderObj->status == 3 && $order['delivery_status'] != 3) || $orderObj->status == 2) {
                Order::where('id', $order['id'])->update($orderData);
            } else {
                unset($data['status']);
            }

            $data['id'] = $order['id'];
            array_push($final_data['data'], $data);
        }

        foreach ($orderIds as $key => $ids) {
            if ($key == 'recall') {
                event(new OrderUpdated($ids, Auth::guard($this->guardType)->user()->id, 1, 'recall', ['recalled_by' => 2]));
            } elseif ($key == 'reject') {
                event(new OrderUpdated($ids, Auth::guard($this->guardType)->user()->id, 1, 'reject'));
            } elseif ($key == 'part_delivered') {
                event(new OrderUpdated($ids, Auth::guard($this->guardType)->user()->id, 1, 'part_delivered'));
            } elseif ($key == 'replace') {
                event(new OrderUpdated($ids, Auth::guard($this->guardType)->user()->id, 1, 'replace'));
            } else {
                event(new OrderUpdated($ids, Auth::guard($this->guardType)->user()->id, 1, 'deliver'));
            }
        }

        /*  Order::where('id', $request->id)->update(['collected_cost' => $request->collected_cost]);
          $data['ids'] = $request->id;
          $data['result'] = true;
          $data['message'] = __('backend.changes_saved_successfully');*/

        $final_data['result'] = true;
        $final_data['message'] = __('backend.changes_saved_successfully');

        return $final_data;
    }

    public function save_captain_exit_orders(Request $request, $ids = [], $driver_id = '')
    {
        $orderIds = count($ids) ? $ids : $request->orders_scan;
        $final_data = [];
        $final_data['data'] = [];
        if (empty($orderIds)) {
            $final_data['result'] = false;
            $final_data['message'] = __('backend.no_changes_detected');
            return $final_data;
        }

        $ordersCount = Order::whereIn('orders.id', $orderIds)
            ->whereIn('orders.status', [1, 2])
            ->count();

        if ($ordersCount != count($orderIds)) {
            $final_data['result'] = false;
            return $final_data;
        }

        $scan_collection = ScanCollection::create([
            "driver_id" => ($driver_id ? $driver_id : $request->driver_id),
            "scan_type" => 2,
            "collection_number" => $this->randomNumber(2)
        ]);
        $final_data['scan_collection_id'] = $scan_collection->id;

        foreach ($orderIds as $order) {

            ScanCollectionOrder::create([
                "order_id" => $order,
                "collection_id" => $scan_collection->id
            ]);

            array_push($final_data['data'], $order);
        }

        $final_data['result'] = true;
        $final_data['message'] = __('backend.changes_saved_successfully');

        return $final_data;
    }

    public function randomNumber($type = 1, $user_id = '')
    {
        return random_number($type, Auth::guard($this->guardType)->user()->id);
    }
}

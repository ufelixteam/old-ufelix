@extends('backend.layouts.app')
@section('css')

@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i>  Edit AgentInvoices #{{$agent_invoice->id}}</h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <form action="{{ route('mngrAdmin.agent_invoices.update', $agent_invoice->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group col-md-6 col-sm-6 @if($errors->has('from_date')) has-error @endif">
                       <label for="from_date-field">From_date</label>
                    <input type="text" id="from_date-field" name="from_date" class="form-control date-picker" value="{{ is_null(old("from_date")) ? $agent_invoice->from_date : old("from_date") }}"/>
                       @if($errors->has("from_date"))
                        <span class="help-block">{{ $errors->first("from_date") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('to_date')) has-error @endif">
                       <label for="to_date-field">To_date</label>
                    <input type="text" id="to_date-field" name="to_date" class="form-control date-picker" value="{{ is_null(old("to_date")) ? $agent_invoice->to_date : old("to_date") }}"/>
                       @if($errors->has("to_date"))
                        <span class="help-block">{{ $errors->first("to_date") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('profit')) has-error @endif">
                       <label for="profit-field">Profit</label>
                    <input type="text" id="profit-field" name="profit" class="form-control" value="{{ is_null(old("profit")) ? $agent_invoice->profit : old("profit") }}"/>
                       @if($errors->has("profit"))
                        <span class="help-block">{{ $errors->first("profit") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('total_income')) has-error @endif">
                       <label for="total_income-field">Total_income</label>
                    <input type="text" id="total_income-field" name="total_income" class="form-control" value="{{ is_null(old("total_income")) ? $agent_invoice->total_income : old("total_income") }}"/>
                       @if($errors->has("total_income"))
                        <span class="help-block">{{ $errors->first("total_income") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('status')) has-error @endif">
                       <label for="status-field">Status</label>
                    <input type="text" id="status-field" name="status" class="form-control" value="{{ is_null(old("status")) ? $agent_invoice->status : old("status") }}"/>
                       @if($errors->has("status"))
                        <span class="help-block">{{ $errors->first("status") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('agent_id')) has-error @endif">
                       <label for="agent_id-field">Agent_id</label>
                    <input type="text" id="agent_id-field" name="agent_id" class="form-control" value="{{ is_null(old("agent_id")) ? $agent_invoice->agent_id : old("agent_id") }}"/>
                       @if($errors->has("agent_id"))
                        <span class="help-block">{{ $errors->first("agent_id") }}</span>
                       @endif
                    </div>
                <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.agent_invoices.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  
@endsection

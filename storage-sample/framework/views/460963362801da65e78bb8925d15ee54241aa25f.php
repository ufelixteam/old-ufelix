	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<?php echo e(asset('/assets/vendor/jquery/jquery.min.js')); ?>"></script>
	<script src="<?php echo e(asset('/assets/vendor/bootstrap/js/bootstrap.min.js')); ?>"></script>
	<script src="<?php echo e(asset('/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js')); ?>"></script>
	<script src="<?php echo e(asset('/assets/scripts/jquery.validate.js')); ?>"></script>

	<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>


	<script src="<?php echo e(asset('/assets/scripts/bootstrap-datepicker.min.js')); ?>"></script>

	<script src="<?php echo e(asset('/assets/scripts/klorofil-common.js')); ?>"></script>

	<script type="text/javascript">
		$('#date').datepicker({
  format: 'yyyy-mm-dd',
    autoclose: true,
    language: 'en',
    todayHighlight: true
});


$('#datepicker').datepicker({
          format: 'yyyy-mm-dd',
            autoclose: true,
            language: 'en',
            todayHighlight: true,
            endDate: new Date()
        });
$('#datepicker1').datepicker({
	format: 'yyyy-mm-dd',
    autoclose: true,
    language: 'en',
    todayHighlight: true,

});
$('#datepickers').datepicker({
	format: 'yyyy-mm-dd',
    autoclose: true,
    language: 'en',
    todayHighlight: true,

});

$('.datepicker2').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd',
    language: 'en',
    todayHighlight: true,
    startDate: new Date()

});
$('#datepickerFilter').datepicker({
	format: 'yyyy-mm-dd',
	autoclose: true,
    language: 'en',
    todayHighlight: true
});
/*-*-*-*-*-*-*-*-* city and country *-*-*-*-*-*-*-*-*/
$(function () {
    $('#country_id-field').on('change', function () {
        $.ajax({
            url: "<?php echo e(url('/mngrAdmin/get_cities/')); ?>" + "/" + $(this).val(),
            type: 'get',

            success: function (data) {
                document.getElementById('city_id-field').innerHTML = "";
                $.each(data, function (i, content) {
                    document.getElementById('city_id-field').innerHTML += "<option value='" + content.id + "'>" + content.city + "</option>";
                });


            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

})
/*-*-*-*-*-*-*-*-* photo upload *-*-*-*-*-*-*-*-*/
$(document).ready(function () {
    $('.uploadPhoto').change(function (e) {
        // var node=this;
        var imageContent = $(this).attr('name');
        var files = e.target.files;
        $.each(files, function (i, file) {
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (e) {
            //console.log($.inArray(file['type'],['image/gif','image/jpeg','image/png'])!=-1);
            	if($.inArray(file['type'],['image/gif','image/jpeg','image/png'])!=-1){
                var template = '<div class="imgDiv"><img src="' + e.target.result + '" style="height:100px;width: 100px;"/><button id="cancel" class="btn btn-danger">&times;</button></div>';
                }else{
                var template = '<div class="imgDiv"><a href="' + e.target.result + '">' + file['name'] + '</a><button id="cancel" class="btn btn-danger">&times;</button></div>';
                }
                if (file['type'] == 'application/pdf') {
                    // if($(node).attr('type'))

                } else {

                }

                $('#' + imageContent + '_upload').html(template);
            }
        })
    })
});
$("div").delegate(".cancel", "click", function (e) {
    e.preventDefault();
    $(this).closest(".imgDiv").remove();
});
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
	</script><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/layouts/js.blade.php ENDPATH**/ ?>
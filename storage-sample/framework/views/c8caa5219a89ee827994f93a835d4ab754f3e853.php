<?php $__env->startSection('css'); ?>
<title><?php echo e(__('backend.activity_log')); ?></title>
<style type="text/css">
@media  only screen and (max-width: 580px) {
    .hide-td{
        display: none;
    }
}
.box-body {
     min-height: 100px;
}
.stylish-input-group .input-group-addon{
    background: white !important;
}
.stylish-input-group .form-control{
    border-right:0;
    box-shadow:0 0 0;
    border-color:#ccc;
}
.stylish-input-group button{
    border:0;
    background:transparent;
}

</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <div class="page-header clearfix">
        <h3 style="display: inline-block">
            <i class="glyphicon glyphicon-align-justify"></i> <?php echo e(__('backend.activity_log')); ?>

        </h3>

    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="row" style="margin-bottom:15px;">
  <div class="col-md-12">

    <div class="group-control col-md-6 col-sm-6">
      <div class='input-group date' id='datepickerFilter' >
        <input type="text" id="date" name="date" class="form-control" value="" autocomplete="off" placeholder="<?php echo e(__('backend.select_date_for_show_activity_log')); ?>"/>
        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
      </div>
    </div>

    <div class="group-control col-md-6 col-sm-6">
       <select id="activity_type" name="activity_type" class="form-control">
         <option value="" data-display="Select"><?php echo e(__('backend.select_activity_log_type')); ?></option>
          <?php if(! empty($ActivityLogType)): ?>
              <?php $__currentLoopData = $ActivityLogType; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <option value="<?php echo e($type->id); ?>" >
                    <?php echo e($type->name); ?>

                  </option>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          <?php endif; ?>
       </select>
     </div>

  </div>
</div>

<div class='list'>
  <?php echo $__env->make('backend.activity_log.table', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script>
  $("#date").on('change', function() {
    $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
    // var d = $("#date").val();
      $.ajax({
          url: '<?php echo e(URL::asset("/mngrAdmin/activity_log")); ?>'+'?date='+$("#date").val(),
          type: 'get',
          success: function(data) {
              $('.list').html(data.view);
          },
          error: function(data) {
              console.log('Error:', data);
          }
      });
  });

  $("#activity_type").on('change', function() {
    	$('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
    	$.ajax({
          url: '<?php echo e(URL::asset("/mngrAdmin/activity_log")); ?>'+'?filter='+$(this).val(),
          type: 'get',
          data: $("#activity_type").serialize(),
          success: function(data) {
              $('.list').html(data.view);
          },
          error: function(data) {
              console.log('Error:', data);
          }
     	});
  });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/activity_log/index.blade.php ENDPATH**/ ?>
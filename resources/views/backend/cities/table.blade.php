@if($cities->count())
  <table class="table table-condensed table-striped">
    <thead>
      <tr>
        <th>#</th>
        <th>  {{__('backend.city')}}</th>
        <th>  {{__('backend.government')}}</th>
        <th>  {{__('backend.status')}}</th>
        <th class="text-right"></th>
      </tr>
    </thead>
    <tbody>
      @foreach($cities as $city)
        <tr>
          <td>{{$city->id}}</td>
          <td>
            @if(session()->has('lang') == 'ar')
              {{$city->city_name}}
            @else
              {{$city->name_en}}
            @endif
          </td>
          <td>
            @if(! empty($city->Governorate))
              @if(session()->has('lang') == 'ar')
                {{$city->Governorate->name_ar}}
              @else
                {{$city->Governorate->name_en}}
              @endif
            @else

            @endif
          </td>
          <td>{!! $city->status_span !!}</td>
          <td class="text-right">
            {{--<a class="btn btn-xs btn-primary" href="{{ route('mngrAdmin.cities.show', $city->id) }}"><i class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>--}}
            @if(permission('editCitie')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
              <a class="btn btn-xs btn-warning" href="{{ route('mngrAdmin.cities.edit', $city->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
            @endif

            @if(permission('deleteCitie')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
              <form action="{{ route('mngrAdmin.cities.destroy', $city->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
                <input type="hidden" name="_method" value="DELETE">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}</button>
              </form>
            @endif
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>

  {!! $cities->appends($_GET)->links() !!}

@else
  <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
@endif

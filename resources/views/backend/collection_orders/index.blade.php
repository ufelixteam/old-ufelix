@extends('backend.layouts.app')
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <link href="{{asset('assets/select2/select2.min.css')}}" rel="stylesheet">
    <title>{{__('backend.Order_Collection')}}</title>
    <style>
        .select2-container {
            width: 100% !important;
            display: block;
        }
    </style>
@endsection
@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.Collection_Orders')}} {{__('backend.form')}}
            <a class="btn btn-success pull-right" href="{{ route('mngrAdmin.orders.create') }}"><i
                    class="glyphicon glyphicon-plus"></i> {{__('backend.add_Collection')}}</a>

            <!-- Button trigger modal -->
            <button type="button" class="btn btn-success import pull-right" style="margin-right: 30px;"
                    data-toggle="modal" data-target="#exampleModal">
                {{__('backend.import_orders')}}
            </button>

            <button type="button" class="btn btn-success import pull-right" style="margin-right: 30px;"
                    data-toggle="modal" data-target="#statusModal">
                {{__('backend.update_status')}}
            </button>
            <button type="button" class="btn btn-success import pull-right" style="margin-right: 30px;"
                    data-toggle="modal" data-target="#deliveryModal">
                {{__('backend.update_delivery_date')}}
            </button>
        </h3>
    </div>
@endsection

@section('content')

    <div class="alert alert-danger" hidden>
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong id="errorId"></strong>
    </div>
    @if (session('success'))
        <div class="alert alert-success alert-block text-center">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ session('success') }}</strong>
        </div>
    @endif
    @if (session('warning'))
        <div class="alert alert-warning alert-block text-center">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ session('warning') }}</strong>
        </div>
    @endif

    <!-- Modal For import Excel sheet-->
    <div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="statusModalModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">{{__('backend.import_orders')}}</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{url('mngrAdmin/reports/update_status')}}" method="post" name="validateUpdateForm"
                          enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="file" name="file" id="input-file-max-fs" class="dropify"
                                       data-max-file-size="2M"
                                       accept=".xls,.xlsx,.xlsm,.xlsb,.xml,application/ms-excel,application/vnd.ms-excel"/>
                            </div>
                            <div class="col-6"></div>
                            <div class="col-md-12">
                                <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal"
                                        style="margin-left: 15px;">Close
                                </button>
                                <button type="submit"
                                        class="btn btn-primary pull-right">{{__('backend.import_orders')}}</button>
                            </div>

                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>


    <!-- Modal For import Excel sheet-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">{{__('backend.import_orders')}}</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{url('mngrAdmin/collection_orders')}}" method="post" name="validateForm"
                          enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="form-group col-md-6 @if($errors->has('corporate_id')) has-error @endif">
                                <label for="corporate_id">{{__('backend.corporate')}}</label>
                                <select class="form-control select2" id="corporate_id" name="corporate_id">
                                    <option value="" data-display="Select">{{__('backend.corporate')}}</option>
                                    @if(! empty($corporates))
                                        @foreach($corporates as $corporate)
                                            <option value="{{$corporate->id}}">
                                                {{$corporate->name}}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has("corporate_id"))
                                    <span class="help-block">{{ $errors->first("corporate_id") }}</span>
                                @endif
                            </div>
                            <div class="form-group col-md-6 @if($errors->has('customer_id')) has-error @endif">
                                <label for="customer_id">{{__('backend.customer')}}</label>
                                <select class="form-control select2 disabled" id="customer_id" name="customer_id">
                                    <option value="" data-dispaly="Select">{{__('backend.customer')}}</option>
                                    @if(! empty($customers))
                                        @foreach($customers as $customer)
                                            <option value="{{$customer->id}}">{{$customer->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has("customer_id"))
                                    <span class="help-block">{{ $errors->first("customer_id") }}</span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <input type="file" name="sheet" id="input-file-max-fs" class="dropify"
                                       data-max-file-size="2M"
                                       accept=".xls,.xlsx,.xlsm,.xlsb,.xml,application/ms-excel,application/vnd.ms-excel"/>
                            </div>
                            <div class="col-6"></div>
                            <div class="col-md-12">
                                <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal"
                                        style="margin-left: 15px;">Close
                                </button>
                                <button type="submit"
                                        class="btn btn-primary pull-right">{{__('backend.import_orders')}}</button>
                            </div>

                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>


    <!-- Modal For import Excel sheet-->
    <div class="modal fade" id="processModal" tabindex="-1" role="dialog" aria-labelledby="processModalModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">{{__('backend.import_orders')}}</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{url('mngrAdmin/reports/upload_sheet')}}" method="post" name="validateDeliveryForm"
                          enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="collection_id" value="" id="collection_id">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="file" name="file" id="input-file-max-fs" class="dropify"
                                       data-max-file-size="2M"
                                       accept=".xls,.xlsx,.xlsm,.xlsb,.xml,application/ms-excel,application/vnd.ms-excel"/>
                            </div>
                            <div class="col-6"></div>
                            <div class="col-md-12">
                                <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal"
                                        style="margin-left: 15px;">Close
                                </button>
                                <button type="submit"
                                        class="btn btn-primary pull-right">{{__('backend.import_orders')}}</button>
                            </div>

                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- Modal For import Excel sheet-->
    <div class="modal fade" id="deliveryModal" tabindex="-1" role="dialog" aria-labelledby="deliveryModalModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">{{__('backend.import_orders')}}</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{url('mngrAdmin/reports/update_delivery_date')}}" method="post"
                          name="validateStatusForm"
                          enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="file" name="file" id="input-file-max-fs" class="dropify"
                                       data-max-file-size="2M"
                                       accept=".xls,.xlsx,.xlsm,.xlsb,.xml,application/ms-excel,application/vnd.ms-excel"/>
                            </div>
                            <div class="col-6"></div>
                            <div class="col-md-12">
                                <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal"
                                        style="margin-left: 15px;">Close
                                </button>
                                <button type="submit"
                                        class="btn btn-primary pull-right">{{__('backend.import_orders')}}</button>
                            </div>

                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>

    <div class="row" style="margin-bottom:15px;">
        <div class="col-md-12">
            <div class="col-md-3 col-sm-3">
                <div id="imaginary_container">
                    <div class="input-group stylish-input-group">
                        <input type="text" class="form-control" id="search-field" name="search"
                               placeholder="{{__('backend.search')}} ">
                        <span class="input-group-addon">
                      <button type="button" id="search-btn1">
                        <span class="glyphicon glyphicon-search"></span>
                      </button>
                    </span>
                    </div>
                </div>
            </div>
            <div class="group-control col-md-3 col-sm-3">
                <select id="saved" name="saved" class="form-control">
                    <option value="-1"> {{__('backend.Filter_By_Completed')}}</option>
                    <option value="1">{{__('backend.completed')}}</option>
                    <option value="0">{{__('backend.under_processing')}}</option>
                </select>
            </div>
            <div class="group-control col-md-3 col-sm-3">
                <select id="collection_type" name="collection_type" class="form-control">
                    <option value="-1"> {{__('backend.collection_type')}}</option>
                    <option value="1">{{__('backend.sheet')}}</option>
                    <option value="2">{{__('backend.dashboard')}}</option>
                    <option value="3">{{__('backend.mobile')}}</option>
                    <option value="4">{{__('backend.profile')}}</option>
                    <option value="5">{{__('backend.integration')}}</option>
                </select>
            </div>
        </div>
    </div>

    <div class="list">
        @include('backend.collection_orders.table')
    </div>

    @include('backend.pickups.create_pickup_popup')
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script src="{{asset('assets/select2/select2.min.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            $('.select2').select2();

            $('#exampleModal').on('show.bs.modal', function (e) {
                $(this).removeAttr('tabindex');
            })

            $('#statusModal').on('show.bs.modal', function (e) {
                $(this).removeAttr('tabindex');
            })
        });

        $(function () {

            $("form[name='validateForm']").validate({
                // Specify validation rules
                rules: {
                    corporate_id: {
                        required: true,
                    },
                    customer_id: {
                        required: true,
                    },
                    file: {
                        required: true,
                    }
                },
                // Specify validation error messages
                messages: {
                    corporate_id: {
                        required: "{{__('backend.Please_Choose_Corporate')}}",
                    },
                    customer_id: {
                        required: "{{__('backend.Please_Choose_Customer')}}",
                    },
                    file: {
                        required: "{{__('backend.Please_Enter_File')}}",
                    }
                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                },
                highlight: function (element) {
                    $(element).parent().addClass('error')
                },
                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                }
            });

            $("form[name='validateStatusForm']").validate({
                // Specify validation rules
                rules: {
                    file: {
                        required: true,
                    }
                },
                // Specify validation error messages
                messages: {
                    file: {
                        required: "{{__('backend.Please_Enter_File')}}",
                    }
                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                },
                highlight: function (element) {
                    $(element).parent().addClass('error')
                },
                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                }
            });
        })
        $("form[name='validateDeliveryForm']").validate({
            // Specify validation rules
            rules: {
                file: {
                    required: true,
                }
            },
            // Specify validation error messages
            messages: {
                file: {
                    required: "{{__('backend.Please_Enter_File')}}",
                }
            },

            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error)
            },
            highlight: function (element) {
                $(element).parent().addClass('error')
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('error')
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function (form) {
                form.submit();
            }
        });

        $("form[name='validateUpdateForm']").validate({
            // Specify validation rules
            rules: {
                file: {
                    required: true,
                }
            },
            // Specify validation error messages
            messages: {
                file: {
                    required: "{{__('backend.Please_Enter_File')}}",
                }
            },

            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error)
            },
            highlight: function (element) {
                $(element).parent().addClass('error')
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('error')
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function (form) {
                form.submit();
            }
        });

        $('#myModal').on('shown.bs.modal', function () {
            $('#myInput').trigger('focus')
        })
        $('.importOrders').on('click', function () {
            $("#collection_id").val($(this).attr('data-id'));
        });
        $('#corporate_id').on('change', function () {
            $.ajax({
                url: "{{ url('/mngrAdmin/get_corporate_customers/')}}" + "/" + $(this).val(),
                type: 'get',
                success: function (data) {
                    console.log('dataaaa', data);
                    $('#customer_id').children('option:not(:first)').remove();
                    $('#customer_id').removeClass('disabled');

                    $.each(data, function (i, content) {
                        $('#customer_id').append($("<option></option>").attr("value", content.id).text(content.name));
                    });

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });
        $(function () {
            $(".packup").on('click', function () {


                let collection_id = $(this).attr('data-value');
                let corporate_id = $(this).attr('data-corporate');
                let customer_id = $(this).attr('data-customer');

                $.confirm({
                    title: '',
                    theme: 'modern',
                    class: 'warning',
                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-simle-o  text-warning"></i></span><span class="jconfirm-title">Confirm Pick Up</span></div><div style="font-weight:bold;"><p>Are You sure To Pick Up This Orders ?</p></div>',
                    buttons: {
                        confirm: {
                            text: 'Pickup Order',
                            btnClass: 'btn-warning',
                            action: function () {
                                $.ajax({
                                    url: "{{ url('/mngrAdmin/pickup_orders/')}}",
                                    type: 'POST',
                                    data: {
                                        '_token': "{{csrf_token()}}",
                                        'collection_id': collection_id,
                                        'pickup_type': 'collection'
                                    },
                                    success: function (data) {
                                        if (data['status'] != 'false') {

                                            $(".alert").hide();
                                            $('#orderpickup').val(data['orders']);
                                            $('#from-pick').val(data['frompick']);
                                            $('#delivery_price-field').val(data['cost']);
                                            $('#bonous-field').val(data['bonous']);
                                            $('#pickupModal').modal('show');
                                            $('#pickup_corporate_id').val(corporate_id);
                                            $('#collection_id').val(collection_id);
                                            $('#pickup_customer_id').val(customer_id);

                                        } else {
                                            $('#errorId').html(data['message']);
                                            $(".alert").show();

                                            $('#pickupModal').modal('hide');

                                        }
                                        window.scrollTo({top: 0, behavior: 'smooth'});
                                    },
                                    error: function (data) {
                                        console.log('Error:', data);
                                    }
                                });
                            }
                        },
                        cancel: {
                            text: 'Cancel',
                            action: function () {
                            }
                        }
                    }
                });
            })

        });

        $("#collection_type").on('change', function () {
            $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
            $.ajax({
                url: '{{URL::asset("/mngrAdmin/collection_orders?type=1")}}' + '&saved=' + $("#saved").val() + '&search=' + $("#search-field").val() + '&collection_type=' + $("#collection_type").val(),
                type: 'get',
                success: function (data) {
                    $('.list').html(data.view);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $("#saved").on('change', function () {
            $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
            $.ajax({
                url: '{{URL::asset("/mngrAdmin/collection_orders?type=1")}}' + '&saved=' + $("#saved").val() + '&search=' + $("#search-field").val() + '&collection_type=' + $("#collection_type").val(),
                type: 'get',
                success: function (data) {
                    $('.list').html(data.view);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $("#search-btn1").on('click', function () {
            $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
            $.ajax({
                url: '{{URL::asset("/mngrAdmin/collection_orders?type=1")}}' + '&saved=' + $("#saved").val() + '&search=' + $("#search-field").val() + '&collection_type=' + $("#collection_type").val(),
                type: 'get',
                success: function (data) {
                    $('.list').html(data.view);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $('#downloadStatus').on('click', function () {
            var _token = $('#_token');
            var id = $('#id');
            var status = $('#status');
            var file_name = $('#file_name');
            $.ajax({
                type: post,
                url: 'mngrAdmin\\collection_orders\\downloadStatus',
                data: {
                    _token: _token.val(),
                    id: id.val(),
                    status: status.val(),
                    file_name: file_name.val(),
                },
                success: function (Response) {
                    $('#downloadStatus').removeClass('btn-primary').addClass('btn-warning');
                },
                error: function () {
                    alert("{{__('backend.Something_went_Wrong_please_try_again_later')}}");
                }
            });
        });
    </script>
@endsection

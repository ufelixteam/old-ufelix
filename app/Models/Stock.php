<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{

    public function getAvailableQuantityAttribute()
    {
        $qty = 0;

        if ($this->qty) {
            $qty = $this->qty - $this->used_qty;
        }

        return $qty;
    }

    public function store()
    {
        return $this->belongsTo('App\Models\Store', 'store_id', 'id');
    }

    public function corporate()
    {
        return $this->belongsTo('App\Models\Corporate', 'corporate_id', 'id');
    }

}

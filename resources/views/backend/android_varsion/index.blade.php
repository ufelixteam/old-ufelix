@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.Android_Versions')}}</title>
@endsection

@section('header')
  <div class="page-header clearfix">
    <h3>
      <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.Android_Versions')}}
      @if(permission('addAndroidVersion')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
        <a class="btn btn-success pull-right" href="{{ url('mngrAdmin/android/create') }}"><i class="glyphicon glyphicon-plus"></i> {{__('backend.create')}} </a>
      @endif
    </h3>
  </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
      @if($versions->count())
        <table class="table table-condensed table-striped">
          <thead>
              <tr>
                <th>#</th>
                <th>{{__('backend.version')}}</th>
                <th>{{__('backend.status')}}</th>
                <th>{{__('backend.platform')}}</th>
                <th>{{__('backend.created_at')}}</th>
                <th>{{__('backend.updated_at')}}</th>
                <th class="text-right"></th>
              </tr>
          </thead>
          <tbody>
            @foreach($versions as $version)
              <tr>
                <td>{{$version->id}}</td>
                <td>{{$version->version}}</td>
                <td>
                  @if($version->status == 1)
                    <span class="badge badge-pill label-success">{{__('backend.update')}}</span>
                  @else
                    <span class="badge badge-pill label-danger">{{__('backend.not_update')}}</span>
                  @endif
                </td>
                <td>
                  @if($version->platform == 1)
                    <span class="badge badge-pill label-success">{{__('backend.Android_Client')}}</span>
                  @elseif ($version->platform == 2)
                    <span class="badge badge-pill label-danger">{{__('backend.Android_Driver')}}</span>
                  @elseif ($version->platform == 3)
                    <span class="badge badge-pill label-danger">{{__('backend.IOS_Client')}}</span>
                  @else
                    <span class="badge badge-pill label-danger">{{__('backend.web')}}</span>
                  @endif
                </td>
                <td>{{$version->created_at}}</td>
                <td>{{$version->updated_at}}</td>
                <td class="text-right">
                  @if(permission('editAndroidVersion')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                    <a class="btn btn-xs btn-warning" href="{{ url('mngrAdmin/android/edit', $version->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                  @endif

                  @if(permission('deleteAndroidVersion')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                    <form action="{{ url('mngrAdmin/android/del', $version->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}</button>
                    </form>
                  @endif
                </td>
              </tr>
            @endforeach
        </tbody>
      </table>

      {!! $versions->appends($_GET)->links() !!}

      @else
        <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
      @endif
    </div>
  </div>
@endsection

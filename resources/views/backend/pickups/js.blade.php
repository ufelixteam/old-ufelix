<script>
    $(document).ready(function () {
        var drivers_bonus = {};
        var drivers_fees = {};
        var s_state_id = '';

        /*get_customers*/
        $("#corporate_id").on('change', function () {
            $.ajax({
                url: "{{ url('/mngrAdmin/corporates/order_details/')}}" + "/" + $(this).val(),
                type: 'get',
                data: {},
                success: function (data) {
                    $('#sender_name').html('');
                    $('#sender_mobile-field').val('');
                    $('#s_government_id').val('');
                    $('#s_state_id').val('');
                    $('#sender_address').val('');
                    s_state_id = '';

                    $.each(data.customers, function (i, content) {
                        $('#sender_name').append($("<option></option>").attr("value", content.id).text(content.name));
                    });
                    if (data.customers.length > 0) {
                        $('#sender_name').val(data.customers[0].name);
                        $('#sender_mobile-field').val(data.customers[0].mobile);
                        s_state_id = data.customers[0].city_id;
                        $('#s_government_id').val(data.customers[0].government_id).change();
                        $('#sender_address').val(data.customers[0].address);
                        $('#sender_latitude').val(data.customers[0].latitude);
                        $('#sender_longitude').val(data.customers[0].longitude);
                    }
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });


        $("#agent_id-field").on('change', function () {
            $('#driver_id').html('<option value="" >Choose Drivers</option>');
            if ($(this).val()) {
                $.ajax({
                    url: "{{ url('/mngrAdmin/getdriver_by_agents')}}" + "/" + $(this).val(),
                    type: 'get',
                    data: {},
                    success: function (data) {

                        $.each(data, function (i, content) {
                            $('#driver_id').append($("<option></option>").attr("value", content.id).text(content.name));
                            drivers_bonus[content.id] = content.bouns_of_pickup_for_one_order ? content.bouns_of_pickup_for_one_order : 0;
                            drivers_fees[content.id] = content.pickup_price ? content.pickup_price : 0;
                        });
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        });


        $("#r_government_id").on('change', function () {
            $.ajax({
                url: "{{ url('/mngrAdmin/get_cities')}}" + "/" + $(this).val(),
                type: 'get',
                data: {},
                success: function (data) {
                    $('#r_state_id').html('<option value="" >Sender City</option>');
                    $.each(data, function (i, content) {
                        $('#r_state_id').append($("<option></option>").attr("value", content.id).text(content.city_name));
                    });
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

            {{--/* get price list*/--}}
            {{--$.ajax({--}}
            {{--    url: "{{ url('/mngrAdmin/get_cost_pickup')}}" + "/" + $("#from-pick").val() + "/" + $(this).val(),--}}
            {{--    type: 'get',--}}
            {{--    data: {},--}}
            {{--    success: function (data) {--}}
            {{--        if (data != "0") {--}}
            {{--            $('#delivery_price-field').val(data.cost);--}}
            {{--            $('#bonous-field').val(data.bonous);--}}

            {{--        }--}}

            {{--    },--}}
            {{--    error: function (data) {--}}
            {{--        console.log('Error:', data);--}}
            {{--    }--}}
            {{--});--}}

        });

        $("#s_government_id").on('change', function () {
            $.ajax({
                url: "{{ url('/mngrAdmin/get_cities')}}" + "/" + $(this).val(),
                type: 'get',
                data: {},
                success: function (data) {
                    $('#s_state_id').html('<option value="" >Sender City</option>');
                    $.each(data, function (i, content) {
                        $('#s_state_id').append($("<option></option>").attr("value", content.id).text(content.city_name));
                    });

                    if (s_state_id) {
                        $('#s_state_id').val(s_state_id);
                    }
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

            {{--/* get price list*/--}}
            {{--$.ajax({--}}
            {{--    url: "{{ url('/mngrAdmin/get_cost_pickup')}}" + "/" + $("#from-pick").val() + "/" + $(this).val(),--}}
            {{--    type: 'get',--}}
            {{--    data: {},--}}
            {{--    success: function (data) {--}}
            {{--        if (data != "0") {--}}
            {{--            $('#delivery_price-field').val(data.cost);--}}
            {{--            $('#bonous-field').val(data.bonous);--}}

            {{--        }--}}

            {{--    },--}}
            {{--    error: function (data) {--}}
            {{--        console.log('Error:', data);--}}
            {{--    }--}}
            {{--});--}}

        });

        $("#store_id").on('change', function () {
            $.ajax({
                url: "{{ url('/mngrAdmin/get_store')}}" + "/" + $(this).val(),
                type: 'get',
                data: {},
                success: function (data) {
                    $('#reciever_name-field').val(data.name);
                    $('#receiver_mobile-field').val(data.mobile);
                    $('#receiver_latitude').val(data.latitude);
                    $('#receiver_longitude').val(data.longitude);
                    $('#receiver_address').val(data.address);
                    $('#pick_r_government_id').val(data.governorate_id).change();
                    console.log(data.governorate_id)
                    $("div.pick_r_government_id > select > option[value=" + data.governorate_id + "]").attr("selected", true);

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

        });

        $("#driver_id").on('change', function (e) {
            const bonus = $(this).val() ? drivers_bonus[$(this).val()] : 0;
            const fees = $(this).val() ? drivers_fees[$(this).val()] : 0;
            $("#bonous-field").val(bonus);
            $("#delivery_price-field").val(fees);
        });
    });

</script>

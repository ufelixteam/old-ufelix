@extends('backend.layouts.app')
@section('css')
<title>{{__('backend.orders')}} - {{__('backend.individual_client')}}</title>
<style type="text/css">
@media only screen and (max-width: 580px) {
    .hide-td{
        display: none;
    }
}
.box-body {
     min-height: 100px;
}
.stylish-input-group .input-group-addon{
    background: white !important;
}
.stylish-input-group .form-control{
    border-right:0;
    box-shadow:0 0 0;
    border-color:#ccc;
}
.stylish-input-group button{
    border:0;
    background:transparent;
}

</style>
@endsection

@section('header')
    <div class="page-header clearfix">
        <h3 style="display: inline-block">
            <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.orders')}} - {{__('backend.individual_client')}}
        </h3>
    </div>
@endsection
@section('content')
<div class="row" style="margin-bottom:15px;">
  <div class="col-md-12">

    <div class="group-control col-md-6 col-sm-6">
      <div class='input-group date' id='datepickerFilter' >
        <input type="text" id="date" name="date" class="form-control" value="" autocomplete="off" placeholder="{{__('backend.select_date_for_show_individual_orders')}}"/>
        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
      </div>
    </div>

  </div>
</div>

<div class='list'>
  @include('backend.invoices.table_orders_individuals')
</div>

@endsection

@section('scripts')
<script>
  $("#date").on('change', function() {
    $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
    // var d = $("#date").val();
      $.ajax({
          url: '{{URL::asset("/mngrAdmin/orders/showOrders/individuals")}}'+'?date='+$("#date").val(),
          type: 'get',
          success: function(data) {
              $('.list').html(data.view);
          },
          error: function(data) {
              console.log('Error:', data);
          }
      });
  });
</script>
@endsection

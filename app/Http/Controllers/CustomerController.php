<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller\Backend;
use App\Mail\contectUs;
use App\Mail\message;
use App\Mail\ResetPassword;
use App\Mail\Verified;
use App\Mail\WelcomeCustomer;
use App\Models\City;
use App\Models\CollectionOrder;
use App\Models\Country;
use App\Models\Customer;
use App\Models\Governorate;
use App\Models\Order;
use App\Models\Store;
use App\Models\Verification;
use Auth;
use File;
use Flash;
use Hash;
use Illuminate\Http\Request;
use Response;

//use DB;

class CustomerController extends Controller
{

    public function __construct(Request $request)
    {
        $this->middleware('setlocale');
    }

    // Get All Customer
    public function index(Request $request)
    {
        $user = Auth::guard('Customer')->user();
        $customers = Customer::orderBy('customers.id', 'DESC');
        if ($user->is_manager) {
            $customers = $customers->where('corporate_id', $user->corporate_id);
        } else {
            $customers = $customers->where('id', $user->id);
        }

        #check if active or block filter or search by name
        $block = app('request')->input('block');    // Search By Block
        $active = app('request')->input('active');   // Search By Active
        $search = app('request')->input('search');   // Search
        $customer_filter = app('request')->input('customer'); // Search By Type

        if (isset($search) && $search != "") {
            $customers = $customers->where('name', 'like', '%' . $search . '%')->orWhere('mobile', 'like', '%' . $search . '%');
        }

        if (isset($active) && $active != -1) {
            $customers = $customers->where('is_active', $active);
        }

        if (isset($block) && $block != -1) {
            $customers = $customers->where('is_block', $block);
        }

        if (isset($customer_filter) && $customer_filter == 2) {
            $customers = $customers->where('corporate_id', '!=', '');
        }

        if (isset($customer_filter) && $customer_filter == 1) {
            $customers = $customers->where('corporate_id', null);
        }

        $customers = $customers->paginate(15);

        return view('frontend.profile.customers.index', compact('customers'));
    }

    // Added New Customer Page
    public function create()
    {
        $countries = Country::orderBy('name', 'ASC')->get();
        $governments = Governorate::orderBy('name_en', 'ASC')->get();
        $stores = Store::where('status', 1)->select('stores.*')->get();

        return view('frontend.profile.customers.create', compact('countries', 'governments', 'stores'));
    }

    // Add New Customer
    public function store(Request $request)
    {
        $request->validate([

            'name' => 'required',
            'email' => 'required|unique:customers',
            'mobile' => 'required|unique:customers',

            'password' => 'required',
//            'country_id' => 'required',
            'government_id' => 'required',
            'city_id' => 'required',

            'image' => 'mimes:jpeg,png,jpg,gif,bitmap',
            'latitude' => 'numeric',
            'longitude' => 'numeric',
            // 'role_id'     => 'required',
            // 'is_active'   => 'required',
        ]);

        $user = Auth::guard('Customer')->user();

        $customer = new Customer();
        $customer->name = $request->input("name");
        $customer->email = $request->input("email");
        $customer->mobile = $request->input("mobile");
        $customer->phone = $request->input("phone");
        $customer->password = $request->input("password");
//        $customer->country_id = $request->input("country_id");
        $customer->government_id = $request->input("government_id");
        $customer->city_id = $request->input("city_id");
        $customer->address = $request->input("address");
        $customer->national_id = $request->input("national_id");
        $customer->latitude = $request->input("latitude");
        $customer->longitude = $request->input("longitude");
        $customer->job = $request->input("job");
        $customer->is_block = 0; // is_block  = 0 :: not block
        $customer->role_id = 4; // role_id   = 3 :: Individual Customer
        $customer->corporate_id = $user->corporate_id;
        $customer->store_id = $request->input("store_id");
        $customer->is_manager = $request->input("is_manager");

        // $customer->role_id     = $request->input("role_id");

        $customer->latitude = $request->input("latitude");
        $customer->longitude = $request->input("longitude");

//        if ($request->input("role_id") == "5") {
//            $customer->corporate_id = $request->input("corporate_id");
//        } else {
//            $customer->corporate_id = null;
//        }

        if ($request->hasFile('image')) {
            $imgExt = $request->file('image')->getClientOriginalExtension();
            $image = 'customer-' . time() . "." . $imgExt;
            $request->file("image")->move(public_path('api_uploades/client/profile/'), $image);
        } else {
            $image = 'default.png';
        }

        $customer->image = $image;


        $customer->save();

        Flash::success(__('backend.Customer_saved_successfully'));

        return redirect()->route('profile.customers.index', app()->getLocale() == 'en' ? 'en' : 'ar');
    }

    // View One Customer
    public function show($id)
    {
        $customer = Customer::findOrFail($id);
        $prices = $customer->prices()
            ->join('governorates', 'governorates.id', '=', 'customer_prices.access_station')
            ->join('areas', 'areas.id', '=', 'governorates.area_id')
            ->orderBy('areas.order', 'asc')
            ->select('customer_prices.*', 'areas.order', 'areas.name_en')
            ->get()
            ->groupBy('name_en');

//        $orders = Order::where('customer_id', $id)->paginate();  // Get All Order For Customer
        return view('frontend.profile.customers.show', compact('customer', 'prices'));
    }

    // Edit Customer
    public function customer_edit_without_lang($id, Request $request)
    {

        $customer = Customer::findOrFail($id);
        $countries = Country::orderBy('name', 'ASC')->get();
        $governments = Governorate::orderBy('name_en', 'ASC')->get();
        $cities = City::where('governorate_id', $customer->government_id)->orderBy('id', 'ASC')->get();
        $stores = Store::where('status', 1)->select('stores.*')->get();

        return view('frontend.profile.customers.edit', compact('customer', 'countries', 'governments', 'cities', 'stores'));
    }

    // Edit Customer
    public function edit($lang='', $id, Request $request)
    {

        $customer = Customer::findOrFail($id);
        $countries = Country::orderBy('name', 'ASC')->get();
        $governments = Governorate::orderBy('name_en', 'ASC')->get();
        $cities = City::where('governorate_id', $customer->government_id)->orderBy('id', 'ASC')->get();
        $stores = Store::where('status', 1)->select('stores.*')->get();

        return view('frontend.profile.customers.edit', compact('customer', 'countries', 'governments', 'cities', 'stores'));
    }

    // Update Customer
    public function update(Request $request, $lang ='', $id)
    {

        $customer = Customer::where('id', $id)->first();
        if ($customer != null) {
            $request->validate([
                'email' => 'required|unique:customers,email,' . $id,
                'mobile' => 'required|unique:customers,mobile,' . $id,

//                'country_id' => 'required',
                'government_id' => 'required',
                'city_id' => 'required',
                'name' => 'required',
                'image' => 'mimes:jpeg,png,jpg,gif,bitmap ',
                'latitude' => 'numeric',
                'longitude' => 'numeric',

            ]);

            $customer->name = $request->input("name");
            $customer->email = $request->input("email");
            $customer->mobile = $request->input("mobile");
            $customer->phone = $request->input("phone");
//            $customer->country_id = $request->input("country_id");
            $customer->government_id = $request->input("government_id");
            $customer->city_id = $request->input("city_id");
            $customer->address = $request->input("address");
            $customer->national_id = $request->input("national_id");
            $customer->job = $request->input("job");
            $customer->address = $request->input("address");
            $customer->store_id = $request->input("store_id");
            $customer->latitude = $request->input("latitude");
            $customer->longitude = $request->input("longitude");
            $customer->is_manager = $request->input("is_manager");

            if (!$request->image_old) {
                $customer->image = '';
            }

            if ($request->input("password") != "") { // If Change Password
                $customer->password = $request->input("password");
            }

            if ($request->file("image") != "") {
                $extension = File::extension($request->file("image")->getClientOriginalName());
                $image = 'customer-' . time() . "." . "jpg";
                $customer->image = $image;
                $request->file("image")->move(public_path('api_uploades/client/profile/'), $image);
            }

            $customer->save();

            Flash::warning(__('backend.Customer_Updated_successfully'));
        } else {
            Flash::warning(__('backend.Customer_does_not_exist'));
        }
        // return redirect()->route('mngrAdmin.customers.index');
        return redirect()->back();

    }

    // Delete Customer
    public function destroy($id)
    {
        $customer = Customer::findOrFail($id);
        $CollectionOrder = CollectionOrder::where('customer_id', $id)->first();
        $orders = Order::where('customer_id', $id)->first();

        if ($CollectionOrder == null && $orders == null) {  // Delete This Customer If Not Have Any Orders
            $customer->delete($id);
            // Function Add Action In Activity Log By Samir
            Flash::warning(__('backend.Customer_deleted_successfully'));
            return redirect()->route('profile.customers.index', app()->getLocale() == 'en' ? 'en' : 'ar');
        } else { // Not Delete This Customer If Have Any Orders
            Flash::warning(__('backend.Customer_not_deleted_because_orders_not_empty_and_collectionorders_not_empty'));
            return redirect()->route('profile.customers.index', app()->getLocale() == 'en' ? 'en' : 'ar');
        }
    }

    // get customer details for order creation
    public function details($id, Request $request)
    {
        return Customer::find($id);
    }
}

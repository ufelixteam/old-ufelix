<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use App\Mail\contectUs;
use App\Mail\message;
use App\Mail\ResetPassword;
use App\Mail\Verified;
use App\Mail\WelcomeCustomer;
use App\Models\Agent;
use App\Models\City;
use App\Models\CollectionOrder;
use App\Models\Corporate;
use App\Models\Country;
use App\Models\Customer;
use App\Models\CustomerDevice;
use App\Models\CustomerTaskDay;
use App\Models\Driver;
use App\Models\Governorate;
use App\Models\Invoice;
use App\Models\Order;
use App\Models\OrderType;
use App\Models\PaymentMethod;
use App\Models\Setting;
use App\Models\Stock;
use App\Models\Store;
use App\Models\TempOrder;
use App\Models\User;
use App\Models\Verification;
use App\Models\Wallet;
use Auth;
use File;
use Flash;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Response;

//use DB;

class CustomerController extends Controller
{
    private $guardType;

    public function __construct(Request $request)
    {
        if ($request->is('mngrAdmin/*') || $request->is('admin/*')) {
            \Config::set('auth.defaults.guard', 'admin');

        } elseif ($request->is('mngrAgent/*') || $request->is('agent/*')) {
            \Config::set('auth.defaults.guard', 'agent');
        }
        $this->guardType = \Config::get('auth.defaults.guard');
    }

    // get customers by corp_id
    public function get_customers($id)
    {
        return Customer::where('corporate_id', $id)->get();
    }

    // website home cunter
    public function home()
    {
        $agent = Setting::where('key', 'no_agents')->value('value');
        $Corporate = Setting::where('key', 'no_corporates')->value('value');
        $Customer = Setting::where('key', 'no_customers')->value('value');
        $Driver = Setting::where('key', 'no_drivers')->value('value');
        $VehicleType = Setting::where('key', 'no_vehicles')->value('value');
        $order = Setting::where('key', 'no_orders')->value('value');
        return View('website.home', compact('agent', 'Corporate', 'Customer', 'Driver', 'VehicleType', 'order'));
    }

    // Website functions
    public function getupdate()
    {
        $costomer = auth()->guard('Customer')->user()->id;
        $select = DB::table('collection_orders')->
        where('type', null)->
        where('customer_id', $costomer)->
        orderBy('id', 'asc')->
        get();
        return View('profile.upload', compact('select'));
    }

    //uploadSheet
    public function upload(Request $request)
    {
        $v = \Validator::make($request->all(), [  // Fr3on
            'file' => 'mimes:xls,xlsx',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        $costomer = $request->customer_id;
        if ($request->file("file") != "") {
            $allowedext = array("xls", "xlsx");
            $extension = File::extension($request->file("file")->getClientOriginalName());
            if (in_array($extension, $allowedext)) {
                $file = "sheet" . "_" . "." . $extension;
                $fileupload = CollectionOrder::insert([
                    'file_name' => $file,
                    'customer_id' => $costomer
                ]);
                $select = CollectionOrder::where('customer_id', $costomer)->latest()->first();
                $fileuploadd = "sheet" . "_" . $select->id . "_" . "customer" . "_" . $costomer . "." . $extension;
                $request->file('file')->move(public_path('api_uploades/client/corporate/sheets/'), $fileuploadd);
                $reupdate = CollectionOrder::where('id', $select->id)->update(['file_name' => $fileuploadd]);
            } else {
                echo 'allowedext extension ';
            }
        } else {
            echo 'error';
        }
        return Redirect()->back();
    }

    // Send Verfiy Code For Customer
    public function send_verify_code(Request $request)
    {
        $concPhone = '002' . $request->phoneNo;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://api.unifonic.com/rest/Verify/GetCode");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "AppSid=Fit0ax1rTrBppUQzhaUOPJFUYl4fF&Recipient=" . $concPhone . "&Body=Ufelix Verify Code : ");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded"));
        $resp = curl_exec($ch);
        curl_close($ch);
        $resp_json = json_decode($resp, true);
        if ($resp_json['success'] == "true") {
            $statusCode = 200;
            $message = "Done";
        } else {
            $statusCode = 410;
            $message = "Error";
        }
        return response()->json([
            'message' => $message,
            'statusCode' => $statusCode
        ]);
    }

    // Verfiy Phone For Customer
    public function verify_phone(Request $request)
    {
        $concPhone = '002' . $request->phoneNo;
        if (!isset($request->phoneNo) || !isset($request->VerifyCode)) {
            $statusCode = 418;
            $message = "Invalid Format !";
        } else {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "http://api.unifonic.com/rest/Verify/VerifyNumber");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "AppSid=Fit0ax1rTrBppUQzhaUOPJFUYl4fF&Recipient=" . $concPhone . "&PassCode=" . $request->VerifyCode);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Content-Type: application/x-www-form-urlencoded"
            ));
            $resp = curl_exec($ch);
            curl_close($ch);
            $resp_json = json_decode($resp, true);
            if ($resp_json['success'] == "true") {
                $statusCode = 200;
                $message = "Done";
            } else {
                $statusCode = 410;
                $message = "Error";
            }
        }
        return response()->json($message, $statusCode);
    }

    /*** Not Used
     * public function wlogin()
     * {
     * return view('website.wlogin');
     * }
     ***/

    // Verfiy Email For Customer
    public function verify_email_link(Request $request)
    {
        $customer = Customer::where('email', $request->username)->first();
        if ($customer != null) {
            $verfytoken = str_random(100, 900);
            // insert table corporates
            $valate = new Verification();
            $valate->object_id = $customer->id;
            $valate->type_id = 2;
            $valate->verification_token = $verfytoken;
            $valate->verification_type = 0;
            $valate->save();
            $customerData = array(
                'email' => $request->username,
                'token' => $valate->verification_token,
                'id' => $customer->id,
                'name' => $customer->name,
            );
            // mailler function
            Mail::to($customerData['email'])->send(new Verified($customerData));

            // email email function
            return redirect('/profile/index')->with('status', __('backend.we_sent_you_a_verification_message_please_check_your_email_and_verify_it'));
        } else {
            return redirect('/profile/index')->with('error', __('backend.Something_Error'));
        }
    }


    public function login(Request $request)
    {
        // Login Customer
        $v = \Validator::make($request->all(), [
            'username' => 'required|email',
            'password' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        // select customer where email
        $customer = Customer::where('email', $request->username)->first();
        if ($customer != null) { // if  != null customer fetch

            //Verification table select  by object id
            $valate = Verification::where('object_id', $customer->id)->
            where('verification_type', 0)->
            where('status', 1)->
            where('type_id', 2)->
            where('verified', 1)->
            first();
            if ($valate == null || $customer->verified != 1) {
                $verfytoken = str_random(100, 900);
                Verification::where('object_id', $customer->id)->
                where('type_id', 2)->
                where('verification_type', 0)->
                delete();
                $valate = new Verification();
                $valate->object_id = $customer->id;
                $valate->type_id = 2;
                $valate->verification_token = $verfytoken;
                $valate->verification_type = 0;
                $valate->verified = 0;
                $valate->status = 0;
                $valate->save();
                $customer = array(
                    'email' => $customer->email,
                    'token' => $valate->verification_token,
                    'id' => $customer->id,
                    'name' => $customer->name,
                );
                // mailler function
                Mail::to($customer['email'])->send(new Verified($customer));

                /*	Mail::send('emails.verified', $customer ,function ($message) use ($customer){
                        $message->to($customer['email']);
                        $message->from('support@ufelix.com');
                        $message->subject('Verify account ufelix');
                    }); */

                // email email function
                return back()->withInput($request->all)->with('status', __('backend.we_sent_you_a_verification_message_please_check_your_email_and_verify_it'));
            } else {
                //CustomerDevice table select by customer id
                $login = CustomerDevice::join('customers', 'customers.id', '=', 'customer_devices.customer_id')->
                first();
                where('customers.email', $request->username)->
                // end mac address cleint
                ob_start();
                system('getmac');
                $Content = ob_get_contents();
                ob_clean();
                $mac = substr($Content, strpos($Content, '\\') - 20, 17);
                // end mac address cleint
                if ($login != null && $mac === $login->device_id) { // check login value if != null get date of CustomerDevice table && get mac cleint == CustomerDevice->derver_id
                    //update status in CustomerDevice
                    CustomerDevice::where('customer_id', $login->id)->update(['status' => 1]);
                    if (is_numeric($request->username)) { // is_numeric (username) === phone number
                        // start login guard customer function if value input phone number
                        if (Auth::guard('Customer')->attempt([
                            'mobile' => $request->username,
                            'password' => $request->password])
                        ) {
                            return redirect('profile/index');
                        } else {
                            return back()->withInput($request->all)->with('error', __('backend.Wrong_Mobile_or_Password'));
                        }
                        // end login guard customer function if value input phone number

                    } else {
                        // start login guard customer function if value input email  next check in customer driver
                        if (Auth::guard('Customer')->attempt([
                            'email' => $request->username,
                            'password' => $request->password])
                        ) {
                            return redirect('profile/index');
                        } else {
                            return back()->withInput($request->all)->with('error', __('backend.Wrong_Email_or_Password'));
                        }
                        // start login guard customer function if value input email  next check in customer driver
                    }
                } else {
                    // condation check phone number or email
                    if (is_numeric($request->username)) {
                        $selectuser = Customer::where('mobile', $request->username)->first();
                    } else {
                        $selectuser = Customer::where('email', $request->username)->first();
                    }
                    // end condation check phone number or email
                    if ($selectuser != null) { // check email or phone number in $selectuser &&checked not coloum in costomerdivice

                        // create new CustomerDevice coloum
                        $online = CustomerDevice::insert([
                            'customer_id' => $selectuser->id,
                            'version_id' => 1,
                            'device_id' => $mac,
                            'status' => 0
                        ]);
                        // create new CustomerDevice coloum
                        if (is_numeric($request->username)) { // check phone number
                            if (Auth::guard('Customer')->attempt([
                                'mobile' => $request->username,
                                'password' => $request->password])
                            ) {
                                return redirect('profile/index');
                            } else {
                                return redirect()->back()->withInput($request->all)->with('error', __('backend.Wrong_Email_or_Password'));
                            }

                        } else {  // check email
                            if (Auth::guard('Customer')->attempt([
                                'email' => $request->username,
                                'password' => $request->password])
                            ) {
                                return redirect('profile/index');
                            } else {
                                return redirect()->back()->withInput($request->all)->with('error', __('backend.Wrong_Email_or_Password'));
                            }
                        }
                    } else { // if not value
                        return redirect()->back()->withInput($request->all)->with('error', __('backend.Email_or_password_does_not_exists'));
                    }
                }
            }
        } else { // if  != null customer fetch end
            return back()->withInput($request->all)->with('error', __('backend.Email_or_password_does_not_exists'));
        }
    }

    // verify email by token
    public function verifyEmailById($id)
    {
        $valate = Verification::where('verification_token', $id)->
        where('verification_type', 0)->
        first();
        if ($valate != null) {
            if ($valate->verified == 0 && $valate->status == 0) {
                $update = Verification::where(['verification_token' => $id, 'verification_type' => 0])->update(['status' => 1, 'verified' => 1]);
                if ($valate->type_id == 1) {
                    $update_costomer = Driver::where(['verification_token' => $id])->update(['is_active' => 1]);
                    return View('activetion_email', compact('name'));
                } elseif ($valate->type_id == 2) {
                    $update_costomer = Customer::where('id', $valate->object_id)->update(['verified' => 1]);
                    $name = Customer::where('id', $valate->object_id)->first();
                    return View('activetion_email', compact('name'));
                } elseif ($valate->type_id == 3) {
                    $update_costomer = User::where('id', $id)->update(['is_active' => 1]);
                    $name = User::where('id', $id)->first();
                    return View('activetion_email', compact('name'));
                } elseif ($valate->type_id == 4) {
                    $update_costomer = User::where('id', $id)->update(['is_active' => 1]);
                    $name = User::where('id', $id)->first();
                    return View('activetion_email', compact('name'));
                }
            } else {
                return redirect('/profile/index');
            }
        } else {
            return redirect('/404');
        }
    }

    // Send Email Customer For Reset Password
    public function send_email_reset_pass(Request $request)
    {
        // validate email
        $this->validate($request, [
            'email' => 'required|email',
        ]);

        /*
    		 * list Customer by type_id >> [ type_id == 1 :: Driver && type_id == 2 :: Customer && type_id == 3 :: Agent && type_id == 4 :: Admin ]
         *
         * list Customer by verification_type >> [ verification_type == 0 :: verify && verification_type == 1 :: reset password ]
    		*/

        $selectuser = Customer::where('email', $request->email)->first();
        if (!empty($selectuser)) {
            $type_id = 2;
            $verfytoken = str_random(100, 900);
            // insert table corporates
            $Verification = Verification::insert([
                'object_id' => $selectuser->id,
                'type_id' => $type_id,
                'verification_token' => $verfytoken,
                'verification_type' => 1
            ]);
            $token = array(
                'token' => $verfytoken,
            );
            $name = Customer::where('id', $selectuser->id)->first();
            $customer = array(
                'email' => $request->email,
                'token' => $verfytoken,
                'name' => $name->name
            );
            //send email
            Mail::to($customer['email'])->send(new ResetPassword($customer));
            return back()->with('status', __('backend.Send_a_message_to_change_your_password'));
        } else {
            return back()->with('error', __('backend.Wrong_Email'));
        }
    }

    // Reset Password For Customer Only
    public function newpassword(Request $request, $token)
    {
        $valate = Verification::where('verification_token', $token)->
        where('status', 0)->
        where('type_id', 2)->
        where('verification_type', 1)->first();
        //var_dump($valate);die();
        if ($valate != null && $valate->status != 1) {
            if (Auth::guard('Customer')->user()) {
                return redirect('/profile/index/')->with('error', __('backend.You_are_already_Logged'));
            }
            $customer = Customer::where('id', $valate->object_id)->first();
            return View('website.newPassword', compact('customer', 'token'));

        } else {
            return redirect('/home/ResetPassword')->with('error', __('backend.Validation_Link_is_wrong_Resent_E-mail_again'));
        }
    }

    // Add a New Password
    public function addnewpassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'min:6|confirmed',
            'password_confirmation' => 'required_with:password|same:password|min:6'
        ]);
        $valate = Verification::where('verification_token', $request->validate_token)->
        where('type_id', 2)->
        first();

        if ($valate != null && $valate->status != 1) {
            if (Auth::guard('Customer')->user()) {
                return redirect('/profile/index/')->with('error', __('backend.You_are_already_Logged'));
            }

            $customer = Customer::where('id', $valate->object_id)->
            where('email', trim($request->username))->
            first();

            if ($customer == null) {
                return redirect()->back()->withErrors(['errors' => __('backend.Email_does_not_exist')]);
            }

            $customer = Customer::where('id', $valate->object_id)->update([
                'password' => bcrypt($request->password)
            ]);
            $valate->status = 1;
            $valate->save();
            return redirect('/login')->withErrors(['status' => __('backend.Succeeded_your_password_has_changed_Try_login_Now')]);
        } else {
            return redirect('/home/ResetPassword')->withErrors(['errors' => __('backend.Reset_Passworde_is_invalid_Something_is_wrong_Try_Again')]);
        }
    }

    // Profile Customer
    public function profile(Request $request)
    {
        /*
         * list Invoice by type >> [ type == 1 :: Driver && type == 2 :: Corporate && type == 3 :: Agent ]
        */
        if (Auth::guard('Customer')->check()) {
            $idcustome = auth()->guard('Customer')->user()->corporate_id;
            $corprates = Corporate::where('id', $idcustome)->get()->First();

            if ($corprates != null) { // customer indavd
                $invoices = Invoice::orderBy('id', 'desc')->where(['type' => 2, 'object_id' => $corprates->id])->count();
            }

            $user_id = auth()->guard('Customer')->user()->id;
            $wallet = Wallet::where('object_id', $idcustome)->where('type', 2)->first();
            $customer = Customer::where('id', $user_id)->first();
            $corprates = Corporate::where('id', $customer->corporate_id)->first();
            // $order = Order::where(['customer_id' => $user_id ,'status' => 0 ])

            $order = Order::select('orders.*', 'accepted_orders.order_id', 'accepted_orders.delivery_price', 'accepted_orders.driver_id')->
            join('accepted_orders', 'orders.id', '=', 'accepted_orders.order_id')->
            where('orders.customer_id', $user_id)->
            where('orders.status', 0)->
            count();
            return View('profile.index', compact('corprates', 'customer', 'wallet', 'invoices', 'order'));
        } else {
            return redirect('/404');
        }
    }

    /* Not Used
    public function invaiceview($id)
    {
        $invaiceviewid = InvoiceOrder::where('invoice_id', $id)->get();
        foreach ($invaiceviewid as $viewid) {
            $item[] = $viewid->id;
        }
        $views = AcceptedOrder::whereIn('id', $item)->paginate(50);
        return View('profile.veiw', compact('views'));
    }
    */

    // View Order For Customer
    public function orderview($type, $id)
    {
        $order = order::where('id', $id)->first();
        return View('profile.orderveiw', compact('order'));
    }

    // Logout For Customer
    public function logout()
    {
        auth()->guard('Customer')->logout();
        return redirect('/');
    }

    //validation functions, used by both website and backend
    function check_customer_email()
    {
        $email = \Request::get("email");
        $customer = Customer::where('email', $email)->first();
        if ($customer) {
            echo 'false';
        } else {
            echo 'true';
        }
    }

    // Check Mobile Customer
    function check_customer_mobile()
    {
        $mobile = \Request::get("mobile");
        $customer = Customer::where('mobile', $mobile)->first();
        if ($customer) {
            echo 'false';
        } else {
            echo 'true';
        }
    }

    ////////////////////////////////////////////////// Backend functions //////////////////////////////////////////////

    // Get All Customer
    public function index(Request $request)
    {
        $user = Auth::guard($this->guardType)->user();
        if ($user->is_warehouse_user) {
            $customers = Customer::join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->select('customers.*')
                ->where('user_id', $user->id)
                ->orderBy('customers.id', 'desc')->where(function ($query) {
                    $query->whereNotNull('corporate_id')
                        ->where('corporate_id', '!=', '');
                });/*->where('role_id', 3)*/
        } else {
            $customers = Customer::orderBy('customers.id', 'desc')->where(function ($query) {
                $query->whereNotNull('corporate_id')
                    ->where('corporate_id', '!=', '');
            });/*->where('role_id', 3)*/
        }

        #check if active or block filter or search by name
        $block = app('request')->input('block');    // Search By Block
        $active = app('request')->input('active');   // Search By Active
        $search = app('request')->input('search');   // Search
        $customer_filter = app('request')->input('customer'); // Search By Type

        if (isset($search) && $search != "") {
            $customers = $customers->where('name', 'like', '%' . $search . '%')
                ->orWhere('mobile', 'like', '%' . $search . '%');
        }

        if (isset($active) && $active != -1) {
            $customers = $customers->where('is_active', $active);
        }

        if (isset($block) && $block != -1) {
            $customers = $customers->where('is_block', $block);
        }

        if (isset($customer_filter) && $customer_filter == 2) {
            $customers = $customers->where('corporate_id', '!=', '');
        }

        if (isset($customer_filter) && $customer_filter == 1) {
            $customers = $customers->where('corporate_id', null);
        }

        $customers = $customers->paginate(15);

        if ($user->role_id == '1') {
            if ($request->ajax()) {
                return [
                    'view' => view('backend.customers.table', compact('customers'))->render(),
                ];
            }
            return view('backend.customers.index', compact('customers'));
        } else {
            return view('backend.customers.index', compact('customers'));
        }
    }

    // Added New Customer Page
    public function create()
    {
        $user = Auth::guard($this->guardType)->user();

        $corporates = Corporate::select('corporates.*')->where('is_active', 1);

        if ($user->is_warehouse_user) {
            $corporates = $corporates->join('customers', 'customers.corporate_id', '=', 'corporates.id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id);
        }

        $corporates = $corporates->get();

        $countries = Country::orderBy('name', 'ASC')->get();
        $governments = Governorate::orderBy('name_en', 'ASC')->get();
        $stores = Store::where('status', 1)->select('stores.*');

        if ($user->is_warehouse_user) {
            $stores = $stores->join('store_user', 'store_user.store_id', '=', 'stores.id')
                ->where('user_id', $user->id);
        }

        $stores = $stores->get();;

        return view('backend.customers.create', compact('corporates', 'countries', 'governments', 'stores'));
    }

    // Add New Customer
    public function store(Request $request)
    {
        $request->validate([

            'name' => 'required',
            'email' => 'required|unique:customers',
            'mobile' => 'required|unique:customers',

            'password' => 'required',
//            'country_id' => 'required',
            'government_id' => 'required',
            'city_id' => 'required',

            'image' => 'mimes:jpeg,png,jpg,gif,bitmap',
            'latitude' => 'numeric',
            'longitude' => 'numeric',
            'corporate_id' => 'required'
            // 'role_id'     => 'required',
            // 'is_active'   => 'required',
        ]);

        $customer = new Customer();
        $customer->name = $request->input("name");
        $customer->email = $request->input("email");
        $customer->mobile = $request->input("mobile");
        $customer->phone = $request->input("phone");
        $customer->password = $request->input("password");
//        $customer->country_id = $request->input("country_id");
        $customer->government_id = $request->input("government_id");
        $customer->city_id = $request->input("city_id");
        $customer->address = $request->input("address");
        $customer->national_id = $request->input("national_id");
        $customer->latitude = $request->input("latitude");
        $customer->longitude = $request->input("longitude");
        $customer->job = $request->input("job");
        $customer->is_block = 0; // is_block  = 0 :: not block
        $customer->role_id = 4; // role_id   = 3 :: Individual Customer
        $customer->corporate_id = $request->input("corporate_id");
        $customer->store_id = $request->input("store_id");
        $customer->is_manager = $request->input("is_manager");

        // $customer->role_id     = $request->input("role_id");

        $customer->latitude = $request->input("latitude");
        $customer->longitude = $request->input("longitude");

//        if ($request->input("role_id") == "5") {
//            $customer->corporate_id = $request->input("corporate_id");
//        } else {
//            $customer->corporate_id = null;
//        }

        if ($request->hasFile('image')) {
            $imgExt = $request->file('image')->getClientOriginalExtension();
            $image = 'customer-' . time() . "." . $imgExt;
            $request->file("image")->move(public_path('api_uploades/client/profile/'), $image);
        } else {
            $image = 'default.png';
        }

        $customer->image = $image;


        $customer->save();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Add New Customer ' . $customer->name, ' اضافة عميل جديد ' . $customer->name, 1); // Helper Function in App\Helpers.php

        Flash::success(__('backend.Customer_saved_successfully'));

        return redirect()->route('mngrAdmin.customers.index');
    }

    // View One Customer
    public function show($id)
    {
        $customer = Customer::findOrFail($id);
        $prices = $customer->prices()
            ->join('governorates', 'governorates.id', '=', 'customer_prices.access_station')
            ->join('areas', 'areas.id', '=', 'governorates.area_id')
            ->orderBy('areas.order', 'asc')
            ->select('customer_prices.*', 'areas.order', 'areas.name_en')
            ->get()
            ->groupBy('name_en');

//        $orders = Order::where('customer_id', $id)->paginate();  // Get All Order For Customer
        return view('backend.customers.show', compact('customer', 'prices'));
    }

    // Edit Customer
    public function edit($id)
    {
        $user = Auth::guard($this->guardType)->user();

        $corporates = Corporate::select('corporates.*')->where('is_active', 1);

        if ($user->is_warehouse_user) {
            $corporates = $corporates->join('customers', 'customers.corporate_id', '=', 'corporates.id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id);
        }

        $corporates = $corporates->get();
        $customer = Customer::findOrFail($id);
        $countries = Country::orderBy('name', 'ASC')->get();
        $governments = Governorate::orderBy('name_en', 'ASC')->get();
        $cities = City::orderBy('id', 'ASC')->get();
        $stores = Store::where('status', 1)->select('stores.*');

        if ($user->is_warehouse_user) {
            $stores = $stores->join('store_user', 'store_user.store_id', '=', 'stores.id')
                ->where('user_id', $user->id);
        }

        $stores = $stores->get();

        return view('backend.customers.edit', compact('customer', 'corporates', 'countries', 'governments', 'cities', 'stores'));
    }

    // Blocked And Not Blocked Customer
    function block($id)
    {
        /*
        * is_block = 0 :: Not Blocked
        * is_block = 1 :: Blocked
        */
        $customer = Customer::findOrFail($id);
        if ($customer->is_block == 0) {
            $customer->is_block = 1;
            // Function Add Action In Activity Log By Samir
            addActivityLog('Blocked Customer ' . $customer->name, ' حظر العميل ' . $customer->name, 1); // Helper Function in App\Helpers.php
        } else {
            $customer->is_block = 0;
            // Function Add Action In Activity Log By Samir
            addActivityLog('Unblocked Customer ' . $customer->name, ' الغاء حظر العميل ' . $customer->name, 1); // Helper Function in App\Helpers.php
        }
        $customer->save();
        return $customer->is_block;
    }

    // Activeed And Not Activeed Customer
    function status($id)
    {
        $customer = Customer::findOrFail($id);
        if ($customer->is_active == 0) {
            $customer->is_active = 1; // Customer Activeed
            // Function Add Action In Activity Log By Samir
            addActivityLog('Activeed Customer ' . $customer->name, ' تفعيل العميل ' . $customer->name, 1); // Helper Function in App\Helpers.php
        } else {
            $customer->is_active = 0; // Customer Not Activeed
            // Function Add Action In Activity Log By Samir
            addActivityLog('Unactiveed Customer ' . $customer->name, ' الغاء تفعيل العميل ' . $customer->name, 1); // Helper Function in App\Helpers.php
        }
        $customer->save();
        return $customer->is_active;
    }

    // Update Customer
    public function update(Request $request, $id)
    {

        $customer = Customer::where('id', $id)->first();
        if ($customer != null) {
            $request->validate([
                'email' => 'required|unique:customers,email,' . $id,
                'mobile' => 'required|unique:customers,mobile,' . $id,

//                'country_id' => 'required',
                'government_id' => 'required',
                'city_id' => 'required',
                'name' => 'required',
                'image' => 'mimes:jpeg,png,jpg,gif,bitmap ',
                'latitude' => 'numeric',
                'longitude' => 'numeric',

            ]);

            $customer->name = $request->input("name");
            $customer->email = $request->input("email");
            $customer->mobile = $request->input("mobile");
            $customer->phone = $request->input("phone");
//            $customer->country_id = $request->input("country_id");
            $customer->government_id = $request->input("government_id");
            $customer->city_id = $request->input("city_id");
            $customer->address = $request->input("address");
            $customer->national_id = $request->input("national_id");
            $customer->job = $request->input("job");
            $customer->address = $request->input("address");
            $customer->store_id = $request->input("store_id");
            $customer->latitude = $request->input("latitude");
            $customer->longitude = $request->input("longitude");
            $customer->is_manager = $request->input("is_manager");

            if (!$request->image_old) {
                $customer->image = '';
            }

            if ($request->input("password") != "") { // If Change Password
                $customer->password = $request->input("password");
            }

            if ($request->file("image") != "") {
                $extension = File::extension($request->file("image")->getClientOriginalName());
                $image = 'customer-' . time() . "." . "jpg";
                $customer->image = $image;
                $request->file("image")->move(public_path('api_uploades/client/profile/'), $image);
            }

            $customer->save();

            $days = is_array($request->input("days")) ? $request->input("days") : [];
            $customer->task_days()->delete();
            if (count($days)) {
                foreach ($days as $day) {
                    CustomerTaskDay::create(['customer_id' => $id, 'collect_day' => $day]);
                }
            }

            // Function Add Action In Activity Log By Samir
            addActivityLog('Edit Customer ' . $customer->name, ' تعديل العميل ' . $customer->name, 1); // Helper Function in App\Helpers.php
            Flash::warning(__('backend.Customer_Updated_successfully'));
        } else {
            Flash::warning(__('backend.Customer_does_not_exist'));
        }
        // return redirect()->route('mngrAdmin.customers.index');
        return redirect()->back();

    }

    // Delete Customer
    public function destroy($id)
    {
        $customer = Customer::findOrFail($id);
        $CollectionOrder = CollectionOrder::where('customer_id', $id)->first();
        $orders = Order::where('customer_id', $id)->first();
        $name = $customer->name;

        if ($CollectionOrder == null && $orders == null) {  // Delete This Customer If Not Have Any Orders
            $customer->delete($id);
            // Function Add Action In Activity Log By Samir
            addActivityLog('Delete Customer ' . $name, ' حذف العميل ' . $name, 1); // Helper Function in App\Helpers.php
            Flash::warning(__('backend.Customer_deleted_successfully'));
            return redirect()->route('mngrAdmin.customers.index');
        } else { // Not Delete This Customer If Have Any Orders
            Flash::warning(__('backend.Customer_not_deleted_because_orders_not_empty_and_collectionorders_not_empty'));
            return redirect()->route('mngrAdmin.customers.index');
        }
    }

    // Send Verify Code New With Infobip
    public function verify_mobile_code_new($pinId, $code)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://464jm.api.infobip.com/2fa/1/pin/" . $pinId . "/verify",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\n\t\"pin\":\" " . $code . "\"\n}",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }
    }


    public function send_verify_code_new($mobile)
    {
        $username = 'S.Essam';
        $password = 'Ufelixapp@#2018';
        $header = base64_encode($username . ":" . $password);

        /*format mobile*/
        if (!(substr($mobile, 0, 2) === '002' || substr($mobile, 0, 2) === '+2')) {
            $mobile = "002" . $mobile;
        }
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://464jm.api.infobip.com/2fa/1/pin?ncNeeded=true",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 20,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\n    \"applicationId\": \"BA4B62CF75D2662F447BC3C45B715664\",\n    \"messageId\": \"A04D1139BA2146594588B4632EBF809A\",\n    \"from\": \"Ufelix\",\n    \"to\": \"" . $mobile . "\"\n}",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Basic " . $header,
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }
    }


    /*Added By Eng samar*/
    public function send_verify_code_mobile(Request $request)
    {   //Fr3on
        $v = \Validator::make($request->all(), [
            'phoneNo' => 'required|exists:customers,mobile|numeric',
        ]);
        if ($v->fails()) {
            return response()->json($v->errors());
        }
        $concPhone = '002' . $request->phoneNo;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://api.unifonic.com/rest/Verify/GetCode");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "AppSid=Fit0ax1rTrBppUQzhaUOPJFUYl4fF&Recipient=" . $concPhone . "&Body=Ufelix Verify Code : ");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded"));
        $resp = curl_exec($ch);
        curl_close($ch);
        $resp_json = json_decode($resp, true);
        if ($resp_json['success'] == "true") {
            return 'done';
            //return redirect('/profile/index')->with('status' , 'We sent code to your Mobile successfully');
        } else {
            return 'error';
            //return redirect('/profile/index')->with('error' , 'Something Error');
        }
    }

    // Verify Mobile Customer
    public function verify_mobile_customer(Request $request)
    {
        $concPhone = '002' . $request->phoneNo;
        if (!isset($request->phoneNo) || !isset($request->VerifyCode)) {
            $statusCode = 418;
            $message = __('backend.Invalid_Format');
        } else {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "http://api.unifonic.com/rest/Verify/VerifyNumber");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "AppSid=Fit0ax1rTrBppUQzhaUOPJFUYl4fF&Recipient=" . $concPhone . "&PassCode=" . $request->VerifyCode);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Content-Type: application/x-www-form-urlencoded"
            ));
            $resp = curl_exec($ch);
            curl_close($ch);
            $resp_json = json_decode($resp, true);
            if ($resp_json['success'] == "true") {
                $customer = Customer::where('mobile', $request->phoneNo)->update(['is_verify_mobile' => 1]);
                return 'done';
                //return redirect('/profile/index')->with('status' , 'Mobile has verified successfully');
            } else {
                return 'error';
                //return redirect('/profile/index')->with('error' , 'Something Error');
            }
        }
    }


    public function order_form_list()
    {
        $user = Auth::guard('Customer')->user();
        if ($user) {
            $collections = CollectionOrder::where('type', 1)->
            orderBy('created_at', 'DESC')->
            where('customer_id', $user->id)->
            get();
            return view('profile.collections.index', compact('collections'));
        }
    }


    public function order_form(Request $request)
    {
        $collection_id = app('request')->input('collection');
        $orders = TempOrder::where('collection_id', $collection_id)->paginate(50);
        $governments = Governorate::orderBy('name_en', 'ASC')->get();
        $types = OrderType::orderBy('name_en', 'ASC')->get();
        $payments = PaymentMethod::orderBy('name', 'ASC')->get();
        $collection = null;
        $stores = [];
        $customer = Auth::guard('Customer')->user();
        if (!empty($customer)) {
            $stores = Stock::whereRaw('qty - used_qty > 0')->where('corporate_id', $customer->corporate_id)->get();
            $collection = CollectionOrder::where('id', $collection_id)->first();
            if (!empty($collection) && $collection != null) {
                $collection->corporate_id = Corporate::where('id', $customer->corporate_id)->value('id');
            }
            if ($collection && $collection->is_saved == 1) {
                flash(__('backend.Collection_that_you_try_to_open_saved_before'))->error();
                return redirect('/form_collections');
            }
        }
        if ($request->ajax()) {
            return [
                'view' => view('profile.collections.create_form', compact('governments', 'types', 'collection_id', 'orders', 'collection', 'customer', 'stores', 'payments'))->render(),
            ];
        }
        return view('profile.create_collection_form', compact('governments', 'types', 'collection_id', 'orders', 'collection', 'customer', 'stores', 'payments'));
    }

    // Create Temp Order
    public function tmporders_create(Request $request)
    {
        $first_one = "0";
        $customer = Auth::guard('Customer')->user();
        if (!$customer) {
            //return to login
            return response()->json(['errors' => [__('backend.please_try_to_login')]]);
        }
        if ($request->ajax()) {
            $validator = \Validator::make($request->all(), [
                'receiver_name' => 'string|max:255',
                'receiver_mobile' => 'required',
                'payment_method_id' => 'required',
            ]);

        } else {

            $this->validate($request, [
                'receiver_name' => 'string|max:255',
                'receiver_mobile' => 'required',
                'payment_method_id' => 'required',

            ]);
        }

        if ($request->ajax()) {
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->all()]);
            }
        }

        DB::beginTransaction();
        try {
            $collection = CollectionOrder::where('id', $request->collection_id)->first();
            if (!$collection) {
                $collection = new CollectionOrder();
                $collection->customer_id = $customer->id;
                $collection->count_no = 0;
                $collection->is_saved = 0;
                $collection->type = 1;
                $collection->save();
                $first_one = "1";
            } else {
                $collection->save();
            }
            $collection_id = $collection->id;
            if (!empty($request->order_id) && $request->order_id > 0) {
                $order = TempOrder::where('id', $request->order_id)->first();
                if (!$order) {
                    $order = new TempOrder();
                }
            } else {
                $order = new TempOrder();
            }

            $order->receiver_name = $request->input("receiver_name");
            $order->receiver_mobile = $request->input("receiver_mobile");
            $order->receiver_phone = $request->input("receiver_phone");
            $order->receiver_latitude = $request->input("receiver_latitude");
            $order->receiver_longitude = $request->input("receiver_longitude");
            $order->receiver_address = $request->input("receiver_address");
            $order->collection_id = $collection->id;
            $order->sender_name = $customer->name;
            $order->sender_mobile = $customer->mobile;
            $order->sender_phone = $customer->phone;
            $order->sender_latitude = $request->input("sender_latitude");
            $order->sender_longitude = $request->input("sender_longitude");
            $order->sender_address = $request->input("sender_address");
            $order->r_state_id = $request->input("r_state_id");
            $order->s_state_id = $request->input("s_state_id");
            $order->r_government_id = $request->input("r_government_id");
            $order->s_government_id = $request->input("s_government_id");
            $order->status = 0;
            $order->type = $request->input("type");
            $order->order_type_id = $request->input("order_type_id");
            $order->agent_id = null;
            $order->customer_id = $collection->customer_id;
            $order->payment_method_id = $request->input("payment_method_id");
            $order->order_price = $request->input("order_price");
            //calculate from to
            $order->delivery_price = $request->input("delivery_price");
            $order->overload = 0;
            $order->notes = $request->input("notes");
            $order->store_id = $request->input("store_id");
            $order->qty = $request->input("qty");
            $order->forward_driver_id = null;
            $order->order_number = 'RC' + time();
            $order->receiver_code = 'DC' + time();
            $order->save();
            DB::commit();
            //Flash::success('Order saved successfully.');
            if ($request->ajax()) {
                if ($validator->fails()) {
                    return response()->json(['errors' => $validator->errors()->all()]);
                }
                $orders = TempOrder::where('collection_id', $collection_id)->paginate(50);
                $governments = Governorate::orderBy('name_en', 'ASC')->get();
                #payment_methods
                $payments = PaymentMethod::orderBy('name', 'ASC')->get();
                $types = OrderType::orderBy('name_en', 'ASC')->get();
                if ($customer) {
                    $stores = Stock::whereRaw('qty - used_qty > 0')->where('corporate_id', $customer->corporate_id)->get();
                    $collection->corporate_id = $customer->corporate_id;
                }
                return [
                    'collection_id' => $collection_id,
                    'first_one' => $first_one,
                    'view' => view('profile.collections.tmptable', compact('orders', 'collection_id', 'collection'))->render(),
                    'view_order' => view('profile.collections.create_form', compact('governments', 'types', 'collection_id', 'orders', 'collection', 'customer', 'stores', 'payments'))->render(),
                    'js' => view('profile.collections.js')->render(),
                ];
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['errors' => [__('backend.please_try_again_something_error')]]);
        }
    }


    #This used to reset form
    public function tmporders_reset_view($id, Request $request)
    {
        $collection_id = $id;
        $user = Auth::guard($this->guardType)->user();
        $governments = Governorate::orderBy('name_en', 'ASC')->get();
        $agents = Agent::orderBy('name', 'ASC')->get();
        #payment_methods
        $payments = PaymentMethod::orderBy('name', 'ASC')->get();
        $types = OrderType::orderBy('name_en', 'ASC')->get();
        $Corporate_ids = Corporate::where('is_active', 1);
        if ($user->is_warehouse_user) {
            $Corporate_ids = $Corporate_ids->join('customers', 'customers.corporate_id', '=', 'corporates.id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id);
        }
        $Corporate_ids = $Corporate_ids->select('corporates.*')->get();

        $customer = null;
        $drivers = [];
        $customers = [];
        $collection = null;
        $stores = [];

        if (!empty($collection_id)) {
            $collection = CollectionOrder::where('id', $collection_id)->first();
            if (!empty($collection) && $collection != null) {
                $customer = Customer::where('id', $collection->customer_id)->first();
                if ($customer) {
                    $customers = Customer::where('corporate_id', $customer->corporate_id)->get();
                    $collection->corporate_id = Corporate::where('id', $customer->corporate_id)->value('id');
                    $stores = Stock::whereRaw('qty - used_qty > 0')->where('corporate_id', $customer->corporate_id)->get();
                }
                if ($request->ajax()) {
                    return [
                        'view' => view('backend.orders.create-form', compact('drivers', 'customers', 'r_cities', 's_cities', 'order', 'collection_id', 'payments', 'agents', 'governments', 'types', 'Corporate_ids', 'collection', 'stores', 'customer'))->render(),
                    ];
                }
            } else {
                return 'false';
            }
        } else {
            return 'false';
        }
    }


    public function tmporders_update_view($id, Request $request)
    {
        $governments = Governorate::orderBy('name_en', 'ASC')->get();
        #payment_methods
        $payments = PaymentMethod::orderBy('name', 'ASC')->get();
        $types = OrderType::orderBy('name_en', 'ASC')->get();
        $collection_id = 0;
        $order = TempOrder::where('id', $id)->first();
        $s_cities = [];
        $r_cities = [];
        $stores = [];
        $collection = null;

        if (!empty($order) && $order != null) {
            $collection_id = $order->collection_id;
            $collection = CollectionOrder::where('id', $collection_id)->first();
            $s_cities = City::where('governorate_id', $order->s_government_id)->orderBy('name_en', 'ASC')->get();
            $r_cities = City::where('governorate_id', $order->r_government_id)->orderBy('name_en', 'ASC')->get();
            if (!empty($order->customer) && $order->customer != null) {
                $order->corporate_id = $order->customer->corporate_id;
                $stores = Stock::whereRaw('qty - used_qty > 0')->where('corporate_id', $order->corporate_id)->get();
            }
        }
        if ($request->ajax()) {
            return [
                'view' => view('profile.collections.update_form', compact('r_cities', 's_cities', 'order', 'collection_id', 'payments', 'governments', 'types', 'collection', 'stores'))->render(),
            ];
        }
    }

    //verify_mobile_customer
    function change_verify_mobile(Request $request)
    {
        $id = $request->id;
        $customer = Customer::findOrFail($id);
        if ($customer != null) {
            if ($customer->is_verify_mobile == 0) {
                $customer->is_verify_mobile = 1;
            } else {
                $customer->is_verify_mobile = 0;
            }
            $customer->save();

            return $customer->is_verify_mobile;
        } else {
            return 'error';
        }

    }


    function change_verify_email(Request $request)
    {
        $id = $request->id;
        $customer = Customer::findOrFail($id);
        if ($customer != null) {
            if ($customer->is_verify_email == 0) {
                $customer->is_verify_email = 1;
            } else {
                $customer->is_verify_email = 0;
            }
            $customer->save();

            return $customer->is_verify_email;
        } else {
            return 'error';
        }

    }


    function change_block(Request $request)
    {
        $id = $request->id;
        $customer = Customer::findOrFail($id);
        if ($customer != null) {
            if ($customer->is_block == 0) {
                $customer->is_block = 1;
            } else {
                $customer->is_block = 0;
            }
            $customer->save();

            return $customer->is_block;
        } else {
            return 'error';
        }

    }

    // get customer details for order creation
    public function details($id, Request $request)
    {
        return Customer::find($id);
    }
}

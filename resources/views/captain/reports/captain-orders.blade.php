<style>
    td, th {
        text-align: center;
        vertical-align: middle;
    }

</style>
<table>
    <tr>
        <th>No</th>
        <th>
            CREATE DATE
        </th>
        <th>
            ACCEPTED DATE
        </th>
        <th>
            RECEIVED DATE
        </th>
        <th>
            LAST STATUS DATE
        </th>
        <th>
            STATUS
        </th>
        <th>
            WAREHOUSE
        </th>
        <th>
            COLLECTED COST
        </th>
        <th>
            Corporate
        </th>
        <th>
            Customer
        </th>
        <th>
            CODE
        </th>
        <th>
            RECEIVER
        </th>
        <th>
            ADDRESS
        </th>
        <th>
            PHONE
        </th>
        <th>
            GOV
        </th>
        <th>
            CITY
        </th>
        <th>
            NOTES
        </th>
        <th>
            TYPE
        </th>
        <th>
            TOTAL
        </th>
        <th>
            Comment1
        </th>
        <th>
            Date1
        </th>
        <th>
            Comment2
        </th>
        <th>
            Date2
        </th>
        <th>
            Comment3
        </th>
        <th>
            Date3
        </th>
        <th>
            Comment4
        </th>
        <th>
            Date4
        </th>
        <th>
            Comment5
        </th>
        <th>
            Date5
        </th>
        <th>
            Comment6
        </th>
        <th>
            Date6
        </th>
        <th>
            Comment7
        </th>
        <th>
            Date7
        </th>
    </tr>
    @php
        $i=1;
    @endphp
    @if(! empty($orders))
        @foreach($orders->cursor() as $order)
            <tr>
                <td>{{$i}}</td>
                <td>
                    {{$order->created_at ? date('Y-m-d', strtotime($order->created_at)) : ''}}
                </td>
                <td>
                    {{$order->accepted_at ? date('Y-m-d', strtotime($order->accepted_at)) : ''}}
                </td>
                <td>
                    {{$order->received_at ? date('Y-m-d', strtotime($order->received_at)) : ''}}
                </td>
                <td>
                    {{$order->last_status_date ? date('Y-m-d', strtotime($order->last_status_date)) : ''}}
                </td>
                <td>
                    {!!$order->status_details_without_refund!!}
                </td>
                <td>
                    @if($order->is_refund == 1)
                        Refund
                    @elseif($order->client_dropoff == 1)
                        Client
                    @elseif($order->warehouse_dropoff == 1)
                        Warehouse
                    @endif
                </td>
                <td>
                    {{$order->collected_cost}}
                </td>
                <td>
                    {{isset($order->customer->Corporate->name) ? $order->customer->Corporate->name : '-'}}
                </td>
                <td>
                    {{isset($order->customer->name) ? $order->customer->name : '-'}}
                </td>
                <td>
                    {{$order->order_number}}
                </td>
                <td>
                    {{$order->receiver_name}}
                </td>
                <td>
                    {{mb_substr(htmlentities(strip_tags($order->receiver_address)),0,150, "utf-8")}}
                </td>
                <td>
                    {{$order->receiver_mobile}}
                </td>
                <td>
                    {{isset($order->to_government->name_en) ? $order->to_government->name_en : ''}}
                </td>
                <td>
                    {{isset($order->to_city->name_en) ? $order->to_city->name_en : ''}}
                </td>
                <td>
                    {{$order->notes}}
                </td>
                <td>
                    {{isset($order->types->name_en) ? $order->types->name_en : ''}}
                </td>
                <td>
                    {{$order->total_price}}
                </td>
                @foreach($order->comments->take(7) as $t => $order_comment)
                    <td>
                        {{$order_comment->comment ? $order_comment->comment : ''}}
                    </td>
                    <td>
                        {{$order_comment->created_at ? date('Y-m-d', strtotime($order_comment->created_at)) : ''}}
                    </td>
                @endforeach
                @php
                    $commentCount = 7 - count($order->comments)
                @endphp
                @if($commentCount)
                    @for ($k = 0; $k < $commentCount; $k++)
                        <td></td>
                        <td></td>
                    @endfor
                @endif
            </tr>

            @php
                $i++;
            @endphp
        @endforeach
    @endif
</table>

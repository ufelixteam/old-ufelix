@extends('captain.layouts.app')
@section('css')
    <title>@lang('captain.dashboard')</title>
@endsection
@section('content')
    @if (session('success'))
        <div class="alert alert-success alert-block text-center">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ session('success') }}</strong>
        </div>
    @endif
    @if (session('warning'))
        <div class="alert alert-warning alert-block text-center">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ session('warning') }}</strong>
        </div>
    @endif
    <div class="panel panel-headline">
        <div class="panel-heading">
            <h3 class="panel-title"> Overview</h3>
            <p class="panel-subtitle"></p>
        </div>

    </div>
    <div class="row">
        <div class="col-md-6">
            <!-- TASKS -->
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Overall Orders</h3>
                    <div class="right">
                        <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                        <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                    </div>
                </div>
                <div class="panel-body">
                    <ul class="list-unstyled task-list">
                        @if($orders > 0)
{{--                            <li>--}}
{{--                                <p>Accepted <span class="label-percent">{{  $orderAccepted }}</b>/{{ $orders }}</span>--}}
{{--                                </p>--}}
{{--                                <div class="progress progress-xs">--}}
{{--                                    <div class="progress-bar progress-bar-info" role="progressbar"--}}
{{--                                         aria-valuenow="{{  $orderAccepted }}" aria-valuemin="0"--}}
{{--                                         aria-valuemax="{{$orders}}"--}}
{{--                                         style="width: {{ ($orderAccepted/$orders) *100 }}%">--}}
{{--                                        <span class="sr-only">{{  $orderAccepted }}</b>/{{ $orders }}% Complete</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </li>--}}

{{--                            <li>--}}
{{--                                <p>Received <span class="label-percent">{{  $orderRunning }}</b>/{{ $orders }}</span>--}}
{{--                                </p>--}}
{{--                                <div class="progress progress-xs">--}}
{{--                                    <div class="progress-bar progress-bar-default" role="progressbar"--}}
{{--                                         aria-valuenow="{{  $orderRunning }}" aria-valuemin="0"--}}
{{--                                         aria-valuemax="{{$orders}}" style="width: {{ ($orderRunning/$orders) *100 }}%">--}}
{{--                                        <span class="sr-only">{{  $orderRunning }}</b>/{{ $orders }}% Complete</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <p>Cancelled Order <span class="label-percent">{{ $orderCancel }}</b>--}}
{{--                                        /{{ $orders }}</span></p>--}}
{{--                                <div class="progress progress-xs">--}}
{{--                                    <div class="progress-bar progress-bar-danger" role="progressbar"--}}
{{--                                         aria-valuenow="{{ $orderCancel }}" aria-valuemin="0"--}}
{{--                                         aria-valuemax="{{$orders}}" style="width:{{ ($orderCancel/$orders) *100 }}%">--}}
{{--                                        <span class="sr-only">{{ $orderCancel }}</b>/{{ $orders }} Complete</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <p>Recalled Order <span class="label-percent">{{ $orderRecall }}</b>--}}
{{--                                        /{{ $orders }}</span></p>--}}
{{--                                <div class="progress progress-xs">--}}
{{--                                    <div class="progress-bar progress-bar-danger" role="progressbar"--}}
{{--                                         aria-valuenow="{{ $orderRecall }}" aria-valuemin="0"--}}
{{--                                         aria-valuemax="{{$orders}}" style="width:{{ ($orderRecall/$orders) *100 }}%">--}}
{{--                                        <span class="sr-only">{{ $orderRecall }}</b>/{{ $orders }} Complete</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <p>Rejected Order <span class="label-percent">{{ $orderReject }}</b>--}}
{{--                                        /{{ $orders }}</span></p>--}}
{{--                                <div class="progress progress-xs">--}}
{{--                                    <div class="progress-bar progress-bar-danger" role="progressbar"--}}
{{--                                         aria-valuenow="{{ $orderReject }}" aria-valuemin="0"--}}
{{--                                         aria-valuemax="{{$orders}}" style="width:{{ ($orderReject/$orders) *100 }}%">--}}
{{--                                        <span class="sr-only">{{ $orderReject }}</b>/{{ $orders }} Complete</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <p>Delivered Orders <span class="label-percent">{{ $orderComplete }}</b>--}}
{{--                                        /{{ $orders }} </span></p>--}}
{{--                                <div class="progress progress-xs">--}}
{{--                                    <div class="progress-bar progress-bar-success" role="progressbar"--}}
{{--                                         aria-valuenow="{{ $orderComplete }}" aria-valuemin="0"--}}
{{--                                         aria-valuemax="{{$orders}}"--}}
{{--                                         style="width: {{ ($orderComplete/$orders) *100 }}%">--}}
{{--                                        <span class="sr-only">{{ $orderComplete }}</b>/{{ $orders }} Complete</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </li>--}}

                            <li>
                                <p>Current Orders <span class="label-percent">{{  $orderCurrent }}</b>/{{ ($orderFinish + $orderCurrent) }}</span>
                                </p>
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-default" role="progressbar"
                                         aria-valuenow="{{  $orderCurrent }}" aria-valuemin="0"
                                         aria-valuemax="{{($orderFinish + $orderCurrent)}}" style="width: {{ ($orderCurrent/$orders) *100 }}%">
                                        <span class="sr-only">{{  $orderCurrent }}</b>/{{ ($orderFinish + $orderCurrent) }}% Complete</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <p>Finished Orders <span class="label-percent">{{ $orderFinish }}</b>
                                        /{{ ($orderFinish + $orderCurrent) }} </span></p>
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-success" role="progressbar"
                                         aria-valuenow="{{ $orderFinish }}" aria-valuemin="0"
                                         aria-valuemax="{{$orders}}"
                                         style="width: {{ ($orderFinish/$orders) *100 }}%">
                                        <span class="sr-only">{{ $orderFinish }}</b>/{{ ($orderFinish + $orderCurrent) }} Complete</span>
                                    </div>
                                </div>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
            <!-- END TASKS -->
        </div>


        <div class="row">
            <div class="col-md-6">
                <!-- TASKS -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">New Orders</h3>
                        <div class="right">
                            <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i>
                            </button>
                            <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Receiver</th>
                                    <th>Sender</th>
                                    <th>Created At</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(! empty($neworders) && count($neworders)>0)
                                    @foreach($neworders as $order)

                                        <tr>
                                            <td><a href="{{ URL::to('/captainDashboard/orders') }}/{{ $order->id }}"
                                                   data-toggle="tooltip" data-placement="bottom"
                                                   title="Go to detail">{{ $order->id }}</a></td>
                                            <td>{{ $order->receiver_name }} </br> {{ $order->receiver_mobile }} </td>
                                            <td>{{ $order->sender_name }} </br> {{ $order->sender_mobile }} </td>

                                            <td> {{ $order->created_at }} </td>
                                            <td>
                                                {!! $order->status_span !!}
                                            </td>
                                        </tr>

                                    @endforeach

                                @else
                                    <tr>
                                        <td colspan="4" class="text-center">No Order Added</td>

                                    </tr>
                                @endif

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-6"><span class="panel-note"><i
                                        class="fa fa-clock-o"></i> Last 24 hours</span></div>
                            <div class="col-md-6 text-right"><a href="{{url('/captainDashboard/orders?type=ofd')}}"
                                                                class="btn btn-primary">View Current Orders</a></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.vehicles')}} - {{__('backend.add_vehicle')}}</title>
@endsection

@section('header')
  <div class="page-header">
    <h3><i class="glyphicon glyphicon-plus"></i> {{__('backend.add_vehicle')}}</h3>
  </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <form action="{{ route('mngrAdmin.trucks.store') }}" name="truck_create" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
          <div class="form-group col-md-6 col-sm-6 @if($errors->has('model_name')) has-error @endif">
            <label for="model_name-field">{{__('backend.Model_Name')}}: </label>
            <input type="text" id="model_name-field" name="model_name" class="form-control" value="{{ old("model_name") }}"/>
            @if($errors->has("model_name"))
              <span class="help-block">{{ $errors->first("model_name") }}</span>
            @endif
          </div>
          <div class="form-group col-md-6 col-sm-6 @if($errors->has('plate_number')) has-error @endif">
            <label for="plate_number-field">{{__('backend.Plate_Number')}}: </label>
            <input type="text" id="plate_number-field" name="plate_number" class="form-control" value="{{ old("plate_number") }}"/>
            @if($errors->has("plate_number"))
              <span class="help-block">{{ $errors->first("plate_number") }}</span>
            @endif
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-6 col-sm-6 @if($errors->has('chassi_number')) has-error @endif">
             <label for="chassi_number-field">{{__('backend.Chassi_Number')}}: </label>
             <input type="text" id="chassi_number-field" name="chassi_number" class="form-control" value="{{ old("chassi_number") }}"/>
             @if($errors->has("chassi_number"))
               <span class="help-block">{{ $errors->first("chassi_number") }}</span>
             @endif
          </div>
          <div class="form-group col-md-6 col-sm-6 @if($errors->has('year')) has-error @endif">
             <label for="year-field">{{__('backend.Model_Year')}}: </label>
             {{ Form::selectYear('year',  date('Y'),1980,null,["class"=>"form-control"]) }}
             @if($errors->has("year"))
              <span class="help-block">{{ $errors->first("year") }}</span>
             @endif
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-6 col-sm-6 @if($errors->has('status')) has-error @endif">
             <label for="status-field">{{__('backend.is_active')}}</label>
              <select id="status-field" name="status" class="form-control" value="{{ old("status") }}" >
                <option value="0" {{ old("status") == "0" ? 'selected' : '' }}>{{__('backend.no')}}</option>
                <option value="1" {{ old("status") == "1" ? 'selected' : '' }}>{{__('backend.yes')}}</option>
              </select>
              @if($errors->has("status"))
                <span class="help-block">{{ $errors->first("status") }}</span>
              @endif
          </div>
          <div class="form-group col-md-6 col-sm-6 @if($errors->has('type_id')) has-error @endif">
            <label for="type_id-field">{{__('backend.Vehicle_Type')}}: </label>
            <select id="vehicle_type_id-field" name="vehicle_type_id" class="form-control" >
              @if(! empty($truck_types))
                  @foreach($truck_types as $type)
                    <option value="{{$type->id}}" {{ old("vehicle_type_id") == $type->id ? 'selected' : '' }}">{{$type->name_en}}</option>
                  @endforeach
              @endif
            </select>
            @if($errors->has("vehicle_type_id"))
              <span class="help-block">{{ $errors->first("vehicle_type_id") }}</span>
            @endif
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-6 col-sm-6 @if($errors->has('color_id')) has-error @endif">
            <label for="color_id-field">{{__('backend.color')}}: </label>
            <select id="color_id-field" name="color_id" class="form-control" >
            @if(! empty($colors))
              @foreach($colors as $color)
                <option value="{{ $color->id }}" {{ old("color_id") == $color->id ? 'selected' : '' }}">{{$color->name}}</option>
              @endforeach
            @endif
            </select>
            @if($errors->has("color_id"))
              <span class="help-block">{{ $errors->first("color_id") }}</span>
            @endif
          </div>
          <div class="form-group col-md-6 col-sm-6 @if($errors->has('model_type_id')) has-error @endif">
            <label for="model_type_id-field">{{__('backend.Model_Type')}}</label>
            <select id="model_type_id-field" name="model_type_id" class="form-control" >
            @if(! empty($model_types))
              @foreach($model_types as $model)
                <option value="{{$model->id}}" {{ old("model_type_id") == $model->id ? 'selected' : '' }}">{{$model->name}}</option>
              @endforeach
            @endif
            </select>
            @if($errors->has("model_type_id"))
              <span class="help-block">{{ $errors->first("model_type_id") }}</span>
            @endif
          </div>
        </div>
        <div class="row">
{{--          <div class="form-group col-md-6 col-sm-6 @if($errors->has('agent_id')) has-error @endif">--}}
{{--            <label for="agent_id-field">{{__('backend.Belonging_Agent')}}: </label>--}}
{{--            <select id="agent_id-field" name="agent_id" class="form-control" >--}}
{{--              <option value="NULL">{{__('backend.Choose_Agent')}}</option>--}}
{{--              @if(! empty($agents))--}}
{{--                @foreach($agents as $agent)--}}
{{--                  <option value="{{$agent->id}}" {{ old("agent_id") == $agent->id ? 'selected' : '' }}">{{$agent->name}}</option>--}}
{{--                  @endforeach--}}
{{--              @endif--}}
{{--            </select>--}}
{{--            @if($errors->has("agent_id"))--}}
{{--              <span class="help-block">{{ $errors->first("agent_id") }}</span>--}}
{{--            @endif--}}
{{--          </div>--}}
          <div class="form-group col-md-6 col-sm-6 @if($errors->has('license_end_date')) has-error @endif">
            <label for="license_end_date_field">{{__('backend.License_End_Date')}}: </label>
            <div class='input-group date datepicker2' >
              <input type="text"  name="license_end_date" class="form-control"  value="{{ old("license_end_date") }}"/>
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
            </div>
            @if($errors->has("license_end_date"))
              <span class="help-block">{{ $errors->first("license_end_date") }}</span>
              @endif
          </div>

        </div>
        <!-- <div class="row">
          <div class="form-group col-md-6 col-sm-6 @if($errors->has('driver_id')) has-error @endif">
            <label for="driver_id-field">{{__('backend.Owner_Driver')}}: </label>
            <select id="driver_id-field" name="driver_id" class="form-control" >
              @if(! empty($drivers))
                @foreach($drivers as $driver)
                  <option value="{{$driver->id}}" {{ old("driver_id") == $driver->id ? 'selected' : '' }}">{{$driver->name}} - {{$driver->mobile}}</option>
                @endforeach
              @endif
            </select>
            @if($errors->has("driver_id"))
              <span class="help-block">{{ $errors->first("driver_id") }}</span>
              @endif
          </div>
          <div class="form-group col-md-6 col-sm-6 @if($errors->has('license_start_date')) has-error @endif">
             <label for="license_start_date_field">{{__('backend.License_Start_Date')}}: </label>
             <div class='input-group date ' id="datepicker">
               <input type="text" name="license_start_date" class="form-control date"  value="{{ old("license_start_date") }}"/>
               @if($errors->has("license_start_date"))
                 <span class="help-block">{{ $errors->first("license_start_date") }}</span>
               @endif
               <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
             </div>
          </div>
        </div> -->
        <div class="row">
          <div class="form-group col-md-6 col-sm-6 @if($errors->has('image')) has-error @endif">
            <div id="image_upload"></div>
            <label for="image-field">{{__('backend.Vehicle_Image')}}</label>
            <input required class="uploadPhoto btn btn-primary" type="file" name="image" id="image">
            @if($errors->has("image"))
              <span class="help-block">{{ $errors->first("image") }}</span>
            @endif
          </div>
          <div class="form-group col-md-6 col-sm-6 @if($errors->has('license_image_front')) has-error @endif">
            <div id="license_image_front_upload"></div>
            <label for="image-field">{{__('backend.License_Image_Front')}}</label>
            <input required class="uploadPhoto btn btn-primary" type="file" name="license_image_front" id="license_image_front">
            @if($errors->has("license_image_front"))
              <span class="help-block">{{ $errors->first("license_image_front") }}</span>
            @endif
          </div>
          <!-- <div class="form-group col-md-4 col-sm-12 @if($errors->has('license_image_back')) has-error @endif">
            <div id="license_image_back_upload"></div>
            <label for="license_image_back">{{__('backend.License_Image_Back')}}</label>
            <input required class="uploadPhoto btn btn-primary" type="file" name="license_image_back" id="license_image_back">
            @if($errors->has("license_image_back"))
              <span class="help-block">{{ $errors->first("license_image_back") }}</span>
            @endif
          </div> -->
        </div>
        <div class="well well-sm col-md-12 col-sm-12 col-xs-12">
          <button type="submit" class="btn btn-primary">{{__('backend.add_vehicle')}}</button>
          <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.trucks.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
        </div>
      </form>
    </div>
  </div>
@endsection
@section('scripts')
  <script>
        $(function () {
            $("form[name='truck_create']").validate({
                // Specify validation rules
                rules: {
                    model_name: {
                        required: true
                    },
                    plate_number: {
                        required: true,
                    },
                    license_end_date: {
                        required: true,
                        date:true
                    },
                    chassi_number: {
                        required: true,
                    },
                    vehicle_type_id: {
                        required: true
                    },
                    year: {
                        required: true
                    },
                    color_id: {
                        required: true
                    },
                    model_type_id: {
                        date:true
                    },
                    agent_id: {
                        date:true
                    },
                    image: {
                        required: true
                    },
                    license_image_front: {
                        required: true
                    },
                    // driver_id: {
                    //     required: true
                    // },
                    // license_image_back: {
                    //     required: true
                    // },
                    // license_start_date: {
                    //     required: true,
                    //     date:true
                    // },
                },
                // Specify validation error messages
                messages: {
                    password: {
                        required: "{{__('backend.Please_Enter_Driver_Password')}}",
                        minlength: "{{__('backend.Password_must_be_more_than5_character')}}"
                    },
                    model_name: {
                        required: "{{__('backend.Please_Enter_Model_Name')}}"
                    },
                    plate_number: {
                        required: "{{__('backend.Please_Enter_Plate_Number')}}",
                    },
                    chassi_number: {
                        required: "{{__('backend.Please_Enter_Chassi_Number')}}",
                    },
                    vehicle_type_id: {
                        required: "{{__('backend.Please_Enter_Vehicle_Type')}}"
                    },
                    year: {
                        required: "{{__('backend.Please_Enter_Year')}}"
                    },
                    color_id: {
                        required: "{{__('backend.Please_Enter_Color')}}"
                    },
                    model_type_id: {
                        required: "{{__('backend.Please_Enter_Model_Type')}}"
                    },
                    agent_id: {
                        required: "{{__('backend.Please_Select_Agent')}}"
                    },
                    license_end_date: {
                        required: "{{__('backend.Pleas_Enter_License_End_Date')}}",
                        date: "{{__('backend.Please_enter_avalid_date')}}"
                    },
                    image: {
                        required: "{{__('backend.Pleas_Chosse_Vehicle_Image')}}"
                    },
                    license_image_front: {
                        required: "{{__('backend.Pleas_Chosse_License_Image_Front')}}"
                    },
                    // driver_id: {
                    //     required: "{{__('backend.Please_Enter_Driver')}}"
                    // },
                    // license_image_back: {
                    //     required: "{{__('backend.Pleas_Chosse_License_Image_Back')}}"
                    // },
                    // license_start_date: {
                    //     required: "{{__('backend.Pleas_Enter_License_Start_Date')}}",
                    //     date: "{{__('backend.Please_enter_avalid_date')}}"
                    // },
                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                },

                highlight: function (element) {
                    $(element).parent().addClass('error')
                },
                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                }
            });
        })
    </script>
@endsection

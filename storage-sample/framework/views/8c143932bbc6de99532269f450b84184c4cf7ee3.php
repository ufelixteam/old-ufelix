

<?php $__env->startSection('css'); ?>
<title><?php echo e(__('backend.pickup_price_list')); ?></title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
<div class="page-header clearfix">
  <h3>
    <i class="glyphicon glyphicon-align-justify"></i> <?php echo e(__('backend.pickup_price_list')); ?>

    <?php if(permission('addPickupPriceList')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
      <a class="btn btn-success pull-right" href="<?php echo e(route('mngrAdmin.pickup_price_lists.create')); ?>"><i class="glyphicon glyphicon-plus"></i> <?php echo e(__('backend.add_pickup_price_list')); ?></a>
    <?php endif; ?>
  </h3>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  <div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
      <?php if($governoratePrices->count()): ?>
        <table class="table table-condensed table-striped text-center">
          <thead>
            <tr>
              <th>#</th>
              <th><?php echo e(__('backend.from')); ?></th>
              <th></th>
              <th><?php echo e(__('backend.to')); ?></th>
              <th><?php echo e(__('backend.cost')); ?></th>
              <th><?php echo e(__('backend.the_bonous')); ?></th>
              <th><?php echo e(__('backend.status')); ?></th>
              <th class="pull-right"></th>
            </tr>
          </thead>
          <tbody>
            <?php $__currentLoopData = $governoratePrices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $governoratePrice): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <tr>
                <td><?php echo e($governoratePrice->id); ?></td>
                <td><?php echo e($governoratePrice->Start->name_en); ?></td>
                <td><i class="glyphicon glyphicon-arrow-right" style="color: blue;"></i></td>
                <td><?php echo e($governoratePrice->End->name_en); ?></td>
                <td><?php echo e(! empty($governoratePrice->cost) ? $governoratePrice->cost : 'Not Specified'); ?></td>
                <td><?php echo e(! empty($governoratePrice->bonous) ? $governoratePrice->bonous : 'Not Specified'); ?></td>
                <td><?php echo $governoratePrice->status_span; ?></td>
                <td class="pull-right">
                  
                  <?php if(permission('editShipmentDestinations')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                    <a class="btn btn-xs btn-warning" href="<?php echo e(route('mngrAdmin.pickup_price_lists.edit', $governoratePrice->id)); ?>"><i class="glyphicon glyphicon-edit"></i> <?php echo e(__('backend.edit')); ?></a>
                  <?php endif; ?>
                  <?php if(permission('deleteShipmentDestinations')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                    <form action="<?php echo e(route('mngrAdmin.pickup_price_lists.destroy', $governoratePrice->id)); ?>" method="POST" style="display: inline;" onsubmit="if(confirm('<?php echo e(__('backend.msg_confirm_delete')); ?>')) { return true } else {return false };">
                      <input type="hidden" name="_method" value="DELETE">
                      <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                      <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> <?php echo e(__('backend.delete')); ?></button>
                    </form>
                  <?php endif; ?>
                </td>
              </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </tbody>
        </table>

        <?php echo $governoratePrices->appends($_GET)->links(); ?>


      <?php else: ?>
        <h3 class="text-center alert alert-warning"><?php echo e(__('backend.No_result_found')); ?></h3>
      <?php endif; ?>
    </div>
  </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
$('#theTable').DataTable({
  "pagingType": "full_numbers"
});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/pickup_price_lists/index.blade.php ENDPATH**/ ?>
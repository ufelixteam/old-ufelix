@extends('backend.layouts.app')

@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> App Reviews
        </h3>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <table id="driversTable" class="table table-condensed table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>User</th>
                        <th>Type</th>
                        <th>Stars</th>
                        <th>Review</th>
                        <th>Date</th>
                        <th class="text-right"></th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($list as $value)
                        <tr>
                            <td>{{$value->id}}</td>

                            @if(($value->type == 1 || $value->type == 2) && $value->customer_id != null)
                                <td><a href="{{ url('mngrAdmin/customers',$value->customer_id) }}">{{ App\Models\Customer::findOrFail($value->customer_id)->name }}</a></td>
                            @endif
                            @if(($value->type == 1 || $value->type == 2) && $value->captain_id != null)
                                <td><a href="{{ url('mngrAdmin/drivers',$value->captain_id) }}">{{ App\Models\Driver::findOrFail($value->captain_id)->name }}</a></td>
                            @endif

                            @if($value->type == 3 && $value->customer_id != null && $value->captain_id == null)
                                <td><a href="{{ url('mngrAdmin/customers',$value->customer_id) }}">{{ App\Models\Customer::findOrFail($value->customer_id)->name }}</a> reviewed <a href="{{ url('mngrAdmin/drivers',$value->second_id) }}">{{ App\Models\Driver::findOrFail($value->second_id)->name }}</a> </td>
                            @endif

                            @if($value->type == 4 && $value->captain_id != null && $value->customer_id == null)
                                <td><a href="{{ url('mngrAdmin/drivers',$value->captain_id) }}">{{ App\Models\Driver::findOrFail($value->captain_id)->name }}</a> reviewed <a href="{{ url('mngrAdmin/customers',$value->second_id) }}">{{ App\Models\Customer::findOrFail($value->second_id)->name }}</a> </td>
                            @endif

                            <td>{{$value->type == 1 ? 'Application rate' : ''}}  {{$value->type == 2 ? 'Services rate' : ''}}  {{$value->type == 3 ? 'Customer to captain rate' : ''}}  {{$value->type == 4 ? 'Captain to customer rate' : ''}}</td>
                            <td>{{$value->stars }}</td>
                            <td>{{$value->description }}</td>
                            <td>{{$value->date }}</td>

                            <td class="text-right">
                                {{--<a class="btn btn-xs btn-primary" href="{{ url('mngrAdmin/feedback/show', $value->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>--}}

                                {{--<form action="{{ route('mngrAdmin.weights.destroy', $weight->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">--}}
                                    {{--<input type="hidden" name="_method" value="DELETE">--}}
                                    {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
                                    {{--<button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>--}}
                                {{--</form>--}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $list->links() }}

        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $('#driversTable').DataTable({
            "bPaginate": false
        });
    </script>
@endsection
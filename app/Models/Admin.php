<?php

namespace App\Models;
use Zizaco\Entrust\Traits\EntrustUserTrait;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable,EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	public function getBlockSpanAttribute($value)
  {
      if($this->is_active != 1){
          $value="<span class='badge badge-pill label-danger'>".__('backend.blocked')."</span>";
      }else{
          $value="<span class='badge badge-pill label-success'>".__('backend.not_blocked')."</span>";
      }
      return $value;
  }

  public function getBlocksSpanAttribute($value)
    {
        if($this->is_active != 1){
            $value="<span class='badge badge-pill badge-danger'><i class='fa fa-times' aria-hidden='true'></i></span>";
            $value="<span class='badge badge-pill label-danger'><i class='fa fa-times' aria-hidden='true'></i></span>";
        }else{
            $value="<span class='badge badge-pill label-success'><i class='fa fa-check' aria-hidden='true'></i></span>";
        }
        return $value;
    }

	 public function getBlockTxtAttribute($value)
    {
        if($this->is_active != 1){
            $value="UnBlock";
        }else{
          $value="Block";
        }
        return $value;
    }
    public function getImageAttribute($value)
    {
        if($value != ""){
            $value=url("/")."/public/backend/images/".$value;
        }else{
          $value = "";
        }
        return $value;
    }
    public function agent() {
        return $this->belongsTo('App\Models\Agent','agent_id');
    }
}

<?php $__env->startSection('css'); ?>
    <title><?php echo e(__('backend.the_orders')); ?></title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">

    <style type="text/css">
        @media  only screen and (max-width: 580px) {
            .hide-td {
                display: none;
            }
        }

        .box-body {
            min-height: 100px;
        }

        .stylish-input-group .input-group-addon {
            background: white !important;
        }

        .stylish-input-group .form-control {
            border-right: 0;
            box-shadow: 0 0 0;
            border-color: #ccc;
        }

        .stylish-input-group button {
            border: 0;
            background: transparent;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <div class="page-header clearfix">
        <h3 style="display: inline-block">
            <i class="glyphicon glyphicon-align-justify"></i> <?php if(app('request')->input('type') != ""): ?> <?php echo e(ucfirst(app('request')->input('type'))); ?> <?php endif; ?>
            Orders
        </h3>

        <?php if($type == 'all'): ?>
            <a href="<?php echo e(url('mngrAdmin/print/collection_orders/' . $collection . '/' . $type)); ?>"
               class="btn btn-info pull-right" target="_blank"> <?php echo e(__('backend.pdf')); ?> </a>
        <?php endif; ?>

    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('error', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <div class="alert alert-danger" hidden>
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong id="errorId"></strong>
    </div>


    <div class="row" style="margin-bottom:15px;" id="orders">
        <div class="col-md-12">
            <form action="<?php echo e(URL::asset('/mngrAdmin/orders')); ?>" method="get"/>

            <div class="col-md-6 col-sm-12">
                <div id="imaginary_container">
                    <div class="input-group stylish-input-group">
                        <input type="text" class="form-control" id="search-field" name="search"
                               placeholder="<?php echo e(__('backend.search')); ?> ">
                        <span class="input-group-addon">
              <button type="button" id="search-btn1">
                <span class="glyphicon glyphicon-search"></span>
              </button>
            </span>
                    </div>
                </div>
            </div>
            <div class="group-control col-md-3 col-sm-3">
                <div class='input-group date' id='datepickerFilter'>
                    <input type="text" id="date" name="date" class="form-control" value=""/>
                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                </div>
            </div>
            <div class="group-control col-md-3 col-sm-3">
                <select id="s_government_id" name="s_government_id" class="form-control">
                    <option value="" data-display="Select"><?php echo e(__('backend.sender_Government')); ?></option>
                    <?php if(! empty($app_governments)): ?>
                        <?php $__currentLoopData = $app_governments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $government): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($government->id); ?>">
                                <?php echo e($government->name_en); ?>

                            </option>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>

                </select>
            </div>

        </div>
    </div>
    <div class="row" style="margin-bottom:15px;">
        <div class="col-md-12">

            <div class="group-control col-md-3 col-sm-3">
                <select id="s_state_id" name="s_state_id" class="form-control">
                    <option value=""><?php echo e(__('backend.sender_City')); ?></option>

                </select>
            </div>

            <div class="group-control col-md-3 col-sm-3">
                <select id="r_government_id" name="r_government_id" class="form-control">
                    <option value="" data-display="Select"><?php echo e(__('backend.Receiver_Government')); ?></option>
                    <?php if(! empty($app_governments)): ?>
                        <?php $__currentLoopData = $app_governments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $government): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($government->id); ?>">
                                <?php echo e($government->name_en); ?>

                            </option>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>

                </select>
            </div>
            <div class="group-control col-md-3 col-sm-3">
                <select id="r_state_id" name="r_state_id" class="form-control">
                    <option value=""><?php echo e(__('backend.Receiver_City')); ?></option>

                </select>
            </div>
            <div class="group-control col-md-2 col-sm-2">
                <input type="hidden" name="export" value="true"/>
                <input type="hidden" name="collection"
                       value="<?php echo e(! empty(app('request')->input('collection')) ? app('request')->input('collection') : ''); ?>"/>
                <input type="hidden" name="type"
                       value="<?php echo e(! empty(app('request')->input('type')) ? app('request')->input('type') : ''); ?>"/>
                <button type="submit" class="btn btn-primary btn-block"> <?php echo e(__('backend.exportExcel')); ?> </button>
            </div>

            </form>

        </div>
    </div>
    <div class='list'>
        <?php echo $__env->make('backend.orders.table', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <?php echo $__env->make('backend.pickups.create_pickup_popup', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <?php echo $__env->make('backend.orders.js-search', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

    <script>
        $(function () {
            /*print*/

            $(".printdata").on('click', function () {
                var url = "<?php echo e(URL::asset('/mngrAdmin/print_pdf/')); ?>/" + $(this).attr('data-id');
                $('<form action="' + url + '" method="get" target="_blank"></form>').appendTo('#orders').submit();
            })
            $(".delete-order").on('click', function () {

                var id = $(this).attr('data-id');
                $.confirm({
                    title: '',
                    theme: 'modern',
                    class: 'danger',
                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Confirm Delete</span></div><div style="font-weight:bold;"><p>Are You sure To Delete This Order ?</p></div>',
                    buttons: {
                        confirm: {
                            text: 'Delete Order',
                            btnClass: 'btn-danger',
                            action: function () {
                                $.ajax({
                                    url: "<?php echo e(url('/mngrAdmin/orders/')); ?>" + "/" + id,
                                    type: 'DELETE',
                                    data: {'_token': "<?php echo e(csrf_token()); ?>"},
                                    success: function (data) {
                                        jQuery('.alert-danger').html('');
                                        jQuery('.alert-danger').show();
                                        if (data != 'false') {
                                            jQuery('.alert-danger').append('<p>' + data + '</p>');
                                            setTimeout(function () {
                                                location.reload();
                                            }, 2000);
                                        } else {
                                            jQuery('.alert-danger').append('<p>Something Error</p>');
                                        }
                                        window.scrollTo({top: 0, behavior: 'smooth'});
                                    },
                                    error: function (data) {
                                        console.log('Error:', data);
                                    }
                                });
                            }
                        },
                        cancel: {
                            text: 'Cancel',
                            action: function () {
                            }
                        }
                    }
                });
            })

            $("#packup").on('click', function () {

                let ids = [];
                $(".selectpack:checked").each(function () {
                    ids.push($(this).val());
                });

                let collection_id = $('.selectpackup  input:checked').attr('data-value');

                let order_ids = $('.selectpackup  input:checked').val();

                $.confirm({
                    title: '',
                    theme: 'modern',
                    class: 'warning',
                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-simle-o  text-warning"></i></span><span class="jconfirm-title">Confirm Pick Up</span></div><div style="font-weight:bold;"><p>Are You sure To Pick Up This Orders ?</p></div>',
                    buttons: {
                        confirm: {
                            text: 'Packup Order',
                            btnClass: 'btn-warning',
                            action: function () {
                                $.ajax({
                                    url: "<?php echo e(url('/mngrAdmin/pickup_orders/')); ?>",
                                    type: 'POST',
                                    data: {'_token': "<?php echo e(csrf_token()); ?>", 'ids': ids, 'collection_id': collection_id},
                                    success: function (data) {
                                        if (data['status'] != 'false') {

                                            $(".alert").hide();
                                            $('#orderpickup').val(data['orders']);
                                            $('#from-pick').val(data['frompick']);
                                            $('#delivery_price-field').val(data['cost']);
                                            $('#bonous-field').val(data['bonous']);
                                            $('#pickupModal').modal('show');

                                        } else {
                                            $('#errorId').html(data['message']);
                                            $(".alert").show();

                                            $('#pickupModal').modal('hide');

                                        }
                                        window.scrollTo({top: 0, behavior: 'smooth'});
                                    },
                                    error: function (data) {
                                        console.log('Error:', data);
                                    }
                                });
                            }
                        },
                        cancel: {
                            text: 'Cancel',
                            action: function () {
                            }
                        }
                    }
                });
            })

        });


        $('#pdf-submit').on('click', function () {

            let ids = [];

            $(".selectpdf   input:checked").each(function () {
                ids.push($(this).val());

            });
            if (ids.length > 0) {
                $('<form action="<?php echo e(url("/mngrAdmin/print_police/")); ?>" method="post" ><input value="' + ids + ' " name="ids"  />" <?php echo e(csrf_field()); ?>"</form>').appendTo('body').submit();

            } else {

                $.confirm({
                    title: '',
                    theme: 'modern',
                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Print PDF</span></div><div style="font-weight:bold;"><p><?php echo e(__("backend.Must_Choose_Orders_To_Print")); ?></p></div>',
                    type: 'red',
                });
            }

        })


    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/orders/index.blade.php ENDPATH**/ ?>
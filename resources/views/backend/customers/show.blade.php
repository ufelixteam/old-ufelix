@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.customers')}} - {{__('backend.view')}}</title>
    <style>
        .img {
            /*border: 1px solid #8e7ad4;*/
            height: 100px;
            width: 100px;
            border-radius: 50%;
            /*padding: 10px;*/
        }
    </style>
@endsection
@section('header')
    <div class="page-header">
        <h3># {{$customer->id}}
            - {{__('backend.customer')}} {{$customer->name."  (".$customer->mobile.")"}}</h3> {!! $customer->type_span !!}
        {!! ! empty($customer->Corporate ) ? $customer->Corporate->link : '' !!} <br>

    <!--{!! $customer->active_span !!}--> {!! $customer->block_span !!} {{$customer->created_at}}
        <form action="{{ route('mngrAdmin.customers.destroy', $customer->id) }}" method="POST" style="display: inline;"
              onsubmit="if(confirm('{{__('Backend.msg_confirm_delete')}}')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">

            @if(permission('showOrdersMember')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <a class="btn btn-default btn-group"
                   href="{{url('mngrAdmin/orders/showOrders') . '/4' . '/' . $customer->id}}"><i
                        class="glyphicon glyphicon-list"></i> {{__('backend.orders')}} </a>
            @endif

            @if(permission('changeStatus')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
            <!-- <button id="change_status" type="button" value="{{ $customer->id }}"  class="btn btn-info">{{ $customer->status_txt }}</button> -->
            @endif

            @if(permission('blockedCustomer')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <button id="block" type="button" value="{{ $customer->id }}"
                        class="btn btn-success">{!! $customer->block_txt !!}</button>
            @endif

            @if(permission('editCustomer')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <a class="btn btn-warning btn-group" role="group"
                   href="{{ route('mngrAdmin.customers.edit', $customer->id) }}"><i
                        class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}} </a>
            @endif

            @if(permission('deleteCustomer')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <button type="submit" class="btn btn-danger"> {{__('backend.delete')}} <i
                        class="glyphicon glyphicon-trash"></i></button>
                @endif
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="col-sm-12" style="border: 2px solid #fdb801;color: #fff;margin-bottom: 20px;background-color: #fdb801;">
        <h5>{{__('backend.personal_information')}}</h5>
    </div>

    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <div class="form-group col-sm-4">
                <p class="form-control-static">
                    <img src="{{ $customer->image != "" ? $customer->image : asset('/assets/images/user.png')  }}"
                         onerror="this.src='{{asset('assets/images/user.png')}}'"
                         class="img"/>
                </p>
            </div>

            <div class="form-group col-sm-4">
                <label for="name">{{__('backend.name')}}</label>
                <p class="form-control-static">{{!empty($customer->name)?$customer->name:""}}</p>
            </div>


            <div class="form-group col-sm-4">
                <label for="name">{{__('backend.email')}}</label>
                <p class="form-control-static">{{!empty($customer->email)?$customer->email:""}}</p>
            </div>


            <div class="form-group col-sm-4">
                <label for="phone"> {{__('backend.mobile_number')}}</label>
                <p class="form-control-static">{{!empty($customer->mobile)?$customer->mobile:""}}</p>
            </div>


            <div class="form-group col-sm-4">
                <label for="phone"> {{__('backend.phone')}}</label>
                <p class="form-control-static">{{!empty($customer->phone)?$customer->phone:""}}</p>
            </div>

            <div class="form-group col-sm-4">
                <label for="governorate "> {{__('backend.governorate')}}</label>
                <p class="form-control-static">{{!empty($customer->governorate )? $customer->governorate->name_en :""}}</p>
            </div>

            <div class="form-group col-sm-4">
                <label for="city"> {{__('backend.city')}}</label>
                <p class="form-control-static">{{!empty($customer->city)? $customer->city->name_en :""}}</p>
            </div>

            <div class="form-group col-sm-4">
                <label for="phone"> {{__('backend.national_id_number')}}</label>
                <p class="form-control-static">{{!empty($customer->national_id)?$customer->national_id: " Not Found "}}</p>
            </div>

            <div class="form-group col-sm-4">
                <label for="phone"> {{__('backend.job_name')}}</label>
                <p class="form-control-static">{{!empty($customer->job)?$customer->job: " Not Found "}}</p>
            </div>


            <div class="form-group col-sm-4">
                <label for="address"> {{__('backend.address')}}</label>
                <p class="form-control-static">{{!empty($customer->address)?$customer->address: " Not Found "}}</p>
            </div>

            <div class="form-group col-sm-4">
                <label for="store">{{__('backend.store')}}: </label>
                <p class="form-control-static">{{!empty($customer->store) ? $customer->store->name : ''}}</p>
            </div>

            <div class="form-group col-sm-4">
                <label for="latitude"> {{__('backend.latitude')}}</label>
                <p class="form-control-static">{{!empty($customer->latitude)?$customer->latitude: " Not Found "}}</p>
            </div>

            <div class="form-group col-sm-4">
                <label for="longitude"> {{__('backend.longitude')}}</label>
                <p class="form-control-static">{{!empty($customer->longitude)?$customer->longitude: " Not Found "}}</p>
            </div>


        </div>
    </div>

    @if (! empty($customer->Corporate))
        <div class="col-sm-12"
             style="border: 2px solid #fdb801;color: #fff;margin-bottom: 20px;background-color: #fdb801;">
            <h5>{{__('backend.corporate_information')}}</h5>
        </div>
        <div class="row">
            <div class="form-group col-sm-4">
                <label for="notes">{{__('backend.name')}}</label>
                <p class="form-control-static">{{ ! empty($customer->Corporate->name)?$customer->Corporate->name:''}}</p>
            </div>
            <div class="form-group col-sm-4">
                <label for="notes">{{__('backend.email')}}</label>
                <p class="form-control-static">{{ ! empty($customer->Corporate->email)?$customer->Corporate->email:''}}</p>
            </div>
            <div class="form-group col-sm-4">
                <label for="notes">{{__('backend.mobile')}}</label>
                <p class="form-control-static">{{ ! empty($customer->Corporate->mobile)?$customer->Corporate->mobile:''}}</p>
            </div>
            {{--            <div class="form-group col-sm-4">--}}
            {{--                <label for="notes">{{__('backend.phone')}}</label>--}}
            {{--                <p class="form-control-static">{{ ! empty($customer->Corporate->phone)?$customer->Corporate->phone:''}}</p>--}}
            {{--            </div>--}}
        </div>

    @endif

    @if($customer->government_id)
        <div class="col-sm-12"
             style="border: 2px solid #fdb801;color: #fff;margin-bottom: 20px;margin-top: 20px;background-color: #fdb801;">
            <h5>
                {{__('backend.customer_prices')}}
                @if(permission('addShipmentDestinations'))
                    <a class="btn btn-primary pull-right" style="margin-top: -10px;"
                       href="{{ route('mngrAdmin.customerPrices.create', ['customer_id' => $customer->id]) }}"><i
                            class="glyphicon glyphicon-plus"></i> {{__('backend.add_price')}} </a>

                    <a class="btn btn-primary pull-right" style="margin: -10px 5px 0 5px;"
                       href="{{ route('mngrAdmin.customerPrices.generate', ['customer_id' => $customer->id]) }}"><i
                            class="glyphicon glyphicon-refresh"></i> {{__('backend.generate_prices')}} </a>

                @endif
            </h5>

        </div>

        <div class="row">

            <div class="form-group col-sm-12">
                @if($prices->count())
                    <table class="table table-condensed table-striped text-center">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('backend.from')}}</th>
                            <th></th>
                            <th>{{__('backend.to')}}</th>
                            <th>{{__('backend.cost')}}</th>
                            <th>{{__('backend.recall_cost')}}</th>
                            <th>{{__('backend.reject_cost')}}</th>
                            <th>{{__('backend.cancel_cost')}}</th>
                            <th>{{__('backend.status')}}</th>
                            <th class=""></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($prices as $key => $customerPrices)
                            <tr>
                                <td colspan="10"><h4><strong>{{$key}}</strong></h4></td>
                            </tr>
                            @foreach($customerPrices as $customerPrice)
                                <tr>
                                    <td>{{$customerPrice->id}}</td>
                                    <td>{{$customerPrice->Start->name_en}}</td>
                                    <td><i class="glyphicon glyphicon-arrow-right" style="color: blue;"></i></td>
                                    <td>{{$customerPrice->End->name_en}}</td>
                                    <td>
                                        <input type="number" class="form-control cost" name="cost"
                                               value="{{$customerPrice->cost}}" id="cost_{{$customerPrice->id}}"
                                               data-id="{{$customerPrice->id}}">
                                    </td>
                                    <td>
                                        <input type="number" class="form-control recall_cost" name="recall_cost"
                                               value="{{$customerPrice->recall_cost}}"
                                               id="recall_cost_{{$customerPrice->id}}"
                                               data-id="{{$customerPrice->id}}">
                                    </td>
                                    <td>
                                        <input type="number" class="form-control reject_cost" name="reject_cost"
                                               value="{{$customerPrice->reject_cost}}"
                                               id="reject_cost_{{$customerPrice->id}}"
                                               data-id="{{$customerPrice->id}}">
                                    </td>
                                    <td>
                                        <input type="number" class="form-control cancel_cost" name="cancel_cost"
                                               value="{{$customerPrice->cancel_cost}}"
                                               id="cancel_cost_{{$customerPrice->id}}"
                                               data-id="{{$customerPrice->id}}">
                                    </td>
                                    <td>{!! $customerPrice->status_span !!}</td>
                                    <td class="">
                                    @if(permission('editShipmentDestinations')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->

                                        <a class="btn btn-xs btn-success save_cost disabled"
                                           href="#" data-id="{{$customerPrice->id}}"
                                           id="save_cost_{{$customerPrice->id}}"><i
                                                class="glyphicon glyphicon-edit"></i> {{__('backend.save')}}</a>

                                        <a class="btn btn-xs btn-warning"
                                           href="{{ route('mngrAdmin.customerPrices.edit', $customerPrice->id)}}"><i
                                                class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>

                                    @endif
                                    @if(permission('deleteShipmentDestinations')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                        <form
                                            action="{{ route('mngrAdmin.customerPrices.destroy', $customerPrice->id) }}"
                                            method="POST" style="display: inline;"
                                            onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button type="submit" class="btn btn-xs btn-danger"><i
                                                    class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}
                                            </button>
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>

                    {{--                {!! $customerPrices->appends($_GET)->links() !!}--}}

                @else
                    <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
                @endif
            </div>
        </div>
    @endif

    <div class="row">

        <div class="col-md-12 col-xs-12 col-sm-12">
            <a class="btn btn-link pull-right" href="{{ URL::previous()  }}"><i
                    class="glyphicon glyphicon-backward"></i> {{__('backend.back')}} </a>
        </div>

    </div>


@endsection
@section('scripts')
    <script>
        $(function () {
            $('#block').on('click', function () {
                var Status = $(this).val();
                $.ajax({
                    url: "{{ url('/mngrAdmin/customer/block/'.$customer->id) }}",
                    Type: "GET",
                    dataType: 'json',
                    success: function (response) {
                        if (response == 0) {
                            $('#block').html('Block');
                        } else {
                            $('#block').html('UnBlock');
                        }
                        location.reload();
                    }
                });
            });

            $('#change_status').on('click', function () {
                var Status = $(this).val();
                // alert(Status);
                $.ajax({
                    url: "{{ url('/mngrAdmin/customer/status/'.$customer->id) }}",
                    Type: "GET",
                    dataType: 'json',
                    success: function (response) {
                        if (response == 0) {
                            $('#change_status').html('Active');
                        } else {
                            $('#change_status').html('UnActive');
                        }
                        location.reload();
                    }
                });
            });

            $('.cost, .recall_cost, .cancel_cost, .reject_cost').on('input', function () {
                $("#save_cost_" + $(this).attr('data-id')).removeClass('disabled');
            });

            $('.save_cost').on('click', function (e) {
                e.preventDefault();
                var self = $(this);
                let id = $(this).attr('data-id');
                $.ajax({
                    url: '{{URL::asset("/mngrAdmin/customerPrices/save_cost")}}',
                    type: 'post',
                    data: {
                        id: id,
                        cost: $("#cost_" + id).val(),
                        recall_cost: $("#recall_cost_" + id).val(),
                        reject_cost: $("#reject_cost_" + id).val(),
                        cancel_cost: $("#cancel_cost_" + id).val(),
                        _token: "{{csrf_token()}}"
                    },
                    success: function (data) {
                        self.addClass('disabled');
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            })
        });
    </script>
@endsection

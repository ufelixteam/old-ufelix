<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use App\Http\Requests;
use App\Models\City;
use App\Models\Governorate;
use App\Models\Store;
use App\Models\StoreUser;
use App\Models\User;
use Auth;
use Config;
use Flash;
use Illuminate\Http\Request;
use PDF;

class StoreController extends Controller
{

    private $guardType;

    public function __construct(Request $request)
    {
        if ($request->is('mngrAdmin/*') || $request->is('admin/*')) {
            Config::set('auth.defaults.guard', 'admin');
        } elseif ($request->is('mngrAgent/*') || $request->is('agent/*')) {
            Config::set('auth.defaults.guard', 'agent');
        }
        $this->guardType = Config::get('auth.defaults.guard');
    }

    // Get All Stores
    public function index()
    {
        $user = Auth::guard($this->guardType)->user();

        $stores = Store::with(['responsibles' => function ($query) {
            $query->where('is_responsible', 1);
        }])->withCount('stocks')
            ->orderBy('id', 'desc');

        if ($user->is_warehouse_user) {
            $stores = $stores->join('store_user', 'store_user.store_id', '=', 'stores.id')
                ->where('user_id', $user->id);
        }

        $stores = $stores->paginate(50);

        return view('backend.stores.index', compact('stores'));
    }

    // Create Store Page
    public function create()
    {
        $user = Auth::guard($this->guardType)->user();

        $governorates = Governorate::orderBy('name_en', 'ASC')->get();
        $responsibles = User::join('role_admins', 'role_admins.id', '=', 'users.role_admin_id')
            ->where('role_id', 1)
            ->where('is_active', 1)
            ->where('is_warehouse_role', 1)
            ->orderBy('users.name', 'ASC')
            ->select('users.*')
            ->get();

        $is_warehouse_role = $user->is_warehouse_user;

        return view('backend.stores.create', compact('governorates', 'responsibles', 'is_warehouse_role'));
    }

    // Added New Store
    public function store(Request $request)
    {
        $user = Auth::guard($this->guardType)->user();

        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'governorate_id' => 'required|integer',
            'status' => 'required',
            'image' => 'mimes:jpeg,png,jpg,gif,bitmap ',
        ]);

        /*
        * status = 0 :: Not Publish
        * status = 1 :: Publish
        */

        $store = new Store();
        $store->name = $request->input("name");
        $store->latitude = $request->input("latitude");
        $store->longitude = $request->input("longitude");
        $store->governorate_id = $request->input("governorate_id");
        $store->address = $request->input("address");
        $store->status = $request->input("status");
        $store->pickup_default = $request->input("pickup_default");
        $store->mobile = $request->input("mobile");
        $store->state_id = $request->input("state_id");
        $store->pickup_default = $request->input("pickup_default");
        $store->responsible = $request->input('responsible');

        if ($request->file("image") != "") {
            $extension = \File::extension($request->file("image")->getClientOriginalName());
            $image = 'warehouse-' . time() . "." . $extension;
            $store->image = $image;
            $request->file("image")->move(public_path('api_uploades/warehouse/'), $image);
        }

        $store->save();

        if (!$user->is_warehouse_user) {
            if ($request->get("responsible")) {
                StoreUser::create(['user_id' => $request->get("responsible"), 'store_id' => $store->id, 'is_responsible' => 1]);
            }

            $users = is_array($request->get("users")) ? $request->get("users") : [];
            foreach ($users as $user) {
                StoreUser::create(['user_id' => $user, 'store_id' => $store->id, 'is_responsible' => 0]);
            }
        }

        if ($request->input("pickup_default") == 1) {
            Store::where('id', '!=', $store->id)->update(['pickup_default' => 0]);
        }
        // Function Add Action In Activity Log By Samir
        addActivityLog('Add New Store ' . $store->name, ' اضافة مخزن جديد ' . $store->name, 12); // Helper Function in App\Helpers.php

        return redirect()->route('mngrAdmin.stores.index')->with('message', __('backend.Store_created_successfully'));
    }

    // View One Store
    public function show($id)
    {
        $store = Store::withCount('stocks')->findOrFail($id);
        $selectedResponsible = $store->responsibles()->where('is_responsible', 1)->first();

        return view('backend.stores.show', compact('store', 'selectedResponsible'));
    }

    // Edit sTORE pAGE
    public function edit($id)
    {
        $user = Auth::guard($this->guardType)->user();

        $store = Store::findOrFail($id);
        $governorates = Governorate::orderBy('name_en', 'ASC')->get();
        $cities = City::where('governorate_id', $store->governorate_id)->orderBy('id', 'desc')->get();
        $responsibles = User::where('role_id', 1)->where('is_active', 1)->orderBy('name', 'ASC')->get();

        $selectedResponsible = $store->responsibles()->where('is_responsible', 1)->first();
        $selectedResponsibles = $store->responsibles()->where('is_responsible', 0)->pluck('users.id')->toArray();

        $is_warehouse_role = $user->is_warehouse_user;

        return view('backend.stores.edit', compact('store', 'governorates', 'cities', 'responsibles', 'selectedResponsibles', 'selectedResponsible', 'is_warehouse_role'));
    }

    // Update Store
    public function update(Request $request, $id)
    {
        $user = Auth::guard($this->guardType)->user();

        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'governorate_id' => 'required|integer',
            'status' => 'required',
            'image' => 'mimes:jpeg,png,jpg,gif,bitmap ',

        ]);

        /*
        * status = 0 :: Not Publish
        * status = 1 :: Publish
        */

        $store = Store::findOrFail($id);
        $store->name = $request->input("name");
        $store->latitude = $request->input("latitude");
        $store->longitude = $request->input("longitude");
        $store->governorate_id = $request->input("governorate_id");
        $store->status = $request->input("status");
        $store->pickup_default = $request->input("pickup_default");
        $store->mobile = $request->input("mobile");
        $store->state_id = $request->input("state_id");
        $store->address = $request->input("address");

        if (!$request->image_old) {
            $store->image = '';
        }

        if ($request->file("image") != "") {
            $extension = \File::extension($request->file("image")->getClientOriginalName());
            $image = 'warehouse-' . time() . "." . $extension;
            $store->image = $image;
            $request->file("image")->move(public_path('api_uploades/warehouse/'), $image);
        }

        $store->save();

        if (!$user->is_warehouse_user) {
            StoreUser::where('store_id', $id)->delete();
            if ($request->get("responsible")) {
                StoreUser::create(['user_id' => $request->get("responsible"), 'store_id' => $id, 'is_responsible' => 1]);
            }

            $users = is_array($request->get("users")) ? $request->get("users") : [];
            foreach ($users as $user) {
                StoreUser::create(['user_id' => $user, 'store_id' => $id, 'is_responsible' => 0]);
            }
        }

        if ($request->input("pickup_default") == 1) {
            Store::where('id', '!=', $store->id)->update(['pickup_default' => 0]);
        }

        // Function Add Action In Activity Log By Samir
        addActivityLog('Edit Store ' . $store->name, ' تعديل المخزن ' . $store->name, 12); // Helper Function in App\Helpers.php

        return redirect()->route('mngrAdmin.stores.index')->with('message', __('backend.Store_Updated_successfully'));
    }

    // Delete Store
    public function destroy($id)
    {
        $store = Store::findOrFail($id);
        $store->delete();
        // Function Add Action In Activity Log By Samir
        addActivityLog('Delete Store ' . $store->name, ' حذف المخزن ' . $store->name, 12); // Helper Function in App\Helpers.php

        return redirect()->route('mngrAdmin.stores.index')->with('message', __('backend.Store_deleted_successfully'));
    }

    #get_store

    public function get_store($id)
    {
        $store = Store::findOrFail($id);
        return $store;
    }

    public function download_pdf($id)
    {
        $store = Store::withCount('stocks')->findOrFail($id);
        $pdf = PDF::loadView('backend.stores.pdf', compact('store', $store), [], ['format' => 'A4-L']);
        return $pdf->stream('store.pdf');
    }

    public function stores_pdf(Request $request)
    {
//        dd($request->select);
        $selected_stores_ids = $request->select;
        if ($selected_stores_ids != Null) {
            $stores = Store::with(['responsibles' => function ($query) {
                $query->where('is_responsible', 1);
            }])->withCount('stocks')
                ->orderBy('id', 'desc')
                ->whereIn('id', $selected_stores_ids)->get();
            $pdf = PDF::loadView('backend.stores.stores-pdf', compact('stores', $stores), [], ['format' => 'A4-L']);
            $pdf->stream('Stores.pdf');
        } else {
            Flash::warning(__('backend.no_selected_stores'));
            return redirect()->back();
        }

    }

//    // Save Responsible
//    public function save_responsible(Request $request) {
//        $request->validate([
//            'responsible' => 'required',
//            'id'=> 'required|integer',
//        ]);
//        // if validation  passed
//        Store::where('id', $request->input('id'))->update(['responsible'=>$request->input('responsible')]);
//
//    }
}

<?php

namespace App\Http\Controllers\Backend;

use App\Events\OrderUpdated;
use App\Http\Controllers\Controller\Backend;
use App\Http\Controllers\SmsController;
use App\Models\AcceptedOrder;
use App\Models\Agent;
use App\Models\AgentGovernment;
use App\Models\City;
use App\Models\CollectionOrder;
use App\Models\Corporate;
use App\Models\Customer;
use App\Models\CustomerPrice;
use App\Models\DeliveryProblem;
use App\Models\Driver;
use App\Models\ForwardOrder;
use App\Models\Governorate;
use App\Models\GovernoratePrice;
use App\Models\Invoice;
use App\Models\InvoiceOrder;
use App\Models\Order;
use App\Models\OrderComment;
use App\Models\OrderDelay;
use App\Models\OrderDeliveryProblem;
use App\Models\OrderType;
use App\Models\PaymentMethod;
use App\Models\RefundCollection;
use App\Models\Stock;
use App\Models\Store;
use App\Models\TempOrder;
use App\Models\User;
use App\Models\VerifyCode;
use App\Models\ViewReceiverData;
use Auth;
use Carbon\Carbon;
use Config;
use DB;
use ExcelReport;
use Flash;
use Illuminate\Http\Request;
use PDF;
use Redirect;
use Response;
use Spatie\Activitylog\Models\Activity;
use Validator;


class OrderController extends Controller
{
    private $guardType;

    /*
        order status ::
        0 == > pending
        1 == > accepted
        2 == > received
        3 == > delivered
        4 == > canceled
        5 == > recall
        6 == > waiting
        7 == > dropped
        8 == > rejected
    */
    public function __construct(Request $request)
    {
        if ($request->is('mngrAdmin/*') || $request->is('admin/*')) {
            Config::set('auth.defaults.guard', 'admin');
        } elseif ($request->is('mngrAgent/*') || $request->is('agent/*')) {
            Config::set('auth.defaults.guard', 'agent');
        }
        $this->guardType = Config::get('auth.defaults.guard');
    }

    //list_receivers

    public function list_receivers()
    {
//        $ordersIds = [];
//        $ids = Order::orderBy('receiver_name', 'ASC')->select('receiver_mobile')->distinct('receiver_mobile')->pluck('receiver_mobile')->toArray();
//        if (count($ids) > 0) {
//            $ids = array_unique($ids);
//            foreach ($ids as $id) {
//                $order = Order::where('receiver_mobile', $id)->first();
//                if ($order !== null) {
//                    array_push($ordersIds, $order->id);
//                }
//            }
//        }
        $queryBuilder = ViewReceiverData::select(\DB::raw('receiver_mobile'),
            \DB::raw('receiver_phone'), 'receiver_name', 'total_orders', 'delivered_orders');

        $search = request()->search;

        if ($search) {
            $queryBuilder->where(function ($query) use ($search) {
                $query->OrWhere('receiver_name', 'like', '%' . $search . '%');
                $query->OrWhere('receiver_mobile', 'like', '%' . $search . '%');
                $query->orWhere('receiver_phone', 'like', '%' . $search . '%');
            });
        }

        if (app('request')->input('export') == 'true') {

            $columns = [

                'Receiver Name' => 'receiver_name',
                'Receiver Mobile' => 'receiver_mobile',
            ];

            $title = 'Receivers Clients';
            $meta = ["Excell For " => "Receivers Clients", "Date" => date('Y-m-d')];
            return ExcelReport::of($title, $meta, $queryBuilder, $columns)
                ->setCss([
                    '.bolder' => 'font-weight: 800;',
                    '.italic-red' => 'color: red;font-style: italic;'
                ])->download('receivers-' . date('Y-m-d'));
        }

        $orders = $queryBuilder->paginate(50);

        return view('backend.orders.receivers', compact('orders'));
    }

    public function send_to_other_driver(Request $request)
    {
        $orderCheck = Order::where('id', $request->order_id)->first();
        if (!empty($orderCheck)) {
            $order = new AcceptedOrder();
            $order->order_id = $request->order_id;
            $order->driver_id = $request->items;
            $order->delivery_price = $orderCheck->delivery_price;
            $order->status = 0;
            $order->save();
            #ssavre order
            $orderCheck->status = 1;
            $order->save();
            //send notification
            $data = [];
            $data['title'] = 'Accept Request';
            $data['message'] = 'Captain accept your request' . $orderCheck->order_no . ' .. :)';
            $data['object_id'] = $request->order_id;
            $data['type'] = 2;
            $data['client_id'] = Order::where('id', $request->order_id)->value('customer_id');
            $data['client_model'] = '2';

//            (new NotificationController())->new_notify($data);

            $data = [];
            $data['title'] = 'Forward Request';
            $data['message'] = 'Request ' . $orderCheck->order_no . ' added to your List .. :)';
            $data['object_id'] = $request->order_id;
            $data['type'] = 2;
            $data['client_id'] = $request->items;
            $data['client_model'] = '1';

//            (new NotificationController())->new_notify($data);
            Flash::success(__('backend.Order_Routed_saved_successfully'));
            return back();
        } else {
            flash(__('backend.Order_does_not_exist'))->error();
            return back();

        }
    }/*send_to_other_driver*/

    public function orders_agent_index()
    {
        $id = Auth::guard($this->guardType)->user()->agent_id;
        $orders = AcceptedOrder::select('orders.id as order_id', 'accepted_orders.received_date', 'accepted_orders.status as accepted_status', 'orders.status as order_status', 'accepted_orders.driver_id', 'orders.sender_mobile', 'orders.sender_name', 'orders.order_price', 'orders.type', 'orders.receiver_name', 'orders.receiver_mobile')->
        join('orders', 'orders.id', '=', 'accepted_orders.order_id')->
        join('drivers', 'accepted_orders.driver_id', '=', 'drivers.id')->
        where('drivers.agent_id', '=', $id)->
        paginate(15);

        return view('agent.orders.all_order', compact('orders'));
    }/*orders_agent_index*/

    public function orders_agent_pending()
    {
        $title = "Pending ";
        $id = Auth::guard($this->guardType)->user()->agent_id;
        $areaids = AgentGovernment::where('agent_id', $id)->pluck('government_id')->toArray();
        $orders = Order::where('agent_id', $id)->where('status', 0)->whereIn('s_government_id', $areaids)->paginate(15);
        $online_drivers = Driver::where('agent_id', $id)->get();

        return view('agent.PolitOrder.pending', compact('orders', 'title', 'online_drivers'));
    }/*orders_agent_pending*/

    public function orders_agent_waiting()
    {
        $type = 1;
        $title = "Waiting ";
        $id = Auth::guard($this->guardType)->user()->agent_id;
        $areaids = AgentGovernment::where('agent_id', $id)->pluck('government_id')->toArray();
        $orders = Order::where('status', 6)->whereIn('agent_id', [null, "", 0, $id])->whereIn('s_government_id', $areaids)->paginate(15);

        $online_drivers = Driver::where('agent_id', $id)->get();
        return view('agent.PolitOrder.pending', compact('orders', 'title', 'online_drivers', 'type'));
    }/*orders_agent_waiting*/

    public function drop_orders($id)
    {
//        $store = Store::where('status', 1)->where('pickup_default', 1)->first();
        $data = ['status' => 7, 'dropped_at' => Carbon::now()];
//        if ($store) {
//            $data['moved_in'] = $store->id;
//            $data['moved_at'] = Carbon::now();
//        }

        Order::where('status', 0)->where('collection_id', $id)->update($data);

        event(new OrderUpdated([$id], Auth::guard($this->guardType)->user()->id, 1, 'dropped'));

        Flash::success(__('backend.convert_pending_to_dropped'));
        return redirect()->back();

    }/*dropped orders*/

    public function drop_bulk_orders(Request $request)
    {
        $ids = $request->ids;
//        $store = Store::where('status', 1)->where('pickup_default', 1)->first();
        $data = ['status' => 7, 'dropped_at' => Carbon::now()];
//        if ($store) {
//            $data['moved_in'] = $store->id;
//            $data['moved_at'] = Carbon::now();
//        }

        Order::where('status', 0)->whereIn('id', $ids)->update($data);

        event(new OrderUpdated($ids, Auth::guard($this->guardType)->user()->id, 1, 'dropped'));

        Flash::success(__('backend.convert_pending_to_dropped'));

        return __('backend.drop_off_package');

    }/*dropped orders*/

    public function send_to_client(Request $request)
    {
        $ids = $request->ids;
        $customer_id = $request->customer_id;
        $serial_code = $this->randomNumber(2);

        $start_date = Order::whereIn('id', $ids)->min('created_at');
        $end_date = Order::whereIn('id', $ids)->max('created_at');

        $refund_collection = RefundCollection::create([
            'customer_id' => $customer_id,
            'serial_code' => $serial_code,
            'start_date' => date('Y-m-d', strtotime($start_date)),
            'end_date' => date('Y-m-d', strtotime($end_date)),
        ]);

        Order::whereIn('id', $ids)->update([
            'refund_collection_id' => $refund_collection->id,
            'client_dropoff' => 1,
            'client_dropoff_date' => date('Y-m-d H:i:s')
        ]);

        Flash::success(__('backend.send_client_successfully'));

        return $refund_collection->id;

    }/*dropped orders*/

    public function forward_order($order, $agent)
    {
        /**this fn used to forward one order to captain and agent */
        $checkOrder = Order::where('id', $order)->first();
        $checkAgent = Agent::where('id', $agent)->first();
        $forward = ForwardOrder::where('order_id', $order)->where('agent_id', $agent)->first();
        if (!empty($checkOrder) && !empty($checkAgent) && empty($forward)) {

            $forward = new ForwardOrder();
            $forward->agent_id = $agent;
            $forward->order_id = $order;
            $forward->status = 0;
            $forward->save();
            return "true";
        }
        return "false";
    }/*forward order*/

    public function list_by_drivers($key)
    {
        $user = Auth::guard($this->guardType)->user();
        $orders = [];
        $driver = Driver::where('id', $key)->first();
        $orderIds = AcceptedOrder::where('driver_id', $key)->pluck('order_id')->toArray();
        if (count($orderIds)) {
            $orders = Order::whereIn('id', $orderIds)->orderBy('id', 'desc')->paginate(50);
            return view('agent.orders.index', compact('orders', 'driver'));
        }
        return view('agent.orders.index', compact('orders', 'driver'));
    }/*list_orders_by_drivers*/

    public function showOrders($type, $id)
    {
        /** list order by type
         *
         * type == 1 :: captain
         * type == 2 :: corporate
         * type == 3 :: agent
         * type == 4 :: customer
         */

        $user = Auth::guard($this->guardType)->user();

        $sort = app('request')->input('sort');
        if (empty($sort)) {
            $sort = 'currentorders';
        }
        if ($type == 1) {

            $start = app('request')->input('start');
            $end = app('request')->input('end');
            $received_at = app('request')->input('received_at');

            $ownDrivers = Driver::where(function ($query) use ($id) {
                $query->where('id', $id)
                    ->orWhere('manager_id', $id);
            })->pluck('id')->toArray();

            $currentorders = Order::join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
                ->whereIn('accepted_orders.driver_id', $ownDrivers)
                ->where(function ($query) {
                    $query->where(function ($query) {
                        $query->where('orders.status', 5)
                            ->where('accepted_orders.status', 5)
                            ->where(function ($query) {
                                $query->where('orders.warehouse_dropoff', 0)
                                    ->orWhereNull('orders.warehouse_dropoff');
                            });
                    })
                        ->orWhere(function ($query) {
                            $query->where('orders.status', 8)
                                ->where('accepted_orders.status', 8)
                                ->where(function ($query) {
                                    $query->where('orders.warehouse_dropoff', 0)
                                        ->orWhereNull('orders.warehouse_dropoff');
                                });
                        })
                        ->orWhere(function ($query) {
                            $query->where('orders.status', 3)
                                ->where('accepted_orders.status', 3)
                                ->where('delivery_status', 2)
                                ->where(function ($query) {
                                    $query->where('orders.warehouse_dropoff', 0)
                                        ->orWhereNull('orders.warehouse_dropoff');
                                });
                        })
                        ->orWhere(function ($query) {
                            $query->where('orders.status', 3)
                                ->where('accepted_orders.status', 3)
                                ->where('delivery_status', 4)
                                ->where(function ($query) {
                                    $query->where('orders.warehouse_dropoff', 0)
                                        ->orWhereNull('orders.warehouse_dropoff');
                                });
                        })
                        ->orWhere(function ($query) {
                            $query->where('orders.status', 1)
                                ->where(function ($query) {
                                    $query->where('orders.warehouse_dropoff', 0)
                                        ->orWhereNull('orders.warehouse_dropoff');
                                });
                        })
                        ->orWhere(function ($query) {
                            $query->where('orders.status', 2)
                                ->where('accepted_orders.status', 2);
                        });
                })
                ->when($received_at, function ($query, $received_at) {
                    return $query->whereDate('received_at', $received_at)
                        ->where('status', 2);
                })
                ->select('orders.*')
                ->orderBy('orders.id', 'desc');

            $orders = Order::join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
                ->whereIn('accepted_orders.driver_id', $ownDrivers)
                ->where(function ($query) {
                    $query->where(function ($query) {
                        $query->where('orders.status', 5)
                            ->where('accepted_orders.status', 5)
                            ->where('orders.warehouse_dropoff', 1);
                    })
                        ->orWhere(function ($query) {
                            $query->where('orders.status', 8)
                                ->where('accepted_orders.status', 8)
                                ->where('orders.warehouse_dropoff', 1);
                        })
                        ->orWhere(function ($query) {
                            $query->where('orders.status', 3)
                                ->where('accepted_orders.status', 3)
                                ->where('delivery_status', 2)
                                ->where('orders.warehouse_dropoff', 1);
                        })
                        ->orWhere(function ($query) {
                            $query->where('orders.status', 3)
                                ->where('accepted_orders.status', 3)
                                ->where('delivery_status', 4)
                                ->where('orders.warehouse_dropoff', 1);
                        })
                        ->orWhere(function ($query) {
                            $query->where('orders.status', 3)
                                ->where('accepted_orders.status', 3)
                                ->where(function ($query) {
                                    $query->whereNull('delivery_status')
                                        ->orWhere('delivery_status', 1);

                                });
                        });
                })
                ->select('orders.*')
                ->orderBy('orders.id', 'desc');

//            $ids = AcceptedOrder::where('driver_id', $id)
//                ->where('status', '!=', '4')
//                ->when($start, function ($query, $start) {
//                    return $query->where('created_at', '>=', $start);
//                })->when($end, function ($query, $end) {
//                    return $query->where('created_at', '<=', $end);
//                })->pluck('order_id')->toArray();
//
//            $currentorders = Order::whereIn('orders.id', $ids)
//                ->where(function ($query) {
//                    $query->whereIn('status', [1, 2])
//                        ->orWhere(function ($query) {
//                            $query->where('status', 5)
//                                ->where('warehouse_dropoff', 0);
//                        })
//                        ->orWhere(function ($query) {
//                            $query->where('status', 4)
//                                ->where('status_before_cancel', '!=', 0)
//                                ->where('warehouse_dropoff', 0);
//                        });
//                })
//                ->when($received_at, function ($query, $received_at) {
//                    return $query->whereDate('received_at', $received_at)
//                        ->where('status', 2);
//                })
//                ->select('orders.*')
//                ->orderBy('orders.id', 'desc');

//            $orders = Order::whereIn('orders.id', $ids)
//                ->where(function ($query) {
//                    $query->whereIn('status', [3])
//                        ->orWhere(function ($query) {
//                            $query->where('status', 5)
//                                ->where('warehouse_dropoff', 1);
//                        })
//                        ->orWhere(function ($query) {
//                            $query->where('status', 4)
//                                ->where('status_before_cancel', '!=', 0)
//                                ->where('warehouse_dropoff', 1);
//                        });
//                })
//                ->select('orders.*')
//                ->orderBy('orders.id', 'desc');

            if ($user->is_warehouse_user) {
                $currentorders = $currentorders->join('store_user', 'store_user.store_id', '=', 'orders.moved_in')
                    ->where('user_id', $user->id);

                $orders = $orders->join('store_user', 'store_user.store_id', '=', 'orders.moved_in')
                    ->where('user_id', $user->id);
            }

            $currentorders = $currentorders->paginate(50, ['*'], 'current_page');

            $orders = $orders->paginate(50, ['*'], 'finished_page');

//            $recalled_orders = Order::whereIn('id', $ids)
//                ->where('status', 5)
//                ->orderBy('id', 'desc')
//                ->paginate(50, ['*'], 'recall_page');

            $driver = Driver::findOrFail($id);

            if (Auth::guard($this->guardType)->user()->role_id == 1) {
                return view('backend.drivers.showOrders', compact('orders', 'sort', 'currentorders', 'id', 'driver'/*, 'recalled_orders'*/));

            } elseif (Auth::guard($this->guardType)->user()->role_id == 2) {

                return view('agent.orders.showorders', compact('orders', 'sort', 'currentorders', 'id'/*, 'recalled_orders'*/));
            }
        } elseif ($type == 2) {

            $ids = Customer::where('corporate_id', $id)->pluck('id')->toArray();

            $currentorders = Order::whereIn('orders.customer_id', $ids)
                ->where(function ($query) {
                    $query->whereIn('status', [0, 1, 2, 7])
                        ->orWhere(function ($query) {
                            $query->where('status', 5)
                                ->where('client_dropoff', 0);
                        })
                        ->orWhere(function ($query) {
                            $query->where('status', 8)
                                ->where('client_dropoff', 0);
                        })
                        ->orWhere(function ($query) {
                            $query->where('status', 4)
                                ->where('status_before_cancel', '!=', 0)
                                ->where('client_dropoff', 0);
                        });
                })
                ->orderBy('orders.id', 'desc');

            $orders = Order::whereIn('orders.customer_id', $ids)
                ->where(function ($query) {
                    $query->whereIn('status', [3])
                        ->orWhere(function ($query) {
                            $query->where('status', 5)
                                ->where('client_dropoff', 1);
                        })
                        ->orWhere(function ($query) {
                            $query->where('status', 8)
                                ->where('client_dropoff', 1);
                        })
                        ->orWhere(function ($query) {
                            $query->where('status', 4)
                                ->where('status_before_cancel', '!=', 0)
                                ->where('client_dropoff', 1);
                        })
                        ->orWhere(function ($query) {
                            $query->where('status', 4)
                                ->where('status_before_cancel', 0);
                        });
                })
                ->orderBy('orders.id', 'desc');

            if ($user->is_warehouse_user) {
                $currentorders = $currentorders->join('store_user', 'store_user.store_id', '=', 'orders.moved_in')
                    ->where('user_id', $user->id);

                $orders = $orders->join('store_user', 'store_user.store_id', '=', 'orders.moved_in')
                    ->where('user_id', $user->id);
            }

            $currentorders = $currentorders->paginate(50, ['*'], 'current_page');

            $orders = $orders->paginate(50, ['*'], 'finished_page');

            //            $recalled_orders = Order::whereIn('customer_id', $ids)->where('status', 5)->orderBy('id', 'desc')->paginate(50, ['*'], 'recall_page');

            $corporate = Corporate::where('id', '=', $id)->first();
            return view('backend.corporates.showOrders', compact('orders', 'sort', 'id', 'corporate', 'currentorders'/*, 'recalled_orders'*/));
        } elseif ($type == 3) {

            $orders = AcceptedOrder::join('orders', 'orders.id', '=', 'accepted_orders.order_id')->
            join('drivers', 'accepted_orders.driver_id', '=', 'drivers.id')->
            join('agents', 'drivers.agent_id', '=', 'agents.id')->
            select('accepted_orders.delivery_price', 'received_date', 'drivers.name AS driver_name', 'drivers.mobile AS driver_mobile', 'accepted_orders.status AS accepted_orders_status', 'orders.type', 'orders.sender_name', 'orders.order_price', 'orders.order_number', 'orders.status AS order_status', 'orders.receiver_name', 'orders.receiver_code', 'orders.id AS order_id', 'agents.name', 'agents.mobile')->
            where('agents.id', $id)->
            get();
            $agent = Agent::findOrFail($id);
            return view('backend.agents.showOrders', compact('orders', 'sort', 'id', 'agent'));

        } elseif ($type == 4) {

            /*$orders = AcceptedOrder::join('orders', 'orders.id', '=', 'accepted_orders.order_id')
            ->join('drivers', 'accepted_orders.driver_id', '=', 'drivers.id')
            ->join('customers', 'orders.customer_id', '=', 'customers.id')
            ->join('corporates', 'customers.corporate_id', '=', 'corporates.id')
            ->select('accepted_orders.delivery_price', 'received_date', 'drivers.name AS driver_name', 'drivers.mobile AS driver_mobile', 'accepted_orders.status AS accepted_orders_status' , 'orders.type', 'orders.sender_name' ,'orders.order_price','orders.order_number' ,'orders.status AS order_status', 'orders.receiver_name' ,'orders.receiver_code', 'orders.id AS order_id', 'customers.name', 'customers.mobile')
            ->where('orders.customer_id', $id)->get();
            */

            $currentorders = Order::where('orders.customer_id', $id)
                ->where(function ($query) {
                    $query->whereIn('status', [0, 1, 2, 7])
                        ->orWhere(function ($query) {
                            $query->where('status', 5)
                                ->where('client_dropoff', 0);
                        })
                        ->orWhere(function ($query) {
                            $query->where('status', 8)
                                ->where('client_dropoff', 0);
                        })
                        ->orWhere(function ($query) {
                            $query->where('status', 4)
                                ->where('status_before_cancel', 7)
                                ->where('client_dropoff', 0);
                        });
                })
                ->orderBy('orders.id', 'desc');

            $orders = Order::where('orders.customer_id', $id)
                ->where(function ($query) {
                    $query->whereIn('status', [3])
                        ->orWhere(function ($query) {
                            $query->where('status', 5)
                                ->where('client_dropoff', 1);
                        })
                        ->orWhere(function ($query) {
                            $query->where('status', 8)
                                ->where('client_dropoff', 1);
                        })
                        ->orWhere(function ($query) {
                            $query->where('status', 4)
                                ->where('status_before_cancel', '!=', 0)
                                ->where('client_dropoff', 1);
                        })
                        ->orWhere(function ($query) {
                            $query->where('status', 4)
                                ->where('status_before_cancel', 0);
                        });
                })
                ->orderBy('orders.id', 'desc');
//            $recalled_orders = Order::where('customer_id', $id)->where('status', 5)->orderBy('id', 'desc')->paginate(50, ['*'], 'recall_page');

            if ($user->is_warehouse_user) {
                $currentorders = $currentorders->join('store_user', 'store_user.store_id', '=', 'orders.moved_in')
                    ->where('user_id', $user->id);

                $orders = $orders->join('store_user', 'store_user.store_id', '=', 'orders.moved_in')
                    ->where('user_id', $user->id);
            }

            $currentorders = $currentorders->paginate(50, ['*'], 'current_page');

            $orders = $orders->paginate(50, ['*'], 'finished_page');

            $customer = Customer::findOrFail($id);
            $corporate = Corporate::where('id', '=', $customer->corporate_id)->first();
            return view('backend.customers.showOrders', compact('orders', 'sort', 'id', 'customer', 'corporate', 'currentorders'/*, 'recalled_orders'*/));
        }
    }/*list Orders by client type (captain,customer,corporates,agent)*/

    public function list_suggested()
    {
        $user = Auth::guard($this->guardType)->user();
        $orders = [];
        $orderIds = ForwardOrder::where('agent_id', $user->agent_id)->pluck('order_id')->toArray();
        if (count($orderIds)) {
            $orders = Order::whereIn('id', $orderIds)->orderBy('id', 'desc')->paginate(50);
            return view('agent.orders.index', compact('orders'));
        }
        return view('agent.orders.index', compact('orders'));
    }/*list_suggested*/

    public function index(Request $request)
    {

        $user = Auth::guard($this->guardType)->user();
        $type = app('request')->input('type');
        $id = app('request')->input('customer');
        $excel = app('request')->input('export');

//        $acceptedIds = AcceptedOrder::pluck('order_id')->toArray();
//        $rejectedIds = AcceptedOrder::where('status', 4)->pluck('order_id')->toArray();
        $date = app('request')->input('date');
        $from_date = app('request')->input('from_date');
        $to_date = app('request')->input('to_date');
        $corporate_id = app('request')->input('corporate_id');
        $search = app('request')->input('search');
        $collection = app('request')->input('collection');
        $move_collection = app('request')->input('move_collection');
        $refund_collection = app('request')->input('refund_collection');
        $r_state_id = app('request')->input('r_state_id');
        $s_state_id = app('request')->input('s_state_id');
        $r_government_id = app('request')->input('r_government_id');
        $s_government_id = app('request')->input('s_government_id');
        $dropped_in = app('request')->input('dropped_in');
        $allStores = Store::where('status', 1);

        if ($user->is_warehouse_user) {
            $allStores = $allStores->join('store_user', 'store_user.store_id', '=', 'stores.id')
                ->where('user_id', $user->id);
        }

        $allStores = $allStores->get();
        $corporates = Corporate::where('is_active', 1)
            ->orderBy('corporates.id', 'desc')
            ->select('corporates.*');

        $online_drivers = Driver::where('is_active', 1)
            ->orderBy('drivers.id', 'desc')
            ->select('drivers.*');

        $pickup_store = Store::where('pickup_default', 1)->first();

        if ($user->is_warehouse_user) {
            $corporates = $corporates->join('customers', 'customers.corporate_id', '=', 'corporates.id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id);

            $online_drivers = $online_drivers->join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                ->where('user_id', $user->id);
        }

        $corporates = $corporates->get();
        $online_drivers = $online_drivers->get();

        $orders = Order::with(['customer', 'warehouse' => function ($query) {
            $query->with(['responsibles' => function ($query) {
                $query->where('is_responsible', 1);
            }]);
        }, 'to_government', 'from_government'])->select('orders.*')->orderBy('orders.id', 'desc');

        if ($user->is_warehouse_user && !$move_collection) {
            $orders = $orders->join('store_user', 'store_user.store_id', '=', 'orders.moved_in')
                ->where('user_id', $user->id);
        }

        if ($id) {
            $orders = $orders->where('customer_id', $id);

        } else if (!empty($move_collection) && $move_collection > 0) {
            $orders = $orders->join('move_collection_orders', 'move_collection_orders.order_id', '=', 'orders.id')
                ->where('move_collection_orders.collection_id', $move_collection)
                ->select('orders.*', 'dropped');

            if ($user->is_warehouse_user) {
                $orders = $orders->join('move_collections', 'move_collections.id', '=', 'move_collection_orders.collection_id')
                    ->join('store_user', 'store_user.store_id', '=', 'move_collections.store_id')
                    ->where('user_id', $user->id);
            }

            $type = 'all';
            $orders = $orders->paginate(50);
            return view('backend.orders.index', compact('orders', 'type', 'move_collection', 'collection', 'corporates'));
        } else if (!empty($refund_collection) && $refund_collection > 0) {
            $orders = $orders->where('orders.refund_collection_id', $refund_collection)
                ->select('orders.*');

            $type = 'all';
            $orders = $orders->paginate(50);
            return view('backend.orders.index', compact('orders', 'type', 'refund_collection', 'collection', 'corporates'));
        } else if (!empty($collection) && $collection > 0) {

            $orders = $orders->where('collection_id', $collection);

            if (isset($excel) && $excel == "true") {
                $title = "Orders";
                $queryBuilder = $orders->join('governorates', 'governorates.id', '=', 'orders.r_government_id')
                    ->join('customers', 'customers.id', '=', 'orders.customer_id')
                    ->join('payment_methods', 'payment_methods.id', '=', 'orders.payment_method_id')
                    ->join('order_types', 'order_types.id', '=', 'orders.order_type_id')
                    ->join('corporates', 'corporates.id', '=', 'customers.corporate_id')
                    ->join('cities', 'cities.id', '=', 'orders.r_state_id')
                    ->select('governorates.name_en as governorate', 'orders.order_type_id', 'orders.payment_method_id', 'order_types.name_en as ordertype', 'payment_methods.name as payment', 'cities.name_en as city', 'orders.r_government_id', 'orders.id', 'orders.order_no', 'orders.delivery_price', 'corporates.name as corporatename', 'customers.name as customername', 'customers.corporate_id', 'orders.receiver_address', 'orders.receiver_mobile', 'orders.notes', 'orders.order_price', 'orders.order_number', 'orders.status', 'orders.receiver_name', 'orders.receiver_code', 'orders.overload', 'orders.created_at')->orderBy('orders.id', 'DESC');

                $columns = [
                    'ID' => 'id',
                    'Order No#' => 'order_no',
                    'Corporate Name' => 'corporatename',
                    'Account Name' => 'customername',
                    'Sender Code' => 'order_number',

                    'Created At' => 'created_at',
                    'Weight' => 'overload',

                    'Order Price' => 'order_price',
                    'Fees' => 'delivery_price',
                    'Total Price' => function ($order) {
                        return $order->order_price + $order->delivery_price;
                    },

                    'Payment Method' => 'payment',
                    'Order Type' => 'ordertype',


                    'Status' => function ($order) {
                        if ($order->status == 0) {
                            return __('backend.pending');
                        } else if ($order->status == 1) {
                            return __('backend.accepted');
                        } else if ($order->status == 2) {
                            return __('backend.received');
                        } else if ($order->status == 3) {
                            return __('backend.delivered');
                        } else if ($order->status == 4) {
                            return __('backend.cancelled');
                        } else if ($order->status == 5) {
                            return __('backend.recalled');
                        } else if ($order->status == 6) {
                            return __('backend.waiting');
                        } else if ($order->status == 7) {
                            return __('backend.dropped');
                        } else if ($order->status == 8) {
                            return __('backend.rejected');
                        }

                    },
                    'Customer Name' => 'receiver_name',
                    'Phone Number' => 'receiver_mobile',
                    'Receiver Government' => 'governorate',
                    'Receiver City' => 'city',
                    'Notes' => 'notes',
                    'Address' => 'receiver_address',


                ];

                $meta = ["Excell For " => "Collection No#" . $collection];
                return ExcelReport::of($title, $meta, $queryBuilder, $columns)
                    ->showTotal([
                        'Order Price' => 'point',
                        'Fees' => 'point',
                        'Total Price' => 'point'
                    ])->setCss([
                        '.bolder' => 'font-weight: 800;',
                        '.italic-red' => 'color: red;font-style: italic;'
                    ])->download('orders-' . date('Y-m-d'));
            }
            $type = 'all';
            $orders = $orders->paginate(50);
            return view('backend.orders.index', compact('orders', 'type', 'move_collection', 'collection', 'corporates'));

        } else {

            if ($corporate_id) {
                $orders = $orders->join('customers', 'customers.id', '=', 'orders.customer_id')
                    ->where('customers.corporate_id', $corporate_id);
            }

            if ($from_date) {
                $orders = $orders->whereDate('created_at', '>=', $from_date);
            }

            if ($to_date) {
                $orders = $orders->whereDate('created_at', '<=', $to_date);
            }

            if ($type == 'pending') {
                $orders = $orders->where('orders.status', 0);
            } else if ($type == 'waiting') {

                $todaydate = Carbon::now()->subDay(5)->toDateTimeString();
                $orders = $orders->where('orders.status', 6)->where('created_at', '>=', $todaydate);

            } else if ($type == 'expired_waiting') {

                $todaydate = Carbon::now()->subDay(5)->toDateTimeString();
                $orders = $orders->where('orders.status', 6)->where('created_at', '<', $todaydate);

            } else if ($type == 'accept') {

                $orders = $orders->where('orders.status', 1);

            } else if ($type == 'receive') {
                $orders = $orders->where('orders.status', 2);

            } else if ($type == 'deliver') {
                $orders = $orders->where('orders.status', 3)
                    ->where('orders.is_refund', 0);

            } else if ($type == 'cancel') {
                $orders = $orders->where('orders.status', 4)
                    ->where(function ($query) {
                        $query->where('orders.status_before_cancel', 0)
                            ->orWhereNull('orders.status_before_cancel');
                    });


            } else if ($type == 'recall') {
                $orders = $orders->where(function ($query) {
                    $query->where('orders.status', 5)
                        ->Orwhere('orders.status', 8)
                        ->orWhere(function ($query) {
                            $query->where('orders.status', 3)
                                ->where('delivery_status', 2);
                        })
                        ->orWhere(function ($query) {
                            $query->where('orders.status', 3)
                                ->where('delivery_status', 4);
                        })
                        ->orWhere(function ($query) {
                            $query->where('orders.status', 4)
                                ->whereNotNull('status_before_cancel')
                                ->where('status_before_cancel', '!=', 0);
                        });
                });

            } else if ($type == 'dropped') {
                $orders = $orders->where('orders.status', 7);
            }

            if (isset($s_state_id) && $s_state_id != "") {
                $orders = $orders->where('s_state_id', $s_state_id);
            }
            if (isset($dropped_in) && $dropped_in == "warehouse_dropoff") {
                $orders = $orders->where('warehouse_dropoff', 1);
            } elseif (isset($dropped_in) && $dropped_in == "client_dropoff") {
                $orders = $orders->where('client_dropoff', 1);
            }

            if (isset($s_government_id) && $s_government_id != "") {
                $orders = $orders->where('s_government_id', $s_government_id);
            }
            if (isset($r_state_id) && $r_state_id != "") {
                $orders = $orders->where('r_state_id', $r_state_id);
            }
            if (isset($r_government_id) && $r_government_id != "") {
                $orders = $orders->where('r_government_id', $r_government_id);
            }

            if (isset($search) && $search != "") {
                $orders = $orders->where(function ($q) use ($search) {

                    $q->where('orders.id', 'like', '%' . $search . '%');
                    $q->OrWhere('order_number', 'like', '%' . $search . '%');
                    $q->OrWhere('receiver_name', 'like', '%' . $search . '%');
                    $q->OrWhere('receiver_mobile', 'like', '%' . $search . '%');
                    $q->orWhere('sender_mobile', 'like', '%' . $search . '%');
                    $q->orWhere('sender_name', 'like', '%' . $search . '%');
                    $q->orWhere('order_no', 'like', '%' . $search . '%');

                });
            }
            if (isset($date) && $date != "") {
                $orders = $orders->where('created_at', 'like', $date . '%');
            }

            if (isset($excel) && $excel == "true") {
                $title = "Orders";
                $meta = ["Excell For " => " Search .. "];
                $queryBuilder = $orders->join('governorates', 'governorates.id', '=', 'orders.r_government_id')
                    ->join('customers', 'customers.id', '=', 'orders.customer_id')
                    ->join('payment_methods', 'payment_methods.id', '=', 'orders.payment_method_id')
                    ->join('order_types', 'order_types.id', '=', 'orders.order_type_id')
                    ->join('corporates', 'corporates.id', '=', 'customers.corporate_id')
                    ->join('cities', 'cities.id', '=', 'orders.r_state_id')
                    ->select('governorates.name_en as governorate', 'orders.order_type_id', 'orders.payment_method_id', 'order_types.name_en as ordertype', 'payment_methods.name as payment', 'cities.name_en as city', 'orders.r_government_id', 'orders.id', 'orders.order_no', 'orders.delivery_price', 'corporates.name as corporatename', 'customers.name as customername', 'customers.corporate_id', 'orders.receiver_address', 'orders.receiver_mobile', 'orders.notes', 'orders.order_price', 'orders.order_number', 'orders.status', 'orders.receiver_name', 'orders.receiver_code', 'orders.overload', 'orders.created_at')->orderBy('orders.id', 'DESC');

                $columns = [
                    'ID' => 'id',
                    'Order No#' => 'order_no',
                    'Corporate Name' => 'corporatename',
                    'Account Name' => 'customername',
                    'Sender Code' => 'order_number',

                    'Created At' => 'created_at',
                    'Weight' => 'overload',

                    'Order Price' => 'order_price',
                    'Fees' => 'delivery_price',
                    'Total Price' => function ($order) {
                        return $order->order_price + $order->delivery_price;
                    },

                    'Payment Method' => 'payment',
                    'Order Type' => 'ordertype',


                    'Status' => function ($order) {
                        if ($order->status == 0) {
                            return __('backend.pending');
                        } else if ($order->status == 1) {
                            return __('backend.accepted');
                        } else if ($order->status == 2) {
                            return __('backend.received');
                        } else if ($order->status == 3) {
                            return __('backend.delivered');
                        } else if ($order->status == 4) {
                            return __('backend.cancelled');
                        } else if ($order->status == 5) {
                            return __('backend.recalled');
                        } else if ($order->status == 6) {
                            return __('backend.waiting');
                        } else if ($order->status == 7) {
                            return __('backend.dropped');
                        } else if ($order->status == 8) {
                            return __('backend.rejected');
                        }
                    },
                    'Customer Name' => 'receiver_name',
                    'Phone Number' => 'receiver_mobile',
                    'Receiver Government' => 'governorate',
                    'Receiver City' => 'city',
                    'Notes' => 'notes',
                    'Address' => 'receiver_address',


                ];
                return ExcelReport::of($title, $meta, $queryBuilder, $columns)
                    ->showTotal([
                        'Order Price' => 'point',
                        'Fees' => 'point',
                        'Total Price' => 'point'
                    ])->setCss([
                        '.bolder' => 'font-weight: 800;',
                        '.italic-red' => 'color: red;font-style: italic;'
                    ])->download('orders-' . date('Y-m-d'));
            }

            $orders = $orders->orderBy('orders.id', 'desc')->paginate(50);

            if ($request->ajax()) {

                $agents = Agent::all();

                return [
                    'view' => view('backend.orders.table', compact('orders', 'type', 'online_drivers', 'agents'))->render(),
//                    'js' => view('backend.orders.js-search', compact('type', 'online_drivers', 'agents'))->render(),
                ];
            }
            if ($type == 'dropped' || $type == 'cancel' || $type == 'recall' || $type == 'waiting' || $type == 'all') {

                $stores = Store::where('status', 1);

//                if ($user->is_warehouse_user) {
//                    $stores = $stores->join('store_user', 'store_user.store_id', '=', 'stores.id')
//                        ->where('user_id', $user->id);
//                }

                $stores = $stores->get();
                $agents = Agent::all();

                return view('backend.orders.index', compact('orders', 'type', 'online_drivers', 'agents', 'allStores', 'pickup_store', 'stores', 'corporates'));
            }
        }
        return view('backend.orders.index', compact('orders', 'type', 'allStores', 'pickup_store', 'corporates', 'online_drivers'));
    }/* list orders index*/

    public function all_orders(Request $request)
    {
        $user = Auth::guard($this->guardType)->user();

        $status = app('request')->input('status');
        $corporate_id = app('request')->input('corporate_id');
        $create_at = app('request')->input('date');
        $r_government_id = app('request')->input('r_government_id');
        $s_government_id = app('request')->input('s_government_id');
        $s_state_id = app('request')->input('s_state_id');
        $r_state_id = app('request')->input('r_state_id');
        $receiver_mobile = app('request')->input('receiver_mobile');
        $receiver_phone = app('request')->input('receiver_phone');
        $r_state_id = app('request')->input('r_state_id');
        $search = app('request')->input('search');
        $type = '';

        $orders = Order::with(['customer', 'warehouse' => function ($query) {
            $query->with(['responsibles' => function ($query) {
                $query->where('is_responsible', 1);
            }]);
        }, 'to_government', 'from_government'])->orderBy('orders.id', 'desc');

        if ($user->is_warehouse_user) {
            $orders = $orders->join('store_user', 'store_user.store_id', '=', 'orders.moved_in')
                ->where('user_id', $user->id);
        }

        if (isset($search) && $search != "") {
            $orders = $orders->where(function ($q) use ($search) {
                $q->where('orders.id', 'like', '%' . $search . '%');
                $q->OrWhere('order_number', 'like', '%' . $search . '%');
                $q->OrWhere('receiver_name', 'like', '%' . $search . '%');
                $q->OrWhere('receiver_mobile', 'like', '%' . $search . '%');
                $q->orWhere('sender_mobile', 'like', '%' . $search . '%');
                $q->orWhere('sender_name', 'like', '%' . $search . '%');
                $q->orWhere('order_no', 'like', '%' . $search . '%');
                $q->orWhere('reference_number', 'like', '%' . $search . '%');
            });
        }

        if ($status || $status === '0') {
            $orders->where('orders.status', $status);
        }

        if ($corporate_id) {
            $orders->join('customers', 'customers.id', '=', 'orders.customer_id')
                ->where('customers.corporate_id', $corporate_id);
        }

        if ($receiver_mobile) {
            $orders->where(function ($query) use ($receiver_mobile) {
                $query->where('receiver_mobile', $receiver_mobile)
                    ->orWhere('receiver_phone', $receiver_mobile);
            });
        }

        if ($receiver_phone) {
            $orders->where(function ($query) use ($receiver_phone) {
                $query->where('receiver_mobile', $receiver_phone)
                    ->orWhere('receiver_phone', $receiver_phone);
            });
        }

        if ($create_at) {
            $orders->whereDate('orders.created_at', $create_at);
        }

        if ($r_government_id) {
            $orders->where('r_government_id', $r_government_id);
        }

        if ($s_government_id) {
            $orders->where('s_government_id', $s_government_id);
        }

        if ($s_state_id) {
            $orders->where('s_state_id', $s_state_id);
        }

        if ($r_state_id) {
            $orders->where('r_state_id', $r_state_id);
        }

        $orders = $orders->select('orders.*')->paginate(50);

        $corporates = Corporate::select('corporates.*')->orderBy('corporates.id', 'desc');

        if ($user->is_warehouse_user) {

            $corporates = $corporates->join('customers', 'customers.corporate_id', '=', 'corporates.id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id);
        }

        $corporates = $corporates->get();

        if (app('request')->has('status')) {
            if ($status === '0') {
                $type = 'pending';
            } else if ($status == 1) {
                $type = 'accept';
            } else if ($status == 2) {
                $type = 'O.F.D';
            } else if ($status == 3) {
                $type = 'deliver';
            } else if ($status == 4) {
                $type = 'cancel';
            } else if ($status == 5) {
                $type = 'recall';
            } else if ($status == 6) {
                $type = 'waiting';
            } else if ($status == 7) {
                $type = 'dropped';
            } else if ($status == 8) {
                $type = 'rejected';
            }

        }

        return view('backend.orders.all_orders', compact('orders', 'corporates', 'type', 'corporate_id', 'create_at', 'r_government_id', 's_government_id'));
    }

    public function order_log(Request $request)
    {
        $user = Auth::guard($this->guardType)->user();

        $user_id = app('request')->input('user_id');
        $create_at = app('request')->input('date');
        $search = app('request')->input('search');

        $users = User::where('role_id', 1)->where('is_active', 1)->select('users.*');

        if ($user->is_warehouse_user) {
            $users = $users->join('store_user', 'store_user.user_id', '=', 'users.id')
                ->where('store_user.user_id', $user->id);
        }

        $users = $users->get();

        $activities = Activity::join('orders', 'orders.id', '=', 'activity_log.subject_id')
            ->with(['causer'])
            ->where('subject_type', 'App\Models\Order')
            ->where('causer_type', 'App\Models\User')
            ->orderBy('activity_log.created_at', 'desc');

        if ($user->is_warehouse_user) {
            $activities = $activities->join('store_user', 'store_user.store_id', '=', 'orders.moved_in')
                ->where('user_id', $user->id);
        }

        if (isset($search) && $search != "") {
            $activities = $activities->where(function ($q) use ($search) {
                $q->where('orders.id', 'like', '%' . $search . '%');
                $q->OrWhere('order_number', 'like', '%' . $search . '%');
                $q->OrWhere('receiver_name', 'like', '%' . $search . '%');
                $q->OrWhere('receiver_mobile', 'like', '%' . $search . '%');
                $q->orWhere('sender_mobile', 'like', '%' . $search . '%');
                $q->orWhere('sender_name', 'like', '%' . $search . '%');
                $q->orWhere('order_no', 'like', '%' . $search . '%');
                $q->orWhere('reference_number', 'like', '%' . $search . '%');
            });
        }

        if ($user_id) {
            $activities->where('causer_id', $user_id);
        }

        if ($create_at) {
            $activities->whereDate('activity_log.created_at', $create_at);
        }

        $activities = $activities->select('activity_log.*', 'orders.order_number', 'orders.id as order_id')->paginate(50);

        return view('backend.orders.order_log', compact('activities', 'users'));
    }

    public function cancelOrder($id)
    {
        $order = Order::findOrFail($id);
        if (isset($order)) {
            AcceptedOrder::where('order_id', $id)->update(['status' => 4]);
            Flash::error(__('backend.Order_Canceled_after_Receiving'));
            return redirect()->back();
        } else {
            Flash::error(__('backend.Somthing_went_Wrong_Please_try_again'));
            return redirect()->route('mngrAdmin.orders.index');
        }
    }/* cancelOrder */

    public function recall_order_operation($id, $type)
    {
        $order = Order::findOrFail($id);
        AcceptedOrder::where('order_id', $id)->where('status', 2)->update(['status' => 5]);
        $order->status = 5;

        $order->recalled_at = Carbon::now();
        $order->recalled_by = $type;
        if ($order->save()) {
            event(new OrderUpdated([$id], Auth::guard($this->guardType)->user()->id, 1, 'recall', ['recalled_by' => $type]));
            return true;
        } else {
            return false;
        }
    }

    /** recall_order_operation */

    public function recallOrder(Request $request)
    {
        $id = $request->order_id;
        $recall_by = $request->recall_by;
        $order = Order::findOrFail($id);
        if (isset($order)) {
            if (in_array($recall_by, [1, 2, 3])) {
                $this->recall_order_operation($id, $recall_by);
            } else {
                Flash::error(__('backend.Somthing_went_Wrong_Please_try_again'));
                return response()->json(false);
//                return redirect()->route('mngrAdmin.orders.index', compact('message'));
            }

//            if ($recall_by == 1) {
//                //check admin password and confirm recall
//                if (password_verify($request->password, Auth::guard('admin')->user()->password) == true) {
//                    $result = $this->recall_order_operation($id, $recall_by);
//                } else {
//                    //wrong password
//                    Flash::error(__('backend.Somthing_went_Wrong_Please_try_again'));
//                    return redirect()->route('mngrAdmin.orders.index', compact('message'));
//                }
//            } elseif ($recall_by == 2) {
//                $mobile = $order->receiver_mobile;
//            } elseif ($recall_by == 3) {
//
//                $mobile = $order->sender_mobile;
//            } else {
//                Flash::error(__('backend.Somthing_went_Wrong_Please_try_again'));
//                return redirect()->route('mngrAdmin.orders.index', compact('message'));
//            }

//            if ($recall_by == 2 || $recall_by == 3) {
//                $verifyCode = VerifyCode::where('object_id', $order->id)
//                    ->where('object_type', '1')->where('mobile', $mobile)
//                    ->where('status', 0)->where('valid_to_date', "<=", Carbon::now())->where('code', $request->code)->first();
//                if ($verifyCode == null) {
//                    //wrong code
//                    Flash::error(__('backend.Somthing_went_Wrong_Please_try_again'));
//                    return redirect()->route('mngrAdmin.orders.index', compact('message'));
//                } else {
//                    $result = $this->recall_order_operation($id, $recall_by);
//                }
//            }

            //recalled successfully
            Flash::error(__('backend.Order_Canceled_after_Receiving'));
            return response()->json(true);
//            return redirect()->back();

        } else {

            Flash::error(__('backend.Somthing_went_Wrong_Please_try_again'));
            return response()->json(false);
//            return redirect()->route('mngrAdmin.orders.index', compact('message'));

        }
    } /* recallOrder By Admin */


    public function send_cofirm_recall_sms(Request $request)
    {
        $provider = 'smsmisr';
        $result = "false";
        $order = Order::findOrFail($request->order_id);
        if (isset($order)) {
            $code = mt_rand(1000, 9999);
            $message = "Confirm Code To Re-call Your Order No#" . $order->order_no . " : is " . $code;
            //recalby ==2 :: mean receiver want to recall this order
            if ($request->recall_by == 2) {
                $mobile = $order->receiver_mobile;
                //save code
                VerifyCode::where('object_id', $order->id)->where('object_type', '1')->where('mobile', $mobile)->update(['status' => 1]);
                $verify_code = new VerifyCode();
                $verify_code->code = $code;
                $verify_code->valid_to_date = Carbon::now()->addMinutes(20);
                $verify_code->mobile = $mobile;
                $verify_code->object_id = $order->id;
                $verify_code->object_type = "1";
                $verify_code->status = 0;
                $verify_code->save();
                $result = (new SmsController())->send($provider, $mobile, $message);
            } elseif ($request->recall_by == 3) {
                $mobile = $order->sender_mobile;
                VerifyCode::where('object_id', $order->id)->where('object_type', '1')->where('mobile', $mobile)->update(['status' => 1]);
                $verify_code = new VerifyCode();
                $verify_code->code = $code;
                $verify_code->valid_to_date = Carbon::now()->addMinutes(20);
                $verify_code->mobile = $mobile;
                $verify_code->object_id = $order->id;
                $verify_code->object_type = "1";
                $verify_code->status = 0;
                $verify_code->save();
                $result = (new SmsController())->send($provider, $mobile, $message);
            } else {
                $result = "false";
            }
        }
        return $result;

    }/*send_cofirm_recall_sms*/

    public function forward_recall_view($id)
    {
        $order = Order::findOrFail($id);
        $user = Auth::guard($this->guardType)->user();
        $governments = Governorate::orderBy('name_en', 'ASC')->get();
        $agents = Agent::orderBy('name', 'ASC')->get();
        #payment_methods
        $payments = PaymentMethod::where('status', 1)->orderBy('name', 'ASC')->get();
        $types = OrderType::orderBy('name_en', 'ASC')->get();
        $allStores = Store::where('status', 1);

        if ($user->is_warehouse_user) {
            $allStores = $allStores->join('store_user', 'store_user.store_id', '=', 'stores.id')
                ->where('user_id', $user->id);
        }

        $allStores = $allStores->get();
        $status = app('request')->input('type');
        $Corporate_ids = Corporate::where('is_active', 1)->select('corporates.*')->orderBy('corporates.name', 'ASC');

        if ($user->is_warehouse_user) {
            $Corporate_ids = $Corporate_ids->join('customers', 'customers.corporate_id', '=', 'corporates.id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id);
        }

        $Corporate_ids = $Corporate_ids->get();

        $s_cities = [];
        $r_cities = [];
        $customers = [];
        $stores = [];
        $drivers = [];
        if (!empty($order) && $order != null) {
            $collection_id = $order->collection_id;
            if ($order->agent_id > 0) {
                $drivers = Driver::where('agent_id', $order->agent_id)->where('is_active', 1)->get();
            } else {
                $drivers = Driver::where('is_active', 1)->select('drivers.*')->orderBy('drivers.name', 'ASC');

                if ($user->is_warehouse_user) {
                    $drivers = $drivers->join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                        ->where('user_id', $user->id);
                }

                $drivers = $drivers->get();
            }
            $s_cities = City::where('governorate_id', $order->s_government_id)->orderBy('name_en', 'ASC')->get();
            $r_cities = City::where('governorate_id', $order->r_government_id)->orderBy('name_en', 'ASC')->get();
            if (!empty($order->customer) && $order->customer != null) {
                $customers = Customer::where('corporate_id', $order->customer->corporate_id)
                    ->where('status', 1)->select('customers.*')->orderBy('customers.name', 'ASC');

                if ($user->is_warehouse_user) {
                    $customers = $customers->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                        ->where('user_id', $user->id);
                }

                $customers = $customers->get();

                $order->corporate_id = $order->customer->corporate_id;
                $stores = Stock::whereRaw('qty - used_qty > 0')->where('corporate_id', $order->corporate_id)->get();
            }
        }
        if ($user->role_id == '1') {
            $drivers = Driver::where('is_active', 1)->select('drivers.*')->orderBy('drivers.name', 'ASC');

            if ($user->is_warehouse_user) {
                $drivers = $drivers->join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                    ->where('user_id', $user->id);
            }

            $drivers = $drivers->get();
            return view('backend.orders.forward_edit', compact('order', 'drivers', 'allStores', 'customers', 'r_cities', 's_cities', 'payments', 'agents', 'governments', 'types', 'Corporate_ids', 'stores'));
        } else {
            $drivers = Driver::where('agent_id', $user->agent_id)->where('is_active', 1)->orderBy('name', 'ASC')->get();
            return view('agent.orders.forward_edit', compact('order', 'drivers', 'allStores'));
        }

    }/*forward_recall view */

    public function forward_recall(Request $request, $id)
    {
        /** forward recall order to another new order  */
        if (!empty($request->input("customer_id")) && $request->input("customer_id") > 0) {
            $validator = Validator::make($request->all(), [
                'receiver_name' => 'string|max:255',
                'receiver_mobile' => 'required',
                'customer_id' => 'required',
                'sender_name' => 'required',
                'sender_mobile' => 'required',
                'payment_method_id' => 'required',
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                'receiver_name' => 'string|max:255',
                'receiver_mobile' => 'required',
                'sender_name' => 'required',
                'sender_mobile' => 'required',
                'payment_method_id' => 'required',
            ]);
        }

        $orderCheck = Order::findOrFail($id);
        if ($orderCheck) {
            $order = new Order();
            //forward_order_id to save old order that he forwared from
            $order->forward_order_id = $orderCheck->id;
            $order->receiver_name = $request->input("receiver_name");
            $order->receiver_mobile = $request->input("receiver_mobile");
            $order->receiver_phone = $request->input("receiver_phone");
            $order->receiver_latitude = $request->input("receiver_latitude");
            $order->receiver_longitude = $request->input("receiver_longitude");
            $order->receiver_address = $request->input("receiver_address");
            $order->sender_name = $request->input("sender_name");
            $order->sender_mobile = $request->input("sender_mobile");
            $order->sender_phone = $request->input("sender_phone");
            $order->sender_latitude = $request->input("sender_latitude");
            $order->sender_longitude = $request->input("sender_longitude");
            $order->sender_address = $request->input("sender_address");
            $order->r_state_id = $request->input("r_state_id");
            $order->s_state_id = $request->input("s_state_id");
            $order->r_government_id = $request->input("r_government_id");
            $order->s_government_id = $request->input("s_government_id");
            $order->customer_id = $request->input("customer_id");
            $order->type = $request->input("type");
            $order->order_type_id = $request->input("order_type_id");
            $order->agent_id = $request->input("agent_id");
            $order->payment_method_id = $request->input("payment_method_id");
            $order->order_price = $request->input("order_price");
            $order->delivery_price = $request->input("delivery_price");
            $order->overload = $request->input("overload");
            $order->notes = $request->input("notes");
            $order->status = 0;
            $order->order_no = $this->randomNumber(1);//substr(uniqid(sha1(time())), 0, 8);
            if ($request->input("store_id") != "0") {
                $order->store_id = $request->input("store_id");
            }
            if ($request->input("main_store") != "0") {
                $order->main_store = $request->input("main_store");
            }
            $order->qty = $request->input("qty");
            $order->order_number = $this->randomNumber(2);//$this->quickRandom();
            $order->receiver_code = $this->randomNumber(3);//$this->quickRandom();
            $order->save();
            if ($request->input("driver_id") != "0") {
                //create new accepted order with this captain
                $accept_order = new AcceptedOrder();
                $accept_order->order_id = $order->id;
                $accept_order->driver_id = $request->input("driver_id");
                $accept_order->delivery_price = $order->delivery_price;
                $accept_order->status = 0;
                $accept_order->save();

                #then update order status to be accepted
                $order->status = 1;
                $order->save();
            }
            // Function Add Action In Activity Log By Samir
            addActivityLog('Edit Order #No ' . $order->id, ' تعديل الطلب #رقم ' . $order->id, 7); // Helper Function in App\Helpers.php
            Flash::warning(__('backend.Order_Updated_successfully'));

            $user = Auth::guard($this->guardType)->user();
            $type = "";
            if ($order->status == 0) {
                $type = 'pending';
            } else if ($order->status == 6) {
                $type = 'waiting';
            } else if ($order->status == 1) {
                $type = 'accept';
            } else if ($order->status == 2) {
                $type = "receive";
            } else if ($order->status == 4) {
                $type = "cancel";
            } else if ($order->status == 5) {
                $type = "recall";
            } else if ($order->status == 7) {
                $type = "dropped";
            }

            return redirect('mngrAdmin/orders?type=' . $type);

        } else {
            return redirect('/mngrAdmin/404');
        }
    }

    /**forward_recall */

    public function create()
    {

        $collection_id = app('request')->input('collection');
        $orders = TempOrder::where('collection_id', $collection_id)->paginate(50);
        $user = Auth::guard($this->guardType)->user();
        $governments = Governorate::orderBy('name_en', 'ASC')->get();
        $agents = Agent::orderBy('name', 'ASC')->get();
        $payments = PaymentMethod::where('status', 1)->orderBy('name', 'ASC')->get();
        $types = OrderType::orderBy('name_en', 'ASC')->get();

        $Corporate_ids = Corporate::where('is_active', 1)->select('corporates.*')->orderBy('corporates.name', 'ASC');

        if ($user->is_warehouse_user) {
            $Corporate_ids = $Corporate_ids->join('customers', 'customers.corporate_id', '=', 'corporates.id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id);
        }

        $Corporate_ids = $Corporate_ids->get();

        $customer = null;
        $drivers = [];
        $customers = [];
        $collection = null;
        $corporate = null;
        $stores = [];
        $allStores = Store::where('status', 1);

        if ($user->is_warehouse_user) {
            $allStores = $allStores->join('store_user', 'store_user.store_id', '=', 'stores.id')
                ->where('user_id', $user->id);
        }

        $allStores = $allStores->get();

        if (!empty($collection_id)) {
            $collection = CollectionOrder::where('id', $collection_id)->first();
            if (!empty($collection) && $collection != null) {

                $customer = Customer::where('id', $collection->customer_id)->first();
                if ($customer) {
                    $customers = Customer::where('corporate_id', $customer->corporate_id)->get();
                    $corporate = Corporate::find($customer->corporate_id);
                    $collection->corporate_id = $corporate->id;
                    $stores = Stock::whereRaw('qty - used_qty > 0')->where('corporate_id', $customer->corporate_id)->get();
                }

            }
            if ($collection && $collection->is_saved == 1) {
                flash(__('backend.Collection_that_you_try_to_open_saved_before'))->error();
                return redirect('/mngrAdmin/collection_orders?type=1');
            }
        }

        if ($user->role_id == '1') {
            $drivers = Driver::where('is_active', 1)->select('drivers.*')->orderBy('drivers.name', 'ASC');

            if ($user->is_warehouse_user) {
                $drivers = $drivers->join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                    ->where('user_id', $user->id);
            }

            $drivers = $drivers->get();

            return view('backend.orders.create', compact('payments', 'drivers', 'agents', 'governments', 'types', 'collection_id', 'orders', 'Corporate_ids', 'collection', 'customers', 'customer', 'stores', 'allStores', 'corporate'));

        } else {
            $drivers = Driver::where('agent_id', $user->agent_id)->where('is_active', 1)->orderBy('name', 'ASC')->get();
            return view('agent.orders.create', compact('payments', 'drivers', 'allStores'));
        }
    } /* create   new order */

    public function save_collection(Request $request)
    {
        $collection = CollectionOrder::where('id', $request->collection_id)->first();
        if ($collection) {
            $orders = TempOrder::where('collection_id', $collection->id)->get();
            if (!empty($orders) && count($orders)) {
                DB::beginTransaction();
                try {
                    $store_id = null;
                    $first_order = $orders[0];
                    if (!$store_id) {
                        $customer = Customer::find($first_order->customer_id);
                        if ($customer && $customer->store_id) {
                            $store_id = $customer->store_id;
                        } else {
                            $store = Store::where('status', 1)->where('pickup_default', 1)->first();
                            if ($store) {
                                $store_id = $store->id;
                            }
                        }
                    }

                    foreach ($orders as $tmporder) {
                        $order = new Order();
                        $order->reference_number = $tmporder->reference_number;
                        $order->delivery_type = $tmporder->delivery_type;
                        $order->sender_subphone = $tmporder->sender_subphone;
                        $order->receiver_name = $tmporder->receiver_name;
                        $order->receiver_mobile = $tmporder->receiver_mobile;
                        $order->receiver_phone = $tmporder->receiver_phone;
                        $order->receiver_latitude = $tmporder->receiver_latitude;
                        $order->receiver_longitude = $tmporder->receiver_longitude;
                        $order->receiver_address = $tmporder->receiver_address;
                        #//
                        $order->collection_id = $collection->id;
                        $order->sender_name = $tmporder->sender_name;
                        $order->sender_mobile = $tmporder->sender_mobile;
                        $order->sender_phone = $tmporder->sender_phone;
                        $order->sender_latitude = $tmporder->sender_latitude;
                        $order->sender_longitude = $tmporder->sender_longitude;
                        $order->sender_address = $tmporder->sender_address;
                        $order->r_state_id = $tmporder->r_state_id;
                        $order->s_state_id = $tmporder->s_state_id;
                        $order->r_government_id = $tmporder->r_government_id;
                        $order->s_government_id = $tmporder->s_government_id;
                        $order->status = 0;
                        $order->type = $tmporder->type;
                        $order->order_type_id = $tmporder->order_type_id;
                        $order->agent_id = $tmporder->agent_id;
                        $order->customer_id = $tmporder->customer_id;
                        $order->payment_method_id = $tmporder->payment_method_id;
                        $order->order_price = $tmporder->order_price;
                        $order->delivery_price = $tmporder->delivery_price;
                        $order->overload = $tmporder->overload;
                        $order->notes = $tmporder->notes;
                        $order->order_number = $tmporder->order_number;
                        $order->receiver_code = $tmporder->receiver_code;
                        $order->moved_in = $store_id;
                        if ($tmporder->order_no != "") {
                            $order->order_no = $tmporder->order_no;
                        } else {
                            $order->order_no = $this->randomNumber(1);//substr(uniqid(sha1(time())), 0, 8);
                        }

                        $order->qty = $tmporder->qty;
                        if ($tmporder->store_id > 0) {
                            $order->store_id = $tmporder->store_id;
                        }
                        $order->main_store = $tmporder->main_store;
                        if ($tmporder->main_store) {
                            $order->head_office = 1;
                        }
                        $order->save();
                        if (!empty($tmporder->forward_driver_id) && $tmporder->forward_driver_id > 0) {
                            // forward orded beefore

                            $accept_order = new AcceptedOrder();
                            $accept_order->order_id = $order->id;
                            $accept_order->driver_id = $tmporder->forward_driver_id;
                            $accept_order->delivery_price = $order->delivery_price;
                            $accept_order->status = 0;

                            $captain = Driver::find($request->driver_id);
                            if ($captain) {
                                $accept_order->driver_bonus = $captain->bouns_of_delivery;
                            }

                            $accept_order->save();

                            #then update order status to be accepted
                            $order->status = 1;
                            $order->accepted_at = Carbon::now();
                            $order->save();
                        }
                    }
                    $collection->saved_at = Carbon::now();
                    $collection->is_saved = 1;
                    $collection->save();

                    // Function Add Action In Activity Log By Samir
                    addActivityLog('Save Collection Orders #No ' . $order->collection_id, ' حفظ مجموعة الطلبات الجديدة #رقم ' . $order->collection_id, 7); // Helper Function in App\Helpers.php
                    TempOrder::where('collection_id', $collection->id)->delete();
                    DB::commit();
                    Flash::success(__('backend.Collection_saved_successfully'));
                    return redirect('/mngrAdmin/collection_orders?type=1');
                } catch (Exception $e) {
                    DB::rollback();
                    Flash::error(__('backend.Collection_not_Saved_Something_Error'));
                    return redirect('/mngrAdmin/collection_orders?type=1');
                }
            } else {
                flash(__('backend.Collection_that_you_try_to_save_does_not_have_orders'))->error();
                return redirect('/mngrAdmin/collection_orders?type=1');
            }
        } else {
            flash(__('backend.Collection_that_you_try_to_save_does_not_exist'))->error();
            return redirect('/mngrAdmin/collection_orders?type=1');
        }

    }/*save_collection save tmp orders to orders*/

    public function tmporders_create(Request $request)
    {
        $user = Auth::guard($this->guardType)->user();

        $first_one = "0";
        /** this field used to create new collection or not */
        if ($request->ajax()) {
            if (!empty($request->input("customer_id")) && $request->input("customer_id") > 0) {
                $validator = Validator::make($request->all(), [
                    'receiver_name' => 'string|max:255',
                    'receiver_mobile' => 'required',
                    'customer_id' => 'required',
                    'sender_mobile' => 'required',
                    'payment_method_id' => 'required',
                    'r_government_id' => 'required',
                    'r_state_id' => 'required',
                    's_government_id' => 'required',
                    's_state_id' => 'required'

                ]);
            } else {
                $validator = Validator::make($request->all(), [
                    'receiver_name' => 'string|max:255',
                    'receiver_mobile' => 'required',
                    'sender_mobile' => 'required',
                    'payment_method_id' => 'required',
                    'r_government_id' => 'required',
                    'r_state_id' => 'required',
                    's_government_id' => 'required',
                    's_state_id' => 'required'
                ]);
            }
        } else {
            $this->validate($request, [
                'receiver_name' => 'string|max:255',
                'receiver_mobile' => 'required',
//                'receiver_phone' => 'required|string',
                'customer_id' => 'required',
                'sender_mobile' => 'required',
//                'sender_phone' => 'required',
                'payment_method_id' => 'required',
                'r_government_id' => 'required',
                'r_state_id' => 'required',
                's_government_id' => 'required',
                's_state_id' => 'required'
            ]);
        }
        if ($request->ajax()) {
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->all()]);
            }
        }

        if ($request->input("store_id") && $request->input("qty") <= 0) {
            if ($request->ajax()) {
                return response()->json(['errors' => [__('backend.stock_quantity')]]);
            } else {
                Flash::error(__('backend.stock_quantity'));
                return redirect()->back();
            }
        }

        $collection = CollectionOrder::where('id', $request->collection_id)->first();
        if (!$collection) {
            $collection = new CollectionOrder();
            $collection->customer_id = $request->input("customer_id");
            $collection->count_no = 0;
            $collection->is_saved = 0;
            $collection->type = 1;
            $collection->uploaded_target = 2;
            $collection->save();
            $first_one = "1";
        } else {
            $collection->save();
        }
        $collection_id = $collection->id;
        DB::beginTransaction();
        try {
            if (!empty($request->order_id) && $request->order_id > 0) {
                $order = TempOrder::where('id', $request->order_id)->first();
                if (!$order) {
                    $order = new TempOrder();
                }
            } else {
                $order = new TempOrder();
            }

            $order->reference_number = $request->input("reference_number");
            $order->delivery_type = $request->input("delivery_type");
            $order->sender_subphone = $request->input("sender_subphone");

            $order->receiver_name = $request->input("receiver_name");
            $order->receiver_mobile = $request->input("receiver_mobile");
            $order->receiver_phone = $request->input("receiver_phone");
            $order->receiver_latitude = $request->input("receiver_latitude");
            $order->receiver_longitude = $request->input("receiver_longitude");
            $order->receiver_address = $request->input("receiver_address");
            #//
            $order->collection_id = $collection->id;
            $order->sender_name = $request->input("sender_name");
            $order->sender_mobile = $request->input("sender_mobile");
            $order->sender_phone = $request->input("sender_phone");
            $order->sender_latitude = $request->input("sender_latitude");
            $order->sender_longitude = $request->input("sender_longitude");
            $order->sender_address = $request->input("sender_address");

            $order->r_state_id = $request->input("r_state_id");
            $order->s_state_id = $request->input("s_state_id");
            $order->r_government_id = $request->input("r_government_id");
            $order->s_government_id = $request->input("s_government_id");

            $order->status = 0;
            $order->type = $request->input("type");
            $order->order_type_id = $request->input("order_type_id");
            $order->agent_id = $request->input("agent_id");
            $order->customer_id = $collection->customer_id;
            $order->payment_method_id = $request->input("payment_method_id");

            $order->order_price = $request->input("order_price");
            $order->delivery_price = $request->input("delivery_price");
            $order->overload = $request->input("overload");
            $order->notes = $request->input("notes");
            $order->order_no = $this->randomNumber(1);//substr(uniqid(sha1(time())), 0, 8);
            $order->store_id = $request->input("store_id");
            $order->main_store = $request->input("main_store");
            $order->qty = $request->input("qty");
            $order->forward_driver_id = $request->input("forward_driver_id");

            $order->order_number = $this->randomNumber(2);//$this->quickRandom();
            $order->receiver_code = $this->randomNumber(3);//$this->quickRandom1();
            $order->save();
            DB::commit();

            if (!empty($request->order_id)) {
                // Function Add Action In Activity Log By Samir
                addActivityLog('Edit Temp Order #No ' . $order->id . ' In Collection Orders #No ' . $order->collection_id, ' تعديل الطلب المؤقت #رقم ' . $order->id . ' داخل مجموعة الطلبات #رقم ' . $order->collection_id, 7); // Helper Function in App\Helpers.php
            } else {
                // Function Add Action In Activity Log By Samir
                addActivityLog('Add New Temp Order #No ' . $order->id . ' In Collection Orders #No ' . $order->collection_id, ' اضافة طلب مؤقت جديد #رقم ' . $order->id . ' داخل مجموعة الطلبات #رقم ' . $order->collection_id, 7); // Helper Function in App\Helpers.php
            }

            if ($request->ajax()) {

                if ($validator->fails()) {
                    return response()->json(['errors' => $validator->errors()->all()]);
                }
                $orders = TempOrder::where('collection_id', $collection_id)->paginate(50);
                $orders->withPath(route('mngrAdmin.orders.create', ['collection' => $collection_id]));
                $governments = Governorate::orderBy('name_en', 'ASC')->get();
                $agents = Agent::orderBy('name', 'ASC')->get();
                #payment_methods
                $payments = PaymentMethod::where('status', 1)->orderBy('name', 'ASC')->get();
                $types = OrderType::orderBy('name_en', 'ASC')->get();
                $Corporate_ids = Corporate::where('is_active', 1)->select('corporates.*')->orderBy('corporates.name', 'ASC');

                if ($user->is_warehouse_user) {
                    $Corporate_ids = $Corporate_ids->join('customers', 'customers.corporate_id', '=', 'corporates.id')
                        ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                        ->where('user_id', $user->id);
                }

                $Corporate_ids = $Corporate_ids->get();
                $customer = Customer::where('id', $collection->customer_id)->first();
                $allStores = Store::where('status', 1);

                if ($user->is_warehouse_user) {
                    $allStores = $allStores->join('store_user', 'store_user.store_id', '=', 'stores.id')
                        ->where('user_id', $user->id);
                }

                $allStores = $allStores->get();
                $corporate = null;

                $drivers = Driver::where('status', 1)->select('drivers.*')->orderBy('drivers.name', 'ASC');

                if ($user->is_warehouse_user) {
                    $drivers = $drivers->join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                        ->where('user_id', $user->id);
                }

                $drivers = $drivers->get();

                if ($customer) {
                    $corporate = Corporate::find($customer->corporate_id);
                    $customers = Customer::where('corporate_id', $customer->corporate_id)->get();
                    $stores = Stock::whereRaw('qty - used_qty > 0')->where('corporate_id', $customer->corporate_id)->get();

                    $collection->corporate_id = Corporate::where('id', $customer->corporate_id)->value('id');
                }
                return [
                    'collection_id' => $collection_id,
                    'first_one' => $first_one,
                    'view' => view('backend.orders.tmptable', compact('orders', 'collection_id', 'collection'))->render(),
                    'view_order' => view('backend.orders.create-form', compact('collection_id', 'payments', 'agents', 'governments', 'types', 'Corporate_ids', 'collection', 'customer', 'customers', 'drivers', 'stores', 'allStores', 'corporate'))->render(),

                    'js' => view('backend.orders.js', array('from_update' => true))->render(),

                ];
            }
        } catch (Exception $e) {
            return $e;
            DB::rollback();

        }
    }/*tmporders_create*/

    public function tmporders_reset_view($id, Request $request)
    {
        /** this fn used to reset form of create tmp order in collections */
        $collection_id = $id;
        $user = Auth::guard($this->guardType)->user();
        $governments = Governorate::orderBy('name_en', 'ASC')->get();
        $agents = Agent::orderBy('name', 'ASC')->get();
        $payments = PaymentMethod::where('status', 1)->orderBy('name', 'ASC')->get();
        $types = OrderType::orderBy('name_en', 'ASC')->get();
        $Corporate_ids = Corporate::where('is_active', 1)->orderBy('corporates.name', 'ASC');
        if ($user->is_warehouse_user) {
            $Corporate_ids = $Corporate_ids->join('customers', 'customers.corporate_id', '=', 'corporates.id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id);
        }
        $Corporate_ids = $Corporate_ids->select('corporates.*')->get();
        $customer = null;
        $drivers = [];
        $customers = [];
        $collection = null;
        $stores = [];
        if (!empty($collection_id)) {
            $collection = CollectionOrder::where('id', $collection_id)->first();
            if (!empty($collection) && $collection != null) {
                $customer = Customer::where('id', $collection->customer_id)->first();
                if ($customer) {
                    $customers = Customer::where('corporate_id', $customer->corporate_id)->get();
                    $collection->corporate_id = Corporate::where('id', $customer->corporate_id)->value('id');
                    $stores = Stock::whereRaw('qty - used_qty > 0')->where('corporate_id', $customer->corporate_id)->get();
                }
                if ($request->ajax()) {
                    return [
                        'view' => view('backend.orders.create-form', compact('drivers', 'customers', 'collection_id', 'payments', 'agents', 'governments', 'types', 'Corporate_ids', 'collection', 'stores', 'customer'))->render(),
                    ];
                }
            } else {
                return 'false';
            }
        } else {
            return 'false';
        }
    }/*tmporders_reset_view*/

    public function delete_tmporder(Request $request)
    {
        //this fn used to delete order in tmp order
        $order = TempOrder::where('id', $request->id)->first();
        if (!empty($order)) {
            $order->delete();
            // Function Add Action In Activity Log By Samir
            addActivityLog('Delete Temp Order #No ' . $order->id . ' In Collection Orders #No ' . $order->collection_id, ' حذف الطلب المؤقت #رقم ' . $order->id . ' من مجموعة الطلبات #رقم ' . $order->collection_id, 7); // Helper Function in App\Helpers.php
            return __('backend.Order_Deleted_successfully');
        } else {
            return 'false';
        }
    }/*delete_tmporder*/

    public function tmporders_update_view($id, Request $request)
    {
        $user = Auth::guard($this->guardType)->user();

        // this fn to update tmp order view
        $governments = Governorate::orderBy('name_en', 'ASC')->get();
        $agents = Agent::orderBy('name', 'ASC')->get();
        $payments = PaymentMethod::where('status', 1)->orderBy('name', 'ASC')->get();
        $allStores = Store::where('status', 1);

        if ($user->is_warehouse_user) {
            $allStores = $allStores->join('store_user', 'store_user.store_id', '=', 'stores.id')
                ->where('user_id', $user->id);
        }

        $allStores = $allStores->get();
        $types = OrderType::orderBy('name_en', 'ASC')->get();
        $Corporate_ids = Corporate::where('is_active', 1)->select('corporates.*')->orderBy('corporates.name', 'ASC');

        if ($user->is_warehouse_user) {
            $Corporate_ids = $Corporate_ids->join('customers', 'customers.corporate_id', '=', 'corporates.id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id);
        }

        $Corporate_ids = $Corporate_ids->get();
        $collection_id = 0;
        $order = TempOrder::where('id', $id)->first();
        $orders = null;
        $s_cities = [];
        $r_cities = [];
        $customers = [];
        $stores = [];
        $drivers = [];
        $collection = null;

        if (!empty($order) && $order != null) {
            $collection_id = $order->collection_id;
            $orders = TempOrder::where('collection_id', $order->collection_id)->paginate(50);
            if ($order->agent_id > 0) {
                //get drivers that belong to agent to display them
                $drivers = Driver::where('agent_id', $order->agent_id)->where('is_active', 1)->get();
            } else {
                $drivers = Driver::where('is_active', 1)->select('drivers.*')->orderBy('drivers.name', 'ASC');

                if ($user->is_warehouse_user) {
                    $drivers = $drivers->join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                        ->where('user_id', $user->id);
                }

                $drivers = $drivers->get();
            }
            $collection = CollectionOrder::where('id', $collection_id)->first();
            //sender cities
            $s_cities = City::where('governorate_id', $order->s_government_id)->orderBy('name_en', 'ASC')->get();
            //receiver cities
            $r_cities = City::where('governorate_id', $order->r_government_id)->orderBy('name_en', 'ASC')->get();

            if (!empty($order->customer) && $order->customer != null) {
                $customers = Customer::where('corporate_id', $order->customer->corporate_id)
                    ->where('status', 1)->select('customers.*')->orderBy('customers.name', 'ASC');

                if ($user->is_warehouse_user) {
                    $customers = $customers->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                        ->where('user_id', $user->id);
                }

                $customers = $customers->get();

                $order->corporate_id = $order->customer->corporate_id;
                //get stocks that found belongs to corporate
                $stores = Stock::whereRaw('qty - used_qty > 0')->where('corporate_id', $order->corporate_id)->get();
            }
        }
        if ($request->ajax()) {
            return [
                'view' => view('backend.orders.update-form', compact('drivers', 'customers', 'r_cities', 's_cities', 'order', 'collection_id', 'payments', 'agents', 'governments', 'types', 'Corporate_ids', 'collection', 'stores', 'allStores'))->render(),
            ];
        }
    }/*tmporders_update_view*/

    public function store(Request $request)
    {

        $order = new Order();
        $order->reference_number = $request->input("reference_number");
        $order->delivery_type = $request->input("delivery_type");
        $order->sender_subphone = $request->input("sender_subphone");
        $order->receiver_name = $request->input("receiver_name");
        $order->receiver_mobile = $request->input("receiver_mobile");
        $order->receiver_phone = $request->input("receiver_phone");
        $order->receiver_latitude = $request->input("receiver_latitude");
        $order->receiver_longitude = $request->input("receiver_longitude");
        $order->receiver_address = $request->input("receiver_address");
        $order->receiver_famous_palce = $request->input("receiver_famous_palce");
        $order->sender_name = $request->input("sender_name");
        $order->sender_mobile = $request->input("sender_mobile");
        $order->sender_phone = $request->input("sender_phone");
        $order->sender_latitude = $request->input("sender_latitude");
        $order->sender_longitude = $request->input("sender_longitude");
        $order->sender_address = $request->input("sender_address");
        $order->sender_famous_palce = $request->input("sender_famous_palce");
        $order->status = $request->input("status");
        $order->type = $request->input("type");
        $order->type_id = $request->input("type_id");
        $order->agent_id = $request->input("agent_id");
        $order->customer_id = $request->input("customer_id");
        $order->payment_method_id = $request->input("payment_method_id");
        $order->size_id = $request->input("size_id");
        $order->notes = $request->input("notes");
        $order->order_number = $this->randomNumber(2);//time();
        $order->save();

        Flash::success(__('backend.Order_saved_successfully'));
        $user = Auth::guard($this->guardType)->user();
        if ($user->role_id == '1') {
            return redirect()->route('mngrAdmin.orders.index');
        } else {
            return redirect()->route('mngrAgent.orders.index');
        }
    }/*store*/

    public function show($id)
    {
        /** this fn used to display order details */
        $order = Order::findOrFail($id);
        if (!$order->order_no) {
            $order->order_no = $this->randomNumber(1);//substr(uniqid(sha1(time())), 0, 8);
            $order->save();
        }

        $user = Auth::guard($this->guardType)->user();

        $online_drivers = Driver::where('is_active', 1)
            ->orderBy('drivers.id', 'desc')
            ->select('drivers.*');

        if ($user->is_warehouse_user) {
            $online_drivers = $online_drivers->join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                ->where('user_id', $user->id);
        }

        $online_drivers = $online_drivers->get();

        $agents = Agent::get();

        $problems = DeliveryProblem::get();
        if ($user->role_id == '1') {
            return view('backend.orders.show', compact('order', 'online_drivers', 'agents', 'problems'));
        } else {
            return view('agent.orders.show', compact('order'));
        }
    }/*show*/

    public function edit($id)
    {
        $user = Auth::guard($this->guardType)->user();

        /** this fn used to display edit form order  */
        $order = Order::findOrFail($id);
        $user = Auth::guard($this->guardType)->user();
        $governments = Governorate::orderBy('name_en', 'ASC')->get();
        $agents = Agent::orderBy('name', 'ASC')->get();
        $payments = PaymentMethod::where('status', 1)->orderBy('name', 'ASC')->get();
        $types = OrderType::orderBy('name_en', 'ASC')->get();
        $allStores = Store::where('status', 1);

        if ($user->is_warehouse_user) {
            $allStores = $allStores->join('store_user', 'store_user.store_id', '=', 'stores.id')
                ->where('user_id', $user->id);
        }

        $allStores = $allStores->get();
        $status = app('request')->input('type');
        $Corporate_ids = Corporate::where('is_active', 1)->select('corporates.*')->orderBy('corporates.name', 'ASC');

        if ($user->is_warehouse_user) {
            $Corporate_ids = $Corporate_ids->join('customers', 'customers.corporate_id', '=', 'corporates.id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id);
        }

        $Corporate_ids = $Corporate_ids->get();
        $s_cities = [];
        $r_cities = [];
        $customers = [];
        $stores = [];
        $drivers = [];

        if (!empty($order) && $order != null) {
            $collection_id = $order->collection_id;
            if ($order->agent_id > 0) {
                $drivers = Driver::where('agent_id', $order->agent_id)->where('is_active', 1)->get();
            } else {
                $drivers = Driver::where('is_active', 1)->select('drivers.*')->orderBy('drivers.name', 'ASC');

                if ($user->is_warehouse_user) {
                    $drivers = $drivers->join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                        ->where('user_id', $user->id);
                }

                $drivers = $drivers->get();
            }
            $s_cities = City::where('governorate_id', $order->s_government_id)->orderBy('name_en', 'ASC')->get();
            $r_cities = City::where('governorate_id', $order->r_government_id)->orderBy('name_en', 'ASC')->get();
            if (!empty($order->customer) && $order->customer != null) {
                $customers = Customer::where('corporate_id', $order->customer->corporate_id)
                    ->select('customers.*')->orderBy('customers.name', 'ASC');

                if ($user->is_warehouse_user) {
                    $customers = $customers->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                        ->where('user_id', $user->id);
                }

                $customers = $customers->get();

                $order->corporate_id = $order->customer->corporate_id;
                $stores = Stock::whereRaw('qty - used_qty > 0')->where('corporate_id', $order->corporate_id)->get();
            }
        }
        if ($user->role_id == '1') {

            $drivers = Driver::where('is_active', 1)->select('drivers.*')->orderBy('drivers.name', 'ASC');

            if ($user->is_warehouse_user) {
                $drivers = $drivers->join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                    ->where('user_id', $user->id);
            }

            $drivers = $drivers->get();
            return view('backend.orders.edit', compact('order', 'drivers', 'allStores', 'customers', 'r_cities', 's_cities', 'payments', 'agents', 'governments', 'types', 'Corporate_ids', 'stores'));
        } else {
            $drivers = Driver::where('agent_id', $user->agent_id)->where('is_active', 1)->orderBy('name', 'ASC')->get();
            return view('agent.orders.edit', compact('order', 'drivers', 'allStores'));
        }
    }/*edit fn*/

    public function update(Request $request, $id)
    {
        if (!empty($request->input("customer_id")) && $request->input("customer_id") > 0) {
            $this->validate($request, [
                'receiver_name' => 'string|max:255',
                'receiver_mobile' => 'required',
//                'receiver_phone' => 'required|string',
                'customer_id' => 'required',
                'sender_mobile' => 'required',
//                'sender_phone' => 'required',
                'payment_method_id' => 'required',
                'r_government_id' => 'required',
                'r_state_id' => 'required',
                's_government_id' => 'required',
                's_state_id' => 'required'
            ]);
        } else {
            $this->validate($request, [
                'receiver_name' => 'string|max:255',
                'receiver_mobile' => 'required',
//                'receiver_phone' => 'required|string',
                'sender_mobile' => 'required',
//                'sender_phone' => 'required',
                'payment_method_id' => 'required',
                'r_government_id' => 'required',
                'r_state_id' => 'required',
                's_government_id' => 'required',
                's_state_id' => 'required'
            ]);
        }

        if ($request->input("store_id") && $request->input("qty") <= 0) {
            Flash::error(__('backend.stock_quantity'));
            return redirect()->back();
        }

        $order = Order::findOrFail($id);
        if ($order) {
            $order->reference_number = $request->input("reference_number");
            $order->delivery_type = $request->input("delivery_type");
            $order->sender_subphone = $request->input("sender_subphone");
            $order->receiver_name = $request->input("receiver_name");
            $order->receiver_mobile = $request->input("receiver_mobile");
            $order->receiver_phone = $request->input("receiver_phone");
            $order->receiver_latitude = $request->input("receiver_latitude");
            $order->receiver_longitude = $request->input("receiver_longitude");
            $order->receiver_address = $request->input("receiver_address");
            $order->sender_name = $request->input("sender_name");
            $order->sender_mobile = $request->input("sender_mobile");
            $order->sender_phone = $request->input("sender_phone");
            $order->sender_latitude = $request->input("sender_latitude");
            $order->sender_longitude = $request->input("sender_longitude");
            $order->sender_address = $request->input("sender_address");
            $order->r_state_id = $request->input("r_state_id");
            $order->s_state_id = $request->input("s_state_id");
            $order->r_government_id = $request->input("r_government_id");
            $order->s_government_id = $request->input("s_government_id");
            $order->type = $request->input("type");
            $order->order_type_id = $request->input("order_type_id");
            $order->agent_id = $request->input("agent_id");
            $order->payment_method_id = $request->input("payment_method_id");
            $order->order_price = $request->input("order_price");
            $order->delivery_price = $request->input("delivery_price");
            $order->overload = $request->input("overload");
            $order->notes = $request->input("notes");
            if ($request->input("store_id") > 0) {
                $order->store_id = $request->input("store_id");
            }
            if ($request->input("main_store") > 0) {
                $order->main_store = $request->input("main_store");
                $order->head_office = 1;
            } else {
                $order->head_office = 0;
            }

            $order->qty = $request->input("qty");

            $order->save();
            // Function Add Action In Activity Log By Samir
            addActivityLog('Edit Order #No ' . $order->id, ' تعديل الطلب #رقم ' . $order->id, 7); // Helper Function in App\Helpers.php
            Flash::warning(__('backend.Order_Updated_successfully'));

            $user = Auth::guard($this->guardType)->user();
            $type = "";
            if ($order->status == 0) {
                $type = 'pending';
            } else if ($order->status == 6) {
                $type = 'waiting';
            } else if ($order->status == 1) {
                $type = 'accept';
            } else if ($order->status == 2) {
                $type = "receive";
            } else if ($order->status == 4) {
                $type = "cancel";
            } else if ($order->status == 5) {
                $type = "recall";
            } else if ($order->status == 7) {
                $type = "dropped";
            }

            if ($user->role_id == '1') {

                return redirect('mngrAdmin/orders?type=' . $type);
            } else {
                return redirect('mngrAgent/orders?type=' . $type);
            }
        } else {
            return redirect('/mngrAdmin/404');
        }
    }/*update*/

    public function destroy($id)
    {
        /** this fn used to delete order  */
        $acceptedOrder = AcceptedOrder::where('order_id', $id)->get();
        if (count($acceptedOrder) > 0) {
            Flash::warning(__('backend.Cannot_Be_Delete_This_Order'));
            return "false";
        } else {
            $order = Order::findOrFail($id);
            $status = $order->status;
            $order->delete();

            Flash::success(__('backend.Order_Deleted_successfully'));
            $type = "all";
            if ($status == 0) {
                $type = 'pending';
            } else if ($status == 6) {
                $type = 'waiting';
            } else if ($status == 1) {
                $type = 'accept';
            } else if ($status == 2) {
                $type = "receive";
            } else if ($status == 4) {
                $type = "cancel";
            } else if ($status == 5) {
                $type = "recall";
            } else if ($status == 7) {
                $type = "dropped";
            }
            return $type;
        }
    }/*destroy*/

    public function randomNumber($type = 1, $user_id = '')
    {
        return random_number($type, Auth::guard($this->guardType)->user()->id);
    }

    public static function quickRandom()
    {
        //$pool 					= \Carbon\Carbon::now();
        //return str_pad(mt_rand(1,99999999),6,'0',STR_PAD_LEFT);
        return time();
    }/*quickRandom*/

    public static function quickRandom1()
    {
        //$pool 					= \Carbon\Carbon::now();
        return str_pad(mt_rand(1, 99999999), 10, '0', STR_PAD_LEFT);
    }/*quickRandom*/

    public function get_stock_by_corporate($id, Request $request)
    {
        $stores = Stock::whereRaw('qty - used_qty > 0')->where('corporate_id', $id)->where('store_id', $request->main_store)->get();
        return $stores;
    }/*get_stock_by_corporate*/

    public function get_cost_sender_receiver($from_id, $to_id, Request $request)
    {
        /** this fn used to git shipment price by from and to stations (sender to receiver ) */
        $cost = '';
        if ($request->corparate_id) {
            $cost = CustomerPrice::where('customer_id', $request->corparate_id)
                ->where(function ($q) use ($to_id, $from_id) {
                    $q->where(function ($q1) use ($to_id, $from_id) {
                        $q1->where('start_station', $from_id);
                        $q1->where('access_station', $to_id);
                    });
                    $q->orWhere(function ($q1) use ($to_id, $from_id) {
                        $q1->where('access_station', $from_id);
                        $q1->where('start_station', $to_id);
                    });
                })->first();
        }

        if (!$cost) {
            $cost = GovernoratePrice::where(function ($q) use ($to_id, $from_id) {
                $q->where(function ($q1) use ($to_id, $from_id) {
                    $q1->where('start_station', $from_id);
                    $q1->where('access_station', $to_id);
                });
                $q->orWhere(function ($q1) use ($to_id, $from_id) {
                    $q1->where('access_station', $from_id);
                    $q1->where('start_station', $to_id);
                });
            })->first();
        }

        if ($cost) {
            return $cost->cost;
        } else {
            return 'not_exist';
        }
    }/*get_cost_sender_receiver*/

    public function scan_print(Request $request)
    {
        $ids = explode(',', app('request')->input('ids'));
        if (is_array($ids) && count($ids)) {

            $driver = Driver::find($request->driver_id);
            $orders = Order::with(['customer' => function ($query) {
                $query->with('Corporate');
            }])->whereIn('id', $ids)->get();

            $pdf = PDF::loadView('backend.orders.pdf-scan', compact('orders', 'driver'), [], ['format' => 'A4-L']);
            return $pdf->stream(time());

        }
    }

    public function scan_validation(Request $request)
    {
        $data = ['result' => true];

        $ids = app('request')->input('ids');
        if (is_array($ids) && count($ids)) {
            if (app('request')->input('type') == 'print') {
                $orders = Order::join('accepted_orders', 'orders.id', '=', 'accepted_orders.order_id')
                    ->whereIn('orders.id', $ids)
                    ->whereIn('orders.status', [1, 2])
                    ->whereIn('accepted_orders.status', [0, 2])
                    ->select('orders.*', 'driver_id')
                    ->get();

                $drivers = $orders->groupBy('driver_id')->map(function ($drivers) {
                    return $drivers->count();
                });

                if (count($orders) != count($ids) || count($drivers) > 1) {
                    $data['result'] = false;
                }
            } elseif (app('request')->input('type') == 'forward') {
                $orders = Order::where('orders.status', 7)
                    ->whereIn('orders.id', $ids)
                    ->get();

                if (count($orders) != count($ids)) {
                    $data['result'] = false;
                }
            } elseif (app('request')->input('type') == 'receive') {
                $orders = Order::where('orders.status', 1)
                    ->whereIn('orders.id', $ids)
                    ->get();

                if (count($orders) != count($ids)) {
                    $data['result'] = false;
                }
            } elseif (app('request')->input('type') == 'deliver' || app('request')->input('type') == 'problems') {
                $orders = Order::where('orders.status', 2)
                    ->whereIn('orders.id', $ids)
                    ->get();

                if (count($orders) != count($ids)) {
                    $data['result'] = false;
                }
            } elseif (app('request')->input('type') == 'recall') {
                $orders = Order::where('orders.status', 2)
                    ->whereIn('orders.id', $ids)
                    ->get();

                if (count($orders) != count($ids)) {
                    $data['result'] = false;
                }
            } elseif (app('request')->input('type') == 'cancel') {
                $orders = Order::whereIn('orders.status', [7, 1, 2])
                    ->whereIn('orders.id', $ids)
                    ->get();

                if (count($orders) != count($ids)) {
                    $data['result'] = false;
                }
            } elseif (app('request')->input('type') == 'delay') {
                $orders = Order::whereIn('orders.status', [7, 1, 2])
                    ->whereIn('orders.id', $ids)
                    ->get();

                if (count($orders) != count($ids)) {
                    $data['result'] = false;
                }
            }
        }

        return $data;
    }

    public function scan_action(Request $request)
    {
        $data = ['ids' => []];

        $ids = app('request')->input('ids');
        if (is_array($ids) && count($ids)) {
            if (app('request')->input('type') == 'forward' && app('request')->input('driver_id')) {
                DB::beginTransaction();
                try {
                    foreach ($ids as $key => $value) {
                        //update order status to be 1 when it assign to driver
                        $order = Order::find($value);
                        $order->status = 1;
                        $order->accepted_at = Carbon::now();
                        $order->save();

                        $accepted_order = new AcceptedOrder();
                        $accepted_order->order_id = $order->id;
                        $accepted_order->driver_id = app('request')->input('driver_id');
                        $accepted_order->delivery_price = $order->delivery_price;
                        $accepted_order->status = 0;
                        $accepted_order->save();
                    }

                    $data['ids'] = $ids;
                    $data['result'] = true;
                    $data['status'] = "<span class='badge badge-pill label-info'>" . __('backend.accepted') . "</span>";
                    $data['message'] = __('backend.Order_Routed_saved_successfully');

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    $data['result'] = false;
                    $data['message'] = __('backend.Something_Error');
                }
            } elseif (app('request')->input('type') == 'receive') {
                DB::beginTransaction();
                try {
                    $accepted_orders = AcceptedOrder::with(['driver', 'order' => function ($query) {
                        $query->with(['customer']);
                    }])->whereIn('order_id', $ids)->where('status', 0)->get();

                    foreach ($accepted_orders as $accepted_order) {
                        $type = 1;
                        if ($accepted_order->driver->agent_id != 1 && $accepted_order->driver->agent_id != null) {
                            $type = 3;
                        }

                        $invoiceCaptain = Invoice::firstOrCreate(
                            ['object_id' => $accepted_order->driver->id, 'type' => $type, 'status' => 0],
                            [
                                'invoice_no' => uniqid(),
                                'object_id' => $accepted_order->driver->id,
                                'type' => $type,
                                'status' => 0,
                                'start_at' => date("Y-m-d"),
                                'start_date' => date("Y-m-d H:i:s")
                            ]
                        );

                        InvoiceOrder::create([
                            'accepted_order_id' => $accepted_order->id,
                            'invoice_id' => $invoiceCaptain->id,
                            'price' => $accepted_order->order->delivery_price,
                        ]);

                        $invoice = Invoice::firstOrCreate(
                            ['object_id' => $accepted_order->order->customer->corporate_id, 'type' => 2, 'status' => 0],
                            [
                                'invoice_no' => uniqid(),
                                'object_id' => $accepted_order->order->customer->corporate_id,
                                'type' => 2,
                                'status' => 0,
                                'start_at' => date("Y-m-d"),
                                'start_date' => date("Y-m-d H:i:s")
                            ]
                        );

                        InvoiceOrder::create([
                            'accepted_order_id' => $accepted_order->id,
                            'invoice_id' => $invoice->id,
                            'price' => $accepted_order->order->delivery_price,
                        ]);

                    }

                    AcceptedOrder::whereIn('order_id', $ids)->where('status', 0)->update(['status' => 2, 'received_date' => date("Y-m-d H:i:s")]);
                    Order::whereIn('id', $ids)->update(['status' => 2, 'received_at' => date("Y-m-d H:i:s")]);

                    $data['ids'] = $ids;
                    $data['result'] = true;
                    $data['status'] = "<span class='badge badge-pill label-warning'>" . __('backend.received') . "</span>";
                    $data['message'] = __('backend.orders_received');

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    $data['result'] = false;
                    $data['message'] = __('backend.Something_Error');
                }
            } elseif (app('request')->input('type') == 'deliver') {
                DB::beginTransaction();
                try {
                    AcceptedOrder::whereIn('order_id', $ids)->where('status', 2)->update(['status' => 3, 'delivery_date' => date("Y-m-d H:i:s")]);
                    Order::whereIn('id', $ids)->update(['status' => 3, 'delivered_at' => date("Y-m-d H:i:s")]);

                    $data['ids'] = $ids;
                    $data['result'] = true;
                    $data['status'] = "<span class='badge badge-pill label-success'>" . __('backend.delivered') . "</span>";
                    $data['message'] = __('backend.orders_delivered');

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    $data['result'] = false;
                    $data['message'] = __('backend.Something_Error');
                }
            } elseif (app('request')->input('type') == 'recall') {
                DB::beginTransaction();
                try {
                    AcceptedOrder::whereIn('order_id', $ids)->where('status', 2)->update(['status' => 5]);
                    Order::whereIn('id', $ids)->update(['status' => 5, 'recalled_at' => date("Y-m-d H:i:s"), 'recalled_by' => 2]);

                    $data['ids'] = $ids;
                    $data['result'] = true;
                    $data['status'] = "<span class='badge badge-pill label-danger'>" . __('backend.recalled') . "</span>";
                    $data['message'] = __('backend.orders_recalled');

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    $data['result'] = false;
                    $data['message'] = __('backend.Something_Error');
                }
            } elseif (app('request')->input('type') == 'cancel') {
                DB::beginTransaction();
                try {
                    AcceptedOrder::whereIn('order_id', $ids)->whereIn('status', [0, 2])->update(['status' => 4, 'cancelled_by' => 1, 'cancelled_at' => date("Y-m-d H:i:s")]);
                    foreach ($ids as $id) {
                        $order = Order::find($id);
                        $order->status_before_cancel = $order->status;
                        $order->status = 4;
                        $order->cancelled_by = 2;
                        $order->cancelled_at = date("Y-m-d H:i:s");
                        $order->save();
                    }
                    Order::whereIn('id', $ids)->update();

                    $data['ids'] = $ids;
                    $data['result'] = true;
                    $data['status'] = "<span class='badge badge-pill label-danger'>" . __('backend.cancelled') . "</span>";
                    $data['message'] = __('backend.orders_cancelled');

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    $data['result'] = false;
                    $data['message'] = __('backend.Something_Error');
                }
            } elseif (app('request')->input('type') == 'problems') {
                DB::beginTransaction();
                try {
                    $before_6_hours = date("Y-m-d H:i:s", strtotime('-6 hours'));

                    $problems_count = OrderDeliveryProblem::whereIn('order_id', $ids)
                        ->where('created_at', '>', $before_6_hours)
                        ->orderBy('created_at', 'DESC')
                        ->count();

                    if ($problems_count) {
                        $data['result'] = false;
                        $data['message'] = __('backend.cannot_send_delivery_problem');

                        return $data;
                    }

                    $problems = $request->only('latitude', 'longitude', 'driver_id', 'delivery_problem_id', 'reason');

                    foreach ($ids as $key => $value) {
                        $problem = OrderDeliveryProblem::where('order_id', $value)
                            ->where('created_at', '>', $before_6_hours)
                            ->orderBy('created_at', 'DESC')
                            ->first();

                        if ($problem) {
                            $data['result'] = false;
                            $data['message'] = __('backend.cannot_send_delivery_problem');

                            return $data;
                        } else {
                            $problems['order_id'] = $value;

                            OrderDeliveryProblem::create($problems);

                            $problem_count = OrderDeliveryProblem::where('order_id', $value)->count();
                            if ($problem_count >= 3) {
                                AcceptedOrder::where('order_id', $value)
                                    ->where('status', 2)
                                    ->update(['status' => 5]);
                                Order::where('id', $value)
                                    ->where('status', 2)
                                    ->update(['status' => 5, 'recalled_at' => date("Y-m-d H:i:s"), 'recalled_by' => 2]);
                            }
                        }
                    }

                    $data['ids'] = $ids;
                    $data['result'] = true;
                    $data['message'] = __('backend.add_problem_successfully');

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    $data['result'] = false;
                    $data['message'] = __('backend.Something_Error');
                }
            }
        }

        return $data;
    }

    public function pdf($type, $id, Request $request)
    {
        if (($request->select) != NULL) {

            $order_ids = $request->select;

            if ($type == 1) {

//                $name = 'Driver';
//
//                $orders = DB::table('accepted_orders')->join('orders', function ($join) use ($order_id) {
//                    $join->on('orders.id', '=', 'accepted_orders.order_id')
//                        ->whereIn('orders.id', $order_id);
//                })
////                    ->join('drivers', 'accepted_orders.driver_id', '=', 'drivers.id')
//                    ->select('orders.delivery_price', 'accepted_orders.delivery_date', 'received_date', 'accepted_orders.status AS accepted_orders_status', 'orders.type', 'orders.notes', 'orders.order_price', 'orders.order_number', 'orders.status AS order_status', 'orders.receiver_name', 'orders.receiver_code', 'orders.id AS order_id', 'orders.sender_name', 'receiver_address', 'orders.payment_method_id', 'overload', 'orders.customer_id', 'orders.receiver_address', 'orders.receiver_name', 'orders.receiver_phone')
//                    ->get();
//
//                return view('backend.pdf', compact('orders', 'name'));

                $driver = Driver::find($id);
                $orders = Order::with(['customer' => function ($query) {
                    $query->with('corporate');
                }])->whereIn('id', $order_ids)->get();

                $pdf = PDF::loadView('backend.orders.pdf-driver-orders', compact('orders', 'driver'), [], ['format' => 'A4-L']);
                return $pdf->stream(time());

            } elseif ($type == 2) {

                $name = 'Corporate';

                $orders = DB::table('orders')->leftJoin('accepted_orders', 'orders.id', '=', 'accepted_orders.order_id')->
                leftJoin('drivers', 'accepted_orders.driver_id', '=', 'drivers.id')->
                join('customers', 'orders.customer_id', '=', 'customers.id')->
                join('corporates', 'customers.corporate_id', '=', 'corporates.id')->
                select('orders.delivery_price', 'accepted_orders.delivery_date', 'received_date', 'drivers.name AS driver_name', 'drivers.id AS driver_id', 'drivers.mobile AS driver_mobile',
                    'accepted_orders.status AS accepted_orders_status', 'orders.type', 'orders.notes', 'orders.sender_name', 'orders.order_price', 'orders.order_number',
                    'orders.status AS order_status', 'orders.receiver_name', 'orders.receiver_code', 'orders.id AS order_id', 'corporates.name', 'corporates.mobile',
                    'orders.payment_method_id', 'corporates.id', 'receiver_address', 'overload')
                    ->whereIn('orders.id', $order_ids)->get();
                return view('backend.pdf', compact('orders', 'name'));

            } elseif ($type == 3) {

                $name = 'Agent';

                $orders = DB::table('accepted_orders')->join('orders', 'orders.id', '=', 'accepted_orders.order_id')
                    ->join('drivers', 'accepted_orders.driver_id', '=', 'drivers.id')->
                    join('agents', 'drivers.agent_id', '=', 'agents.id')->
                    select('orders.delivery_price', 'orders.notes', 'accepted_orders.delivery_date', 'received_date', 'drivers.name AS driver_name', 'drivers.mobile AS driver_mobile',
                        'accepted_orders.status AS accepted_orders_status', 'orders.type', 'orders.notes', 'orders.sender_name', 'orders.order_price', 'orders.order_number',
                        'orders.status AS order_status', 'orders.receiver_name', 'orders.receiver_code', 'orders.id AS order_id', 'agents.name', 'agents.mobile', 'agents.id',
                        'orders.payment_method_id', 'receiver_address', 'overload')
                    ->whereIn('orders.id', $order_ids)->get();
                return view('backend.pdf', compact('orders', 'name'));

            } elseif ($type == 4) {

                $name = 'Customer';

                $orders = DB::table('accepted_orders')->join('orders', 'orders.id', '=', 'accepted_orders.order_id')
                    ->join('drivers', 'accepted_orders.driver_id', '=', 'drivers.id')
                    ->join('customers', 'orders.customer_id', '=', 'customers.id')->
                    join('corporates', 'customers.corporate_id', '=', 'corporates.id')->
                    select('orders.delivery_price', 'accepted_orders.delivery_date', 'received_date', 'drivers.name AS driver_name', 'drivers.mobile AS driver_mobile', 'accepted_orders.status AS accepted_orders_status', 'orders.type', 'orders.notes', 'orders.sender_name', 'orders.order_price', 'orders.order_number', 'orders.status AS order_status', 'orders.receiver_name', 'orders.receiver_code', 'orders.id AS order_id', 'customers.name', 'customers.mobile', 'customers.id', 'receiver_address', 'orders.payment_method_id', 'overload')
                    ->whereIn('orders.id', $order_ids)->get();
                return view('backend.pdf', compact('orders', 'name'));
            }
        } else {
            Flash::warning(__('backend.No_Any_Selected_Orders'));
            return redirect()->back();
        }
    }/* Print Orders PDF function **/

    public function getAllDriverInAgent(Request $request)
    {
        /** get all drivere in agent  */
        $id = $request->id;
        $drivers[] = DB::table('drivers')->where('agent_id', $id)->get();
        $response = array(
            'drivers' => $drivers,
        );
        return response()->json($response, 200);
    }/* getAllDriverInAgent*/

    public function forward(Request $request)
    {
        /* 	$request->select attibute :: order ids that u want to forward
            $request->driverID :: driver u want to assign order to him
             $request->agent :: agent id u want to assign order
        */
        if (!isset($request->select) && empty ($request->select)) {
            Flash::warning(__('backend.Please_Select_Any_Order'));
            return redirect()->back();
        }
        if (($request->driverID == "" || !$request->driverID) && $request->agent) {
            foreach ($request->select as $value) {
                Order::find($value)->update(['agent_id' => $request->agent]);
            }
            Flash::success(__('backend.Order_Routed_saved_successfully'));
        } else {
            foreach ($request->select as $key => $value) {
                //update order status to be 1 when it assign to driver
                Order::find($value)->update(['status' => 1, 'accepted_at' => Carbon::now()]);
                $order = new AcceptedOrder();
                $order->order_id = Order::where('id', $value)->value('id');
                $order->driver_id = $request->driverID;
                $order->delivery_price = Order::where('id', $value)->value('delivery_price');
                $order->status = 0;
                $order->save();

                //send notification

                $data = [];
                $data['title'] = 'Accept Request';
                $data['message'] = 'Captain accept your request No # ' . $order->order_no . ' .. :)';
                $data['object_id'] = $value;
                $data['type'] = 2;
                $data['client_id'] = Order::where('id', $value)->value('customer_id');
                $data['client_model'] = '2';

//                (new NotificationController())->new_notify($data);


                $data = [];
                $data['title'] = 'Forward Request';
                $data['message'] = 'Request No # ' . $order->order_no . ' added to your List .. :)';
                $data['object_id'] = $request->order_id;
                $data['type'] = 2;
                $data['client_id'] = $request->driverID;
                $data['client_model'] = '1';

//                (new NotificationController())->new_notify($data);


            }

            event(new OrderUpdated($request->select, Auth::guard($this->guardType)->user()->id, 1, 'forward', ['driver_id' => $request->driverID]));


            Flash::success(__('backend.Order_Routed_saved_successfully'));
        }
        return back();
    }/* Forward Orders To Agent Or Driver function*/

    public function cancel_forward_to_another_captain(Request $request)
    {

        /** this fn used to cancel order from driver and forward to another driver
         * $request->accepted_id :: accept order row from old driver
         * $request->items :: new driver that order assign to him
         */
        DB::beginTransaction();
        try {

            $id = $request->accepted_id;
            $order_id = $request->order_id;
            $new_driver = $request->items;
            $driver_id = $request->driver_id;
            $orderCheck = Order::where('id', $order_id)->first();
            if ($orderCheck) {
                $update_other_driver = AcceptedOrder::where('order_id', $order_id)->get();
                if (count($update_other_driver) > 0) {
                    #cancel from other driver mean update accepted order with status 4 and cancelled_by :: 1 if admin make this action
                    AcceptedOrder::where('order_id', $order_id)->update(['status' => '4', 'cancelled_at' => Carbon::now(), 'cancelled_by' => 1]);
                }
                $captain = Driver::find($request->items);
                /** then create new accepted order with new captain */
                $other_driver = new AcceptedOrder();
                $other_driver->order_id = $request->order_id;
                $other_driver->driver_id = $request->items;
                $other_driver->delivery_price = $orderCheck->delivery_price;
                if ($captain) {
                    $other_driver->driver_bonus = $captain->bouns_of_delivery;
                }
                $other_driver->status = 0;

                $other_driver->save();
                $update_other_order = Order::find($order_id)->update(
                    [
                        'status' => '1',
                        'accepted_at' => Carbon::now(),
                        'received_at' => null
                    ]);

                if ($request->forward_fees == 1) {
                    if (count($update_other_driver) > 0) {
                        foreach ($update_other_driver as $update_driver) {
                            InvoiceOrder::join('invoices', 'invoices.id', 'invoice_orders.invoice_id')
                                ->where('accepted_order_id', $update_driver->id)
                                ->where('invoices.type', 1)
                                ->where('invoices.object_id', $driver_id)
                                ->delete();

                        }
                    }
                }

                /*  //send notification
                  $data 							= [];
                  $data['title'] 					= 'Accept Request';
                  $data['message'] 				= 'Captain accept your request No # '.$orderCheck->order_no.' .. :)';
                  $data['object_id'] 				= $request->order_id;
                  $data['type'] 					= 2;
                  $data['client_id'] 				= Order::where('id', $request->order_id)->value('customer_id');
                  $data['client_model'] 			= '2';

                  (new NotificationController())->new_notify($data);

              */
                $data = [];
                $data['title'] = 'Forward Request';
                $data['message'] = 'Request added to your List .. :)';
                $data['object_id'] = $request->order_id;
                $data['type'] = 2;
                $data['client_id'] = $request->items;
                $data['client_model'] = '1';

//                (new NotificationController())->new_notify($data);

                Flash::success(__('backend.Order_Forward_saved_successfully'));
                DB::commit();
            } else {
                Flash::error(__('backend.Order_Forward_not_Saved_Something_Error'));
            }
        } catch (Exception $e) {
            DB::rollback();
            Flash::error(__('backend.Order_Forward_not_Saved_Something_Error'));
        }
        return back();
    }/* cancel_forward_to_another_captain */


    public function scan_page(Request $request)
    {
//        dd($this->randomNumber(1), $this->randomNumber(2), $this->randomNumber(3));
//        $status = app('request')->input('status');
//        $driver = app('request')->input('driver');

        $code = [];
        if (app('request')->has('code') && app('request')->input('code')) {
            $code = array_map('trim', explode(',', app('request')->input('code')));
        }

//        $pattern = "/\"order_id\":([0-9]*)/";
//        $matches = preg_match_all($pattern, $code, $ids);

        /** type to display any order  */
//        $type = 'all';
        $orders = null;

//        if (count($ids) && isset($ids[1])) {
//            $orders = Order::whereIn('id', $ids[1])->paginate(50);
//        }

        if (!empty($code) && isset($code)) {
            $orders = Order::where('status', '!=', 0)
                ->where(function ($query) use ($code) {
                    $query->whereIn('receiver_code', $code)
                        ->orWhereIn('order_number', $code)
                        ->orWhereIn('order_no', $code);

                });

//            if ($status || $status === '0') {
//                $orders->where('orders.status', $status);
//            }
//
//            if ($driver) {
//                $orders->join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
//                    ->where('accepted_orders.status', '!=', 4)
//                    ->where('driver_id', $driver);
//            }

            $orders = $orders->select('orders.*')->first();

        }

//        if (app('request')->has('status')) {
//            if ($status === '0') {
//                $type = 'pending';
//            } else if ($status == 1) {
//                $type = 'accept';
//            } else if ($status == 2) {
//                $type = 'receive';
//            } else if ($status == 3) {
//                $type = 'deliver';
//            } else if ($status == 4) {
//                $type = 'cancel';
//            } else if ($status == 5) {
//                $type = 'recall';
//            } else if ($status == 6) {
//                $type = 'waiting';
//            } else if ($status == 7) {
//                $type = "dropped";
//            }
//        }

        $agents = Agent::get();

        /** if this ajax request mean that scan button pressed */
        if ($request->ajax()) {

            $data = [
                'count' => ($orders ? 1 : 0),
                'id' => ($orders ? $orders->id : 0)
            ];

            if (app('request')->input('init') == 1) {
                $data['view'] = view('backend.orders.scan-table', compact('orders', 'agents'))->render();
            } else {
                $data['view'] = view('backend.orders.scan-tr', compact('orders', 'agents'))->render();

            }

            return $data;
        }

        $problems = DeliveryProblem::get();

        $online_drivers = Driver::where('agent_id', 1)->get();

        // display scan page if request get from browser not ajax
        return view('backend.orders.scan', compact('online_drivers', 'problems'));
    }/*scan_page*/

    public function print_order_pdf_byid($order_id)
    {
        $order = Order::where('id', $order_id)->first();
        $customer = Customer::where('id', $order->customer_id)->value('corporate_id');
        $corporteName = Corporate::where('id', $customer)->value('name');
        return view('backend.orders.pdf', compact('order', 'corporteName'));
    }/*print_order_pdf_byid*/

    public function cancel_order_client(Request $request)
    {
        #cancel order according to client want
        $order = Order::where('id', $request->id)->first();
        if ($order && $order->status != 3 && $order->status != 4) {
            #cancel order update status with 4 and cancelled_by 2 if client want to cancel order
            $orderReceivedCount = AcceptedOrder::where('order_id', $request->id)->where('old_status', 2)->count();
            if (($order->status == 2 && $orderReceivedCount >= 2) || (in_array($order->status, [1, 7]) && $orderReceivedCount >= 1)) {
                $acceptedOrder = AcceptedOrder::where('order_id', $request->id)->orderBy('created_at', 'DESC')->first();
                $acceptedOrder->update(['status' => 5]);
                Order::find($request->id)
                    ->update(['status' => 5, 'recalled_at' => date("Y-m-d H:i:s"), 'recalled_by' => 2]);
                event(new OrderUpdated([$request->id], Auth::guard($this->guardType)->user()->id, 1, 'recall', ['recalled_by' => 2]));
            } else {
                AcceptedOrder::where('order_id', $request->id)->whereIn('status', [0, 2])
                    ->update(['status' => 4, 'cancelled_by' => 1, 'cancelled_at' => date("Y-m-d H:i:s")]);
                $status_before_cancel = $order->status;
                Order::find($request->id)
                    ->update(['status_before_cancel' => $status_before_cancel, 'status' => '4', 'cancelled_by' => '2', 'cancelled_at' => Carbon::now()]);
                event(new OrderUpdated([$request->id], Auth::guard($this->guardType)->user()->id, 1, 'cancel', ['cancelled_by' => 2]));
            }

            return __('backend.Order_Cancel_successfully');
        } else {
            return 'false';
        }
    }/*cancel_order_client == By samar*/

    public function cancel_order_admin(Request $request)
    {
        #cancel order according to client want
        $order = Order::where('id', $request->id)->first();
        if ($order && $order->status != 3 && $order->status != 4) {
            #cancel order
            /** cancel by in this case 1  */

            $orderReceivedCount = AcceptedOrder::where('order_id', $request->id)->where('old_status', 2)->count();
            if (($order->status == 2 && $orderReceivedCount >= 2) || (in_array($order->status, [1, 7]) && $orderReceivedCount >= 1)) {
                $acceptedOrder = AcceptedOrder::where('order_id', $request->id)->orderBy('created_at', 'DESC')->first();
                $acceptedOrder->update(['status' => 5]);
                Order::find($request->id)
                    ->update(['status' => 5, 'recalled_at' => date("Y-m-d H:i:s"), 'recalled_by' => 2]);
                event(new OrderUpdated([$request->id], Auth::guard($this->guardType)->user()->id, 1, 'recall', ['recalled_by' => 2]));
            } else {
                AcceptedOrder::where('order_id', $request->id)->whereIn('status', [0, 2])
                    ->update(['status' => 4, 'cancelled_by' => 1, 'cancelled_at' => date("Y-m-d H:i:s")]);
                $status_before_cancel = $order->status;
                Order::find($request->id)
                    ->update(['status_before_cancel' => $status_before_cancel, 'status' => '4', 'cancelled_by' => '1', 'cancelled_at' => Carbon::now()]);
                event(new OrderUpdated([$request->id], Auth::guard($this->guardType)->user()->id, 1, 'cancel', ['cancelled_by' => 1]));
            }

            return __('backend.Order_Cancel_successfully');
        } else {
            return 'false';
        }
    }/*cancel_order_by_admin  == By samar*/

    public function printCollection($id, $type)
    {
        $orders = Order::where('collection_id', $id)->orderBy('id', 'desc')->get();
        return view('backend.collection_orders.printCollectionOrder', compact('orders', 'type'));
    }/*Print Collection Orders*/

    public function warehouse_dropoff(Request $request)
    {
        foreach ($request->ids as $id) {
            Order::where('id', $id)->update(['warehouse_dropoff' => 1, 'warehouse_dropoff_date' => date('Y-m-d H:i:s')]);
        }

//        Flash::success(__('backend.drop_off_package'));

        return response()->json(true);

    }

    public function head_office(Request $request)
    {
        foreach ($request->ids as $id) {
            Order::where('id', $id)->update(['head_office' => ($request->head_office == 'true' ? 1 : 0)]);
        }

//        Flash::success(__('backend.head_office'));

        return response()->json(true);

    }

    public function client_dropoff(Request $request)
    {
        foreach ($request->ids as $id) {
            Order::where('id', $id)->update(['client_dropoff' => 1, 'client_dropoff_date' => date('Y-m-d H:i:s')]);
        }

//        Flash::success(__('backend.drop_off_package'));

        return response()->json(true);

    }

    public function delay_orders(Request $request)
    {

        $data = ['ids' => []];

        $ids = app('request')->input('ids');

        if (is_array($ids) && count($ids)) {
            DB::beginTransaction();
            try {
                if ($request->change_status == 1) {
                    $accepted_orders = AcceptedOrder::whereIn('order_id', $ids)
                        ->whereIn('status', [0, 2])
                        ->pluck('id')->toArray();
                    AcceptedOrder::whereIn('id', $accepted_orders)->update(['status' => 4]);
                    Order::whereIn('id', $ids)->update(['status' => 7]);
                    InvoiceOrder::whereIn('accepted_order_id', $accepted_orders)->delete();
                    event(new OrderUpdated($ids, Auth::guard($this->guardType)->user()->id, 1, 'dropped'));
                }

                $comment = $request->delay_reason ? $request->delay_reason : $request->delay_comment;
                foreach ($ids as $id) {
                    $data = ['delay_at' => $request->delay_date, 'delay_comment' => $comment, 'order_id' => $id];
                    $data['change_status'] = $request->change_status;
                    $data['created_by'] = Auth::guard($this->guardType)->user()->id;

                    OrderDelay::create($data);
                }
                event(new OrderUpdated($ids, Auth::guard($this->guardType)->user()->id, 1, 'delay', ['message' => $comment, 'delay_at' => $request->delay_date]));

                $data['ids'] = $ids;
                $data['result'] = true;
                if ($request->change_status == 1) {
                    $data['status'] = "<span class='badge badge-pill label-black'>" . __('backend.dropped') . "</span>";
                }
                $data['message'] = __('backend.delay_orders_successfully');

                if ($request->flash == 1) {
                    Flash::success(__('backend.delay_orders_successfully'));
                }

                DB::commit();
            } catch (Exception $e) {
                DB::rollback();
                $data['result'] = false;
                $data['message'] = __('backend.Something_Error');

                if ($request->flash == 1) {
                    Flash::error(__('backend.Something_Error'));
                }
            }
        }

        return response()->json($data);
    }

    public function deliver_orders(Request $request)
    {

        $data = array();

        if (count($request->ids)) {
            DB::beginTransaction();
            try {
                AcceptedOrder::whereIn('order_id', $request->ids)->where('status', 2)->update(['status' => 3, 'delivery_date' => date("Y-m-d H:i:s")]);
                Order::whereIn('id', $request->ids)->update(['status' => 3, 'delivered_at' => date("Y-m-d H:i:s")]);
                event(new OrderUpdated($request->ids, Auth::guard($this->guardType)->user()->id, 1, 'deliver'));
//                $data['status'] = true;
//                $data['message'] = __('backend.orders_delivered');
                Flash::success(__('backend.orders_delivered'));
                DB::commit();
            } catch (Exception $e) {
                DB::rollback();
//                $data['status'] = false;
//                $data['message'] = __('backend.Something_Error');
                Flash::error(__('backend.Something_Error'));
            }
        }

        return response()->json($data);
    }

    public function receive_orders(Request $request)
    {

        $data = array();

        if (count($request->ids)) {
            DB::beginTransaction();
            try {
                $accepted_orders = AcceptedOrder::with(['driver', 'order' => function ($query) {
                    $query->with(['customer']);
                }])->whereIn('order_id', $request->ids)->where('status', 0)->get();

                foreach ($accepted_orders as $accepted_order) {
                    $type = 1;
                    if ($accepted_order->driver->agent_id != 1 && $accepted_order->driver->agent_id != null) {
                        $type = 3;
                    }

                    $invoiceCaptain = Invoice::firstOrCreate(
                        ['object_id' => $accepted_order->driver->id, 'type' => $type, 'status' => 0],
                        [
                            'invoice_no' => uniqid(),
                            'object_id' => $accepted_order->driver->id,
                            'type' => $type,
                            'status' => 0,
                            'start_at' => date("Y-m-d"),
                            'start_date' => date("Y-m-d H:i:s")
                        ]
                    );

                    InvoiceOrder::create([
                        'accepted_order_id' => $accepted_order->id,
                        'invoice_id' => $invoiceCaptain->id,
                        'price' => $accepted_order->order->delivery_price,
                    ]);

                    $invoice = Invoice::firstOrCreate(
                        ['object_id' => $accepted_order->order->customer->corporate_id, 'type' => 2, 'status' => 0],
                        [
                            'invoice_no' => uniqid(),
                            'object_id' => $accepted_order->order->customer->corporate_id,
                            'type' => 2,
                            'status' => 0,
                            'start_at' => date("Y-m-d"),
                            'start_date' => date("Y-m-d H:i:s")
                        ]
                    );

                    InvoiceOrder::create([
                        'accepted_order_id' => $accepted_order->id,
                        'invoice_id' => $invoice->id,
                        'price' => $accepted_order->order->delivery_price,
                    ]);

                }

                AcceptedOrder::whereIn('order_id', $request->ids)->where('status', 0)->update(['status' => 2, 'old_status' => 2, 'received_date' => date("Y-m-d H:i:s")]);
                Order::whereIn('id', $request->ids)->update(['status' => 2, 'received_at' => date("Y-m-d H:i:s")]);

                event(new OrderUpdated($request->ids, Auth::guard($this->guardType)->user()->id, 1, 'receive_orders'));

//                $data['status'] = true;
//                $data['message'] = __('backend.orders_received');
                Flash::success(__('backend.orders_received'));
                DB::commit();
            } catch (Exception $e) {
                DB::rollback();
//                $data['status'] = false;
//                $data['message'] = __('backend.Something_Error');
                Flash::error(__('backend.Something_Error'));
            }
        }

        return response()->json($data);
    }


    public function add_problem(Request $request)
    {

//        DB::beginTransaction();
        try {
            $before_6_hours = date("Y-m-d H:i:s", strtotime('-6 hours'));
            $problem = OrderDeliveryProblem::where('order_id', $request->order_id)
                ->where('created_at', '>', $before_6_hours)
                ->orderBy('created_at', 'DESC')
                ->first();

            if ($problem) {
                Flash::error(__('backend.cannot_send_delivery_problem'));
            } else {
                $data = $request->only('order_id', 'latitude', 'longitude', 'driver_id', 'delivery_problem_id', 'reason', 'delay_at', 'created_at');
                if (!$data['created_at']) {
                    unset($data['created_at']);
                }
                $data['created_by'] = Auth::guard($this->guardType)->user()->id;

                $problem_radio = $request->problem_radio;

                OrderDeliveryProblem::create($data);
                event(new OrderUpdated([$request->order_id],
                    Auth::guard($this->guardType)->user()->id,
                    1,
                    'problems',
                    ['reason' => $request->get('reason'), 'problem_id' => $request->get('delivery_problem_id')]
                ));

                $problem_count = OrderDeliveryProblem::where('order_id', $request->order_id)->count();

                $order = Order::find($request->order_id);
                if ($order->status == 2) {
                    if ($problem_count >= 5 || $problem_radio === 'recall') {
                        AcceptedOrder::where('order_id', $request->order_id)
                            ->where('status', 2)
                            ->update(['status' => 5]);
                        Order::where('id', $request->order_id)
                            ->where('status', 2)
                            ->update(['status' => 5, 'recalled_at' => date("Y-m-d H:i:s"), 'recalled_by' => 2]);
                        event(new OrderUpdated([$request->order_id], Auth::guard($this->guardType)->user()->id, 1, 'recall', ['recalled_by' => 2]));
                    } else if ($problem_radio === 'delay') {
                        AcceptedOrder::where('order_id', $request->order_id)
                            ->where('status', 2)
                            ->update(['status' => 4, 'cancelled_by' => 1, 'cancelled_at' => date("Y-m-d H:i:s")]);
                        Order::where('id', $request->order_id)
                            ->where('status', 2)
                            ->update(['status' => 7, 'delay_at' => $request->delay_at]);
                        event(new OrderUpdated([$request->order_id], Auth::guard($this->guardType)->user()->id, 1, 'dropped'));
                    }
                }

                Flash::success(__('backend.add_problem_successfully'));
                DB::commit();
            }
        } catch (Exception $e) {
//            DB::rollback();
            Flash::error(__('backend.Something_Error'));
        }

        return redirect()->back();
    }

    public function delete_problem($id)
    {
        OrderDeliveryProblem::destroy($id);
        Flash::warning(__('backend.problem_deleted_successfully'));
        return redirect()->back();
    }

    public function addOrderComment(Request $request)
    {

        $message = '';
        DB::beginTransaction();
        try {

            $data = $request->only('order_id', 'latitude', 'longitude', 'driver_id', 'comment', 'created_at', 'user_type');
            if (!$request->created_at) {
                unset($data['created_at']);
            }
            $data['user_id'] = Auth::guard($this->guardType)->user()->id;

            OrderComment::create($data);
            event(new OrderUpdated([$request->order_id],
                Auth::guard($this->guardType)->user()->id,
                1,
                'comments',
                ['comment' => $request->get('comment')]
            ));

            if ($request->ajax()) {
                $message = __('backend.add_order_comment_successfully');
            } else {
                Flash::success(__('backend.add_order_comment_successfully'));
            }

            DB::commit();

        } catch (Exception $e) {
            if ($request->ajax()) {
                $message = __('backend.Something_Error');
            } else {
                Flash::error(__('backend.Something_Error'));
            }

            DB::rollback();
        }

        if ($request->ajax()) {
            return response()->json(['message' => $message]);
        } else {
            return redirect()->back();
        }
    }

    public function deleteOrderComment($id)
    {
        OrderComment::destroy($id);
        Flash::warning(__('backend.order_comment_deleted_successfully'));
        return redirect()->back();
    }

    public function action_back(Request $request)
    {
        $order_ids = $request->ids;
        $status = $request->action_status;
        try {
            foreach ($order_ids as $order_id) {
                $order = Order::where('id', $order_id)->where('is_refund', 0)->first();
                if ($order) {
                    if ($status == 2) {
                        if ($order->status == 3) {
                            AcceptedOrder::where('order_id', $order_id)
                                ->where('status', 3)
                                ->update(['status' => 2, 'delivery_date' => null]);
                            Order::where('id', $order_id)
                                ->update(['status' => 2, 'delivered_at' => null, 'delivery_status' => null, 'collected_cost' => null]);
                        } elseif ($order->status == 5) {
                            AcceptedOrder::where('order_id', $order_id)
                                ->where('status', 5)
                                ->update(['status' => 2]);
                            Order::where('id', $order_id)
                                ->update(['status' => 2, 'recalled_at' => null, 'recalled_by' => null, 'delivery_status' => null, 'collected_cost' => null]);
                        } elseif ($order->status == 8) {
                            AcceptedOrder::where('order_id', $order_id)
                                ->where('status', 8)
                                ->update(['status' => 2]);
                            Order::where('id', $order_id)
                                ->update(['status' => 2, 'rejected_at' => null, 'delivery_status' => null, 'collected_cost' => null]);
                        } elseif ($order->status == 4) {
                            AcceptedOrder::where('order_id', $order_id)
                                ->where('status', 4)
                                ->update(['status' => 2, 'cancelled_at' => null, 'cancelled_by' => null, 'received_date' => \DB::raw('updated_at')]);
                            Order::where('id', $order_id)
                                ->update(['status' => 2, 'cancelled_at' => null, 'cancelled_by' => null,
                                    'accepted_at' => \DB::raw('updated_at'), 'received_at' => \DB::raw('updated_at'),
                                    'delivery_status' => null, 'collected_cost' => null]);
                        }
                        event(new OrderUpdated([$order_id], Auth::guard($this->guardType)->user()->id, 1, 'receive_orders'), ['update_reason' => 'action_back']);
                    } elseif ($status == 3) {
                        if ($order->status == 5) {
                            AcceptedOrder::where('order_id', $order_id)
                                ->where('status', 5)
                                ->update(['status' => 3, 'delivery_date' => \DB::raw('received_date')]);
                            Order::where('id', $order_id)
                                ->update(['status' => 3, 'recalled_at' => null, 'recalled_by' => null,
                                    'received_at' => \DB::raw('accepted_at'), 'delivered_at' => \DB::raw('accepted_at'),
                                    'delivery_status' => null, 'collected_cost' => null]);
                        } elseif ($order->status == 8) {
                            AcceptedOrder::where('order_id', $order_id)
                                ->where('status', 8)
                                ->update(['status' => 3, 'delivery_date' => \DB::raw('received_date')]);
                            Order::where('id', $order_id)
                                ->update(['status' => 3, 'rejected_at' => null,
                                    'received_at' => \DB::raw('accepted_at'), 'delivered_at' => \DB::raw('accepted_at'),
                                    'delivery_status' => null, 'collected_cost' => null]);
                        }
                        event(new OrderUpdated([$order_id], Auth::guard($this->guardType)->user()->id, 1, 'deliver'), ['update_reason' => 'action_back']);
                    } elseif ($status == 7) {
                        if ($order->status == 2) {
                            AcceptedOrder::where('order_id', $order_id)
                                ->where('status', 2)
                                ->update(['status' => 4]);
                            Order::where('id', $order_id)
                                ->update(['status' => 7, 'accepted_at' => null,
                                    'captain_received_time' => null, 'delivery_status' => null, 'collected_cost' => null,
                                    'dropped_at' => \DB::raw('received_at'), 'received_at' => null]);
                        } elseif ($order->status == 3) {
                            AcceptedOrder::where('order_id', $order_id)
                                ->where('status', 3)
                                ->update(['status' => 4]);
                            Order::where('id', $order_id)
                                ->update(['status' => 7, 'received_at' => null, 'accepted_at' => null,
                                    'captain_received_time' => null, 'delivery_status' => null, 'collected_cost' => null,
                                    'dropped_at' => \DB::raw('delivered_at'), 'delivered_at' => null]);
                        } elseif ($order->status == 5) {
                            AcceptedOrder::where('order_id', $order_id)
                                ->where('status', 5)
                                ->update(['status' => 4]);
                            Order::where('id', $order_id)
                                ->update(['status' => 7, 'recalled_by' => null,
                                    'received_at' => null, 'accepted_at' => null, 'captain_received_time' => null,
                                    'delivery_status' => null, 'collected_cost' => null, 'dropped_at' => \DB::raw('recalled_at'),
                                    'recalled_at' => null,]);
                        } elseif ($order->status == 8) {
                            AcceptedOrder::where('order_id', $order_id)
                                ->where('status', 8)
                                ->update(['status' => 4]);
                            Order::where('id', $order_id)
                                ->update(['status' => 7,
                                    'received_at' => null, 'accepted_at' => null, 'captain_received_time' => null,
                                    'delivery_status' => null, 'collected_cost' => null, 'dropped_at' => \DB::raw('recalled_at'),
                                    'rejected_at' => null,]);
                        } elseif ($order->status == 4) {
                            Order::where('id', $order_id)
                                ->update(['status' => 7, 'recalled_at' => null, 'recalled_by' => null,
                                    'received_at' => null, 'accepted_at' => null, 'captain_received_time' => null,
                                    'delivery_status' => null, 'collected_cost' => null, 'dropped_at' => \DB::raw('cancelled_at'),
                                    'cancelled_at' => null]);
                        }

                        event(new OrderUpdated([$order_id], Auth::guard($this->guardType)->user()->id, 1, 'dropped'), ['update_reason' => 'action_back']);
                    } elseif ($status == 5) {
                        if ($order->status == 2) {
                            AcceptedOrder::where('order_id', $order_id)
                                ->where('status', 2)
                                ->update(['status' => 5]);
                            Order::where('id', $order_id)
                                ->update(['status' => 5, 'recalled_at' => \DB::raw('received_at'), 'recalled_by' => 2,
                                    'delivery_status' => null, 'collected_cost' => null]);
                        } elseif ($order->status == 3) {
                            AcceptedOrder::where('order_id', $order_id)
                                ->where('status', 3)
                                ->update(['status' => 5]);
                            Order::where('id', $order_id)
                                ->update(['status' => 5, 'recalled_at' => \DB::raw('delivered_at'), 'recalled_by' => 2,
                                    'delivered_at' => null, 'delivery_status' => null, 'collected_cost' => null]);
                        }
                        event(new OrderUpdated([$order_id], Auth::guard($this->guardType)->user()->id, 1, 'recall', ['update_reason' => 'action_back', 'recalled_by' => 2]));
                    } elseif ($status == 8) {
                        if ($order->status == 2) {
                            AcceptedOrder::where('order_id', $order_id)
                                ->where('status', 2)
                                ->update(['status' => 8]);
                            Order::where('id', $order_id)
                                ->update(['status' => 8, 'rejected_at' => \DB::raw('received_at'),
                                    'delivery_status' => null, 'collected_cost' => null]);
                        } elseif ($order->status == 3) {
                            AcceptedOrder::where('order_id', $order_id)
                                ->where('status', 3)
                                ->update(['status' => 8]);
                            Order::where('id', $order_id)
                                ->update(['status' => 8, 'rejected_at' => \DB::raw('delivered_at'),
                                    'delivered_at' => null, 'delivery_status' => null, 'collected_cost' => null]);
                        }
                        event(new OrderUpdated([$order_id], Auth::guard($this->guardType)->user()->id, 1, 'reject', ['update_reason' => 'action_back']));
                    } elseif ($status == 4) {
                        if ($order->status == 0) {
                            Order::where('id', $order_id)
                                ->update(['status' => 4, 'received_at' => null, 'accepted_at' => null,
                                    'captain_received_time' => null, 'cancelled_at' => \DB::raw('updated_at'),
                                    'cancelled_by' => 2, 'status_before_cancel' => 0, 'delivery_status' => null, 'collected_cost' => null]);
                        } elseif ($order->status == 7) {
                            Order::where('id', $order_id)
                                ->update(['status' => 4, 'received_at' => null, 'accepted_at' => null,
                                    'captain_received_time' => null, 'cancelled_at' => \DB::raw('updated_at'),
                                    'cancelled_by' => 2, 'status_before_cancel' => 7, 'delivery_status' => null, 'collected_cost' => null]);
                        }
                        event(new OrderUpdated([$order_id], Auth::guard($this->guardType)->user()->id, 1, 'cancel', ['update_reason' => 'action_back', 'cancelled_by' => 2]));
                    } elseif ($status == 0) {
                        if ($order->status == 2) {
                            AcceptedOrder::where('order_id', $order_id)
                                ->where('status', 2)
                                ->update(['status' => 4]);
                            Order::where('id', $order_id)
                                ->update(['status' => 0, 'received_at' => null, 'accepted_at' => null,
                                    'captain_received_time' => null, 'delivery_status' => null, 'collected_cost' => null]);
                        } elseif ($order->status == 3) {
                            AcceptedOrder::where('order_id', $order_id)
                                ->where('status', 3)
                                ->update(['status' => 4]);
                            Order::where('id', $order_id)
                                ->update(['status' => 0, 'received_at' => null, 'accepted_at' => null,
                                    'captain_received_time' => null, 'delivered_at' => null, 'delivery_status' => null, 'collected_cost' => null]);
                        } elseif ($order->status == 4) {
                            Order::where('id', $order_id)
                                ->update(['status' => 0, 'received_at' => null, 'accepted_at' => null,
                                    'captain_received_time' => null, 'cancelled_at' => null, 'cancelled_by' => null,
                                    'delivery_status' => null, 'collected_cost' => null]);
                        } elseif ($order->status == 7) {
                            Order::where('id', $order_id)
                                ->update(['status' => 0, 'dropped_at' => null, 'delivery_status' => null, 'collected_cost' => null]);
                        }
                        event(new OrderUpdated([$order_id], Auth::guard($this->guardType)->user()->id, 1, 'pending', ['update_reason' => 'action_back']));
                    }
                }
            }

            Flash::success(__('backend.action_back_successfully'));
            return [];

        } catch (Exception $e) {

            Flash::error(__('backend.action_back_Error'));
            return [];

        }
    }

    public function recall_if_dropped($id)
    {
        $order = Order::findOrFail($id);
        if (isset($order)) {
            $order->recall_if_dropped = 1;
            $order->save();
            event(new OrderUpdated([$id], Auth::guard($this->guardType)->user()->id, 1, 'recall_if_dropped'));
            Flash::warning(__('backend.Order_Updated_successfully'));
        }

        return redirect()->back();
    }
}

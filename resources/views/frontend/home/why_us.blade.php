
<!-- Start About -->
<section id="about" class="about-x">
    <div class="container">
        <!-- Start Title -->
        <div class="col-xs-12">
            <div class="title">

                <h3>@lang('website.why_us')</h3>
                <p>
                   @if(! empty($app_about) )  {!! app()->getLocale() == 'en' ? $app_about['why_us'] : $app_about['why_us_ar'] !!} @endif
                </p>
            </div>
        </div>
        <!-- End Title -->
        <!-- Start Block -->

        @if(! empty($why_us ))
            @foreach($why_us as $service)
                <div class="col-md-4 col-xs-12">
                    <div class="block-item">
                        <div class="icon-block">
                            <img src="{{$service->image}}" alt="" />
                        </div>
                        <div class="details-serv">
                            <h1 class="pre-wrap">{{$service->title}}</h1>
                            <p class="pre-wrap">
                               {{$service->description}}
                            </p>
                        </div>
                    </div>


                </div>
            @endforeach

            <div class="col-xs-12">
                <a href="{{ app()->getLocale() == 'en' ? url('/en/services') : url('/services') }}" class="btn btn-style">@lang('website.more')</a>
            </div>
        @endif
    </div>
</section>
<!-- End About -->

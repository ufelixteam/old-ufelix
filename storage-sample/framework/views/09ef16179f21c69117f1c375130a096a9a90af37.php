<div class="modal fade" id="box-id" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" 
            aria-hidden="true">
  	<div class="modal-dialog cascading-modal" role="document">
    	<!--Content-->
    	<div class="modal-content">
     		<div class="modal-c-tabs">
            	<div class="modal-body mb-1">
            		<div class="box-id">
					<div class="icon-logo">
						<img src="<?php echo e(asset('/website/images/flogo.png')); ?>" alt="" />
					</div>
					<div class="box-head">
						<h1><?php echo app('translator')->getFromJson('website.track_order'); ?> </h1>
						<span><?php echo app('translator')->getFromJson('website.track_order_hint'); ?></span>
					</div>
					<div class="box-body">
						
						<form action="<?php echo e(url('tracking')); ?>" action="get">
							<p>
								<label class="contw"><?php echo app('translator')->getFromJson('website.sender'); ?>
									<input type="radio" checked="<?php echo e(! empty($search_type) && $search_type == 1 ? 'checked' : ''); ?>" value="1" name="check" required="true" />
									<span class="checkmark"></span>
								</label>
							</p>
							<p>
								<label class="contw"><?php echo app('translator')->getFromJson('website.receiver'); ?>
									<input type="radio" name="check" checked="<?php echo e(! empty($search_type) && $search_type == 2 ? 'checked' : ''); ?>" value="2" />
									<span class="checkmark"></span>
								</label>
							</p>
							<input type="text" placeholder="<?php echo app('translator')->getFromJson('website.order_no'); ?>"  name="search" value="<?php echo e(! empty($search) ? $search : ''); ?>" />
							<button type="submit"><i class="fa fa-search"></i></button>
						</form>
						
					</div>
				</div>

            	</div>

            </div>

        </div>

    </div>

</div>

<?php /**PATH /home/www/ufelix/ufelix/resources/views/frontend/layouts/track.blade.php ENDPATH**/ ?>
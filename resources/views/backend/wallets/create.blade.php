@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.the_wallets')}} - {{__('backend.add_wallet')}}</title>
@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-plus"></i> {{ $header }} {{__('backend.add_wallet')}} </h3>
    </div>
@endsection

@section('content')
    @include('error')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <form action="{{ route('mngrAdmin.wallets.store') }}" method="POST" name='wallet_create'>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input id="type" type="hidden" name="type" value="{{ $type }}">
                <div hidden class="form-group col-md-6 col-sm-6 @if($errors->has('value')) has-error @endif">
                    <label for="value-field"> {{__('backend.value')}}</label>
                    <input type="hidden" min='0' id="value-field" name="value" class="form-control" value="0"/>
                    @if($errors->has("value"))
                        <span class="help-block">{{ $errors->first("value") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-6 col-sm-6 @if($errors->has('object_id')) has-error @endif">
                    <label for="object_id-field">{{ $header }}</label>
                    <select class="form-control" id="object_id" name="object_id">
                        @foreach($users as $value)
                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                        @endforeach
                    </select>
                    <span class="error" id="object_error"
                          name="object_error">{{__('backend.User_has_Already_Wallet')}}</span>
                    @if($errors->has("object_id"))
                        <span class="help-block">{{ $errors->first("object_id") }}</span>
                    @endif
                </div>
                <div class="clearfix"></div>
                <div class="form-group col-md-6 col-sm-6 @if($errors->has('status')) has-error @endif">
                    <label for="status-field">{{__('backend.status')}}</label>
                    <select class="form-control" name="status">
                        <option value="1">{{__('backend.available')}}</option>
                        <option value="0">{{__('backend.Not_Available')}}</option>
                    </select>
                    @if($errors->has("status"))
                        <span class="help-block">{{ $errors->first("status") }}</span>
                    @endif
                </div>
                <div class="well well-sm col-md-12 col-sm-12 col-xs-12">
                    <button type="submit" id="form_submit" class="btn btn-primary">{{__('backend.add_wallet')}}</button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.wallets.index') }}"><i
                            class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(function () {
            $("form[name='wallet_create']").validate({
                // Specify validation rules
                rules: {
                    value: {
                        required: true,
                        number: true,
                    },
                },
                // Specify validation error messages
                messages: {
                    value: {
                        required: "{{__('backend.Please_Enter_the_value')}}",
                    },
                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                },

                highlight: function (element) {
                    $(element).parent().addClass('error')
                },
                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                }
            });
        })
        $("select[name='object_id']").change(function () {
            var object_id = $(this).val();
            var type = $('#type').val();
            var token = $("input[name='_token']").val();
            $.ajax({
                url: "{!! url('/check-user-wallet') !!}",
                method: 'POST',
                data: {
                    object_id: object_id,
                    type: type,
                    _token: token
                },
                success: function (data) {
                    console.log(data);
                    if (data == "false") {
                        $("#object_error").show();
                        $("#form_submit").attr('disabled', true);
                    } else {
                        $("#object_error").hide();
                        $("#form_submit").removeAttr('disabled');
                    }
                }
            });
        });
        var object_id = $('#object_id').val();
        var token = $("input[name='_token']").val();
        $.ajax({
            url: "{!! url('/check-user-wallet') !!}",
            method: 'POST',
            data: {object_id: object_id, _token: token},
            success: function (data) {
                if (data == "false") {
                    $("#object_error").show();
                    $("#form_submit").attr('disabled', true);
                } else {
                    $("#object_error").hide();
                    $("#form_submit").removeAttr('disabled');
                }
            }
        });
    </script>
@endsection

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    //
    protected $table = 'feedback';

    public function getNameAttribute($value)
    {
    	if ($this->captain_id != "") {
    		# code...
    		$user = Driver::where('id',$this->captain_id)->first();
    	}else{
    		
    		$user = Customer::where('id',$this->customer_id)->first();
    	}

    	if (! empty($user)) {
    		return $user->name;
    	}else{
    		return $value;
    	}
    }

     public function getMobileAttribute($value)
    {
    	if ($this->captain_id != "") {
    		# code...
    		$user = Driver::where('id',$this->captain_id)->first();
    	}else{
    		
    		$user = Customer::where('id',$this->customer_id)->first();
    	}

    	if (! empty($user)) {
    		return $user->mobile;
    	}else{
    		return $value;
    	}
    }

     public function getEmailAttribute($value)
    {
    	if ($this->captain_id != "") {
    		# code...
    		$user = Driver::where('id',$this->captain_id)->first();
    	}else{
    		
    		$user = Customer::where('id',$this->customer_id)->first();
    	}

    	if (! empty($user)) {
    		return $user->email;
    	}else{
    		return $value;
    	}
    }

    public function getTypeSpanAttribute()
    {
        if ($this->captain_id != "") {
            $value="<span class='badge badge-pill label-info'>captain</span>";
        }else if ($this->customer_id != "") {
            $value="<span class='badge badge-pill label-warning'>Customer</span>";
        }else{
            $value = "<span class='badge badge-pill label-success'>website</span>";
        }
        return $value;
    }
}

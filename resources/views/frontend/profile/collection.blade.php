@extends('frontend.profile.layouts.app')

@section('title') @lang('website.FormCollection') @endsection
@section('css')
    <style>
        .pac-container {
            z-index: 9999 !important;
        }

    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card card-default">
                <div class="card-header">
                    <h4 class="card-title m-b-0">@lang('website.AddCollection') </h4>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="alert alert-danger" style="display:none"></div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="alert alert-success" style="display:none"></div>
                    </div>
                    <div class="container-fluid">
                        <div id="create-form" class="create-form">
                            @include('frontend.profile.collections.create_form')
                        </div>
                    </div>
                    <div class="row">
                        <div class="table-responsive">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="list">
                                    @include('frontend.profile.collections.tmptable')
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

        <div class="col-lg-12 col-md-12" style="margin-top: 30px">

            <button type="button" id="complete_later"
                    class="btn btn-info btn-rounded btn-block  @if(empty($collection) || (!empty($collection) && $collection->complete_later == 1)) hidden @endif">
                <i
                    class="fa fa-check"></i>@lang('website.send_orders')</button>

        </div>
        <div id="js-form" class="js-form"></div>

        @include('frontend.profile.collections.map')
        @include('frontend.profile.collections.map1')

        @endsection

        @section('scripts')

            <script type="text/javascript" src="{{ asset('/assets/scripts/map-order.js')}}"></script>
            <script
                src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key=AIzaSyDQiN4qc7pC9b1BRQwkqXXD28peMfWcHvw&callback=myMap"></script>
            @include('frontend.profile.collections.js')
            <script>
                $("#complete_later").on('click', function () {
                    if ($("#collection_id").val()) {
                        /*send to temp*/
                        $.ajax({
                            url: "{{ url('/customer/account/complete_collection')}}" + "/" + $("#collection_id").val(),
                            type: 'post',
                            data: {'_token': "{{csrf_token()}}"},
                            success: function (data) {
                                isDirty = true;
                                window.location.href = "{{ url('/'.app()->getLocale().'/customer/account/form-collections')}}";
                            },
                            error: function (data) {

                                console.log('Error:', data);
                            }
                        });
                    }
                });
                // $(window).on("unload", function(e) {
                //     $.ajax({
                //         type: 'POST',
                //         url: 'script.php',
                //         async:false,
                //         data: {key_leave:"289583002"}
                //     });
                // });
                // var isDirty = function() { return true; }
                //
                // window.onload = function() {
                //     window.addEventListener("beforeunload", function (e) {
                //         if (!isDirty()) {
                //             return undefined;
                //         }
                //
                //         var confirmationMessage = 'It looks like you have been editing something. '
                //             + 'If you leave before saving, your changes will be lost.';
                //
                //         (e || window.event).returnValue = confirmationMessage; //Gecko + IE
                //         return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
                //     });
                // };

                var isDirty = function () {
                    return false;
                }

                // window.onpageshow = function(event) {
                //     if (event.persisted) {
                //         window.isRefresh = true;
                //         window.location.reload();
                //     }
                // };

                window.onbeforeunload = function () {
                    if (/*!window.isRefresh &&*/ !isDirty()) {
                        $.ajax({
                            type: 'POST',
                            url: '{{url('customer/account/form-collections/delete/')}}' + '/'+$("#collection_id").val(),
                            async: true,
                            data: {'_token': "{{csrf_token()}}", collection_id: $("#collection_id").val()},
                            success: function (data) {
                            }
                        });
                    }
                    // window.isRefresh = false;
                }

                window.onload = function () {
                    window.addEventListener('beforeunload', function (e) {
                        if (!isDirty()) {
                            // Cancel the event as stated by the standard.
                            e.preventDefault();
                            // Chrome requires returnValue to be set.
                            e.returnValue = '';
                        }
                    });
                }
            </script>
    </div>
@endsection


@extends('backend.layouts.app')

@section('css')
    <title>{{__('backend.admins')}} - {{__('backend.view')}}</title>
@endsection

@section('header')
@endsection

@section('content')
    <div class="page-header">
        <h3>{{__('backend.admin')}} :: #{{$user->name}} </h3>{!! $user->block_span !!}
        <form action="{{ route('mngrAdmin.users.destroy', $user->id) }}" method="POST" style="display: inline;"
              onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
            @if(permission('blockedAdmin')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <button id="block" type="button" value="{{ $user->id }}"
                        class="btn btn-success">{{ $user->block_txt }}</button>
            @endif

            @if(permission('editAdmin')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <a class="btn btn-warning btn-group" role="group" href="{{ route('mngrAdmin.users.edit', $user->id) }}"><i
                        class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
            @endif

            @if(permission('deleteAdmin')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <button type="submit" class="btn btn-danger">{{__('backend.delete')}} <i
                        class="glyphicon glyphicon-trash"></i></button>
                @endif
                <a href="{{ URL::asset('mngrAdmin/corporates_report?user_id='.$user->id) }}"
                   class="btn btn-success btn-group" role="group"
                >{{__('backend.corporates_excel')}}</a>
            </div>
        </form>
    </div>

    <div class="row">
        <div class="col-md-12">
            <form action="#">
                <div class="form-group col-sm-4">
                    <label class="label-control" for="name">#</label>
                    <p class="form-control-static">{{$user->id}}</p>
                </div>

                <div class="form-group col-sm-4">
                    <label class="label-control" for="name">{{__('backend.name')}}</label>
                    <p class="form-control-static">{{$user->name}}</p>
                </div>
                <div class="form-group col-sm-4">
                    <label class="label-control" for="email">{{__('backend.email')}}</label>
                    <p class="form-control-static">{{$user->email}}</p>
                </div>
                <div class="form-group col-sm-4">
                    <label class="label-control" for="mobile">{{__('backend.mobile')}}</label>
                    <p class="form-control-static">{{$user->mobile}}</p>
                </div>
                <div class="form-group col-sm-4">
                    <label class="label-control" for="mobile"> {{__('backend.role')}} </label>
                    <p class="form-control-static">{{ ! empty($user->roleAdmin) ? $user->roleAdmin->name : ' Not Found ' }}</p>
                </div>
                <div class="form-group col-sm-4">
                    <label class="label-control" for="created_at"> {{__('backend.Create_at')}} </label>
                    <p class="form-control-static">{{$user->created_at}}</p>
                </div>
            </form>
            <div class="row col-md-12 pull-right">
                <a class="btn btn-link pull-right" href="{{url()->previous() }}"><i
                        class="glyphicon glyphicon-backward"></i> {{__('backend.back')}} </a>
            </div>

        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function () {
            $('#block').on('click', function () {
                var Status = $(this).val();
                // alert(Status);
                $.ajax({
                    url: "{{ url('/mngrAdmin/user/block/'.$user->id) }}",
                    Type: "GET",
                    dataType: 'json',
                    success: function (response) {
                        if (response == 0) {
                            $('#block').html("{{__('backend.Block')}}");
                        } else {
                            $('#block').html("{{__('backend.Unblock')}}");
                        }
                        location.reload();
                    }
                });
            });
        });
    </script>
@endsection

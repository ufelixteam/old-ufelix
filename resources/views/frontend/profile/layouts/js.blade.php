<script src="{{ asset('/profile/js/jquery.js')}}"></script>
<script src="{{ asset('/profile/js/bootstrap.min.js')}}"></script>


<script src="{{ asset('/profile/js/retina.js')}}"></script>
<script src="{{ asset('/profile/js/custom-sparkline.js')}}"></script>
<script src="{{ asset('/profile/js/jquery.scrollUp.js')}}"></script>
<script src="{{ asset('/profile/js/d3.v3.min.js')}}"></script>
<script src="{{ asset('/profile/js/d3.powergauge.js')}}"></script>
<script src="{{ asset('/profile/js/gauge.js')}}"></script>
<script src="{{ asset('/profile/js/gauge-custom.js')}}"></script>
<script src="{{ asset('/profile/js/c3.min.js')}}"></script>
<script src="{{ asset('/profile/js/c3.custom.js')}}"></script>
<script src="{{ asset('/profile/js/nv.d3.js')}}"></script>
<script src="{{ asset('/profile/js/nv.d3.custom.boxPlotChart.js')}}"></script>
<script src="{{ asset('/profile/js/nv.d3.custom.stackedAreaChart.js')}}"></script>
<script src="{{ asset('/profile/js/horizBarChart.min.js')}}"></script>
<script src="{{ asset('/profile/js/horizBarCustom.js')}}"></script>
<script src="{{ asset('/profile/js/gaugeMeter-2.0.0.min.js')}}"></script>
<script src="{{ asset('/profile/js/gaugemeter.custom.js')}}"></script>


<script src="{{ asset('/profile/js/peity.min.js')}}"></script>
<script src="{{ asset('/profile/js/custom-peity.js')}}"></script>
<script src="{{ asset('/profile/js/circliful.min.js')}}"></script>
<script src="{{ asset('/profile/js/circliful.custom.js')}}"></script>
<script src="{{ asset('/profile/js/react.min.js')}}"></script>
<script src="{{ asset('/profile/js/react-dom.min.js')}}"></script>
<script src="{{ asset('/profile/js/Chart.js')}}"></script>

<script src="{{ asset('/profile/js/custom.js')}}"></script>

<script>
    // check all
    $('#checkall').change(function () {
        if ($(this).is(":checked")) {
            $("input[name='ids[]']:not(:disabled)").each(function () {
                $(this).prop("checked", true).change();
            });
        } else {
            $("input[name='ids[]']:not(:disabled)").each(function () {
                $(this).prop("checked", false).change();
            });
        }
    });

    // check change
    $("table").on('change', "input[name='check[]']", function (e) {
        if ($("input[name='ids[]']:checked").length > 0) {
            $("#delete").removeClass('disabled');
        } else {
            $("#delete").addClass('disabled');
        }
    });

    $('#pdf-print').on('click', function () {

        let ids = [];

        $(".selectpdf input:checked").each(function () {
            ids.push($(this).val());

        });

        if (ids.length > 0) {
            $("#print_type").val('policy');
            $("#myform").submit();
        }

    });

    $('#report-print').on('click', function () {

        let ids = [];

        $(".selectpdf input:checked").each(function () {
            ids.push($(this).val());

        });

        if (ids.length > 0) {
            $("#print_type").val('excel');
            $("#myform").submit();
        }

    });
</script>

<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models\OrderLog;
use Response;
use Flash;
use File;
use Auth;

class OrderLogController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$order_logs = OrderLog::orderBy('id', 'desc')->paginate(50);

		return view('backend.order_logs.index', compact('order_logs'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('backend.order_logs.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$order_log = new OrderLog();

		$order_log->driver_id = $request->input("driver_id");
        $order_log->order_id = $request->input("order_id");
        $order_log->status = $request->input("status");

		$order_log->save();

        Flash::success(__('backend.OrderLog_saved_successfully'));
		return redirect()->route('mngrAdmin.order_logs.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$order_log = OrderLog::findOrFail($id);

		return view('backend.order_logs.show', compact('order_log'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$order_log = OrderLog::findOrFail($id);

		return view('backend.order_logs.edit', compact('order_log'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$order_log = OrderLog::findOrFail($id);

		$order_log->driver_id = $request->input("driver_id");
        $order_log->order_id = $request->input("order_id");
        $order_log->status = $request->input("status");

		$order_log->save();

        Flash::warning(__('backend.OrderLog_Updated_successfully'));

		return redirect()->route('mngrAdmin.order_logs.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$order_log = OrderLog::findOrFail($id);
		$order_log->delete();

        Flash::danger(__('backend.OrderLog_deleted_successfully'));

		return redirect()->route('mngrAdmin.order_logs.index');
	}

}

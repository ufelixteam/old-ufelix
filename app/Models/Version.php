<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Version extends Model
{
   public function CustomerDevice()
   {
     return $this->hasMany('App\Models\CustomerDevice');
   }
}

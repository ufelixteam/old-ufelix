@extends('backend.layouts.app')
@section('header')
    <div class="page-header">
        <h3>Show #{{$corprate->id}}</h3>
        <form action="{{ route('mngrAdmin.corprates.destroy', $corprate->id) }}" method="POST" style="display: inline;"
              onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group"
                   href="{{ route('mngrAdmin.corprates.edit', $corprate->id) }}"><i
                        class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <div class="form-group">
                <label for="nome">#</label>

                <p class="form-control-static">{!! $corprate->active_span !!}</p>
            </div>
            <div class="form-group col-sm-6">
                <label for="name">Name</label>
                <p class="form-control-static">{{$corprate->name}}</p>
            </div>
            <div class="form-group col-sm-6">
                <label for="name">Email</label>
                <p class="form-control-static">{{$corprate->email}}</p>
            </div>
            <div class="form-group col-sm-6">
                <label for="name">Mobile</label>
                <p class="form-control-static">{{$corprate->mobile}}</p>
            </div>
            <div class="form-group col-sm-6">
                <label for="name">Phone</label>
                <p class="form-control-static">{{$corprate->phone}}</p>
            </div>

            <div class="form-group col-sm-6">
                <label for="mobile">Field</label>
                <p class="form-control-static">{{$corprate->field}}</p>
            </div>
            <div class="form-group col-sm-6">
                <label for="phone">Commercial Record Number</label>
                <p class="form-control-static">{{$corprate->commercial_record_number}}</p>
            </div>
        </div>
        <div class="form-group col-sm-6">
            <label for="phone">Commercial Record Image</label>
            <p class="form-control-static">@if($corprate->commercial_record_image)
                    <img src="{{ asset('public/backend/images/'.$corprate->commercial_record_image) }}"
                         style="width:100px;height:100px">
                @endif</p>
        </div>
        <?php if ( $corprate->latitude > 0 && $corprate->longitude > 0 ) {?>
        <div class="form-group col-sm-12" style="width:100%;height:500px;">
            <?php
            Mapper::map($corprate->latitude, $corprate->longitude, ['strokeColor' => '#003400', 'strokeOpacity' => 0.1, 'strokeWeight' => 2, 'fillColor' => '#65638ea', 'clusters' => ['size' => 10, 'center' => true, 'zoom' => 20], 'zoom' => 8]);

            Mapper::marker($corprate->latitude, $corprate->longitude, ['markers' => ['title' => 'Busy', 'symbol' => 'rectangle', 'scale' => 1000, 'animation' => 'DROP']])->informationWindow($corprate->latitude, $corprate->longitude, "", ['markers' => ['animation' => 'DROP']]);

            // Mapper::map(53.381128999999990000, -1.470085000000040000);
            $map = Mapper::render();
            echo $map;
            ?>
        </div>
        <?php } $counter = 0;  ?>
        @if($corprate->customers)
            <div class="form-group col-sm-12">
                <h3>Clients Information</h3>
                <hr>
                <table class="table">
                    <tr>
                        <th>#</th>
                        <th>Client Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Operation</th>
                    </tr>
                    <tr>
                        @foreach($corprate->customers as $value)
                            <td>{{ ++$counter }}</td>
                            <td>{{ ! empty($value->name)?$value->name:''}}</td>
                            <td>{{ ! empty($value->email)?$value->email:''}}</td>
                            <td>{{ ! empty($value->mobile)?$value->mobile:''}}</td>
                            <td><a class="btn btn-xs btn-primary"
                                   href="{{ route('mngrAdmin.customers.show', $value->id) }}"><i
                                        class="glyphicon glyphicon-eye-open"></i> View</a></td>
                        @endforeach
                    </tr>

                </table>

            </div>
        @endif

        <div class="col-md-12 col-xs-12 col-sm-12">
            <a class="btn btn-link" href="{{ route('mngrAdmin.corprates.index') }}"><i
                    class="glyphicon glyphicon-backward"></i> Back</a>
        </div>

    </div>
    </div>

@endsection

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>welecome to support Team </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <style>
    .card {
      padding: 40px;
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid rgba(0,0,0,.125);
        border-radius: .25rem;
        text-align: center;
    }
    .card-header {
      padding: .75rem 1.25rem;
      margin-bottom: 0;
      background-color: #4b4b4b;
      color: #fff;
      font-weight: bold;
      font-family: sans-serif;
    }
    .card-body {
      -webkit-box-flex: 1;
      -ms-flex: 1 1 auto;
      flex: 1 1 auto;
      padding: 1.25rem;
      border: 1px solid #4b4b4b;
      color: #4b4b4b!important;
    }
    .card-title {
      margin-bottom: 0rem;
      /* color: #f80; */
      font-weight: bold;
      margin-top: 0;
    }

    .card-footer {
      padding: .75rem 1.25rem;
      background-color: #4b4b4b;
      border-top: 1px solid #333;
      color: #fff!important;
    }
    .card-footer:last-child {
      border-radius: 0 0 calc(.25rem - 1px) calc(.25rem - 1px);
    }
    .text-muted {
       color: #4b4b4b;
     }
   h5 { font-size: 1.25rem; }
   .logo .log
   {
     width: 200px;
      height: 100%;
      overflow: hidden;
      margin: auto;
   }
    .logo .log img {
      width: 100%;
    }

    #bto
     {
       display: inline-block;
        font-weight: 400;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        border: 1px solid transparent;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        border-radius: .25rem;
        color: #fff;
        background-color: #28a745;
        border-color: #28a745;
        text-decoration: none;
        text-transform: capitalize;
        font-weight: bold;
        letter-spacing: 1px;
        width: 100px;
        margin: auto;
        transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
        }
        h1{
          text-align: center;
          text-transform: capitalize;
          color: #58585a;
          font-family: sans-serif;
        }
        .verify{
          width: 252px;
          border: 2px solid #58585a;
          padding: 26px;
          font-size: 10px;
          margin: auto;
          border-radius: 5px;
        }
     }
    </style>
  </head>
  <body>

    <div class="card">
      <div class="logo">
        <div class="log">
          <a href="https://ufelix.com/" title=""><img class="card-img" src="{{ asset('public/website/images/logo.png') }}" alt=""/></a>
        </div>
      </div>
      <div class="verify">
        <h1>to verify your email ufelix please click here</h1>
        <a id="bto" style="color: #fff;background-color: #28a745;border-color: #28a745;" href="{{url('home/verifyEmail' ,$token)}}">click here</a>
      </div>

    </div>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

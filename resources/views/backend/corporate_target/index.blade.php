@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.corporate_target')}}</title>
  <style type="text/css">
    @media only screen and (max-width: 580px) {
        .hide-td{
            display: none;
        }
    }
    .box-body {
         min-height: 100px;
    }
    .stylish-input-group .input-group-addon{
        background: white !important;
    }
    .stylish-input-group .form-control{
        border-right:0;
        box-shadow:0 0 0;
        border-color:#ccc;
    }
    .stylish-input-group button{
        border:0;
        background:transparent;
    }
  </style>
@endsection

@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.corporate_target')}}
            <a class="btn btn-success pull-right" data-toggle="modal" data-target="#addTarget"><i class="glyphicon glyphicon-plus"></i> {{__('backend.add_corporate_target')}}</a>
        </h3>
    </div>



    <!-- Model Add Target For Corporate -->
    <div class="row">
      <!-- Modal -->
      <div class="modal fade" id="addTarget" role="dialog">
        <div class="modal-dialog" style="width: 80%;">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title text-center"> {{__('backend.Add_New_Deal_For_This_Corporate')}}</h4>
            </div>

            <form action="{{ route('mngrAdmin.CorporateTarget.store') }}" method="POST" name="validateForm">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                      <div class="form-main">
                        <div class="form-group col-md-3 col-sm-3 @if($errors->has('corporate_target_from')) has-error @endif">
                            <label for="corporate_target_from_field">{{__('backend.target_from')}}</label>
                            <input type="number" min="1" id="corporate_target_from_field" name="corporate_target_from" class="form-control" value="{{ old("corporate_target_from") ? old("corporate_target_from")  : '' }}" />
                           @if($errors->has("corporate_target_from"))
                            <span class="help-block">{{ $errors->first("corporate_target_from") }}</span>
                           @endif
                        </div>
                        <div class="form-group col-md-3 col-sm-3 @if($errors->has('corporate_target_to')) has-error @endif">
                            <label for="corporate_target_to_field">{{__('backend.target_to')}}</label>
                            <input type="number" min="1" id="corporate_target_to_field" name="corporate_target_to" class="form-control" value="{{ old("corporate_target_to") ? old("corporate_target_to")  : '' }}" />
                           @if($errors->has("corporate_target_to"))
                            <span class="help-block">{{ $errors->first("corporate_target_to") }}</span>
                           @endif
                        </div>
                        <div class="form-group col-md-3 col-sm-3 @if($errors->has('corporate_discount')) has-error @endif">
                          <label for="corporate_discount_field">{{__('backend.discount_percentage')}}</label>
                            <div class='input-group'>
                                  <input type="text" id="corporate_discount_field" name="corporate_discount" class="form-control" value="{{ old("corporate_discount") }}"/>
                                  <div class="input-group-addon">
                                      <i class="fa fa-percent"></i>
                                  </div>
                             </div>
                             @if($errors->has("corporate_discount"))
                              <span class="help-block">{{ $errors->first("corporate_discount") }}</span>
                             @endif
                        </div>
                        <div class="form-group col-md-3 col-sm-3 @if($errors->has('corporate_id')) has-error @endif">
                          <label for="type-field">{{__('backend.choose_corporate')}}: </label>
                          <select id="type-field" name="corporate_id" id="corporate_id" class="form-control">
                            @foreach($corporates as $corporate)
                              <option value="{{ $corporate->id }}">{{ $corporate->name }}</option>
                            @endforeach
                          </select>
                          @if($errors->has("corporate_id"))
                            <span class="help-block">{{ $errors->first("corporate_id") }}</span>
                          @endif
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="form-group col-md-12 col-sm-6 @if($errors->has('report')) has-error @endif">
                        <label for="report-field">{{__('backend.report')}}: </label>
                        <textarea class="form-control" id="report-field" rows="4" name="report">{{ old("report") }}</textarea>
                        @if($errors->has("report"))
                          <span class="help-block">{{ $errors->first("report") }}</span>
                        @endif
                      </div>
                    </div>

                  </div>
                </div>
              </div>

              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">{{__('backend.create')}}</button>
              </div>
            </form>

          </div>
        </div>
      </div>
    </div>


@endsection
@section('content')
<div class="row" style="margin-bottom:15px;">
  <div class="col-md-12">

    <div class="group-control col-md-6 col-sm-6">
       <select id="corporate" name="corporate" class="form-control">
         <option value="" data-display="Select">{{__('backend.select_corporate_for_show_targets')}}</option>
          @if(! empty($corporates))
              @foreach($corporates as $corporate)
                  <option value="{{$corporate->id}}" >
                    {{$corporate->name}}
                  </option>
              @endforeach
          @endif
       </select>
     </div>

  </div>
</div>

<div class='list'>
  @include('backend.corporate_target.table')
</div>

@endsection

@section('scripts')
<script>

  $("#corporate").on('change', function() {
    	$('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
    	$.ajax({
          url: '{{URL::asset("/mngrAdmin/corporate_targets")}}'+'?filter='+$(this).val(),
          type: 'get',
          data: $("#corporate").serialize(),
          success: function(data) {
              $('.list').html(data.view);
          },
          error: function(data) {
              console.log('Error:', data);
          }
     	});
  });
</script>
@endsection

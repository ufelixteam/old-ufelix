@extends('agent.layouts.app')

@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> Vehicles
            <a class="btn btn-success pull-right" href="{{ route('mngrAgent.trucks.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </h3>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            @if($trucks->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>MODEL NAME</th>

                      <th>PLATE NUMBER</th>

                        <th>STATUS</th>
                      
                            <th class="text-right"></th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($trucks as $truck)
                            <tr>
                                <td>{{$truck->id}}</td>
                                <td>{{$truck->model_name}}</td>
                                <td>{{$truck->plate_number}}</td>

                                <td>{!! $truck->status_span !!}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('mngrAgent.trucks.show', $truck->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                  
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $trucks->render() !!}
            @else
                <h3 class="text-center alert alert-warning">No Result Found !</h3>
            @endif

        </div>
    </div>

@endsection
@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.promo_codes')}} - {{__('backend.create')}}</title>
@endsection

@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-plus"></i> {{__('backend.promo_codes')}} / {{__('backend.add_promo_code')}} </h3>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <form action="{{ route('mngrAdmin.promo_codes.store') }}" method="POST" name='validateForm'>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                  <div class="form-group col-md-6 col-sm-6 @if($errors->has('code')) has-error @endif">
                    <label for="code-field">{{__('backend.code')}}</label>
                    <input type="text" id="code-field" name="code" class="form-control" value="{{ old("code") }}"/>
                     @if($errors->has("code"))
                      <span class="help-block">{{ $errors->first("code") }}</span>
                     @endif
                  </div>
                  <div class="form-group col-md-6 col-sm-6 @if($errors->has('no_tries')) has-error @endif">
                     <label for="no_tries-field">{{__('backend.no_tries')}}</label>
                     <input type="text" id="no_tries-field" name="no_tries" class="form-control" value="{{ old("no_tries") }}"/>
                     @if($errors->has("no_tries"))
                      <span class="help-block">{{ $errors->first("no_tries") }}</span>
                     @endif
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-6 col-sm-6 @if($errors->has('discount')) has-error @endif">
                     <label for="discount-field">{{__('backend.discount')}} %</label>
                     <input type="text" id="discount-field" name="discount" class="form-control" value="{{ old("discount") }}"/>
                     @if($errors->has("discount"))
                      <span class="help-block">{{ $errors->first("discount") }}</span>
                     @endif
                  </div>
                  <div class="form-group col-md-6 col-sm-6 @if($errors->has('status')) has-error @endif">
                     <label for="status-field" style="display: block;">{{__('backend.status')}}</label>
                     <select id="status-field" name="status" class="form-control" value="{{ old("status") }}" >
                        <option value="0" {{ old("status") == "0" ? 'selected' : '' }}>{{__('backend.no')}}</option>
                        <option value="1" {{ old("status") == "1" ? 'selected' : '' }}>{{__('backend.yes')}}</option>
                      </select>
                       @if($errors->has("status"))
                        <span class="help-block">{{ $errors->first("status") }}</span>
                       @endif
                   </div>
                 </div>
                 <div class="well well-sm col-md-12 col-sm-12 col-xs-12">
                    <button type="submit" class="btn btn-primary">{{__('backend.create')}}</button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.promo_codes.index') }}"><i class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
                 </div>
            </form>

        </div>
    </div>
@endsection

@section('scripts')
   <script type="text/javascript">
  $(function() {
    $("form[name='validateForm']").validate({
      // Specify validation rules
      rules: {
        code: {
          required: true,
        },
        no_tries: {
          required: true,
          number:true
        },
        discount: {
          required: true,
          number:true
        },
        status: {
          required: true,
        },
      },

      // Specify validation error messages
      messages: {
        discount: {
          required: "{{__('backend.Please_Enter_Discount')}}",
          number: "{{__('backend.Please_Enter_Avalid_Number')}}",
        },
        no_tries: {
          required: "{{__('backend.Please_Enter_No_of_tries')}}",
          number: "{{__('backend.Please_Enter_Avalid_Number')}}",
        },
        code: {
          required: "{{__('backend.Please_Enter_Code')}}",
        },
        status: {
          required: "{{__('backend.Please_Chosse_The_Status')}}",
        },
      },

       errorPlacement: function(error, element) {
          $(element).parents('.form-group').append(error)
      },

      highlight: function (element) {
          $(element).parent().addClass('error')
      },
      unhighlight: function (element) {
          $(element).parent().removeClass('error')
      },
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function(form) {
          form.submit();
      }
    });
  })
</script>
@endsection

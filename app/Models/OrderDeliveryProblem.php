<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDeliveryProblem extends Model
{
    protected $guarded = ['id'];

    public function getReasonAttribute($value){
        $pattern = '/[^\x{0600}-\x{065F}\x{066A}-\x{06EF}\x{06FA}-\x{06FF}a-zA-Z0-9\s]/u';
        $output = preg_replace($pattern, "", $value);

        return $output;
    }

    public function driver()
    {
        return $this->belongsTo('App\Models\Driver', 'driver_id');
    }

    public function agent()
    {
        return $this->belongsTo('App\Models\Agent', 'agent_id');
    }

    public function problem()
    {
        return $this->belongsTo('App\Models\DeliveryProblem', 'delivery_problem_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'created_by');
    }

}

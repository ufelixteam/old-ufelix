@extends('frontend.layouts.app')
@section('title') @lang('website.track_order') @endsection
@section('content')
    <section class="traking body-in">
        <div class="container">
            <div class="col-md-12 col-xs-12">
                <div class="col-md-10 col-md-offset-1 col-xs-12">
                    <div class="req-x">
                        @if(! empty($order))

                            <div class="row">
                                <div class="col-md-6 order-info">
                                    <table>
                                        <tr>
                                            <td>
                                                <h3>@lang('website.seller_name'): </h3>
                                            </td>

                                            <td>
                                                <h3>{{!empty($order->customer->Corporate) ? $order->customer->Corporate->name : "-"}}</h3>
                                            </td>

                                        </tr>

                                        <tr>
                                            <td>
                                                <h3>@lang('website.seller_phone'): </h3>
                                            </td>

                                            <td>
                                                <h3>{{$order->sender_mobile}}</h3>
                                            </td>

                                        </tr>

                                        <tr>
                                            <td>
                                                <h3>@lang('website.customer_name'): </h3>
                                            </td>

                                            <td>
                                                <h3>{{$order->receiver_name}}</h3>
                                            </td>

                                        </tr>

                                        <tr>
                                            <td>
                                                <h3>@lang('website.customer_phone'):</h3>
                                            </td>

                                            <td>
                                                <h3>{{$order->receiver_mobile}}</h3>
                                            </td>

                                        </tr>

                                        <tr>
                                            <td>
                                                <h3>@lang('website.address'):</h3>
                                            </td>

                                            <td>
                                                <h3>{{$order->receiver_address}}</h3>
                                            </td>

                                        </tr>


                                        <tr>
                                            <td>
                                                <h3>@lang('website.notes'):</h3>
                                            </td>

                                            <td>
                                                <h3>{{$order->notes}}</h3>
                                            </td>

                                        </tr>


                                    </table>
                                    <div class="track-another-order">
                                        <a class="btn-id" data-toggle="modal" data-target="#box-id">
                                        </a>
                                    </div>

                                </div>
                                <div class="col-md-6 tracking-details">
                                    <div class="background">
                                        <div class="container">
                                            <div class="row header text-center">
                                                <div class="col-xs-3 icon-back">

                                                </div>
                                                <div class="col-xs-6 order">
                                                    <div class="order-number">
                                                        {{app("request")->search}}
                                                    </div>
                                                    <div class="order-status">
                                                        {!! $order->website_status_span !!}
                                                    </div>
                                                </div>
                                                <div class="col-xs-3 icon-brand">
                                                </div>
                                            </div>
                                            <div class="row content">
                                                <div class="timeline">

                                                    <div class="item @if($order->status == 0) active @endif">
                                                        <div class="item-label">
                                                            <div class="item-label-date">
                                                                {{Carbon\Carbon::parse($order->created_at)->toFormattedDateString()}}
                                                            </div>
                                                            <div class="item-label-hour">
                                                                {{Carbon\Carbon::parse($order->created_at)->format('g:i:s a')}}
                                                            </div>
                                                        </div>
                                                        <div class="item-description">
                                                            <div class="item-description-status">
                                                                @lang('website.created_status')
                                                            </div>
                                                            <div class="item-description-location">

                                                            </div>
                                                        </div>
                                                    </div>
                                                    @if( in_array($order->status, [1,2,3,4,5,7]) && $order->dropped_at)
                                                        <div class="item @if($order->status == 7) active @endif">
                                                            <div class="item-label">
                                                                <div class="item-label-date">
                                                                    {{Carbon\Carbon::parse($order->dropped_at)->toFormattedDateString()}}
                                                                </div>
                                                                <div class="item-label-hour">
                                                                    {{Carbon\Carbon::parse($order->dropped_at)->format('g:i:s a')}}
                                                                </div>
                                                            </div>
                                                            <div class="item-description">
                                                                <div class="item-description-status">
                                                                    @lang('website.dropped_status')
                                                                </div>
                                                                <div class="item-description-location">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    @if( in_array($order->status, [1,2,3,4,5]) && $order->accepted_at)
                                                        <div class="item @if($order->status == 1) active @endif">
                                                            <div class="item-label">
                                                                <div class="item-label-date">
                                                                    {{Carbon\Carbon::parse($order->accepted_at)->toFormattedDateString()}}
                                                                </div>
                                                                <div class="item-label-hour">
                                                                    {{Carbon\Carbon::parse($order->accepted_at)->format('g:i:s a')}}
                                                                </div>
                                                            </div>
                                                            <div class="item-description">
                                                                <div class="item-description-status">
                                                                    @lang('website.accepted_status')
                                                                </div>
                                                                <div class="item-description-location">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    @if( in_array($order->status, [2,3,4,5]) && $order->received_at)
                                                        <div class="item @if($order->status == 2) active @endif">
                                                            <div class="item-label">
                                                                <div class="item-label-date">
                                                                    {{Carbon\Carbon::parse($order->received_at)->toFormattedDateString()}}
                                                                </div>
                                                                <div class="item-label-hour">
                                                                    {{Carbon\Carbon::parse($order->received_at)->format('g:i:s a')}}
                                                                </div>
                                                            </div>
                                                            <div class="item-description">
                                                                <div class="item-description-status">
                                                                    @lang('website.received_status')
                                                                </div>
                                                                <div class="item-description-location">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    @if( in_array($order->status, [3]) && $order->delivered_at)
                                                        <div class="item @if($order->status == 3) active @endif">
                                                            <div class="item-label">
                                                                <div class="item-label-date">
                                                                    {{Carbon\Carbon::parse($order->delivered_at)->toFormattedDateString()}}
                                                                </div>
                                                                <div class="item-label-hour">
                                                                    {{Carbon\Carbon::parse($order->delivered_at)->format('g:i:s a')}}
                                                                </div>
                                                            </div>
                                                            <div class="item-description">
                                                                <div class="item-description-status">
                                                                    @lang('website.delivered_status')
                                                                </div>
                                                                <div class="item-description-location">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    @if( in_array($order->status, [5]) && $order->recalled_at)
                                                        <div class="item @if($order->status == 5) active @endif">
                                                            <div class="item-label">
                                                                <div class="item-label-date">
                                                                    {{Carbon\Carbon::parse($order->recalled_at)->toFormattedDateString()}}
                                                                </div>
                                                                <div class="item-label-hour">
                                                                    {{Carbon\Carbon::parse($order->recalled_at)->format('g:i:s a')}}
                                                                </div>
                                                            </div>
                                                            <div class="item-description">
                                                                <div class="item-description-status">
                                                                    @lang('website.recalled_status')
                                                                </div>
                                                                <div class="item-description-location">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    @if( in_array($order->status, [4]) && $order->cancelled_at)
                                                        <div class="item @if($order->status == 4) active @endif">
                                                            <div class="item-label">
                                                                <div class="item-label-date">
                                                                    {{Carbon\Carbon::parse($order->cancelled_at)->toFormattedDateString()}}
                                                                </div>
                                                                <div class="item-label-hour">
                                                                    {{Carbon\Carbon::parse($order->cancelled_at)->format('g:i:s a')}}
                                                                </div>
                                                            </div>
                                                            <div class="item-description">
                                                                <div class="item-description-status">
                                                                    @lang('website.cancelled_status')
                                                                </div>
                                                                <div class="item-description-location">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <h3>@lang('backend.order_comments')</h3>
                                    @if(count($order->comments))
                                        <table id="theTable" class="table table-condensed table-striped">
                                            <thead>
                                            <tr>
                                                <th>{{__('backend.comment')}}</th>
                                                <th>{{__('backend.user')}}</th>
                                                <th>{{__('backend.driver')}}</th>
                                                <th>{{__('backend.date')}}</th>
                                                <th>{{__('backend.location')}}</th>
                                            </tr>
                                            </thead>

                                            @foreach($order->comments as $comment)
                                                <tr>
                                                    <td>{{$comment->comment}}</td>
                                                    <td>{{!empty($comment->user) ? $comment->user->name : '-'}}</td>
                                                    <td>{{!empty($comment->driver) ? $comment->driver->name : '-'}}</td>
                                                    <td>{{date("Y-m-d", strtotime($comment->created_at))}}</td>
                                                    <td>
                                                        <a href="http://maps.google.com/?q={{$comment->latitude}},{{$comment->longitude}}"
                                                           target="_blank"><i class="fa fa-map-marker"></i></a></td>
                                                </tr>
                                            @endforeach

                                            <tbody>
                                            </tbody>
                                        </table>
                                    @else
                                        <div class="form-group text-center">
                                            <div class="alert alert-info" style="padding-bottom: 3px;">
                                                <h4>{{__('backend.order_comments')}}</h4>
                                            </div>
                                        </div>
                                    @endif
                                </div>


                            <div class="cancel-x">
                                <ul>
                                    <li>
                                        <a class="btn btn-info tracking-options" data-toggle="modal"
                                           data-target="#box-id">@lang('website.track_another_order')</a>
                                    </li>
                                    {{--                                    <li>--}}
                                    {{--                                        <a class="btn btn-info tracking-options" data-toggle="modal"--}}
                                    {{--                                           data-target="#cancel">الغاء التسليم</a>--}}
                                    {{--                                    </li>--}}
                                    {{--                                    <li>--}}
                                    {{--                                        <a class="btn btn-info tracking-options" data-toggle="modal"--}}
                                    {{--                                           data-target="#change">تغير مكان--}}
                                    {{--                                            & وقت التسليم</a>--}}
                                    {{--                                    </li>--}}
                                </ul>
                            </div>

                        @else

                            not found

                        @endif

                    </div>
                </div>
            </div>

            <div class="col-md-3 col-xs-12"></div>
        </div>

    </section>
@endsection

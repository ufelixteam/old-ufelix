<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="author" content="Ufelix Team">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('/website/images/flogo.png')}}"/>

    <title>@yield('title')</title>
    @include('frontend.profile.layouts.css')
    @yield('css')

</head>
<body>
@include('frontend.profile.layouts.header')
@include('frontend.profile.layouts.sidemenu')

<!-- Start DashBoard -->
<div class="dashboard-wrapper dashboard-wrapper-lg">
    @yield('header')
    <div class="container-fluid">
        @yield('content')
    </div>
</div>
<!-- End DashBoard -->
<script>
    // var data = {
    //     labels: ['Month:', 'Jan', 'Feb', 'March', 'Apri', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    //     datasets: [
    //         {
    //             label: "Cancelled Orders",
    //             backgroundColor: '#f56565',
    //             borderColor: '#f56565',
    //             data: [
    //                 5, 6, 10, 16, 24, 31, 40, 52, 67, 71, 75, 79, 84]
    //         },
    //         {
    //             label: "Deliverd Orders",
    //             backgroundColor: '#65f5bd',
    //             borderColor: '#65f5bd',
    //             data: [
    //                 120, 180, 281, 439, 687, 885, 1140, 1469, 1893, 2003, 2120, 2244, 2375]
    //         },
    //
    //         {
    //             label: "Recale",
    //             backgroundColor: '#cc82ff',
    //             borderColor: '#cc82ff',
    //             data: [
    //                 1000, 1372, 1946, 2719, 3798, 4800, 6066, 7666, 9688, 10202, 10744, 11314, 11915]
    //         },
    //
    //         {
    //             label: "Received Orders",
    //             backgroundColor: '#ffd5dd',
    //             borderColor: '#ffd5dd',
    //             data: [
    //                 250, 300, 500, 800, 1200, 1550, 2000, 2600, 3350, 3550, 3750, 3950, 4200]
    //         }
    //     ]
    // };
</script>

@include('frontend.profile.layouts.js')

@yield('scripts')
</body>
</html>

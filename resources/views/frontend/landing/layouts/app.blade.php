<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ufelix l يوفيلكس</title>
    <link rel="icon" href="{{asset('/front/images/fav.png')}}" type="image/gif" sizes="24x24">
    <!-- Bootstrap -->
    <link href="{{asset('/front/css/bootstrap.css')}}" rel="stylesheet">
    <!--bootstrap rtl for arabick web site direction-->
    <link href="{{asset('/front/css/bootstrap-rtl.min.css')}}" rel="stylesheet">
    <!--fontawesome-->
    <link href="{{asset('/front/css/font-awesome.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--my style-->
    <link href="{{asset('/front/css/style.css')}}" rel="stylesheet">
</head>

<body>

<!--************************* NavBar *************************-->
<div class="navbar">
    <div class="container">
        <div class="logo">
            <img src="{{asset('/front/images/logo.png')}}">
        </div>
        <div class="menu">
            <ul>
                <a href="#map"><li>المناطق</li></a>
                <a href="#features"><li>المميزات</li></a>
                <a href="#services"><li>الخدمات</li></a>
                <a href="#get-app"><li>التطبيق</li></a>
                <a href="#"><li><button type="button" class="cta" data-toggle="modal" data-target="#myModal">اتصل بنا</button></li></a>
            </ul>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <form action="" method="post">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">تواصل مع احد ممثلينا من الدعم الفني</h4>
                </div>
                <div class="modal-body">
                    <input type="text" name="name" placeholder="الإسم بالكامل">
                    <input type="text" name="mobile" placeholder="رقم الهاتف">
                    <input type="email" name="email" placeholder="البريد الإلكتروني">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary">إزسال</button>
                </div>
            </div>
        </div>
    </form>
</div>

@yield('content')

<!--************************* Footer *************************-->

<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <h3>عن الشركة</h3>
                <p>يوفيكلس هي شركة لتقديم الحلول اللوجيستية عن طريق تطبيقات الهواتف الذكية و مواقع الويب بتغطية كاملة لكل مصر عن طريق اسطول الغير بمبدأ اشحن في سكتك .. </p>
            </div>
            <div class="col-md-3 col-xs-12">
                <h3>اتصل بنا</h3>
                <h4>المقر الرئيسي</h4>
                <p>28 شارع عدنان المدني - احمد عرابي - المهندسين</p>
                <div class="clearfix"></div>
                <h4>الهاتف</h4>
                <p class="phone">+2 0100 78 14 60</p>
                <div class="clearfix"></div>
                <p class="phone">+02 33 47 28 08</p>
            </div>
            <div class="col-md-4 col-xs-12">
                <h3>تابع اخبار و عروض يوفيكلس</h3>
                <form>
                    <input type="" name="" placeholder="بريدك الإلكتروني">
                    <i class="fa fa-arrow-left"></i>
                </form>
                <div class="social-media">
                    <a href=""><i class="fa fa-facebook facebook"></i></a>
                    <a href=""><i class="fa fa-twitter twitter"></i></a>
                    <a href=""><i class="fa fa-youtube youtube"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright text-center">
        <p>جميع الحقوق محفوظة لدي - يوفيلكس - @2019</p>
    </div>
</div>


<!-- jQuery  -->
<script src="{{asset('/front/js/jquery-1.11.2.min.js')}}"></script>
<!--bootstrap-->
<script src="{{asset('/front/js/bootstrap.min.js')}}"></script>
<!--my JS file-->
<script src="{{asset('/front/js/code.js')}}"></script>


<!-- Auto-Scroll  -->


<script type="text/javascript">
    document.querySelectorAll('a[href^="#"]').forEach(anchor => {
        anchor.addEventListener('click', function (e) {
            e.preventDefault();

            document.querySelector(this.getAttribute('href')).scrollIntoView({
                behavior: 'smooth'
            });
        });
    });
</script>
</body>
</html>

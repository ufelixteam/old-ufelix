@extends('backend.layouts.app')

@section('css')
<title>{{__('backend.Shipment_Orders')}} - {{__('backend.edit')}}</title>
@endsection

@section('header')
<div class="page-header">
  <h3><i class="glyphicon glyphicon-edit"></i>  {{__('backend.Edit_Shipment_Orders')}} - #{{$shipment_tool_order->id}}</h3>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12 col-xs-12 col-sm-12">
    <form action="{{ route('mngrAdmin.shipment_tool_orders.update', $shipment_tool_order->id) }}" method="POST" name="validateForm">
      <input type="hidden" name="_method" value="PUT">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
{{--      <div class="form-group col-md-6 col-sm-6 @if($errors->has('agent_id')) has-error @endif">--}}
{{--        <label for="agent_id-field">{{__('backend.agent')}} </label>--}}
{{--        <select id="agent_id-field" name="agent_id" id="agent_id" class="form-control" disabled>--}}
{{--          @if(! empty($agents))--}}
{{--          @foreach($agents as $agent)--}}
{{--          <option value="{{$agent->id}}" {{ old("agent_id") == $agent->id || $shipment_tool_order->agent_id == $agent->id  ? 'selected' : '' }}--}}
{{--          >{{$agent->name}}</option>--}}
{{--          @endforeach--}}
{{--          @endif--}}
{{--        </select>--}}
{{--        @if($errors->has("agent_id"))--}}
{{--        <span class="help-block">{{ $errors->first("agent_id") }}</span>--}}
{{--        @endif--}}
{{--      </div>--}}
      <div class="form-group col-md-6 col-sm-6 @if($errors->has('status')) has-error @endif">
        <label for="status-field">{{__('backend.status')}}</label>
        <select id="status-field" name="status" class="form-control" value="{{ old("status") }}" >
          <option value="0" {{ old("status") == "0"  || $shipment_tool_order->status == "0"  ? 'selected' : '' }}>{{__('backend.pending')}}</option>
          <option value="1" {{ old("status") == "1"  || $shipment_tool_order->status == "1"  ? 'selected' : '' }}>{{__('backend.accepted')}}</option>
          <option value="2" {{ old("status") == "2"  || $shipment_tool_order->status == "2"  ? 'selected' : '' }}>{{__('backend.rejected')}}</option>
        </select>
        @if($errors->has("status"))
        <span class="help-block">{{ $errors->first("status") }}</span>
        @endif
      </div>
      <div id="fields">

        @if( ! empty($shipment_tool_order->items) && count($shipment_tool_order->items) >0)
        @php
        $i = 0;
        @endphp
        @foreach($shipment_tool_order->items as $item)
        <div class="col-sm-12 col-md-12"  style="margin-top: 10px;">
          <div class="col-sm-5 col-md-5">
            <input required  type="number"  name="quantity[]" class="form-control" placeholder="{{__('backend.quantity')}}" value="{{$item->quantity}}"  />
          </div>
          <div class="form-group col-md-6 col-sm-6 @if($errors->has('items')) has-error @endif">
            <select id="items-field" name="items[]" id="items" class="form-control">

              @if(! empty($shipment_tools))
              @foreach($shipment_tools as $item)
              <option value="{{$item->id}}" {{ old("items") == $item->id || $item->tool_id == $item->id ? 'selected' : '' }}
              >{{$item->name_en}}</option>
              @endforeach
              @endif
            </select>

          </div>
          @if($i == 0)
          <div class="input-group">
            <span class="input-group-btn">
              <input type="button" id="add-field" value="+" class="btn btn-success add-more">
            </span>
          </div>
          @else
          <div class="input-group">
            <span class="input-group-btn">
              <input type="button" id="remove-field" value="-" class="btn btn-danger remove">
            </span>
          </div>
          @endif
        </div>
        @php
        $i ++ ;
        @endphp
        @endforeach
        @else
        <div id="fields">
          <div class="col-sm-12 col-md-12"  style="margin-top: 10px;">
            <div class="col-sm-5 col-md-5">
              <input required  type="number"  name="quantity[]" class="form-control" placeholder="{{__('backend.quantity')}}"  />
            </div>
            <div class="form-group col-md-6 col-sm-6 @if($errors->has('items')) has-error @endif">
              <select id="items-field" name="items[]" id="items" class="form-control">

                @if(! empty($shipment_tools))
                @foreach($shipment_tools as $item)
                <option value="{{$item->id}}" {{ old("items") == $item->id ? 'selected' : '' }}
                >{{$item->name_en}}</option>
                @endforeach
                @endif
              </select>

            </div>
            <div class="input-group">
              <span class="input-group-btn">
                <input type="button" id="add-field" value="+" class="btn btn-success add-more">
              </span>
            </div>
          </div>
        </div>
        @endif
      </div>

      <div class="form-group col-md-12 col-sm-12 @if($errors->has('address')) has-error @endif">
        <label for="notes-field">{{__('backend.address')}}</label>
        <input type="text" name="address" class="form-control" placeholder="{{__('backend.address')}}" value="{{ is_null(old("address")) ? $shipment_tool_order->address : old("address") }}" />
        @if($errors->has("address"))
        <span class="help-block">{{ $errors->first("address") }}</span>
        @endif
      </div>

      <div class="form-group col-md-12 col-sm-12 @if($errors->has('notes')) has-error @endif">
        <label for="notes-field">{{__('backend.notes')}}</label>
        <textarea class="form-control" id="notes-field" rows="4" name="notes">{{ is_null(old("notes")) ? $shipment_tool_order->notes : old("notes") }}</textarea>
        @if($errors->has("notes"))
        <span class="help-block">{{ $errors->first("notes") }}</span>
        @endif
      </div>

      <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
        <button type="submit" class="btn btn-primary">{{__('backend.save')}}</button>
        <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.shipment_tool_orders.index') }}"><i class="glyphicon glyphicon-backward"></i>  {{__('backend.back')}}</a>
      </div>
    </form>
  </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
  $(function(){

    $("form[name='validateForm']").validate({
      // Specify validation rules
      rules: {
        agent_id: {
          required: true,
        },
        status: {
          required: true,
        },
        quantity: {
          required: true,
        },
        items: {
          required: true,
        },
        address: {
          required: true,
        },
      },
      // Specify validation error messages
      messages: {
        agent_id: {
          required: "{{__('backend.Please_Chosse_Agent_Name')}}",
        },
        status: {
          required: "{{__('backend.Please_Enter_Status_Tool')}}",
        },
        quantity: {
          required: "{{__('backend.Please_Enter_The_Quantity')}}",
        },
        items: {
          required: "{{__('backend.Please_Enter_Shipment_Tool')}}",
        },
        address: {
          required: "{{__('backend.Please_Enter_The_Address')}}",
        },
      },

      errorPlacement: function(error, element) {
          $(element).parents('.form-group').append(error)
      },
      highlight: function (element) {
          $(element).parent().addClass('error')
      },
      unhighlight: function (element) {
          $(element).parent().removeClass('error')
      },
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function(form) {
          form.submit();
      }
    });

    $("body").on("click", ".remove", function () {
      $(this).closest(".remove-field").remove();
    });

    $(".add-more").on('click',function(){
      $.ajax({
        url: "{{url('/mngrAdmin/new_shippeng_field/')}}" ,
        type: 'get',
        success: function (data) {
          $('#fields').append(data.view);
        },
        error: function (data) {
          console.log('Error:', data);
        }
      });
    });
  });
</script>
@endsection

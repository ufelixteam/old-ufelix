<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use App\Http\Requests;
use App\Models\Area;
use App\Models\Country;
use App\Models\Governorate;
use App\Models\GovernorateKeyword;
use App\Models\GovernoratePrice;
use Auth;
use File;
use Flash;
use Illuminate\Http\Request;
use Response;

class governorateController extends Controller
{

    // Get Governorate By Country ID
    public function get_governorate($id)
    {
        return Governorate::where('country_id', $id)->get();
    }

    // Get All Governorate
    public function index()
    {
        $governorates = Governorate::orderBy('id', 'desc')->paginate(15);
        return view('backend.governorates.index', compact('governorates'));
    }

    // Create Governorate Page
    public function create()
    {
        $list_countries = Country::orderBy('name', 'ASC')->get();
        $areas = Area::orderBy('order', 'ASC')->get();
        return view('backend.governorates.create', compact('list_countries', 'areas'));
    }

    // Added New Governorate
    public function store(Request $request)
    {

        /*
        *	status = 0 :: Not Verify
        *	status = 1 :: Verify
        */
        $v = \Validator::make($request->all(), [
            'name_en' => 'required',
            'name_ar' => 'required',
            'country_id' => 'required',
            'area_id' => 'required',
            'status' => 'required',
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        $governorate = new Governorate();
        $governorate->name_en = $request->input("name_en");
        $governorate->name_ar = $request->input("name_ar");
        $governorate->country_id = $request->input("country_id");
        $governorate->status = $request->input("status");
        $governorate->area_id = $request->input("area_id");
        $governorate->save();

        if ($request->filled('keywords')) {
            $keywords = explode(',', $request->keywords);
            foreach ($keywords as $keyword) {
                GovernorateKeyword::create(['governorate_id' => $governorate->id, 'keyword' => $keyword]);
            }
        }

        // Function Add Action In Activity Log By Samir
        addActivityLog('Add New Governorate ' . $governorate->name_en, ' اضافة محافظة جديدة ' . $governorate->name_ar, 27); // Helper Function in App\Helpers.php

        Flash::success(__('backend.Governorate_Added_successfully'));
        return redirect()->route('mngrAdmin.governorates.index');
    }

    // View One Governorate
    public function show($id)
    {
        $governorate = Governorate::findOrFail($id);
        return view('backend.governorates.show', compact('governorate'));
    }

    // Edit Governorate Page
    public function edit($id)
    {
        $governorate = Governorate::findOrFail($id);
        $list_countries = Country::orderBy('name', 'ASC')->get();
        $areas = Area::orderBy('order', 'ASC')->get();
        return view('backend.governorates.edit', compact('governorate', 'list_countries', 'areas'));
    }

    // Update Governorate
    public function update(Request $request, $id)
    {

        /*
        *	status = 0 :: Not Verify
        *	status = 1 :: Verify
        */
        $v = \Validator::make($request->all(), [
            'name_en' => 'required',
            'name_ar' => 'required',
            'country_id' => 'required',
            'area_id' => 'required',
            'status' => 'required',
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        $governorate = Governorate::findOrFail($id);
        $governorate->name_en = $request->input("name_en");
        $governorate->name_ar = $request->input("name_ar");
        $governorate->country_id = $request->input("country_id");
        $governorate->status = $request->input("status");
        $governorate->area_id = $request->input("area_id");
        $governorate->save();

        GovernorateKeyword::where('governorate_id', $id)->delete();

        if ($request->filled('keywords')) {
            $keywords = explode(',', $request->keywords);
            foreach ($keywords as $keyword) {
                GovernorateKeyword::create(['governorate_id' => $id, 'keyword' => $keyword]);
            }
        }

        // Function Add Action In Activity Log By Samir
        addActivityLog('Edit Governorate ' . $governorate->name_en, ' تعديل المحافظة ' . $governorate->name_ar, 27); // Helper Function in App\Helpers.php

        Flash::warning(__('backend.Governorate_Updated_successfully'));
        return redirect()->route('mngrAdmin.governorates.index');
    }

    // Delete Governorate
    public function destroy($id)
    {
        $governorate = Governorate::findOrFail($id);
        $gPrice = GovernoratePrice::where('start_station', $id)->orWhere('access_station', $id)->first();

        if (!empty($gPrice)) { // If This Governorate Have Any Price Not Deleted Governorate
            Flash::error(__('backend.Theres_a_Shipment_Line_already_setup_for_this_Governorate_try_to_delete_it_first'));
            return redirect()->route('mngrAdmin.governorates.index');
        } else {    // If This Governorate Have Any Price Deleted Governorate
            $governorate->delete();

            // Function Add Action In Activity Log By Samir
            addActivityLog('Delete Governorate ' . $governorate->name_en, ' حذف المحافظة ' . $governorate->name_ar, 27); // Helper Function in App\Helpers.php

            flash(__('backend.Governorate_deleted_successfully'))->error();
            return redirect()->route('mngrAdmin.governorates.index');
        }
    }

}

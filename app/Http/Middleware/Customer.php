<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Flash;
class Customer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next = null, $guard = 'Customer')
     {

        if (! Auth::guard($guard)->user() ) {

            return redirect('/login');

        }else {
            \Config::set('auth.defines.guard','Customer');
            if ($request->session()->has('locale')  ) {
                $locale = $request->session()->get('locale');
                App::setLocale($locale);
            } 
            return $next($request);

        }
      /*  \Config::set('auth.defaults.guard','Customer');

        if (! Auth::guard($guard)->check() && $guard == 'Customer' ) {

            return redirect('/login');

        }else {
            if(auth()->guard('Customer')->user()->is_active != '1' && auth()->guard('Customer')->user()->Corporate->is_active != '1' )
            {
                Flash::warning('Please Verify Your E-mail .. !!');

            }else if(auth()->guard('Customer')->user()->is_verify_mobile != 1 )
            {
                Flash::warning('Please Verify Your Mobile .. !!');

            }

            return $next($request);
        }
        */
     }
}

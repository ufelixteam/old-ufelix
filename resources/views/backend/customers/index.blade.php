@extends('backend.layouts.app')

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <style>
        .toggle.ios, .toggle-on.ios, .toggle-off.ios {
            border-radius: 20px;
        }

        .toggle.ios .toggle-handle {
            border-radius: 20px;
        }
    </style>
    <title>{{__('backend.customers')}}</title>
    <style type="text/css">
        @media only screen and (max-width: 580px) {
            .hide-td {
                display: none;
            }
        }

        .box-body {
            min-height: 100px;
        }

        .stylish-input-group .input-group-addon {
            background: white !important;
        }

        .stylish-input-group .form-control {
            border-right: 0;
            box-shadow: 0 0 0;
            border-color: #ccc;
        }

        .stylish-input-group button {
            border: 0;
            background: transparent;
        }
    </style>
@endsection

@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.corporate_users')}}
            {{--      @if(permission('addCustomer')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->--}}
            {{--        <a class="btn btn-success pull-right" href="{{ route('mngrAdmin.customers.create') }}"><i class="glyphicon glyphicon-plus"></i> {{__('backend.add_user')}} </a>--}}
            {{--      @endif--}}
        </h3>
    </div>
@endsection

@section('content')
    <div class="row" style="margin-bottom:15px;">
        <div class="col-md-12">
            <div class="col-md-6 col-sm-6">
                <form action="{{URL::asset('/mngrAdmin/customers')}}" method="get" id="search-form"/>
                <div id="imaginary_container">
                    <div class="input-group stylish-input-group">
                        <input type="text" class="form-control" id="search-field" name="search"
                               placeholder="Search by Name Or Mobile ">
                        <span class="input-group-addon">
                <button type="button" id="search-btn1">
                  <span class="glyphicon glyphicon-search"></span>
                </button>
              </span>
                    </div>
                </div>
                </form>
            </div>
            <div class="group-control col-md-6 col-sm-6">
                <select id="block-field" name="block" class="form-control">
                    <option value="-1">{{__('backend.sort_by_blocked')}}</option>
                    <option value="0">{{__('backend.not_blocked')}}</option>
                    <option value="1">{{__('backend.blocked')}}</option>
                </select>
            </div>

        </div>
    </div>
    <div class="list">
        @include('backend.customers.table')
    </div>
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <script>
        $('.toggle').bootstrapToggle();

        $("#search-btn1").on('click', function () {
            // Wait icon
            $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
            $.ajax({
                url: '{{URL::asset("/mngrAdmin/customers")}}' + '?block=' + $("#block-field").val() + '&&search=' + $(this).val(),
                type: 'get',
                data: $("#search-form").serialize(),
                success: function (data) {
                    $('.list').html(data.view);
                    // $('#theTable').DataTable({
                    //   "pagingType": "full_numbers"
                    // });
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $("#block-field").on('change', function () {
            $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
            $.ajax({
                url: '{{URL::asset("/mngrAdmin/customers")}}' + '?block=' + $("#block-field").val() + '&&search=' + $(this).val(),
                type: 'get',
                success: function (data) {
                    $('.list').html(data.view);
                    // $('#theTable').DataTable({
                    //   "pagingType": "full_numbers"
                    // });
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $('.verify_mobile').on('change', function () {
            let id = $(this).attr('data-id');
            $.ajax({
                url: '{{URL::asset("/mngrAdmin/verify-mobile-customer")}}',
                type: 'post',
                data: {id: id, _token: "{{csrf_token()}}"},
                success: function (data) {

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        })

        $('.verify_email').on('change', function () {
            let id = $(this).attr('data-id');
            $.ajax({
                url: '{{URL::asset("/mngrAdmin/verify-email-customer")}}',
                type: 'post',
                data: {id: id, _token: "{{csrf_token()}}"},
                success: function (data) {

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        })

        $('.change_block').on('change', function () {
            let id = $(this).attr('data-id');
            $.ajax({
                url: '{{URL::asset("/mngrAdmin/change-block-customer")}}',
                type: 'post',
                data: {id: id, _token: "{{csrf_token()}}"},
                success: function (data) {

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        })

    </script>

@endsection

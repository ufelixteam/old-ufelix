@extends('agent.layouts.app')
@section('css')

@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-plus"></i> Create Shipment Tool Orders </h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <form action="{{ route('mngrAgent.shipment_tool_orders.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="status" value="0">
                <input type="hidden" name="object_id" value="{{Auth::guard('agent')->user()->agent_id}}">
                    <div id="fields">
                    <div class="col-sm-12 col-md-12"  style="margin-top: 10px;">
                      <div class="col-sm-5 col-md-5">
                        <input required  type="number"  name="quantity[]" class="form-control" placeholder="Quantity"  />
                      </div>

                      <div class="form-group col-md-6 col-sm-6 @if($errors->has('items')) has-error @endif">
                           <select id="items-field" name="items[]" id="items" class="form-control">
                                @foreach($shipment_tools as $item)
                                    <option value="{{$item->id}}" {{ old("items") == $item->id ? 'selected' : '' }}
                                    >{{$item->name_en}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="input-group">
                          <span class="input-group-btn">
                              <input type="button" id="add-field" value="+" class="btn btn-success add-more">
                          </span>
                      </div>

                    </div>

                  </div>
                    <div class="col-sm-12 col-md-12" style="margin-bottom: 15px;">
                      <label for="notes-field">Address</label>
                      <input type="text" name="address" class="form-control" placeholder="address" required />
                    </div>

                    <div class="form-group col-md-12 col-sm-12 @if($errors->has('notes')) has-error @endif">
                       <label for="notes-field">Notes</label>
                    <textarea class="form-control" id="notes-field" rows="4" name="notes">{{ old("notes") }}</textarea>
                       @if($errors->has("notes"))
                        <span class="help-block">{{ $errors->first("notes") }}</span>
                       @endif
                    </div>

                <div class="well well-sm col-md-12 col-sm-12 col-xs-12">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAgent.shipment_tool_orders.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script type="text/javascript">
    $(function(){
      $("body").on("click", ".remove", function () {
        $(this).closest(".remove-field").remove();

      });

      $(".add-more").on('click',function(){
        $.ajax({
            url: "{{url('/mngrAgent/new_shippeng_field/')}}" ,
            type: 'get',

            success: function (data) {
                $('#fields').append(data.view);

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });



      });
    });
  </script>
@endsection

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderComment extends Model
{
    protected $guarded = ['id'];

    public function getCommentAttribute($value){
        $pattern = '/[^\x{0600}-\x{065F}\x{066A}-\x{06EF}\x{06FA}-\x{06FF}a-zA-Z0-9\x{0621}-\x{064A0}-9\s]/u';
        $output = preg_replace($pattern, "", $value);

        return $output;
    }

    public function getUserAttribute()
    {
        /*
        * user_type == 1 :: User
        * user_type == 2 :: Customer
        * user_type == 3 :: Driver
        */
        if ($this->user_type == 1) {
            return $this->csUser;
        } else if ($this->user_type == 2) {
            return $this->profileUser;
        } else if ($this->user_type == 3) {
            return $this->driverUser;
        }
    }

    public function driver()
    {
        return $this->belongsTo('App\Models\Driver', 'driver_id');
    }

    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'order_id');
    }

    public function csUser()
    {
        if ($this->user_type == 1) {
            return $this->belongsTo('App\Models\User', 'user_id');
        } else {
            return false;
        }
    }

    public function profileUser()
    {
        if ($this->user_type == 2) {
            return $this->belongsTo('App\Models\Customer', 'user_id');
        } else {
            return false;
        }
    }

    public function driverUser()
    {
        if ($this->user_type == 3) {
            return $this->belongsTo('App\Models\Driver', 'user_id');
        } else {
            return false;
        }
    }

}

<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        @if($ticketRules->count())
            <table class="table table-condensed table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('backend.name')}}</th>
                    <th>{{__('backend.active')}}</th>
                    <th>{{__('backend.created_at')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($ticketRules as $ticketRule)
                    <tr>
                        <td>{{$ticketRule->id}}</td>
                        <td>{{$ticketRule->name}}</td>
                        <td class="exclude-td">
                            <input data-id="{{$ticketRule->id}}" data-size="mini" class="toggle change_active"
                                   {{$ticketRule->status == 1 ? 'checked' : ''}} data-onstyle="success"
                                   type="checkbox" data-style="ios" data-on="Yes" data-off="No">

                        </td>
                        <td>{{$ticketRule->created_at}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {!! $ticketRules->appends($_GET)->links() !!}

        @else
            <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}!</h3>
        @endif
    </div>
</div>

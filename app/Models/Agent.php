<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{

    public function getAgentData(){
        return $this->hasOne('App\Models\User','agent_id');
    }

    public function getEmailAttribute()
    {
        $user = User::where('agent_id',$this->id)->first();
        if (! empty($user)) {
          return $user->email;
        }
        return "";
      }

    public function getStatusSpanAttribute($value)
    {
        /*
        * status = 0 :: Not Active
        * status = 1 :: Active
        */
        if($this->status == 0){
            $value="<span class='badge badge-pill label-danger'>".__('backend.not_activated')."</span>";
        }else if($this->status == 1){
            $value="<span class='badge badge-pill label-success'>".__('backend.active')."</span>";
        }else{
          $value = "";
        }
        return $value;
    }

  	public function getStatusTxtAttribute($value) {
        /*
        * status = 0 :: Not Active
        * status = 1 :: Active
        */
        if($this->status == 0 || $this->status != 1) {
            $value= __('backend.active');
        } elseif($this->status == 1) {
            $value= __('backend.not_activated');
        } else {
          $value = "";
        }
        return $value;
    }

    public function wallet()
    {
        return $this->hasOne('App\Models\Wallet','object_id');
    }

}

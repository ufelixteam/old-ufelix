<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TempOrder extends Model
{

    public function getTotalPriceAttribute()
    {
        $delivery_fees = $this->delivery_price;
        $order_price = $this->order_price;
        $payment_method = $this->payment_method_id;
        $total_price = 0;
        if ($payment_method == "1") {
            $total_price = $order_price ? $order_price : $delivery_fees;
        } else if ($payment_method == "2") {
            $total_price = $delivery_fees + $order_price;
        }

        return $total_price;
    }

    //this model for temp order before saving to orders
    public function sender_city()
    {
        return $this->belongsTo('App\Models\City', 's_state_id');
    }

    public function receiver_city()
    {
        return $this->belongsTo('App\Models\City', 'r_state_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }

    public function stock()
    {
        return $this->belongsTo('App\Models\Stock', 'store_id');
    }
}

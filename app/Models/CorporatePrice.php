<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CorporatePrice extends Model
{
    protected $hidden = ['created_at', 'updated_at'];

    protected $guarded = ['id'];

    public function Start()
    {
        return $this->belongsTo('App\Models\Governorate', 'start_station');
    }

    public function End()
    {
        return $this->belongsTo('App\Models\Governorate', 'access_station');
    }

    public function corporate()
    {
        return $this->belongsTo('App\Models\Corporate', 'corporate_id');
    }

    public function getStatusSpanAttribute($value)
    {
        /*
        * status = 0 :: Not Verify
        * status = 1 :: Verify
        */
        if ($this->status == 0) {
            $value = "<span class='badge badge-pill label-danger'>" . __('backend.not_verify') . "</span>";
        } else if ($this->status == 1) {
            $value = "<span class='badge badge-pill label-success'>" . __('backend.verfiy') . "</span>";
        } else {
            $value = "";
        }
        return $value;
    }
}

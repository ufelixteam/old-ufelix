@extends('frontend.layouts.app')

@section('title') @lang('website.Terms') @endsection

@section('content')
<section class="details-contact-p">
	<div class="container">
    	<div class="col-xs-12 title-cont-g">
        	<h1>@lang('website.Terms')</h1>
        </div>
		<div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
	        <div class="btn-group" role="group">
	            <button type="button" id="stars" class="btn btn-primary" href="#tab1" data-toggle="tab"><span class="fa fa-user" aria-hidden="true"></span>
	                <div class="hidden-xs">@lang('website.client')</div>
	            </button>
	        </div>
	        <div class="btn-group" role="group">
	            <button type="button" id="favorites" class="btn btn-default" href="#tab2" data-toggle="tab"><span class="fa fa-heart" aria-hidden="true"></span>
	                <div class="hidden-xs">@lang('website.captain')</div>
	            </button>
	        </div>

	    </div>

	        <div class="well" >
	      <div class="tab-content">
	        <div class="tab-pane fade in active pre-wrap" id="tab1" >
	          @if(! empty($app_about)  && ! empty($app_about['client_terms'] ) )  {!! app()->getLocale() == 'en' ? $app_about['client_terms'] : $app_about['client_terms_ar'] !!} @endif
	        </div>
	        <div class="tab-pane fade in pre-wrap" id="tab2" >
	          @if(! empty($app_about)  && ! empty($app_about['driver_terms'] ) )  {!! app()->getLocale() == 'en' ? $app_about['driver_terms'] : $app_about['driver_terms_ar'] !!} @endif
	        </div>

	      </div>
	    </div>

	</div>
</section>

@endsection


@section('scripts')
<script>
	$(document).ready(function() {
$(".btn-pref .btn").click(function () {
    $(".btn-pref .btn").removeClass("btn-primary").addClass("btn-default");
    // $(".tab").addClass("active"); // instead of this do the below
    $(this).removeClass("btn-default").addClass("btn-primary");
});
});
</script>
@endsection

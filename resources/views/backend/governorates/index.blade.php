@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.governorates')}}</title>
@endsection

@section('header')
<div class="page-header clearfix">
  <h3>
    <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.governorates')}}
    @if(permission('addGovernorate')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
      <a class="btn btn-success pull-right" href="{{ route('mngrAdmin.governorates.create') }}"><i class="glyphicon glyphicon-plus"></i> {{__('backend.create')}}</a>
    @endif
  </h3>
</div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
      @if($governorates->count())
        <table class="table table-condensed table-striped">
          <thead>
            <tr>
              <th>#</th>
              <th>{{__('backend.governorate')}}</th>
              <th>{{__('backend.country')}}</th>
              <th>{{__('backend.status')}}</th>
              <th class="text-right"></th>
            </tr>
          </thead>
          <tbody>
            @foreach($governorates as $governorate)
              <tr>
                <td>{{$governorate->id}}</td>
                <td>
                  @if(session()->has('lang') == 'ar')

                    {{$governorate->name_ar}}
                  @else
                    {{$governorate->name_en}}
                  @endif
                </td>
                <td>{{! empty($governorate->Country) ? $governorate->Country->name : ''}}</td>
                <td>{!! $governorate->status_span !!}</td>
                <td class="text-right">
                  {{--<a class="btn btn-xs btn-primary" href="{{route('mngrAdmin.governorates.show', $governorate->id)}}"><i class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>--}}
                  @if(permission('editGovernorate')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                    <a class="btn btn-xs btn-warning" href="{{ route('mngrAdmin.governorates.edit', $governorate->id)}}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                  @endif

                  @if(permission('deleteGovernorate')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                    <form action="{{ route('mngrAdmin.governorates.destroy', $governorate->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
                      <input type="hidden" name="_method" value="DELETE">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}</button>
                    </form>
                  @endif
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>

        {!! $governorates->appends($_GET)->links() !!}

      @else
        <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
      @endif
    </div>
  </div>
@endsection

@section('scripts')
  <script type="text/javascript">
    $('#theTable').DataTable({
      "pagingType": "full_numbers"
    });
  </script>
@endsection

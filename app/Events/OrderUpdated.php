<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;

class OrderUpdated
{
    use SerializesModels;

    public $ids;
    public $user_id;
    public $user_type;
    public $action;
    public $data;

    /**
     * Create a new event instance.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function __construct($ids, $user_id, $user_type, $action, $data = [])
    {
        $this->ids = $ids;
        $this->user_id = $user_id;
        $this->user_type = $user_type;
        $this->action = $action;
        $this->data = $data;
    }
}

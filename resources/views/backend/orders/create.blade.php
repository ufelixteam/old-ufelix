@extends('backend.layouts.app')
@section('css')
    <style>
        .input-group {
            -moz-box-shadow: none !important;
            -webkit-box-shadow: none !important;
            box-shadow: none !important;
        }

        .input-group[class*=col-] {
            padding-right: inherit !important;
            padding-left: inherit !important;
            float: inherit;
        }

        .pac-container {
            z-index: 9999 !important;
        }

    </style>
    <title>{{__('backend.the_orders')}} - {{__('backend.add_order')}}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
@endsection
@section('header')
    <div class="page-header">
        <h4>
            <i class="glyphicon glyphicon-plus"></i> {{__('backend.Create_Orders_Collection')}}</h4>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="alert alert-danger" style="display:none"></div>

        </div>
        <div id="create-form" class="create-form">
            @include('backend.orders.create-form')

        </div>

        <div id="js-form" class="js-form">
        </div>

    </div>
    @include('backend.dialog-map1')
    @include('backend.dialog-map2')

    <div class="row" style="margin-top:30px;">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="alert alert-success" style="display:none"></div>
            <div class="table-responsive">

                <div class="list">
                    @include('backend.orders.tmptable')

                </div>
            </div>
        </div>
    </div>



    <div class="row" style="margin-top:20px;">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <form action="{{URL::asset('mngrAdmin/save_collection')}}" method="post" id="saveCollection">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="collection_id" id="saved_collection_id"
                       value="{{ ! empty($collection_id) ? $collection_id : '0' }}">
                {{-- <button type="submit" class="btn btn-success btn-block" id="saved" @if(! ( ! empty($collection_id) &&  count($collection_id) > 0 && ! empty($orders) && count($orders) > 0 ) ) style="display: none;"   @endif>Save Collection To Orders</button> --}}
                <button type="submit" class="btn btn-success btn-block" id="saved"
                        @if(! ( ! empty($collection_id) && ! empty($orders) ) ) style="display: none;" @endif>Save
                    Collection To Orders
                </button>

            </form>

        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">


            <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.orders.index') }}"><i
                    class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
        </div>
    </div>
@endsection



@section('scripts')
    <script type="text/javascript" src="{{asset('/assets/scripts/map-order.js')}}"></script>

    <script
        src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key=AIzaSyDQiN4qc7pC9b1BRQwkqXXD28peMfWcHvw&callback=myMap"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    @include('backend.orders.js')

    <script>
        // var isDirty = function() { return true; }
        //
        // window.onload = function() {
        //     window.addEventListener("beforeunload", function (e) {
        //         if (!isDirty()) {
        //             return undefined;
        //         }
        //
        //         var confirmationMessage = 'It looks like you have been editing something. '
        //             + 'If you leave before saving, your changes will be lost.';
        //
        //         (e || window.event).returnValue = confirmationMessage; //Gecko + IE
        //         return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
        //     });
        // };

        var isDirty = function () {
            return false;
        }
        // window.onpageshow = function(event) {
        //     if (event.persisted) {
        //         window.isRefresh = true;
        //         window.location.reload();
        //     }
        // };

        window.onbeforeunload = function () {
            if (/*!window.isRefresh &&*/ !isDirty()) {
                $.ajax({
                    type: 'POST',
                    url: '{{url('mngrAdmin/collection_orders/delete_collection')}}',
                    async: true,
                    data: {'_token': "{{csrf_token()}}", collection_id: $("#saved_collection_id").val()},
                    success: function (data) {
                    }
                });
            }
            // window.isRefresh = false;
        }

        window.onload = function () {
            window.addEventListener('beforeunload', function (e) {
                if (!isDirty()) {
                    // Cancel the event as stated by the standard.
                    e.preventDefault();
                    // Chrome requires returnValue to be set.
                    e.returnValue = '';
                }
            });
        }

        $(function () {
            $("#saveCollection").submit(function () {
                isDirty = true;
            });
        });
    </script>

@endsection

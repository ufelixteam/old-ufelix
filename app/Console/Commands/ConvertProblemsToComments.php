<?php

namespace App\Console\Commands;

use App\Models\DeliveryProblem;
use App\Models\OrderComment;
use App\Models\OrderDeliveryProblem;
use Illuminate\Console\Command;

class ConvertProblemsToComments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'problems:convert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert Problems To Comments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        OrderDeliveryProblem::chunk(200, function ($problems) {
            foreach ($problems as $problem) {
                $reason = $problem->reason;
                if(!$reason && $problem->delivery_problem_id){
                    $reason = DeliveryProblem::where('id', $problem->delivery_problem_id)->value('reason_ar');
                }

                $comment = new OrderComment();
                $comment->order_id = $problem->order_id;
                $comment->comment = $reason;
                $comment->latitude = $problem->latitude;
                $comment->longitude = $problem->longitude;
                $comment->user_type = 1;
                $comment->user_id = ($problem->created_by ? $problem->created_by : 44);
                $comment->is_from_problem = 1;
                $comment->driver_id = $problem->driver_id;
                $comment->created_at = $problem->created_at;
                $comment->updated_at = $problem->updated_at;
                $comment->save();

            }
        });

    }
}

<div id="sidebar-nav" class="sidebar">
    <div class="sidebar-scroll">
        <nav>
            <ul class="nav">
                <li><a href="{{url('mngrAdmin')}}" class="{{ Request::is('mngrAdmin/dashboard')  ? 'active' : '' }}">
                        <i class="lnr lnr-home"></i> <span>{{__('backend.dashboard')}}</span>
                    </a>
                </li>
            @if(permission('corporates')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <li><a href="{!! URL::asset('mngrAdmin/corporates') !!}"><i
                            class="fa fa-building"></i><span>{{__('backend.corporates')}}</span></a></li>
                @endif
                {{--                <li>--}}
                {{--                    <a href="#catPages" data-toggle="collapse"--}}
                {{--                       class="{{ Request::is('mngrAdmin/corporates*') || Request::is('mngrAdmin/customers*') || Request::is('mngrAdmin/agents*')  ? 'active' : 'collapsed' }} "--}}
                {{--                       aria-expanded="{{ Request::is('mngrAdmin/corporates*') || Request::is('mngrAdmin/customers*') || Request::is('mngrAdmin/agents*')  ? 'true' : 'false' }} ">--}}
                {{--                        <i class="lnr lnr-users"></i> <span>{{__('backend.clients')}}</span> <i--}}
                {{--                            class="icon-submenu lnr lnr-chevron-left"></i></a>--}}
                {{--                    <div id="catPages"--}}
                {{--                         class="collapse {{  Request::is('mngrAdmin/corporates*') || Request::is('mngrAdmin/customers*') || Request::is('mngrAdmin/agents*')  ? 'in' : '' }}--}}
                {{--                             "--}}
                {{--                         aria-expanded="{{ Request::is('mngrAdmin/agents*') || Request::is('mngrAdmin/customers*') || Request::is('mngrAdmin/corporates*')  ? 'true' : 'false' }}">--}}
                {{--                        <ul class="nav">--}}
                {{--                        @if(permission('customers')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->--}}
                {{--                            <li><a href="{!! URL::asset('mngrAdmin/customers') !!}"><i--}}
                {{--                                        class="fa fa-users"></i><span>{{__('backend.corporate_users')}}</span></a>--}}
                {{--                            </li>--}}
                {{--                        @endif--}}

                {{--                        @if(permission('corporates')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->--}}
                {{--                            <li><a href="{!! URL::asset('mngrAdmin/corporates') !!}"><i--}}
                {{--                                        class="fa fa-building"></i><span>{{__('backend.corporates')}}</span></a></li>--}}
                {{--                            @endif--}}

                {{--                            --}}{{--                        @if(permission('agents')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->--}}
                {{--                            --}}{{--                            <li><a href="{!! URL::asset('mngrAdmin/agents') !!}"><i--}}
                {{--                            --}}{{--                                        class="fa fa-user-secret"></i><span>{{__('backend.agents')}}</span></a></li>--}}
                {{--                            --}}{{--                            @endif--}}

                {{--                        </ul>--}}
                {{--                    </div>--}}
                {{--                </li>--}}

                <li>
                    <a href="#carPages" data-toggle="collapse"
                       class="{{ Request::is('mngrAdmin/trucks*') || Request::is('mngrAdmin/drivers*') || Request::is('mngrAdmin//track_drivers/all')  ? 'active' : 'collapsed' }} "
                       aria-expanded="{{Request::is('mngrAdmin/trucks*') || Request::is('mngrAdmin/drivers*') || Request::is('mngrAdmin//track_drivers/all')  ? 'true' : 'false' }} ">
                        <i class="lnr lnr-car"></i> <span>{{__('backend.drivers')}}</span> <i
                            class="icon-submenu lnr lnr-chevron-left"></i></a>
                    <div id="carPages"
                         class="collapse {{  Request::is('mngrAdmin/trucks*') || Request::is('mngrAdmin/drivers*') || Request::is('mngrAdmin//track_drivers/all')  ? 'in' : '' }}
                             "
                         aria-expanded="{{ Request::is('mngrAdmin/trucks*') || Request::is('mngrAdmin/drivers*') || Request::is('mngrAdmin//track_drivers/all')  ? 'true' : 'false' }}">
                        <ul class="nav">
                        @if(permission('drivers')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                            <li><a href="{!! URL::asset('mngrAdmin/drivers') !!}"><i
                                        class="fa fa-car"></i><span>{{__('backend.drivers')}}</span></a></li>
                        @endif

                        @if(permission('Vehicles')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                            <li><a href="{!! URL::asset('mngrAdmin/trucks') !!}"><i
                                        class="fa fa-truck"></i><span>{{__('backend.vehicles')}}</span></a></li>
                            @endif
                            <li><a href="{!! URL::asset('mngrAdmin/track_drivers/all') !!}"><i
                                        class="fa fa-map"></i><span>{{__('backend.map')}}</span></a></li>
                        </ul>
                    </div>
                </li>
                @if(permission('wallets'))
                    <li>
                        <a href="#walletPages" data-toggle="collapse"
                           class="{{ Request::is('mngrAdmin/wallets*')  ? 'active' : 'collapsed' }} "
                           aria-expanded="{{Request::is('mngrAdmin/wallets*')  ? 'true' : 'false' }} ">
                            <i class="fa fa-money "></i> <span>{{__('backend.the_wallets')}}</span> <i
                                class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="walletPages" class="collapse {{  Request::is('mngrAdmin/wallets*')  ? 'in' : '' }}
                            " aria-expanded="{{ Request::is('mngrAdmin/wallets*')  ? 'true' : 'false' }}">
                            <ul class="nav">
                                <li><a href="{!! URL::asset('mngrAdmin/wallets?type=1') !!}"><i
                                            class="fa fa-info"></i><span>{{__('backend.drivers')}}</span></a></li>
                                <li><a href="{!! URL::asset('mngrAdmin/wallets?type=2') !!}"><i class="fa fa-check"></i><span>{{__('backend.corporates')}}</span></a>
                                </li>
                                {{--                                <li><a href="{!! URL::asset('mngrAdmin/wallets?type=3') !!}"><i class="fa fa-close"></i><span>{{__('backend.agents')}}</span></a>--}}
                                {{--                                </li>--}}
                                <li><a href="{!! URL::asset('mngrAdmin/wallets/1') !!}"><i
                                            class="fa fa-cc"></i><span>{{__('backend.Ufelix_Account')}}</span></a></li>
                            </ul>
                        </div>
                    </li>
                @endif

                @if(permission('Invoices'))
                    <li>
                        <a href="#invoicePages" data-toggle="collapse"
                           class="{{ Request::is('mngrAdmin/invoices*')  ? 'active' : 'collapsed' }} "
                           aria-expanded="{{Request::is('mngrAdmin/invoices*')  ? 'true' : 'false' }} ">
                            <i class="fa fa-usd"></i> <span>{{__('backend.the_invoices')}}</span> <i
                                class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="invoicePages" class="collapse {{  Request::is('mngrAdmin/invoices*')  ? 'in' : '' }}
                            " aria-expanded="{{ Request::is('mngrAdmin/invoices*')  ? 'true' : 'false' }}">
                            <ul class="nav">
                                <li><a href="{!! URL::asset('mngrAdmin/invoices?type=1') !!}"><i class="fa fa-info"></i><span>{{__('backend.drivers')}}</span></a>
                                </li>
                                <li><a href="{!! URL::asset('mngrAdmin/invoices?type=2') !!}"><i
                                            class="fa fa-check"></i><span>{{__('backend.corporates')}}</span></a></li>
                                {{--                                <li><a href="{!! URL::asset('mngrAdmin/invoices?type=3') !!}"><i--}}
                                {{--                                            class="fa fa-close"></i><span>{{__('backend.agents')}}</span></a></li>--}}
                                {{--                                <li><a href="{!! URL::asset('mngrAdmin/orders/showOrders/individuals') !!}"><i--}}
                                {{--                                            class="fa fa-users"></i><span>{{__('backend.individuals')}}</span></a></li>--}}
                            </ul>
                        </div>
                    </li>
                @endif
                @if(permission('orders')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <li>
                    <a href="#orderPages" data-toggle="collapse"
                       class="{{ Request::is('mngrAdmin/orders*')  ? 'active' : 'collapsed' }} "
                       aria-expanded="{{Request::is('mngrAdmin/orders*')  ? 'true' : 'false' }} ">
                        <i class="fa fa-tree"></i> <span>{{__('backend.the_orders')}}</span> <i
                            class="icon-submenu lnr lnr-chevron-left"></i></a>
                    <div id="orderPages" class="collapse {{  Request::is('mngrAdmin/orders*')  ? 'in' : '' }}
                        " aria-expanded="{{ Request::is('mngrAdmin/orders*')  ? 'true' : 'false' }}">
                        <ul class="nav">
                            <li><a href="{!! URL::asset('mngrAdmin/orders?type=pending') !!}"><i class="fa fa-info"></i><span>{{__('backend.Pending_Orders')}}</span></a>
                            </li>
                            <li><a href="{!! URL::asset('mngrAdmin/orders?type=dropped') !!}"><i
                                        class="fa fa-download"></i><span>{{__('backend.Dropped_Orders')}}</span></a>
                            </li>
                            <li><a href="{!! URL::asset('mngrAdmin/orders?type=accept') !!}"><i class="fa fa-check"></i><span>{{__('backend.Accepted_Orders')}}</span></a>
                            </li>
                            <li><a href="{!! URL::asset('mngrAdmin/orders?type=receive') !!}"><i
                                        class="fa fa-battery-half"></i><span>{{__('backend.Received_Orders')}}</span></a>
                            </li>
                            <li><a href="{!! URL::asset('mngrAdmin/orders?type=deliver') !!}"><i
                                        class="fa fa-battery"></i><span>{{__('backend.Delivered_Orders')}}</span></a>
                            </li>
                            <li><a href="{!! URL::asset('mngrAdmin/orders?type=cancel') !!}"><i class="fa fa-close"></i><span>{{__('backend.Unverified_Orders')}}</span></a>
                            </li>
                            <li><a href="{!! URL::asset('mngrAdmin/orders?type=recall') !!}"><i
                                        class="fa fa-history"></i><span>{{__('backend.Recalled_Orders')}}</span></a>
                            </li>
                        {{--                            <li><a href="{!! URL::asset('mngrAdmin/orders?type=waiting') !!}"><i class="fa fa-info"></i><span>{{__('backend.Waiting_Orders')}}</span></a>--}}
                        {{--                            </li>--}}
                        @if(permission('moveCollections')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                            <li><a href="{!! URL::asset('mngrAdmin/move_collections') !!}">
                                    <i class="fa fa-file-text"></i> <span>{{__('backend.move_collections')}}</span>
                                </a>
                            </li>
                            @endif

                            <li><a href="{!! URL::asset('mngrAdmin/orders/all') !!}"><i
                                        class="glyphicon glyphicon-align-justify"></i><span>{{__('backend.all_orders')}}</span></a>
                            </li>
                            {{--                            <li><a href="{!! URL::asset('mngrAdmin/orders?type=expired_waiting') !!}"><i--}}
                            {{--                                        class="fa fa-info"></i><span>{{__('backend.Expired_Waiting_Orders')}}</span></a>--}}
                            {{--                            </li>--}}
                            {{--                            <li><a href="{!! URL::asset('mngrAdmin/collection_orders?type=0') !!}"><i--}}
                            {{--                                        class="fa fa-file-excel-o"></i><span>{{__('backend.Collection_Order_Excel')}}</span></a>--}}
                            {{--                            </li>--}}
                            <li><a href="{!! URL::asset('mngrAdmin/order_cancel_requests/') !!}">
                                    <i class="fa fa-send"></i><span>{{__('backend.order_cancel_requests')}}</span></a>
                            </li>
                        </ul>
                    </div>
                </li>
                @endif

                @if(permission('stores') || permission('Pickups') || permission('PickupPriceList'))
                    <li>
                        <a href="#pickupPages" data-toggle="collapse"
                           class="{{ Request::is('mngrAdmin/pickups*') || Request::is('mngrAdmin/pickup_price_lists*') || Request::is('mngrAdmin/stores*')  ? 'active' : 'collapsed' }} "
                           aria-expanded="{{Request::is('mngrAdmin/pickups*') || Request::is('mngrAdmin/pickup_price_lists*') || Request::is('mngrAdmin/stores*')  ? 'true' : 'false' }} ">
                            <i class="fa fa-money "></i> <span>{{__('backend.task_orders')}}</span> <i
                                class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="pickupPages"
                             class="collapse {{  Request::is('mngrAdmin/pickups*')  || Request::is('mngrAdmin/pickup_price_lists*') || Request::is('mngrAdmin/stores*') ? 'in' : '' }}
                                 "
                             aria-expanded="{{ Request::is('mngrAdmin/pickups*') || Request::is('mngrAdmin/pickup_price_lists*') || Request::is('mngrAdmin/stores*') ? 'true' : 'false' }}">
                            <ul class="nav">
                                @if(permission('Pickups'))
                                    <li><a href="{!! URL::asset('mngrAdmin/pickups') !!}"><i
                                                class="fa fa-info"></i><span>{{__('backend.task_orders')}}</span></a>
                                    </li>
                                @endif


                                @if(permission('stores')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li><a href="{!! URL::asset('mngrAdmin/stores') !!}"><i
                                            class="fa fa-check"></i><span>{{__('backend.pickup_store')}}</span></a></li>

                                @endif

                                @if(permission('PickupPriceList'))
                                    <li><a href="{!! URL::asset('mngrAdmin/pickup_price_lists') !!}"><i
                                                class="fa fa-close"></i><span>{{__('backend.pickup_price_list')}}</span></a>
                                    </li>
                                @endif

                            </ul>
                        </div>
                    </li>
                @endif

                @if(permission('collectionOrders')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <li><a class="{{ Request::is('mngrAdmin/collection_orders*') ? 'active' : '' }}"
                       href="{!! URL::asset('mngrAdmin/collection_orders') !!}">
                        <i class="fa fa-file-text"></i> <span>{{__('backend.Collection_Order_Form')}}</span>
                    </a>
                </li>
                @endif

                @if(permission('recallOrder')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <li><a class="{{ Request::is('mngrAdmin/refund_collections*') ? 'active' : '' }}"
                       href="{!! URL::asset('mngrAdmin/refund_collections') !!}">
                        <i class="fa fa-file-text"></i> <span>{{__('backend.refund_collections')}}</span>
                    </a>
                </li>
                @endif

                @if(permission('scanQRCode')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <li><a class="{{ Request::is('mngrAdmin/scan_collections*') ? 'active' : '' }}"
                       href="{!! URL::asset('mngrAdmin/scan_collections') !!}">
                        <i class="fa fa-file-text"></i> <span>{{__('backend.scan_collections')}}</span>
                    </a>
                </li>
                @endif

                @if(permission('scanQRCode')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <li><a class="{{ Request::is('mngrAdmin/scan_order_qrcode*') ? 'active' : '' }}"
                       href="{!! URL::asset('mngrAdmin/scan_order_qrcode') !!}">
                        <i class="fa fa-camera"></i> <span>{{__('backend.scan_order')}}</span>
                    </a>
                </li>
                @endif

                @if(permission('tickets'))
                    <li>
                        <a href="#tickets" data-toggle="collapse"
                           class="{{ Request::is('mngrAdmin/tickets*')  ? 'active' : 'collapsed' }} "
                           aria-expanded="{{Request::is('mngrAdmin/tickets*')  ? 'true' : 'false' }} ">
                            <i class="fa fa-ticket"></i> <span>{{__('backend.tickets')}}</span> <i
                                class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="tickets" class="collapse {{  Request::is('mngrAdmin/tickets*')  ? 'in' : '' }}
                            " aria-expanded="{{ Request::is('mngrAdmin/tickets*')  ? 'true' : 'false' }}">
                            <ul class="nav">
                                <li><a href="{!! URL::asset('mngrAdmin/tickets') !!}"><i
                                            class="fa fa-ticket"></i><span>{{__('backend.tickets')}}</span></a>
                                </li>
                                <li><a href="{!! URL::asset('mngrAdmin/ticket_rules') !!}"><i
                                            class="fa fa-ticket"></i><span>{{__('backend.ticket_rules')}}</span></a></li>
                            </ul>
                        </div>
                    </li>
                @endif
{{--                @if(permission('tickets')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->--}}
{{--                <li>--}}
{{--                    <a class="{{ Request::is('mngrAdmin/tickets*') ? 'active' : '' }}"--}}
{{--                       href="{!! URL::asset('mngrAdmin/tickets') !!}">--}}
{{--                        <i class="fa fa-ticket"></i> <span>{{__('backend.tickets')}}</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                @endif--}}

                {{--                @if(permission('scanQRCode')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->--}}
                {{--                <li><a class="{{ Request::is('mngrAdmin/scan_qrcode*') ? 'active' : '' }}"--}}
                {{--                       href="{!! URL::asset('mngrAdmin/scan_qrcode') !!}">--}}
                {{--                        <i class="fa fa-camera"></i> <span>{{__('backend.scan')}}</span>--}}
                {{--                    </a>--}}
                {{--                </li>--}}
                {{--                @endif--}}

                @if(permission('notifications') || permission('notificationTypes')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <li>
                    <a href="#notifPages" data-toggle="collapse"
                       class="{{ Request::is('mngrAdmin/notifications*')  ? 'active' : 'collapsed' }} "
                       aria-expanded="{{Request::is('mngrAdmin/notifications*')  ? 'true' : 'false' }} ">
                        <i class="lnr lnr-alarm"></i> <span>{{__('backend.notifications')}}</span> <i
                            class="icon-submenu lnr lnr-chevron-left"></i></a>
                    <div id="notifPages" class="collapse {{  Request::is('mngrAdmin/notifications*')  ? 'in' : '' }}
                        " aria-expanded="{{ Request::is('mngrAdmin/notifications*')  ? 'true' : 'false' }}">
                        <ul class="nav">
                            @if(permission('notifications'))
                                <li><a href="{!! URL::asset('mngrAdmin/notifications') !!}"><i
                                            class="fa fa-bell-o"></i><span>{{__('backend.notifications')}}</span></a>
                                </li>
                            @endif
                            @if(permission('notificationTypes'))
                                <li><a href="{!! URL::asset('mngrAdmin/notifications/types') !!}"><i
                                            class="fa fa-bell-o"></i><span>{{__('backend.Notifications_Types')}}</span></a>
                                </li>
                            @endif
                        </ul>
                    </div>
                </li>
                @endif

                @if(permission('shipmentDestinations') || permission('shipmentOrders') || permission('shipmentTools'))
                    <li>
                        <a href="#shipmentPages" data-toggle="collapse"
                           class="{{ Request::is('mngrAdmin/governoratePrices*') || Request::is('mngrAdmin/shipment_tools*') || Request::is('mngrAdmin/shipment_tool_orders*')  ? 'active' : 'collapsed' }} "
                           aria-expanded="{{ Request::is('mngrAdmin/governoratePrices*') || Request::is('mngrAdmin/shipment_tools*') || Request::is('mngrAdmin/shipment_tool_orders*')  ? 'true' : 'false' }} ">

                            <i class="fa fa-gavel"></i> <span>{{__('backend.shipment')}}</span> <i
                                class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="shipmentPages"
                             class="collapse {{ Request::is('mngrAdmin/governoratePrices*') || Request::is('mngrAdmin/shipment_tools*') || Request::is('mngrAdmin/shipment_tool_orders*')  ? 'in' : '' }}
                                 "
                             aria-expanded="{{ Request::is('mngrAdmin/governoratePrices*') || Request::is('mngrAdmin/shipment_tools*') || Request::is('mngrAdmin/shipment_tool_orders*')  ? 'true' : 'false' }}">
                            <ul class="nav">
                            @if(permission('shipmentTools')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li><a href="{!! URL::asset('mngrAdmin/shipment_tools') !!}"><i
                                            class="fa fa-gavel"></i><span>{{__('backend.shipment_material')}}</span></a>
                                </li>
                            @endif

                            @if(permission('shipmentOrders')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li><a href="{!! URL::asset('mngrAdmin/shipment_tool_orders') !!}"><i
                                            class="fa fa-envelope"></i><span>{{__('backend.shipment_material_orders')}}</span></a>
                                </li>
                            @endif

                            @if(permission('shipmentDestinations')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li><a href="{!! URL::asset('mngrAdmin/governoratePrices') !!}"><i
                                            class="fa fa-arrow-right"></i><span>{{__('backend.shipment_destinations')}}</span></a>
                                </li>
                                @endif
                            </ul>
                        </div>
                    </li>
                @endif

                @if(permission('listReceivers'))
                    <li><a class="{{ Request::is('mngrAdmin/list_receivers*') ? 'active' : '' }}"
                           href="{!! url('mngrAdmin/list_receivers') !!}">
                            <i class="fa fa-mobile"></i> <span>{{__('backend.list_receivers')}}</span>
                        </a>
                    </li>
                @endif
                @if(permission('adminRoles') || permission('permissionTypes') || permission('permissions'))
                    <li>
                        <a href="#permissionPages" data-toggle="collapse"
                           class="{{ Request::is('mngrAdmin/admin_roles*') || Request::is('mngrAdmin/permissions*') || Request::is('mngrAdmin/type_permissions*')  ? 'active' : 'collapsed' }} "
                           aria-expanded="{{ Request::is('mngrAdmin/admin_roles*') || Request::is('mngrAdmin/permissions*') || Request::is('mngrAdmin/type_permissions*')  ? 'true' : 'false' }} ">

                            <i class="fa fa-gavel"></i> <span>{{__('backend.permissions')}}</span> <i
                                class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="permissionPages"
                             class="collapse {{ Request::is('mngrAdmin/admin_roles*') || Request::is('mngrAdmin/permissions*') || Request::is('mngrAdmin/type_permissions*')  ? 'in' : '' }}
                                 "
                             aria-expanded="{{ Request::is('mngrAdmin/admin_roles*') || Request::is('mngrAdmin/permissions*') || Request::is('mngrAdmin/type_permissions*')  ? 'true' : 'false' }}">
                            <ul class="nav">
                            @if(permission('permissions')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li><a href="{!! route('mngrAdmin.permissions.index') !!}"><i
                                            class="fa fa-lock"></i><span>{{__('backend.permissions')}}</span></a></li>
                            @endif

                            @if(permission('permissionTypes')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li><a href="{!! route('mngrAdmin.type_permissions.index') !!}"><i
                                            class="fa fa-lock"></i><span>{{__('backend.type_permission')}}</span></a>
                                </li>
                            @endif

                            @if(permission('adminRoles')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li><a href="{!! route('mngrAdmin.admin_roles.index') !!}"><i
                                            class="fa fa-users"></i><span>{{__('backend.roles')}}</span></a></li>
                                @endif
                            </ul>
                        </div>
                    </li>
                @endif

                @if(permission('activityLog')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <li>
                    <a href="#logPages" data-toggle="collapse"
                       class="{{ Request::is('mngrAdmin/activity_log*') || Request::is('mngrAdmin/activity_log_type*')   ? 'active' : 'collapsed' }} "
                       aria-expanded="{{  Request::is('mngrAdmin/activity_log*') || Request::is('mngrAdmin/activity_log_type*')  ? 'true' : 'false' }} ">
                        <i class="fa fa-tags"></i> <span>{{__('backend.activity_log')}}</span> <i
                            class="icon-submenu lnr lnr-chevron-left"></i></a>
                    <div id="logPages"
                         class="collapse {{  Request::is('mngrAdmin/activity_log*') || Request::is('mngrAdmin/activity_log_type*')  ? 'in' : '' }}
                             "
                         aria-expanded="{{  Request::is('mngrAdmin/activity_log*') || Request::is('mngrAdmin/activity_log_type*')  ? 'true' : 'false' }}">
                        <ul class="nav">
                            <li><a class="{{ Request::is('mngrAdmin/order_log*') ? 'active' : '' }}"
                                   href="{!! URL::asset('mngrAdmin/order_log') !!}">
                                    <i class="fa fa-history"></i> <span>{{__('backend.order_log')}}</span>
                                </a>
                            </li>
                            <li><a href="{!! route('mngrAdmin.activity_log.index') !!}"><i class="fa fa-tag"></i>
                                    <span>{{__('backend.activity_log')}}</span></a></li>
                            <li><a href="{!! route('mngrAdmin.activity_log_type.index') !!}"><i
                                        class="fa fa-tag"></i><span>{{__('backend.Activity_log_type')}}</span></a>
                            </li>
                        </ul>
                    </div>
                </li>
                @endif

                @if(permission('documentations') || permission('priceLists') || permission('cities') || permission('governorates')
 || permission('countries') || permission('paymentMethods') || permission('vehicleColors') || permission('vehicleTypes') || permission('vehicleModels') || permission('resources')
  || permission('orderTypes') || permission('androidVersions') || permission('promoCodes') || permission('stocks'))
                    <li>
                        <a href="#otherPage" data-toggle="collapse" class="{{Request::is('mngrAdmin/countries*') || Request::is('mngrAdmin/cities*') || Request::is('mngrAdmin/payment_methods*')
                  || Request::is('mngrAdmin/colors*')|| Request::is('mngrAdmin/weights*') || Request::is('mngrAdmin/types/*') ||
                   Request::is('mngrAdmin/sizes*') ||  Request::is('mngrAdmin/model_types*') || Request::is('mngrAdmin/governorates*')  ? 'active' : 'collapsed' }} "
                           aria-expanded="{{Request::is('mngrAdmin/countries*') || Request::is('mngrAdmin/cities*') || Request::is('mngrAdmin/payment_methods*')
                  || Request::is('mngrAdmin/colors*')|| Request::is('mngrAdmin/weights*') || Request::is('mngrAdmin/types/*') ||
                   Request::is('mngrAdmin/sizes*') ||  Request::is('mngrAdmin/model_types*') || Request::is('mngrAdmin/governorates*')  ? 'true' : 'false' }} ">

                            <i class="fa fa-medkit"></i> <span>{{__('backend.others')}}</span> <i
                                class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="otherPage" class="collapse {{Request::is('mngrAdmin/countries*') || Request::is('mngrAdmin/cities*') || Request::is('mngrAdmin/payment_methods*')
                  || Request::is('mngrAdmin/colors*')|| Request::is('mngrAdmin/weights*') || Request::is('mngrAdmin/types/*') ||
                   Request::is('mngrAdmin/sizes*') ||  Request::is('mngrAdmin/model_types*') || Request::is('mngrAdmin/governorates*')  ? 'in' : '' }}
                            " aria-expanded="{{Request::is('mngrAdmin/countries*') || Request::is('mngrAdmin/cities*') || Request::is('mngrAdmin/payment_methods*')
                  || Request::is('mngrAdmin/colors*')|| Request::is('mngrAdmin/weights*') || Request::is('mngrAdmin/types/*') ||
                   Request::is('mngrAdmin/sizes*') ||  Request::is('mngrAdmin/model_types*') || Request::is('mngrAdmin/governorates*')  ? 'true' : 'false' }}">
                            <ul class="nav">


                            @if(permission('stocks')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li style="margin-top: 3px;"><a
                                        class="{{ Request::is('mngrAdmin/stocks*') ? 'active' : '' }}"
                                        href="{!! route('mngrAdmin.stocks.index') !!}">
                                        <i class="fa fa-shopping-basket"></i> <span>{{__('backend.stocks')}}</span>
                                    </a>
                                </li>
                            @endif


                            @if(permission('promoCodes')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li style="margin-top: 3px;"><a
                                        class="{{ Request::is('mngrAdmin/promo_codes*') ? 'active' : '' }}"
                                        href="{!! route('mngrAdmin.promo_codes.index') !!}">
                                        <i class="fa fa-gift "></i> <span>{{__('backend.promo_codes')}}</span>
                                    </a>
                                </li>
                            @endif

                            @if(permission('androidVersions')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li style="margin-top: 3px;"><a href="{{ url('mngrAdmin/android')}}"><i
                                            class="fa fa-android"></i><span>{{__('backend.Android_Versions')}}</span></a>
                                </li>
                            @endif

                            @if(permission('orderTypes')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li><a href="{!! URL::asset('mngrAdmin/order_types') !!}"><i
                                            class="fa fa-list-alt"></i><span>{{__('backend.Order_Types')}}</span></a>
                                </li>
                            @endif

                            @if(permission('resources')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li><a href="{!! URL::asset('mngrAdmin/resources') !!}"><i
                                            class="fa fa-list-alt"></i><span>{{__('backend.resources')}}</span></a></li>
                            @endif

                            @if(permission('vehicleModels')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li><a href="{!! route('mngrAdmin.model_types.index') !!}"><i
                                            class="fa fa-magnet"></i><span>{{__('backend.Vehicles_Model')}}</span></a>
                                </li>
                            @endif

                            @if(permission('vehicleTypes')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li><a href="{{url('mngrAdmin/vehicle')}}"><i
                                            class="fa fa-motorcycle"></i><span> {{__('backend.Vehicle_Types')}}</span></a>
                                </li>
                            @endif

                            @if(permission('vehicleColors')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li><a href="{!! route('mngrAdmin.colors.index') !!}"><i
                                            class="fa fa-paint-brush"></i><span>{{__('backend.Vehicle_Colors')}}</span></a>
                                </li>
                            @endif

                            @if(permission('paymentMethods')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li><a href="{!! route('mngrAdmin.payment_methods.index') !!}"><i
                                            class="fa fa-credit-card"></i><span>{{__('backend.Payment_Methods')}}</span></a>
                                </li>
                            @endif

                            @if(permission('countries')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li><a href="{!! route('mngrAdmin.countries.index') !!}"><i
                                            class="fa fa-language"></i><span>{{__('backend.countries')}}</span></a></li>
                            @endif

                            @if(permission('governorates')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li><a href="{!! route('mngrAdmin.governorates.index') !!}"><i
                                            class="fa fa-language"></i><span>{{__('backend.governorates')}}</span></a>
                                </li>
                            @endif

                            @if(permission('cities')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li><a href="{!! route('mngrAdmin.cities.index') !!}"><i
                                            class="fa fa-language"></i><span>{{__('backend.cities')}}</span></a></li>
                            @endif

                            @if(permission('priceLists')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li><a href="{!! route('mngrAdmin.price_lists.index') !!}"><i
                                            class="fa fa-money"></i><span>{{__('backend.Pricing_List')}}</span></a></li>
                                @endif

                                <li style="margin-top: 13px;"><a
                                        href="{!! route('mngrAdmin.CorporateTarget.index') !!}"><i
                                            class="fa fa-money"></i><span>{{__('backend.corporate_target')}}</span></a>
                                </li>

                                @if(permission('documentations')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li><a class="{{ Request::is('mngrAdmin/docs*') ? 'active' : '' }}"
                                       href="{!! URL::asset('mngrAdmin/docs') !!}">
                                        <i class="fa fa-book"></i> <span>{{__('backend.documentations')}}</span>
                                    </a>
                                </li>
                                @endif
                            </ul>
                        </div>
                    </li>
                @endif

                @if(permission('reports')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <li><a class="{{ Request::is('mngrAdmin/reports*') ? 'active' : '' }}"
                       href="{!! URL::asset('mngrAdmin/reports') !!}">
                        <i class="fa fa-file-excel-o"></i> <span>{{__('backend.reports')}}</span>
                    </a>
                </li>

                <li><a class="{{ Request::is('mngrAdmin/corporate-sheets*') ? 'active' : '' }}"
                       href="{!! URL::asset('mngrAdmin/corporate-sheets') !!}">
                        <i class="fa fa-file-excel-o"></i> <span>{{__('backend.corporates_excel')}}</span>
                    </a>
                </li>
                <li><a class="{{ Request::is('mngrAdmin/driver-sheets*') ? 'active' : '' }}"
                       href="{!! URL::asset('mngrAdmin/driver-sheets') !!}">
                        <i class="fa fa-file-excel-o"></i> <span>{{__('backend.drivers_excel')}}</span>
                    </a>
                </li>
                <li><a class="{{ Request::is('mngrAdmin/driver-sheets/daily-report*') ? 'active' : '' }}"
                       href="{!! URL::asset('mngrAdmin/driver-sheets/daily-report') !!}">
                        <i class="fa fa-file-excel-o"></i> <span>{{__('backend.drivers_daily_report')}}</span>
                    </a>
                </li>
                @endif

                @if(permission('contacts')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <li><a class="{{ Request::is('mngrAdmin/contacts*') ? 'active' : '' }}"
                       href="{!! route('mngrAdmin.contacts.index') !!}">
                        <i class="fa fa-envelope"></i> <span>{{__('backend.Contact_Us')}}</span>
                    </a>
                </li>
                @endif

                @if(permission('services'))
                    <li>
                        <a href="#websitePages" data-toggle="collapse"
                           class="{{ Request::is('mngrAdmin/services*')  ? 'active' : 'collapsed' }} "
                           aria-expanded="{{ Request::is('mngrAdmin/services*') ? 'true' : 'false' }} ">
                            <i class="fa fa-asterisk"></i> <span>{{__('backend.website')}}</span> <i
                                class="icon-submenu lnr lnr-chevron-left"></i></a>

                        <div id="websitePages" class="collapse {{  Request::is('mngrAdmin/services*')  ? 'in' : '' }}
                            " aria-expanded="{{  Request::is('mngrAdmin/services*')  ? 'true' : 'false' }}">
                            <ul class="nav">
                            @if(permission('services')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li><a class="{{ Request::is('mngrAdmin/services?type=1') ? 'active' : '' }}"
                                       href="{!! url('/mngrAdmin/services?type=1') !!}">
                                        <i class="fa fa-glass"></i> <span>{{__('backend.services')}}</span>
                                    </a>
                                </li>

                                <li><a class="{{ Request::is('mngrAdmin/services?type=2') ? 'active' : '' }}"
                                       href="{!! url('/mngrAdmin/services?type=2') !!}">
                                        <i class="fa fa-cloud"></i> <span>{{__('backend.why_us')}}</span>
                                    </a>
                                </li>

                                <li><a class="{{ Request::is('mngrAdmin/services?type=3') ? 'active' : '' }}"
                                       href="{!! url('/mngrAdmin/services?type=3') !!}">
                                        <i class="fa fa-cloud"></i> <span>{{__('backend.slider')}}</span>
                                    </a>
                                </li>

                                <li><a class="{{ Request::is('mngrAdmin/services?type=4') ? 'active' : '' }}"
                                       href="{!! url('/mngrAdmin/services?type=4') !!}">
                                        <i class="fa fa-user"></i> <span>{{__('backend.drivers')}}</span>
                                    </a>
                                </li>

                                <li><a class="{{ Request::is('mngrAdmin/services?type=5') ? 'active' : '' }}"
                                       href="{!! url('/mngrAdmin/services?type=5') !!}">
                                        <i class="fa fa-building"></i> <span>{{__('backend.customers')}}</span>
                                    </a>
                                </li>
                                @endif

                            </ul>
                        </div>
                    </li>
                @endif

                @if(permission('admins') || permission('editAboutUs') || permission('employees'))
                    <li>
                        <a href="#compPages" data-toggle="collapse"
                           class="{{ Request::is('mngrAdmin/employees*') || Request::is('mngrAdmin/admins*') || Request::is('mngrAdmin/settings*')  ? 'active' : 'collapsed' }} "
                           aria-expanded="{{ Request::is('mngrAdmin/employees*') || Request::is('mngrAdmin/admins*') || Request::is('mngrAdmin/settings*')  ? 'true' : 'false' }} ">
                            <i class="lnr lnr-users"></i> <span>{{__('backend.company')}}</span> <i
                                class="icon-submenu lnr lnr-chevron-left"></i></a>

                        <div id="compPages"
                             class="collapse {{  Request::is('mngrAdmin/employees*') || Request::is('mngrAdmin/admins*') || Request::is('mngrAdmin/settings*')  ? 'in' : '' }}
                                 "
                             aria-expanded="{{ Request::is('mngrAdmin/settings*') || Request::is('mngrAdmin/admins*') || Request::is('mngrAdmin/employees*')  ? 'true' : 'false' }}">
                            <ul class="nav">
                            @if(permission('admins')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li><a class="{{ Request::is('mngrAdmin/admins*') ? 'active' : '' }}"
                                       href="{!! route('mngrAdmin.admins.index') !!}">
                                        <i class="fa fa-user"></i> <span>{{__('backend.admins')}}</span>
                                    </a>
                                </li>
                            @endif
                            @if(permission('editAboutUs')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li><a class="{{ Request::is('mngrAdmin/settings*') ? 'active' : '' }}"
                                       href="{!! url('mngrAdmin/settings') !!}">
                                        <i class="lnr lnr-cog"></i> <span>{{__('backend.settings')}}</span>
                                    </a>
                                </li>
                            @endif

                            @if(permission('employees')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <li><a class="{{ Request::is('mngrAdmin/employees*') ? 'active' : '' }}"
                                       href="{!! route('mngrAdmin.employees.index') !!}">
                                        <i class="fa fa-user-secret "></i> <span>{{__('backend.employees')}}</span>
                                    </a>
                                </li>
                                @endif

                            </ul>
                        </div>
                    </li>
                @endif

            </ul>
        </nav>
    </div>
</div>

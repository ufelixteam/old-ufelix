@extends('backend.layouts.app')

@section('css')
    <title>{{__('backend.stocks')}} - {{__('backend.create')}}</title>
    <style>
        .form-group {
            min-height: 94px;
            margin-bottom: 0px;
        }

    </style>
@endsection

@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-plus"></i> {{__('backend.add_stock')}} </h3>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('mngrAdmin.stocks.store') }}" method="POST" name="validateForm">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="row">
                    <div class="form-group col-md-6 @if($errors->has('title')) has-error @endif">
                        <label for="name-field">{{__('backend.title')}}</label>
                        <input type="text" id="title" name="title" class="form-control" value="{{ old("title") }}"/>
                        @if($errors->has("title"))
                            <span class="help-block">{{ $errors->first("title") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-6 @if($errors->has('dropped_by')) has-error @endif">
                        <label for="name-field">{{__('backend.dropped_by')}}</label>
                        <input type="text" id="dropped_by" name="dropped_by" class="form-control"
                               value="{{ old("dropped_by") }}"/>
                        @if($errors->has("dropped_by"))
                            <span class="help-block">{{ $errors->first("dropped_by") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-6 @if($errors->has('qty')) has-error @endif">
                        <label for="name-field">{{__('backend.quantity')}}</label>
                        <input type="number" id="qty" name="qty" class="form-control" value="{{ old("qty") }}"/>
                        @if($errors->has("qty"))
                            <span class="help-block">{{ $errors->first("qty") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-6 @if($errors->has('store_id')) has-error @endif">
                        <label for="governorate_id-field">{{__('backend.store')}}</label>
                        <select class="form-control" id="store_id" name="store_id">
                            <option value="" data-display="select">{{__('backend.Choose_Store')}}</option>
                            @if(! empty($stores))
                                @foreach($stores as $store)
                                    <option value="{{$store->id}}"
                                        {{$warehouse && $store->id == $warehouse->id ?'selected': ($store->id == old("store_id") ? 'selected' : '')}} >
                                        {{$store->name}}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                        @if($errors->has("store_id"))
                            <span class="help-block">{{ $errors->first("store_id") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-6 @if($errors->has('corporate_id')) has-error @endif">
                        <label for="governorate_id-field">{{__('backend.corporate')}}</label>
                        <select class="form-control" id="corporate_id" name="corporate_id">
                            <option value="" data-display="Select">{{__('backend.choose_corporate')}}</option>
                            @if(! empty($corporates))
                                @foreach($corporates as $corporate)
                                    <option
                                        value="{{$corporate->id}}" {{$corporate->id == old("corporate_id") ? 'selected' : ''}} >
                                        {{$corporate->name}}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                        @if($errors->has("corporate_id"))
                            <span class="help-block">{{ $errors->first("corporate_id") }}</span>
                        @endif
                    </div>

                    {{--                <div class="row">--}}
                    {{--                    <div class="form-group col-md-6 @if($errors->has('used_qty')) has-error @endif">--}}
                    {{--                        <label for="name-field">{{__('backend.used_quantity')}}</label>--}}
                    {{--                        <input type="number" id="used_qty" name="used_qty" class="form-control" value="{{ old("used_qty") }}"/>--}}
                    {{--                        @if($errors->has("used_qty"))--}}
                    {{--                            <span class="help-block">{{ $errors->first("used_qty") }}</span>--}}
                    {{--                        @endif--}}
                    {{--                    </div>--}}
                    {{--                </div>--}}

                    <div class="form-group col-md-6 @if($errors->has('description')) has-error @endif">
                        <label for="address">{{__('backend.description')}}: </label>
                        <textarea type="text" id="description" name="description" rows="4"
                                  class="form-control">{{old("description")}}</textarea>
                        @if($errors->has("description"))
                            <span class="help-block">{{ $errors->first("description") }}</span>
                        @endif
                    </div>
                </div>
                <div class="row well well-sm col-md-12" style="margin-top: 20px">
                    <button type="submit" class="btn btn-primary">{{__('backend.create')}}</button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.stocks.index') }}"><i
                            class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
                </div>

            </form>
        </div>
    </div>
@endsection

@section('scripts')

    <script type="text/javascript">
        $(function () {
            $("form[name='validateForm']").validate({
                // Specify validation rules
                rules: {
                    title: {
                        required: true,
                    },
                    qty: {
                        required: true,
                    },
                    store_id: {
                        required: true,
                    },
                    corporate_id: {
                        required: true,
                    },
                    description: {
                        required: true,
                    },
                },
                // Specify validation error messages
                messages: {
                    title: {
                        required: "{{__('backend.Please_Enter_Stock_Name')}}",
                    },
                    qty: {
                        required: "{{__('backend.Please_Enter_Quantity')}}",
                    },
                    store_id: {
                        required: "{{__('backend.Please_Chosse_The_Main_Store')}}",
                    },
                    corporate_id: {
                        required: "{{__('backend.Please_Chosse_The_Corporate')}}",
                    },
                    description: {
                        required: "{{__('backend.Please_Enter_The_Description')}}",
                    },
                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                },
                highlight: function (element) {
                    $(element).parent().addClass('error')
                },
                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                }
            });
        })
    </script>

@endsection

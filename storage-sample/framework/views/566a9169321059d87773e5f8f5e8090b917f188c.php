<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        <?php if($collection_orders->count()): ?>
            <table class="table table-condensed table-striped ">
                <thead>
                <tr>
                    <th>#</th>
                    <th><?php echo e(__('backend.Pickup_Numper')); ?></th>
                    <th><?php echo e(__('backend.Driver')); ?></th>
                    <th><?php echo e(__('backend.Agent')); ?></th>


                    <th><?php echo e(__('backend.Create_at')); ?></th>
                    <th><?php echo e(__('backend.Status')); ?></th>
                    <th>
                        <a class="btn btn-xs btn btn btn-success" id="drop_off">
                            <i class="fa fa-dropbox"></i> <?php echo e(__('backend.drop_off')); ?>


                        </a>
                    </th>
                    <th></th>

                </tr>
                </thead>
                <tbody>

                <?php $__currentLoopData = $collection_orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $collection): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($collection->id); ?></td>
                        <td><?php echo e($collection->pickup_number); ?></td>

                        <td><?php echo e(! empty($collection->driver) ? $collection->driver->name : ''); ?></td>
                        <td><?php echo e(! empty($collection->agent) ? $collection->agent->name : ''); ?></td>
                        <td><?php echo e($collection->created_at); ?></td>

                        <td><?php echo $collection->status_span; ?></td>
                        <td class="selectpackup" id="selectpackup">
                            <input type="checkbox" class="selectpack" name="selectpackup[]"
                                   value="<?php echo e($collection->id); ?>" <?php if($collection->status == 3): ?> disabled <?php endif; ?>/>
                        </td>
                        <td class="pull-right">
                            <a class="btn btn-xs btn-primary"
                               href="<?php echo e(route('mngrAdmin.pickups.show', $collection->id)); ?>"><i
                                    class="glyphicon glyphicon-eye-open"></i> <?php echo e(__('backend.view')); ?> </a>
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
            <?php echo $collection_orders->appends($_GET)->links(); ?>

        <?php else: ?>
            <h3 class="text-center alert alert-warning">No Result Found !</h3>
        <?php endif; ?>
    </div>
</div>
<?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/pickups/table.blade.php ENDPATH**/ ?>
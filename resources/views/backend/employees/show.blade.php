@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.employees')}} - {{__('backend.view')}}</title>
@endsection

@section('header')
<div class="page-header">
        <h3>{{__('backend.show_employee')}} - {{$employee->name}}</h3>
        <form action="{{ route('mngrAdmin.employees.destroy', $employee->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
              @if(permission('editEmployee')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <a class="btn btn-warning btn-group" role="group" href="{{ route('mngrAdmin.employees.edit', $employee->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
              @endif
              @if(permission('deleteEmployee')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <button type="submit" class="btn btn-danger">{{__('backend.delete')}} <i class="glyphicon glyphicon-trash"></i></button>
              @endif
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="form-group col-md-6 col-sm-6">
                <img src="{{ $employee->image }}" width="60%" />
            </div>
            <div class="form-group col-md-6 col-sm-6">
                 <label for="name">{{__('backend.name')}}</label>
                 <p class="form-control-static">{{$employee->name}}</p>
            </div>
            <div class="form-group col-md-6 col-sm-6">
                 <label for="email">{{__('backend.email')}}</label>
                 <p class="form-control-static">{{$employee->email}}</p>
            </div>
            <div class="form-group col-md-6 col-sm-6">
                 <label for="mobile">{{__('backend.mobile')}}</label>
                 <p class="form-control-static">{{$employee->mobile}}</p>
            </div>

            <div class="form-group col-md-6 col-sm-6">
                 <label for="salary">{{__('backend.salary')}}</label>
                 <p class="form-control-static">{{$employee->salary}}</p>
            </div>
            <div class="form-group col-md-6 col-sm-6">
                 <label for="job_title">{{__('backend.job_title')}}</label>
                 <p class="form-control-static">{{$employee->job_title}}</p>
            </div>
            <div class="form-group col-md-6 col-sm-6">
                 <label for="bio">{{__('backend.bio')}}</label>
                 <p class="form-control-static">{{$employee->bio}}</p>
            </div>
        </div>
        <div class="col-md-12">
            <a class="btn btn-link" href="{{ route('mngrAdmin.employees.index') }}"><i class="glyphicon glyphicon-backward"></i>  {{__('backend.back')}}</a>
        </div>
    </div>

@endsection

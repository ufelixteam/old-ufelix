@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.corporate')}} - {{__('backend.view')}}</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <style>
        .toggle.ios, .toggle-on.ios, .toggle-off.ios {
            border-radius: 20px;
        }

        .toggle.ios .toggle-handle {
            border-radius: 20px;
        }

        .img {
            /*border: 1px solid #8e7ad4;*/
            height: 100px;
            width: 100px;
            border-radius: 50%;
            /*padding: 10px;*/
        }
    </style>

@endsection
@section('header')


    <div class="page-header">
        <h3>{{__('backend.view')}} {{__('backend.corporate')}}
            - {{$corprate->name}} </h3> {!! $corprate->active_span !!} {{$corprate->created_at}}
        <form action="{{ route('mngrAdmin.corporates.destroy', $corprate->id) }}" method="POST" style="display: inline;"
              onsubmit="if(confirm('{{__('Backend.msg_confirm_delete')}}')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
            @if(permission('activeCorporate')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <button id="active" type="button" value="{{ $corprate->id }}"
                        class="btn btn-info">{{ $corprate->active_txt }}</button>
            @endif

            @if(permission('editCorporate')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <a class="btn btn-warning btn-group" role="group"
                   href="{{ route('mngrAdmin.corporates.edit', $corprate->id) }}"><i
                        class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
            @endif

            @if(permission('deleteCorporate')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <button type="submit" class="btn btn-danger"><i
                        class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}</button>
            @endif

            @if(permission('showOrdersMember')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <a class="btn btn-default btn-group"
                   href="{{url('mngrAdmin/orders/showOrders') . '/2' . '/' . $corprate->id}}"><i
                        class="glyphicon glyphicon-list"></i> {{__('backend.the_orders')}}</a>
            @endif

            @if(permission('reports')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <a class="btn btn-success btn-group"
                   href="{{url('mngrAdmin/reports/daily_report') . '?corporate_id=' . $corprate->id}}"
                   target="_blank"><i
                        class="fa fa-file-excel-o"></i> {{__('backend.daily_report')}}</a>
                @endif
            </div>
        </form>
    </div>


@endsection
@section('content')
    <div class="col-sm-12"
         style="border: 2px solid #fdb801;color: #fff;margin-bottom: 20px;margin-top: 20px;background-color: #fdb801;">
        <h5>{{__('backend.company_information')}}</h5>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="form-group col-sm-3">
                <label for="name">{{__('backend.email')}}</label>
                <p class="form-control-static">{{$corprate->email}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="company_website-field">{{__('backend.responsible')}}: </label>
                <p class="form-control-static">{{!empty($corprate->responsible_user->name) ? $corprate->responsible_user->name : '-'}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="name">{{__('backend.mobile')}}</label>
                <p class="form-control-static">{{isset($corprate->mobile) ? $corprate->mobile : __("backend.not_found")}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="name">{{__('backend.mobile_number2')}}</label>
                <p class="form-control-static"> {{isset($corprate->phone) ? $corprate->phone : __("backend.not_found")}}</p>
            </div>

            {{--            <div class="form-group col-sm-4">--}}
            {{--                <label for="mobile">{{__('backend.field')}}</label>--}}
            {{--                <p class="form-control-static">{{$corprate->field}}</p>--}}
            {{--            </div>--}}

            <div class="form-group col-sm-3">
                <label for="mobile">{{__('backend.order_type')}}</label>
                <p class="form-control-static">{{isset($corprate->type->name_en) ? $corprate->type->name_en : '-'}}</p>
            </div>

            <div class="form-group col-sm-3">
                <label for="mobile">{{__('backend.shipping_type')}}</label>
                <p class="form-control-static">
                    @if($corprate->shipping_type=1)
                        {{__('backend.n_shipping')}}
                    @elseif($corprate->shipping_type=2)
                        {{__('backend.rt_shipping')}}
                    @else
                        -
                    @endif
                </p>
            </div>

            <div class="form-group col-sm-3">
                <label for="phone">{{__('backend.commercial_record_number')}}</label>
                <p class="form-control-static">{{$corprate->commercial_record_number}}</p>
            </div>


            <div class="form-group col-sm-3">
                <label for="phone">{{__('backend.Officail_site')}}</label>
                <p class="form-control-static">{{$corprate->company_website}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="phone">{{__('backend.government')}}</label>
                <p class="form-control-static">
                    @if(\App\Models\Governorate::where('id', $corprate->government_id))
                        {{\App\Models\Governorate::where('id', $corprate->government_id)->value('name_en')}}
                    @else
                        {{__('backend.not_found')}}
                    @endif
                </p>
            </div>
            <div class="form-group col-sm-3">
                <label for="phone">{{__('backend.city')}}</label>
                <p class="form-control-static">
                    @if(\App\Models\City::where('id', $corprate->government_id))
                        {{\App\Models\City::where('id', $corprate->city_id)->value('name_en')}}
                    @else
                        {{__('backend.not_found')}}
                    @endif
                </p>
            </div>

            <div class="form-group col-sm-3">
                <label for="phone">{{__('backend.account_type')}}</label>
                <p class="form-control-static">
                    {{$corprate->type_of_account}}
                </p>
            </div>

            <div class="form-group col-sm-3">
                <label for="store">{{__('backend.store')}}</label>
                <p class="form-control-static">
                    {{!empty($corprate->store) ? $corprate->store->name : ''}}
                </p>
            </div>

            <div class="form-group col-sm-12">
                <label for="notes">{{__('backend.ContractNote')}}</label>
                <p class="form-control-static">{{$corprate->notes}}</p>
            </div>


            <div class="form-group col-sm-6">
                <label for="logo">{{__('backend.logo')}}</label>
                <p class="form-control-static">
                    <img src="{{ $corprate->logo }}" onerror="this.src='{{asset('assets/images/image.png')}}'"
                         class="img">
                </p>
            </div>


            <div class="form-group col-sm-6">
                <label for="commercial_record_image">{{__('backend.ContractForm')}}</label>
                <p class="form-control-static">
                    @php
                        $myString = $corprate->commercial_record_image;
                        $myStringParts = explode('.', $corprate->commercial_record_image);
                    @endphp
                    @if(substr($myString, -4) == '.pdf')

                        <embed src="{{ $corprate->commercial_record_image }}" width="100%" height="300px"
                               onerror="this.src='{{asset('assets/images/image.png')}}'">
                    @else
                        <img src="{{ $corprate->commercial_record_image }}"
                             class="img" onerror="this.src='{{asset('assets/images/image.png')}}'">

                    @endif
                </p>
            </div>
        </div>

    </div>

    @if($corprate->customers)
        <div class="col-sm-12"
             style="border: 2px solid #fdb801;color: #fff;margin-bottom: 20px;margin-top: 20px;background-color: #fdb801;">
            <h5>
                {{__('backend.orders')}}
            </h5>

        </div>
        @if($corprate->customers)
            <div class="row">

                <div class="form-group col-sm-12">
                    <table class="table table-bordered">
                        <tr>
                            <th>{{__('backend.customer')}}</th>
                            <th>{{__('backend.pending')}}</th>
                            <th>{{__('backend.dropped')}}</th>
                            <th>{{__('backend.accepted')}}</th>
                            <th>{{__('backend.received')}}</th>
                            <th>{{__('backend.delivered')}}</th>
                            <th>{{__('backend.recalled')}}</th>
                            <th>{{__('backend.rejected')}}</th>
                            <th>{{__('backend.cancelled')}}</th>
                        </tr>
                        @foreach($corprate->customers as $customer)
                            <tr>
                                <td><h5><strong style="color: #f55f;">{{$customer->name}}</strong></h5></td>
                                <td>{{$customer->pending_orders_count }}</td>
                                <td>{{$customer->dropped_orders_count }}</td>
                                <td>{{$customer->accepted_orders_count }}</td>
                                <td>{{$customer->received_orders_count }}</td>
                                <td>{{$customer->delivered_orders_count }}</td>
                                <td>{{$customer->recalled_orders_count }}</td>
                                <td>{{$customer->rejected_orders_count }}</td>
                                <td>{{$customer->cancelled_orders_count }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        @endif

        @if($corprate->customers)
            <div class="col-sm-12"
                 style="border: 2px solid #fdb801;color: #fff;margin-bottom: 20px;margin-top: 20px;background-color: #fdb801;">
                <h5>
                    {{__('backend.users_information')}}
                    @if(permission('addCustomer'))
                        <a class="btn btn-primary pull-right" style="margin-top: -10px;"
                           href="{{ route('mngrAdmin.customers.create', ['corporate_id' => $corprate->id]) }}"><i
                                class="glyphicon glyphicon-plus"></i> {{__('backend.add_user')}} </a>
                    @endif
                </h5>

            </div>
            <div class="row">

                <div class="form-group col-sm-12">
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>{{__('backend.name')}}</th>
                            <th>{{__('backend.email')}}</th>
                            <th>{{__('backend.mobile')}}</th>
                            <th>{{__('backend.block')}}</th>
                            <th>{{__('backend.verify_email')}}</th>
                            <th>{{__('backend.verfiy_mobile')}}</th>
                            <th></th>
                        </tr>

                        @foreach($corprate->customers as $value)
                            <tr class='clickable-row' data-href="{{ route('mngrAdmin.customers.show', $value->id) }}"
                                @if($value->is_something_wrong) style='background: #9e8f9e; color: #fff' @endif>
                                <td>{{$value->id }}</td>
                                <td>{{ ! empty($value->name)?$value->name:''}}</td>
                                <td>{{ ! empty($value->email)?$value->email:''}}</td>
                                <td>{{ ! empty($value->mobile)?$value->mobile:''}}</td>

                                <td class="exclude-td"><input data-id="{{$value->id}}" data-size="mini"
                                                              class="toggle change_block"
                                                              {{$value->is_block == 1 ? 'checked' : ''}} data-onstyle="danger"
                                                              type="checkbox"
                                                              data-style="ios" data-on="Yes" data-off="No"></td>

                                <td class="exclude-td"><input data-id="{{$value->id}}" data-size="mini"
                                                              class="toggle verify_email"
                                                              {{$value->is_verify_email == 1 ? 'checked' : ''}} data-onstyle="success"
                                                              type="checkbox" data-style="ios" data-on="Yes"
                                                              data-off="No">
                                </td>
                                <td class="exclude-td"><input data-id="{{$value->id}}" data-size="mini"
                                                              class="toggle verify_mobile"
                                                              {{$value->is_verify_mobile == 1 ? 'checked' : ''}} data-onstyle="success"
                                                              type="checkbox" data-style="ios" data-on="Yes"
                                                              data-off="No">
                                </td>

                                <td class="exclude-td">
                                    {{--                                <a class="btn btn-xs btn-primary"--}}
                                    {{--                                   href="{{ route('mngrAdmin.customers.show', $value->id) }}"><i--}}
                                    {{--                                        class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>--}}
                                    <a class="btn btn-xs btn-warning"
                                       href="{{ route('mngrAdmin.customers.edit', $value->id) }}"><i
                                            class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>

                                    @if(permission('showOrdersMember')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                    <a class="btn btn-xs btn-default"
                                       href="{{url('mngrAdmin/orders/showOrders') . '/4' . '/' . $value->id}}"><i
                                            class="glyphicon glyphicon-list"></i> {{__('backend.the_orders')}}</a>
                                    @endif

                                    @if(permission('reports')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                    <a class="btn btn-xs btn-success"
                                       href="{{url('mngrAdmin/reports/daily_report') . '?customer_id=' . $value->id}}"
                                       target="_blank"><i
                                            class="fa fa-file-excel-o"></i> {{__('backend.daily_report')}}</a>
                                    @endif
                                </td>

                            </tr>



                        @endforeach


                    </table>
                </div>
            </div>
        @endif
    @endif

        {{--    <div class="col-sm-12"--}}
        {{--         style="border: 2px solid #fdb801;color: #fff;margin-bottom: 20px;margin-top: 20px;background-color: #fdb801;">--}}
        {{--        <h5>--}}
        {{--            {{__('backend.corporate_prices')}}--}}
        {{--            @if(permission('addShipmentDestinations'))--}}
        {{--                <a class="btn btn-primary pull-right" style="margin-top: -10px;"--}}
        {{--                   href="{{ route('mngrAdmin.corporatePrices.create', ['corporate_id' => $corprate->id]) }}"><i--}}
        {{--                        class="glyphicon glyphicon-plus"></i> {{__('backend.add_price')}} </a>--}}

        {{--                <a class="btn btn-primary pull-right" style="margin: -10px 5px 0 5px;"--}}
        {{--                   href="{{ route('mngrAdmin.corporatePrices.generate', ['corporate_id' => $corprate->id]) }}"><i--}}
        {{--                        class="glyphicon glyphicon-refresh"></i> {{__('backend.generate_prices')}} </a>--}}

        {{--            @endif--}}
        {{--        </h5>--}}

        {{--    </div>--}}
        {{--    <div class="row">--}}

        {{--        <div class="form-group col-sm-12">--}}
        {{--            @if($corprate->prices->count())--}}
        {{--                <table class="table table-condensed table-striped text-center">--}}
        {{--                    <thead>--}}
        {{--                    <tr>--}}
        {{--                        <th>#</th>--}}
        {{--                        <th>{{__('backend.from')}}</th>--}}
        {{--                        <th></th>--}}
        {{--                        <th>{{__('backend.to')}}</th>--}}
        {{--                        <th>{{__('backend.cost')}}</th>--}}
        {{--                        <th>{{__('backend.recall_cost')}}</th>--}}
        {{--                        <th>{{__('backend.reject_cost')}}</th>--}}
        {{--                        <th>{{__('backend.cancel_cost')}}</th>--}}
        {{--                        <th>{{__('backend.status')}}</th>--}}
        {{--                        <th class=""></th>--}}
        {{--                    </tr>--}}
        {{--                    </thead>--}}
        {{--                    <tbody>--}}
        {{--                    @foreach($corprate->prices as $corporatePrice)--}}
        {{--                        <tr>--}}
        {{--                            <td>{{$corporatePrice->id}}</td>--}}
        {{--                            <td>{{$corporatePrice->Start->name_en}}</td>--}}
        {{--                            <td><i class="glyphicon glyphicon-arrow-right" style="color: blue;"></i></td>--}}
        {{--                            <td>{{$corporatePrice->End->name_en}}</td>--}}
        {{--                            <td>--}}
        {{--                                <input type="number" class="form-control cost" name="cost"--}}
        {{--                                       value="{{$corporatePrice->cost}}" id="cost_{{$corporatePrice->id}}"--}}
        {{--                                       data-id="{{$corporatePrice->id}}">--}}
        {{--                            </td>--}}
        {{--                            <td>--}}
        {{--                                <input type="number" class="form-control recall_cost" name="recall_cost"--}}
        {{--                                       value="{{$corporatePrice->recall_cost}}" id="recall_cost_{{$corporatePrice->id}}"--}}
        {{--                                       data-id="{{$corporatePrice->id}}">--}}
        {{--                            </td>--}}
        {{--                            <td>--}}
        {{--                                <input type="number" class="form-control reject_cost" name="reject_cost"--}}
        {{--                                       value="{{$corporatePrice->reject_cost}}" id="reject_cost_{{$corporatePrice->id}}"--}}
        {{--                                       data-id="{{$corporatePrice->id}}">--}}
        {{--                            </td>--}}
        {{--                            <td>--}}
        {{--                                <input type="number" class="form-control cancel_cost" name="cancel_cost"--}}
        {{--                                       value="{{$corporatePrice->cancel_cost}}" id="cancel_cost_{{$corporatePrice->id}}"--}}
        {{--                                       data-id="{{$corporatePrice->id}}">--}}
        {{--                            </td>--}}
        {{--                            <td>{!! $corporatePrice->status_span !!}</td>--}}
        {{--                            <td class="">--}}
        {{--                            @if(permission('editShipmentDestinations')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->--}}

        {{--                                <a class="btn btn-xs btn-success save_cost disabled"--}}
        {{--                                   href="#" data-id="{{$corporatePrice->id}}" id="save_cost_{{$corporatePrice->id}}"><i--}}
        {{--                                        class="glyphicon glyphicon-edit"></i> {{__('backend.save')}}</a>--}}

        {{--                                <a class="btn btn-xs btn-warning"--}}
        {{--                                   href="{{ route('mngrAdmin.corporatePrices.edit', $corporatePrice->id)}}"><i--}}
        {{--                                        class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>--}}

        {{--                            @endif--}}
        {{--                            @if(permission('deleteShipmentDestinations')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->--}}
        {{--                                <form action="{{ route('mngrAdmin.corporatePrices.destroy', $corporatePrice->id) }}"--}}
        {{--                                      method="POST" style="display: inline;"--}}
        {{--                                      onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">--}}
        {{--                                    <input type="hidden" name="_method" value="DELETE">--}}
        {{--                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
        {{--                                    <button type="submit" class="btn btn-xs btn-danger"><i--}}
        {{--                                            class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}</button>--}}
        {{--                                </form>--}}
        {{--                                @endif--}}
        {{--                            </td>--}}
        {{--                        </tr>--}}
        {{--                    @endforeach--}}
        {{--                    </tbody>--}}
        {{--                </table>--}}

        {{--                --}}{{--                {!! $corporatePrices->appends($_GET)->links() !!}--}}

        {{--            @else--}}
        {{--                <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>--}}
        {{--            @endif--}}
        {{--        </div>--}}
        {{--    </div>--}}

        {{--    <div class="col-sm-12"--}}
        {{--         style="border: 2px solid #fdb801;color: #fff;margin-bottom: 20px;margin-top: 20px;background-color: #fdb801;">--}}
        {{--        <h5>{{__('backend.corporate_target')}}</h5>--}}
        {{--    </div>--}}


        {{--    <div class="row">--}}
        {{--        <div class="form-group col-sm-12">--}}
        {{--            @if(count($c_targets) > 0)--}}
        {{--                <table class="table">--}}
        {{--                    <tr>--}}
        {{--                        <td></td>--}}
        {{--                        <th>#{{__('backend.id')}}</th>--}}
        {{--                        <th>{{__('backend.target_from')}}</th>--}}
        {{--                        <th>{{__('backend.target_to')}}</th>--}}
        {{--                        <th>{{__('backend.discount_percentage')}}</th>--}}
        {{--                        <th></th>--}}
        {{--                    </tr>--}}
        {{--                    @foreach($c_targets as $target)--}}
        {{--                        @if($corprate->corporate_target_id == $target->id)--}}
        {{--                            <tr style="background-color: #ccc; font-weight: bold; color: #000">--}}
        {{--                                <td><input type="checkbox" checked disabled></td>--}}
        {{--                        @else--}}
        {{--                            <tr>--}}
        {{--                                <td><input type="checkbox" disabled></td>--}}
        {{--                                @endif--}}

        {{--                                <td>{{$target->id }}</td>--}}
        {{--                                <td>{{$target->corporate_target_from }}</td>--}}
        {{--                                <td>{{$target->corporate_target_to }}</td>--}}
        {{--                                <td>{{$target->corporate_discount }} %</td>--}}
        {{--                                <td>--}}
        {{--                                    <a class="btn btn-xs btn-info" data-toggle="modal"--}}
        {{--                                       data-target="#editTarget_{{$target->id}}"><i--}}
        {{--                                            class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>--}}
        {{--                                    <form action="{{ route('mngrAdmin.CorporateTarget.destroy', $target->id) }}"--}}
        {{--                                          method="POST" style="display: inline;"--}}
        {{--                                          onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">--}}
        {{--                                        <input type="hidden" name="_method" value="DELETE">--}}
        {{--                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
        {{--                                        <button type="submit" class="btn btn-xs btn-danger"><i--}}
        {{--                                                class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}</button>--}}
        {{--                                    </form>--}}
        {{--                                </td>--}}
        {{--                            </tr>--}}

        {{--                            <!-- Model Add Target For Corporate -->--}}
        {{--                            <div class="row">--}}
        {{--                                <!-- Modal -->--}}
        {{--                                <div class="modal fade" id="editTarget_{{$target->id}}" role="dialog">--}}
        {{--                                    <div class="modal-dialog" style="width: 80%;">--}}

        {{--                                        <!-- Modal content-->--}}
        {{--                                        <div class="modal-content">--}}
        {{--                                            <div class="modal-header">--}}
        {{--                                                <button type="button" class="close" data-dismiss="modal">&times;--}}
        {{--                                                </button>--}}
        {{--                                                <h4 class="modal-title text-center"> {{__('backend.edit_target')}}</h4>--}}
        {{--                                            </div>--}}

        {{--                                            <form action="{{ route('mngrAdmin.CorporateTarget.update', $target->id) }}"--}}
        {{--                                                  method="POST" name="validateForm">--}}
        {{--                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
        {{--                                                <input type="hidden" name="_method" value="PUT">--}}
        {{--                                                <div class="modal-body">--}}
        {{--                                                    <div class="row">--}}
        {{--                                                        <div class="col-md-12 col-sm-12 col-xs-12">--}}
        {{--                                                            <div class="row">--}}
        {{--                                                                <div class="form-main">--}}
        {{--                                                                    <div--}}
        {{--                                                                        class="form-group col-md-4 col-sm-4 @if($errors->has('corporate_target_from')) has-error @endif">--}}
        {{--                                                                        <label--}}
        {{--                                                                            for="corporate_target_from_field">{{__('backend.target_from')}}</label>--}}
        {{--                                                                        <input type="number" min="1"--}}
        {{--                                                                               id="corporate_target_from_field"--}}
        {{--                                                                               name="corporate_target_from"--}}
        {{--                                                                               class="form-control"--}}
        {{--                                                                               value="{{$target->corporate_target_from}}"/>--}}
        {{--                                                                        @if($errors->has("corporate_target_from"))--}}
        {{--                                                                            <span--}}
        {{--                                                                                class="help-block">{{ $errors->first("corporate_target_from") }}</span>--}}
        {{--                                                                        @endif--}}
        {{--                                                                    </div>--}}
        {{--                                                                    <div--}}
        {{--                                                                        class="form-group col-md-4 col-sm-4 @if($errors->has('corporate_target_to')) has-error @endif">--}}
        {{--                                                                        <label--}}
        {{--                                                                            for="corporate_target_to_field">{{__('backend.target_to')}}</label>--}}
        {{--                                                                        <input type="number" min="1"--}}
        {{--                                                                               id="corporate_target_to_field"--}}
        {{--                                                                               name="corporate_target_to"--}}
        {{--                                                                               class="form-control"--}}
        {{--                                                                               value="{{$target->corporate_target_to}}"/>--}}
        {{--                                                                        @if($errors->has("corporate_target_to"))--}}
        {{--                                                                            <span--}}
        {{--                                                                                class="help-block">{{ $errors->first("corporate_target_to") }}</span>--}}
        {{--                                                                        @endif--}}
        {{--                                                                    </div>--}}
        {{--                                                                    <div--}}
        {{--                                                                        class="form-group col-md-4 col-sm-4 @if($errors->has('corporate_discount')) has-error @endif">--}}
        {{--                                                                        <label--}}
        {{--                                                                            for="corporate_discount_field">{{__('backend.discount_percentage')}}</label>--}}
        {{--                                                                        <div class='input-group'>--}}
        {{--                                                                            <input type="text"--}}
        {{--                                                                                   id="corporate_discount_field"--}}
        {{--                                                                                   name="corporate_discount"--}}
        {{--                                                                                   class="form-control"--}}
        {{--                                                                                   value="{{$target->corporate_discount}}"/>--}}
        {{--                                                                            <div class="input-group-addon">--}}
        {{--                                                                                <i class="fa fa-percent"></i>--}}
        {{--                                                                            </div>--}}
        {{--                                                                        </div>--}}
        {{--                                                                        @if($errors->has("corporate_discount"))--}}
        {{--                                                                            <span--}}
        {{--                                                                                class="help-block">{{ $errors->first("corporate_discount") }}</span>--}}
        {{--                                                                        @endif--}}
        {{--                                                                    </div>--}}
        {{--                                                                </div>--}}
        {{--                                                            </div>--}}

        {{--                                                            <div class="row">--}}
        {{--                                                                <div--}}
        {{--                                                                    class="form-group col-md-12 col-sm-6 @if($errors->has('report')) has-error @endif">--}}
        {{--                                                                    <label for="report-field">{{__('backend.report')}}--}}
        {{--                                                                        : </label>--}}
        {{--                                                                    <textarea class="form-control" id="report-field"--}}
        {{--                                                                              rows="4"--}}
        {{--                                                                              name="report">{{$target->report}}</textarea>--}}
        {{--                                                                    @if($errors->has("report"))--}}
        {{--                                                                        <span--}}
        {{--                                                                            class="help-block">{{ $errors->first("report") }}</span>--}}
        {{--                                                                    @endif--}}
        {{--                                                                </div>--}}
        {{--                                                            </div>--}}
        {{--                                                        </div>--}}
        {{--                                                    </div>--}}
        {{--                                                </div>--}}

        {{--                                                <div class="modal-footer">--}}
        {{--                                                    <button type="submit"--}}
        {{--                                                            class="btn btn-primary">{{__('backend.create')}}</button>--}}
        {{--                                                </div>--}}
        {{--                                            </form>--}}

        {{--                                        </div>--}}
        {{--                                    </div>--}}
        {{--                                </div>--}}
        {{--                            </div>--}}
        {{--                            @endforeach--}}
        {{--                </table>--}}
        {{--            @else--}}
        {{--                <div class="text-center"><b>{{__('backend.this_corporate_not_have_any_target')}}</b></div>--}}
        {{--            @endif--}}
        {{--        </div>--}}
        {{--    </div>--}}

        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.corporates.index') }}"><i
                        class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
            </div>
        </div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script>
        $('.toggle').bootstrapToggle();
        $(function () {
            $('#active').on('click', function () {
                var Status = $(this).val();
                $.ajax({
                    url: "{{ url('/mngrAdmin/corporate/active/'.$corprate->id) }}",
                    Type: "GET",
                    dataType: 'json',
                    success: function (response) {
                        if (response == 0) {
                            $('#active').html('Active');

                        } else {
                            $('#active').html('UnActive');
                        }
                        location.reload();
                    }
                });
            });

            $('.verify_mobile').on('change', function () {
                let id = $(this).attr('data-id');
                $.ajax({
                    url: '{{URL::asset("/mngrAdmin/verify-mobile-customer")}}',
                    type: 'post',
                    data: {id: id, _token: "{{csrf_token()}}"},
                    success: function (data) {

                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            })

            $('.verify_email').on('change', function () {
                let id = $(this).attr('data-id');
                $.ajax({
                    url: '{{URL::asset("/mngrAdmin/verify-email-customer")}}',
                    type: 'post',
                    data: {id: id, _token: "{{csrf_token()}}"},
                    success: function (data) {

                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            })

            $('.change_block').on('change', function () {
                let id = $(this).attr('data-id');
                $.ajax({
                    url: '{{URL::asset("/mngrAdmin/change-block-customer")}}',
                    type: 'post',
                    data: {id: id, _token: "{{csrf_token()}}"},
                    success: function (data) {

                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            })

            $('.cost, .recall_cost, .cancel_cost, .reject_cost').on('input', function () {
                $("#save_cost_" + $(this).attr('data-id')).removeClass('disabled');
            });

            $('.save_cost').on('click', function (e) {
                e.preventDefault();
                var self = $(this);
                let id = $(this).attr('data-id');
                $.ajax({
                    url: '{{URL::asset("/mngrAdmin/corporatePrices/save_cost")}}',
                    type: 'post',
                    data: {
                        id: id,
                        cost: $("#cost_" + id).val(),
                        recall_cost: $("#recall_cost_" + id).val(),
                        reject_cost: $("#reject_cost_" + id).val(),
                        cancel_cost: $("#cancel_cost_" + id).val(),
                        _token: "{{csrf_token()}}"
                    },
                    success: function (data) {
                        self.addClass('disabled');
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            })
        });
    </script>
@endsection

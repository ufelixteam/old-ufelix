<div class="row">
  <div class="col-md-12 col-xs-12 col-sm-12">
      <?php if($corporates->count()): ?>
      <table class="table table-condensed table-striped">
        <thead>
          <tr>
            <th>#</th>
            <th><?php echo e(__('backend.name')); ?></th>
          	<th><?php echo e(__('backend.mobile_number')); ?></th>
          	<th><?php echo e(__('backend.mobile_number2')); ?></th>
            <th><?php echo e(__('backend.active')); ?></th>
          	<th><?php echo e(__('backend.in_slider')); ?></th>
            <th><?php echo e(__('backend.created_at')); ?></th>
            <th class="text-right"></th>
          </tr>
        </thead>
        <tbody>
            <?php $__currentLoopData = $corporates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $corporate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
              <td><?php echo e($corporate->id); ?></td>
              <td><?php echo e($corporate->name); ?></td>
  			      <td><?php echo e($corporate->mobile); ?></td>
  			      <td><?php echo e(isset($corporate->phone) ? $corporate->phone : __('backend.not_found')); ?></td>
  			      <td>
                <input data-id="<?php echo e($corporate->id); ?>" data-size="mini" class="toggle change_active"  <?php echo e($corporate->is_active == 1 ? 'checked' : ''); ?> data-onstyle="success" type="checkbox" data-style="ios" data-on="Yes" data-off="No">

              </td>

              <td>
              
                <input data-id="<?php echo e($corporate->id); ?>" data-size="mini" class="toggle change_slider"  <?php echo e($corporate->in_slider == 1 ? 'checked' : ''); ?> data-onstyle="success" type="checkbox" data-style="ios" data-on="Yes" data-off="No">

              </td>

              <td><?php echo e($corporate->created_at); ?></td>
              <td class="text-right">
                <?php if(permission('showOrdersMember')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                  <a class="btn btn-xs btn-default" href="<?php echo e(url('mngrAdmin/orders/showOrders') . '/2' . '/' . $corporate->id); ?>"><i class="glyphicon glyphicon-list"></i> <?php echo e(__('backend.the_orders')); ?></a>
                <?php endif; ?>

                <?php if(permission('showCorporate')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                  <a class="btn btn-xs btn-primary" href="<?php echo e(route('mngrAdmin.corporates.show', $corporate->id)); ?>"><i class="glyphicon glyphicon-eye-open"></i> <?php echo e(__('backend.view')); ?></a>
                <?php endif; ?>

                <?php if(permission('editCorporate')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                  <a class="btn btn-xs btn-warning" href="<?php echo e(route('mngrAdmin.corporates.edit', $corporate->id)); ?>"><i class="glyphicon glyphicon-edit"></i> <?php echo e(__('backend.edit')); ?></a>
                <?php endif; ?>
              
              </td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
      </table>

      <?php echo $corporates->appends($_GET)->links(); ?>


      <?php else: ?>
        <h3 class="text-center alert alert-warning"><?php echo e(__('backend.No_result_found')); ?>!</h3>
      <?php endif; ?>
  </div>
</div>

<script>
$('.toggle').bootstrapToggle();
</script><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/corporates/table.blade.php ENDPATH**/ ?>
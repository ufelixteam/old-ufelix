@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.admins')}} - {{__('backend.edit')}}</title>
@endsection

@section('content')
<h3>
<i class="glyphicon glyphicon-edit"></i> {{__('backend.edit_admin')}} - {{$user->name}}
</h3>
<div class="row">
  <div class="col-md-12 ">
    <!-- <form  method="POST" action="{{ URL::asset('mngrAdmin/users/'.$user->id) }}" name='validateForm'> -->
    <form  method="POST" action="{{ route('mngrAdmin.users.update', $user->id) }}" name='validateForm'>
      <input type="hidden" name="_method" value="PUT">
      {{ csrf_field() }}

      <div class="row">
        <div class="form-group col-sm-6 col-xs-6 col-md-6 @if($errors->has('name')) has-error @endif">
          <label for="name-field">{{__('backend.name')}}</label>
          <input type="text" id="name-field" name="name" class="form-control" value="{{ is_null(old("name")) ? $user->name : old("name") }}"/>
           @if($errors->has("name"))
            <span class="help-block">{{ $errors->first("name") }}</span>
           @endif
        </div>
        <div class="form-group col-sm-6 col-xs-6 col-md-6 @if($errors->has('email')) has-error @endif">
          <label for="email-field">{{__('backend.email')}}</label>
          <input type="email" id="email-field" name="email" class="form-control" value="{{ is_null(old("email")) ? $user->email : old("email") }}"/>
          @if($errors->has("email"))
          <span class="help-block">{{ $errors->first("email") }}</span>
          @endif
        </div>
      </div>
      <div class="row">
        <div class="form-group col-sm-6 col-xs-6 col-md-6 @if($errors->has('mobile')) has-error @endif">
          <label for="mobile-field">{{__('backend.mobile')}}</label>
          <input type="text" id="mobile-field" name="mobile" class="form-control" value="{{ is_null(old("mobile")) ? $user->mobile : old("mobile") }}"/>
          @if($errors->has("mobile"))
          <span class="help-block">{{ $errors->first("mobile") }}</span>
          @endif
        </div>
        <div class="form-group col-md-6 col-sm-6{{ $errors->has('role_admin_id') ? ' has-error' : '' }}">
            <label for="role_admin_id" class="control-label"> {{__('backend.choose_role')}} </label>
            <select class="form-control" name="role_admin_id">
              <option value="">{{__('backend.choose_role')}}</option>
              @foreach($role_admins as $role)
                <option value="{{$role->id}}" {{ $role->id == $user->role_admin_id ? 'selected' : '' }} >{{$role->name}}</option>
              @endforeach()
            </select>
            @if ($errors->has('role_admin_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('role_admin_id') }}</strong>
                </span>
            @endif
        </div>
      </div>
      <div class="row">
        <div class="form-group col-md-6 col-sm-6 @if($errors->has('password')) has-error @endif">
            <label for="password-field">{{__('backend.password')}}</label>
            <input type="password" id="password-field" name="password" class="form-control" value="{{ old("password") }}"/>
            <span class="help-block">{{__('backend.if_you_does_not_change_password_please_leave_it_empty')}}</span>
            @if($errors->has("password"))
                <span class="help-block">{{ $errors->first("password") }}</span>
            @endif
        </div>
        <div class="form-group col-md-6 col-sm-6 @if($errors->has('confirm_password')) has-error @endif">
            <label for="confirm_password-field">{{__('backend.confirm_password')}}</label>
            <input type="password" id="confirm_password-field" name="confirm_password" class="form-control" value="{{ old("confirm_password") }}"/>
            @if($errors->has("confirm_password"))
                <span class="help-block">{{ $errors->first("confirm_password") }}</span>
            @endif
        </div>
      </div>
      <div class="well well-sm col-md-12 col-sm-12">
          <button type="submit" class="btn btn-primary">{{__('backend.save_edits')}}</button>
          <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.admins.index') }}"><i class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
      </div>
    </form>
  </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(function() {
    $("form[name='validateForm']").validate({
      // Specify validation rules
      rules: {
          name: {
              required: true,
            },
          email: {
              required: true,
              email: true,
            },
          mobile: {
              required: true,
              number: true,
              rangelength:[11,11],
            },
          role_admin_id: {
              required: true,
            },
          password: {
              minlength: 5
          },
          password_confirmation: {
            	required: true,
              equalTo: '#password-field',
              minlength: 5
          },
      },
      // Specify validation error messages
      messages: {
          name: {
              required: "{{__('backend.Please_Enter_Admin_Name')}}",
            },
          email: {
               required: "{{__('backend.Please_Enter_Admin_Email')}}",
               email: "{{__('backend.Please_Enter_User_Correct_E-mail')}}",
             },
          mobile: {
                required: "{{__('backend.Please_Enter_Mobile_Number')}}",
                number: "{{__('backend.Please_Enter_Correct_Number')}}",
                rangelength:"{{__('backend.Mobile_Must_Be11_Digits')}}",
             },
          role_admin_id: {
                required: "{{__('backend.Please_Choose_Admin_Role')}}",
            },
          password_confirmation: "{{__('backend.Confirm_password_is_wrong')}}",
          password: {
              minlength: "{{__('backend.Password_must_be_more_than_5_character')}}"
          },
      },

       errorPlacement: function(error, element) {
          $(element).parents('.form-group').append(error)
      },

      highlight: function (element) {
          $(element).parent().addClass('error')
      },
      unhighlight: function (element) {
          $(element).parent().removeClass('error')
      },
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function(form) {
          form.submit();
      }
    });
  })
</script>
@endsection

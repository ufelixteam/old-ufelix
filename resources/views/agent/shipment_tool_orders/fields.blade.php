<div class="col-sm-12 col-md-12 remove-field"  style="margin-top: 10px;">

	<div class="col-sm-5 col-md-5">
		<input required id="quantity-field" type="number" name="quantity[]" class="form-control" placeholder="{{__('backend.quantity')}}"  />
	</div>

	<div class="form-group col-md-6 col-sm-6 @if($errors->has('items')) has-error @endif">
       <select id="items-field" name="items[]" class="form-control">
            @if(! empty($shipment_tools))
                @foreach($shipment_tools as $item)
                    <option value="{{$item->id}}" {{ old("items") == $item->id ? 'selected' : '' }}>
												@if(session()->has('lang') == 'ar')
													{{$item->name_ar}}
												@else
												{{$item->name_en}}
												@endif
										 </option>
                @endforeach
            @endif
        </select>
    </div>

  	<div class="input-group">
    	<span class="input-group-btn">
        	<input type="button" id="remove-field" value="-" class="btn btn-danger remove">
    	</span>
	</div>

</div>

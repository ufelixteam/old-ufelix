@extends('agent.layouts.app')
@section('css')
    <link href="{{URL::asset('/public/backend/css/style.css')}}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{URL::asset('/public/backend/css/font-awesome.css')}}" rel="stylesheet">
@endsection

@section('content')
    {{--//////////////////--}}
    <div class="profile-main">
        <div class="profile-pic wthree">
            <?php if ($agent->getAgentData->image):?>
            <a href="{{ asset('public/backend/images/'.$agent->getAgentUser->image) }}"> <img
                        style="height: 100px;width:100px;"
                        src="{{ asset('public/backend/images/'.$agent->getAgentUser->image) }}"></a>
            <?php endif;?>
            <h2>{{$agent->name}}</h2>
            <p>Agent</p>
            <p>{{$agent->getAgentData->email}}</p>
        </div>
        <div class="w3-message">
            <h5>About Me</h5>
            <table class="table">
                <tr>
                    <td>
                        Mobile
                    </td>
                    <td>
                    {{$agent->mobile}}
                    </td>
                </tr>
                <tr>
                    <td>
                        Phone
                    </td>

                    <td>
                        {{$agent->phone}}
                    </td>
                </tr>
                <tr>
                    <td>
                        Fax
                    </td>
                    <td>
                        {{$agent->fax}}
                    </td>
                </tr>
                <tr>
                    <td>
                        Commercial Record Number
                    </td>
                    <td>
                        {{$agent->commercial_record_number}}
                    </td>
                </tr>
                <tr>
                    <td>
                        Deal Date
                    </td>

                    <td>
                        {{$agent->deal_date}}
                    </td>
                </tr>
                <tr>
                    <td>
                        Profit Rate
                    </td>
                    <td>
                        {{$agent->profit_rate}}
                    </td>
                </tr>
                <tr>
                    <td>
                        Address
                    </td>
                    <td>
                        {{$agent->address}}
                    </td>
                </tr>
            </table>
            <p></p>
        </div>

    </div>
@endsection
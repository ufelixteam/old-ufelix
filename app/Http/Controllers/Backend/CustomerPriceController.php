<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use App\Models\Customer;
use App\Models\CustomerPrice;
use App\Models\Governorate;
use App\Models\GovernoratePrice;
use App\Models\Order;
use Auth;
use Flash;
use Illuminate\Http\Request;
use Response;

class CustomerPriceController extends Controller
{

    // Get Cities By Country ID
    public function get_governorate($id)
    {
        return Governorate::where('country_id', $id)->get();
    }

    // Get All Governorate Price
    public function prices_index()
    {
        $customerPrices = CustomerPrice::orderBy('id', 'desc')->paginate('15');
        return view('backend.customerPrice.index', compact('customerPrices'));
    }

    // Create Governorate Price Page
    public function create()
    {
        $list_governorates = Governorate::orderBy('name_en', 'ASC')->get();
        return view('backend.customer_prices.create', compact('list_governorates'));
    }

    // Added New Governorate Price
    public function store(Request $request)
    {
        /*
        * status = 0 :: Not Verify
        * status = 1 :: Verify
        */

        $v = \Validator::make($request->all(), [
            'start_station' => 'required',
            'access_station' => 'required',
            'cost' => 'required',
            'overweight_cost' => 'required',
            'status' => 'required',
            'customer_id' => 'required',
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        $customerPrice = new CustomerPrice();
        $customerPrice->start_station = $request->input("start_station");
        $customerPrice->access_station = $request->input("access_station");
        $customerPrice->cost = $request->input("cost");
        $customerPrice->recall_cost = $request->input("recall_cost");
        $customerPrice->reject_cost = $request->input("reject_cost");
        $customerPrice->cancel_cost = $request->input("cancel_cost");
        $customerPrice->overweight_cost = $request->input("overweight_cost");
        $customerPrice->status = $request->input("status");
        $customerPrice->customer_id = $request->input("customer_id");
        $customerPrice->save();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Add New Shipment Destinations #No ' . $customerPrice->id, ' اضافة وجهة شحن جديدة رقم ' . $customerPrice->id, 11); // Helper Function in App\Helpers.php

        Flash::success(__('backend.Destination_Added_successfully'));
        return redirect()->route('mngrAdmin.customers.show', ['customer_id' => $customerPrice->customer_id]);
    }

    // View One Governorate Price
    public function show($id)
    {
        $governorate = Governorate::findOrFail($id);
        return view('backend.governorates.show', compact('governorate'));
    }

    // Edit Governorate Price Page
    public function edit($id)
    {
        $customerPrice = CustomerPrice::findOrFail($id);
        $list_governorates = Governorate::orderBy('name_en', 'ASC')->get();
        return view('backend.customer_prices.edit', compact('customerPrice', 'list_governorates'));
    }

    // Update Governorate Price
    public function update(Request $request, $id)
    {
        /*
        * status = 0 :: Not Verify
        * status = 1 :: Verify
        */

        $v = \Validator::make($request->all(), [
            'start_station' => 'required',
            'access_station' => 'required',
            'cost' => 'required|numeric',
            'overweight_cost' => 'required|numeric',
            'status' => 'required',
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        $customerPrice = CustomerPrice::findOrFail($id);
        $customerPrice->start_station = $request->input("start_station");
        $customerPrice->access_station = $request->input("access_station");
        $customerPrice->cost = $request->input("cost");
        $customerPrice->recall_cost = $request->input("recall_cost");
        $customerPrice->reject_cost = $request->input("reject_cost");
        $customerPrice->cancel_cost = $request->input("cancel_cost");
        $customerPrice->overweight_cost = $request->input("overweight_cost");
        $customerPrice->status = $request->input("status");
        $customerPrice->save();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Edit Shipment Destinations #No ' . $customerPrice->id, ' تعديل واجهة الشحن رقم  ' . $customerPrice->id, 11); // Helper Function in App\Helpers.php

        Flash::warning(__('backend.Destination_Updated_successfully'));
        return redirect()->route('mngrAdmin.customers.show', ['customer_id' => $customerPrice->customer_id]);
    }

    // Delete Governorate Price
    public function destroy($id)
    {
        $customerPrice = CustomerPrice::findOrFail($id);
        $OrderCustomerPrice = Order::where('governorate_cost_id', $id)->first();
        if ($OrderCustomerPrice == null) { // If Not Have Orders Governorate Price Deleted
            $customerPrice->delete();

            // Function Add Action In Activity Log By Samir
            addActivityLog('Delete Shipment Destinations #No ' . $customerPrice->id, ' حذف واجهة الشحن رقم ' . $customerPrice->id, 11); // Helper Function in App\Helpers.php

            flash(__('backend.Destination_deleted_successfully'))->error();
            return redirect()->back();
        } else { // If Haves Orders Governorate Price Not Deleted
            Flash::error(__('backend.Can_not_delete_Destination_because_of_Order_Dependency'));
            return redirect()->back();
        }
    }

    public function generate(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'customer_id' => 'required'
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        $customer_id = $request->input("customer_id");
        $customer = Customer::find($customer_id);

        if ($customer->government_id) {
            $destinations = GovernoratePrice::where('start_station', $customer->government_id)->where('status', 1);

            if ($customer->government_id) {
                $destinations->where('start_station', $customer->government_id);
            }

            $destinations = $destinations->get();

            foreach ($destinations as $destination) {
                CustomerPrice::firstOrCreate([
                    'start_station' => $destination->start_station,
                    'access_station' => $destination->access_station,
                    'customer_id' => $customer_id
                ], [
                    'start_station' => $destination->start_station,
                    'access_station' => $destination->access_station,
                    'cost' => $destination->cost,
                    'recall_cost' => $destination->recall_cost,
                    'reject_cost' => $destination->reject_cost,
                    'cancel_cost' => $destination->cancel_cost,
                    'overweight_cost' => $destination->overweight_cost,
                    'status' => $destination->status,
                    'customer_id' => $customer_id,
                ]);
            }
            Flash::success(__('backend.Destination_Added_successfully'));
        } else {
            Flash::error(__('backend.Customer_not_have_governorate'));
        }

        return redirect()->route('mngrAdmin.customers.show', ['customer_id' => $request->input("customer_id")]);

    }

    public function save_cost(Request $request)
    {

//        $v = \Validator::make($request->all(), [
//            'cost' => 'required|numeric',
//            'recall_cost' => 'required|numeric',
//            'reject_cost' => 'required|numeric',
//            'cancel_cost' => 'required|numeric',
//            'id' => 'required',
//        ]);
//
//        if ($v->fails()) {
//            return [false];
//        }

        CustomerPrice::where('id', $request->input("id"))
            ->update([
                'cost' => ($request->input("cost") ? $request->input("cost") : 0),
                'recall_cost' => ($request->input("recall_cost") ? $request->input("recall_cost") : 0),
                'reject_cost' => ($request->input("reject_cost") ? $request->input("reject_cost") : 0),
                'cancel_cost' => ($request->input("cancel_cost") ? $request->input("cancel_cost") : 0)
            ]);

        return [true];
    }
}

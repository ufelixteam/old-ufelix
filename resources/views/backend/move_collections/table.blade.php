<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        @if($collections->count())
            <table class="table table-condensed table-striped text-center">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('backend.from_warehouse')}}</th>
                    <th>{{__('backend.to_warehouse')}}</th>
                    <th>{{__('backend.driver')}}</th>
                    <th>{{__('backend.status')}}</th>
                    <th>{{__('backend.cost')}}</th>
                    <th>{{__('backend.move_by')}}</th>
                    <th>{{__('backend.count_orders')}}</th>
                    <th>{{__('backend.Create_at')}}</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                @foreach($collections as $collection)
                    <tr>
                        <td>{{$collection->id}}</td>
                        <td>{{! empty($collection->from_store) ? $collection->from_store->name : '-'}}</td>
                        <td>{{! empty($collection->store) ? $collection->store->name : '-'}}</td>
                        <td>{{isset($collection->driver->name) ? $collection->driver->name : '-'}}</td>
                        <td>{!! $collection->dropped_span !!}</td>
                        <td>{{$collection->cost}}</td>
                        <td>{{$collection->move_by}}</td>
                        <td>{{$collection->orders_count}}</td>
                        <td>{{$collection->created_at}}</td>
                        <td>
                            <a class="btn btn-xs btn-primary "
                               href="{{ URL::asset('mngrAdmin/orders?move_collection='.$collection->id) }}">
                                {{__('backend.Orders_List')}}
                            </a>
                            <a class="btn btn-xs btn-success " target="_blank"
                               href="{{ URL::asset('mngrAdmin/move_excel?move_collection='.$collection->id) }}">
                                {{__('backend.Excel')}}
                            </a>
                            @if(!$collection->dropped_status && (!Auth::guard('admin')->user()->is_warehouse_user || checkWarehouse(Auth::guard('admin')->user()->id, $collection->store_id )))
                                <a class="btn btn-xs btn-info "
                                   href="{{ URL::asset('mngrAdmin/move_collections/drop_orders?collection_id='.$collection->id) }}">
                                    {{__('backend.confirm_drop_orders')}}
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {!! $collections->appends($_GET)->links() !!}
        @else
            <h3 class="text-center alert alert-warning">No Result Found !</h3>
        @endif
    </div>
</div>

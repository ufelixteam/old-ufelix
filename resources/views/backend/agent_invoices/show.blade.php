@extends('backend.layouts.app')
@section('header')
<div class="page-header">
        <h3>Show #{{$agent_invoice->id}}</h3>
        <form action="{{ route('mngrAdmin.agent_invoices.destroy', $agent_invoice->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('mngrAdmin.agent_invoices.edit', $agent_invoice->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <div class="form-group">
                <label for="nome">#</label>
                <p class="form-control-static"></p>
            </div>
            <div class="form-group col-sm-6">
                     <label for="from_date">FROM_DATE</label>
                     <p class="form-control-static">{{$agent_invoice->from_date}}</p>
                </div>
                    <div class="form-group col-sm-6">
                     <label for="to_date">TO_DATE</label>
                     <p class="form-control-static">{{$agent_invoice->to_date}}</p>
                </div>
                    <div class="form-group col-sm-6">
                     <label for="profit">PROFIT</label>
                     <p class="form-control-static">{{$agent_invoice->profit}}</p>
                </div>
                    <div class="form-group col-sm-6">
                     <label for="total_income">TOTAL_INCOME</label>
                     <p class="form-control-static">{{$agent_invoice->total_income}}</p>
                </div>
                    <div class="form-group col-sm-6">
                     <label for="status">STATUS</label>
                     <p class="form-control-static">{{$agent_invoice->status}}</p>
                </div>
                    <div class="form-group col-sm-6">
                     <label for="agent_id">AGENT_ID</label>
                     <p class="form-control-static">{{$agent_invoice->agent_id}}</p>
                </div>
          
            <div class="col-md-12 col-xs-12 col-sm-12">
                <a class="btn btn-link" href="{{ route('mngrAdmin.agent_invoices.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
            </div>

        </div>
    </div>

@endsection
@extends('captain.layouts.app')
@section('css')
    <title>{{__('captain.the_orders')}} - {{__('captain.view')}}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <style>
        .input-group {
            -moz-box-shadow: none !important;
            -webkit-box-shadow: none !important;
            box-shadow: none !important;
        }

        .input-group[class*=col-] {
            padding-right: inherit !important;
            padding-left: inherit !important;
            float: inherit;
        }

        .pac-container {
            z-index: 9999 !important;
        }
    </style>
@endsection
@section('header')
    <div class="page-header">
        <div class="pull-left">
            <h3>{{__('captain.Order_No')}} #{{$order->id}}</h3>

            {!! $order->status_span !!} Payment
            : {!! ! empty($order->payment_method ) ? '<span class="badge badge-pill label-success">'.$order->payment_method->name.'</span>' : '' !!}
            Total Price:

            {{ ! empty($order) ? $order->total_price: ''}}


            <br>
            <br>
            <span class="text-success"> Order Price: {{ ! empty($order) ? $order->order_price: ''}} </span> <br>
            <span class="text-primary"> Delivery Price: {{ ! empty($order) ? $order->delivery_price: ''}} </span><br>
            <span class="text-warning"> Total Price: {{ ! empty($order) ? $order->total_price: ''}} </span><br>

        </div>
        <div class="pull-right" style="margin-top: 45px">
            @if($order->status == 5 || ($order->status == 4 && $order->status_before_cancel != 0)|| ($order->status == 3 && $order->delivery_status == 2)|| ($order->status == 3 && $order->delivery_status == 4))
                @if($order->warehouse_dropoff)
                    <span class="text-info"> {{__('captain.warehouse')}}</span>
                    <br>
                @endif
                @if($order->client_dropoff)
                    <span class="text-warning"> {{__('captain.client')}}</span>
                    <br>
                @endif
            @endif
            @if(!empty($order) && ($order->collected_cost || $order->collected_cost == 0) && in_array($order->status, [3,5]) )
                <span
                    class="text-success"> {{__('captain.collected_cost')}}: {{ $order->collected_cost ? $order->collected_cost : 0 }} </span>
                <br>
            @endif
            @if(!empty($order) && $order->delivery_status)
                <span class="text-primary"> {{__('captain.delivery_status')}}:
                    @if ($order->status == 3 && $order->delivery_status == 1)
                        <span class='badge badge-pill label-success'> {{__('captain.full_delivered')}} </span>
                    @elseif ($order->status == 3 && $order->delivery_status == 2)
                        <span class='badge badge-pill label-black'> {{__('captain.pr')}} </span>
                    @elseif ($order->status == 3 && $order->delivery_status == 4)
                        <span class='badge badge-pill label-info'> {{__('captain.replace')}}  </span>
                    @elseif ($order->status == 5 && $order->delivery_status == 3)
                        <span class='badge badge-pill label-danger'> {{__('captain.reject')}}  </span>
                    @elseif ($order->status == 8 && $order->delivery_status == 5)
                        <span class='badge badge-pill label-danger'> {{__('backend.reject')}}  </span>
                    @endif
            </span>
                <br>
            @endif

            @if(!empty($order) && $order->is_refund)
                <span class='badge badge-pill label-refund'>{{ __('captain.refund')}} </span>
                <br>
            @endif
        </div>
        <div class="clearfix"></div>

    </div>

    <div class="row" style="padding: 12px 20px;">
        <div class="alert alert-danger" style="display:none"></div>
        <!-- Create at: -->
        <div class="form-group col-sm-3">
            <label for="receiver_name">{{__('captain.Create_at')}}: </label>
            <p class="form-control-static"
               style="color: #0376d9; font-weight: bold;">@if($order->created_at){{$order->created_at}} @else {{__('captain.No_Date')}} @endif </p>
        </div>

        <!-- Dropped at: -->
        <div class="form-group col-sm-3">
            <label for="receiver_name">{{__('captain.Dropped_at')}}: </label>
            <p class="form-control-static"
               style="color: #0376d9; font-weight: bold;">@if($order->dropped_at){{$order->dropped_at}} @else {{__('captain.No_Date')}} @endif </p>
        </div>

        <!-- Accept at: -->
        <div class="form-group col-sm-3">
            <label for="reciever_mobile"> {{__('captain.Accept_at')}}: </label>
            <p class="form-control-static" style="color: #0376d9; font-weight: bold;">
                @if( $order->status != 0 && $order->status != 7 )
                    {{$order->accepted_at}}
                @else {{__('captain.No_Date')}} @endif
            </p>
        </div>

        <!-- Receiver at: -->
        <div class="form-group col-sm-3">
            <label for="receiver_name">{{__('captain.Received_at')}}: </label>
            <p class="form-control-static" style="color: #0376d9; font-weight: bold;">
                @if( $order->status != 0  )
                    {{$order->received_at}}
                @else {{__('captain.No_Date')}}  @endif
            </p>
        </div>

        <!-- Deliver at: -->
        <div class="form-group col-sm-2">
            <label for="reciever_mobile"> {{__('captain.Deliver_at')}}: </label>
            <p class="form-control-static" style="color: #0376d9; font-weight: bold;">
                @if ($order->status != 0 )
                    {{$order->delivered_at}}
                @else {{__('captain.No_Date')}} @endif
            </p>
        </div>

        <!-- Recalled at: -->
        <div class="form-group col-sm-2">
            <label for="reciever_mobile"> {{__('captain.Recalled_at')}}: </label>
            <p class="form-control-static" style="color: #0376d9; font-weight: bold;">
                @if ($order->status != 0 )
                    {{$order->recalled_at}}
                @else {{__('captain.No_Date')}} @endif
            </p>
        </div>

        <!-- Rejected at: -->
        <div class="form-group col-sm-2">
            <label for="reciever_mobile"> {{__('backend.Rejected_at')}}: </label>
            <p class="form-control-static" style="color: #0376d9; font-weight: bold;">
                @if ($order->status != 0 )
                    {{$order->rejected_at}}
                @else {{__('backend.No_Date')}} @endif
            </p>
        </div>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="col-sm-12"
                 style="border: 2px solid #607d8b;color: #fff;margin-bottom: 20px;background-color: #607d8b;">
                <h4>{{__('captain.Receiver_Data')}}</h4>
            </div>
            <div class="form-group col-sm-3">
                <label for="receiver_name">{{__('captain.receiver_name')}}: </label>
                <p class="form-control-static">{{$order->receiver_name}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="reciever_mobile">{{__('captain.mobile_number')}}: </label>
                <p class="form-control-static">{{$order->receiver_mobile}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="reciever_phone">{{__('captain.mobile_number2')}}: </label>
                <p class="form-control-static">{{$order->receiver_phone}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="reciever_address">{{__('captain.Receiver_Address')}}:</label>
                <p class="form-control-static">{{$order->receiver_address}}</p>
            </div>
            <div class="form-group col-sm-12">
                <div class="row">
                    <div class="col-sm-12"><h4><strong>{{__('captain.Customer_Account')}}</strong></h4></div>
                    <div class="col-sm-3"><strong>{{__('captain.name')}}
                            : </strong>{{! empty($order->customer) ? $order->customer->name : '' }}</div>
                    <div class="col-sm-3"><strong>{{__('captain.email')}}
                            : </strong>{{! empty($order->customer) ? $order->customer->email : '' }}</div>
                    <div class="col-sm-3"><strong>{{__('captain.mobile_number')}}
                            : </strong>{{! empty($order->customer) ? $order->customer->mobile : '' }}</div>
                </div>
            </div>

            <div class="col-sm-12"
                 style="border: 2px solid #607d8b;color: #fff;margin-bottom: 20px;background-color: #607d8b;">
                <h4>{{__('captain.Sender_Data')}}</h4>
            </div>
            <div class="form-group col-sm-3">
                <label for="sender_name">{{__('captain.sender_name')}}: </label>
                <p class="form-control-static">{{$order->sender_name}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="sender_mobile">{{__('captain.mobile_number')}}: </label>
                <p class="form-control-static">{{$order->sender_mobile}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="sender_phone">{{__('captain.mobile_number2')}}:</label>
                <p class="form-control-static">{{$order->sender_phone}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="sender_address">{{__('captain.Sender_Address')}}: </label>
                <p class="form-control-static">{{$order->sender_address}}</p>
            </div>

            <div class="col-sm-12"
                 style="border: 2px solid #607d8b;color: #fff;margin-bottom: 20px;background-color: #607d8b;">
                <h4>{{__('captain.Package_Data')}}</h4>
            </div>

            <div class="form-group col-sm-3">
                <label for="notes">{{__('captain.Image')}}: </label>
                <p class="form-control-static"><img src="{{! empty($order->image) ? $order->image : '#' }}"
                                                    onerror="this.src='{{asset('assets/images/image.png')}}'"
                                                    style="border-radius: 50%;width: 100px;height: 100px;"></p>
            </div>
            <div class="form-group col-sm-3">
                <label for="type_id">{{__('captain.order_type')}}: </label>
                <p class="form-control-static">{{! empty($order->type) ? $order->type : '' }}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="order_number">{{__('captain.order_number')}}: </label>
                <p class="form-control-static">{{$order->order_number}}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="notes">{{__('captain.notes')}}: </label>
                <p class="form-control-static">{{! empty($order->notes) ? $order->notes : '-' }}</p>
            </div>
            <div class="form-group col-sm-3">
                <label for="order_number">{{__('captain.receiver_code')}}: </label>
                <p class="form-control-static">{{$order->receiver_code}}</p>
            </div>

            <div class="col-sm-12"
                 style="border: 2px solid #607d8b;color: #fff;margin-bottom: 20px;background-color: #607d8b;">
                <h4>{{__('captain.Driver_Data')}}</h4>
            </div>
            @if(! empty($order->accepted ) && ! empty($order->accepted->driver))
                <div class="form-group col-sm-3">
                    <label for="sender_name">{{__('captain.Driver_Name')}}: </label>
                    <p class="form-control-static">{{ ! empty($order->accepted ) && ! empty($order->accepted->driver) ? $order->accepted->driver->name : ''}}</p>
                </div>
                <div class="form-group col-sm-3">
                    <label for="sender_mobile">{{__('captain.mobile_number')}}: </label>
                    <p class="form-control-static">{{ ! empty($order->accepted ) && ! empty($order->accepted->driver) ? $order->accepted->driver->mobile : ''}}</p>
                </div>
                <div class="form-group col-sm-3">
                    <label for="email">{{__('captain.email')}}: </label>
                    <p class="form-control-static">{{! empty($order->accepted ) && ! empty($order->accepted->driver) ? $order->accepted->driver->email : ''}}</p>
                </div>
                <div class="form-group col-sm-3">
                    <label for="sender_address">{{__('captain.Driver_Address')}}: </label>
                    <p class="form-control-static">{{  ! empty($order->accepted ) && ! empty($order->accepted->driver) ? $order->accepted->driver->address : ''}}</p>
                </div>
            @else
                <div class="form-group col-sm-4 col-sm-offset-4 text-center">
                    <div class="alert alert-info" style="padding-bottom: 3px;">
                        <h4>{{__('captain.Order_is_not_Accepted_yet')}}</h4>
                    </div>
                </div>
            @endif

            <div class="col-sm-12"
                 style="border: 2px solid #607d8b;color: #fff;margin-bottom: 20px;background-color: #607d8b;">
                <h4>
                    {{__('captain.order_delay')}}
                </h4>

            </div>
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="table-responsive">
                    @if(count($order->order_delay))
                        <table id="theTable" class="table table-condensed table-striped text-center">
                            <thead>
                            <tr>
                                <th>{{__('captain.note')}}</th>
                                <th>{{__('captain.created_by')}}</th>
                                <th>{{__('captain.status')}}</th>
                                <th>{{__('captain.comment')}}</th>
                                <th>{{__('captain.delay_at')}}</th>
                            </tr>
                            </thead>

                            @foreach($order->order_delay as $order_delay)
                                <tr>
                                    <td>{{$order_delay->delay_comment}}</td>
                                    <td>{{!empty($order_delay->user) ? $order_delay->user->name : '-'}}</td>
                                    <td>
                                        @if($order_delay->change_status == 1)
                                            {!! "<span class='badge badge-pill label-black'>" . __('captain.dropped') . "</span>" !!}
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>{{$order_delay->delay_comment}}</td>
                                    <td>{{$order_delay->delay_at}}</td>
                                </tr>
                            @endforeach

                            <tbody>
                            </tbody>
                        </table>
                    @else
                        <div class="form-group col-sm-4 col-sm-offset-4 text-center">
                            <div class="alert alert-info" style="padding-bottom: 3px;">
                                <h4>{{__('captain.no_order_delay')}}</h4>
                            </div>
                        </div>
                    @endif
                </div>
            </div>

            <div class="col-sm-12"
                 style="border: 2px solid #607d8b;color: #fff;margin-bottom: 20px;background-color: #607d8b;">
                <h4>
                    {{__('captain.delivery_problems')}}
                </h4>

            </div>
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="table-responsive">
                    @if(count($order->delivery_problems))
                        <table id="theTable" class="table table-condensed table-striped text-center">
                            <thead>
                            <tr>
                                <th>{{__('captain.driver')}}</th>
                                <th>{{__('captain.reason')}}</th>
                                <th>{{__('captain.date')}}</th>
                                <th>{{__('captain.created_by')}}</th>
                                <th>{{__('captain.location')}}</th>
                                <th></th>
                            </tr>
                            </thead>

                            @foreach($order->delivery_problems as $delivery_problems)
                                <tr>
                                    <td>{{isset($delivery_problems->driver->name) ? $delivery_problems->driver->name : '-'}}</td>
                                    <td>{{$delivery_problems->reason ? $delivery_problems->reason : $delivery_problems->problem->reason_en}}</td>
                                    <td>{{date("Y-m-d", strtotime($delivery_problems->created_at))}}</td>
                                    <td>{{!empty($delivery_problems->user) ? $delivery_problems->user->name : '-'}}</td>
                                    <td>
                                        <a href="http://maps.google.com/?q={{$delivery_problems->latitude}},{{$delivery_problems->longitude}}"
                                           target="_blank"><i class="fa fa-map-marker"></i></a></td>
                                    <td>
                                        @if(permission('deleteDeliveryProblem'))
                                            <form
                                                action="{{ route('captainDashboard.orders.delete_problem', $delivery_problems->id) }}"
                                                method="POST" style="display: inline;"
                                                onsubmit="if(confirm('{{__('captain.msg_confirm_delete')}}')) { return true } else {return false };">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <button type="submit" class="btn btn-xs btn-danger"><i
                                                        class="glyphicon glyphicon-trash"></i> {{__('captain.delete')}}
                                                </button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach

                            <tbody>
                            </tbody>
                        </table>
                    @else
                        <div class="form-group col-sm-4 col-sm-offset-4 text-center">
                            <div class="alert alert-info" style="padding-bottom: 3px;">
                                <h4>{{__('captain.no_delivery_problems')}}</h4>
                            </div>
                        </div>
                    @endif
                </div>
            </div>

            <div class="col-md-12 col-xs-12 col-sm-12">
                <a class="btn btn-link pull-right" href="{{URL::previous()}}"><i
                        class="fa fa-backward"></i> {{__('captain.back')}}</a>

            </div>

        </div>
    </div>

@endsection

@section('scripts')

    <script type="text/javascript" src="{{asset('/assets/scripts/map-order.js')}}"></script>

    <script
        src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key=AIzaSyDQiN4qc7pC9b1BRQwkqXXD28peMfWcHvw&callback=myMap"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

    <script>

        $('#delay_at').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            language: 'en',
            todayHighlight: true,
            startDate: new Date()
        });

        $('#created_at').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            language: 'en',
            todayHighlight: true
        });

        $('body').on('click', '.problem_radio', function () {
            var problem_radio = $("input[name='problem_radio']:checked").val();
            console.log('hereeee in body ', problem_radio);

            if (problem_radio == 'delay') {
                console.log('hereeee in body if', problem_radio);

                $('.delay_at').removeClass('hidden');
            } else {
                console.log('hereeee in body ', problem_radio);

                $('.delay_at').addClass('hidden');
            }
        });

        $("#delivery_problem_id").on('change', function () {
            if ($(this).val() == 5) {
                $("#other_reason").show();
            } else {
                $("#reason").val("");
                $("#other_reason").hide();
            }
        });

        /*Cancel-order*/
        $("#cancelOrderByAdmin").on('click', function () {
            var id = $(this).attr('data-id');
            $.confirm({
                title: '',
                theme: 'modern',
                class: 'danger',
                content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Confirm Cancel</span></div><div style="font-weight:bold;"><p>Are You want To Cancel This Order ? According To Admin Wanted</p></div>',
                buttons: {
                    confirm: {
                        text: 'Cancel Order',
                        btnClass: 'btn-blue',
                        action: function () {
                            $.ajax({
                                url: "{{ url('/captainDashboard/cancel_order_captain')}}",
                                type: 'post',
                                data: {'_token': "{{csrf_token()}}", 'id': id},
                                success: function (data) {
                                    jQuery('.alert-danger').html('');
                                    jQuery('.alert-danger').show();
                                    if (data != 'false') {
                                        jQuery('.alert-danger').append('<p>' + data + '</p>');
                                        // setTimeout(function () {
                                        location.reload();
                                        // }, 2000);
                                    } else {
                                        jQuery('.alert-danger').append('<p>Something Error</p>');
                                    }
                                    window.scrollTo({top: 0, behavior: 'smooth'});
                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                            });
                        }
                    },
                    cancel: {
                        text: 'Cancel',
                        action: function () {
                        }
                    }
                }
            });
        });
        $("#cancelOrderClient").on('click', function () {
            var id = $(this).attr('data-id');
            $.confirm({
                title: '',
                theme: 'modern',
                class: 'danger',
                content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Confirm Cancel</span></div><div style="font-weight:bold;"><p>Are You want To Cancel This Order ? According To Client Wanted</p></div>',
                buttons: {
                    confirm: {
                        text: 'Cancel Order',
                        btnClass: 'btn-blue',
                        action: function () {
                            $.ajax({
                                url: "{{ url('/captainDashboard/cancel_order_client')}}",
                                type: 'post',
                                data: {'_token': "{{csrf_token()}}", 'id': id},
                                success: function (data) {
                                    jQuery('.alert-danger').html('');
                                    jQuery('.alert-danger').show();
                                    if (data != 'false') {
                                        jQuery('.alert-danger').append('<p>' + data + '</p>');
                                        // setTimeout(function () {
                                        location.reload();
                                        // }, 2000);
                                    } else {
                                        jQuery('.alert-danger').append('<p>Something Error</p>');
                                    }
                                    window.scrollTo({top: 0, behavior: 'smooth'});
                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                            });
                        }
                    },
                    cancel: {
                        text: 'Cancel',
                        action: function () {
                        }
                    }
                }
            });
        });

        $(function () {
            {{--$("#recall_by").on('change', function () {--}}
            {{--    if ($(this).val() > 1) {--}}
            {{--        $('#password_div').hide();--}}
            {{--        $('#password').attr('readonly', true);--}}
            {{--        $('#code_div').show();--}}
            {{--        $('#code').attr('readonly', false);--}}
            {{--        $("#send_div").show();--}}
            {{--    } else {--}}
            {{--        $('#password_div').show();--}}
            {{--        $('#password').attr('readonly', false);--}}

            {{--        $('#code_div').hide();--}}
            {{--        $('#code').attr('readonly', true);--}}
            {{--        $("#send_div").hide();--}}
            {{--    }--}}
            {{--});--}}
            {{--$("#send_sms").on('click', function () {--}}
            {{--    $.ajax({--}}
            {{--        url: "{{ url('/captainDashboard/send_cofirm_recall_sms')}}",--}}
            {{--        type: 'post',--}}
            {{--        data: $("#recallForm").serialize(),--}}
            {{--        success: function (data) {--}}
            {{--            jQuery('.danger').html('');--}}
            {{--            jQuery('.danger').show();--}}
            {{--            jQuery('.success').html('');--}}
            {{--            jQuery('.success').show();--}}
            {{--            console.log(data)--}}
            {{--            if (data != 'false') {--}}
            {{--                jQuery('.success').append('<p>Sent Succefully</p>');--}}
            {{--                // setTimeout(function() {--}}
            {{--                //     location.reload();--}}
            {{--                // }, 2000);--}}
            {{--            } else {--}}
            {{--                jQuery('.danger').append('<p>Something Error</p>');--}}
            {{--            }--}}
            {{--        },--}}
            {{--        error: function (data) {--}}
            {{--            console.log('Error:', data);--}}
            {{--        }--}}
            {{--    });--}}
            {{--});--}}
            $("#send_recall").on('click', function () {
                $.ajax({
                    url: "{{ url('/captainDashboard/orders/recallOrder')}}",
                    type: 'post',
                    data: $("#recallForm").serialize(),
                    success: function (data) {
                        jQuery('.danger').html('');
                        jQuery('.danger').show();
                        jQuery('.success').html('');
                        jQuery('.success').show();
                        console.log(data);
                        if (data != 'false') {
                            jQuery('.success').append('<p>Sent Successfully</p>');
                            // setTimeout(function () {
                            location.reload();
                            // }, 2000);
                        } else {
                            jQuery('.danger').append('<p>Something Error</p>');
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            });
            $("#delete-order").on('click', function () {
                var id = $(this).attr('data-id');
                $.confirm({
                    title: '',
                    theme: 'modern',
                    class: 'danger',
                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Confirm Delete</span></div><div style="font-weight:bold;"><p>Are You sure To Delete This Order ?</p></div>',
                    buttons: {
                        confirm: {
                            text: 'Delete Order',
                            btnClass: 'btn-danger',
                            action: function () {
                                $.ajax({
                                    url: "{{ url('/captainDashboard/orders/')}}" + "/" + id,
                                    type: 'DELETE',
                                    data: {'_token': "{{csrf_token()}}"},
                                    success: function (data) {

                                        if (data != 'false') {

                                            // setTimeout(function () {
                                            location.reload();
                                            window.location.href = "{{ url('/captainDashboard/orders?type=')}}" + data;
                                            // }, 2000);
                                        } else {
                                            jQuery('.alert-danger').append('<p>Something Error</p>');
                                        }
                                        window.scrollTo({top: 0, behavior: 'smooth'});
                                    },
                                    error: function (data) {
                                        console.log('Error:', data);
                                    }
                                });
                            }
                        },
                        cancel: {
                            text: 'Cancel',
                            action: function () {
                            }
                        }
                    }
                });
            })

        });

    </script>

@endsection

<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use App\Http\Requests;
use App\Models\PriceList;
use Illuminate\Http\Request;
use Flash;
class PriceListController extends Controller {

	// Get All Price List
	public function index()
	{
		$price_lists = PriceList::orderBy('id', 'desc')->paginate(15);
		return view('backend.price_lists.index', compact('price_lists'));
	}

	// Create Price List Page
	public function create()
	{
		return view('backend.price_lists.create');
	}

	// Added New Price List
	public function store(Request $request)
	{
		$this->validate($request, [
        'target' 									=> 'required|numeric',
        'discount_percentage' 		=> 'required|numeric'
    ]);

		$price_list 											= new PriceList();
		$price_list->target 							= $request->input("target");
    $price_list->discount_percentage 	= $request->input("discount_percentage");
		$price_list->save();

		// Function Add Action In Activity Log By Samir
		addActivityLog('Add New Price List #ID ' . $price_list->id , ' اضافة قائمة اسعار جديدة #رقم ' . $price_list->id, 29); // Helper Function in App\Helpers.php

		Flash::success(__('backend.Price_List_saved_successfully'));
		return redirect()->route('mngrAdmin.price_lists.index');
	}

	// View One Price List
	public function show($id)
	{
		return back();
		// $price_list = PriceList::findOrFail($id);
		// return view('backend.price_lists.show', compact('price_list'));
	}

	// Edit Price List Page
	public function edit($id)
	{
		$price_list = PriceList::findOrFail($id);
		return view('backend.price_lists.edit', compact('price_list'));
	}

	// Update Price List
	public function update(Request $request, $id)
	{
		$this->validate($request, [
        'target' 									=> 'required|numeric',
        'discount_percentage' 		=> 'required|numeric'
    ]);

		$price_list 											= PriceList::findOrFail($id);
		$price_list->target 							= $request->input("target");
    $price_list->discount_percentage 	= $request->input("discount_percentage");
		$price_list->save();

		// Function Add Action In Activity Log By Samir
		addActivityLog('Edit Price List #ID ' . $price_list->id , ' تعديل قائمة الاسعار #رقم ' . $price_list->id, 29); // Helper Function in App\Helpers.php

		Flash::warning(__('backend.Price_List_updated_successfully'));
		return redirect()->route('mngrAdmin.price_lists.index');
	}

	// Delete Price List
	public function destroy($id)
	{
		$price_list = PriceList::findOrFail($id);
		$price_list->delete();

		// Function Add Action In Activity Log By Samir
		addActivityLog('Delete Price List #ID ' . $price_list->id , ' حذف قائمة الاسعار #رقم ' . $price_list->id, 29); // Helper Function in App\Helpers.php

		flash(__('backend.Price_List_deleted_successfully'))->error();
		return redirect()->route('mngrAdmin.price_lists.index');
	}

}

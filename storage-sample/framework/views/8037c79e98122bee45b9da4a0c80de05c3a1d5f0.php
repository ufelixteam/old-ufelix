<?php $__env->startSection('css'); ?>
    <title><?php echo e(__('backend.the_pickups')); ?> - <?php echo e(__('backend.view')); ?></title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">

<?php $__env->stopSection(); ?>
<?php $__env->startSection('header'); ?>
    <div class="page-header">
        <h3><?php echo e(__('backend.Pickup_No')); ?> #<?php echo e($pickup->pickup_number); ?></h3> <?php echo $pickup->status_span; ?>


        <div class="dropdown  pull-right">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="true">
                <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
            </button>
            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                <?php if( $pickup->status != 3 ): ?>
                    <li>
                        <a href="<?php echo e(route('mngrAdmin.pickups.edit', $pickup->id)); ?>"><i
                                class="fa fa-edit"></i> <?php echo e(__('backend.edit')); ?></a>
                    </li>
                <?php endif; ?>

                <?php if($pickup->total_order_no >0): ?>
                    <li>
                        <a href="<?php echo e(URL::asset('/mngrAdmin/print_pickup_pdf/'.$pickup->id)); ?>" target="_blank"
                           data-id="<?php echo e($pickup->id); ?>"><i class="fa fa-print"></i> <?php echo e(__('backend.pdf')); ?></a>
                    </li>

                    <?php if( $pickup->status == 2 ): ?>
                        <li>
                            <a href="<?php echo e(url('mngrAdmin/receive_pickup/'.$pickup->id)); ?>"><i
                                    class="fa fa-cart-arrow-down "></i> <?php echo e(__('backend.receive')); ?></a>
                        </li>
                    <?php endif; ?>

                    <?php if($pickup->status == 0 ): ?>
                        <li>
                            <a id="forward_agent" data-toggle="modal" data-target="#forward_agentMo"><i
                                    class="fa fa-user-secret" aria-hidden="true"></i> <?php echo e(__('backend.Change_Agent')); ?></a>
                        </li>
                    <?php endif; ?>


                    <?php if( $pickup->status < 2 ): ?>
                        <li>
                            <a id="forward" data-toggle="modal" data-target="#exampleModal"> <i class="fa fa-share"
                                                                                                aria-hidden="true"></i> <?php if($pickup->status != 0 ): ?>  <?php echo e(__('backend.Cancel_and')); ?> <?php else: ?> <?php echo e(__('backend.forward')); ?> <?php endif; ?>
                            </a>
                        </li>
                    <?php endif; ?>
                <?php endif; ?>

                <?php if( $pickup->status == 0): ?>

                    <li>
                        <a data-id="<?php echo e($pickup->id); ?>" id="delete-order"><i
                                class="fa fa-trash"></i> <?php echo e(__('backend.delete')); ?></a>
                    </li>
                <?php endif; ?>

            </ul>
        </div>

    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>


    <div class="row" style="padding: 12px 20px;">
        <div class="alert alert-danger" style="display:none"></div>

        <div class="form-group col-sm-2">
            <label for="agent"><?php echo e(__('backend.Agent')); ?>: </label>
            <p class="form-control-static"><?php if($pickup->agent): ?> <?php echo e($pickup->agent->name); ?>  <?php endif; ?> </p>
        </div>

        <div class="form-group col-sm-2">
            <label for="agent"><?php echo e(__('backend.Delivery_Price')); ?>: </label>
            <p class="form-control-static"> <?php echo e($pickup->delivery_price); ?>   </p>
        </div>

        <!-- Create at: -->
        <div class="form-group col-sm-2">
            <label for="receiver_name"><?php echo e(__('backend.captain_received_time')); ?>: </label>
            <p class="form-control-static"><?php echo e($pickup->captain_received_time); ?>  </p>
        </div>
        <!-- Create at: -->
        <div class="form-group col-sm-2">
            <label for="receiver_name"><?php echo e(__('backend.Create_at')); ?>: </label>
            <p class="form-control-static"><?php echo e($pickup->created_at); ?>  </p>
        </div>

        <!-- Accept at: -->
        <div class="form-group col-sm-2">
            <label for="reciever_mobile"> <?php echo e(__('backend.Accept_at')); ?> / <?php echo e(__('backend.Forward_at')); ?> : </label>
            <p class="form-control-static">
                <?php echo e($pickup->accepted_at); ?>


            </p>
        </div>

        <!-- Receiver at: -->
        <div class="form-group col-sm-2">
            <label for="receiver_name"><?php echo e(__('backend.Received_at')); ?>: </label>
            <p class="form-control-static">

                <?php echo e($pickup->received_at); ?>


            </p>
        </div>

        <!-- Deliver at: -->
        <div class="form-group col-sm-2">
            <label for="reciever_mobile"> <?php echo e(__('backend.Deliver_at')); ?>: </label>
            <p class="form-control-static">

                <?php echo e($pickup->delivered_at); ?>

            </p>
        </div>


        <div class="form-group col-sm-2">
            <label for="reciever_mobile"> <?php echo e(__('backend.orders_no')); ?>: </label>
            <p class="form-control-static">

                <?php echo e($pickup->total_order_no); ?>

            </p>
        </div>


        <div class="form-group col-sm-2">
            <label for="reciever_mobile"> <?php echo e(__('backend.totalbounus')); ?>: </label>
            <p class="form-control-static">

                <?php echo e($pickup->bonus_per_order * $pickup->total_order_no); ?>

            </p>
        </div>

        <div class="form-group col-sm-2">
            <label for="reciever_mobile"> <?php echo e(__('backend.total_price')); ?>: </label>
            <p class="form-control-static">

                <?php echo e(($pickup->bonus_per_order * $pickup->total_order_no) + $pickup->delivery_price); ?>

            </p>
        </div>


    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="col-sm-12"
                 style="border: 2px solid #607d8b;color: #fff;margin-bottom: 20px;background-color: #607d8b;">
                <h4><?php echo e(__('backend.Receiver_Data')); ?></h4>
            </div>
            <div class="form-group col-sm-6">
                <label for="receiver_name"><?php echo e(__('backend.receiver_name')); ?>: </label>
                <p class="form-control-static"><?php echo e($pickup->receiver_name); ?></p>
            </div>
            <div class="form-group col-sm-6">
                <label for="reciever_mobile"><?php echo e(__('backend.mobile_number')); ?>: </label>
                <p class="form-control-static"><?php echo e($pickup->receiver_mobile); ?></p>
            </div>
            <div class="form-group col-sm-6">
                <label for="reciever_address"><?php echo e(__('backend.Receiver_Address')); ?>:</label>
                <p class="form-control-static"><?php echo e($pickup->receiver_address); ?></p>
            </div>

            <div class="col-sm-12"
                 style="border: 2px solid #607d8b;color: #fff;margin-bottom: 20px;background-color: #607d8b;">
                <h4><?php echo e(__('backend.Sender_Data')); ?></h4>
            </div>
            <div class="form-group col-sm-6">
                <label for="sender_name"><?php echo e(__('backend.sender_name')); ?>: </label>
                <p class="form-control-static"><?php echo e($pickup->sender_name); ?></p>
            </div>
            <div class="form-group col-sm-6">
                <label for="sender_mobile"><?php echo e(__('backend.mobile_number')); ?>: </label>
                <p class="form-control-static"><?php echo e($pickup->sender_mobile); ?></p>
            </div>
            <div class="form-group col-sm-6">
                <label for="sender_address"><?php echo e(__('backend.Sender_Address')); ?>: </label>
                <p class="form-control-static"><?php echo e($pickup->sender_address); ?></p>
            </div>


            <div class="col-sm-12"
                 style="border: 2px solid #607d8b;color: #fff;margin-bottom: 20px;background-color: #607d8b;">
                <h4><?php echo e(__('backend.Driver_Data')); ?></h4>
            </div>
            <?php if(! empty($pickup ) && ! empty($pickup->driver)): ?>
                <div class="form-group col-sm-6">
                    <label for="sender_name"><?php echo e(__('backend.Driver_Name')); ?>: </label>
                    <p class="form-control-static"><?php echo e(! empty($pickup ) && ! empty($pickup->driver) ? $pickup->driver->name : ''); ?></p>
                </div>
                <div class="form-group col-sm-6">
                    <label for="sender_mobile"><?php echo e(__('backend.mobile_number')); ?>: </label>
                    <p class="form-control-static"><?php echo e(! empty($pickup ) && ! empty($pickup->driver) ? $pickup->driver->mobile : ''); ?></p>
                </div>
                <div class="form-group col-sm-6">
                    <label for="email"><?php echo e(__('backend.email')); ?>: </label>
                    <p class="form-control-static"><?php echo e(! empty($pickup ) && ! empty($pickup->driver) ? $pickup->driver->email : ''); ?></p>
                </div>
                <div class="form-group col-sm-6">
                    <label for="sender_address"><?php echo e(__('backend.Driver_Address')); ?>: </label>
                    <p class="form-control-static"><?php echo e(! empty($pickup ) && ! empty($pickup->driver) ? $pickup->driver->address : ''); ?></p>
                </div>
            <?php else: ?>
                <div class="form-group col-sm-4 col-sm-offset-4 text-center">
                    <div class="alert alert-info" style="padding-bottom: 3px;">
                        <h4><?php echo e(__('backend.Pickup_is_not_Accepted_yet')); ?></h4>
                    </div>
                </div>
            <?php endif; ?>


            <div class="col-sm-12"
                 style="border: 2px solid #607d8b;color: #fff;margin-bottom: 20px;background-color: #607d8b;">
                <h4><?php echo e(__('backend.Packup_orders')); ?></h4>

            </div>
            <?php echo $__env->make('backend.pickups.orders', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

            <div class="col-md-12 col-xs-12 col-sm-12">
                <a class="btn btn-link pull-right" href="<?php echo e(URL::previous()); ?>"><i
                        class="fa fa-backward"></i> <?php echo e(__('backend.back')); ?></a>

            </div>
        </div>
    </div>

    <?php echo $__env->make('backend.pickups.driver_forward_modal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('backend.pickups.agent_forward_modal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

    <script>

        $(function () {

            $(".editPickup").on('click', function () {
                $('#delivery_price_modal').val($(this).attr('data-bonous'))
                $('#order_id_modal').val($(this).attr('data-id'));
                $('#editPickupModal').modal('show')

            });
            $(".delete-order-pickup").on('click', function () {
                var orderid = $(this).attr('data-id');
                var pickid = $(this).attr('data-pickup');
                $.confirm({
                    title: '',
                    theme: 'modern',
                    class: 'danger',
                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Confirm Delete</span></div><div style="font-weight:bold;"><p>Are You sure To Delete This Order From PickUp ?</p></div>',
                    buttons: {
                        confirm: {
                            text: 'Delete Order From PickUp',
                            btnClass: 'btn-danger',
                            action: function () {
                                $.ajax({
                                    url: "<?php echo e(url('/mngrAdmin/delete_order_pickup/')); ?>",
                                    type: 'DELETE',
                                    data: {'_token': "<?php echo e(csrf_token()); ?>", 'order_id': orderid, 'pickup_id': pickid},
                                    success: function (data) {

                                        if (data != 'false') {

                                            location.reload();
                                        } else {
                                            jQuery('.alert-danger').append('<p>Something Error</p>');
                                        }
                                        window.scrollTo({top: 0, behavior: 'smooth'});
                                    },
                                    error: function (data) {
                                        console.log('Error:', data);
                                    }
                                });
                            }
                        },
                        cancel: {
                            text: 'Cancel',
                            action: function () {
                            }
                        }
                    }
                });
            });

            $("#delete-order").on('click', function () {
                var id = $(this).attr('data-id');
                $.confirm({
                    title: '',
                    theme: 'modern',
                    class: 'danger',
                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Confirm Delete</span></div><div style="font-weight:bold;"><p>Are You sure To Delete This PickUp ?</p></div>',
                    buttons: {
                        confirm: {
                            text: 'Delete PickUp',
                            btnClass: 'btn-danger',
                            action: function () {
                                $.ajax({
                                    url: "<?php echo e(url('/mngrAdmin/pickups/')); ?>" + "/" + id,
                                    type: 'DELETE',
                                    data: {'_token': "<?php echo e(csrf_token()); ?>"},
                                    success: function (data) {

                                        if (data != 'false') {

                                            setTimeout(function () {
                                                location.reload();
                                                window.location.href = "<?php echo e(url('/mngrAdmin/pickups')); ?>";
                                            }, 2000);
                                        } else {
                                            jQuery('.alert-danger').append('<p>Something Error</p>');
                                        }
                                        window.scrollTo({top: 0, behavior: 'smooth'});
                                    },
                                    error: function (data) {
                                        console.log('Error:', data);
                                    }
                                });
                            }
                        },
                        cancel: {
                            text: 'Cancel',
                            action: function () {
                            }
                        }
                    }
                });
            })

        });

    </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/pickups/show.blade.php ENDPATH**/ ?>
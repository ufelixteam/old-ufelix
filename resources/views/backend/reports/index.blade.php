@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.reports')}}</title>
    <link rel="stylesheet" href="{{asset('/assets/scripts/multiselect/css/multi-select.css')}}">
    <style>
        .ms-container {
            width: 100% !important;
        }
    </style>
@endsection
@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="fa fa-file-excel-o"></i> {{__('backend.reports')}}

        </h3>

    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <form action="{{URL::asset('/mngrAdmin/reports')}}" method="post" id="sort-form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group col-md-2 col-sm-6 col-xs-12 @if($errors->has('from_date')) has-error @endif">
                    <label for="from_date-field">{{__('backend.From_Date')}}</label>
                    <div class='input-group date ' id="datepicker1">
                        <input type="text" id="from_date-field" name="from_date" class="form-control" required/>
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                    </div>
                    @if($errors->has("from_date"))
                        <span class="help-block">{{ $errors->first("from_date") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-2 col-sm-6 col-xs-12 @if($errors->has('to_date')) has-error @endif">
                    <label for="to_date-field">{{__('backend.To_Date')}}</label>
                    <div class='input-group date ' id="datepickers">
                        <input type="text" id="to_date-field" name="to_date" class="form-control" required/>
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                    </div>
                    @if($errors->has("to_date"))
                        <span class="help-block">{{ $errors->first("to_date") }}</span>
                    @endif
                </div>


                <div class="form-group col-md-2 col-sm-6 col-xs-12 @if($errors->has('type')) has-error @endif">
                    <label for="type-field"> {{__('backend.Search_about')}} </label>
                    <select id="type-field" name="type" class="form-control" value="{{  old("type") }}">
                        <option value="1">{{__('backend.drivers')}}</option>
                        <option value="2">{{__('backend.customers')}}</option>
                        {{--                        <option value="3">{{__('backend.agents')}}</option>--}}
                        <option value="4">{{__('backend.corporates')}}</option>
                        <option value="5">{{__('backend.the_orders')}}</option>
                        {{--                        <option value="6">{{__('backend.requests')}}</option>--}}
                    </select>
                    @if($errors->has("type"))
                        <span class="help-block">{{ $errors->first("type") }}</span>
                    @endif
                </div>

                <div class="form-group col-md-2 col-sm-6 col-xs-12 @if($errors->has('sort_by')) has-error @endif">
                    <label for="sort_by-field">{{__('backend.Order_By')}}</label>
                    <select id="sort_by-field" name="sort_by" class="form-control" value="{{  old("sort_by") }}">
                        <option value="id">{{__('backend.id')}}</option>
                        <option value="created_at"> {{__('backend.Create_at')}} </option>

                    </select>
                    @if($errors->has("sort_by"))
                        <span class="help-block">{{ $errors->first("sort_by") }}</span>
                    @endif
                </div>

                <div class="form-group col-xs-12 col-md-4 col-sm-12 ">

                    <label for="download-field" style="visibility: hidden;">. . . </label>

                    <div class=" col-xs-12 col-md-6 col-sm-12 ">
                        <button id="download-field" type="submit" class="btn btn-primary btn-block"
                                style="margin-top: 24px;"> {{__('backend.download')}} </button>
                    </div>

                    {{--                    <div class=" col-xs-12 col-md-6 col-sm-12 ">--}}
                    {{--                        <button id="sort-btn" type="button"--}}
                    {{--                                class="btn btn-new btn-block"> {{__('backend.display')}} </button>--}}
                    {{--                    </div>--}}

                </div>


                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h3 class="text-center">{{__('backend.columns')}}</h3>
                    <hr>
                </div>

                <input type="hidden" name="columns" id="columns">

                <div class="row columnsWrapper" id="driversWrapper">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        {{--                        <input type="hidden" name="drivers-order" id="drivers-hidden">--}}
                        <select multiple="multiple" id="drivers" name="drivers[]">
                            <option value="ID">ID</option>
                            <option value="Joined At">Joined At</option>
                            <option value="Name">Name</option>
                            <option value="Email">Email</option>
                            <option value="Mobile">Mobile</option>
                            <option value="Phone">Phone</option>
                            <option value="Address">Address</option>
                        </select>
                    </div>
                </div>

                <div class="row columnsWrapper" id="customersWrapper" style="display: none">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        {{--                        <input type="hidden" name="customers-order" id="customers-hidden">--}}
                        <select multiple="multiple" id="customers" name="customers[]">
                            <option value="ID">ID</option>
                            <option value="Joined At">Joined At</option>
                            <option value="Name">Name</option>
                            <option value="Email">Email</option>
                            <option value="Mobile">Mobile</option>
                            <option value="Phone">Phone</option>
                            <option value="Address">Address</option>
                        </select>
                    </div>
                </div>

                <div class="row columnsWrapper" id="corporatesWrapper" style="display: none">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        {{--                        <input type="hidden" name="corporates-order" id="corporates-hidden">--}}
                        <select multiple="multiple" id="corporates" name="corporates[]">
                            <option value="ID">ID</option>
                            <option value="Joined At">Joined At</option>
                            <option value="Name">Name</option>
                            <option value="Email">Email</option>
                            <option value="Mobile">Mobile</option>
                            <option value="Phone">Phone</option>
                            <option value="Field">Field</option>
                            <option value="Commercial Record Number">Commercial Record Number</option>
                        </select>
                    </div>
                </div>

                <div class="row columnsWrapper" id="ordersWrapper" style="display: none">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <input type="hidden" name="orders-order" id="orders-hidden">
                        <select multiple="multiple" id="orders" name="orders[]">
                            <option value="ID">ID</option>
                            <option value="Corporate Name">Corporate Name</option>
                            <option value="Account Name">Account Name</option>
                            <option value="Sender Code">Sender Code</option>
                            <option value="Reference Number">Reference Number</option>
                            <option value="Created At">Created At</option>
                            <option value="Last Update Date">Last Update Date</option>
                            <option value="New Date">New Date</option>
                            <option value="Dropped Date">Dropped Date</option>
                            <option value="Fees">Fees</option>
                            <option value="Total Price">Total Price</option>
                            <option value="Status">Status</option>
                            <option value="Warehouse">Warehouse</option>
                            <option value="Collected Cost">Collected Cost</option>
                            <option value="Customer Name">Customer Name</option>
                            <option value="Phone Number">Phone Number</option>
                            <option value="Receiver Government">Receiver Government</option>
                            <option value="Manager">Manager</option>
                            <option value="Captain">Captain</option>
                            <option value="Notes">Notes</option>
                            <option value="Address">Address</option>
                        </select>
                    </div>
                </div>

            </form>
        </div>

        {{--        <div class=" row col-md-12 text-center" id="table-view">--}}

        {{--        </div>--}}
    </div>

@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('/assets/scripts/multiselect/js/jquery.multi-select.js')}}"></script>
    <script>
        var columns = '';
        $('#drivers').multiSelect({
            // keep the original order
            keepOrder: true,
            afterSelect: function (values) {
                // after select
                if (columns) {
                    columns += ',' + values[0];
                } else {
                    columns += values[0];
                }

                $("#columns").val(columns);
            },
            afterDeselect: function (values) {
                // after deselect
                let res = columns.split(",");
                let filtered = res.filter(function (value, index, arr) {
                    return !res.includes(value);
                });
                columns = filtered.join();

                $("#columns").val(columns);
            }

        });

        $('#customers').multiSelect({
            // keep the original order
            keepOrder: true,
            afterSelect: function (values) {
                // after select
                if (columns) {
                    columns += ',' + values[0];
                } else {
                    columns += values[0];
                }

                $("#columns").val(columns);
            },
            afterDeselect: function (values) {
                // after deselect
                let res = columns.split(",");
                let filtered = res.filter(function (value, index, arr) {
                    return !res.includes(value);
                });
                columns = filtered.join();

                $("#columns").val(columns);
            }

        });

        $('#corporates').multiSelect({
            // keep the original order
            keepOrder: true,
            afterSelect: function (values) {
                // after select
                if (columns) {
                    columns += ',' + values[0];
                } else {
                    columns += values[0];
                }

                $("#columns").val(columns);
            },
            afterDeselect: function (values) {
                // after deselect
                let res = columns.split(",");
                let filtered = res.filter(function (value, index, arr) {
                    return !res.includes(value);
                });
                columns = filtered.join();

                $("#columns").val(columns);
            }

        });

        $('#orders').multiSelect({
            // keep the original order
            keepOrder: true,
            afterSelect: function (values) {
                // after select
                if (columns) {
                    columns += ',' + values[0];
                } else {
                    columns += values[0];
                }

                $("#columns").val(columns);
            },
            afterDeselect: function (values) {
                // after deselect
                let res = columns.split(",");
                let filtered = res.filter(function (value, index, arr) {
                    return !res.includes(value);
                });
                columns = filtered.join();

                $("#columns").val(columns);
            }

        });

        $("#type-field").change(function () {
            $(".columnsWrapper").hide();
            if ($(this).val() == 1) {
                $("#driversWrapper").show();
            } else if ($(this).val() == 2) {
                $("#customersWrapper").show();
            } else if ($(this).val() == 4) {
                $("#corporatesWrapper").show();
            } else if ($(this).val() == 5) {
                $("#ordersWrapper").show();
            }

            $('#drivers').multiSelect('deselect_all');
            $('#customers').multiSelect('deselect_all');
            $('#corporates').multiSelect('deselect_all');
            $('#orders').multiSelect('deselect_all');
        });

        $("#sort-btn").on('click', function () {
            // Wait icon
            $('.breadcrumb li').append('<span id="tempo" class="fa fa-refresh fa-spin"></span>');
            $.ajax({
                url: "{{url('/mngrAdmin/reports')}}",
                type: 'POST',
                data: $("#sort-form").serialize(),
                success: function (data) {
                    $('#table').html('');
                    $('#table-view').html(data.view)
                },
                error: function (data) {

                    $('#tempo').remove();
                    alert('fail');
                    console.log('Error:', data);
                }
            });

        });


    </script>

@endsection

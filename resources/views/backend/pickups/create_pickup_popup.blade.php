<div class="modal fade" id="pickupModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create Pick Up</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ URL::asset('/mngrAdmin/pickups') }}" method="POST" id="pickup-form" class="order-form">


                    <div class="modal-body  " style="min-height: 420px">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="from" id="from-pick" value="0">
                        <input type="hidden" name="orderpickup[]" id="orderpickup" value="[]">
                        <input type="hidden" name="corporate_id" id="pickup_corporate_id" value="">
                        <input type="hidden" name="customer_id" id="pickup_customer_id" value="">
                        <input type="hidden" name="collection_id" id="collection_id" value="">
                        <input type="hidden" name="is_pickup" id="is_pickup" value="1">

                        <div class="form-group col-md-3 col-sm-3 @if($errors->has('store_id')) has-error @endif">
                            <label for="store_id">{{__('backend.dropped_pickup_store')}}:</label>
                            <select id="store_id" name="store_id" class="form-control">
                                <option value="">{{__('backend.Choose_Store')}}</option>
                                @if(! empty($allStores))
                                    @foreach($allStores as $store)
                                        <option
                                            value="{{$store->id}}" {{ ! empty($pickup_store) && $store->id == $pickup_store->id ? 'selected' : '' }} >
                                            {{$store->name}}
                                        </option>
                                    @endforeach
                                @endif

                            </select>
                            @if($errors->has("store_id"))
                                <span class="help-block">{{ $errors->first("store_id") }}</span>
                            @endif
                        </div>
                        <div class="form-group col-md-3 col-sm-3 @if($errors->has('receiver_name')) has-error @endif">
                            <label for="receiver_name-field"> {{__('backend.name')}}: </label>
                            <input type="text" required id="reciever_name-field" name="receiver_name"
                                   class="form-control"
                                   value=" {{ ! empty($pickup_store) ? $pickup_store->name : '' }}"/>
                            @if($errors->has("receiver_name"))
                                <span class="help-block">{{ $errors->first("receiver_name") }}</span>
                            @endif
                        </div>
                        <div class="form-group col-md-3 col-sm-3 @if($errors->has('receiver_mobile')) has-error @endif">
                            <label for="reciever_mobile-field"> {{__('backend.mobile_number')}}: </label>
                            <input type="text" required id="receiver_mobile-field" name="receiver_mobile"
                                   class="form-control"
                                   value="{{ ! empty($pickup_store) ? $pickup_store->mobile : '' }}"/>
                            @if($errors->has("receiver_mobile"))
                                <span class="help-block">{{ $errors->first("receiver_mobile") }}</span>
                            @endif
                        </div>

                        <div class="form-group col-md-3 col-sm-3">
                            <label class="control-label" for="r_government_id"> {{__('backend.government')}}:</label>

                            <select class="form-control pick_r_government_id" id="pick_r_government_id"
                                    name="r_government_id">
                                <option value=""
                                        data-display="Select">{{__('backend.Receiver_Government')}}</option>
                                @if(! empty($app_governments))
                                    @foreach($app_governments as $government)
                                        <option
                                            value="{{$government->id}}">
                                            {{$government->name_en}}
                                        </option>

                                    @endforeach
                                @endif

                            </select>

                        </div>

                        <div class="form-group col-md-3 col-sm-3">
                            <label class="control-label" for="r_state_id"> {{__('backend.city')}}:</label>

                            <select class=" form-control " id="pick_r_state_id" name="r_state_id">
                                <option value="">{{__('backend.Receiver_City')}}</option>

                            </select>

                        </div>
                        <div
                            class="form-group col-md-3 col-sm-3 @if($errors->has('receiver_latitude')) has-error @endif">
                            <label for="receiver_latitude"> {{__('backend.latitude')}}: </label>
                            <input type="text" required id="receiver_latitude" name="receiver_latitude"
                                   class="form-control"
                                   value="{{ ! empty($pickup_store) ? $pickup_store->latitude : '' }}"/>
                            @if($errors->has("receiver_latitude"))
                                <span class="help-block">{{ $errors->first("receiver_latitude") }}</span>
                            @endif
                        </div>
                        <div
                            class="form-group col-md-3 col-sm-3 @if($errors->has('receiver_longitude')) has-error @endif">
                            <label for="reciever_longitude"> {{__('backend.longitude')}}: </label>
                            <input type="text" required id="receiver_longitude" name="receiver_longitude"
                                   class="form-control"
                                   value="{{ ! empty($pickup_store) ? $pickup_store->longitude : '' }}"/>
                            @if($errors->has("receiver_longitude"))
                                <span class="help-block">{{ $errors->first("receiver_longitude") }}</span>
                            @endif
                        </div>


                        <div
                            class="form-group col-md-12 col-sm-12 @if($errors->has('receiver_address')) has-error @endif">
                            <label for="reciever_address">{{__('backend.Receiver_Address')}}: </label>
                            <input type="text" required id="receiver_address" name="receiver_address"
                                   class="form-control"
                                   value="{{ ! empty($pickup_store) ? $pickup_store->address : '' }}"/>
                            @if($errors->has("receiver_address"))
                                <span class="help-block">{{ $errors->first("receiver_address") }}</span>
                            @endif
                        </div>

                        <div class="form-group col-md-4 col-sm-4 @if($errors->has('delivery_price')) has-error @endif">
                            <label for="delivery_price-field">{{__('backend.Delivery_Price')}}: </label>
                            <input type="number" required class="form-control" id="delivery_price-field"
                                   name="delivery_price"
                                   value="{{ old('delivery_price')  ? old('delivery_price')  : '0'}}">
                            @if($errors->has("delivery_price"))
                                <span class="help-block">{{ $errors->first("delivery_price") }}</span>
                            @endif
                        </div>

                        <div class="form-group col-md-4 col-sm-4 @if($errors->has('bonous')) has-error @endif">
                            <label for="bonous-field">{{__('backend.the_bonous')}}: </label>
                            <input type="number" required class="form-control" id="bonous-field" name="bonus_per_order"
                                   value="{{ old('bonous')  ? old('bonous')  : '0'}}">
                            @if($errors->has("bonous"))
                                <span class="help-block">{{ $errors->first("bonous") }}</span>
                            @endif
                        </div>

{{--                        <div class="form-group col-md-4 col-sm-4 @if($errors->has('agent_id')) has-error @endif">--}}
{{--                            <label for="agent_id-field"> {{__('backend.agent')}}: </label>--}}
{{--                            <select id="pick_agent_id" name="agent_id" class="form-control">--}}
{{--                                <option value=""> {{__('backend.Choose_Agent')}}</option>--}}
{{--                                @if(! empty($agents))--}}
{{--                                    @foreach($agents as $agent)--}}
{{--                                        <option value="{{$agent->id}}">--}}
{{--                                            {{$agent->name}}--}}
{{--                                        </option>--}}
{{--                                    @endforeach--}}
{{--                                @endif--}}

{{--                            </select>--}}
{{--                            @if($errors->has("agent_id"))--}}
{{--                                <span class="help-block">{{ $errors->first("agent_id") }}</span>--}}
{{--                            @endif--}}
{{--                        </div>--}}

                        <div class="form-group col-md-4 col-sm-4">
                            <label class="control-label" for="driver_id"> {{__('backend.driver')}}:</label>

                            <select class=" form-control driver_id" id="pick_driver_id" name="driver_id">
                                <option value=""> {{__('backend.Choose_Driver')}}</option>
                                @if(! empty($drivers))

                                    @foreach($drivers as $driver1)
                                        <option value="{{$driver1->id}}">
                                            {{$driver1->name}}
                                        </option>
                                    @endforeach

                                @endif

                            </select>

                        </div>

                        <div class="group-control col-md-3 col-sm-3">
                            <label class="control-label"
                                   for="captain_received_date"> {{__('backend.captain_received_date')}}:</label>

                            <div class='input-group date' id='datepickerFilter'>
                                <input type="date" id="date" name="captain_received_date" class="form-control" value=""
                                       required="required" data-date-format="YYYY-MM-DD"/>
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>


{{--                        <div class="group-control col-md-3 col-sm-3">--}}
{{--                            <label class="control-label" for="driver_id"> {{__('backend.captain_received_time')}}--}}
{{--                                :</label>--}}

{{--                            <div class='input-group date' id='datepickerFilter'>--}}
{{--                                <input type="time" name="captain_received_time" class="form-control" value=""--}}
{{--                                       required="required"/>--}}
{{--                                <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="savePickup">Save</button>
                    </div>

                </form>
            </div>


        </div>
    </div>
</div>

<script src="{{asset('/assets/vendor/jquery/jquery.min.js')}}"></script>

<script>

    $(document).ready(function () {
        var state_id = "{{@$pickup_store->state_id}}";
        var from_store = true;
        var drivers_bonus = {};
        var drivers_fees = {};

        $("#savePickup").on('click', function () {
            var data = $("#pickup-form").serialize();
            $.ajax({
                url: "{{ url('/mngrAdmin/pickups')}}",
                type: 'post',
                data: data,
                success: function (data) {
                    if (data['status'] != 'false') {
                        $('#pickup-form')[0].reset();
                        $('#pickupModal').modal('hide');
                        window.location.href = '{{url("/mngrAdmin/pickups")}}'
                    } else {
                        $('#errorId').html(data['message']);
                        $(".alert").show();
                        $('#pickupModal').modal('hide');
                    }

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $("#pick_agent_id").on('change', function () {
            $('#pick_driver_id').html('<option value="" >Choose Drivers</option>');
            $.ajax({
                url: "{{ url('/mngrAdmin/getdriver_by_agents')}}" + "/" + $(this).val(),
                type: 'get',
                data: {},
                success: function (data) {
                    drivers_bonus = {};
                    drivers_fees = {};

                    $.each(data, function (i, content) {
                        $('#pick_driver_id').append($("<option></option>").attr("value", content.id).text(content.name));
                        drivers_bonus[content.id] = content.bouns_of_pickup_for_one_order ? content.bouns_of_pickup_for_one_order : 0;
                        drivers_fees[content.id] = content.pickup_price ? content.pickup_price : 0;
                    });
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $("#pick_r_government_id").on('change', function () {
            $('#pick_r_state_id').html('<option value="" >Sender City</option>');
            console.log('Hereeeeeeee');
            if ($(this).val()) {
                $.ajax({
                    url: "{{ url('/mngrAdmin/get_cities')}}" + "/" + $(this).val(),
                    type: 'get',
                    data: {},
                    success: function (data) {

                        $.each(data, function (i, content) {
                            $('#pick_r_state_id').append($("<option></option>").attr("value", content.id).text(content.city_name));
                        });
                        if (from_store) {
                            console.log('gffc');
                            $("#pick_r_state_id").val(state_id);
                            from_store = false;
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }

            {{--/* get price list*/--}}
            {{--$.ajax({--}}
            {{--    url: "{{ url('/mngrAdmin/get_cost_pickup')}}" + "/" + $("#from-pick").val() + "/" + $(this).val(),--}}
            {{--    type: 'get',--}}
            {{--    data: {},--}}
            {{--    success: function (data) {--}}
            {{--        if (data != "0") {--}}
            {{--            $('#delivery_price-field').val(data.cost);--}}
            {{--            $('#bonous-field').val(data.bonous);--}}

            {{--        }--}}

            {{--    },--}}
            {{--    error: function (data) {--}}
            {{--        console.log('Error:', data);--}}
            {{--    }--}}
            {{--});--}}

        });

        $("#store_id").on('change', function () {
            from_store = true;
            $.ajax({
                url: "{{ url('/mngrAdmin/get_store')}}" + "/" + $(this).val(),
                type: 'get',
                data: {},
                success: function (data) {
                    $('#reciever_name-field').val(data.name);
                    $('#receiver_mobile-field').val(data.mobile);
                    $('#receiver_latitude').val(data.latitude);
                    $('#receiver_longitude').val(data.longitude);
                    $('#receiver_address').val(data.address);
                    $("#pick_r_government_id").val(data.governorate_id).trigger('change');
                    state_id = data.state_id;
                    // $("div.pick_r_government_id > select > option[value=" + data.governorate_id + "]").attr("selected", true);

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

        });

        $("#pick_driver_id").on('change', function (e) {
            const bonus = $(this).val() ? drivers_bonus[$(this).val()] : 0;
            const fees = $(this).val() ? drivers_fees[$(this).val()] : 0;
            $("#bonous-field").val(bonus);
            $("#delivery_price-field").val(fees);
        });

        $("#pick_r_government_id").val("{{@$pickup_store->governorate_id}}").trigger('change');
    });

</script>


@extends('backend.layouts.app')

@section('content')
    <div class="main_content_container">
        <div class="main_container  main_menu_open page_content">
            <h1 class="heading_title">Track Drivers</h1>
            <div style="margin-top:20px;"></div>

            <div class="row">
                <div class="col-md-12 col-xs-6 ">

                    <div style="width:800px;height:500px;">
                        <?php
                        $lat = array();
                        $long = array();
                        $lat_off = array();
                        $long_off = array();
                        $lat_busy = array();
                        $long_busy = array();
                        $name = array();
                        $name1 = array();
                        $name2 = array();

                        if (!empty($drivers_last_locations) && count($drivers_last_locations) > 0) {
                            # code...
                            foreach ($drivers_last_locations as $drivers_last_location) {
                                if ($drivers_last_location->status == "on") {
                                    array_push($long, $drivers_last_location->last_long);
                                    array_push($lat, $drivers_last_location->last_lat);
                                    array_push($name, $drivers_last_location->driver_id);
                                }
                                if ($drivers_last_location->status == "off") {
                                    array_push($long_off, $drivers_last_location->last_long);
                                    array_push($lat_off, $drivers_last_location->last_lat);
                                    array_push($name1, $drivers_last_location->driver_id);

                                }
                                if ($drivers_last_location->status == "busy") {
                                    array_push($long_busy, $drivers_last_location->last_long);
                                    array_push($lat_busy, $drivers_last_location->last_lat);
                                    array_push($name2, $drivers_last_location->driver_id);

                                }
                            }

                            Mapper::map($lat[0], $long[0], ['strokeColor' => '#003400', 'strokeOpacity' => 0.1, 'strokeWeight' => 2, 'fillColor' => '#65638ea', 'clusters' => ['size' => 10, 'center' => true, 'zoom' => 20], 'zoom' => 8]);


                            for ($i = 0; $i < count($lat); $i++) {
                                // echo $lat[$i],$long[$i];
                                Mapper::marker($lat[$i], $long[$i], ['markers' => ['title' => 'Online', 'symbol' => 'circle', 'scale' => 1000, 'animation' => 'DROP']])->informationWindow($lat[$i], $long[$i], "$name[$i]", ['markers' => ['animation' => 'DROP']])->rectangle([['latitude' => $lat[$i], 'longitude' => $long[$i]], ['latitude' => $lat[$i], 'longitude' => $long[$i]]], ['strokeColor' => '#ea234a', 'strokeOpacity' => 0.1, 'strokeWeight' => 2, 'fillColor' => '#FFFFFF']);
                            }

                            for ($i = 0; $i < count($lat_off); $i++) {
                                // echo $lat[$i],$long[$i];
                                Mapper::marker($lat_off[$i], $long_off[$i], ['markers' => ['title' => 'Offline', 'symbol' => 'circle', 'scale' => 1000, 'animation' => 'DROP']])->informationWindow($lat_off[$i], $long_off[$i], "$name1[$i]", ['markers' => ['animation' => 'DROP']]);
                            }

                            for ($i = 0; $i < count($lat_busy); $i++) {
                                // echo $lat[$i],$long[$i];
                                Mapper::marker($lat_busy[$i], $long_busy[$i], ['markers' => ['title' => 'Busy', 'symbol' => 'rectangle', 'scale' => 1000, 'animation' => 'DROP']])->informationWindow($lat_busy[$i], $long_busy[$i], "$name2[$i]", ['markers' => ['animation' => 'DROP']]);
                            }
                        }
                        Mapper::map(53.381128999999990000, -1.470085000000040000);
                        $map = Mapper::render();
                        echo $map;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

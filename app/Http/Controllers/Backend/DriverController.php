<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use App\Mail\Verified;
use App\Models\AcceptedOrder;
use App\Models\Agent;
use App\Models\City;
use App\Models\Color;
use App\Models\Country;
use App\Models\Device;
use App\Models\Driver;
use App\Models\Governorate;
use App\Models\ModelType;
use App\Models\OrderLog;
use App\Models\ShipmentType;
use App\Models\Store;
use App\Models\Truck;
use App\Models\VehicleType;
use App\Models\Verification;
use Auth;
use DB;
use File;
use Flash;
use Illuminate\Http\Request;
use Mail;
use Mapper;
use move;
use Response;
use Storage;

class DriverController extends Controller
{
    private $guardType;

    public function __construct(Request $request)
    {
        if ($request->is('mngrAdmin/*') || $request->is('admin/*')) {
            \Config::set('auth.defaults.guard', 'admin');
        } elseif ($request->is('mngrAgent/*') || $request->is('agent/*')) {
            \Config::set('auth.defaults.guard', 'agent');
        }
        $this->guardType = \Config::get('auth.defaults.guard');

    }

    // Get All Driver With Agent
    public function showdriver($id)
    {
        $drivers = Driver::where('agent_id', $id)->get();
        $agent = Agent::where('id', $id)->value('name');
        return view('backend.agents.showdriver', compact('drivers', 'agent'));
    }

    /**
     * Track Drivers
     */
    public function track_drivers($key)
    {
        $user = Auth::guard($this->guardType)->user();
        if ($user->role_id == '1' && $this->guardType == 'admin') {
            $onIds = Device::where('object_type', 2)->where('status', 1)->pluck('object_id')->toArray();

            if ($user->is_warehouse_user) {
                $online_drivers = Driver::join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                    ->select('drivers.*')
                    ->where('user_id', $user->id)
                    ->whereIn('drivers.id', $onIds)
                    ->get();
            } else {
                $online_drivers = Driver::whereIn('id', $onIds)->get();
            }
//            $drivers = Driver::all();
            //            $offIds = Device::where('object_type', 2)->where('status', 0)->pluck('object_id')->toArray();

//            $busy_drivers = Driver::whereIn('id', $onIds)->get();
//            $offline_drivers = Driver::whereIn('id', $offIds)->get();
//            $duplicate = DB::select('SELECT id FROM drivers INNER JOIN (SELECT latitude, longitude FROM drivers GROUP BY ROUND(latitude, 2), ROUND(longitude, 2) HAVING COUNT(*) > 1) dup ON drivers.latitude = dup.latitude AND drivers.longitude = dup.longitude');
//            $duplicate = array_pluck($duplicate, 'id');
            return view('backend.drivers.map', compact(/*'busy_drivers', 'offline_drivers',*/
                'online_drivers'/*, 'duplicate'*/));
        } else {
            $drivers = Driver::where('agent_id', $user->agent_id)->get();
            $online_drivers = Driver::where('online', 1)->where('agent_id', $user->agent_id)->get();
//            $busy_drivers = Driver::where('online', 2)->where('agent_id', $user->agent_id)->get();
//            $offline_drivers = Driver::where('online', 0)->where('agent_id', $user->agent_id)->get();
            return view('agent.drivers.track', compact('drivers', /*'busy_drivers', 'offline_drivers',*/ 'online_drivers'));
        }
    }

    // Get All Driver
    public function index(Request $request)
    {
        $user = Auth::guard($this->guardType)->user();

        if ($user->is_warehouse_user) {
            $drivers = Driver::with('manager')->join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                ->where('user_id', $user->id)
                ->orderBy('drivers.id', 'desc')
                ->select("drivers.*");
        } else {
            $drivers = Driver::with('manager')->orderBy('drivers.id', 'desc');
        }

        if ($user->role_id != 1) {
            $drivers = $drivers->where('agent_id', $user->agent_id);
        }

        // Check If Active Or Block Filter Or Search By Name
        $block = app('request')->input('block');
        $active = app('request')->input('active');
        $search = app('request')->input('search');
        $status = app('request')->input('status');
        $request_status = app('request')->input('request_status');
        $agent = app('request')->input('agent');
        $verify_mobile = app('request')->input('verify_mobile');
        $verify_email = app('request')->input('verify_email');

        if (isset($search) && $search != "") {
            $drivers = $drivers->where('name', 'like', '%' . $search . '%')->
            orWhere('mobile', 'like', '%' . $search . '%')->
            orWhere('national_id_number', 'like', '%' . $search . '%');
        }

        if (isset($active) && $active != -1) {
            if ($active == 0) { // Not Active
                $drivers = $drivers->where(function ($query) use ($active) {
                    $query->where('is_active', $active);
                    $query->orWhere('is_active', NULL);
                });
            } else { // Active
                $drivers = $drivers->where('is_active', $active);
            }
        }

        if (isset($block) && $block != -1) {
            if ($block == 0) { // Not Blocked
                $drivers = $drivers->whereNull('is_block');
            } else { // Blocked
                $drivers = $drivers->where('is_block', $block);
            }
        }

        if (isset($verify_mobile) && $verify_mobile != -1) {
            if ($verify_mobile == 1) {
                $drivers = $drivers->where('is_verify_mobile', 1);

            } else { // Blocked
                $drivers = $drivers->whereNull('is_verify_mobile');
            }
        }

        if (isset($verify_email) && $verify_email != -1) {
            if ($verify_email == 1) {
                $drivers = $drivers->where('is_verify_email', 1);

            } else { // Blocked
                $drivers = $drivers->whereNull('is_verify_email');
            }
        }


        if (isset($status) && $status != -1) {
            $onIds = [];
            if ($status != 0) {
                $onIds = Device::where('object_type', 2)->where('status', 1)->pluck('object_id')->toArray();
            } else {
                $onIds = Device::where('object_type', 2)->where('status', '!=', 1)->pluck('object_id')->toArray();

            }

            $drivers = $drivers->whereIn('id', $onIds);

        }

        //request_status
        if (isset($request_status) && $request_status != -1) {
            $onIds = [];
            if ($request_status != 0) {
                $onIds = Device::where('object_type', 2)->where('request_status', 1)->pluck('object_id')->toArray();
            } else {
                $onIds = Device::where('object_type', 2)->where('request_status', '!=', 1)->pluck('object_id')->toArray();

            }

            $drivers = $drivers->whereIn('id', $onIds);

        }

        if (isset($agent) && $agent != -1) {
            if ($agent == 1) {
                $drivers = $drivers->where('agent_id', 1);
            } else {
                $drivers = $drivers->where('agent_id', '!=', 1);
            }
        }

        $drivers = $drivers->paginate(15);

        if ($user->role_id == 1) {
            if ($request->ajax()) {
                return [
                    'view' => view('backend.drivers.table', compact('drivers'))->render(),
                ];
            }
            return view('backend.drivers.index', compact('drivers'));
        } else {
            return view('agent.drivers.index', compact('drivers'));
        }
    }

    // Added Driver Page
    public function create()
    {
        $user = Auth::guard($this->guardType)->user();
        $countries = Country::orderBy('name', 'ASC')->get();
        $agents = Agent::orderBy('name', 'desc')->get();
        $truck_types = VehicleType::orderBy('name_en', 'ASC')->get();
        $model_types = ModelType::orderBy('name', 'ASC')->get();
        $colors = Color::orderBy('name', 'ASC')->get();
        $governments = Governorate::orderBy('name_en', 'ASC')->get();
        $stores = Store::where('status', 1)->select('stores.*');
        $drivers = Driver::select('drivers.*')->where('is_active', 1)->where('is_manager', 1);

        if ($user->is_warehouse_user) {
            $stores = $stores->join('store_user', 'store_user.store_id', '=', 'stores.id')
                ->where('user_id', $user->id);

            $drivers = $drivers->join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                ->where('user_id', $user->id);
        }

        $drivers = $drivers->get();
        $stores = $stores->get();

        if ($user->role_id == '1') {

            return view('backend.drivers.create', compact('drivers', 'countries', 'agents', 'truck_types', 'model_types', 'colors', 'governments', 'stores', 'drivers'));
        } else {
            return view('agent.drivers.create', compact('drivers', 'countries', 'agents', 'model_types', 'colors', 'governments', 'drivers'));
        }
    }

    // Add New Driver
    public function store(Request $request)
    {
        $this->validate($request, [

            'name' => 'required',
            'email' => 'required|unique:drivers',
            'mobile' => 'required|unique:drivers',
            'password' => 'required|min:5',
            'confirm_password' => 'required_with:password|same:password|min:5',
            'address' => 'required',
//            'country_id' => 'required',
            'government_id' => 'required',
            'city_id' => 'required',
            'national_id_number' => 'required',
            'national_id_expired_date' => 'required',
            'basic_salary' => 'required',
            'bouns_of_delivery' => 'required',
            'pickup_price' => 'required',
            'recall_price' => 'required',
            'reject_price' => 'required',
            'bouns_of_pickup_for_one_order' => 'required',
            'driver_has_vehicle' => 'required',
            'driver_profile' => 'required|mimes:jpeg,png,jpg,gif',
            'national_id_image_front' => 'required|mimes:jpeg,png,jpg,gif',
            'criminal_record_image_front' => 'required|mimes:jpeg,png,jpg,gif',
//            'driving_licence_image_back' => 'mimes:jpeg,png,jpg,gif',
//            'national_id_image_back' => 'mimes:jpeg,png,jpg,gif',
            'ship_line' => 'required',
            'internal_government' => 'required_if:ship_line,1',
            'internal_city' => 'required_if:ship_line,1',
            'start_government' => 'required_if:ship_line,2',
            'destination_government' => 'required_if:ship_line,2',
            'shipping_types' => 'required|min: 1'
        ]);

        if ($request->input("driver_has_vehicle") == 1) {
            $this->validate($request, [
                /*   'model_name'                       => 'required',
                   'plate_number'                     => 'required',
                   'chassi_number'                    => 'required',
                   'year'                             => 'required',
                   */
                'vehicle_type_id' => 'required',
                // 'color_id'                         => 'required',
                'driving_licence_expired_date' => 'required',
                'license_end_date' => 'required',
                'driving_licence_image_front' => 'required|mimes:jpeg,png,jpg,gif',
                'license_image_front' => 'required|mimes:jpeg,png,jpg,gif',

            ]);
        }

        $user = Auth::guard($this->guardType)->user();
        DB::beginTransaction();
        try {
            $driver = new Driver();
            $driver->name = $request->input("name");
            $driver->mobile = $request->input("mobile");
            $driver->email = $request->input("email");
//            $driver->phone = $request->input("phone");
            $driver->password = $request->input("password");
            $driver->address = $request->input("address");
//            $driver->country_id = $request->input("country_id");
            $driver->government_id = $request->input("government_id");
            $driver->city_id = $request->input("city_id");

            $driver->ship_line = $request->input("ship_line");
            $driver->internal_government = $request->input("internal_government");
            $driver->internal_city = $request->input("internal_city");
            $driver->start_government = $request->input("start_government");
            $driver->destination_government = $request->input("destination_government");
            $driver->is_manager = $request->input("is_manager");
            $driver->manager_id = $request->input("is_manager") == 1 ? null : $request->input("manager_id");
            $driver->auto_close_invoice = $request->input("auto_close_invoice");

            $driver->transfer_method = $request->input("transfer_method");
            $driver->bank_name = $request->input("bank_name");
            $driver->bank_account = $request->input("bank_account");
            $driver->mobile_transfer = $request->input("mobile_transfer");

            $driver->agent_id = $request->input("agent_id");
            $driver->driving_licence_expired_date = $request->input("driving_licence_expired_date");
            $driver->national_id_number = $request->input("national_id_number");
            $driver->national_id_expired_date = $request->input("national_id_expired_date");
            //   $driver->version_id                   = '3';
            //     $driver->device_id                    = '0';
            // $driver->driving_licence_number       = $request->input("driving_licence_number");
            $driver->latitude = $request->input('latitude');
            $driver->longitude = $request->input('longitude');
            $driver->store_id = $request->input('store_id');
            if ($user->role_id == '1') {
                $driver->online = 0;
                $driver->is_active = $request->input("is_active");
            } else {
                $driver->status = 0;
                $driver->is_active = 0;

            }

            $driver->notes = $request->input("notes");
            $driver->driver_has_vehicle = $request->input("driver_has_vehicle");

            $driver->basic_salary = $request->input("basic_salary");
            $driver->bouns_of_delivery = $request->input("bouns_of_delivery");
            $driver->pickup_price = $request->input("pickup_price");
            $driver->recall_price = $request->input("recall_price");
            $driver->reject_price = $request->input("reject_price");
            $driver->bouns_of_pickup_for_one_order = $request->input("bouns_of_pickup_for_one_order");
            $driver->profit = $request->input("profit");

//            if ($request->input("agent_id") == 1) {
//                $driver->basic_salary = $request->input("basic_salary");
//                $driver->bouns_of_delivery = $request->input("bouns_of_delivery");
//                $driver->pickup_price = $request->input("pickup_price");
//                $driver->bouns_of_pickup_for_one_order = $request->input("bouns_of_pickup_for_one_order");
//                $driver->profit = $request->input("profit");
//            } else {
//                $driver->basic_salary = 0;
//                $driver->bouns_of_delivery = 0;
//                $driver->pickup_price = 0;
//                $driver->bouns_of_pickup_for_one_order = 0;
//                $driver->profit = 0;
//            }

            if ($request->input("agent_id") != "0") {
                $driver->agent_id = $request->input("agent_id");
            }

            $counter = 0;

            if ($request->file("driver_profile") != "") {
                $counter++;
                // $extension = File::extension($request->file("driver_profile")->getClientOriginalName());
                $image = 'driver-' . time() . $counter . "." . "jpg";
                $driver->image = $image;
                $request->file("driver_profile")->move(public_path('api_uploades/driver/profile/'), $image);
            }

            if ($request->file("criminal_record_image_front") != "") {
                $counter++;
                // $extension = File::extension($request->file("criminal_record_image_front")->getClientOriginalName());
                $image = 'driver-' . time() . $counter . "." . "jpg";
                $driver->criminal_record_image_front = $image;
                $request->file("criminal_record_image_front")->move(public_path('api_uploades/driver/cre_record/'), $image);
            }

            if ($request->file("driving_licence_image_front") != "") {
                $counter++;
                // $extension = File::extension($request->file("driving_licence_image_front")->getClientOriginalName());
                $image = 'dId-' . time() . $counter . "." . "jpg";
                $driver->driving_licence_image_front = $image;
                $request->file("driving_licence_image_front")->move(public_path('api_uploades/driver/driver_licence/'), $image);
            }


            if ($request->file("national_id_image_front") != "") {
                $counter++;
                // $extension = File::extension($request->file("national_id_image_front")->getClientOriginalName());
                $image = 'nId-' . time() . $counter . "." . "jpg";
                $driver->national_id_image_front = $image;
                $request->file("national_id_image_front")->move(public_path('api_uploades/driver/national_id_front/'), $image);
            }

            $driver->save();

            foreach ($request->shipping_types as $shipping_type) {
                ShipmentType::create(['driver_id' => $driver->id, 'shipping_type' => $shipping_type]);
            }

            // If This Driver Has vehicle Add Vehicle For He >> [$request->input("driver_has_vehicle") = 1]
            if ($request->input("driver_has_vehicle") == 1) {
                $truck = new Truck();
                $user = Auth::guard($this->guardType)->user();
                $truck->model_name = $request->input("model_name");
                $truck->plate_number = $request->input("plate_number");
                $truck->chassi_number = $request->input("chassi_number");
                $truck->year = $request->input("year");
                $truck->vehicle_type_id = $request->input("vehicle_type_id");
                $truck->model_type_id = $request->input("model_type_id");
                $truck->color_id = $request->input("color_id");
                $truck->driver_id = $driver->id;
                $truck->license_end_date = $request->input("license_end_date");
                // $truck->license_start_date    = $request->input("license_start_date");

                if ($user->role_id == '1') {
                    $truck->status = 1;
                } else {
                    $truck->status = 0;
                }

                $counter = 0;

                if ($request->file("image") != "") {
                    $counter++;
                    $extension = File::extension($request->file("image")->getClientOriginalName());
                    $image = 'truck-' . time() . $counter . "." . "jpg";
                    $truck->image = $image;
                    $request->file("image")->move(public_path('backend/images/'), $image);
                }

                if ($request->file("license_image_front") != "") {
                    $counter++;
                    $extension = File::extension($request->file("license_image_front")->getClientOriginalName());
                    $image = 'truck-' . time() . $counter . "." . "jpg";
                    $truck->license_image_front = $image;
                    $request->file("license_image_front")->move(public_path('api_uploades/driver/car_license_front/'), $image);
                }

                $truck->save();
            }
            DB::commit();

            // Function Add Action In Activity Log By Samir
            addActivityLog('Add Driver ' . $driver->name, ' اضافة الكابتن ' . $driver->name, 4); // Helper Function in App\Helpers.php

            $latestcustomer = Driver::latest()->first();


            // start email function
            Flash::success(__('backend.Driver_saved_successfully'));
            if ($user->role_id == '1') {
                return redirect()->route('mngrAdmin.drivers.index');
            } else {
                return redirect()->route('mngrAgent.drivers.index');
            }


        } catch (Exception $e) {
            DB::rollback();
            Flash::error(__('backend.Corporate_not_Saved_Something_Error'));
            if ($user->role_id == '1') {
                return redirect()->route('mngrAdmin.drivers.index');
            } else {
                return redirect()->route('mngrAgent.drivers.index');
            }
        }

    }


    // View One Driver
    public function show($id)
    {
        $driver = Driver::findOrFail($id);
        $truck = Truck::where('driver_id', $driver->id)->first();
        $user = Auth::guard($this->guardType)->user();
        if ($user->role_id == '1') {
            return view('backend.drivers.show', compact('driver'));
        } else {
            return view('agent.drivers.show', compact('driver'));
        }
        return view('backend.drivers.show', compact('driver'));
    }

    // Edit Driver Page
    public function edit($id)
    {
        $driver = Driver::findOrFail($id);
        $user = Auth::guard($this->guardType)->user();
        $countries = Country::orderBy('name', 'ASC')->get();
        $truck_types = VehicleType::orderBy('name_en', 'ASC')->get();
        $model_types = ModelType::orderBy('name', 'ASC')->get();
        $colors = Color::orderBy('name', 'ASC')->get();
        $trucks = Truck::orderBy('id', 'ASC')->where('driver_id', $id)->get();
        $governments = Governorate::orderBy('name_en', 'ASC')->get();
        $cities = City::where('governorate_id', $driver->government_id)->orderBy('id', 'ASC')->get();
        $shipping_types = ShipmentType::where('driver_id', $id)->pluck('shipping_type')->toArray();
        $stores = Store::where('status', 1)->select('stores.*');
        $drivers = Driver::select('drivers.*')->where('is_active', 1)->where('is_manager', 1);

        if ($user->is_warehouse_user) {
            $stores = $stores->join('store_user', 'store_user.store_id', '=', 'stores.id')
                ->where('user_id', $user->id);

            $drivers = $drivers->join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                ->where('user_id', $user->id);
        }

        $drivers = $drivers->get();
        $stores = $stores->get();

        //dd($driver->truck);die();
        if (isset($city)) {
            $driver->country_id = $city->country_id;
        }
        $agents = Agent::orderBy('name', 'ASC')->get();
        if ($user->role_id == '1') {
            return view('backend.drivers.edit', compact('driver', 'countries', 'agents', 'truck_types', 'model_types', 'colors', 'trucks', 'governments', 'cities', 'shipping_types', 'stores', 'drivers'));
        } else {
            return view('agent.drivers.edit', compact('driver', 'countries', 'agents', 'drivers'));
        }
    }

    // Edit Driver
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:drivers,email,' . $id,
            'mobile' => 'required|unique:drivers,mobile,' . $id,
            // 'address'                         => 'required',
//            'country_id' => 'required',
            'government_id' => 'required',
            'city_id' => 'required',
            // 'national_id_number'              => 'required',
            //'national_id_expired_date'        => 'required',
            'driver_profile' => 'required_without:driver_profile_old|mimes:jpeg,png,jpg,gif',
            'criminal_record_image_front' => 'required_without:criminal_record_image_front_old|mimes:jpeg,png,jpg,gif',
            'national_id_image_front' => 'required_without:national_id_image_front_old|mimes:jpeg,png,jpg,gif',
            'basic_salary' => 'required',
            'bouns_of_delivery' => 'required',
            'pickup_price' => 'required',
            'recall_price' => 'required',
            'reject_price' => 'required',
            'bouns_of_pickup_for_one_order' => 'required',
            'driver_has_vehicle' => 'required',
            'shipping_types' => 'required|min: 1',
        ]);

        if ($request->input("driver_has_vehicle") == 1) {
            $this->validate($request, [
                /* 'model_name'                       => 'required',
                 'plate_number'                     => 'required',
                 'chassi_number'                    => 'required',
                 'year'                             => 'required',
                 'vehicle_type_id'                  => 'required',
                 'color_id'                         => 'required',
                 'driving_licence_expired_date'     => 'required',
                 'license_end_date'                 => 'required',
                 */
//                'driving_licence_image_front' => 'mimes:jpeg,png,jpg,gif',
//                'license_image_front' => 'mimes:jpeg,png,jpg,gif',

                'vehicle_type_id' => 'required',
                // 'color_id'                         => 'required',
                'driving_licence_expired_date' => 'required',
                'license_end_date' => 'required',
                'driving_licence_image_front' => 'required_without:driving_licence_image_front_old|mimes:jpeg,png,jpg,gif',
                'license_image_front' => 'required_without:license_image_front_old|mimes:jpeg,png,jpg,gif',

            ]);
        }

        $driver = Driver::findOrFail($id);
        $driver->name = $request->input("name");
        $driver->mobile = $request->input("mobile");
        $driver->email = $request->input("email");
//        $driver->phone = $request->input("phone");

        if ($request->input("password") != "") {
            $driver->password = $request->input("password");
        }

        $driver->address = $request->input("address");
//        $driver->country_id = $request->input("country_id");
        $driver->government_id = $request->input("government_id");
        $driver->city_id = $request->input("city_id");
        $driver->driving_licence_number = $request->input("driving_licence_number");
        $driver->driving_licence_expired_date = $request->input("driving_licence_expired_date");
        $driver->national_id_number = $request->input("national_id_number");
        $driver->national_id_expired_date = $request->input("national_id_expired_date");
        $driver->online = 0;
        $driver->is_active = $request->input("is_active");
        $driver->notes = $request->input("notes");
        $driver->is_manager = $request->input("is_manager");
        $driver->manager_id = $request->input("is_manager") == 1 ? null : $request->input("manager_id");
        $driver->auto_close_invoice = $request->input("auto_close_invoice");

        $driver->transfer_method = $request->input("transfer_method");
        $driver->bank_name = $request->input("bank_name");
        $driver->bank_account = $request->input("bank_account");
        $driver->mobile_transfer = $request->input("mobile_transfer");

        $driver->basic_salary = $request->input("basic_salary");
        $driver->bouns_of_delivery = $request->input("bouns_of_delivery");
        $driver->pickup_price = $request->input("pickup_price");
        $driver->recall_price = $request->input("recall_price");
        $driver->reject_price = $request->input("reject_price");
        $driver->bouns_of_pickup_for_one_order = $request->input("bouns_of_pickup_for_one_order");
        $driver->profit = $request->input("profit");
        $driver->latitude = $request->input('latitude');
        $driver->longitude = $request->input('longitude');
        $driver->store_id = $request->input('store_id');
//        if ($request->input("agent_id") == 1) {
//            $driver->basic_salary = $request->input("basic_salary");
//            $driver->bouns_of_delivery = $request->input("bouns_of_delivery");
//            $driver->pickup_price = $request->input("pickup_price");
//            $driver->bouns_of_pickup_for_one_order = $request->input("bouns_of_pickup_for_one_order");
//            $driver->profit = $request->input("profit");
//        } else {
//            $driver->basic_salary = $request->input("old_basic_salary");
//            $driver->bouns_of_delivery = $request->input("old_bouns_of_delivery");
//            $driver->pickup_price = $request->input("old_pickup_price");
//            $driver->bouns_of_pickup_for_one_order = $request->input("old_bouns_of_pickup_for_one_order");
//            $driver->profit = $request->input("old_profit");
//        }

        $driver->driver_has_vehicle = $request->input("driver_has_vehicle");

        if ($request->input("agent_id")) {
            $driver->agent_id = $request->input("agent_id");
        }


        if (!$request->driver_profile_old) {
            $driver->image = '';
        }

        if (!$request->criminal_record_image_front_old) {
            $driver->criminal_record_image_front = '';
        }

        if (!$request->national_id_image_front_old) {
            $driver->national_id_image_front = '';
        }

        if ($request->file("driver_profile") != "") {
            $extension = File::extension($request->file("driver_profile")->getClientOriginalName());
            $image = 'driver-' . time() . "." . "jpg";
            $driver->image = $image;
            $request->file("driver_profile")->move(public_path('api_uploades/driver/profile/'), $image);
        } elseif (!$request->input("driver_profile_old")) {
            flash(__('backend.Please_Enter_Driver_Profile'))->error();
            return redirect()->back();
        }

        if ($request->file("criminal_record_image_front") != "") {
            $extension = File::extension($request->file("criminal_record_image_front")->getClientOriginalName());
            $image = 'driver-' . time() . "." . "jpg";
            $driver->criminal_record_image_front = $image;
            $request->file("criminal_record_image_front")->move(public_path('api_uploades/driver/cre_record/'), $image);
        } elseif (!$request->input("criminal_record_image_front_old")) {
            flash(__('backend.Please_Enter_Criminal_Driver_Image'))->error();
            return redirect()->back();
        }

        if ($request->file("national_id_image_front") != "") {
            $extension = File::extension($request->file("national_id_image_front")->getClientOriginalName());
            $image = 'nId-' . time() . "." . "jpg";
            $driver->national_id_image_front = $image;
            $request->file("national_id_image_front")->move(public_path('api_uploades/driver/national_id_front/'), $image);
        } elseif (!$request->input("national_id_image_front_old")) {
            flash(__('backend.Please_Enter_National_Id_Image_Front'))->error();
            return redirect()->back();
        }


        if ($request->input("driver_has_vehicle") == 1) {
            if ($driver->truck) { // If This Driver Already Has a Vehicle Update This Vehicle
                $truck = Truck::findOrFail($request->input("vehicle_id"));
            } else { // If This Driver Not Has a Vehicle Added New Vehicle
                $truck = new Truck();
            }

            $user = Auth::guard($this->guardType)->user();
            $truck->model_name = $request->input("model_name");
            $truck->plate_number = $request->input("plate_number");
            $truck->chassi_number = $request->input("chassi_number");
            $truck->year = $request->input("year");
            $truck->vehicle_type_id = $request->input("vehicle_type_id");
            $truck->model_type_id = $request->input("model_type_id");
            $truck->color_id = $request->input("color_id");
            $truck->driver_id = $driver->id;
            $truck->license_end_date = $request->input("license_end_date");

            if ($user->role_id == '1') {
                $truck->status = 1;
            } else {
                $truck->status = 0;
            }

            $counter = 0;

            // if ($request->file("image") != "") {
            //     $counter++;
            //     $extension                      = File::extension($request->file("image")->getClientOriginalName());
            //     $image                          = 'truck-' . time() . $counter . "." . "jpg";
            //     $truck->image                   = $image;
            //     $request->file("image")->move(public_path('backend/images/'), $image);
            // }

            if (!$request->driving_licence_image_front_old) {
                $driver->driving_licence_image_front = '';
            }

            if (!$request->license_image_front_old) {
                $truck->license_image_front = '';
            }

            if ($request->file("driving_licence_image_front") != "") {
                $extension = File::extension($request->file("driving_licence_image_front")->getClientOriginalName());
                $image = 'dId-' . time() . "." . "jpg";
                $driver->driving_licence_image_front = $image;
                $request->file("driving_licence_image_front")->move(public_path('api_uploades/driver/driver_licence/'), $image);
            } elseif (!$request->input("driving_licence_image_front_old")) {
                flash(__('backend.Please_Enter_Driving_Licence_Image_Front'))->error();
                return redirect()->back();
            }


            if ($request->file("license_image_front") != "") {
                $counter++;
                $extension = File::extension($request->file("license_image_front")->getClientOriginalName());
                $image = 'truck-' . time() . $counter . "." . "jpg";
                $truck->license_image_front = $image;
                $request->file("license_image_front")->move(public_path('api_uploades/driver/car_license_front/'), $image);
            }

            $truck->save();

        } else { // If [$request->input("driver_has_vehicle") = 0] >> Delete Vehicle

            if ($driver->truck) {
                $truck = Truck::findOrFail($request->input("vehicle_id"));
                $truck->delete();
            }

        }

        ShipmentType::where('driver_id', $id)->delete();
        foreach ($request->shipping_types as $shipping_type) {
            ShipmentType::create(['driver_id' => $id, 'shipping_type' => $shipping_type]);
        }

        $driver->save();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Edit Driver ' . $driver->name, ' تعديل الكابتن ' . $driver->name, 4); // Helper Function in App\Helpers.php

        Flash::warning(__('backend.Driver_Updated_successfully'));
        $user = Auth::guard($this->guardType)->user();

        if ($user->role_id == '1') {
            return redirect()->route('mngrAdmin.drivers.index');
        } else {
            return redirect()->route('mngrAgent.drivers.index');
        }

        return redirect()->route('mngrAdmin.drivers.index');
    }

    // Delete Driver
    public function destroy($id)
    {
        $driver = Driver::findOrFail($id);
        $driver->delete();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Delete Driver ' . $driver->name, ' حذف الكابتن ' . $driver->name, 4); // Helper Function in App\Helpers.php

        flash(__('backend.Driver_deleted_successfully'))->error();
        $user = Auth::guard($this->guardType)->user();
        if ($user->role_id == '1') {
            return redirect()->route('mngrAdmin.drivers.index');
        } else {
            return redirect()->route('mngrAgent.drivers.index');
        }

        return redirect()->route('mngrAdmin.drivers.index');
    }

    // Blocke And Unblocke Driver
    function block($id)
    {
        /*
        * is_block = 0 :: Not Blocked
        * is_block = 1 :: Blocked
        */
        $driver = Driver::findOrFail($id);
        if (!empty($driver)) {
            if ($driver->is_block == 0) {
                $driver->is_block = 1;

                // Function Add Action In Activity Log By Samir
                addActivityLog('Blocked Driver ' . $driver->name, ' حظر الكابتن ' . $driver->name, 4); // Helper Function in App\Helpers.php

            } else {
                $driver->is_block = 0;

                // Function Add Action In Activity Log By Samir
                addActivityLog('Unblock Driver ' . $driver->name, ' الغاء حظر الكابتن ' . $driver->name, 4); // Helper Function in App\Helpers.php
            }
            $driver->save();
            return $driver->is_block;
        }
    }

    // Active And Unactive Driver
    function active($id)
    {
        /*
        * is_active = 0 :: Not Active
        * is_active = 1 :: Active
        */
        $driver = Driver::findOrFail($id);
        if (!empty($driver)) {
            if ($driver->is_active == 0) {
                $driver->is_active = 1;

                // Function Add Action In Activity Log By Samir
                addActivityLog('Active Driver ' . $driver->name, ' تفعيل الكابتن ' . $driver->name, 4); // Helper Function in App\Helpers.php

            } else {
                $driver->is_active = 0;

                // Function Add Action In Activity Log By Samir
                addActivityLog('Unactive Driver ' . $driver->name, ' الغاء تفعيل الكابتن ' . $driver->name, 4); // Helper Function in App\Helpers.php

            }
            $driver->save();
            return $driver->is_active;
        }
    }

    // Delete Driver
    public function del($id)
    {
        $aorder = AcceptedOrder::where('driver_id', $id)->first();
        $trucks = Truck::where('driver_id', $id)->first();
        $order_logs = OrderLog::where('driver_id', $id)->first();
        $user = Auth::guard($this->guardType)->user();

        if ($user->role_id == '1') {
            if ($aorder != null || $trucks != null || $order_logs != null) { // If This Driver Have Any >> Orders & Trucks & Orderslog == Not Deleted Driver
                flash(__('backend.Driver_not_delete_Delete_trucks_or_his_orders_before'))->error();
                return back();
            } else { // If This Driver Not Have Any >> Orders & Trucks & Orderslog == Deleted Driver
                $driver = Driver::findOrFail($id);
                $driver->delete();

                // Function Add Action In Activity Log By Samir
                addActivityLog('Delete Driver ' . $driver->name, ' حذف الكابتن ' . $driver->name, 4); // Helper Function in App\Helpers.php

                flash(__('backend.Driver_deleted_successfully'))->error();
                return redirect()->route('mngrAdmin.drivers.index');
            }
        } else {
            if (empty($aorder) && empty($trucks) && empty($order_logs)) {
                flash(__('backend.Driver_not_delete_Delete_trucks_or_his_orders_before'))->error();
                return back();
            } else {
                $driver = Driver::findOrFail($id);
                $driver->delete();

                // Function Add Action In Activity Log By Samir
                addActivityLog('Delete Driver ' . $driver->name, ' حذف الكابتن ' . $driver->name, 4); // Helper Function in App\Helpers.php

                flash(__('backend.Driver_deleted_successfully'))->error();
                return redirect()->route('mngrAgent.drivers.index');
            }
        }
    }

    // Get Agents By Driver
    public function getdriver_by_agents($id)
    {
        $drivers = [];
        if ($id > 0) {
            $drivers = Driver::where('agent_id', $id)->where('is_active', 1)->get();
        }
        return $drivers;
    }

    public function checkHasVehicle($id)
    {

        $driver = Driver::findOrFail($id);
        if ($driver && !empty($driver->truck) && count($driver->truck) > 0) {
            $msg = 1;
        } else {
            $msg = 0;
        }
        return $msg;
    }

    //verify_mobile_customer
    function change_verify_mobile(Request $request)
    {
        $id = $request->id;
        $driver = Driver::findOrFail($id);
        if ($driver != null) {
            if ($driver->is_verify_mobile == 0) {
                $driver->is_verify_mobile = 1;
            } else {
                $driver->is_verify_mobile = 0;
            }
            $driver->save();

            return $driver->is_verify_mobile;
        } else {
            return 'error';
        }

    }


    function change_verify_account(Request $request)
    {
        $id = $request->id;
        $driver = Driver::findOrFail($id);
        if ($driver != null) {
            if ($driver->is_active == 0) {
                $driver->is_active = 1;
            } else {
                $driver->is_active = 0;
            }
            $driver->save();

            return $driver->is_active;
        } else {
            return 'error';
        }

    }


    function change_block(Request $request)
    {
        $id = $request->id;
        $driver = Driver::findOrFail($id);
        if ($driver != null) {
            if ($driver->is_block == 0) {
                $driver->is_block = 1;
            } else {
                $driver->is_block = 0;
            }
            $driver->save();

            return $driver->is_block;
        } else {
            return 'error';
        }

    }

    function change_verify_email(Request $request)
    {
        $id = $request->id;
        $driver = Driver::findOrFail($id);
        if ($driver != null) {
            if ($driver->is_verify_email == 0) {
                $driver->is_verify_email = 1;
            } else {
                $driver->is_verify_email = 0;
            }
            $driver->save();

            return $driver->is_verify_email;
        } else {
            return 'error';
        }

    }


    function check_driver_email()
    {
        $email = \Request::get("email");
        $driver = Driver::where('email', $email)->first();
        if ($driver) {
            echo 'false';
        } else {
            echo 'true';
        }
    }

    function check_driver_mobile()
    {
        $mobile = \Request::get("mobile");
        $driver = Driver::where('mobile', $mobile)->first();
        if ($driver) {
            echo 'false';
        } else {
            echo 'true';
        }
    }

    function check_driver_phone()
    {
        $phone = \Request::get("phone");
        $driver = Driver::where('phone', $phone)->first();
        if ($driver) {
            echo 'false';
        } else {
            echo 'true';
        }
    }

    function download_papers($id, Request $request)
    {
        $driver = \DB::table('drivers')->where('id', $id)->first();

        $files = [];

        $image = public_path("/api_uploades/driver/profile/" . $driver->image);
        if ($driver->image && \File::exists($image) && \File::size($image) > 0) {
            $files['profile'] = $image;
        }

        $criminal_record_image_front = public_path("/api_uploades/driver/cre_record/" . $driver->criminal_record_image_front);
        if ($driver->criminal_record_image_front && \File::exists($criminal_record_image_front) && \File::size($criminal_record_image_front) > 0) {
            $files['cre_record'] = $criminal_record_image_front;
        }

        $driving_licence_image_front = public_path("/api_uploades/driver/driver_licence/" . $driver->driving_licence_image_front);
        if ($driver->driving_licence_image_front && \File::exists($driving_licence_image_front) && \File::size($driving_licence_image_front) > 0) {
            $files['driver_licence'] = $driving_licence_image_front;
        }

        $national_id_image_front = public_path("/api_uploades/driver/national_id_front/" . $driver->national_id_image_front);
        if ($driver->national_id_image_front && \File::exists($national_id_image_front) && \File::size($national_id_image_front) > 0) {
            $files['national_id_front'] = $national_id_image_front;
        }

        $trucks = \DB::table('trucks')->where('driver_id', $id)->get();
        if(count($trucks)){
            foreach ($trucks as $truck){
                $car_license_front = public_path("/api_uploades/driver/car_license_front/" . $truck->license_image_front);
                if ($truck->license_image_front && \File::exists($car_license_front) && \File::size($car_license_front) > 0) {
                    $files['car_license_front'] = $car_license_front;
                }
            }
        }

        if (count($files)) {
            $file = public_path('api_uploades/driver/papers/driver_papers_' . $driver->id . '.zip');

            $zipper = \Zipper::make($file);
            foreach ($files as $key => $value) {
                $zipper->add($value, $key);
            }
            $zipper->close();

            return response()->download($file, 'driver_papers_' . $driver->id . '.zip');
        }

    }
}

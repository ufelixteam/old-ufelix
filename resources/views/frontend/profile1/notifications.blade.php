@extends('frontend.profile.layouts.app')

@section('title') @lang('website.notifications') @endsection

@section('content')

    <div class="top-bar clearfix">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="page-title">
                    <h3>@lang('website.notifications')</h3>
                    <hr>
                </div>
            </div>

        </div>
        <div class="row">

            <div class="all-not">
                @if(! empty($notifications) )
                    @foreach($notifications as $notification)
                        <div class="col-12">
                            <div class="not-block">
                                <div class="day">
                                    <i class="icon-calendar2"></i><span>{{Carbon\Carbon::parse($notification->created_at)->toFormattedDateString()}}</span>
                                    <span class="time"><i class="icon-clock"></i> {{Carbon\Carbon::parse($notification->created_at)->format('g:i:s a')}}</span>
                                </div>
                                <div class="details-not">
                                    <span class="done-or">{{$notification->title}}</span>
                                    <p>{{$notification->message}}</p>
                                </div>
                                <div class="remove-s">
                                    <a href="{{ app()->getLocale() == 'en'  ? route('profile.notifications.delete', ['locale' =>'en','id' => $notification->id] )  : url('/customer/account/notifications/delete?id='.$notification->id ) }}"
                                       class="delete"><i class="icon-trashcan"></i></a>
                                    <a href="{{ app()->getLocale() == 'en'  ? route('profile.orders', ['locale' =>'en','id' => $notification->object_id] )  : url('/customer/account/orders?id='.$notification->object_id ) }}"
                                       class="view"><i class="icon-eye"></i></a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif

            </div>

        </div>
    </div>
@endsection

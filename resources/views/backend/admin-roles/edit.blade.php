@extends('backend.layouts.app')

@section('css')
    <title>{{__('backend.Edit_Roles')}} - {{$roleAdmin->name}}</title>
@endsection

@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i> {{__('backend.Edit_Roles')}} / {{$roleAdmin->name}}</h3>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('mngrAdmin.admin_roles.update', $roleAdmin->id) }}" method="POST"
                  name="validateForm">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('name')) has-error @endif">
                    <label for="name-field">{{__('backend.role_name')}}</label>
                    <input type="text" id="name-field" name="name" class="form-control"
                           value="{{ is_null(old("name")) ? $roleAdmin->name : old("name") }}"/>
                    @if($errors->has("name"))
                        <span class="help-block">{{ $errors->first("name") }}</span>
                    @endif
                </div>
                <div class="form-group">
                    <input type="checkbox" class="btn-lg" name="is_warehouse_role" value="1"
                        @if($roleAdmin->is_warehouse_role) checked @endif>
                    <label>{{__('backend.warehouse_role')}}</label>
                </div>
                <br>
                <h2 style="display: inline-block"><i
                        class="fa fa-check-square"></i> {{__('backend.Choose_Permissions')}} </h2>
                <a style="display: inline-block;margin: 0 10px; vertical-align: super" href="#"
                   class="btn btn-xs btn-primary" id="selectAll"><i
                        class="fa fa-check"></i> {{__('backend.select_all')}}</a>
                <br>
                <div class="form-group">
                    @foreach($permissionType as $type)
                        <div class="col-md-3">
                            <h4><b> {{$type->name}} </b></h4>
                            <div class="box box-primary box-body" style="min-height: 320px;">
                                <ul style="list-style: none; padding: 10px 5px;">
                                    @foreach($permissions as $permission)
                                        @if($permission->type_permission_id == $type->id)
                                            <li>
                                                <input type="checkbox" class="btn-lg" name="permation[]"
                                                       value="{{$permission->id}}"
                                                       @foreach($permission_admin as $per_adm) @if($per_adm->permission_id == $permission->id) checked @endif @endforeach>
                                                <label>{{$permission->name}}</label>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="well well-sm col-md-12">
                    <button type="submit" class="btn btn-primary">{{__('backend.save_edits')}}</button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.admin_roles.index') }}">
                        <i class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(function () {
            $('#selectAll').click(function (e) {
                e.preventDefault();
                $('input[type="checkbox"]').prop('checked', true)
            })

            $("form[name='validateForm']").validate({

                // Specify validation rules
                rules: {
                    name: {
                        required: true,
                    },
                    permation: {
                        required: true,
                    },
                },

                // Specify validation error messages
                messages: {
                    name: {
                        required: "{{__('backend.Please_Enter_Role_Name')}}",
                    },
                    permation: {
                        required: "{{__('backend.Please_Selected_Permission_Administrator')}}",
                    },
                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                },

                highlight: function (element) {
                    $(element).parent().addClass('error')
                },

                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                },
            });
        })
    </script>
@endsection

@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.the_orders')}}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <style type="text/css">
        @media only screen and (max-width: 580px) {
            .hide-td {
                display: none;
            }
        }

        .box-body {
            min-height: 100px;
        }

        .stylish-input-group .input-group-addon {
            background: white !important;
        }

        .stylish-input-group .form-control {
            border-right: 0;
            box-shadow: 0 0 0;
            border-color: #ccc;
        }

        .stylish-input-group button {
            border: 0;
            background: transparent;
        }

        .toggle.ios, .toggle-on.ios, .toggle-off.ios {
            border-radius: 20px;
        }

        .toggle.ios .toggle-handle {
            border-radius: 20px;
        }
    </style>
@endsection

@section('header')
    <div class="page-header clearfix">
        <h3 style="display: inline-block"><i class="glyphicon glyphicon-align-justify"></i> Orders </h3>
    </div>
@endsection
@section('content')

    @include('error')

    <div class="alert alert-danger" hidden>
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong id="errorId"></strong>
    </div>

    <div class='list'>
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                @if(! empty($orders_errors) && count($orders_errors) > 0)
                    <form method="post" id="myform" action="{{ url('/mngrAdmin/reports/handle_sheet') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="customer_id" value="{{ $customer_id }}">
                        <input type="hidden" name="collection_id" value="{{ $collection->id }}">
                        <input type="hidden" name="store_id" value="{{ $store_id }}">
                        <input type="hidden" name="collection_id" value="{{ $collection->id }}">
                        <div class="table-responsive">
                            <table id="theTable" class="table table-condensed table-striped text-center"
                                   style="min-height: 200px;">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>رقم الشحنة</th>
                                    <th>رقم الرسل الفرعي</th>
                                    <th>الاسم</th>
                                    <th>رقم التليفون</th>
                                    <th>رقم تليفون آخر</th>
                                    <th>العنوان</th>
                                    <th>المحافظة</th>
                                    <th>محافظة الشحنة</th>
                                    <th>الاجمالى</th>
                                    <th>الطلب</th>
                                    <th>نوع التسليم</th>
                                    <th>ملاحظات</th>
                                    {{--                                <th>الأخطاء</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders_errors as $i => $order)
                                    <tr>
                                        <td>{{($i+1)}} <input type="hidden" name="orders[{{$i}}][no.]" value="{{($i+1)}}"></td>
                                        <td><input type="text" value="{{$order['rkm_alshhn']}}"
                                                   name="orders[{{$i}}][rkm_alshhn]" class="form-control"></td>
                                        <td><input type="text" value="{{$order['rkm_alrasl_alfraay']}}"
                                                   name="orders[{{$i}}][rkm_alrasl_alfraay]" class="form-control"></td>
                                        <td><input type="text" value="{{$order['alasm']}}" name="orders[{{$i}}][alasm]"
                                                   class="form-control"></td>
                                        <td><input type="text" value="{{$order['rkm_altlyfon']}}"
                                                   name="orders[{{$i}}][rkm_altlyfon]" class="form-control"></td>
                                        <td><input type="text" value="{{$order['rkm_tlyfon_aakhr']}}"
                                                   name="orders[{{$i}}][rkm_tlyfon_aakhr]" class="form-control">
                                        </td>
                                        <td><input type="text" value="{{$order['alaanoan']}}"
                                                   name="orders[{{$i}}][alaanoan]"
                                                   class="form-control"></td>
                                        <td><select name="orders[{{$i}}][almhafth]" class="form-control">
                                                <option value=""
                                                        data-display="Select">{{__('backend.government')}}</option>
                                                @if(! empty($app_governments))
                                                    @foreach($app_governments as $government)
                                                        <option value="{{$government->id}}"
                                                                @if($order['almhafth'] == $government->id) selected @endif>
                                                            {{$government->name_en}}
                                                        </option>

                                                    @endforeach
                                                @endif

                                            </select></td>
                                        <td>
                                            {{$order['government']}}
                                            <input type="hidden" name="orders[{{$i}}][government]"
                                                   value="{{$order['government']}}">
                                        </td>
                                        <td><input type="number" min="0" step=".01" value="{{$order['alajmal']}}"
                                                   name="orders[{{$i}}][alajmal]"
                                                   class="form-control"></td>
                                        <td><input type="text" value="{{$order['altlb']}}" name="orders[{{$i}}][altlb]"
                                                   class="form-control"></td>
                                        <td><input type="text" value="{{$order['noaa_altslym']}}"
                                                   name="orders[{{$i}}][noaa_altslym]" class="form-control"></td>
                                        <td><textarea class="form-control" rows="3" style="resize: vertical"
                                                      name="orders[{{$i}}][mlahthat]">{{$order['mlahthat']}}</textarea>
                                        </td>
                                        {{--                                    <td>{{$order['validation_errors']}}</td>--}}
                                    </tr>
                                    <tr>
                                        <td colspan="13" class="text-center"
                                            style="color: red">
                                            {{$order['validation_errors']}}
                                            <input type="hidden" name="orders[{{$i}}][validation_errors]"
                                                   value="{{$order['validation_errors']}}">
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                        <button type="button"
                                class="btn btn-primary btn-block" id="save"> {{__('backend.save')}} </button>
                        <button type="button"
                                class="btn btn-success btn-block" id="excel"> {{__('backend.excel')}} </button>
                        <a href="{{route('mngrAdmin.collection_orders.index', ['type_id' => 1])}}"
                           class="btn btn-warning btn-block" id="done"> {{__('backend.done')}} </a>
                    </form>
                @else
                    <h3 class="text-center alert alert-warning">No Result Found !</h3>
                @endif
            </div>
        </div>

    </div>


@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $("#save").click(function (e) {
                e.preventDefault();
                $("#myform").attr('action', "{{ url('/mngrAdmin/reports/handle_sheet') }}");
                $("#myform").submit();
            });

            $("#excel").click(function (e) {
                e.preventDefault();
                $("#myform").attr('action', "{{ url('/mngrAdmin/reports/export_sheet') }}");
                $("#myform").submit();
            });
        });
    </script>
@endsection

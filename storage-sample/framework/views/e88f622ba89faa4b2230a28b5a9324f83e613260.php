<!DOCTYPE html>
<html lang="en">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width,initial-scale=1">
      <meta name="author" content="Ufelix Team">
      <link rel="shortcut icon" type="image/x-icon" href="<?php echo e(asset('/website/images/flogo.png')); ?>" />

      <title><?php echo $__env->yieldContent('title'); ?></title>
      <?php echo $__env->make('frontend.profile.layouts.css', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
      <?php echo $__env->yieldContent('css'); ?>

    </head>
    <body>
      <?php echo $__env->make('frontend.profile.layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
      <?php echo $__env->make('frontend.profile.layouts.sidemenu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <!-- Start DashBoard -->
    <div class="dashboard-wrapper dashboard-wrapper-lg">
    	<div class="container-fluid">
    			<?php echo $__env->yieldContent('content'); ?>
      </div>
    </div>
    <!-- End DashBoard -->
		
    <?php echo $__env->make('frontend.profile.layouts.js', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <?php echo $__env->yieldContent('scripts'); ?>
	
</body>
</html>
<?php /**PATH /home/www/ufelix/ufelix/resources/views/frontend/profile/layouts/app.blade.php ENDPATH**/ ?>
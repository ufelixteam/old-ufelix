<?php $__env->startSection('css'); ?>
  <title><?php echo e(__('backend.the_orders')); ?> - <?php echo e(__('backend.edit')); ?></title>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('header'); ?>
  <div class="page-header">
    <h3><i class="glyphicon glyphicon-edit"></i>  <?php echo e(__('backend.Edit_Order')); ?> #<?php echo e($order->id); ?></h3>
  </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  <?php echo $__env->make('error', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
      <form action="<?php echo e(route('mngrAdmin.orders.update', $order->id)); ?>" method="POST">
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">


          <div class="col-md-12 col-sm-12 col-xs-12" style="border: 2px solid #ea2;margin-bottom: 15px;">
              <h4 style="color: #ea2;font-weight: bold">Receiver Data</h4>

              <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('receiver_name')): ?> has-error <?php endif; ?>">
                  <label for="receiver_name-field"> Name: </label>
                  <input type="text" id="receiver_name-field" name="receiver_name" class="form-control" value="<?php echo e(! empty($order) ? $order->receiver_name : old("receiver_name")); ?>"/>
                  <?php if($errors->has("receiver_name")): ?>
                      <span class="help-block"><?php echo e($errors->first("receiver_name")); ?></span>
                  <?php endif; ?>
              </div>
              <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('receiver_mobile')): ?> has-error <?php endif; ?>">
                  <label for="reciever_mobile-field"> Mobile: </label>
                  <input type="text" id="receiver_mobile-field" name="receiver_mobile" class="form-control" value="<?php echo e(! empty($order) ? $order->receiver_mobile : old("receiver_mobile")); ?>"/>
                  <?php if($errors->has("receiver_mobile")): ?>
                      <span class="help-block"><?php echo e($errors->first("receiver_mobile")); ?></span>
                  <?php endif; ?>
              </div>
              <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('receiver_phone')): ?> has-error <?php endif; ?>">
                  <label for="receiver_phone-field"> Mobile 2:</label>
                  <input type="text" id="receiver_phone-field" name="receiver_phone" class="form-control" value="<?php echo e(! empty($order) ? $order->receiver_phone : old("receiver_phone")); ?>"/>
                  <?php if($errors->has("receiver_phone")): ?>
                      <span class="help-block"><?php echo e($errors->first("receiver_phone")); ?></span>
                  <?php endif; ?>
              </div>

              <div class="form-group col-md-3 col-sm-3">
                  <label class="control-label" for="r_government_id"> Government</label>

                  <select  class="form-control r_government_id" id="r_government_id"  name="r_government_id">
                      <option value="" data-display="Select">Receiver Government</option>
                      <?php if(! empty($governments)): ?>
                          <?php $__currentLoopData = $governments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $government): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($government->id); ?>"  <?php echo e(! empty($order) && $order->r_government_id  == $government->id ? 'selected':  ''); ?> >
                                  <?php echo e($government->name_en); ?>

                              </option>

                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php endif; ?>

                  </select>

              </div>

              <div class="form-group col-md-2 col-sm-2">
                  <label class="control-label" for="r_state_id"> City</label>

                  <select  class=" form-control r_state_id" id="r_state_id"  name="r_state_id" >
                      <?php if(! empty($r_cities)): ?>

                          <?php $__currentLoopData = $r_cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r_city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($r_city->id); ?>" <?php echo e(! empty($order) && $order->r_state_id  == $r_city->id ? 'selected':  ''); ?> >
                                  <?php echo e($r_city->name_en); ?>

                              </option>

                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php endif; ?>

                  </select>

              </div>
              <div class="form-group col-md-2 col-sm-2 <?php if($errors->has('receiver_latitude')): ?> has-error <?php endif; ?>">
                  <label for="receiver_latitude"> Latitude: </label>
                  <input type="text" id="receiver_latitude" name="receiver_latitude" class="form-control" value="<?php echo e(! empty($order) ? $order->receiver_latitude :  '30.0668886'); ?>"/>
                  <?php if($errors->has("receiver_latitude")): ?>
                      <span class="help-block"><?php echo e($errors->first("receiver_latitude")); ?></span>
                  <?php endif; ?>
              </div>
              <div class="form-group col-md-2 col-sm-2 <?php if($errors->has('receiver_longitude')): ?> has-error <?php endif; ?>">
                  <label for="receiver_longitude"> Longitude: </label>
                  <input type="text" id="receiver_longitude" name="receiver_longitude" class="form-control" value="<?php echo e(! empty($order) ? $order->receiver_longitude :  '31.1962743'); ?>"/>
                  <?php if($errors->has("receiver_longitude")): ?>
                      <span class="help-block"><?php echo e($errors->first("receiver_longitude")); ?></span>
                  <?php endif; ?>
              </div>


              <div class="form-group input-group col-md-6 col-sm-6 <?php if($errors->has('receiver_address')): ?> has-error <?php endif; ?>">
                  <label for="receiver_address">Receiver Address: </label>

                  <input type="text" id="receiver_address" name="receiver_address" class="form-control" value="<?php echo e(! empty($order) ? $order->receiver_address : old("receiver_address")); ?>"/>

                  <span class="input-group-append">
                  <button class="input-group-text bg-transparent" type="button" data-toggle="modal" data-target="#map1Modal"> <i class="fa fa-map-marker"></i></button>
                </span>

                  <?php if($errors->has("receiver_address")): ?>
                      <span class="help-block"><?php echo e($errors->first("receiver_address")); ?></span>
                  <?php endif; ?>
              </div>

          </div>

          <div class="col-md-12 col-sm-12 col-xs-12" style="border: 2px solid #ea2;margin-bottom: 15px;">
              <h4 style="color: #ea2;font-weight: bold">Sender Data</h4>

              <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('corparate_id')): ?> has-error <?php endif; ?>">
                  <label for="corparate_id-field">Corporate</label>


                  <select  class="form-control" id="corparate_id"  name="corparate_id" disabled>
                      <option value="" data-display="Select">Corparate</option>
                      <?php if(! empty($Corporate_ids)): ?>
                          <?php $__currentLoopData = $Corporate_ids; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $corparate_id): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($corparate_id->id); ?>" <?php echo e(! empty($order) && $order->corporate_id  == $corparate_id->id ? 'selected':  ''); ?> >
                                  <?php echo e($corparate_id->id.' - ' .$corparate_id->name); ?>

                              </option>

                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php endif; ?>

                  </select>

                  <?php if($errors->has("corparate_id")): ?>
                      <span class="help-block"><?php echo e($errors->first("corparate_id")); ?></span>
                  <?php endif; ?>
              </div>

              <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('customer_id')): ?> has-error <?php endif; ?>">
                  <label for="customer_id-field">Customer </label>
                  <select  class=" form-control" id="customer_id"  name="customer_id" disabled>
                      <?php if(! empty($customers)): ?>
                          <?php $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option <?php echo e(! empty($order) && $order->customer_id  == $customer->id ? 'selected':  ''); ?>   value="<?php echo e($customer->id); ?>">
                                  <?php echo e($customer->name); ?> <?php echo e($customer->id); ?>

                              </option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php endif; ?>


                  </select>
                  <?php if($errors->has("customer_id")): ?>
                      <span class="help-block"><?php echo e($errors->first("customer_id")); ?></span>
                  <?php endif; ?>
              </div>

              <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('sender_name')): ?> has-error <?php endif; ?>">
                  <label for="sender_name-field">Name: </label>
                  <input type="text" id="sender_name-field" name="sender_name" class="form-control" value="<?php echo e(! empty($order) ? $order->sender_name : old("sender_name")); ?>"/>
                  <?php if($errors->has("sender_name")): ?>
                      <span class="help-block"><?php echo e($errors->first("sender_name")); ?></span>
                  <?php endif; ?>
              </div>

              <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('sender_mobile')): ?> has-error <?php endif; ?>">
                  <label for="sender_mobile-field">Mobile: </label>
                  <input type="text" id="sender_mobile-field" name="sender_mobile" class="form-control" value="<?php echo e(! empty($order) ? $order->sender_mobile : old("sender_mobile")); ?>"/>
                  <?php if($errors->has("sender_mobile")): ?>
                      <span class="help-block"><?php echo e($errors->first("sender_mobile")); ?></span>
                  <?php endif; ?>
              </div>
              <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('sender_phone')): ?> has-error <?php endif; ?>">
                  <label for="sender_phone-field">Mobile 2: </label>
                  <input type="text" id="sender_phone-field" name="sender_phone" class="form-control" value="<?php echo e(! empty($order) ? $order->sender_phone : old("sender_phone")); ?>"/>
                  <?php if($errors->has("sender_phone")): ?>
                      <span class="help-block"><?php echo e($errors->first("sender_phone")); ?></span>
                  <?php endif; ?>
              </div>

              <div class="form-group col-md-3 col-sm-3">
                  <label class="control-label" for="s_government_id">Government</label>

                  <select  class="form-control s_government_id" id="s_government_id"  name="s_government_id">
                      <option value="" data-display="Select">Government</option>
                      <?php if(! empty($governments)): ?>
                          <?php $__currentLoopData = $governments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $government): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($government->id); ?>" <?php echo e(! empty($order) && $order->s_government_id  == $government->id ? 'selected':  ''); ?> >
                                  <?php echo e($government->name_en); ?>

                              </option>

                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php endif; ?>

                  </select>

              </div>

              <div class="form-group col-md-2 col-sm-2">
                  <label class="control-label" for="s_state_id">City</label>

                  <select  class=" form-control s_state_id" id="s_state_id"  name="s_state_id" >
                      <?php if(! empty($s_cities)): ?>

                          <?php $__currentLoopData = $s_cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $s_city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($s_city->id); ?>" <?php echo e(! empty($order) && $order->s_state_id  == $s_city->id ? 'selected':  ''); ?> >
                                  <?php echo e($s_city->name_en); ?>

                              </option>

                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php endif; ?>

                  </select>

              </div>



              <div class="form-group col-md-2 col-sm-2 <?php if($errors->has('sender_latitude')): ?> has-error <?php endif; ?>">
                  <label for="sender_latitude">Latitude: </label>
                  <input type="text" id="sender_latitude" name="sender_latitude" class="form-control" value="<?php echo e(! empty($order) ? $order->sender_latitude : '30.0668886'); ?>"/>
                  <?php if($errors->has("sender_latitude")): ?>
                      <span class="help-block"><?php echo e($errors->first("sender_latitude")); ?></span>
                  <?php endif; ?>
              </div>
              <div class="form-group col-md-2 col-sm-2 <?php if($errors->has('sender_longitude')): ?> has-error <?php endif; ?>">
                  <label for="sender_longitude">Longitude: </label>
                  <input type="text" id="sender_longitude" name="sender_longitude" class="form-control" value="<?php echo e(! empty($order) ? $order->sender_longitude : '31.1962743'); ?>"/>
                  <?php if($errors->has("sender_longitude")): ?>
                      <span class="help-block"><?php echo e($errors->first("sender_longitude")); ?></span>
                  <?php endif; ?>
              </div>

              <div class="form-group input-group col-md-6 col-sm-6 <?php if($errors->has('sender_address')): ?> has-error <?php endif; ?>">
                  <label for="sender_address">Sender Address: </label>
                  <input type="text" id="sender_address" name="sender_address" class="form-control" value="<?php echo e(! empty($order) ? $order->sender_address : old("sender_address")); ?>"/>
                  <span class="input-group-append">
                  <button class="input-group-text bg-transparent" type="button" data-toggle="modal" data-target="#map2Modal"> <i class="fa fa-map-marker"></i></button>
                </span>
                  <?php if($errors->has("sender_address")): ?>
                      <span class="help-block"><?php echo e($errors->first("sender_address")); ?></span>
                  <?php endif; ?>
              </div>

          </div>


          <div class="col-md-12 col-sm-12 col-xs-12" style="border: 2px solid #ea2;margin-bottom: 15px;">
              <h4 style="color: #ea2;font-weight: bold">Order Data</h4>


              <div class="form-group col-md-2 col-sm-2 <?php if($errors->has('type')): ?> has-error <?php endif; ?>">
                  <label for="type-field">Order Type</label>


                  <select  class="form-control" id="order_type_id"  name="order_type_id">
                      <option value="" data-display="Select">Type</option>
                      <?php if(! empty($types)): ?>
                          <?php $__currentLoopData = $types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($type->id); ?>"  <?php echo e(! empty($order) && $order->order_type_id  == $type->id ? 'selected':  ''); ?> >
                                  <?php echo e($type->name_en); ?>

                              </option>

                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php endif; ?>

                  </select>

                  <?php if($errors->has("type")): ?>
                      <span class="help-block"><?php echo e($errors->first("type")); ?></span>
                  <?php endif; ?>
              </div>

              <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('type')): ?> has-error <?php endif; ?>">
                  <label for="type-field">Type Name: </label>
                  <input type="text" id="type-field" name="type" class="form-control" value="<?php echo e(! empty($order) ? $order->type : old("type")); ?>"/>
                  <?php if($errors->has("type")): ?>
                      <span class="help-block"><?php echo e($errors->first("type")); ?></span>
                  <?php endif; ?>
              </div>
              <div class="form-group col-md-2 col-sm-2 <?php if($errors->has('main_store')): ?> has-error <?php endif; ?>">
                  <label for="store_id-field"><?php echo e(__('backend.store')); ?>:</label>
                  <select id="store_id-field" name="main_store" class="form-control" >
                      <option value="0" ><?php echo e(__('backend.Choose_Store')); ?></option>
                      <?php if(! empty($allStores)): ?>
                          <?php $__currentLoopData = $allStores; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $store): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($store->id); ?>" <?php echo e($store->id == $order->main_store ? 'selected' : ''); ?> >
                                  <?php echo e($store->name); ?>

                              </option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php endif; ?>

                  </select>
                  <?php if($errors->has("main_store")): ?>
                      <span class="help-block"><?php echo e($errors->first("main_store")); ?></span>
                  <?php endif; ?>
              </div>


              <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('stock_id')): ?> has-error <?php endif; ?>">
                  <label for="stock_id-field">Stock </label>
                  <select id="stock_id-field" name="store_id" class="form-control" >
                      <option value="0" >Choose Stock</option>
                      <?php if(! empty($stores)): ?>
                          <?php $__currentLoopData = $stores; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stock): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($stock->id); ?>" <?php echo e($stock->id == $order->store_id ? 'selected' : ''); ?> >
                                  <?php echo e($stock->title); ?>

                              </option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php endif; ?>

                  </select>
                  <?php if($errors->has("stock_id")): ?>
                      <span class="help-block"><?php echo e($errors->first("stock_id")); ?></span>
                  <?php endif; ?>
              </div>

              <div class="form-group col-md-2 col-sm-2 <?php if($errors->has('qty')): ?> has-error <?php endif; ?>">
                  <label for="qty-field">Quantity Order </label>
                  <input type="text" id="qty-field" <?php if( ! empty($order->stock) ): ?> max="<?php echo e($order->stock->qty); ?>" <?php endif; ?> name="qty" class="form-control" value="<?php echo e(! empty($order) ? $order->qty : old("qty")); ?>"/>
                  <?php if($errors->has("qty")): ?>
                      <span class="help-block"><?php echo e($errors->first("qty")); ?></span>
                  <?php endif; ?>
              </div>


              <div class="form-group col-md-2 col-sm-2 <?php if($errors->has('overload')): ?> has-error <?php endif; ?>">
                  <label for="overload-field">Overload </label>
                  <input type="text" id="overload-field" name="overload" class="form-control" value="<?php echo e(! empty($order) ? $order->overload : old("overload")); ?>"/>
                  <?php if($errors->has("overload")): ?>
                      <span class="help-block"><?php echo e($errors->first("overload")); ?></span>
                  <?php endif; ?>
              </div>


              <div class="form-group col-md-2 col-sm-2 <?php if($errors->has('agent_id')): ?> has-error <?php endif; ?>">
                  <label for="agent_id-field">Agent </label>

                  <select id="agent_id-field" name="agent_id" class="form-control" >
                      <option value="">Choose Agent</option>
                      <?php if(! empty($agents)): ?>
                          <?php $__currentLoopData = $agents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $agent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($agent->id); ?>" <?php echo e(! empty($order) && $order->agent_id  == $agent->id ? 'selected':  ''); ?> >
                                  <?php echo e($agent->name); ?>

                              </option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php endif; ?>

                  </select>
                  <?php if($errors->has("agent_id")): ?>
                      <span class="help-block"><?php echo e($errors->first("agent_id")); ?></span>
                  <?php endif; ?>
              </div>

              <div class="form-group col-md-3 col-sm-3">
                  <label class="control-label" for="driver_id">Driver</label>

                  <select  class=" form-control driver_id" id="driver_id"  name="forward_driver_id" >
                      <option value="">Choose Drivers</option>
                      <?php if(! empty($drivers)): ?>
                          <?php $__currentLoopData = $drivers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $driver): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($driver->id); ?>" <?php echo e(! empty($order) && $order->forward_driver_id  == $driver->id ? 'selected':  ''); ?>>
                                  <?php echo e($driver->name); ?>

                              </option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php endif; ?>

                  </select>

              </div>
              <div class="form-group col-md-3 col-sm-3 <?php if($errors->has('payment_method_id')): ?> has-error <?php endif; ?>">
                  <label for="payment_method_id-field">Payment Method</label>
                  <select id="payment_method_id-field" name="payment_method_id" class="form-control" >
                      <option value="" data-display="Select">Payment Method</option>

                      <?php if(! empty($payments)): ?>
                          <?php $__currentLoopData = $payments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($payment->id); ?>" <?php echo e(! empty($order) && $order->payment_method_id  == $payment->id ? 'selected':  ''); ?> >
                                  <?php echo e($payment->name); ?>

                              </option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php endif; ?>

                  </select>
                  <?php if($errors->has("payment_method_id")): ?>
                      <span class="help-block"><?php echo e($errors->first("payment_method_id")); ?></span>
                  <?php endif; ?>
              </div>

              <div class="form-group col-md-2 col-sm-3 <?php if($errors->has('order_price')): ?> has-error <?php endif; ?>">
                  <label for="order_price-field">Order Price: </label>
                  <input type="text" class="form-control" id="order_price-field" name="order_price" value="<?php echo e(! empty($order) ? $order->order_price : old("order_price")); ?>" >
                  <?php if($errors->has("order_price")): ?>
                      <span class="help-block"><?php echo e($errors->first("order_price")); ?></span>
                  <?php endif; ?>
              </div>

              <div class="form-group col-md-2 col-sm-3 <?php if($errors->has('delivery_price')): ?> has-error <?php endif; ?>">
                  <label for="delivery_price-field">Delivery Price: </label>
                  <input type="text" required class="form-control" id="delivery_price-field" name="delivery_price" value="<?php echo e(! empty($order) ? $order->delivery_price : old("delivery_price")); ?>" >
                  <?php if($errors->has("delivery_price")): ?>
                      <span class="help-block"><?php echo e($errors->first("delivery_price")); ?></span>
                  <?php endif; ?>
              </div>


              <div class="form-group col-md-12 col-sm-12 <?php if($errors->has('notes')): ?> has-error <?php endif; ?>">
                  <label for="notes-field">Notes: </label>

                  <input type="text" id="notes-field" name="notes" class="form-control" value="<?php echo e(! empty($order) ? $order->notes : old("notes")); ?>"/>

                  <?php if($errors->has("notes")): ?>
                      <span class="help-block"><?php echo e($errors->first("notes")); ?></span>
                  <?php endif; ?>
              </div>

          </div>

        <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
          <button type="submit" class="btn btn-primary"><?php echo e(__('backend.save_edits')); ?></button>
          <a class="btn btn-link pull-right" href="<?php echo e(route('mngrAdmin.orders.index')); ?>"><i class="glyphicon glyphicon-backward"></i> <?php echo e(__('backend.back')); ?></a>
        </div>
      </form>
    </div>
  </div>
  <?php echo $__env->make('backend.dialog-map1', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <?php echo $__env->make('backend.dialog-map2', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript" src="<?php echo e(asset('/assets/scripts/map-order-edit.js')); ?>"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQiN4qc7pC9b1BRQwkqXXD28peMfWcHvw&&callback=myMap"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <?php echo $__env->make('backend.orders.js', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/orders/edit.blade.php ENDPATH**/ ?>
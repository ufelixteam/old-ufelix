@extends('frontend.profile.layouts.app')

@section('title')
    @lang('website.orderview')
@endsection
{{--@section('css')--}}

{{--    <link href="{{asset('public/profile/assets/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">--}}
{{--@endsection--}}

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="">
                <div class="table-responsive m-t-40">
                    <h1 class="title">@lang('website.order_view')</h1>
                    @if(! empty($order) && $order)
                        <div class="container-fluid">

                            <div class="row" style="padding: 12px 20px;">
                                <div class="alert alert-danger" style="display:none"></div>
                                <!-- Create at: -->
                                <div class="form-group col-sm-3">
                                    <label for="receiver_name">{{__('backend.created_at')}}: </label>
                                    <p class="form-control-static"
                                       style="color: #0376d9; font-weight: bold;">@if($order->created_at){{$order->created_at}} @else {{__('backend.No_Date')}} @endif </p>
                                </div>

                                <!-- Accept at: -->
                                <div class="form-group col-sm-3">
                                    <label for="reciever_mobile"> {{__('backend.Accept_at')}}: </label>
                                    <p class="form-control-static" style="color: #0376d9; font-weight: bold;">


                                        @if( $order->status != 0 )
                                            {{$order->accepted_at}}
                                        @else {{__('backend.No_Date')}} @endif
                                    </p>
                                </div>

                                <!-- Receiver at: -->
                                <div class="form-group col-sm-3">
                                    <label for="receiver_name">{{__('backend.Received_at')}}: </label>
                                    <p class="form-control-static" style="color: #0376d9; font-weight: bold;">
                                        @if( $order->status != 0  )

                                            {{$order->received_at}}

                                        @else {{__('backend.No_Date')}}  @endif
                                    </p>
                                </div>

                                <!-- Deliver at: -->
                                <div class="form-group col-sm-2">
                                    <label for="reciever_mobile"> {{__('backend.Deliver_at')}}: </label>
                                    <p class="form-control-static" style="color: #0376d9; font-weight: bold;">
                                        @if ($order->status != 0 )
                                            {{$order->delivered_at}}
                                        @else {{__('backend.No_Date')}} @endif
                                    </p>
                                </div>


                            </div>
                            <div class="row">

                                <div class="col-md-6 col-xs-12">
                                    <div class="ribbon-wrapper card">
                                        <div class="ribbon ribbon-default">Receiver Data</div>

                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-sm-4"
                                                       style="font-size: 14px;margin-top: 5px;">@lang('website.ReceiverName')
                                                    : </label>
                                                <div class="col-12 col-md-8">
                                                    <input disabled class="form-control" type="text"
                                                           name="customer_name" value="{{$order->receiver_name}}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;">Mobile
                                                    Number: </label>
                                                <div class="col-12 col-md-8">
                                                    <input disabled class="form-control" type="text"
                                                           name="customer_email" value="{{$order->receiver_mobile}}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;">Receiver
                                                    Address: </label>
                                                <div class="col-12 col-md-8">
                                                    <input disabled class="form-control" type="text" name=""
                                                           value="{{$order->receiver_address}}"></div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="ribbon-wrapper card">
                                        <div class="ribbon ribbon-primary">Driver Details:</div>

                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;">Driver
                                                    Name: </label>
                                                <div class="col-12 col-md-8">
                                                    <input disabled class="form-control" type="text"
                                                           name="corporate_name"
                                                           value="{{ ! empty($order->accepted ) && ! empty($order->accepted->driver) ? $order->accepted->driver->name : ''}}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;">Driver
                                                    Mobile: </label>
                                                <div class="col-12 col-md-8">
                                                    <input disabled class="form-control" type="text"
                                                           name="corporate_email"
                                                           value="{{ ! empty($order->accepted ) && ! empty($order->accepted->driver) ? $order->accepted->driver->mobile : ''}}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;"> Driver
                                                    E-Mail: </label>
                                                <div class="col-12 col-md-8">
                                                    <input disabled class="form-control" type="text"
                                                           name="corporate_mobile"
                                                           value="{{! empty($order->accepted ) && ! empty($order->accepted->driver) ? $order->accepted->driver->email : ''}}">
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                {{--                                <div class="col-md-6 col-xs-12">--}}
                                {{--                                    <div class="ribbon-wrapper card">--}}
                                {{--                                        <div class="ribbon ribbon-info">Sender Data</div>--}}

                                {{--                                        <div class="form-group">--}}
                                {{--                                            <div class="row">--}}
                                {{--                                                <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;">Sender--}}
                                {{--                                                    Name: </label>--}}
                                {{--                                                <div class="col-12 col-md-8">--}}
                                {{--                                                    <input disabled class="form-control" type="text"--}}
                                {{--                                                           name="corporate_name" value="{{$order->sender_name}}">--}}
                                {{--                                                </div>--}}
                                {{--                                            </div>--}}
                                {{--                                        </div>--}}

                                {{--                                        <div class="form-group">--}}
                                {{--                                            <div class="row">--}}
                                {{--                                                <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;">Mobile--}}
                                {{--                                                    Number: </label>--}}
                                {{--                                                <div class="col-12 col-md-8">--}}
                                {{--                                                    <input disabled class="form-control" type="text"--}}
                                {{--                                                           name="corporate_email" value="{{$order->sender_mobile}}">--}}
                                {{--                                                </div>--}}
                                {{--                                            </div>--}}
                                {{--                                        </div>--}}

                                {{--                                        <div class="form-group">--}}
                                {{--                                            <div class="row">--}}
                                {{--                                                <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;">Sender--}}
                                {{--                                                    Address: </label>--}}
                                {{--                                                <div class="col-12 col-md-8">--}}
                                {{--                                                    <input disabled class="form-control" type="text" name=""--}}
                                {{--                                                           value="{{$order->sender_address}}">--}}
                                {{--                                                </div>--}}
                                {{--                                            </div>--}}
                                {{--                                        </div>--}}

                                {{--                                    </div>--}}
                                {{--                                </div>--}}
                            </div>

                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <div class="ribbon-wrapper card">
                                        <div class="ribbon ribbon-danger">Order Data</div>

                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;">Order
                                                    Type: </label>
                                                <div class="col-12 col-md-8">
                                                    <input disabled class="form-control" type="text"
                                                           name="corporate_name"
                                                           value="{{! empty($order->type) ? $order->type : '' }}">
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-sm-4"
                                                       style="font-size: 14px;margin-top: 5px;">@lang('website.OrderPrice')
                                                    : </label>
                                                <div class="col-12 col-md-8">
                                                    <input disabled class="form-control" type="text" name="order_price"
                                                           value="{{! empty($order->order_price) ? $order->order_price : '' }}">
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-sm-4"
                                                       style="font-size: 14px;margin-top: 5px;">@lang('website.OrderNumber')
                                                    : </label>
                                                <div class="col-12 col-md-8">
                                                    <input disabled class="form-control" type="text"
                                                           name="corporate_email" value="{{$order->order_number}}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;">Receiver
                                                    Code: </label>
                                                <div class="col-12 col-md-8">
                                                    <input disabled class="form-control" type="text" name=""
                                                           value="{{$order->receiver_code}}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-sm-4"
                                                       style="font-size: 14px;margin-top: 5px;">Notes: </label>
                                                <div class="col-12 col-md-8">
                                                    <textarea disabled class="form-control" rows="3"
                                                              name="corporate_mobile" style="min-height: 150px;resize: vertical;"> {{! empty($order->notes) ? $order->notes : '-' }} </textarea>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                    @else
                        <div class="container-fluid">

                            <div class="alert alert-danger text-center">
                                <p>
                                    No Order Found
                                </p>
                            </div>

                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>


@endsection
{{--@section('scripts')--}}
{{--    <!-- Sweet-Alert  -->--}}
{{--    <script src="{{asset('public/Profile/assets/plugins/sweetalert/sweetalert.min.js') }}"></script>--}}
{{--    <script src="{{asset('public/Profile/assets/plugins/sweetalert/jquery.sweet-alert.custom.js') }}"></script>--}}


{{--@endsection--}}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $guarded = ['id'];

    public function getStatusSpanAttribute()
    {
        if ($this->status == 0) {
            return "<span class='badge badge-pill label-danger'>" . __('backend.opened') . "</span>";
        } elseif ($this->status == 1) {
            return "<span class='badge badge-pill label-success'>" . __('backend.closed') . "</span>";
        } elseif ($this->status == 2) {
            return "<span class='badge badge-pill label-warning'>" . __('backend.done') . "</span>";
        }
    }

    public function getCreatedByAttribute()
    {
        if ($this->user) {
            return "<span class='badge badge-pill label-warning'>" . $this->user->name . "</span>";
        } else {
            return "<span class='badge badge-pill label-info'>" . __('backend.system') . "</span>";
        }
    }

    public function getClosedByUserAttribute()
    {
        if ($this->status == 1) {
            if ($this->closed_by) {
                return "<span class='badge badge-pill label-warning'>" . $this->closed_by->name . "</span>";
            } else {
                return "<span class='badge badge-pill label-info'>" . __('backend.system') . "</span>";
            }
        } else {
            return "-";
        }
    }

    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'order_id');
    }

    public function corporate()
    {
        return $this->belongsTo('App\Models\Corporate', 'corporate_id');
    }

    public function role()
    {
        return $this->belongsTo('App\Models\Role_admin', 'role_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function assigned_to()
    {
        return $this->belongsTo('App\Models\User', 'assigned_to_id');
    }

    public function closed_by()
    {
        return $this->belongsTo('App\Models\User', 'closed_by_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\TicketComment', 'ticket_id');
    }
}

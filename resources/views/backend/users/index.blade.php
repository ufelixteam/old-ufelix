@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.admins')}}</title>
  <style type="text/css">
      .input-sm{
          width: 70%;
      }
      input[type="search"] {
          width: 100%;
      }
  </style>
@endsection

@section('header')
@endsection

@section('content')
    <div class="page-header">
      <h3>
          <i class="fa fa-users"></i> {{__('backend.admins')}}
          @if(permission('addAdmin')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
            <a class="btn btn-success pull-right" href="{{ route('mngrAdmin.users.create') }}"><i class="glyphicon glyphicon-plus"></i> {{__('backend.add_admin')}}</a>
          @endif
      </h3>
    </div>

    <div class="row">
        <div class="col-md-12 table-responsive">
            @if($users->count())
                <table class="table table-condensed table-striped" >
                    <thead>
                        <tr>
                            <th style="width: 1%;">#</th>
                            <th style="width: 14%;">{{__('backend.name')}}</th>
                            <th>{{__('backend.email')}}</th>
                            <th>{{__('backend.mobile')}}</th>
                            <th>{{__('backend.role')}}</th>
                            <th>{{__('backend.block')}}</th>
                            <th style="width: 26%;" class="text-right"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->id}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->mobile}}</td>
                                <td>{{ ! empty($user->roleAdmin) ? $user->roleAdmin->name : ' Not Found ' }}</td>
                                <td>{!! $user->blocks_span !!}</td>
                                <!-- <td><input type="checkbox" value="{{$user->id}}"  @if ($user->is_block == 1) checked="checked" @endif name="is_block" ></td>-->
                                <td class="text-right">
                                    @if(permission('showAdmin')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                      <a class="btn btn-xs btn-primary" href="{{ route('mngrAdmin.users.show', $user->id) }}"><i class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>
                                    @endif

                                    @if(permission('editAdmin')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                      <a class="btn btn-xs btn-warning" href="{{ route('mngrAdmin.users.edit', $user->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                                    @endif

                                    @if(permission('deleteAdmin')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                      <form action="{{ route('mngrAdmin.users.destroy', $user->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
                                          <input type="hidden" name="_method" value="DELETE">
                                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                          <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}</button>
                                      </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {!! $users->appends($_GET)->links() !!}

            @else
                <h3 class="text-center alert alert-info">{{__('backend.No_result_found')}}</h3>
            @endif
        </div>
    </div>
@endsection

@section('scripts')
  <script type="text/javascript">
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    $('#theTable').DataTable({
      "pagingType": "full_numbers"
    });
     $("input:checkbox").change(function() {
       // var user_id = $(this).closest('tr').attr('#');
        var user_id = $(this).val();
       // console.log(user_id)
        $.ajax({
                type:'POST',
                url:baseUrl+'/block_user',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                data: { "user_id" : user_id },
                success: function(data){
                  if(data.data.success){
                    //do something
                //    console.log('done');
                    alert(data.data.message)
                  }
                }
            });
        });
    /*$("[name='is_block']").bootstrapSwitch();*/
    /*
    $("#search").keyup(function(e) {
            e.preventDefault();
            var serchKy = $('#search').val();
            var getUrl = window.location;
    	var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
            $.ajax({
                url: baseUrl+"/searchCustomer/"+$('#search').val(),
                type: 'GET',
                data: {},
                success: function(data) {

                console.log('hghg');
                $('#table').html(data.view);

                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        });
    */
  </script>
@endsection

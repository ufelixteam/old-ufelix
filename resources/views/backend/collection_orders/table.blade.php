<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        @if($collection_orders->count())
            <table class="table table-condensed table-striped text-center">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('backend.customer')}}</th>
                    <th>{{__('backend.corporate')}}</th>
                    <th>{{__('backend.create_upload_at')}}</th>
                    <th>{{__('backend.complete_process_at')}}</th>
                    <th>{{__('backend.completed')}}</th>
                    <th>{{__('backend.count_orders')}}</th>
                    <th>{{__('backend.type')}}</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                @foreach($collection_orders as $collection)
                    <tr @if(count($collection->listorders) > 0) style="background: #dad8d8" @endif>
                        <td>{{$collection->id}}</td>
                        <td>{{! empty($collection->customer) ? $collection->customer->name : ''}}</td>
                        <td>{{isset($collection->customer->Corporate) ? $collection->customer->Corporate->name : '-'}}</td>
                        <td>{{$collection->created_at}}</td>
                        <td>{{$collection->saved_at}}</td>
                        <td>{!!$collection->completed_span!!}</td>
                        <td>
                            @if($collection->is_saved == 1)
                                {{$collection->listorders_count}}
                            @elseif($collection->type == 1)
                                {{$collection->temporders_count}}
                            @else
                                0
                            @endif
                        </td>
                        <td>{!!$collection->target_span!!}</td>
                        <td>
                            @if(!$collection->type && ($collection->is_saved != 1 || $collection->is_orders_pending))
                                <button type="button" class="btn btn-xs btn-success importOrders" data-id="{{$collection->id}}"
                                        data-toggle="modal" data-target="#processModal">
                                    <i class="fa fa-upload"></i>
                                </button>
                            @endif
                            @if(!$collection->type)
                                <a href="{{ asset('/api_uploades/client/corporate/sheets/'.$collection->file_name) }}">
                                    <button type="submit" class="btn btn-xs btn-warning"
                                            name="button"><i class="fa fa-download"></i></button>
                                </a>
                            @endif
                            @if($collection->is_saved != 1 || ($collection->listorders_count == 0 && $collection->temporders_count == 0))
                                @if((!$collection->type && $collection->is_saved == 1) || $collection->type)
                                <form
                                    action="{{ URL::asset('/mngrAdmin/collection_orders/'. $collection->id.'?type=1') }}"
                                    method="POST" style="display: inline;"
                                    onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="submit" class="btn btn-xs btn-danger"><i
                                            class="glyphicon glyphicon-trash"></i></button>
                                </form>
                                @endif
                            @else
                                <a class="btn btn-xs btn-primary "
                                   href="{{ URL::asset('mngrAdmin/orders?collection='.$collection->id) }}">
                                    <i class="fa fa-list"></i>
                                </a>

                                <button type="button" class="btn btn-xs btn-info"
                                        disabled><i class="fa fa-check-circle"></i></button>
                                <a class="btn btn-xs btn-info "
                                   href="{{ URL::asset('mngrAdmin/collection_orders/print/'.$collection->id) }}"
                                   target="_blank">
                                    <i class="fa fa-print"></i>
                                </a>
                                @if(in_array($collection->allow_pickup_status, [1, 2]))
                                    <a class="btn btn-xs btn-success packup @if($collection->allow_pickup_status == 2) disabled @endif"
                                       data-value="{{$collection->id}}"
                                       data-corporate="{{isset($collection->customer->Corporate) ? $collection->customer->Corporate->id : ''}}"
                                       data-customer="{{$collection->customer_id}}"
                                       btn btn-xs btn-success href="#">
                                        <i class="fa fa-cart-plus"></i>
                                    </a>
                                @endif
                                @if($collection->is_orders_pending)
                                    <form
                                        action="{{ URL::asset('/mngrAdmin/collection_orders/'. $collection->id.'?type=1') }}"
                                        method="POST" style="display: inline;"
                                        onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i
                                                class="glyphicon glyphicon-trash"></i></button>
                                    </form>
                                @endif
                            @endif
                        </td>


                    </tr>
                @endforeach
                </tbody>
            </table>
            {!! $collection_orders->appends($_GET)->links() !!}
        @else
            <h3 class="text-center alert alert-warning">No Result Found !</h3>
        @endif
    </div>
</div>

@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.corporates')}}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <style>
        .toggle.ios, .toggle-on.ios, .toggle-off.ios {
            border-radius: 20px;
        }

        .toggle.ios .toggle-handle {
            border-radius: 20px;
        }

        @media only screen and (max-width: 580px) {
            .hide-td {
                display: none;
            }
        }

        .box-body {
            min-height: 100px;
        }

        .stylish-input-group .input-group-addon {
            background: white !important;
        }

        .stylish-input-group .form-control {
            border-right: 0;
            box-shadow: 0 0 0;
            border-color: #ccc;
        }

        .stylish-input-group button {
            border: 0;
            background: transparent;
        }

        .something-wrong {
            background: #9e8f9e;
        }
    </style>
@endsection
@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.corporates')}}
        @if(permission('addCorporate')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
            <a class="btn btn-success pull-right" href="{{ route('mngrAdmin.corporates.create') }}"><i
                    class="glyphicon glyphicon-plus"></i> {{__('backend.add_corporate')}}</a>
            @endif
        </h3>
    </div>
@endsection

@section('content')

    <div class="row" style="margin-bottom:15px;">
        <div class="col-md-12">
            <div class="col-md-3 col-sm-3">
                <form action="{{URL::asset('/mngrAdmin/corporates')}}" method="get" id="search-form"/>
                <div id="imaginary_container">
                    <div class="input-group stylish-input-group">
                        <input type="text" class="form-control" id="search-field" name="search"
                               placeholder="{{__('backend.search_by_name_or_mobile')}}">
                        <span class="input-group-addon">
                            <button type="button" id="search-btn1">
                              <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </span>
                    </div>
                </div>
                </form>
            </div>
            <div class="group-control col-md-3 col-sm-3">
                <select id="active-field" name="active" class="form-control">
                    <option value="-1">{{__('backend.sort_by_active')}}</option>
                    <option value="1">{{__('backend.active')}}</option>
                    <option value="0">{{__('backend.not_activated')}}</option>
                </select>
            </div>
            <div class="group-control col-md-3 col-sm-3">
                <select id="store_id" name="store_id" class="form-control">
                    <option value="">{{__('backend.store')}}</option>
                    @foreach($stores as $store)
                        <option value="{{$store->id}}">{{$store->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="group-control col-md-3 col-sm-3">
                <select id="responsible_id" name="responsible_id" class="form-control">
                    <option value="">{{__('backend.responsible')}}</option>
                    @foreach($users as $user)
                        <option value="{{$user->id}}">{{$user->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    <div class="list">
        @include('backend.corporates.table')
    </div>
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <script>
        // Select All Orders function For Print
        function do_this() {
            var checkboxes = document.getElementsByName('select[]');
            var button = document.getElementById('toggle');

            if (button.value == 'Select') {
                for (var i in checkboxes) {
                    checkboxes[i].checked = 'FALSE';
                }
                button.value = 'Deselect'
            } else {
                for (var i in checkboxes) {
                    checkboxes[i].checked = '';
                }
                button.value = 'Select';
            }
        }

        $('body').on('click', '#pdf-submit', function (e) {

            let ids = [];

            $(".selectpdf input:checked").each(function () {
                ids.push($(this).val());

            });
            if (ids.length > 0) {
                $('<form action="{{ url("/mngrAdmin/reports/corporate_list/")}}" method="post" target="_blank"><input value="' + ids + ' " name="ids"  />" {{csrf_field()}}"</form>').appendTo('body').submit();

            } else {

                $.confirm({
                    title: '',
                    theme: 'modern',
                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Print</span></div><div style="font-weight:bold;"><p>{{__("backend.Must_Choose_Orders_To_Print")}}</p></div>',
                    type: 'red',
                });
            }

        });

        $('.toggle').bootstrapToggle();

        $("#search-btn1").on('click', function () {
            // Wait icon
            $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
            $.ajax({
                url: '{{URL::asset("/mngrAdmin/corporates")}}' + '?search=' + $(this).val() + '&active=' + $("#active-field").val() + '&store_id=' + $("#store_id").val() + '&responsible_id=' + $("#responsible_id").val(),
                type: 'get',
                data: $("#search-form").serialize(),
                success: function (data) {

                    $('.list').html(data.view);
                    $(".toggle").bootstrapToggle('destroy')
                    $(".toggle").bootstrapToggle();
                    // $('#theTable').DataTable({
                    //   "pagingType": "full_numbers"
                    // });
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $("#active-field").on('change', function () {
            $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
            $.ajax({
                url: '{{URL::asset("/mngrAdmin/corporates")}}' + '?active=' + $("#active-field").val() + '&search=' + $("#search-field").val() + '&store_id=' + $("#store_id").val() + '&responsible_id=' + $("#responsible_id").val(),
                type: 'get',
                success: function (data) {
                    $('.list').html(data.view);
                    $(".toggle").bootstrapToggle('destroy')
                    $(".toggle").bootstrapToggle();
                    // $('#theTable').DataTable({
                    //   "pagingType": "full_numbers"
                    // });
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $("#store_id").on('change', function () {
            $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
            $.ajax({
                url: '{{URL::asset("/mngrAdmin/corporates")}}' + '?active=' + $("#active-field").val() + '&search=' + $("#search-field").val() + '&store_id=' + $("#store_id").val() + '&responsible_id=' + $("#responsible_id").val(),
                type: 'get',
                success: function (data) {
                    $('.list').html(data.view);
                    $(".toggle").bootstrapToggle('destroy')
                    $(".toggle").bootstrapToggle();
                    // $('#theTable').DataTable({
                    //   "pagingType": "full_numbers"
                    // });
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $("#responsible_id").on('change', function () {
            $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
            $.ajax({
                url: '{{URL::asset("/mngrAdmin/corporates")}}' + '?active=' + $("#active-field").val() + '&search=' + $("#search-field").val() + '&store_id=' + $("#store_id").val() + '&responsible_id=' + $("#responsible_id").val(),
                type: 'get',
                success: function (data) {
                    $('.list').html(data.view);
                    $(".toggle").bootstrapToggle('destroy')
                    $(".toggle").bootstrapToggle();
                    // $('#theTable').DataTable({
                    //   "pagingType": "full_numbers"
                    // });
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $('body').on('change', '.change_active', function (e) {
            let id = $(this).attr('data-id');
            $.ajax({
                url: '{{URL::asset("/mngrAdmin/change-active-corporate")}}',
                type: 'post',
                data: {id: id, _token: "{{csrf_token()}}"},
                success: function (data) {

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $('body').on('change', '.change_slider', function (e) {
            let id = $(this).attr('data-id');
            $.ajax({
                url: '{{URL::asset("/mngrAdmin/change-slider-corporate")}}',
                type: 'post',
                data: {id: id, _token: "{{csrf_token()}}"},
                success: function (data) {

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });


    </script>
@endsection

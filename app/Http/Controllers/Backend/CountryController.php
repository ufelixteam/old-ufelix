<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Country;
use Response;
use Flash;
use File;
use Auth;

class CountryController extends Controller
{

    // Get All Countries
    public function index()
    {
        $countries = Country::orderBy('id', 'desc')->paginate(50);
        return view('backend.countries.index', compact('countries'));
    }

    // Create Country Page
    public function create()
    {
        return view('backend.countries.create');
    }

    // Added New Country
    public function store(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'name' => 'required',
            'code' => 'required',
            'image' => 'mimes:jpeg,png,jpg,gif',
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        $country = new Country();
        $country->name = $request->input("name");
        $country->code = $request->input("code");

        if ($request->file("image") != "") {
            $extension = File::extension($request->file("image")->getClientOriginalName());
            $image = 'country-' . time() . "." . "jpg";
            $country->image = $image;
            $request->file("image")->move(public_path('backend/images/'), $image);
        }
        $country->save();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Add New Country ' . $country->name, ' اضافة بلد جديدة ' . $country->name, 26); // Helper Function in App\Helpers.php

        Flash::success(__('backend.Country_saved_successfully'));
        return redirect()->route('mngrAdmin.countries.index');
    }

    // View One Country
    public function show($id)
    {
        return back();
        // $country = Country::findOrFail($id);
        // return view('backend.countries.show', compact('country'));
    }

    // Edit Country
    public function edit($id)
    {
        $country = Country::findOrFail($id);
        return view('backend.countries.edit', compact('country'));
    }

    // Update Country
    public function update(Request $request, $id)
    {

        $v = \Validator::make($request->all(), [
            'name' => 'required',
            'code' => 'required',
            'image' => 'mimes:jpeg,png,jpg,gif',
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        $country = Country::findOrFail($id);
        $country->name = $request->input("name");
        $country->code = $request->input("code");
        if ($request->file("image") != "") {
            $extension = File::extension($request->file("image")->getClientOriginalName());
            $image = 'country-' . time() . "." . "jpg";
            $country->image = $image;
            $request->file("image")->move(public_path('backend/images/'), $image);
        }
        $country->save();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Edit Country ' . $country->name, ' تعديل البلد ' . $country->name, 26); // Helper Function in App\Helpers.php

        Flash::warning(__('backend.Country_Updated_successfully'));
        return redirect()->route('mngrAdmin.countries.index');
    }

    // Delete Country
    public function destroy($id)
    {
        $country = Country::findOrFail($id);
        $country->delete();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Delete Country ' . $country->name, ' حذف البلد ' . $country->name, 26); // Helper Function in App\Helpers.php

        flash(__('backend.Country_deleted_successfully'))->error();
        return redirect()->route('mngrAdmin.countries.index');
    }

}

<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\DailyReport',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('daily:report')->dailyAt('21:00');

        $schedule->command('cashCollectTask:generate')
            ->dailyAt('03:00');

        $schedule->command('orders:cancel')
            ->dailyAt('02:00');

        $schedule->command('ticket:generate')
            ->dailyAt('03:00');

//            ->cron('0 */2 * * *'); // every 2 hours
//            ->everyMinute();
//            ->sendOutputTo('/home/amokhtar/Desktop/log');

        $schedule->command('ticket:close')
            ->dailyAt('04:00');
//            ->cron('0 */3 * * *'); // every 3 hours
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}

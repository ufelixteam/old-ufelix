@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.task_orders')}} - {{__('backend.view')}}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">

@endsection
@section('header')
    <div class="page-header">
        <h3>{{__('backend.task')}} #{{$pickup->pickup_number}}</h3>
        {!! $pickup->status_span !!}
        <br>

        <div class="pull-left">{!! $pickup->task_types !!}</div>
        <div class="pull-right">{!! $pickup->confirm_task_types !!}</div>
        <div class="clearfix"></div>

        <div class="pull-right">{!! $pickup->confirm_task_count!!}</div>
        <div class="clearfix"></div>

        <br>
        <div class="dropdown  pull-right">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="true">
                <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
            </button>
            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">

                @if ( permission('editpickups'))
                    <li>
                        <a href="{{ route('mngrAdmin.pickups.edit', $pickup->id) }}"><i
                                class="fa fa-edit"></i> {{__('backend.edit')}}</a>
                    </li>
                @endif

                {{--                @if($pickup->total_order_no > 0)--}}
                <li>
                    <a href="{{ URL::asset('/mngrAdmin/print_pickup_pdf/'.$pickup->id)}}" target="_blank"
                       data-id="{{$pickup->id}}"><i class="fa fa-print"></i> {{__('backend.pdf')}}</a>
                </li>

                @if ( $pickup->status == 2 || ($pickup->status == 1 && $pickup->driver_id) )
                    <li>
                        <a href="{{ url('mngrAdmin/receive_pickup/'.$pickup->id) }}"><i
                                class="fa fa-cart-arrow-down "></i> {{__('backend.drop_off')}}</a>
                    </li>
                @endif

{{--                @if($pickup->status == 0 )--}}
{{--                    <li>--}}
{{--                        <a id="forward_agent" data-toggle="modal" data-target="#forward_agentMo"><i--}}
{{--                                class="fa fa-user-secret" aria-hidden="true"></i> {{__('backend.Change_Agent')}}</a>--}}
{{--                    </li>--}}
{{--                @endif--}}


                @if( $pickup->status < 2 )
                    <li>
                        <a href="{{ url('mngrAdmin/cancel_pickup/'.$pickup->id) }}"><i
                                class="fa fa-cart-arrow-down "></i> {{__('backend.cancel_pickup')}}</a>
                    </li>
                    <li>
                        <a id="forward" data-toggle="modal" data-target="#exampleModal"> <i class="fa fa-share"
                                                                                            aria-hidden="true"></i> @if($pickup->status != 0 )  {{__('backend.Cancel_and')}} @else {{__('backend.forward')}} @endif
                        </a>
                    </li>
                @endif
                {{--                @endif--}}

                @if( $pickup->status == 0)

                    <li>
                        <a data-id="{{$pickup->id}}" id="delete-order"><i
                                class="fa fa-trash"></i> {{__('backend.delete')}}</a>
                    </li>
                @endif

            </ul>
        </div>

    </div>

@endsection

@section('content')


    <div class="row" style="padding: 12px 20px;">
        <div class="alert alert-danger" style="display:none"></div>

{{--        <div class="form-group col-sm-2">--}}
{{--            <label for="agent">{{__('backend.Agent')}}: </label>--}}
{{--            <p class="form-control-static">@if($pickup->agent) {{$pickup->agent->name}}  @endif </p>--}}
{{--        </div>--}}

        <div class="form-group col-sm-2">
            <label for="agent">{{__('backend.Delivery_Price')}}: </label>
            <p class="form-control-static"> {{$pickup->delivery_price}}   </p>
        </div>

        <!-- Create at: -->
        <div class="form-group col-sm-2">
            <label for="receiver_name">{{__('backend.captain_received_time')}}: </label>
            <p class="form-control-static">{{$pickup->captain_received_time}}  </p>
        </div>
        <!-- Create at: -->
        <div class="form-group col-sm-2">
            <label for="receiver_name">{{__('backend.Create_at')}}: </label>
            <p class="form-control-static">{{$pickup->created_at}}  </p>
        </div>

        <!-- Accept at: -->
        <div class="form-group col-sm-2">
            <label for="reciever_mobile"> {{__('backend.Accept_at')}} / {{__('backend.Forward_at')}} : </label>
            <p class="form-control-static">
                {{$pickup->accepted_at}}

            </p>
        </div>

        <!-- Receiver at: -->
        <div class="form-group col-sm-2">
            <label for="receiver_name">{{__('backend.Received_at')}}: </label>
            <p class="form-control-static">

                {{$pickup->received_at}}

            </p>
        </div>

        <!-- Deliver at: -->
        <div class="form-group col-sm-2">
            <label for="reciever_mobile"> {{__('backend.Deliver_at')}}: </label>
            <p class="form-control-static">

                {{$pickup->delivered_at}}
            </p>
        </div>


        <div class="form-group col-sm-2">
            <label for="reciever_mobile"> {{__('backend.orders_no')}}: </label>
            <p class="form-control-static">

                {{$pickup->is_fake_pickup ?  $pickup->orders_count : $pickup->total_order_no}}
            </p>
        </div>


        <div class="form-group col-sm-2">
            <label for="reciever_mobile"> {{__('backend.totalbounus')}}: </label>
            <p class="form-control-static">

                {{$pickup->bonus_per_order * $pickup->total_order_no}}
            </p>
        </div>

        <div class="form-group col-sm-2">
            <label for="reciever_mobile"> {{__('backend.total_price')}}: </label>
            <p class="form-control-static">

                {{($pickup->bonus_per_order * $pickup->total_order_no) + $pickup->delivery_price}}
            </p>
        </div>

        <div class="form-group col-sm-6">
            <label for="notes"> {{__('backend.notes')}}: </label>
            <p class="form-control-static">

                {{$pickup->notes}}
            </p>
        </div>

        <div class="form-group col-sm-6">
            <label for="comment"> {{__('backend.comment')}}: </label>
            <p class="form-control-static">

                {{$pickup->drop_comment}}
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
{{--            <div class="col-sm-12"--}}
{{--                 style="border: 2px solid #607d8b;color: #fff;margin-bottom: 20px;background-color: #607d8b;">--}}
{{--                <h4>{{__('backend.Receiver_Data')}}</h4>--}}
{{--            </div>--}}
{{--            <div class="form-group col-sm-6">--}}
{{--                <label for="receiver_name">{{__('backend.receiver_name')}}: </label>--}}
{{--                <p class="form-control-static">{{$pickup->receiver_name}}</p>--}}
{{--            </div>--}}
{{--            <div class="form-group col-sm-6">--}}
{{--                <label for="reciever_mobile">{{__('backend.mobile_number')}}: </label>--}}
{{--                <p class="form-control-static">{{$pickup->receiver_mobile}}</p>--}}
{{--            </div>--}}
{{--            <div class="form-group col-sm-6">--}}
{{--                <label for="reciever_address">{{__('backend.Receiver_Address')}}:</label>--}}
{{--                <p class="form-control-static">{{$pickup->receiver_address}}</p>--}}
{{--            </div>--}}

            <div class="col-sm-12"
                 style="border: 2px solid #607d8b;color: #fff;margin-bottom: 20px;background-color: #607d8b;">
                <h4>{{__('backend.Sender_Data')}}</h4>
            </div>
            <div class="form-group col-sm-6">
                <label for="sender_name">{{__('backend.sender_name')}}: </label>
                <p class="form-control-static">{{$pickup->sender_name}}</p>
            </div>
            <div class="form-group col-sm-6">
                <label for="sender_mobile">{{__('backend.mobile_number')}}: </label>
                <p class="form-control-static">{{$pickup->sender_mobile}}</p>
            </div>
            <div class="form-group col-sm-6">
                <label for="sender_address">{{__('backend.Sender_Address')}}: </label>
                <p class="form-control-static">{{$pickup->sender_address}}</p>
            </div>


            <div class="col-sm-12"
                 style="border: 2px solid #607d8b;color: #fff;margin-bottom: 20px;background-color: #607d8b;">
                <h4>{{__('backend.Driver_Data')}}</h4>
            </div>
            @if(! empty($pickup ) && ! empty($pickup->driver))
                <div class="form-group col-sm-6">
                    <label for="sender_name">{{__('backend.Driver_Name')}}: </label>
                    <p class="form-control-static">{{ ! empty($pickup ) && ! empty($pickup->driver) ? $pickup->driver->name : ''}}</p>
                </div>
                <div class="form-group col-sm-6">
                    <label for="sender_mobile">{{__('backend.mobile_number')}}: </label>
                    <p class="form-control-static">{{ ! empty($pickup ) && ! empty($pickup->driver) ? $pickup->driver->mobile : ''}}</p>
                </div>
                <div class="form-group col-sm-6">
                    <label for="email">{{__('backend.email')}}: </label>
                    <p class="form-control-static">{{! empty($pickup ) && ! empty($pickup->driver) ? $pickup->driver->email : ''}}</p>
                </div>
                <div class="form-group col-sm-6">
                    <label for="sender_address">{{__('backend.Driver_Address')}}: </label>
                    <p class="form-control-static">{{  ! empty($pickup ) && ! empty($pickup->driver) ? $pickup->driver->address : ''}}</p>
                </div>
            @else
                <div class="form-group col-sm-4 col-sm-offset-4 text-center">
                    <div class="alert alert-info" style="padding-bottom: 3px;">
                        <h4>{{__('backend.Pickup_is_not_Accepted_yet')}}</h4>
                    </div>
                </div>
            @endif


            <div class="col-sm-12"
                 style="border: 2px solid #607d8b;color: #fff;margin-bottom: 20px;background-color: #607d8b;">
                <h4>{{__('backend.Task_details')}}</h4>

            </div>
{{--            @include('backend.pickups.orders')--}}

            <table id="theTable" class="table table-condensed table-striped text-center table-bordered" style="min-height: 200px;">
                <thead>
                <tr>
                    <th>{{__('backend.task_type')}}</th>
                    <th>{{__('backend.created')}}</th>
                    <th>{{__('backend.confirmed')}}</th>
                    <th>{{__('backend.details')}}</th>

                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{__('backend.pickup')}}</td>
                    <td>
                        @if($pickup->is_pickup)
                            <span class=" text-success"><i class="fa fa-check" aria-hidden="true"></i></span>
                        @else
                            <span class=" text-danger"><i class="fa fa-times" aria-hidden="true"></i></span>
                        @endif
                    </td>
                    <td>
                        @if($pickup->confirm_pickup)
                            <span class=" text-success"><i class="fa fa-check" aria-hidden="true"></i></span>
                        @else
                            <span class=" text-danger"><i class="fa fa-times" aria-hidden="true"></i></span>
                        @endif
                    </td>
                    <td>
                        {{$pickup->pickup_orders}}
                    </td>
                </tr>
                <tr>
                    <td>{{__('backend.cache')}}</td>
                    <td>
                        @if($pickup->is_cash_collect)
                            <span class=" text-success"><i class="fa fa-check" aria-hidden="true"></i></span>
                        @else
                            <span class=" text-danger"><i class="fa fa-times" aria-hidden="true"></i></span>
                        @endif
                    </td>
                    <td>
                        @if($pickup->confirm_cash_collect)
                            <span class=" text-success"><i class="fa fa-check" aria-hidden="true"></i></span>
                        @else
                            <span class=" text-danger"><i class="fa fa-times" aria-hidden="true"></i></span>
                        @endif
                    </td>
                    <td>
                        {{$pickup->cash_money}}
                    </td>
                </tr>
                <tr>
                    <td>{{__('backend.recall')}}</td>
                    <td>
                        @if($pickup->is_recall_orders)
                            <span class=" text-success"><i class="fa fa-check" aria-hidden="true"></i></span>
                        @else
                            <span class=" text-danger"><i class="fa fa-times" aria-hidden="true"></i></span>
                        @endif
                    </td>
                    <td>
                        @if($pickup->confirm_recall_orders)
                            <span class=" text-success"><i class="fa fa-check" aria-hidden="true"></i></span>
                        @else
                            <span class=" text-danger"><i class="fa fa-times" aria-hidden="true"></i></span>
                        @endif
                    </td>
                    <td>
                        {{$pickup->recall_orders}}
                    </td>
                </tr>
                </tbody>
            </table>

            @if(count($pickup->refund_collections))
                <table class="table table-condensed table-striped text-center">
                    <caption><h3 class="text-center">{{__('backend.refund_collections')}}</h3></caption>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{__('backend.serial_code')}}</th>
                        <th>{{__('backend.count_orders')}}</th>
                        <th>{{__('backend.Create_at')}}</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($pickup->refund_collections as $collection)
                        <tr>
                            <td>{{$collection->id}}</td>
                            <td>{{$collection->serial_code}}</td>
                            <td>{{$collection->orders_count}}</td>
                            <td>{{$collection->created_at}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
            <div class="col-md-12 col-xs-12 col-sm-12">
                <a class="btn btn-link pull-right" href="{{URL::previous()}}"><i
                        class="fa fa-backward"></i> {{__('backend.back')}}</a>

            </div>
        </div>
    </div>

    @include('backend.pickups.driver_forward_modal')
{{--    @include('backend.pickups.agent_forward_modal')--}}
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

    <script>

        $(function () {

            $(".editPickup").on('click', function () {
                $('#delivery_price_modal').val($(this).attr('data-bonous'))
                $('#order_id_modal').val($(this).attr('data-id'));
                $('#editPickupModal').modal('show')

            });
            $(".delete-order-pickup").on('click', function () {
                var orderid = $(this).attr('data-id');
                var pickid = $(this).attr('data-pickup');
                $.confirm({
                    title: '',
                    theme: 'modern',
                    class: 'danger',
                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Confirm Delete</span></div><div style="font-weight:bold;"><p>Are You sure To Delete This Order From PickUp ?</p></div>',
                    buttons: {
                        confirm: {
                            text: 'Delete Order From PickUp',
                            btnClass: 'btn-danger',
                            action: function () {
                                $.ajax({
                                    url: "{{ url('/mngrAdmin/delete_order_pickup/')}}",
                                    type: 'DELETE',
                                    data: {'_token': "{{csrf_token()}}", 'order_id': orderid, 'pickup_id': pickid},
                                    success: function (data) {

                                        if (data != 'false') {

                                            location.reload();
                                        } else {
                                            jQuery('.alert-danger').append('<p>Something Error</p>');
                                        }
                                        window.scrollTo({top: 0, behavior: 'smooth'});
                                    },
                                    error: function (data) {
                                        console.log('Error:', data);
                                    }
                                });
                            }
                        },
                        cancel: {
                            text: 'Cancel',
                            action: function () {
                            }
                        }
                    }
                });
            });

            $("#delete-order").on('click', function () {
                var id = $(this).attr('data-id');
                $.confirm({
                    title: '',
                    theme: 'modern',
                    class: 'danger',
                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Confirm Delete</span></div><div style="font-weight:bold;"><p>Are You sure To Delete This PickUp ?</p></div>',
                    buttons: {
                        confirm: {
                            text: 'Delete PickUp',
                            btnClass: 'btn-danger',
                            action: function () {
                                $.ajax({
                                    url: "{{ url('/mngrAdmin/pickups/')}}" + "/" + id,
                                    type: 'DELETE',
                                    data: {'_token': "{{csrf_token()}}"},
                                    success: function (data) {

                                        if (data != 'false') {

                                            setTimeout(function () {
                                                location.reload();
                                                window.location.href = "{{ url('/mngrAdmin/pickups')}}";
                                            }, 2000);
                                        } else {
                                            jQuery('.alert-danger').append('<p>Something Error</p>');
                                        }
                                        window.scrollTo({top: 0, behavior: 'smooth'});
                                    },
                                    error: function (data) {
                                        console.log('Error:', data);
                                    }
                                });
                            }
                        },
                        cancel: {
                            text: 'Cancel',
                            action: function () {
                            }
                        }
                    }
                });
            })

        });

    </script>

@endsection

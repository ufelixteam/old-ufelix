<!-- Start Services -->

<section id="services" class="services-x">
	<div class="container">
    	<!-- Start Title -->
    	<div class="col-xs-12">
        	<div class="title">
            	<h3><?php echo app('translator')->getFromJson('website.services'); ?></h3>
                <p>
                	 <?php if(! empty($app_about) ): ?>  <?php echo e(app()->getLocale() == 'en' ? $app_about['service'] : $app_about['service_ar']); ?> <?php endif; ?>
                </p>
            </div>
        </div>
        <!-- End Title -->
    	
        <?php if(! empty($services ) && count($services) > 0): ?>
            <?php $__currentLoopData = $services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-md-4 col-xs-12">
                	<a href="#" class="block-serv">
                    	<div class="img-block">
                        	<div class="img" style="background-image: url(<?php echo e($service->image); ?>);"></div>
                        </div>
                        <div class="block-details">
                        	<h2><?php echo e($service->title); ?></h2>
                            <p>
                               <?php echo e($service->description); ?>

                            </p>
                        </div>
                    </a>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            
        <?php endif; ?>
       
        
    </div>
</section>
<!-- End Services -->
<?php /**PATH /home/www/ufelix/ufelix/resources/views/frontend/home/services.blade.php ENDPATH**/ ?>
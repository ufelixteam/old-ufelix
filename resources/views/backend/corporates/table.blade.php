<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        @if($corporates->count())
            <table class="table table-condensed table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('backend.name')}}</th>
                    <th>{{__('backend.shipping_type')}}</th>
                    <th>{{__('backend.order_type')}}</th>
                    <th>{{__('backend.government')}}</th>
                    <th>{{__('backend.account_type')}}</th>
                    <th>{{__('backend.mobile_number')}}</th>
                    <th>{{__('backend.responsible')}}</th>
                    <th>{{__('backend.active')}}</th>
                    <th>{{__('backend.in_slider')}}</th>
                    <th>{{__('backend.created_at')}}</th>
                    <th>
                        <input type="button" class="btn btn-info btn-xs" id="toggle"
                               value="Select" onClick="do_this()"/>
                        <button type="button" id="pdf-submit"
                                class="btn btn-warning btn-pdf btn-xs ">{{__('backend.print')}}</button>
                    </th>
                    {{--                    <th class="text-right"></th>--}}
                </tr>
                </thead>
                <tbody>
                @foreach($corporates as $corporate)
                    <tr @if(permission('showCorporate')) class='clickable-row'
                        data-href="{{ route('mngrAdmin.corporates.show', $corporate->id) }}"
                        @endif @if($corporate->is_something_wrong) style='background: #9e8f9e; color: #fff' @endif>
                        <td>{{$corporate->id}}</td>
                        <td>{{$corporate->name}}</td>
                        <td>{{$corporate->type_of_shipping}}</td>
                        <td>
                            {{isset($corporate->type->name_en) ? $corporate->type->name_en : '-'}}
                        </td>
                        <td>
                            {{isset($corporate->government->name_en) ? $corporate->government->name_en : '-'}}
                        </td>
                        <td>{{$corporate->type_of_account}}</td>
                        <td>{{$corporate->mobile}}</td>
                        <td>{{isset($corporate->responsible_user->name) ? $corporate->responsible_user->name : '-'}}</td>
                        <td class="exclude-td">
                            <input data-id="{{$corporate->id}}" data-size="mini" class="toggle change_active"
                                   {{$corporate->is_active == 1 ? 'checked' : ''}} data-onstyle="success"
                                   type="checkbox" data-style="ios" data-on="Yes" data-off="No">

                        </td>

                        <td class="exclude-td">

                            <input data-id="{{$corporate->id}}" data-size="mini" class="toggle change_slider"
                                   {{$corporate->in_slider == 1 ? 'checked' : ''}} data-onstyle="success"
                                   type="checkbox" data-style="ios" data-on="Yes" data-off="No">

                        </td>

                        <td>{{$corporate->created_at}}</td>
                        <td class="exclude-td selectpdf" id="selectpdf">
                            <input type="checkbox" name="select[]" value="{{$corporate->id}}"
                                   class="selectOrder"
                                   data-value="{{$corporate->id}}"/>

                        </td>
                        {{--                        <td class="exclude-td">--}}
                        {{--                        @if(permission('showOrdersMember')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->--}}
                        {{--                            <a class="btn btn-xs btn-default"--}}
                        {{--                               href="{{url('mngrAdmin/orders/showOrders') . '/2' . '/' . $corporate->id}}"><i--}}
                        {{--                                    class="glyphicon glyphicon-list"></i> {{__('backend.the_orders')}}</a>--}}
                        {{--                        @endif--}}

                        {{--                        --}}{{--                        @if(permission('showCorporate')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->--}}
                        {{--                        --}}{{--                            <a class="btn btn-xs btn-primary"--}}
                        {{--                        --}}{{--                               href="{{ route('mngrAdmin.corporates.show', $corporate->id) }}"><i--}}
                        {{--                        --}}{{--                                    class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>--}}
                        {{--                        --}}{{--                        @endif--}}

                        {{--                        @if(permission('editCorporate')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->--}}
                        {{--                            <a class="btn btn-xs btn-warning"--}}
                        {{--                               href="{{ route('mngrAdmin.corporates.edit', $corporate->id) }}"><i--}}
                        {{--                                    class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>--}}
                        {{--                            @endif--}}

                        {{--                        </td>--}}
                    </tr>
                @endforeach
                </tbody>
            </table>

            {!! $corporates->appends($_GET)->links() !!}

        @else
            <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}!</h3>
        @endif
    </div>
</div>

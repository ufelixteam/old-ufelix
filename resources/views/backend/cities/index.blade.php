@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.cities')}}</title>
@endsection

@section('header')
<div class="page-header clearfix">
  <h3>
    <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.cities')}}
    @if(permission('addCitie')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
      <a class="btn btn-success pull-right" href="{{ route('mngrAdmin.cities.create') }}"><i class="glyphicon glyphicon-plus"></i> {{__('backend.create')}} </a>
    @endif
  </h3>
</div>
@endsection

@section('content')
  <div class="col-md-12">
     <div class="group-control col-md-4 col-sm-4" style="margin: 0px 0px 25px 0px">
       <select id="city-field" class="form-control">
         <option value="-1">{{__('backend.sort_by_city')}}</option>
         @foreach ($list_governorates as $list): ?>
         <option value="{{$list->id}}">
           @if(session()->has('lang') == 'ar')
            {{$list->name_ar}}
           @else
            {{$list->name_en}}
           @endif
         </option>
         @endforeach
       </select>
     </div>
  </div>

  <div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12 list">
      @include('backend.cities.table')
    </div>
  </div>
@endsection
@section('scripts')
<script>

$('#theTable').DataTable({
  "pagingType": "full_numbers"
});

$("#city-field").on('change', function() {

  // Wait icon
  	$('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
  	$.ajax({
        url: '{{URL::asset("/mngrAdmin/cities")}}'+'?filter='+$(this).val(),
        type: 'get',
        data: $("#city-field").serialize(),
        success: function(data) {
            $('.list').html(data.view);
            $('#theTable').DataTable({
              "pagingType": "full_numbers"
            });
        },
        error: function(data) {
            console.log('Error:', data);
        }
   	});
});

</script>

@endsection

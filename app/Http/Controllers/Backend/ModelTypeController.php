<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\ModelType;
use Response;
use Flash;
use File;
use Auth;

class ModelTypeController extends Controller {

	// Get All Veichle model
	public function index()
	{
		$model_types = ModelType::orderBy('id', 'desc')->paginate(50);
		return view('backend.model_types.index', compact('model_types'));
	}

	// Create Veichle Model Page
	public function create()
	{
		return view('backend.model_types.create');
	}

	// Added New Veichle Model
	public function store(Request $request)
	{
				/*
				* status = 0 :: Not Verify
				* status = 1 :: Verify
				*/
				$this->validate($request,[
					 'name'   		=> 'required',
					 'status'   	=> 'required',
				]);
				$model_type 					= new ModelType();
				$model_type->name 		= $request->input("name");
        $model_type->status 	= $request->input("status");
				$model_type->save();

				// Function Add Action In Activity Log By Samir
				addActivityLog('Add New Model Type Vehicle ' . $model_type->name , ' اضافة طراز مركبة جديدة ' . $model_type->name, 22); // Helper Function in App\Helpers.php

        Flash::success(__('backend.Model_Type_saved_successfully'));
				return redirect()->route('mngrAdmin.model_types.index');
	}

	// View One Veichle Model
	public function show($id)
	{
		$model_type = ModelType::findOrFail($id);
		return view('backend.model_types.show', compact('model_type'));
	}

	// Edit Veichle Model Page
	public function edit($id)
	{
		$model_type = ModelType::findOrFail($id);
		return view('backend.model_types.edit', compact('model_type'));
	}

	// Update Veichle Model
	public function update(Request $request, $id)
	{
			/*
			* status = 0 :: Not Verify
			* status = 1 :: Verify
			*/
			$this->validate($request,[
				 'name'   		=> 'required',
				 'status'   	=> 'required',
			]);
			$model_type 					= ModelType::findOrFail($id);
			$model_type->name 		= $request->input("name");
      $model_type->status 	= $request->input("status");
			$model_type->save();

			// Function Add Action In Activity Log By Samir
			addActivityLog('Edit Model Type Vehicle ' . $model_type->name , ' تعديل طراز المركبة ' . $model_type->name, 22); // Helper Function in App\Helpers.php

      Flash::warning(__('backend.Model_Type_Updated_successfully'));
			return redirect()->route('mngrAdmin.model_types.index');
	}

	// Delete Veichel Model
	public function destroy($id)
	{
			$model_type = ModelType::findOrFail($id);
			$model_type->delete();

			// Function Add Action In Activity Log By Samir
			addActivityLog('Delete Model Type Vehicle ' . $model_type->name , ' حذف طراز المركبة ' . $model_type->name, 22); // Helper Function in App\Helpers.php

      flash(__('backend.Model_Type_deleted_successfully'))->error();
			return redirect()->route('mngrAdmin.model_types.index');
	}

}

@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.admin_roles')}}</title>
@endsection

@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.admin_roles')}}
            @if(permission('addAdminRole')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
              <a class="btn btn-success pull-right" href="{{ route('mngrAdmin.admin_roles.create') }}"><i class="glyphicon glyphicon-plus"></i> {{__('backend.create')}}</a>
            @endif
        </h3>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($adminRoles->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th class="text-center">{{__('backend.id')}} #</th>
                            <th class="text-center">{{__('backend.name')}}</th>
                            <th class="text-center">{{__('backend.Create_at')}}</th>
                            <th class="text-right"></th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($adminRoles as $role)
                            <tr>
                                <td class="text-center">{{$role->id}}</td>
                                <td class="text-center">{{$role->name}}</td>
                                <td class="text-center">{{$role->created_at}}</td>
                                <td class="text-right">
                                  @if(permission('showAdminRole')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                    <a class="btn btn-xs btn-primary" href="{{ route('mngrAdmin.admin_roles.show', $role->id) }}"><i class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>
                                  @endif

                                  @if(permission('editAdminRole')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                    <a class="btn btn-xs btn-warning" href="{{ route('mngrAdmin.admin_roles.edit', $role->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                                  @endif
                                  @if(permission('deleteAdminRole')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                      <form action="{{ route('mngrAdmin.admin_roles.destroy', $role->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
                                          <input type="hidden" name="_method" value="DELETE">
                                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                          <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}</button>
                                      </form>
                                      @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
            @endif

        </div>
    </div>

@endsection

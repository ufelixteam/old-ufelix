<form action="{{url('/customer/account/orders/add_order_comment')}}" method="POST" id="comments_form">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="order_id" value="{{ @$order->id }}">
    <input type="hidden" name="user_type" value="2">
    <div class="modal" tabindex="-1" role="dialog" id="commentModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-header">
                        <h5 class="modal-title">{{__('backend.add_order_comment')}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="form-group col-md-12 col-xs-12 col-sm-12">
                                <label for="order_price-field">{{__('backend.comment')}} *: </label>
                                <textarea class="form-control" name="comment" id="comment" rows="3"
                                          style="min-height: 100px; resize: horizontal" required></textarea>
                            </div>

                        </div>

                    </div>
                    <div class="modal-footer">

                        <button type="submit" class="btn btn-secondary" id="save_comment" style="background: #0b2e13; color: white">
                            {{__('backend.save')}}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

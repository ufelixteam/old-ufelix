
<?php $__env->startSection('css'); ?>
  <title><?php echo e(__('backend.corporate')); ?> - <?php echo e(__('backend.view')); ?></title>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <style>
        .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20px; }
        .toggle.ios .toggle-handle { border-radius: 20px; }
    </style>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('header'); ?>


<div class="page-header">
	<h3><?php echo e(__('backend.view')); ?> <?php echo e(__('backend.corporate')); ?> - <?php echo e($corprate->name); ?> </h3> <?php echo $corprate->active_span; ?> <?php echo e($corprate->created_at); ?>

	<form action="<?php echo e(route('mngrAdmin.corporates.destroy', $corprate->id)); ?>" method="POST" style="display: inline;" onsubmit="if(confirm('<?php echo e(__('Backend.msg_confirm_delete')); ?>')) { return true } else {return false };">
		<input type="hidden" name="_method" value="DELETE">
		<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
		<div class="btn-group pull-right" role="group" aria-label="...">
      <?php if(permission('activeCorporate')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
        <button id="active" type="button" value="<?php echo e($corprate->id); ?>"  class="btn btn-info"><?php echo e($corprate->active_txt); ?></button>
      <?php endif; ?>

      <?php if(permission('editCorporate')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
        <a class="btn btn-warning btn-group" role="group" href="<?php echo e(route('mngrAdmin.corporates.edit', $corprate->id)); ?>"><i class="glyphicon glyphicon-edit"></i> <?php echo e(__('backend.edit')); ?></a>
      <?php endif; ?>

      <?php if(permission('deleteCorporate')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
        <button type="submit" class="btn btn-danger"><?php echo e(__('backend.delete')); ?> <i class="glyphicon glyphicon-trash"></i></button>
      <?php endif; ?>
    </div>
	</form>
</div>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="col-sm-12" style="border: 2px solid #fdb801;color: #fff;margin-bottom: 20px;margin-top: 20px;background-color: #fdb801;" >
	<h5><?php echo e(__('backend.company_information')); ?></h5>
</div>
<div class="row">
	<div class="col-md-12 col-xs-12 col-sm-12">
		<div class="form-group col-sm-4">
			<label for="name"><?php echo e(__('backend.email')); ?></label>
			<p class="form-control-static"><?php echo e($corprate->email); ?></p>
		</div>
		<div class="form-group col-sm-4">
			<label for="name"><?php echo e(__('backend.mobile')); ?></label>
			<p class="form-control-static"><?php echo e(isset($corprate->mobile) ? $corprate->mobile : __("backend.not_found")); ?></p>
		</div>
		<div class="form-group col-sm-4">
			<label for="name"><?php echo e(__('backend.mobile_number2')); ?></label>
			<p class="form-control-static"> <?php echo e(isset($corprate->phone) ? $corprate->phone : __("backend.not_found")); ?></p>
		</div>

		<div class="form-group col-sm-4">
			<label for="mobile"><?php echo e(__('backend.field')); ?></label>
			<p class="form-control-static"><?php echo e($corprate->field); ?></p>
		</div>

		<div class="form-group col-sm-4">
			<label for="phone"><?php echo e(__('backend.commercial_record_number')); ?></label>
			<p class="form-control-static"><?php echo e($corprate->commercial_record_number); ?></p>
		</div>





    <div class="form-group col-sm-4">
			<label for="phone"><?php echo e(__('backend.Officail_site')); ?></label>
			<p class="form-control-static"><?php echo e($corprate->company_website); ?></p>
		</div>
    <div class="form-group col-sm-4">
      <label for="phone"><?php echo e(__('backend.government')); ?></label>
      <p class="form-control-static">
        <?php if(\App\Models\Governorate::where('id', $corprate->government_id)): ?>
          <?php echo e(\App\Models\Governorate::where('id', $corprate->government_id)->value('name_en')); ?>

        <?php else: ?>
          <?php echo e(__('backend.not_found')); ?>

        <?php endif; ?>
      </p>
    </div>
    <div class="form-group col-sm-6">
      <label for="phone"><?php echo e(__('backend.city')); ?></label>
      <p class="form-control-static">
        <?php if(\App\Models\City::where('id', $corprate->government_id)): ?>
          <?php echo e(\App\Models\City::where('id', $corprate->city_id)->value('name_en')); ?>

        <?php else: ?>
          <?php echo e(__('backend.not_found')); ?>

        <?php endif; ?>
      </p>
    </div>

    <div class="form-group col-sm-12">
      <label for="notes"><?php echo e(__('backend.ContractNote')); ?></label>
      <p class="form-control-static"><?php echo e($corprate->notes); ?></p>
    </div>


<div class="form-group col-sm-6">
    <label for="logo"><?php echo e(__('backend.logo')); ?></label>
    <p class="form-control-static"><?php if($corprate->logo): ?>
      <img src="<?php echo e($corprate->logo); ?>"
      style="width:100px;height:100px">


    <?php endif; ?></p>
  </div>


	<div class="form-group col-sm-6">
		<label for="commercial_record_image"><?php echo e(__('backend.ContractForm')); ?></label>
		<p class="form-control-static"><?php if($corprate->commercial_record_image): ?>
      <?php 
      $myString = $corprate->commercial_record_image;
      $myStringParts = explode('.', $corprate->commercial_record_image);
      ?>
      <?php if(substr($myString, -4) == '.pdf'): ?>
			
      <embed src="<?php echo e($corprate->commercial_record_image); ?>" width="100%" height="300px">
        <?php else: ?>
        <img src="<?php echo e($corprate->commercial_record_image); ?>"
      style="width:100px;height:100px">
      
      <?php endif; ?>
		<?php endif; ?></p>
	</div>
	</div>

</div>


<?php if($corprate->customers): ?>
  <div class="col-sm-12" style="border: 2px solid #fdb801;color: #fff;margin-bottom: 20px;margin-top: 20px;background-color: #fdb801;" >
		<h5><?php echo e(__('backend.users_information')); ?></h5>
	</div>
<div class="row">

	<div class="form-group col-sm-12">
		<table class="table">
			<tr>
				<th>#</th>
				<th><?php echo e(__('backend.name')); ?></th>
				<th><?php echo e(__('backend.email')); ?></th>
				<th><?php echo e(__('backend.mobile')); ?></th>
        <th><?php echo e(__('backend.block')); ?></th>
        <th><?php echo e(__('backend.verify_email')); ?></th>
        <th><?php echo e(__('backend.verfiy_mobile')); ?></th>
				<th></th>
			</tr>

				<?php $__currentLoopData = $corprate->customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
        	<td><?php echo e($value->id); ?></td>
        	<td><?php echo e(! empty($value->name)?$value->name:''); ?></td>
        	<td><?php echo e(! empty($value->email)?$value->email:''); ?></td>
        	<td><?php echo e(! empty($value->mobile)?$value->mobile:''); ?></td>
         
           <td><input data-id="<?php echo e($value->id); ?>" data-size="mini" class="toggle change_block"  <?php echo e($value->is_block == 1 ? 'checked' : ''); ?> data-onstyle="danger" type="checkbox" data-style="ios" data-on="Yes" data-off="No"> </td>

            <td><input data-id="<?php echo e($value->id); ?>" data-size="mini" class="toggle verify_email"  <?php echo e($value->is_verify_email == 1 ? 'checked' : ''); ?> data-onstyle="success" type="checkbox" data-style="ios" data-on="Yes" data-off="No" ></td>
            <td><input data-id="<?php echo e($value->id); ?>" data-size="mini" class="toggle verify_mobile"  <?php echo e($value->is_verify_mobile == 1 ? 'checked' : ''); ?> data-onstyle="success" type="checkbox" data-style="ios" data-on="Yes" data-off="No" ></td>

            <td>
    				<a class="btn btn-xs btn-primary" href="<?php echo e(route('mngrAdmin.customers.show', $value->id)); ?>"><i class="glyphicon glyphicon-eye-open"></i> <?php echo e(__('backend.view')); ?></a>
            <a class="btn btn-xs btn-warning" href="<?php echo e(route('mngrAdmin.customers.edit', $value->id)); ?>"><i class="glyphicon glyphicon-edit"></i> <?php echo e(__('backend.edit')); ?></a>
    			</td>
        </tr>



				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


		</table>
	</div>
</div>
<?php endif; ?>

<div class="col-sm-12" style="border: 2px solid #fdb801;color: #fff;margin-bottom: 20px;margin-top: 20px;background-color: #fdb801;" >
	<h5><?php echo e(__('backend.corporate_target')); ?></h5>
</div>


<div class="row">
  	<div class="form-group col-sm-12">
      <?php if(count($c_targets) > 0): ?>
    		<table class="table">
    			<tr>
            <td></td>
    				<th>#<?php echo e(__('backend.id')); ?></th>
    				<th><?php echo e(__('backend.target_from')); ?></th>
    				<th><?php echo e(__('backend.target_to')); ?></th>
    				<th><?php echo e(__('backend.discount_percentage')); ?></th>
    				<th></th>
    			</tr>
    				<?php $__currentLoopData = $c_targets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $target): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <?php if($corprate->corporate_target_id == $target->id): ?>
                <tr style="background-color: #ccc; font-weight: bold; color: #000">
                <td><input type="checkbox" checked disabled></td>
              <?php else: ?>
                <tr>
                <td><input type="checkbox" disabled></td>
              <?php endif; ?>

                	<td><?php echo e($target->id); ?></td>
                	<td><?php echo e($target->corporate_target_from); ?></td>
                	<td><?php echo e($target->corporate_target_to); ?></td>
                	<td><?php echo e($target->corporate_discount); ?> %</td>
                	<td>
                    <a class="btn btn-xs btn-info" data-toggle="modal" data-target="#editTarget_<?php echo e($target->id); ?>"><i class="glyphicon glyphicon-edit"></i> <?php echo e(__('backend.edit')); ?></a>
                    <form action="<?php echo e(route('mngrAdmin.CorporateTarget.destroy', $target->id)); ?>" method="POST" style="display: inline;" onsubmit="if(confirm('<?php echo e(__('backend.msg_confirm_delete')); ?>')) { return true } else {return false };">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> <?php echo e(__('backend.delete')); ?></button>
                    </form>
                  </td>
                </tr>

                <!-- Model Add Target For Corporate -->
                <div class="row">
                  <!-- Modal -->
                  <div class="modal fade" id="editTarget_<?php echo e($target->id); ?>" role="dialog">
                    <div class="modal-dialog" style="width: 80%;">

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title text-center"> <?php echo e(__('backend.edit_target')); ?></h4>
                        </div>

                        <form action="<?php echo e(route('mngrAdmin.CorporateTarget.update', $target->id)); ?>" method="POST" name="validateForm">
                          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                          <input type="hidden" name="_method" value="PUT">
                          <div class="modal-body">
                            <div class="row">
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                  <div class="form-main">
                                    <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('corporate_target_from')): ?> has-error <?php endif; ?>">
                                        <label for="corporate_target_from_field"><?php echo e(__('backend.target_from')); ?></label>
                                        <input type="number" min="1" id="corporate_target_from_field" name="corporate_target_from" class="form-control" value="<?php echo e($target->corporate_target_from); ?>" />
                                       <?php if($errors->has("corporate_target_from")): ?>
                                        <span class="help-block"><?php echo e($errors->first("corporate_target_from")); ?></span>
                                       <?php endif; ?>
                                    </div>
                                    <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('corporate_target_to')): ?> has-error <?php endif; ?>">
                                        <label for="corporate_target_to_field"><?php echo e(__('backend.target_to')); ?></label>
                                        <input type="number" min="1" id="corporate_target_to_field" name="corporate_target_to" class="form-control" value="<?php echo e($target->corporate_target_to); ?>" />
                                       <?php if($errors->has("corporate_target_to")): ?>
                                        <span class="help-block"><?php echo e($errors->first("corporate_target_to")); ?></span>
                                       <?php endif; ?>
                                    </div>
                                    <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('corporate_discount')): ?> has-error <?php endif; ?>">
                                      <label for="corporate_discount_field"><?php echo e(__('backend.discount_percentage')); ?></label>
                                        <div class='input-group'>
                                              <input type="text" id="corporate_discount_field" name="corporate_discount" class="form-control" value="<?php echo e($target->corporate_discount); ?>"/>
                                              <div class="input-group-addon">
                                                  <i class="fa fa-percent"></i>
                                              </div>
                                         </div>
                                         <?php if($errors->has("corporate_discount")): ?>
                                          <span class="help-block"><?php echo e($errors->first("corporate_discount")); ?></span>
                                         <?php endif; ?>
                                    </div>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="form-group col-md-12 col-sm-6 <?php if($errors->has('report')): ?> has-error <?php endif; ?>">
                                    <label for="report-field"><?php echo e(__('backend.report')); ?>: </label>
                                    <textarea class="form-control" id="report-field" rows="4" name="report"><?php echo e($target->report); ?></textarea>
                                    <?php if($errors->has("report")): ?>
                                      <span class="help-block"><?php echo e($errors->first("report")); ?></span>
                                    <?php endif; ?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="modal-footer">
                            <button type="submit" class="btn btn-primary"><?php echo e(__('backend.create')); ?></button>
                          </div>
                        </form>

                      </div>
                    </div>
                  </div>
                </div>
    				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    		</table>
      <?php else: ?>
        <div class="text-center"><b><?php echo e(__('backend.this_corporate_not_have_any_target')); ?></b></div>
      <?php endif; ?>
  	</div>
  </div>

<div class="row">
	<div class="col-md-12 col-xs-12 col-sm-12">
		<a class="btn btn-link pull-right" href="<?php echo e(route('mngrAdmin.corporates.index')); ?>"><i class="glyphicon glyphicon-backward"></i>  <?php echo e(__('backend.back')); ?></a>
	</div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script>
  $('.toggle').bootstrapToggle();
	$(function () {
 		$('#active').on('click', function ()
		{
       		var Status = $(this).val();
        	$.ajax({
            	url: "<?php echo e(url('/mngrAdmin/corporate/active/'.$corprate->id)); ?>",
            	Type:"GET",
            	dataType : 'json',
        		success: function(response){
        			if(response==0){
        				$('#active').html('Active');

        			}else{
        				$('#active').html('UnActive');
        			}
            		location.reload();
   				}
        	});
    	});

    $('.verify_mobile').on('change',function(){
    let id = $(this).attr('data-id');
    $.ajax({
        url: '<?php echo e(URL::asset("/mngrAdmin/verify-mobile-customer")); ?>',
        type: 'post',
        data:{id:id,_token:"<?php echo e(csrf_token()); ?>"},
        success: function(data) {

        },
        error: function(data) {
            console.log('Error:', data);
        }
    });
})

$('.verify_email').on('change',function(){
    let id = $(this).attr('data-id');
    $.ajax({
        url: '<?php echo e(URL::asset("/mngrAdmin/verify-email-customer")); ?>',
        type: 'post',
        data:{id:id,_token:"<?php echo e(csrf_token()); ?>"},
        success: function(data) {

        },
        error: function(data) {
            console.log('Error:', data);
        }
    });
})

$('.change_block').on('change',function(){
    let id = $(this).attr('data-id');
    $.ajax({
        url: '<?php echo e(URL::asset("/mngrAdmin/change-block-customer")); ?>',
        type: 'post',
        data:{id:id,_token:"<?php echo e(csrf_token()); ?>"},
        success: function(data) {

        },
        error: function(data) {
            console.log('Error:', data);
        }
    });
  })
	});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/corporates/show.blade.php ENDPATH**/ ?>
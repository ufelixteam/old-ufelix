<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">

    <style>
        td, th {
            text-align: center;
            vertical-align: middle;
        }

    </style>
</head>

<body>
<table>
    <tr>
        <th>No</th>
        <th>
            ID
        </th>
        <th>
            CREATE DATE
        </th>
        <th>
            RECEIVED DATE
        </th>
        <th>
            LAST STATUS DATE
        </th>
        <th>
            STATUS
        </th>
        <th>
            WAREHOUSE
        </th>
        <th>
            COLLECTED COST
        </th>
        @if($type == 1)
            <th>
                CORPORATE
            </th>
            <th>
                USER
            </th>
        @endif
        <th>
            Client Code
        </th>
        <th>
            Captain
        </th>
        <th>
            ORDER NUMBER
        </th>
        <th>
            CODE
        </th>
        <th>
            RECEIVER NAME
        </th>
        <th>
            RECEIVER ADDRESS
        </th>
        <th>
            RECEIVER MOBILE
        </th>
        <th>
            RECEIVER MOBILE2
        </th>
        <th>
            GOV
        </th>
        <th>
            CITY
        </th>
        <th>
            NOTES
        </th>
        <th>
            TYPE
        </th>
        <th>
            TOTAL
        </th>
        <th>
            CLIENT
        </th>
        <th>
            UFELIX
        </th>
        <th>
            DELAY1
        </th>
        <th>
            Date1
        </th>
        <th>
            DELAY2
        </th>
        <th>
            Date2
        </th>
        <th>
            DELAY3
        </th>
        <th>
            Date3
        </th>
        <th>
            DELAY4
        </th>
        <th>
            Date4
        </th>
        <th>
            DELAY5
        </th>
        <th>
            Date5
        </th>
        <th>
            DELAY6
        </th>
        <th>
            Date6
        </th>
        <th>
            DELAY7
        </th>
        <th>
            Date7
        </th>
        <th>
            DELIVERY PROBLEMS1
        </th>
        <th>
            DATE1
        </th>
        <th>
            DELIVERY PROBLEMS2
        </th>
        <th>
            DATE2
        </th>
        <th>
            DELIVERY PROBLEMS3
        </th>
        <th>
            DATE3
        </th>
        <th>
            DELIVERY PROBLEMS4
        </th>
        <th>
            DATE4
        </th>
        <th>
            DELIVERY PROBLEMS5
        </th>
        <th>
            DATE5
        </th>
        <th>
            DELIVERY PROBLEMS6
        </th>
        <th>
            DATE6
        </th>
        <th>
            DELIVERY PROBLEMS7
        </th>
        <th>
            DATE7
        </th>
    </tr>
    @php
        $i=1;
    @endphp
    @if(! empty($orders))
        @foreach($orders->cursor() as $order)
            <tr>
                <td>{{$i}}</td>
                <td>
                    {{$order->id}}
                </td>
                <td>
                    {{$order->created_at ? date('Y-m-d', strtotime($order->created_at)) : ''}}
                </td>
                <td>
                    {{$order->received_at ? date('Y-m-d', strtotime($order->received_at)) : ''}}
                </td>
                <td>
                    {{$order->last_status_date ? date('Y-m-d', strtotime($order->last_status_date)) : ''}}
                </td>
                <td>
                    {!!$order->status_details_without_refund!!}
                </td>
                <td>
                    @if($order->is_refund == 1)
                        Refund
                    @elseif($order->client_dropoff == 1)
                        Client
                    @elseif($order->warehouse_dropoff == 1)
                        Warehouse
                    @endif
                </td>
                <td>
                    {{$order->collected_cost}}
                </td>
                @if($type == 1)
                    <td>
                        {{@$order->customer->Corporate->name}}
                    </td>
                    <td>
                        {{$order->customer->name}}
                    </td>
                @endif
                <td>
                    {{$order->reference_number}}
                </td>
                <td>
                    @if(! empty($order->accepted ) && ! empty($order->accepted->driver))
                        {{$order->accepted->driver->name}}
                    @else
                        -
                    @endif
                </td>
                <td>
                    {{$order->order_no}}
                </td>
                <td>
                    {{$order->order_number}}
                </td>
                <td>
                    {{$order->receiver_name}}
                </td>
                <td>
                    {{mb_substr(htmlentities(strip_tags($order->receiver_address)),0,150, "utf-8")}}
                </td>
                <td>
                    {{$order->receiver_mobile}}
                </td>
                <td>
                    {{$order->receiver_phone}}
                </td>
                <td>
                    {{isset($order->to_government->name_en) ? $order->to_government->name_en : ''}}
                </td>
                <td>
                    {{isset($order->to_city->name_en) ? $order->to_city->name_en : ''}}
                </td>
                <td>
                    {{$order->notes}}
                </td>
                <td>
                    {{isset($order->types->name_en) ? $order->types->name_en : ''}}
                </td>
                <td>
                    {{$order->total_price}}
                </td>
                <td>
                    {{$order->total_price - $order->delivery_price}}
                </td>
                <td>
                    {{$order->delivery_price}}
                </td>
                @foreach($order->order_delay->take(7) as $t => $order_delay)
                    <td>
                        {{$order_delay->delay_comment ? $order_delay->delay_comment : ''}}
                    </td>
                    <td>
                        {{$order_delay->delay_at ? date('Y-m-d', strtotime($order_delay->delay_at)) : ''}}
                    </td>
                @endforeach
                @php
                    $delayCount = 7 - count($order->order_delay)
                @endphp
                @if($delayCount)
                    @for ($k = 0; $k < $delayCount; $k++)
                        <td></td>
                        <td></td>
                    @endfor
                @endif

                @foreach($order->delivery_problems->take(7) as $j => $problem)
                    <td>
                        {{mb_substr(($problem->reason ? htmlentities(strip_tags($problem->reason)) : $problem->problem->reason_en),0,150, "utf-8")}}
                    </td>
                    <td>
                        {{$problem->created_at ? date('Y-m-d', strtotime($problem->created_at)) : ''}}
                    </td>
                @endforeach
                @php
                    $problemCount = 7 - count($order->delivery_problems)
                @endphp
                @if($problemCount)
                    @for ($f = 0; $f < $problemCount; $f++)
                        <td></td>
                        <td></td>
                    @endfor
                @endif
            </tr>

            @php
                $i++;
            @endphp
        @endforeach
    @endif
</table>
</body>
</html>

@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.the_orders')}}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <style type="text/css">
        @media only screen and (max-width: 580px) {
            .hide-td {
                display: none;
            }
        }

        .box-body {
            min-height: 100px;
        }

        .stylish-input-group .input-group-addon {
            background: white !important;
        }

        .stylish-input-group .form-control {
            border-right: 0;
            box-shadow: 0 0 0;
            border-color: #ccc;
        }

        .stylish-input-group button {
            border: 0;
            background: transparent;
        }

        .toggle.ios, .toggle-on.ios, .toggle-off.ios {
            border-radius: 20px;
        }

        .toggle.ios .toggle-handle {
            border-radius: 20px;
        }
    </style>
@endsection

@section('header')
    <div class="page-header clearfix">
        <h3 style="display: inline-block"><i class="glyphicon glyphicon-align-justify"></i> Orders </h3>
        <button type="button" class="btn btn-success import pull-right" style="margin-right: 30px;"
                data-toggle="modal" data-target="#invoiceModal">
            {{__('backend.upload_invoice')}}
        </button>
    </div>
@endsection
@section('content')
    <div class="alert alert-danger" hidden>
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong id="errorId"></strong>
    </div>
    @if (session('success'))
        <div class="alert alert-success alert-block text-center">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ session('success') }}</strong>
        </div>
    @endif
    @if (session('warning'))
        <div class="alert alert-warning alert-block text-center">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ session('warning') }}</strong>
        </div>
    @endif
    @include('error')

    <div class="alert alert-danger" hidden>
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong id="errorId"></strong>
    </div>
    <!-- Modal For Action back-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">{{__('backend.action_back')}}</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <div class="group-control col-md-10 col-sm-10">
                                <select id="action_status" name="action_status" class="form-control action_status">
                                    <option value="" data-display="Select">{{__('backend.status')}}</option>
                                    <option value="0"
                                            @if(app('request')->input('status') === '0') selected @endif>{{__('backend.pending')}} </option>
                                    <option value="1"
                                            @if(app('request')->input('status') == 1) selected @endif>{{__('backend.accepted')}} </option>
                                    <option value="2"
                                            @if(app('request')->input('status') == 2) selected @endif>{{__('backend.received')}} </option>
                                    <option value="3"
                                            @if(app('request')->input('status') == 3) selected @endif>{{__('backend.delivered')}} </option>
                                    <option value="4"
                                            @if(app('request')->input('status') == 4) selected @endif>{{__('backend.cancelled')}} </option>
                                    <option value="5"
                                            @if(app('request')->input('status') == 5) selected @endif>{{__('backend.recalled')}} </option>
                                    <option value="8"
                                            @if(app('request')->input('status') == 8) selected @endif>{{__('backend.rejected')}} </option>
                                    <option value="7"
                                            @if(app('request')->input('status') == 7) selected @endif>{{__('backend.dropped')}} </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-6"></div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal"
                                    style="margin-left: 15px;">Close
                            </button>
                            <button type="submit" class="btn btn-primary pull-right action_back" id="action_back"
                            >{{__('backend.action_back')}}</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <form action="{{URL::asset('/mngrAdmin/orders/all')}}" method="get">
        <div class="row" style="margin-bottom:15px;" id="orders">
            <div class="col-md-12">


                <div class="col-md-3 col-sm-3">
                    <input type="text" class="form-control" id="search-field" name="search"
                           value="{{ app('request')->input('search')}}"
                           placeholder="{{__('backend.search')}} ">
                </div>
                <div class="col-md-3 col-sm-3">
                    <select id="corporate_id" name="corporate_id" class="form-control">
                        <option value="" data-display="Select">{{__('backend.corporate')}}</option>
                        @if(! empty($corporates))
                            @foreach($corporates as $corporate)
                                <option value="{{$corporate->id}}"
                                        @if(app('request')->input('corporate_id') == $corporate->id) selected @endif>
                                    {{$corporate->name}}
                                </option>

                            @endforeach
                        @endif

                    </select>
                </div>
                <div class="group-control col-md-3 col-sm-3">
                    <div class='input-group date' id='datepickerFilter'>
                        <input type="text" id="date" name="date" class="form-control"
                               value="{{ app('request')->input('date')}}"/>
                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
                <div class="group-control col-md-3 col-sm-3">
                    <select id="s_government_id" name="s_government_id" class="form-control">
                        <option value="" data-display="Select">{{__('backend.sender_Government')}}</option>
                        @if(! empty($app_governments))
                            @foreach($app_governments as $government)
                                <option value="{{$government->id}}">
                                    {{$government->name_en}}
                                </option>

                            @endforeach
                        @endif

                    </select>
                </div>

            </div>
        </div>
        <div class="row" style="margin-bottom:15px;">
            <div class="col-md-12">

                <div class="group-control col-md-3 col-sm-3">
                    <select id="s_state_id" name="s_state_id" class="form-control">
                        <option value="">{{__('backend.sender_City')}}</option>

                    </select>
                </div>

                <div class="group-control col-md-3 col-sm-3">
                    <select id="r_government_id" name="r_government_id" class="form-control">
                        <option value="" data-display="Select">{{__('backend.Receiver_Government')}}</option>
                        @if(! empty($app_governments))
                            @foreach($app_governments as $government)
                                <option value="{{$government->id}}">
                                    {{$government->name_en}}
                                </option>

                            @endforeach
                        @endif

                    </select>
                </div>

                <div class="group-control col-md-3 col-sm-3">
                    <select id="r_state_id" name="r_state_id" class="form-control">
                        <option value="">{{__('backend.Receiver_City')}}</option>

                    </select>
                </div>

                <div class="group-control col-md-3 col-sm-3">
                    <select id="status" name="status" class="form-control">
                        <option value="" data-display="Select">{{__('backend.status')}}</option>
                        <option value="0"
                                @if(app('request')->input('status') === '0') selected @endif>{{__('backend.pending')}} </option>
                        <option value="1"
                                @if(app('request')->input('status') == 1) selected @endif>{{__('backend.accepted')}} </option>
                        <option value="2"
                                @if(app('request')->input('status') == 2) selected @endif>{{__('backend.received')}} </option>
                        <option value="3"
                                @if(app('request')->input('status') == 3) selected @endif>{{__('backend.delivered')}} </option>
                        <option value="4"
                                @if(app('request')->input('status') == 4) selected @endif>{{__('backend.cancelled')}} </option>
                        <option value="5"
                                @if(app('request')->input('status') == 5) selected @endif>{{__('backend.recalled')}} </option>
                        <option value="8"
                                @if(app('request')->input('status') == 8) selected @endif>{{__('backend.rejected')}} </option>
                        <option value="7"
                                @if(app('request')->input('status') == 7) selected @endif>{{__('backend.dropped')}} </option>
                    </select>
                </div>

                <div class="group-control col-md-2 col-sm-2" style="margin-top: 20px">
                    <button type="submit"
                            class="btn btn-primary btn-block"> {{__('backend.filter_orders')}} </button>
                </div>


            </div>
        </div>
    </form>
    <div class='list'>
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                @if(! empty($orders) && count($orders) > 0)
                    <form method="post" id="myform" action="{{ url('/mngrAdmin/forward_order') }}">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <div class="table-responsive">
                            <table id="theTable" class="table table-condensed table-striped text-center"
                                   style="min-height: 200px;">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ID</th>
                                    <th>No</th>
                                    @if($type == 'pending' || $type == 'dropped' )
                                        <th>{{__('backend.agent')}}</th>
                                    @endif

                                    <th>{{__('backend.collection')}}</th>

                                    <th>{{__('backend.receiver_name')}}</th>
                                    <th>{{__('backend.sender_name')}}</th>
                                    <th>{{__('backend.status')}}</th>
                                    <th>{{__('backend.payment_status')}}</th>
                                    <th>{{__('backend.final_action')}}</th>
                                    <th>{{__('backend.from')}}</th>
                                    <th>{{__('backend.to')}}</th>
                                    @if($type == 'pending' || $type == 'dropped' )
                                        <th>{{__('backend.city')}}</th>
                                    @endif
                                    <th style="font-weight: bold; color: #333;">{{__('backend.order_number')}}</th>
                                    {{--                                    <th style="font-weight: bold; color: #333;">{{__('backend.receiver_code')}}</th>--}}
                                    @if($type == 'receive' || $type=='deliver' )
                                        <th>{{__('backend.delivery_price')}}</th>
                                    @endif
                                    @if($type == 'recall')
                                        <th>{{__('backend.recall_by')}}</th>
                                        <th>{{__('backend.recall_at')}}</th>
                                    @endif
                                    <th>{{__('backend.warehouse')}}</th>
                                    <th>{{__('backend.warehouse_manager')}}</th>
                                    @if($type == 'recall' || $type == 'cancel' )
                                        <th>{{__('backend.in_warehouse')}}</th>
                                        <th>{{__('backend.in_client')}}</th>
                                    @endif

                                    {{--                                    @if($type == 'pending')--}}
                                    {{--                                        <th>{{__('backend.headoffice')}}</th>--}}
                                    {{--                                    @endif--}}

                                    @if($type == 'cancel' )
                                        <th>{{__('backend.cancel_by')}}</th>
                                        <th>{{__('backend.cancel_at')}}</th>

                                    @endif

                                    <th style="width: 150px;">
                                        @if($type == 'dropped' || $type=='waiting' )

                                            <input type="button" class="btn btn-info btn-xs" id="toggle"
                                                   value="select" onClick="do_this()"/>
                                            <!-- <button type="submit" class="btn btn-warning btn-xs btn-pdf">PDF</button> -->
                                            <a class="btn btn-xs btn btn btn-success" id="forward" data-toggle="modal"
                                               data-target="#forwardModal">
                                                <i class="fa fa-eye"></i> {{__('backend.forward')}}

                                            </a>
                                        @endif
                                        @if($type == 'receive'  || $type == 'accept')
                                            <a class="btn btn-xs btn btn btn-info disabled" id="delay_orders"
                                               data-toggle="modal"
                                               data-target="#delayModal"
                                               style="margin-bottom: 5px">
                                                <i class="fa fa-calendar"></i> {{__('backend.delay')}}
                                            </a>
                                        @endif
                                        @if($type == 'receive')
                                            <a class="btn btn-xs btn btn btn-success disabled" id="deliver_orders">
                                                <i class="fa fa-battery"></i> {{__('backend.deliver')}}
                                            </a>
                                        @endif
                                        @if($type == 'accept' )
                                            <a class="btn btn-xs btn btn btn-success disabled" id="receive_orders">
                                                <i class="fa fa-battery-half"></i> {{__('backend.Receive')}}
                                            </a>
                                        @endif
                                        <button type="button" id="pdf-submit"
                                                class="btn btn-warning btn-pdf btn-xs ">{{__('backend.pdf')}}</button>

                                    </th>


                                    @if($type == 'pending' || $type=='waiting' )

                                        <th>
                                            <a class="btn btn-xs btn btn btn-primary" id="packup">
                                                <i class="fa fa-eye"></i> {{__('backend.packup')}}

                                            </a>
                                        </th>

                                    @endif

                                    @if(permission('actionBack'))
                                        <th>
                                            <button type="button" id="action_back_popup" data-toggle="modal"
                                                    data-target="#exampleModal"
                                                    class="btn btn-warning btn-xs action_back_popup">{{__('backend.action_back')}}</button>
                                        </th>
                                    @endif

                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $i => $order)
                                    <tr>
                                        <td>{{($i+1)}}</td>
                                        <td>{{$order->id}}</td>
                                        <td>{{$order->order_no}}</td>
                                        @if($type == 'pending' || $type == 'dropped' )
                                            <td>@if(! empty($order->agent) ) <a style="color:#f5b404;font-weight: bold"
                                                                                target="_blank"
                                                                                href="{{URL::asset('/mngrAdmin/agents/'.$order->agent_id)}}"> {{$order->agent->name}} </a> @endif
                                            </td>
                                        @endif
                                        <td>{{$order->collection_id}}</td>
                                        <td>{{$order->receiver_name}}</td>
                                        <td>
                                            {{$order->sender_name}}
                                            @if($order->customer && $order->customer->Corporate)
                                                <strong
                                                        style="color: #0c81bc"> {{$order->customer->Corporate->name}}</strong>
                                            @endif

                                        </td>
                                        <td>{!! $order->status_details_without_refund !!}</td>
                                        <td>{!! $order->payment_status_span !!}</td>
                                        <td>
                                            @if($order->is_refund)
                                                <span
                                                        class='badge badge-pill label-refund'>{{__('backend.refund')}}</span>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>
                                            @if(! empty($order->from_government)  && $order->from_government != null )
                                                {{--                                        <a style="color:#f5b404;font-weight: bold" target="_blank"--}}
                                                {{--                                           href="{{URL::asset('/mngrAdmin/governorates/'.$order->s_government_id)}}"> {{$order->from_government->name_en}} </a>--}}
                                                {{$order->from_government->name_en}}
                                            @endif
                                        </td>

                                        <td>
                                            @if(! empty($order->to_government)  && $order->from_government != null  )
                                                {{--                                        <a--}}
                                                {{--                                        style="color:#f5b404;font-weight: bold" target="_blank"--}}
                                                {{--                                        href="{{URL::asset('/mngrAdmin/governorates/'.$order->r_government_id)}}"> {{$order->to_government->name_en}} </a>--}}
                                                {{$order->to_government->name_en}}
                                            @endif
                                        </td>
                                        @if($type == 'pending' || $type == 'dropped' )
                                            <th> {{isset($order->to_city->name_en) ? $order->to_city->name_en : '-'}}</th>
                                        @endif
                                        <td style="font-weight: bold; color: #a43;">{{$order->order_number}}</td>
                                        {{--                                        <td style="font-weight: bold; color: #a4e;">{{$order->receiver_code}}</td>--}}

                                        @if($type == 'receive' || $type=='deliver' )
                                            <td>{{$order->delivery_price}}</td>
                                        @endif
                                        @if($type == 'recall' )
                                            <td>{!! $order->recall_span !!}</td>
                                            <td>{!! $order->recalled_at !!}</td>
                                        @endif
                                        <td> {{isset($order->warehouse->name) ? $order->warehouse->name : '-'}}</td>
                                        <td>
                                            @if(!empty($order->warehouse->responsibles))
                                                @foreach($order->warehouse->responsibles as $responsible)
                                                    {{$responsible->name}}
                                                @endforeach
                                            @endif
                                        </td>
                                        @if($type == 'recall' || $type == 'cancel' )
                                            <td><input data-id="{{$order->id}}" data-size="mini"
                                                       class="toggle warehouse_dropoff"
                                                       {{$order->warehouse_dropoff == 1 ? 'checked' : ''}} disabled
                                                       data-onstyle="success"
                                                       type="checkbox" data-style="ios" data-on="Yes" data-off="No">
                                            </td>
                                            <td><input data-id="{{$order->id}}" data-size="mini"
                                                       class="toggle client_dropoff"
                                                       {{$order->client_dropoff == 1 ? 'checked' : ''}} disabled
                                                       {{--$order->warehouse_dropoff == 0 || $order->client_dropoff == 1 ? 'disabled' : ''--}} data-onstyle="success"
                                                       type="checkbox" data-style="ios" data-on="Yes" data-off="No"
                                                       id="client_dropoff_{{$order->id}}"></td>
                                        @endif
                                        {{--                                        @if($type == 'pending')--}}
                                        {{--                                            <td><input data-id="{{$order->id}}" data-size="mini"--}}
                                        {{--                                                       class="toggle head_office"--}}
                                        {{--                                                       {{$order->head_office == 1 ? 'checked' : ''}} data-onstyle="success"--}}
                                        {{--                                                       type="checkbox" data-style="ios" data-on="Yes" data-off="No">--}}
                                        {{--                                            </td>--}}
                                        {{--                                        @endif--}}
                                        @if($type == 'cancel' )
                                            <td>{!! $order->cancel_span !!}</td>
                                            <td>{!! $order->cancelled_at !!}</td>

                                        @endif

                                        <td class="selectpdf" id="selectpdf">
                                            @if($order->is_pickup == "0")

                                                <input type="checkbox" name="select[]" value="{{$order->id}}"
                                                       class="selectOrder"
                                                       data-value="{{$order->id}}"/>


                                            @endif

                                        </td>

                                        @if(permission('actionBack'))
                                            <td class="selectBack" id="selectBack">
                                                <input type="checkbox" name="selectActionBack[]" value="{{$order->id}}"
                                                       class="selectActionBack"
                                                       data-value="{{$order->id}}"
                                                       @if($order->is_pickup == 1 || $order->is_refund == 1 || $order->move_collection_id)
                                                       disabled
                                                        @endif
                                                />
                                            </td>
                                        @endif

                                        <td class="selectpackup" id="selectpackup">
                                            @if ( ($type == 'pending' ) && $order->is_packedup !=1  && $order->collection_id != null )
                                                @if($order->is_pickup == "0")
                                                    <input type="checkbox" class="selectpack" name="selectpackup[]"
                                                           value="{{$order->id}}"
                                                           data-value="{{$order->collection_id}}"/>

                                                @else
                                                    <span class='badge badge-pill label-warning'>Pick Up</span>
                                                @endif
                                            @endif
                                        </td>

                                        <td>
                                            <div class="dropdown">
                                                <button class="btn btn-default dropdown-toggle" type="button"
                                                        id="dropdownMenu1"
                                                        data-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="true">
                                                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right"
                                                    aria-labelledby="dropdownMenu1">
                                                    <li>
                                                        <a href="{{ route('mngrAdmin.orders.show', $order->id ) }}"><i
                                                                    class="fa fa-eye"></i> {{__('backend.view')}}</a>
                                                    </li>
                                                    @if ( $type != 'deliver' && $type != 'recall'  && ($type == 'cancel' && ( $order->cancelled_by == 1 ||  $order->cancelled_by == 2 ) ) )

                                                        <li>
                                                            <a href="{{ route('mngrAdmin.orders.edit', $order->id) }}"><i
                                                                        class="fa fa-edit"></i> {{__('backend.edit')}}</a>
                                                        </li>
                                                    @endif
                                                    @if ( $type == 'recall' )
                                                        <li>
                                                            <a href="{{ url('/mngrAdmin/forward_recall/'.$order->id) }}"><i
                                                                        class="fa fa-share"></i> {{__('backend.edit_forward')}}
                                                            </a>
                                                        </li>
                                                    @endif
                                                    <li>
                                                        <a class=" printdata" data-id="{{$order->id}}"><i
                                                                    class="fa fa-print"></i> {{__('backend.pdf')}}</a>
                                                    </li>

                                                    @if (  $type == 'cancel' ||  $type ==  'expired_waiting' || $type == 'pending' || $type == 'dropped' )
                                                        <li>
                                                            <a data-id="{{$order->id}}" class="delete-order"><i
                                                                        class="fa fa-trash"></i> {{__('backend.delete')}}
                                                            </a>
                                                        </li>
                                                    @endif
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach

                                <div class="modal fade" id="forwardModal" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <input type="hidden" class="form-control" name="orderTypeID[]"
                                                           value="{{$order->order_type_id}}">
                                                    <input type="hidden" class="form-control" name="governorateCostID[]"
                                                           value="{{$order->governorate_cost_id}}">
                                                </div>
                                                <p>{{__('backend.Forward_Orders_In_Agent_Or_Driver')}}</p>
                                                <div class="form-group">
                                                    <br>
                                                    <select name="agent" id="agent" class="form-control agent" required>
                                                        <option value=""
                                                                selected>{{__('backend.Choose_Agent')}}</option>
                                                        @if(! empty($agents))
                                                            @foreach($agents as $agent)
                                                                <option value="{{$agent->id}}">{{$agent->name}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>

                                                    <br>
                                                    <select class="form-control" name="driverID" id="driver">
                                                        <option value="">{{__('backend.Choose_Driver')}}</option>
                                                    </select>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-primary"
                                                            onclick="document.forms['myform'].submit(); return false;">{{__('backend.forward')}}</button>
                                                    <button type="button" class="btn btn-danger"
                                                            data-dismiss="modal">{{__('backend.close')}}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </tbody>
                            </table>
                        </div>
                    </form>
                    {!! $orders->appends($_GET)->links() !!}
                @else
                    <h3 class="text-center alert alert-warning">No Result Found !</h3>
                @endif
            </div>
        </div>
        <div id="js-form" class="js-form">
        </div>

    </div>
    @include('backend.pickups.create_pickup_popup')
    @include('backend.orders.delay_order_modal')

    <!-- Modal For import Excel sheet-->
    <div class="modal fade" id="invoiceModal" tabindex="-1" role="dialog" aria-labelledby="statusModalModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">{{__('backend.import_orders')}}</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{url('mngrAdmin/reports/upload_invoice')}}" method="post" name="validateUpdateForm"
                          enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="file" name="file" id="input-file-max-fs" class="dropify"
                                       data-max-file-size="2M"
                                       accept=".xls,.xlsx,.xlsm,.xlsb,.xml,application/ms-excel,application/vnd.ms-excel"/>
                            </div>
                            <div class="col-6"></div>
                            <div class="col-md-12">
                                <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal"
                                        style="margin-left: 15px;">Close
                                </button>
                                <button type="submit"
                                        class="btn btn-primary pull-right">{{__('backend.import_orders')}}</button>
                            </div>

                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>

@endsection
@section('scripts')
    {{--    @include('backend.orders.js-search')--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script>
        $(function () {
            /*print*/
            $('.toggle').bootstrapToggle();

            $('body').on('change', '.selectOrder', function (e) {
                if ($('input[name="select[]"]').is(':checked')) {
                    $("#delay_orders").removeClass('disabled');
                    $("#deliver_orders").removeClass('disabled');
                    $("#receive_orders").removeClass('disabled');
                } else {
                    $("#delay_orders").addClass('disabled');
                    $("#deliver_orders").addClass('disabled');
                    $("#receive_orders").addClass('disabled');
                }
            });

            $('body').on('click', '#saveDelay', function (e) {
                if ($("#delay_date").val()) {
                    let ids = [];
                    $(".selectOrder:checked").each(function () {
                        ids.push($(this).val());
                    });
                    $.ajax({
                        url: "{{ url('/mngrAdmin/delay_orders')}}",
                        type: 'post',
                        data: {
                            ids: ids,
                            delay_date: $("#delay_date").val(),
                            delay_comment: $("#delay_comment").val(),
                            '_token': "{{csrf_token()}}"
                        },
                        success: function (data) {
                            // if (data['status'] != 'false') {
                            location.reload();
                            // }
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }
            });

            $('body').on('click', '#deliver_orders', function (e) {

                let ids = [];

                $(".selectpdf input:checked").each(function () {
                    ids.push($(this).val());

                });

                $.confirm({
                    title: '',
                    theme: 'modern',
                    class: 'danger',
                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-meh-o text-success"></i></span><span class="jconfirm-title">{{__("backend.Confirm")}}</span></div><div style="font-weight:bold;"><p>{{__("backend.delivery_orders_confirm")}}</p></div>',
                    buttons: {
                        confirm: {
                            text: '{{__("backend.Ok")}}',
                            btnClass: 'btn-danger',
                            action: function () {
                                let ids = [];
                                $(".selectOrder:checked").each(function () {
                                    ids.push($(this).val());
                                });
                                $.ajax({
                                    url: "{{ url('/mngrAdmin/deliver_orders')}}",
                                    type: 'post',
                                    data: {ids: ids, '_token': "{{csrf_token()}}"},
                                    success: function (data) {
                                        // if (data['status'] != 'false') {
                                        location.reload();
                                        // }
                                    },
                                    error: function (data) {
                                        console.log('Error:', data);
                                    }
                                });
                            }
                        },
                        cancel: {
                            text: '{{__("backend.Cancel")}}',
                            action: function () {
                            }
                        }
                    }
                });

                $('body').on('click', '#receive_orders', function (e) {

                    let ids = [];

                    $(".selectpdf input:checked").each(function () {
                        ids.push($(this).val());

                    });

                    $.confirm({
                        title: '',
                        theme: 'modern',
                        class: 'danger',
                        content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-meh-o text-success"></i></span><span class="jconfirm-title">{{__("backend.Confirm")}}</span></div><div style="font-weight:bold;"><p>{{__("backend.receive_orders_confirm")}}</p></div>',
                        buttons: {
                            confirm: {
                                text: '{{__("backend.Ok")}}',
                                btnClass: 'btn-danger',
                                action: function () {
                                    let ids = [];
                                    $(".selectOrder:checked").each(function () {
                                        ids.push($(this).val());
                                    });
                                    $.ajax({
                                        url: "{{ url('/mngrAdmin/receive_orders')}}",
                                        type: 'post',
                                        data: {ids: ids, '_token': "{{csrf_token()}}"},
                                        success: function (data) {
                                            // if (data['status'] != 'false') {
                                            location.reload();
                                            // }
                                        },
                                        error: function (data) {
                                            console.log('Error:', data);
                                        }
                                    });
                                }
                            },
                            cancel: {
                                text: '{{__("backend.Cancel")}}',
                                action: function () {
                                }
                            }
                        }
                    });

                });

                $('#delay_date').datepicker({
                    format: 'yyyy-mm-dd',
                    autoclose: true,
                    language: 'en',
                    todayHighlight: true,
                    startDate: new Date()
                });

                $('body').on('click', '.printdata', function (e) {

                    var url = "{{URL::asset('/mngrAdmin/print_pdf/')}}/" + $(this).attr('data-id');
                    $('<form action="' + url + '" method="get" target="_blank"></form>').appendTo('#orders').submit();
                });

                $('body').on('click', '.delete-order', function (e) {

                    var id = $(this).attr('data-id');
                    $.confirm({
                        title: '',
                        theme: 'modern',
                        class: 'danger',
                        content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Confirm Delete</span></div><div style="font-weight:bold;"><p>Are You sure To Delete This Order ?</p></div>',
                        buttons: {
                            confirm: {
                                text: 'Delete Order',
                                btnClass: 'btn-danger',
                                action: function () {
                                    $.ajax({
                                        url: "{{ url('/mngrAdmin/orders/')}}" + "/" + id,
                                        type: 'DELETE',
                                        data: {'_token': "{{csrf_token()}}"},
                                        success: function (data) {
                                            jQuery('.alert-danger').html('');
                                            jQuery('.alert-danger').show();
                                            if (data != 'false') {
                                                jQuery('.alert-danger').append('<p>' + data + '</p>');
                                                setTimeout(function () {
                                                    location.reload();
                                                }, 2000);
                                            } else {
                                                jQuery('.alert-danger').append('<p>Something Error</p>');
                                            }
                                            window.scrollTo({top: 0, behavior: 'smooth'});
                                        },
                                        error: function (data) {
                                            console.log('Error:', data);
                                        }
                                    });
                                }
                            },
                            cancel: {
                                text: 'Cancel',
                                action: function () {
                                }
                            }
                        }
                    });
                });

                $('body').on('click', '#packup', function (e) {

                    let ids = [];
                    $(".selectpack:checked").each(function () {
                        ids.push($(this).val());
                    });

                    let collection_id = $('.selectpackup  input:checked').attr('data-value');

                    let order_ids = $('.selectpackup  input:checked').val();

                    $.confirm({
                        title: '',
                        theme: 'modern',
                        class: 'warning',
                        content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-simle-o  text-warning"></i></span><span class="jconfirm-title">Confirm Pick Up</span></div><div style="font-weight:bold;"><p>Are You sure To Pick Up This Orders ?</p></div>',
                        buttons: {
                            confirm: {
                                text: 'Pickup Order',
                                btnClass: 'btn-warning',
                                action: function () {
                                    $.ajax({
                                        url: "{{ url('/mngrAdmin/pickup_orders/')}}",
                                        type: 'POST',
                                        data: {
                                            '_token': "{{csrf_token()}}",
                                            'ids': ids,
                                            'collection_id': collection_id
                                        },
                                        success: function (data) {
                                            if (data['status'] != 'false') {
                                                $(".alert").hide();

                                                $('#orderpickup').val(data['orders']);
                                                $('#from-pick').val(data['frompick']);
                                                $('#delivery_price-field').val(data['cost']);
                                                $('#bonous-field').val(data['bonous']);

                                                $('#pickupModal').modal('show');

                                            } else {
                                                $('#errorId').html(data['message']);
                                                $(".alert").show();

                                                $('#pickupModal').modal('hide');

                                            }
                                            window.scrollTo({top: 0, behavior: 'smooth'});
                                        },
                                        error: function (data) {
                                            console.log('Error:', data);
                                        }
                                    });
                                }
                            },
                            cancel: {
                                text: 'Cancel',
                                action: function () {
                                }
                            }
                        }
                    });
                })

            });

            $('body').on('click', '#pdf-submit', function (e) {

                let ids = [];

                $(".selectpdf input:checked").each(function () {
                    ids.push($(this).val());

                });
                if (ids.length > 0) {
                    $('<form action="{{ url("/mngrAdmin/print_police/")}}" method="post" ><input value="' + ids + ' " name="ids"  />" {{csrf_field()}}"</form>').appendTo('body').submit();

                } else {

                    $.confirm({
                        title: '',
                        theme: 'modern',
                        content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Print PDF</span></div><div style="font-weight:bold;"><p>{{__("backend.Must_Choose_Orders_To_Print")}}</p></div>',
                        type: 'red',
                    });
                }

            });
            $('body').on('click', '#action_back', function (e) {
                $("#exampleModal").modal("hide");

                let ids = [];
                let action_status = $('#action_status').val();

                $(".selectBack input:checked").each(function () {
                    ids.push($(this).val());

                });
                if (ids.length > 0) {

                    $.ajax({
                        type: 'post',
                        url: '{{ url("/mngrAdmin/orders/action_back")}}',
                        data: {'ids': ids, 'action_status': action_status, _token: "{{csrf_token()}}"},
                        success: function (data) {
                            location.reload();
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }

                    });
                    // $('<form action="{{ url("/mngrAdmin/orders/action_back/")}}" method="post" ><input value="" name="ids"  /><input value="'+action_status+'"  name="action_status" />" {{csrf_field()}}"</form>').appendTo('body').submit();

                } else {

                    $.confirm({
                        title: '',
                        theme: 'modern',
                        content: '<div class="jconfirm-title-c jconfirm-hand">' +
                            '<span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span>' +
                            '<span class="jconfirm-title">{{__('backend.action_back')}}</span></div><div style="font-weight:bold;">' +
                            '<p>{{__("backend.Must_Choose_Orders_To_Action_Back")}}</p></div>',
                        type: 'red',
                    });
                }

            });

            $('body').on('change', '.warehouse_dropoff', function (e) {
                if ($(this).is(':checked')) {
                    var self = $(this).attr('data-id');
                    $(this).bootstrapToggle('disable');
                    $("#client_dropoff_" + self).bootstrapToggle('enable');
                    $.ajax({
                        url: "{{ url('/mngrAdmin/orders/warehouse_dropoff/')}}",
                        type: 'POST',
                        data: {'_token': "{{csrf_token()}}", ids: [self]},
                        success: function (data) {

                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }
            });

            $('body').on('change', '.head_office', function (e) {
                var self = $(this).attr('data-id');
                var is_checked = $(this).is(':checked');
                $.ajax({
                    url: "{{ url('/mngrAdmin/orders/head_office/')}}",
                    type: 'POST',
                    data: {'_token': "{{csrf_token()}}", ids: [self], head_office: is_checked},
                    success: function (data) {

                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });

            });

            $('.client_dropoff').on('change', function (e) {
                if ($(this).is(':checked')) {
                    var self = $(this).attr('data-id');
                    $(this).bootstrapToggle('disable');
                    $.ajax({
                        url: "{{ url('/mngrAdmin/orders/client_dropoff/')}}",
                        type: 'POST',
                        data: {'_token': "{{csrf_token()}}", ids: [self]},
                        success: function (data) {

                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });

                }
            });

            $("#s_government_id").on('change', function () {
                $('#s_state_id').html('<option value="" >Sender City</option>');
                if ($(this).val()) {
                    $.ajax({
                        url: "{{ url('/get_cities')}}" + "/" + $(this).val(),
                        type: 'get',
                        data: {},
                        success: function (data) {
                            $.each(data, function (i, content) {
                                $('#s_state_id').append($("<option></option>").attr("value", content.id).text(content.city_name));
                            });
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }

            });


            $("#r_government_id").on('change', function () {
                $('#r_state_id').html('<option value="" >Receiver City</option>');
                if ($(this).val()) {
                    $.ajax({
                        url: "{{ url('/get_cities')}}" + "/" + $(this).val(),
                        type: 'get',
                        data: {},
                        success: function (data) {
                            $.each(data, function (i, content) {
                                $('#r_state_id').append($("<option></option>").attr("value", content.id).text(content.city_name));
                            });
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }

            });

            $("#r_government_id").val("{{app('request')->input('r_government_id')}}").change();
            $("#s_government_id").val("{{app('request')->input('s_government_id')}}").change();


        });

        // Select All Orders function For Print
        function do_this() {
            var checkboxes = document.getElementsByName('select[]');
            var button = document.getElementById('toggle');

            if (button.value == 'select') {
                for (var i in checkboxes) {
                    checkboxes[i].checked = 'FALSE';
                }
                button.value = 'deselect'
            } else {
                for (var i in checkboxes) {
                    checkboxes[i].checked = '';
                }
                button.value = 'select';
            }
        }

    </script>
@endsection

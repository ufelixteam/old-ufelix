<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShipmentTool extends Model
{
	protected $appends = ['status_span'];

	protected $hidden =['description','created_at','status','updated_at','status_span'];

  public function getStatusSpanAttribute() {
			/*
			* status = 0 :: Not Available
			* status = 1 :: Available
			*/
      if($this->status == 1){
          $value="<span class='badge badge-pill label-success'>".__('backend.available')."</span>";
      }else{
         $value="<span class='badge badge-pill label-danger'>".__('backend.Not_Available')."</span>";
      }
      return $value;
  }

	public function ShipmentToolOrder() {
		return $this->belongsToMany('App\Models\ShipmentToolOrder', 'Shipment_order_items', 'tool_id', 'shipment_order_id');
	}

}

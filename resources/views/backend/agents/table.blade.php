<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        @if($agents->count())
            <table class="table table-condensed table-striped text-center">
                <thead>
                <tr>
                    <th>#No.</th>
                    <th>{{__('backend.name')}}</th>
                    <th>{{__('backend.mobile')}}</th>
                    <th>{{__('backend.commercual_record_no')}}</th>
                    <th>{{__('backend.start_date')}}</th>
                    <th>{{__('backend.deal_date')}}</th>
                    <th>{{__('backend.government')}}</th>
                    <th>{{__('backend.city')}}</th>
                    <th>{{__('backend.active')}}</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($agents as $agent)
                    <tr @if(permission('showAgent')) class='clickable-row'
                        data-href="{{ route('mngrAdmin.agents.show', $agent->id) }}" @endif>
                        <td>{{$agent->id}}</td>
                        <td>{{$agent->name}}</td>
                        <td>{{$agent->mobile}}</td>
                        <td>{{$agent->commercial_record_number}}</td>
                        <td>{{!empty($agent->start_date) ? $agent->start_date : __('backend.not_found')}}</td>
                        <td>{{!empty($agent->deal_date) ? $agent->deal_date : __('backend.not_found')}}</td>
                        <td>
                            @if(\App\Models\Governorate::where('id', $agent->government_id)->exists())
                                {{\App\Models\Governorate::where('id', $agent->government_id)->value('name_en')}}
                            @else
                                {{__('backend.not_found')}}
                            @endif
                        </td>
                        <td>
                            @if(\App\Models\City::where('id', $agent->city_id)->exists())
                                {{\App\Models\City::where('id', $agent->city_id)->value('name_en')}}
                            @else
                                {{__('backend.not_found')}}
                            @endif
                        </td>
                        <td>{!! $agent->status_span !!}</td>
                        <td class="exclude-td">
                        @if(permission('showDriverAgent')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                            <a class="btn btn-xs btn-primary"
                               href="{{url('mngrAdmin/agents/showdriver',$agent->id)}}"><i
                                    class="fa fa-car"></i> {{__('backend.drivers')}}</a>
                        @endif

                        @if(permission('showOrdersMember')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                            <a class="btn btn-xs btn-default"
                               href="{{url('mngrAdmin/orders/showOrders') . '/3' . '/' . $agent->id}}"><i
                                    class="glyphicon glyphicon-list"></i> {{__('backend.orders')}}</a>
                        @endif

                        {{--                        @if(permission('showAgent')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->--}}
                        {{--                            <a class="btn btn-xs btn-primary" href="{{ route('mngrAdmin.agents.show', $agent->id) }}"><i--}}
                        {{--                                    class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>--}}
                        {{--                        @endif--}}

                        @if(permission('editAgent')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                            <a class="btn btn-xs btn-warning" href="{{ route('mngrAdmin.agents.edit', $agent->id) }}"><i
                                    class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                        @endif

                        <!--<form action="{{ route('mngrAdmin.agents.destroy', $agent->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                      <input type="hidden" name="_method" value="DELETE">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                  </form>-->
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {!! $agents->appends($_GET)->links() !!}
        @else
            <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
        @endif
    </div>
</div>

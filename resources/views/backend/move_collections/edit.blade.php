@extends('backend.layouts.app')
@section('css')
<title>{{__('backend.Order_Collection')}} - {{__('backend.edit')}}</title>
@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i>  {{__('backend.edit')}} {{__('backend.Order_Collection')}} #{{$collection_orders->id}}</h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <form action="{{ route('mngrAdmin.collection_orders.update', $collection_orders->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
 				         <div class="form-group col-md-6 col-sm-6 @if($errors->has('customer')) has-error @endif">
                    <label for="name-field"> {{__('backend.customer')}} </label>
                    <select id="customer_id-field" name="customer_id" class="form-control" value="{{ old("customer_id") }}" >
                  		 @foreach($customers as $value)
							                <option {{ ($collection_orders->customer_id==$value->id)?"selected":"" }} value="{{ $value->id }}">{{ $value->name }}</option>
              		     @endforeach
                    </select>
                 </div>
                  <!-- <div class="form-group col-md-6 col-sm-6 ">
                    <label for="count-field">Count</label>
                    <input required type="number" id="count-field" min="0" name="count" class="form-control" value="{{ (!empty($collection_orders->count_no)?$collection_orders->count_no:"") }}"/>
                  </div> -->
                  <div class="form-group col-md-6 col-sm-6 @if($errors->has('sheet')) has-error @endif">
                  <div id="sheet_upload">
                     @if($collection_orders->file_name)
                  <div class="imgDiv">
                      <input type="hidden" value="{{ $collection_orders->file_name }}" name="sheet_old">
                      <a href="{{ asset('public/api_uploades/client/corporate/sheets//'.$collection_orders->file_name) }}">{{ $collection_orders->file_name }}</a>
                      <button  class="btn btn-danger cancel">&times;</button>
                    </div>
                      @endif
                  </div>
                  <label for="sheet-field">{{__('backend.sheet')}} </label>
                  <input  class="uploadPhoto btn btn-primary" type="file" name="sheet" id="sheet">
                  @if($errors->has("sheet"))
                      <span class="help-block">{{ $errors->first("sheet") }}</span>
                  @endif
                </div>
                <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
                    <button type="submit" class="btn btn-primary">{{__('backend.save_edits')}} </button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.collection_orders.index') }}"><i class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')

  <script>
    $(function () {
        $("form[name='corprate_create']").validate({
            // Specify validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: "{!! url('/check-email-corprate') !!}",
                        type: "get"
                    }
                },
                mobile: {
                    required: true,
                    number:true,
                    rangelength:[11,11],
                    remote: {
                        url: "{!! url('/check-mobile-corprate') !!}",
                        type: "get"
                    }
                },
                phone: {
                    required: true,
                    number:true
                },

                name: {
                    required: true
                },

                latitude: {
                    required: true,
                    number:true
                },
                longitude: {
                    required: true,
                    number:true
                },
                field: {
                    required: true,
                },
                commercial_record_number: {
                    required: true
                },
            },
            // Specify validation error messages
            messages: {

                name: {
                    required: "{{__('backend.Please_Enter_Corporate_Name')}}",

                },
                email: {
                    required: "{{__('backend.Please Enter Corprate E-mail')}}",
                    email: "{{__('backend.Please_Enter_Corporate_E-mail')}}",
                    remote: jQuery.validator.format("{{__('backend.Corporate_already_exist')}}")
                },
                mobile: {
                    rangelength:"{{__('backend.Mobile_Must_Be11_Digits')}}",
                    required: "{{__('backend.Please_Enter_Corporate_Mobile')}}",
                    remote: jQuery.validator.format("{{__('backend.Mobile_already_exist')}}")
                },
                latitude: {
                    required: "{{__('backend.Please_Enter_Latitude')}}",
                },
                longitude: {
                    required: "{{__('backend.Please_Enter_Longitude')}}",
                },
                field: {
                    required: "{{__('backend.Please_Enter_field')}}",
                },

                commercial_record_number: {
                    required: "{{__('backend.Please_Enter_Commercial_Record_Number')}}",
                }
            },

            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error)
            },

            highlight: function (element) {
                $(element).parent().addClass('error')
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('error')
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function (form) {

                form.submit();

            }
        });
    })
</script>
@endsection

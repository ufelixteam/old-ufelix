@extends('frontend.profile.layouts.app')

@section('title')
    @lang('website.add_task_orderr')
@endsection
@section('css')
    <link href="{{asset('profile/bootstrap-datepicker/css/bootstrap-datepicker.css')}}" rel="stylesheet">
    <style>
        .pac-container {
            z-index: 9999 !important;
        }

        .form-group {
            min-height: 99px;
            margin-bottom: 0px;
        }

        .form-check-inline {
            display: -webkit-inline-box;
            display: -ms-inline-flexbox;
            display: inline-flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            padding-left: 0;
            margin-right: .75rem;
        }

        .form-check-inline input {
            margin: 5px;
        }

        .datepicker-dropdown {
            right: inherit !important;
        }
    </style>
@endsection

@section('content')


    <div class="row" @if(app()->getLocale() != 'en') style="text-align: right" @endif>
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="table-responsive m-t-40">
                <h1 class="title">@lang('backend.add_task_order')</h1>
                <form
                    action="{{ route('profile.tasks.store', ['locale' => (app()->getLocale() != 'en' ? 'ar' : 'en')]) }}"
                    method="POST"
                    enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="container-fluid">
                        @include('flash::message')
                    </div>
                    <div class="row" style="margin-bottom: 30px;">
                        <div
                            class="form-group col-md-4 col-sm-4 @if($errors->has('captain_received_date')) has-error @endif">
                            <label class="control-label"
                                   for="captain_received_date"> {{__('backend.captain_received_date')}}:</label>

                            <input type="text" id="datepicker" name="captain_received_date" class="form-control"
                                   value="{{  old("captain_received_date") }}"/>
                            @if($errors->has("captain_received_date"))
                                <span class="help-block">{{ $errors->first("captain_received_date") }}</span>
                            @endif
                        </div>

                        <div
                            class="form-group col-md-4 col-sm-4 @if($errors->has('sender_address')) has-error @endif">
                            <label class="control-label"
                                   for="captain_received_date"> {{__('backend.address')}}:
                                <i class="icon-map-pin" data-toggle="modal"
                                   data-target="#map1Modal"
                                   data-whatever="@fat"
                                   style="font-size: 18px;"></i>
                            </label>

                            <input type="text" name="sender_address" id="receiver_address" class="form-control"
                                   value="{{ is_null(old("sender_address")) ? @Auth::guard('Customer')->user()->address : old('sender_address') }}"/>
                            @if($errors->has("sender_address"))
                                <span class="help-block">{{ $errors->first("sender_address") }}</span>
                            @endif
                        </div>

                        <div class="form-group col-md-4 col-sm-4">
                            <label class="control-label"
                                   for="captain_received_date"> {{__('backend.task_type')}}:</label>

                            <div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="1"
                                           name="is_pickup">
                                    <label class="form-check-label"
                                           for="inlineCheckbox1">{{__('backend.pickup')}}</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="1"
                                           name="is_cash_collect">
                                    <label class="form-check-label"
                                           for="inlineCheckbox2">{{__('backend.cache_collect')}}</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="1"
                                           name="is_recall_orders">
                                    <label class="form-check-label"
                                           for="inlineCheckbox3">{{__('backend.recall_orders')}}</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-4 col-sm-4">
                            <label class="control-label"
                                   for="captain_received_date"> {{__('backend.notes')}}:</label>
                            <textarea class="form-control" rows="3" style="resize: vertical;"
                                      name="notes">{{  old("notes") }}</textarea>
                        </div>

                        <div class="form-group col-md-4 col-sm-4 " id="orders_count" style="display: none">
                            <label class="control-label"
                                   for="captain_received_date"> {{__('backend.count_orders')}}:</label>

                            <input type="text" name="orders_count" class="form-control"
                                   value="{{old('orders_count')}}"/>
                        </div>


                    </div>


                    <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
                        <button type="submit" class="btn btn-primary">{{__('backend.save_edits')}}</button>
                        {{--                    <a class="btn btn-link pull-right"--}}
                        {{--                       href="{{ URL::previous() /*route('mngrAdmin.customers.index')*/ }}"><i--}}
                        {{--                            class="glyphicon glyphicon-backward"></i> {{__('backend.back')}} </a>--}}
                    </div>
                </form>
            </div>
        </div>
    </div>

    @include('frontend.profile.collections.map')
    @include('frontend.profile.collections.map1')

@endsection

@section('scripts')

    <script src="{{asset('profile/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('/assets/scripts/map-task.js')}}"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key=AIzaSyDQiN4qc7pC9b1BRQwkqXXD28peMfWcHvw&callback=myMap"></script>
    <script>
        $('#datepicker').datepicker({
            @if(date('H') <= 12)
            startDate: "today",
            @else
                startDate: '+1d',
            @endif
            format: 'yyyy-mm-dd',
        });
        $('#inlineCheckbox1').change(function () {
            if ($('input#inlineCheckbox1').is(':checked')) {
                $("#orders_count").show();
            } else {
                $("#orders_count").hide();
            }
        });
    </script>
@endsection

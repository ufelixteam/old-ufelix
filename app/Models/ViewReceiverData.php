<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ViewReceiverData extends Model
{
    public $table = "view_receiver_data";
}

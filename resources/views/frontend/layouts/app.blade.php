<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta name="description" content="">
        <meta name="keywords" content="{{__('website.keywords')}}">
        <meta charset="utf-8">
        <meta name="author" content="Ufelix Team">
        <meta name="description" content="Ufelix - ufelix">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>@yield('title')</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('website/images/flogo.png')}}" />
        <link href="https://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Tajawal" rel="stylesheet">

        <link href="{{asset('website/css/style.css')}}" rel="stylesheet" type="text/css" />

        @if( app()->getLocale() == 'en' )
            <link href="{{ asset('website/css/style-en.css')}}" rel="stylesheet" type="text/css" />
        @else
        <link href="{{asset('website/css/bootstrap-rtl.min.css')}}" rel="stylesheet" type="text/css" /><!-- DELETE "TO RUN THE APP ONLINE" -->
        @endif
        <link href="{{asset('website/css/mobile.css')}}" rel="stylesheet" type="text/css" />

        <link rel="stylesheet" href="{{asset('/assets/vendor/font-awesome/css/font-awesome.min.css')}}">

        @yield('css')
    </head>
    <body>
    	<!-- Start Top-h -->
    	<header class="toph">
        	<div class="container">
            	<div class="col-md-6 col-xs-12">
                	<ul>

                        @if(Auth::guard('Customer')->user())
                            <li>
                                <a class="btn login-btn" href="{{ app()->getLocale() == 'en' ? url('/en/customer/account') : url('/customer/account') }}">
                                    <i class="fa fa-user"></i> @lang('website.Profile')
                                </a>
                            </li>

                        @else
                    	<li>
                            <a class="btn login-btn" href="{{ app()->getLocale() == 'en' ? url('/en/login') : url('/login') }}">
                                <i class="fa fa-user"></i> @lang('website.Login')
                            </a>
                        </li>
                    	<li>
                            <a href="{{ app()->getLocale() == 'en' ? url('/en/register') : url('/register') }}" class="btn login-btn">
                                <i class="fa fa-user-plus"></i> @lang('website.Register')
                            </a>
                        </li>
                        @endif
                    </ul>
                </div>
                <div class="col-md-6 col-xs-12">
                	<p> @lang('website.OpeningHours') : @if(! empty($app_about)  && ! empty($app_about['working_hour'] ) )  {{ app()->getLocale() == 'en' ? $app_about['working_hour'] : $app_about['working_hour_ar'] }} @endif</p>
                </div>
            </div>
        </header>
    	<!-- End Top-h -->
    	<!-- Start logo-h -->
        <section class="logo-h">
        	<div class="container">
            	<div class="col-md-3 col-xs-6">
                    <a href="{{ app()->getLocale() == 'en' ? url('/en/') : url('/') }}" class="logo navbar-brand">
                    	<img src="{{asset('website/images/logo.png')}}" lat="" class="logo-img" />
                    </a>
                </div>
                <div class="col-md-9 col-xs-6 padding">
                	<div class="contact-h">
                    	<div class="col-md-4">
                        	<div class="details-con">
                            	<div class="icon-i">
                                	<i class="far fa-envelope"></i>
                                    <h1>@lang('website.Email') :</h1>
                                    <a href="mailto:email@yourdomain.com"> @if(! empty($app_about)  && ! empty($app_about['support_email'] ))  {{  $app_about['support_email']  }} @endif</a>
                                </div>
                            </div>
                        </div>
                    	<div class="col-md-4">
                        	<div class="details-con">
                            	<div class="icon-i">
                                	<i class="fas fa-mobile-alt"></i>
                                    <h1>@lang('website.Mobile') :</h1>
                                    <span> @if(! empty($app_about) && ! empty($app_about['telephone'] ) )  {{  $app_about['telephone']  }} @endif</span>
                                </div>
                            </div>
                        </div>
                    	<div class="col-md-4">
                        	<div class="details-con">
                            	<div class="icon-i">
                                	<i class="fas fa-map-marked-alt"></i>
                                    <h1>@lang('website.FindUs') :</h1>
                                    <span> @if(! empty($app_about) && ! empty($app_about['address'] ) )  {{ app()->getLocale() == 'en' ? $app_about['address'] : $app_about['address_ar'] }} @endif</span>
                                </div>
                            </div>
                        </div>
                    </div>
             		<button type="button" class="open-sidebar"><i class="fa fa-bars"></i></button>
                </div>
            </div>
        </section>
    	<!-- End logo-h -->
        <!-- Start Nav -->
        <nav class="navbar navbar-inverse">
          	<div class="container">
            	<div class="collapse navbar-collapse" id="myNavbar">
              		<ul class="nav navbar-nav">
                		<li class="current-menu-item"><a href="{{ app()->getLocale() == 'en' ? url('/en/') : url('/') }}">@lang('website.Home')</a></li>
                		<li><a class="nav-link scroll" href="{{ app()->getLocale() == 'en' ? url('/en/services') : url('/services') }}">@lang('website.why_us')</a></li>
                		<li><a class="nav-link scroll" href="{{ app()->getLocale() == 'en' ? url('/en/captain') : url('/captain') }}">@lang('website.captain')</a></li>
                		<li><a class="nav-link scroll" href="{{ app()->getLocale() == 'en' ? url('/en/customer') : url('/customer') }}">@lang('website.client')</a></li>
                        <li><a class="btn-id" data-toggle="modal" data-target="#box-id">@lang('website.track_order')</a></li>

                		<li><a class="nav-link scroll" href="{{ app()->getLocale() == 'en' ? url('/en/contact') : url('/contact') }}">@lang('website.Contact')</a></li>

              		</ul>
                    <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(),  app()->getLocale() == 'en' ?'':'en' ) }}" class="lang">{{ app()->getLocale() == 'en' ? __('website.Arabic') : __('website.English') }}</a>
            	</div>
          	</div>

        </nav>
        <div>
        	<!-- Start Sidebar -->
            <div class="overlay_gen"></div>
            <div class="sidebar">
                <div class="side-logo">
                    <a href="#">
                        <img src="{{asset('website/images/logo.png')}}" alt="">
                    </a>
                </div>
                <div class="side-social">
                    <ul>
                        <li>
                            <a href="#" class="lin">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="tw">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="ins">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="side-nav">
                    <ul>
                		<li class="current-menu-item"><a href="{{ app()->getLocale() == 'en' ? url('/en/') : url('/') }}">@lang('website.Home')</a></li>
                		<li><a class="nav-link scroll" href="{{ app()->getLocale() == 'en' ? url('/en/services') : url('/services') }}">@lang('website.services')</a></li>
                		<li><a class="nav-link scroll" href="{{ app()->getLocale() == 'en' ? url('/en/captain') : url('/captain') }}">@lang('website.captain')</a></li>
                		<li><a class="nav-link scroll" href="{{ app()->getLocale() == 'en' ? url('/en/customer') : url('/customer') }}">@lang('website.client')</a></li>
                		<li><a class="nav-link scroll" href="{{ app()->getLocale() == 'en' ? url('/en/contact') : url('/contact') }}">@lang('website.Contact')</a></li>
                    </ul>
                </div>
            </div>
            <!-- End Sidebar -->
        </div>

        <main class="main-content">
        @yield('content')
    </main>




    <footer>
            <div class="footer-top">
                <div class="container">
                    <!-- Start Col -->
                    <div class="col-md-2 col-xs-12">
                        <div class="logo logo-footer mb-sm-50">
                            <a href="#">
                                <img src="{{asset('website/images/flogo.png')}}" alt="" />
                                <img src="{{asset('website/images/text-logo.png')}}" alt="" />
                            </a>
                        </div>
                    </div>
                    <!-- End -->

                    <!-- Start Col -->
                    <div class="col-md-3 col-xs-12 mb-sm-50">
                        <div class="details-con-f">
                            <ul>
                                <li><i class="fas fa-mobile-alt"></i><span> @if(! empty($app_about)  && ! empty($app_about['telephone'] ))  {{  $app_about['telephone']  }} @endif</span></li>
                                <li><i class="fa fa-envelope"></i><span> @if(! empty($app_about)  && ! empty($app_about['support_email'] ))  {{  $app_about['support_email']  }} @endif</span></li>
                                <li><i class="fas fa-map-marked-alt"></i><span> @if(! empty($app_about) && ! empty($app_about['address'] ) )  {{ app()->getLocale() == 'en' ? $app_about['address'] : $app_about['address_ar'] }} @endif</span></li>
                            </ul>
                        </div>
                    </div>
                    <!-- End -->

                    <!-- Start Cal -->
                    <div class="col-md-4 col-xs-12 mb-sm-50">

                        <ul class="links">
                            <li><a href="{{ app()->getLocale() == 'en' ? url('/en/services') : url('/services') }}">@lang('website.why_us')</a></li>
                            <li><a href="{{ app()->getLocale() == 'en' ? url('/en/captain') : url('/captain') }}">@lang('website.captain')</a></li>
                            <li><a href="{{ app()->getLocale() == 'en' ? url('/en/customer') : url('/customer') }}">@lang('website.client')</a></li>
                            <li><a href="{{ app()->getLocale() == 'en' ? url('/en/contact') : url('/contact') }}">@lang('website.Contact')</a></li>
                        <li><a class="btn-id" data-toggle="modal" data-target="#box-id"> @lang('website.track_order')</a></li>

                            <li><a href="{{ app()->getLocale() == 'en' ? url('/en/terms') : url('/terms') }}">@lang('website.Terms')</a></li>
                        </ul>
                    </div>
                    <!-- End -->

                    <div class="col-md-3 col-xs-8">
                        <div class="other-logos">
                            <a href="#"><img src="{{asset('/images/google.png')}}" class="mb-sm-50" /></a>
                            <a href="#"><img src="{{asset('/images/apple.png')}}"  class="mb-sm-50 mr-sm-70"/></a>
                        </div>
                        <div class="social-media-footer">
                            <ul>
                                <li><a target="_blank" href="@if(! empty($app_about) && ! empty($app_about['facebook'] ) )  {{  $app_about['facebook']  }}  @else # @endif"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a target="_blank" href="@if(! empty($app_about) && ! empty($app_about['twitter'] ) )  {{  $app_about['twitter']  }}  @else # @endif"><i class="fab fa-twitter"></i></a></li>
                                <li><a target="_blank" href="@if(! empty($app_about) && ! empty($app_about['google'] ) )  {{  $app_about['google']  }}  @else # @endif"><i class="fab fa-google-plus-g"></i></a></li>
                                <li><a target="_blank" href="@if(! empty($app_about) && ! empty($app_about['linkedin'] ) )  {{  $app_about['linkedin']  }}  @else # @endif"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <p> @lang('website.copywright')<span >@lang('website.ufelix')</span></p>
                </div>
            </div>
        </footer>



        @include('frontend.layouts.track')

        <script src="{{asset('website/js/jquery-1.11.0.min.js')}}"></script>
        <script src="{{asset('website/js/bootstrap.js')}}"></script>
        <script src="{{asset('website/js/owl.carousel.js')}}"></script>
        <script src="{{asset('website/js/wow.min.js')}}"></script>
        <script src="{{asset('website/js/responsiveCarousel.min.js')}}"></script>
        <script src="{{asset('website/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
        <script src="{{asset('website/js/aos.js')}}"></script>
        <script src="{{asset('website/js/jquery-scrolloffset.min.js')}}"></script>
        <script src="{{asset('website/js/jquery.counterup.min.js')}}"></script>
        <script src="{{asset('website/js/waypoints.min.js')}}"></script>
        <script src="{{asset('website/js/java.js')}}"></script>
        <script>
            AOS.init();
        </script>
        @yield('scripts')

        <!-- Messenger Chat Plugin Code -->
        <div id="fb-root"></div>
        <script>
            window.fbAsyncInit = function () {
                FB.init({
                    xfbml: true,
                    version: 'v10.0'
                });
            };

            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s);
                js.id = id;
                js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>

        <!-- Your Chat Plugin code -->
        <div class="fb-customerchat"
             attribution="page_inbox"
             page_id="485525115156861">
        </div>
    </body>
</html>

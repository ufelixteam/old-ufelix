<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{

    protected $guarded = ['id'];

    public function Agents()
    {
        return $this->belongsTo('App\Models\Agent', 'object_id');
    }

    public function Drivers()
    {
        return $this->belongsTo('App\Models\Driver', 'object_id');
    }

    public function Corporates()
    {
        return $this->belongsTo('App\Models\Corporate', 'object_id');
    }

    public function logs()
    {
        return $this->hasMany('App\Models\WalletLog', 'wallet_id');
    }

    public function getTypeSpanAttribute()
    {
        if ($this->type == 1) {
            $value = "<span class='badge badge-pill label-info'>" . __('backend.driver') . "</span>";
        } else if ($this->type == 2) {
            $value = "<span class='badge badge-pill label-warning'>" . __('backend.corporate') . "</span>";
        } else if ($this->type == 3) {
            $value = "<span class='badge badge-pill label-success'>" . __('backend.agent') . "</span>";
        } else {
            $value = "<span class='badge badge-pill label-info'></span>";
        }
        return $value;
    }

    public function getActiveSpanAttribute($value)
    {
        if ($this->status == 1) {
            $value = "<span class='badge badge-pill label-success'>" . __('backend.opened') . "</span>";
        } else {
            $value = "<span class='badge badge-pill label-danger'>" . __('backend.closed') . "</span>";
        }
        return $value;
    }


    public function getAmountAttribute()
    {
        return $this->logs()->sum('value');
    }
}


<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModelType extends Model
{
    /*
    * status = 0 :: Not Verify
    * status = 1 :: Verify
    */
    public function getStatusSpanAttribute($value)
    {
        if($this->status == 0){
            $value="<span class='badge badge-pill label-danger'>".__('backend.not_verify')."</span>";
        }else if($this->status == 1){
            $value="<span class='badge badge-pill label-success'>".__('backend.verfiy')."</span>";
        }else{
          $value = "";
        }
        return $value;
    }
}

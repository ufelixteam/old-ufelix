@extends('backend.layouts.app')

@section('css')
    <title>{{__('backend.the_wallets')}} - {{__('backend.view')}}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
@endsection

@section('header')
    <div class="page-header">
        <h3>
            @if($wallet->type == 1)
                {{ ! empty($wallet->drivers) ? $wallet->drivers->name : '' }}
            @elseif($wallet->type == 2)
                {{ ! empty($wallet->corporates) ? $wallet->corporates->name : '' }}
{{--            @elseif($wallet->type == 3)--}}
{{--                {{ ! empty($wallet->agents) ? $wallet->agents->name : '' }}--}}

            @elseif($wallet->type == 0)
                Ufelix Account
            @endif

            {!! $wallet->type_span !!}
            {!! $wallet->active_span !!}
        </h3>


        <form action="{{ route('mngrAdmin.wallets.destroy', $wallet->id) }}" method="POST" style="display: inline;"
              onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">

            @if(permission('incrementWallet')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <button type="button" class="btn btn-success " data-toggle="modal" data-target="#addModal"
                        data-value="{{$wallet->value}}"> + {{__('backend.deposit')}}
                </button>
            @endif

            @if(permission('decrementWallet')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#subModal"
                        data-value="{{$wallet->value}}"> - {{__('backend.withdraw')}}
                </button>
            @endif

            @if(permission('closeWallet')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                <button id="closeWallet" type="button" data-qty="{{$wallet->value}}" data-value="{{ $wallet->status }}"
                        class="btn btn-warning">@if($wallet->status == 1) {{__('backend.close')}} @else {{__('backend.open')}} @endif </button>
            @endif

            <!-- <a class="btn btn-warning btn-group" href="{{ route('mngrAdmin.wallets.edit', $wallet->id)."?type=".$wallet->type }}"><i class="glyphicon glyphicon-edit"></i> Edit</a> -->
            @if(permission('deleteWallet')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                @if(! ( $wallet->logs && count($wallet->logs) > 0 ))
                    <button type="submit" class="btn btn-danger"><i
                            class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}</button>
                @endif
                @endif
            </div>
        </form>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12" style="margin-top: 35px;">

            @if($wallet->type != 0)
                <div class="form-group col-sm-4">
                    <label for="object_id"> {{__('backend.Wallet_Owner_ID')}}: </label>
                    <p class="form-control-static">#{{$wallet->object_id}}

                    </p>
                </div>


                <div class="form-group col-sm-4">
                    <label for="type">{{__('backend.Wallet_Owner')}}:</label>
                    <p> @if($wallet->type == 1)
                            {{$wallet->drivers->name}}
                        @elseif($wallet->type == 2)
                            {{$wallet->corporates->name}}
                            {{--                        @elseif($wallet->type == 3)--}}
                            {{--                            {{$wallet->agents->name}}--}}
                        @endif </p>
                </div>

            @endif
            <div class="form-group col-sm-4">
                <label for="value">{{__('backend.Wallet_Amount')}}: </label>
                <p class="form-control-static">{{$wallet->amount}}</p>
            </div>

            <div class="row">
                <div class="col-md-12 col-xs-12 col-sm-12" style="margin-top: 35px;">

                    <h4 class="text-center text-warning"><strong> - - {{__('backend.Wallet_Transactions')}} -
                            - </strong></h4>


                    <div class="row">
                        <div class="col-md-12 col-xs-12 col-sm-12">
                            @if( ! empty($wallet->logs) && $wallet->logs->count())
                                <table id="theTable" class="table table-condensed table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{__('backend.value')}}</th>
                                        <th>{{__('backend.invoice')}}</th>
                                        <th>{{__('backend.reason')}}</th>
                                        {{--                                        <th>{{__('backend.type')}}</th>--}}
                                        {{--                                        <th class="text-left"></th>--}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($wallet->logs as $log)
                                        <tr>
                                            <td>{{ $log->id }}</td>

                                            <td>{{ $log->value }}</td>

                                            <td>@if($log->invoice_id > 0) <a
                                                    href="{{ URL::asset('/mngrAdmin/invoices/'.$log->invoice_id)}}"><strong> {{$log->invoice_id}} </strong></a> @endif
                                            </td>

                                            <td>{{ $log->reason }}</td>

                                            {{--                                            <td>{!! $log->type_span !!}</td>--}}

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <h3 class="text-center alert alert-warning">{{__('backend.No_Wallet_Log')}}</h3>
                            @endif
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-12 col-xs-12 col-sm-12">
                <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.wallets.index') }}"><i
                        class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="subModal" tabindex="-1" role="dialog" aria-labelledby="subModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="subModalLabel">{{__('backend.Withdrawal_Amount_From_Wallet')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{URL::asset('mngrAdmin/sub_wallet')}}" method="post">
                    <div class="modal-body">

                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="type" value="{{ $wallet->type }}"/>
                        <input type="hidden" name="wallet_id" value="{{$wallet->id}}">

                        <div class="form-group @if($errors->has('value')) has-error @endif">
                            <label for="subvalue"> {{__('backend.amount')}}: </label>
                            <input type="number" min="1" required id="subvalue" name="value"
                                   class="form-control" value="{{ old("value") }}"/>
                            @if($errors->has("value"))
                                <span class="help-block">{{ $errors->first("value") }}</span>
                            @endif
                        </div>

                        <div class="form-group @if($errors->has('reason')) has-error @endif">
                            <label for="subreason"> {{__('backend.reason')}}: </label>
                            <input type="text" id="subreason" name="reason" class="form-control" required
                                   value="{{ old('reason') }}"/>
                            @if($errors->has("reason"))
                                <span class="help-block">{{ $errors->first("reason") }}</span>
                            @endif
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary"
                                data-dismiss="modal">{{__('backend.close')}}</button>
                        <button type="submit" class="btn btn-primary">{{__('backend.Save_changes')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('backend.Add_Amount_To_Wallet')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{URL::asset('mngrAdmin/add_wallet')}}">
                    <div class="modal-body">

                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="type" value="{{$wallet->type }}"/>
                        <input type="hidden" name="wallet_id" value="{{$wallet->id}}">
                        <div class="form-group @if($errors->has('value')) has-error @endif">
                            <label for="subvalue"> {{__('backend.amount')}}: </label>
                            <input type="number" min="1" required id="subvalue" name="value"
                                   class="form-control" value="{{ old("value") }}"/>
                            @if($errors->has("value"))
                                <span class="help-block">{{ $errors->first("value") }}</span>
                            @endif
                        </div>


                        <div class="form-group @if($errors->has('reason')) has-error @endif">
                            <label for="subreason"> {{__('backend.reason')}}: </label>
                            <input type="text" id="subreason" name="reason" class="form-control" required
                                   value="{{ old('reason') }}"/>
                            @if($errors->has("reason"))
                                <span class="help-block">{{ $errors->first("reason") }}</span>
                            @endif
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary"
                                data-dismiss="modal">{{__('backend.close')}}</button>
                        <button type="submit" class="btn btn-primary">{{__('backend.Save_changes')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script>
        $(function () {
            $(document).on('input paste', 'input[type=number]', function (evt) {
                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                    evt.preventDefault();
                }
                //
                // if ($(this).val() < $(this).attr('min')) {
                //     $(this).val($(this).attr('min'));
                // }
                //
                // if ($(this).val() > $(this).attr('max')) {
                //     $(this).val($(this).attr('max'));
                // }
            });

            $('#closeWallet').on('click', function () {
                if ($(this).attr('data-value') == "1" && $(this).attr('data-qty') > 0) {
                    $.alert({
                        title: '',
                        theme: 'modern',
                        class: 'danger',
                        content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Confirm Close</span></div><div style="font-weight:bold;"><p> We can not continue Closed Wallet Operation because Wallet have Money.</p><p class="text-primary">Please withdraw before Close It.</p><p class="text-danger"> Something error! </p></div>',
                    });
                } else {

                    $.ajax({
                        url: "{{ url('/mngrAdmin/close_wallet') }}",
                        type: "POST",
                        data: {'wallet_id': "{{$wallet->id}}", '_token': "{{csrf_token()}}"},
                        success: function (response) {
                            if (response == 'error') {
                                $.alert({
                                    title: '',
                                    theme: 'modern',
                                    class: 'danger',
                                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Confirm Close</span></div><div style="font-weight:bold;"><p class="text-danger"> Something error! </p></div>',
                                });
                            } else if (response == 1) {
                                $('#closeWallet').html('Close');
                            } else {
                                $('#closeWallet').html('Open');
                            }
                            location.reload();
                        }
                    });

                }
            });
        });
    </script>
@endsection

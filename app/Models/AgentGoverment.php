<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class AgentGoverment extends Model
{
    //

    public function governorate() {
        return $this->belongsTo('App\Models\Governorate','government_id');
    }

    public function agent() {
        return $this->belongsTo('App\Models\Agent','agent_id');
    }
}

<div class="col-md-12 col-sm-12 col-xs-12">
    <form action="{{ URL::asset('/mngrAdmin/tmporders_create') }}" method="POST" id="order-form" class="order-form">
        <input type="hidden" id="collection_id" name="collection_id"
               value="{{ ! empty($collection_id) ? $collection_id : '0' }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <input type="hidden" id="order_type_id" name="order_type_id"
               value="{{ ! empty($corporate) && $corporate != null ? $corporate->order_type : 2 }}">

        <div class="col-md-12 col-sm-12 col-xs-12" style="border: 2px solid #ea2;margin-bottom: 15px;">
            <h4 style="color: #ea2;font-weight: bold">{{__('backend.Sender_Data')}}</h4>

            <div class="form-group col-md-3 col-sm-3 @if($errors->has('corparate_id')) has-error @endif">
                <label for="corparate_id-field">{{__('backend.corporate')}}</label>

                <select class="form-control" id="corparate_id"
                        name="corparate_id" {{ ! empty($collection) && $collection != null ? 'disabled':  ''}}>
                    <option value="" data-display="Select">{{__('backend.corporate')}}</option>
                    @if(! empty($Corporate_ids))
                        @foreach($Corporate_ids as $corparate_id)
                            <option
                                value="{{$corparate_id->id}}" {{ ! empty($collection) && $collection != null && $collection->corporate_id == $corparate_id->id  ? 'selected':  ''}} >
                                {{ $corparate_id->id.' - '.$corparate_id->name}}
                            </option>

                        @endforeach
                    @endif

                </select>

                @if($errors->has("corparate_id"))
                    <span class="help-block">{{ $errors->first("corparate_id") }}</span>
                @endif
            </div>

            <div class="form-group col-md-2 col-sm-2 @if($errors->has('customer_id')) has-error @endif">
                <label for="customer_id-field">{{__('backend.customer')}} </label>
                <select class=" form-control" id="customer_id"
                        name="customer_id" {{ ! empty($collection) && $collection != null && $customer != null ? 'disabled':  ''}} >

                    @if(! empty($customers) && count($customers) > 0)
                        @foreach($customers as $customer_id)
                            <option
                                value="{{$customer_id->id}}" {{ ! empty($collection) && $collection != null && $collection->customer_id == $customer_id->id  ? 'selected':  ''}} >
                                {{$customer_id->name}}
                            </option>

                        @endforeach

                    @else
                        <option value="">{{__('backend.customer')}}</option>
                    @endif

                </select>
                @if($errors->has("customer_id"))
                    <span class="help-block">{{ $errors->first("customer_id") }}</span>
                @endif
            </div>

            <div class="form-group col-md-3 col-sm-3 @if($errors->has('sender_name')) has-error @endif hidden">
                <label for="sender_name-field">{{__('backend.name')}}: </label>
                <input type="text" required id="sender_name-field" name="sender_name" class="form-control"
                       value="{{ ! empty($customer) && $customer != null ? $customer->name : old("sender_name") }}"/>
                @if($errors->has("sender_name"))
                    <span class="help-block">{{ $errors->first("sender_name") }}</span>
                @endif
            </div>

            <div class="form-group col-md-2 col-sm-2 @if($errors->has('sender_mobile')) has-error @endif">
                <label for="sender_mobile-field">{{__('backend.mobile_number')}}: </label>
                <input type="text" required id="sender_mobile-field" name="sender_mobile" class="form-control"
                       value="{{  ! empty($customer) && $customer != null ? $customer->mobile : old("sender_mobile") }}"/>
                @if($errors->has("sender_mobile"))
                    <span class="help-block">{{ $errors->first("sender_mobile") }}</span>
                @endif
            </div>
            <div class="form-group col-md-2 col-sm-2 @if($errors->has('sender_phone')) has-error @endif">
                <label for="sender_phone-field">{{__('backend.mobile_number2')}}: </label>
                <input type="text" required id="sender_phone-field" name="sender_phone" class="form-control"
                       value="{{ old("sender_phone") }}"/>
                @if($errors->has("sender_phone"))
                    <span class="help-block">{{ $errors->first("sender_phone") }}</span>
                @endif
            </div>

            <div class="form-group col-md-3 col-sm-3">
                <label class="control-label" for="s_government_id">{{__('backend.government')}}</label>

                <select class="form-control s_government_id" id="s_government_id" name="s_government_id"
                        data-id="{{@$customer->government_id}}" data-first="true">
                    <option value="" data-display="Select">{{__('backend.government')}}</option>
                    @if(! empty($governments))
                        @foreach($governments as $government)
                            <option value="{{$government->id}}">
                                {{$government->name_en}}
                            </option>

                        @endforeach
                    @endif

                </select>

            </div>

            <div class="form-group col-md-2 col-sm-2">
                <label class="control-label" for="s_state_id">{{__('backend.city')}}</label>

                <select class=" form-control s_state_id" id="s_state_id" name="s_state_id"
                        data-id="{{@$customer->city_id}}">
                    <option value="">{{__('backend.city')}}</option>

                </select>

            </div>


            <div
                class="form-group input-group  col-md-2 col-sm-2 @if($errors->has('sender_latitude')) has-error @endif">
                <label for="sender_latitude">{{__('backend.latitude')}}: </label>
                <input type="text" required id="sender_latitude" name="sender_latitude" class="form-control"
                       value="{{ ! empty($customer) && $customer != null ? $customer->latitude : (old("sender_latitude") ? old("sender_latitude") : '30.0668886') }}"/>
                <span class="input-group-append">
                      <button class="input-group-text bg-transparent" type="button" data-toggle="modal"
                              data-target="#map2Modal"> <i class="fa fa-map-marker"></i></button>
                                    </span>

                @if($errors->has("sender_latitude"))
                    <span class="help-block">{{ $errors->first("sender_latitude") }}</span>
                @endif
            </div>
            <div class="form-group col-md-2 col-sm-2 @if($errors->has('sender_longitude')) has-error @endif">
                <label for="sender_longitude">{{__('backend.longitude')}}: </label>
                <input type="text" required id="sender_longitude" name="sender_longitude" class="form-control"
                       value="{{ ! empty($customer) && $customer != null ? $customer->longitude : (old("sender_longitude") ? old("sender_longitude") : '31.1962743') }}"/>
                @if($errors->has("sender_longitude"))
                    <span class="help-block">{{ $errors->first("sender_longitude") }}</span>
                @endif
            </div>

            <div class="form-group col-md-6 col-sm-6 @if($errors->has('sender_address')) has-error @endif">
                <label for="sender_address">{{__('backend.Sender_Address')}}: </label>
                <input type="text" required id="sender_address" name="sender_address" class="form-control"
                       value="{{ ! empty($customer) && $customer != null ? $customer->address : (old("sender_address") ? old("sender_address") : '') }}"/>

                @if($errors->has("sender_address"))
                    <span class="help-block">{{ $errors->first("sender_address") }}</span>
                @endif
            </div>

            <div class="form-group col-md-3 col-sm-3 @if($errors->has('reference_number')) has-error @endif" style="clear: both">
                <label for="reference_number-field"> {{__('backend.reference_number')}}: </label>
                <input type="text" id="reference_number-field" name="reference_number" class="form-control"
                       value="{{old("reference_number")}}"/>
                @if($errors->has("reference_number"))
                    <span class="help-block">{{ $errors->first("reference_number") }}</span>
                @endif
            </div>
            <div class="form-group col-md-3 col-sm-3 @if($errors->has('delivery_type')) has-error @endif">
                <label for="delivery_type-field"> {{__('backend.delivery_type')}}: </label>
                <input type="text" id="delivery_type-field" name="delivery_type" class="form-control"
                       value="{{ old("delivery_type") }}"/>
                @if($errors->has("delivery_type"))
                    <span class="help-block">{{ $errors->first("delivery_type") }}</span>
                @endif
            </div>
            <div class="form-group col-md-3 col-sm-3 @if($errors->has('sender_subphone')) has-error @endif">
                <label for="sender_subphone-field"> {{__('backend.sender_subphone')}}: </label>
                <input type="text" id="sender_subphone-field" name="sender_subphone" class="form-control"
                       value="{{old("sender_subphone") }}"/>
                @if($errors->has("sender_subphone"))
                    <span class="help-block">{{ $errors->first("sender_subphone") }}</span>
                @endif
            </div>

        </div>

        <div class="col-md-12 col-sm-12 col-xs-12" id="receiver_inputs"
             style="border: 2px solid #ea2;margin-bottom: 15px;">
            <h4 style="color: #ea2;font-weight: bold">{{__('backend.Receiver_Data')}}</h4>

            <div class="form-group col-md-3 col-sm-3 @if($errors->has('receiver_name')) has-error @endif">
                <label for="receiver_name-field"> {{__('backend.name')}}: </label>
                <input type="text" required id="reciever_name-field" name="receiver_name" class="form-control"
                       value="{{ old('reciever_name') }}"/>
                @if($errors->has("receiver_name"))
                    <span class="help-block">{{ $errors->first("receiver_name") }}</span>
                @endif
            </div>
            <div class="form-group col-md-3 col-sm-3 @if($errors->has('receiver_mobile')) has-error @endif">
                <label for="reciever_mobile-field"> {{__('backend.mobile_number')}}: </label>
                <input type="text" required id="receiver_mobile-field" name="receiver_mobile" class="form-control"
                       value="{{ old('receiver_mobile') }}"/>
                @if($errors->has("receiver_mobile"))
                    <span class="help-block">{{ $errors->first("receiver_mobile") }}</span>
                @endif
            </div>
            <div class="form-group col-md-3 col-sm-3 @if($errors->has('receiver_phone')) has-error @endif">
                <label for="receiver_phone-field"> {{__('backend.mobile_number2')}}:</label>
                <input type="text" required id="receiver_phone-field" name="receiver_phone" class="form-control"
                       value="{{ old("receiver_phone") }}"/>
                @if($errors->has("receiver_phone"))
                    <span class="help-block">{{ $errors->first("receiver_phone") }}</span>
                @endif
            </div>

            <div class="form-group col-md-3 col-sm-3">
                <label class="control-label" for="r_government_id"> {{__('backend.government')}}:</label>

                <select class="form-control r_government_id" id="r_government_id" name="r_government_id">
                    <option value="" data-display="Select">{{__('backend.Receiver_Government')}}</option>
                    @if(! empty($governments))
                        @foreach($governments as $government)
                            <option value="{{$government->id}}">
                                {{$government->name_en}}
                            </option>

                        @endforeach
                    @endif

                </select>
                @if($errors->has("order_price"))
                    <span class="help-block">{{ $errors->first("order_price") }}</span>
                @endif
            </div>

            <div class="form-group col-md-2 col-sm-2">
                <label class="control-label" for="r_state_id"> {{__('backend.city')}}:</label>

                <select class=" form-control r_state_id" id="r_state_id" name="r_state_id">
                    <option value="">{{__('backend.Receiver_City')}}</option>

                </select>

            </div>
            <div
                class="form-group input-group col-md-2 col-sm-2 @if($errors->has('receiver_latitude')) has-error @endif">
                <label for="receiver_latitude"> {{__('backend.latitude')}}: </label>
                <input type="text" required id="receiver_latitude" name="receiver_latitude" class="form-control"
                       value="{{ old("receiver_latitude") ? old("receiver_latitude")  : '30.0668886'  }}"/>
                <span class="input-group-append">
              <button class="input-group-text bg-transparent" type="button" data-toggle="modal"
                      data-target="#map1Modal"> <i class="fa fa-map-marker"></i></button>
            </span>

                @if($errors->has("receiver_latitude"))
                    <span class="help-block">{{ $errors->first("receiver_latitude") }}</span>
                @endif
            </div>
            <div class="form-group col-md-2 col-sm-2 @if($errors->has('receiver_longitude')) has-error @endif">
                <label for="reciever_longitude"> {{__('backend.longitude')}}: </label>
                <input type="text" required id="receiver_longitude" name="receiver_longitude" class="form-control"
                       value="{{ old("receiver_longitude") ? old("receiver_longitude")  : '31.1962743'  }}"/>
                @if($errors->has("receiver_longitude"))
                    <span class="help-block">{{ $errors->first("receiver_longitude") }}</span>
                @endif
            </div>


            <div
                class="form-group col-md-6 col-sm-6 @if($errors->has('receiver_address')) has-error @endif">
                <label for="reciever_address">{{__('backend.Receiver_Address')}}: </label>

                <input type="text" required id="receiver_address" name="receiver_address" class="form-control"
                       value="{{ old("receiver_address") }}"/>


                @if($errors->has("receiver_address"))
                    <span class="help-block">{{ $errors->first("receiver_address") }}</span>
                @endif
            </div>

        </div>

        <div class="col-md-12 col-sm-12 col-xs-12" id="order_inputs"
             style="border: 2px solid #ea2;margin-bottom: 15px;">
            <h4 style="color: #ea2;font-weight: bold">{{__('backend.Order_Data')}}</h4>

            {{--            <div class="form-group col-md-2 col-sm-2 @if($errors->has('type')) has-error @endif"--}}
            {{--                 style="min-height:80px">--}}
            {{--                <label for="type-field">{{__('backend.order_type')}}</label>--}}
            {{--                <select class="form-control" id="order_type_id" name="order_type_id">--}}
            {{--                    <option value="" data-display="Select">{{__('backend.type')}}</option>--}}
            {{--                    @if(! empty($types))--}}
            {{--                        @foreach($types as $type)--}}
            {{--                            <option value="{{$type->id}}">--}}
            {{--                                {{$type->name_en}}--}}
            {{--                            </option>--}}

            {{--                        @endforeach--}}
            {{--                    @endif--}}
            {{--                </select>--}}
            {{--                @if($errors->has("type"))--}}
            {{--                    <span class="help-block">{{ $errors->first("type") }}</span>--}}
            {{--                @endif--}}
            {{--            </div>--}}

            <div class="form-group col-md-2 col-sm-2 @if($errors->has('type')) has-error @endif"
                 style="min-height:80px">
                <label for="type-field">{{__('backend.Type_Name')}}: </label>
                <input type="text" required id="type-field" name="type" class="form-control" value="{{ old("type") }}"/>
                @if($errors->has("type"))
                    <span class="help-block">{{ $errors->first("type") }}</span>
                @endif
            </div>

            <div class="form-group col-md-2 col-sm-2 @if($errors->has('main_store')) has-error @endif"
                 style="min-height:80px">
                <label for="store_id-field">{{__('backend.store')}}:</label>
                <select id="store_id-field" name="main_store" class="form-control">
                    <option value="">{{__('backend.Choose_Store')}}</option>
                    @if(! empty($allStores))
                        @foreach($allStores as $store)
                            <option value="{{$store->id}}" {{ $store->id == old('main_store') ? 'selected' : '' }} >
                                {{$store->name}}
                            </option>
                        @endforeach
                    @endif

                </select>
                @if($errors->has("main_store"))
                    <span class="help-block">{{ $errors->first("main_store") }}</span>
                @endif
            </div>

            <div class="form-group col-md-2 col-sm-2 @if($errors->has('stock_id')) has-error @endif"
                 style="min-height:80px">
                <label for="stock_id-field"> {{__('backend.stock')}}:</label>
                <select id="stock_id-field" name="store_id" class="form-control">
                    <option value="">{{__('backend.Choose_Stock')}}</option>
                    @if(! empty($stores))
                        @foreach($stores as $stock)
                            <option value="{{$stock->id}}" {{ $stock->id == old('store_id') ? 'selected' : '' }} >
                                {{$stock->title}}
                            </option>
                        @endforeach
                    @endif

                </select>
                @if($errors->has("stock_id"))
                    <span class="help-block">{{ $errors->first("stock_id") }}</span>
                @endif
            </div>

            <div class="form-group col-md-2 col-sm-2 @if($errors->has('qty')) has-error @endif" style="min-height:80px">
                <label for="qty-field"> {{__('backend.Quantity_Order')}}: </label>
                <input type="number" id="qty-field" name="qty" class="form-control"
                       value="{{ old("qty") ? old("qty") : '0' }}" @if(! empty($stores)) disabled @endif/>
                {{__('backend.available_quantity')}}: <span id="available_quantity">0</span>
                @if($errors->has("qty"))
                    <span class="help-block">{{ $errors->first("qty") }}</span>
                @endif
            </div>

            <div class="form-group col-md-2 col-sm-2 @if($errors->has('overload')) has-error @endif"
                 style="min-height:80px">
                <label for="overload-field"> {{__('backend.overload')}}:</label>
                <input type="number" required id="overload-field" name="overload" class="form-control"
                       value="{{ old("overload") ? old("overload") : '0' }}"/>
                @if($errors->has("overload"))
                    <span class="help-block">{{ $errors->first("overload") }}</span>
                @endif
            </div>

{{--            <div class="form-group col-md-2 col-sm-2 @if($errors->has('agent_id')) has-error @endif"--}}
{{--                 style="min-height:80px">--}}
{{--                <label for="agent_id-field"> {{__('backend.agent')}}: </label>--}}
{{--                <select id="agent_id-field" name="agent_id" class="form-control">--}}
{{--                    <option value=""> {{__('backend.Choose_Agent')}}</option>--}}
{{--                    @if(! empty($agents))--}}
{{--                        @foreach($agents as $agent)--}}
{{--                            <option value="{{$agent->id}}">--}}
{{--                                {{$agent->name}}--}}
{{--                            </option>--}}
{{--                        @endforeach--}}
{{--                    @endif--}}

{{--                </select>--}}
{{--                @if($errors->has("agent_id"))--}}
{{--                    <span class="help-block">{{ $errors->first("agent_id") }}</span>--}}
{{--                @endif--}}
{{--            </div>--}}

            <div class="form-group col-md-2 col-sm-2" style="min-height:80px">
                <label class="control-label" for="driver_id"> {{__('backend.driver')}}:</label>

                <select class=" form-control driver_id" id="driver_id" name="forward_driver_id">
                    <option value=""> {{__('backend.Choose_Driver')}}</option>
                    @if(! empty($drivers))

                        @foreach($drivers as $driver1)
                            <option value="{{$driver1->id}}">
                                {{$driver1->name}}
                            </option>
                        @endforeach

                    @endif

                </select>

            </div>

            <div class="form-group col-md-2 col-sm-2 @if($errors->has('delivery_price')) has-error @endif"
                 style="min-height:80px">
                <label for="delivery_price-field">{{__('backend.Delivery_Price')}}: </label>
                <input type="number" required class="form-control" id="delivery_price-field" name="delivery_price"
                       value="{{ old("delivery_price")  ? old("delivery_price")  : '0'}}">
                @if($errors->has("delivery_price"))
                    <span class="help-block">{{ $errors->first("delivery_price") }}</span>
                @endif
                <div class="has-error hidden" id="no_fees">
                    <span class="help-block">{{__('backend.no_fees')}}</span>
                </div>
            </div>


            <div class="form-group col-md-2 col-sm-2 @if($errors->has('')) has-error @endif" style="min-height:80px">
                <label for="order_price-field">{{__('backend.order_price')}}: </label>
                <input type="number" required class="form-control" id="order_price-field" name="order_price"
                       value="{{ old("order_price")  ? old("order_price")  : '0'}}">
            </div>

            <div class="form-group col-md-2 col-sm-2 @if($errors->has('payment_method_id')) has-error @endif"
                 style="min-height:80px">
                <label for="payment_method_id-field"> {{__('backend.Fees_From')}}:</label>
                <select id="payment_method_id-field" name="payment_method_id" class="form-control">
                    <option value="" data-display="Select"> {{__('backend.Choose_Method')}}</option>

                    @if(! empty($payments))
                        @foreach($payments as $payment)
                            <option value="{{$payment->id}}">
                                {{$payment->name}}
                            </option>
                        @endforeach
                    @endif

                </select>
                @if($errors->has("payment_method_id"))
                    <span class="help-block">{{ $errors->first("payment_method_id") }}</span>
                @endif
            </div>

            <div class="form-group col-md2 col-sm-2 " style="min-height:80px">
                <label for="payment_method_id-field"> {{__('backend.Total_Price')}}:</label>
                <div class="form-control" id="total_price">0</div>
            </div>

            <div class="form-group col-md-12 col-sm-12 @if($errors->has('notes')) has-error @endif"
                 style="min-height:80px">
                <label for="notes-field">{{__('backend.notes')}}: </label>

                <input type="text" required id="notes-field" name="notes" class="form-control"
                       value="{{ old("notes") }}"/>

                @if($errors->has("notes"))
                    <span class="help-block">{{ $errors->first("notes") }}</span>
                @endif
            </div>

        </div>

        <div class="col-md-12 col-sm-12 col-xs-12 text-center" style="margin-bottom: 15px;">
            <button type="button" class="btn btn-warning btn-block create-btn"
                    id="order-btn">{{__('backend.add_order')}}
            </button>
        </div>
    </form>
</div>

<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\PaymentMethod;
use Response;
use Flash;
use File;
use Auth;

class PaymentMethodController extends Controller {

	//Get All Payment Methods
	public function index()
	{
		$payment_methods = PaymentMethod::orderBy('id', 'desc')->paginate(50);
		return view('backend.payment_methods.index', compact('payment_methods'));
	}

	// create Payment Method Page
	public function create()
	{
		return view('backend.payment_methods.create');
	}

	// Added New Payment Method
	public function store(Request $request)
	{
			/*
			* status = 0 :: Not Verify
			* status = 1 :: Verify
			*/

			$v = \Validator::make($request->all(), [
						'name'    => 'required',
						'status'  => 'required',
			]);

			if ($v->fails()) {
					return redirect()->back()->withErrors($v->errors());
			}

			$payment_method 						= new PaymentMethod();
			$payment_method->name 			= $request->input("name");
      $payment_method->status 		= $request->input("status");
			$payment_method->save();

			// Function Add Action In Activity Log By Samir
			addActivityLog('Add New Payment Method ' . $payment_method->name , ' اضافة طريقة دفع جديدة ' . $payment_method->name, 25); // Helper Function in App\Helpers.php

      Flash::success(__('backend.PaymentMethod_saved_successfully'));
			return redirect()->route('mngrAdmin.payment_methods.index');
	}

	// View One Payment Method
	public function show($id)
	{
		return back();
		// $payment_method = PaymentMethod::findOrFail($id);
		// return view('backend.payment_methods.show', compact('payment_method'));
	}

	// Edit Payment Method Page
	public function edit($id)
	{
		$payment_method = PaymentMethod::findOrFail($id);
		return view('backend.payment_methods.edit', compact('payment_method'));
	}

	// Update Payment Method
	public function update(Request $request, $id)
	{
			/*
			* status = 0 :: Not Verify
			* status = 1 :: Verify
			*/

			$v = \Validator::make($request->all(), [
						'name'    => 'required',
						'status'  => 'required',
			]);

			if ($v->fails()) {
					return redirect()->back()->withErrors($v->errors());
			}

			$payment_method 						= PaymentMethod::findOrFail($id);
			$payment_method->name 			= $request->input("name");
      $payment_method->status 		= $request->input("status");
			$payment_method->save();

			// Function Add Action In Activity Log By Samir
			addActivityLog('Edit Payment Method ' . $payment_method->name , ' تعديل طريقة الدفع ' . $payment_method->name, 25); // Helper Function in App\Helpers.php

      Flash::warning(__('backend.PaymentMethod_Updated_successfully'));
			return redirect()->route('mngrAdmin.payment_methods.index');
	}

	// Delete Payment Method
	public function destroy($id)
	{
				$payment_method = PaymentMethod::findOrFail($id);
				$payment_method->delete();

				// Function Add Action In Activity Log By Samir
				addActivityLog('Delete Payment Method ' . $payment_method->name , ' حذف طريقة الدفع ' . $payment_method->name, 25); // Helper Function in App\Helpers.php

        flash(__('backend.PaymentMethod_deleted_successfully'))->error();
				return redirect()->route('mngrAdmin.payment_methods.index');
	}

}

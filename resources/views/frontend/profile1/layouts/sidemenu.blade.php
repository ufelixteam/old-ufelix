<!-- Start Nav -->
<div class="vertical-nav">
    <button class="collapse-menu">
        <i class="icon-menu2"></i>
    </button>
    <div class="user-details clearfix">



        <div class="user-details logoSideMenue">

            <a href="" class="logo-img" style="width: 65px !important">
                <img src="{{asset('assets/images/logo.png')}}">
            </a>
        </div>

        <a href="" class="user-img">
            <img src="{{ Auth::guard('Customer')->user() ?  Auth::guard('Customer')->user()->image : '' }}"
                 onerror="this.src='{{asset('assets/images/user.png')}}'"
                 alt="User Info">
            <span
                class="likes-info">{{ Auth::guard('Customer')->user() ?  Auth::guard('Customer')->user()->rate : '' }}</span>
        </a>
        <h5 class="user-name">{{ Auth::guard('Customer')->user() ?  Auth::guard('Customer')->user()->name : '' }}</h5>
    </div>
    <ul class="menu clearfix">
        <li class="active selected">
            <a href="{{ app()->getLocale() == 'en' ? url('/en/customer/account') : url('/customer/account') }}">
                <img src="{{asset('assets/images/user.ico')}}" class="personIconSidemenu">
                <!-- <i class="icon-user"></i> -->
                <span class="menu-item">@lang('website.Profile')</span>
            </a>
        </li>
        <li class="@if(Route::currentRouteName() == 'profile.orders' || Route::currentRouteName() == 'profile.orderview') active @endif">
            <a href="">
                <i class="icon-align-justify iconNew"></i>
                <span class="menu-item">@lang('website.orders')</span>
                <span class="down-arrow"></span>
            </a>
            <ul>
                <li class="@if(app('request')->input('type') == 'pending') active @endif">
                    <a href="{{ app()->getLocale() == 'en' ? url('/en/customer/account/orders?type=pending') : url('/customer/account/orders?type=pending') }}"><img src="{{asset('assets/images/pending.ico')}}"> @lang('website.pending_orders')</a>
                </li>
                <li class="@if(app('request')->input('type') == 'accept') active @endif">
                    <a href="{{ app()->getLocale() == 'en' ? url('/en/customer/account/orders?type=accept') : url('/customer/account/orders?type=accept') }}">
                    <img src="{{asset('assets/images/accepted.ico')}}">
                    @lang('website.accepted_orders')</a>
                </li>
                <li class="@if(app('request')->input('type') == 'receive') active @endif">
                    <a href="{{ app()->getLocale() == 'en' ? url('/en/customer/account/orders?type=receive') : url('/customer/account/orders?type=receive') }}">
                    <img src="{{asset('assets/images/received.ico')}}">
                    @lang('website.received_orders')</a>
                </li>
                <li class="@if(app('request')->input('type') == 'deliver') active @endif">
                    <a href="{{ app()->getLocale() == 'en' ? url('/en/customer/account/orders?type=deliver') : url('/customer/account/orders?type=deliver') }}">
                    <img src="{{asset('assets/images/delivered.ico')}}">
                    @lang('website.delivered_orders')</a>
                </li>
                <li class="@if(app('request')->input('type') == 'cancel') active @endif">
                    <a href="{{ app()->getLocale() == 'en' ? url('/en/customer/account/orders?type=cancel') : url('/customer/account/orders?type=cancel') }}">
                    <img src="{{asset('assets/images/cancelled.ico')}}">
                    @lang('website.cancelled_orders')</a>
                </li>
                <li class="@if(app('request')->input('type') == 'recall') active @endif">
                    <a href="{{ app()->getLocale() == 'en' ? url('/en/customer/account/orders?type=recall') : url('/customer/account/orders?type=recall') }}">
                    <img src="{{asset('assets/images/recalled.jpg')}}">
                    @lang('website.recalled_orders')</a>
                </li>
                <li class="@if(app('request')->input('type') == 'waiting') active @endif">
                    <a href="{{ app()->getLocale() == 'en' ? url('/en/customer/account/orders?type=waiting') : url('/customer/account/orders?type=waiting') }}">
                    <img src="{{asset('assets/images/waiting.ico')}}">
                    @lang('website.waiting_orders')</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="{{ app()->getLocale() == 'en' ? url('/en/customer/account/invoices') : url('/customer/account/invoices') }}">
                <i class="icon-envelope iconNew" style=""></i>
                <span class="menu-item"> @lang('website.invoices')</span>
            </a>
        </li>
        <li>
            <a href="{{ app()->getLocale() == 'en' ? url('/en/customer/account/wallets') : url('/customer/account/wallets') }}">
                <i class="icon-wallet iconNew"></i>
                <span class="menu-item"> @lang('website.wallets')</span>
            </a>
        </li>
        <li>
            <a href="{{ app()->getLocale() == 'en' ? url('/en/customer/account/upload-excel') : url('/customer/account/upload-excel') }}">
                <i class="icon-upload iconNew"></i>
                <span class="menu-item">@lang('website.UploadExcel')</span>
            </a>
        </li>
        <li>
            <a href="{{ app()->getLocale() == 'en' ? url('/en/customer/account/form-collections') : url('/customer/account/form-collections') }}">
                <i class="icon-file-text2 iconNew"></i>
                <span class="menu-item">  @lang('website.FormCollection')</span>
            </a>
        </li>
    </ul>
</div>
<!-- End Nav -->

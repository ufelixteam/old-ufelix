@extends('backend.layouts.app')
@section('css')
    <title>Invoices - View</title>
    {{--    <link rel="stylesheet" href="{{URL::asset('/public/css/custome.css')}}"/>--}}
@endsection
@section('header')
    <div class="page-header noprint">
        <h3>Invoice #{{$invoice->id}}</h3>
        <div class=" pull-right" role="group" id="group" aria-label="...">
            @if($message == 1 || $message === 2 || $invoice->status == 1)
                <button type="button" class="btn btn-success" disabled><i class="glyphicon glyphicon-usd"></i> Pay
                </button>
            @else
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal"><i
                        class="glyphicon glyphicon-usd"> Pay</i></button>
                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Pay Invoice</h4>
                            </div>
                            <div class="modal-body">
                                <div class="alert alert-danger cs-error" style="display: none; margin-top: 5px;">
                                    Something Went Wrong! please Try again in a minute.
                                </div>
                                <div id="VerifyForm">
                                    <form action="{{ route('mngrAdmin.invoices.send_verify_code') }}" method="POST">
                                        <div class="form-group">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" id="EntityType" name="EntityType"
                                                   value="{{$invoice->type}}">
                                            <input type="hidden" id="EntityId" name="EntityId"
                                                   value="{{$invoice->object_id}}">
                                            <label>Please Enter the Client Phone No. to Confirm:</label>
                                            <input class="form-control" type="number" id="phoneNo" name="phoneNo"
                                                   placeholder="Phone Number" required
                                                   value="{{ ! empty($invoice->owner) ? $invoice->owner->mobile : ''}}">
                                            <div class="alert alert-danger cs-alert"
                                                 style="display: none; margin-top: 5px;">
                                                Phone Number can not be <strong>Empty</strong> or <strong>Less</strong>
                                                than <strong>14</strong> Number!
                                            </div>
                                            <div class="alert alert-danger phone-alert"
                                                 style="display: none; margin-top: 5px;">
                                                Phone Number is <strong>Wrong</strong>!
                                            </div>
                                        </div>
                                        <button type="submit" id="confirmBott" class="btn btn-primary btn-block">
                                            Verify
                                        </button>
                                    </form>
                                </div>

                                <div id="confirmForm" style="display: none;">
                                    <form action="{{ route('mngrAdmin.invoices.verify_phone') }}" method="POST">
                                        <div class="form-group">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <label>Enter the Verification Code you Recieved: </label>
                                `            <input class="form-control" type="number" id="VerifyCode" name="mobile"
                                                   placeholder="Verificaion Code" required>
                                            <div class="alert alert-danger cs-alert"
                                                 style="display: none; margin-top: 5px;">
                                                Verification Code can not be <strong>Empty</strong> or
                                                <strong>Less</strong> than <strong>4</strong> Numbers!
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary" id="VerifyBott"
                                                name="VerifyButton">Confirm
                                        </button>
                                    </form>
                                </div>

                                <div id="successMsg" class="alert alert-success text-center" style="display: none;">
                                    Code Verification <strong>Done</strong>.
                                </div>

                            </div> <!-- End Modal Body -->

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
        @endif
        <!--<a class="btn btn-warning btn-group" role="group" href="{{ route('mngrAdmin.invoices.edit', $invoice->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>-->
            <button type="button" id="printInvoice" class="btn btn-info"><i class="glyphicon glyphicon-print"></i> Print
            </button>
        </div>
    </div>
@endsection

@section('content')

    <div class="row">
        @if($message == 1 && $invoice->status == 0)
            <div class="col-md-12 col-xs-12 col-sm-12 noprint">
                <div class="alert alert-danger" style="margin-top: 25px;">
                    Balance in the Invoice owner's Wallet is <strong>less</strong> than the total !
                </div>
            </div>
        @elseif($message == 2)
            <div class="col-md-12 col-xs-12 col-sm-12 noprint">
                <div class="alert alert-danger" style="margin-top: 25px;">
                    No Wallet <strong>Found</strong> For this User!
                </div>
            </div>
        @elseif($invoice->status == 1)
            <div class="col-md-12 col-xs-12 col-sm-12 noprint">
                <div class="alert alert-warning" style="margin-top: 25px;">
                    This Invoice is already <strong>Closed</strong>!
                </div>
            </div>
        @endif
        <div class="col-md-12 col-xs-12 col-sm-12">
            <h2 class="text-center">
                {{$invoice->owner->name}} {{$invoice->type_txt}} Orders
            </h2>

            <table class="table table-condensed table-striped text-center" style="margin-top: 25px;">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Delivered Item</th>
                    <th>Item Price</th>
                    <th>Order Date</th>
                    <th>Delivery Price</th>
                    <th>O.F.D Date</th>
                    <th>Delivered Date</th>

                    @if($invoice->type == 3)
                        <th>Driver Name</th>
                        <th>Driver Mobile</th>
                    @endif

                    @if($invoice->status == 0)
                        <th class="noprint">Transfer To Other Invoice</th>
                        <th class="pull-right"></th>
                    @endif
                </tr>
                </thead>

                <tbody>
                @foreach($AllAcceptedOrders as $index => $Order)
                    @if($Order->accepted_order_status == 4)
                        @if($Order->payment_method_id >= 5)
                            <tr style="color: red; background-color: yellow;">
                        @else
                            <tr style="color: red;">
                        @endif
                    @elseif($Order->payment_method_id >= 5)
                        <tr style="background-color: yellow;">
                    @else
                        <tr>
                            @endif
                            <td>{{$Order->id}}</td>
                            <td>{{$Order->type}}</td>
                            <td>{{$Order->order_price}}</td>
                            <th>{{$Order->created_at}}</th>
                            <td>{{$Order->delivery_price}}</td>
                            <td>{{$Order->received_date}}</td>
                            <td>{{$Order->delivery_date}}</td>

                            @if($invoice->type == 3)
                                <td>{{$Order->name}}</td>
                                <td>{{$Order->mobile}}</td>
                            @endif

                            @if($invoice->status == 0)
                                @if($Order->delivery_date == NULL)
                                    <td class="text-center noprint">
                                        <input type="checkbox" name="sendId[]" value="{{$Order->accepted->id}}"><br>
                                    </td>
                                @endif

                                @if($Order->delivery_date == NULL)
                                    <td class="pull-right">
                                        <a class="btn btn-xs btn-danger"
                                           href="{{route('mngrAdmin.orders.cancelOrder', $Order->id)}}">
                                            <i class="glyphicon glyphicon-remove"></i> Recall</a>
                                    </td>
                                @endif

                            @endif
                        </tr>
                        @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-12 col-xs-12 col-sm-12" style="margin-top: 30px;">
            <h2 class="text-center">
                {{$invoice->owner->name}}  {{$invoice->type_txt}} Bill
            </h2>

            <div id="Transaction">
                <form class="noprint" action="{{ route('mngrAdmin.invoices.Transaction') }}" method="POST">
                    <table class="table table-condensed table-striped text-center">
                        <thead style="color:#0e8a29">
                        <tr>
                            @if($invoice->type == 2)
                                <th>Order #</th>
                                <th>Order Price</th>
                                <th>Total Delivery</th>
                                <th>Pocket</th>
                                <th>Remain</th>
                                <th>Discount</th>

                                <th>Corporate Deserved</th>
                                <th>Ufelix Deserved</th>


                                <th width="40%">Target Percentage Discount %</th>
                            @else
                                <th>Total order Price</th>

                                <th>Total Delivery</th>
                                <th>
                                    {{$invoice->type_txt}} Pocket
                                </th>
                                @if($invoice->type == 1)
                                    <th>{{$invoice->type_txt}} Profit</th>
                                @endif
                                <th>
                                    {{$invoice->type_txt}} Commission
                                </th>
                                <th>UFelix Commission</th>
                                <th>
                                    {{$invoice->type_txt}} Depth
                                </th>
                                <th>UFelix Depth</th>
                                <th>Total {{$invoice->type_txt}} Deserved</th>
                                <th>Total UFelix Deserved</th>

                            @endif
                        </tr>

                        </thead>
                        <tbody>

                        <tr>
                            @if($invoice->type == 2)

                                <td>  {{ ! empty($orderDeliceredDone) ? $orderDeliceredDone : '0' }} </td>
                                <td>{{ $totalItem }} </td>

                                <td>  {{  is_null($totalDeliveryFinal) ? 0 : $totalDeliveryFinal}}</td>
                                <td>  {{  $totalDeliveryFinal - $othtotalDelivery}}</td>

                                <td>{{  is_null($othtotalDelivery) ? 0 : $othtotalDelivery}}</td>

                                <td id="percentTd">


                                    <strong
                                        id="percentTotal">{{ ( ( $invoice->target_percentage * $totalDeliveryFinal ) / 100 ) }}
                                    </strong>


                                </td>

                                <td id="delivery_price">{{  empty($othtotalDelivery ) && $othtotalDelivery > 0 ?   ($othtotalDelivery)-( ($invoice->target_percentage * $totalDeliveryFinal ) / 100 ) : 0 }}</td>

                                <td id="delivery_priceU">{{  empty($othtotalDelivery ) && $othtotalDelivery > 0 ?  0 :  ($othtotalDelivery)-( ($invoice->target_percentage * $totalDeliveryFinal ) / 100 )  }}</td>



                                <td>
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <div class='input-group  '>
                                                <input id="percent" type="text" class="form-control"
                                                       value="{{  $invoice->target_percentage ? $invoice->target_percentage : '0' }}"/>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-percent"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <a type="button" id="percentBtn" class="btn  btn-info btn-group"
                                               id="changePercent">حساب الإجمالي بعد الخصم
                                            </a>
                                        </div>
                                    </div>
                                </td>

                            @else
                                <td>{{$totalItem}}</td>
                                <td>{{$totalDelivery}}</td>
                                <td>{{$collectedDelivery}}</td>
                                @if($invoice->type == 1)
                                    <th>{{$invoice->owner->profit}} %</th>
                                @endif
                                <td>{{$commission}}</td>
                                <td>{{$totalDelivery-$commission}}</td>

                                @if($commission > $collectedDelivery )

                                    <td>0</td>
                                    <td>{{ ($commission) - $collectedDelivery }}</td>
                                    <td>{{ $commission }}</td>
                                    <td>{{ $totalItem - (($commission) - $collectedDelivery) }}</td>

                                @elseif(($commission) < $collectedDelivery)

                                    <td>{{ $collectedDelivery - ($commission) }}</td>
                                    <td>0</td>
                                    <td>{{ $commission }}</td>
                                    <td>{{ $totalItem - (($commission) - $collectedDelivery) }}</td>

                                @elseif(($commission) == $collectedDelivery)
                                    <td>0</td>
                                    <td>0</td>

                                    <td>{{ $commission }}</td>



                                    <td>{{ $totalItem - (($commission) - $collectedDelivery) }}</td>

                                @endif
                            @endif
                        </tr>
                        </tbody>


                    </table>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="Id" value="{{$invoice->id}}">
                    <input type="hidden" name="Type" value="{{$invoice->type}}">
                    <input type="hidden" name="objId" value="{{$invoice->object_id}}">
                    @if($invoice->type == 2)

                        <input type="hidden" name="deserved" value="{{$totalItem}}">
                    @else
                        <input type="hidden" name="deserved" value="{{is_null($result)? 0 : $result}}">
                    @endif
                    @if($invoice->status == 1)
                        <button type="submit" class="btn btn-primary btn-block noprint" id="TransactionBott"
                                name="Transaction" disabled>Confirm Transaction
                        </button>
                    @elseif($invoice->status == 0)
                        <button type="submit" class="btn btn-primary btn-block noprint" id="TransactionBott"
                                name="Transaction" disabled>Confirm Transaction
                        </button>
                    @endif
                </form>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

    <script>
        $(function () {
            $("#percentBtn").on('click', function () {
                var percent = parseFloat($('#percent').val());

                var percentVal = percent + " * " + " {{$totalDeliveryFinal}}" + "  / 100 ";
                // $('#percentT').text(percentVal);
                $("#percentTotal").text(percent * "{{ $totalDeliveryFinal}}" / 100);

                percentVal = percent * parseFloat("{{ $totalDeliveryFinal}}") / 100;

                if (parseFloat("{{$othtotalDelivery}}") > 0) {

                    if (parseFloat("{{$othtotalDelivery}}") - parseFloat(percentVal) > 0) {

                        $('#delivery_priceU').text(Math.abs(parseFloat("{{$othtotalDelivery}}") - parseFloat(percentVal)));
                        $('#delivery_price').text("0");

                    } else {
                        $('#delivery_price').text(Math.abs(parseFloat("{{$othtotalDelivery}}") - parseFloat(percentVal)));
                        $('#delivery_priceU').text("0");

                    }

                } else {
                    if (parseFloat("{{$othtotalDelivery}}") - parseFloat(percentVal) > 0) {
                        $('#delivery_price').text(Math.abs(parseFloat("{{$othtotalDelivery}}") - parseFloat(percentVal)));
                        $('#delivery_priceU').text("0");

                    } else {
                        $('#delivery_priceU').text(Math.abs(parseFloat("{{$othtotalDelivery}}") - parseFloat(percentVal)));
                        $('#delivery_price').text("0");

                    }

                }
            })
        })
    </script>
@endsection

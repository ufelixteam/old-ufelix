@extends('frontend.profile.layouts.app')

@section('title') @lang('website.Profile') @endsection

@section('content')

    @php
        $user = Auth::guard('Customer')->user();
    @endphp
    <div class="top-bar clearfix test">
        <div class="row gutter">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="page-title">
                    <h3>@lang('website.Profile')</h3>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <ul class="right-stats" id="mini-nav-right">
                    <li>
                        <a href="javascript:void(0)" class="btn btn-danger">
                            <span>76</span>Sales
                        </a>
                    </li>
                    <li>
                        <a href="" class="btn btn-info">
                            <span>18</span>Tasks
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" class="btn btn-success">
                            <i class="icon-download6"></i> Export
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row gutter">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="panel">
                <div class="website-performance">
                    <div class="row gutter">
                        <div class="col-lg-7 col-md-6 col-sm-6 col-xs-6">
                            <div class="performance">
                                <h5>Sales</h5>
                                <div class="performance-graph">
                                    <div id="sales" class="chart-height5"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6">
                            <div class="performance-stats">
                                <h3 id="salesOdometer" class="odometer">0</h3>
                                <p>21.2%<i class="icon-triangle-up up"></i></p>
                                <p>vs. 6.5% (prev.)</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="panel">
                <div class="website-performance">
                    <div class="row gutter">
                        <div class="col-lg-7 col-md-6 col-sm-6 col-xs-6">
                            <div class="performance">
                                <h5>Expenses</h5>
                                <div class="performance-graph">
                                    <div id="expenses" class="chart-height5"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6">
                            <div class="performance-stats">
                                <h3 id="expensesOdometer" class="odometer">0</h3>
                                <p>15.7%<i class="icon-triangle-down down"></i></p>
                                <p>vs. 8.3% (prev.)</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="panel">
                <div class="website-performance">
                    <div class="row gutter">
                        <div class="col-lg-7 col-md-6 col-sm-6 col-xs-6">
                            <div class="performance">
                                <h5>Profits</h5>
                            </div>
                            <div class="performance-graph">
                                <div id="profits" class="chart-height5"></div>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6">
                            <div class="performance-stats">
                                <h3 id="profitsOdometer" class="odometer">0</h3>
                                <p>19.3%<i class="icon-triangle-up up"></i></p>
                                <p>vs. 8.8% (prev.)</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <form
                action="{{ app()->getLocale() == 'en'  ? url('/en/customer/account/edit-profile') : url('/customer/account/edit-profile') }}"
                method="post">
                {!! csrf_field() !!}
                <div class="col-md-6 col-xs-12">
                    <div class="ribbon-wrapper card">
                        <div class="ribbon ribbon-default">@lang('website.profile_customer')</div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4">@lang('website.CustomerName'): </label>
                                <div class="col-12 col-md-8">
                                    <input class="form-control" type="text" name="@lang('website.CustomerName')" disabled
                                           value="{{ ! empty($user) ? $user->name : ''}}"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-4">@lang('website.CustomerEmail'): </label>
                                <div class="col-12 col-md-8">
                                    <input class="form-control" type="text" name="@lang('website.CustomerEmail')" disabled
                                           value="{{ ! empty($user) ? $user->email : ''}}"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-4">@lang('website.CustomerMobile'): </label>
                                <div class="col-12 col-md-8">
                                    <input class="form-control" type="text" name="@lang('website.CustomerMobile')" disabled
                                           value="{{ ! empty($user) ? $user->mobile : ''}}"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-4">@lang('website.CustomerActive'): </label>
                                <div class="col-12 col-md-8">
                                    {!! ! empty($user) ? $user->mobile_verify_span : '' !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="ribbon-wrapper card">
                        <div class="ribbon ribbon-info">@lang('website.profile_corporate')</div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-4">@lang('website.CorporateName'): </label>
                                <div class="col-12 col-md-8">
                                    <input class="form-control" type="text" name="@lang('website.CorporateName')" disabled
                                           value="{{! empty($user) && ! empty($user->Corporate) ? $user->Corporate->name : ''}}"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-4">@lang('website.CorporateEmail'): </label>
                                <div class="col-12 col-md-8">
                                    <input class="form-control" type="text" name="corporate_email" disabled
                                           value="{{! empty($user) && ! empty($user->Corporate) ? $user->Corporate->email : ''}}"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-4">@lang('website.CorporateMobile'): </label>
                                <div class="col-12 col-md-8">
                                    <input class="form-control" type="text" name="@lang('website.CorporateMobile')" disabled
                                           value="{{! empty($user) && ! empty($user->Corporate) ? $user->Corporate->mobile : ''}}"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;">
                                    @lang('website.CorporateActive'):
                                </label>
                                <div class="col-12 col-md-8">
                                    {!! ! empty($user) && ! empty($user->Corporate) ? $user->Corporate->active_span : '' !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
{{--                <div class="save">--}}
{{--                    <button type="submit" class="btn-save">Save</button>--}}
{{--                </div>--}}
            </form>
        </div>
    </div>

@endsection


@section('scripts')
    <script src="{{ asset('/profile/js/odometer.min.js')}}"></script>
    <script src="{{ asset('/profile/js/custom-odometer.js')}}"></script>

@endsection

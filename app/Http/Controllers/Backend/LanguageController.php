<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models\Language;
use Response;
use Flash;
use File;
use Auth;

class LanguageController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$languages = Language::orderBy('id', 'desc')->paginate(50);

		return view('backend.languages.index', compact('languages'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('backend.languages.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$language = new Language();

		$language->name = $request->input("name");
        $language->code = $request->input("code");

		$language->save();

        Flash::success(__('backend.Language_saved_successfully'));
		return redirect()->route('mngrAdmin.languages.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$language = Language::findOrFail($id);

		return view('backend.languages.show', compact('language'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$language = Language::findOrFail($id);

		return view('backend.languages.edit', compact('language'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$language = Language::findOrFail($id);

		$language->name = $request->input("name");
        $language->code = $request->input("code");

		$language->save();

        Flash::warning(__('backend.Language_Updated_successfully'));

		return redirect()->route('mngrAdmin.languages.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$language = Language::findOrFail($id);
		$language->delete();

        Flash::danger(__('backend.Language_deleted_successfully'));

		return redirect()->route('mngrAdmin.languages.index');
	}

}

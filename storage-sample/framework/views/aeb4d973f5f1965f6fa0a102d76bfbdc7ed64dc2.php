<script>

    var s_state_id = '';

    if ($("#payment_method_id-field").val() == "1" || $("#payment_method_id-field").val() == "2" || $("#payment_method_id-field").val() == "7" || $("#payment_method_id-field").val() == "5") {

        $("#order_price-field").val(0);
        $("#order_price-field").attr('readonly', 'readonly');
    } else {
        $("#order_price-field").prop("readonly", false);
    }

    /* */
    $("#payment_method_id-field").on('change', function () {
        if ($(this).val() == "1" || $(this).val() == "2" || $(this).val() == "7" || $("#payment_method_id-field").val() == "5") {

            $("#order_price-field").val(0);
            $("#order_price-field").attr('readonly', 'readonly');
        } else {
            $("#order_price-field").prop("readonly", false);
        }
    });


    /* reset Form */
    $('#reset-btn').on('click', function () {
        $.ajax({
            url: "<?php echo e(url('/mngrAdmin/tmporders_reset_view')); ?>/" + $(this).attr('data-value'),
            type: 'get',
            success: function (data) {
                if (data == "false") {
                    alert(' Something error')
                } else {
                    $('#create-form').html(data.view);

                }
            },
            error: function (data) {

                console.log('Error:', data);
            }
        });

    });
    /*view-order*/

    $(".view-order").on('click', function () {
        $.ajax({
            url: "<?php echo e(url('/mngrAdmin/tmporders_update_view')); ?>/" + $(this).attr('data-value'),
            type: 'get',
            success: function (data) {

                $('#create-form').html(data.view);
            },
            error: function (data) {

                console.log('Error:', data);
            }
        });

    });

    /*delete-order*/
    $(".delete-order").on('click', function () {
        var id = $(this).attr('data-value');
        $.confirm({
            title: '',
            theme: 'modern',
            class: 'danger',
            content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Confirm Delete</span></div><div style="font-weight:bold;"><p>Are You want To delete This Order ?</p></div>',
            buttons: {
                confirm: {
                    text: 'Delete',
                    btnClass: 'btn-blue',
                    action: function () {

                        $.ajax({
                            url: "<?php echo e(url('/mngrAdmin/delete_tmporder')); ?>",
                            type: 'post',
                            data: {'_token': "<?php echo e(csrf_token()); ?>", 'id': id},
                            success: function (data) {

                                jQuery('.alert-danger').html('');

                                jQuery('.alert-danger').show();

                                if (data != 'false') {
                                    jQuery('.alert-danger').append('<p>' + data + '</p>');

                                    setTimeout(function () {
                                        location.reload();
                                    }, 5000);
                                } else {
                                    jQuery('.alert-danger').append('<p>Something Error</p>');

                                }
                                window.scrollTo({top: 0, behavior: 'smooth'});


                            },
                            error: function (data) {

                                console.log('Error:', data);
                            }
                        });
                    }
                },
                cancel: {
                    text: 'Cancel',
                    action: function () {
                    }
                }
            }
        });
    });
    $("#order-btn").on('click', function () {
        /*send to temp*/
        $.ajax({
            url: "<?php echo e(url('/mngrAdmin/tmporders_create')); ?>",
            type: 'post',
            data: $("#order-form").serialize(),
            success: function (data) {
                if (data.errors) {
                    jQuery('.alert-danger').html('');
                    jQuery.each(data.errors, function (key, value) {
                        jQuery('.alert-danger').show();

                        jQuery('.alert-danger').append('<p>' + value + '</p>');
                    });
                    window.scrollTo({top: 0, behavior: 'smooth'});
                } else {
                    jQuery('.alert-danger').hide();
                    $('#collection_id').val(data.collection_id);
                    if (data.first_one == "1") {
                        /*create-btn*/
                        window.history.replaceState(null, null, "/mngrAdmin/orders/create?collection=" + data.collection_id);
                        $("#saved_collection_id").val(data.collection_id);
                        jQuery('#saved').show();
                    }
                    $('.list').html(data.view);
                    // //* change collection id*/
                    // $('.create-form').html(data.view_order);
                    // $('.js-form').html(data.js);
                    // $("#order-form")[0].reset();
                    $("#receiver_inputs").find('input[type=text]').val('');
                    $("#receiver_inputs").find('input[type=number]').val(0);
                    $("#receiver_inputs").find('select').each(function () {
                        $(this).prop('selectedIndex', 0);
                        $(this).change();
                    });
                    $("#r_state_id").html('');

                    $("#order_inputs").find('input[type=text]').val('');
                    $("#order_inputs").find('input[type=number]').val(0);
                    $("#order_inputs").find('select').each(function () {
                        $(this).prop('selectedIndex', 0);
                        $(this).change();
                    });
                }
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
    $(".s_government_id").on('change', function () {
        $('.s_state_id').html('');
        $.ajax({
            url: "<?php echo e(url('/get_cities')); ?>" + "/" + $(this).val(),
            type: 'get',
            data: {},
            success: function (data) {
                $('.s_state_id').html('');
                $.each(data, function (i, content) {
                    $('.s_state_id').append($("<option></option>").attr("value", content.id).text(content.city_name));
                });
                if (s_state_id) {
                    $('#s_state_id').val(s_state_id);
                }
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
        var receiver = $(".r_government_id").val();
        if (receiver != null) {

            $.ajax({
                url: "<?php echo e(url('/mngrAdmin/get_cost')); ?>" + "/" + $(this).val() + "/" + receiver,
                type: 'get',
                data: {},
                success: function (data) {
                    if (data == 0) {
                        $("#delivery_price-field").css("border-color", "red");
                    } else {
                        $("#delivery_price-field").css("border-color", "#d2d6de");
                    }
                    $('#delivery_price-field').val(data);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    });

    $(".r_government_id").on('change', function () {

        $.ajax({
            url: "<?php echo e(url('/get_cities')); ?>" + "/" + $(this).val(),
            type: 'get',
            data: {},
            success: function (data) {
                $('.r_state_id').html('');
                $.each(data, function (i, content) {
                    $('.r_state_id').append($("<option></option>").attr("value", content.id).text(content.city_name));
                });
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
        var sender = $(".s_government_id").val();
        if (sender != null) {
            $.ajax({
                url: "<?php echo e(url('/mngrAdmin/get_cost')); ?>" + "/" + sender + "/" + $(this).val(),
                type: 'get',
                data: {},
                success: function (data) {
                    if (data == 0) {
                        $("#delivery_price-field").css("border-color", "red");
                    } else {
                        $("#delivery_price-field").css("border-color", "#d2d6de");
                    }
                    $('#delivery_price-field').val(data);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }

    });
    /*get_customers*/
    $("#corparate_id").on('change', function () {
        $.ajax({
            //url: "<?php echo e(url('/mngrAdmin/get_customers/')); ?>"+"/"+$(this).val(),
            url: "<?php echo e(url('/mngrAdmin/corporates/order_details/')); ?>" + "/" + $(this).val(),
            type: 'get',
            data: {},
            success: function (data) {
                $('#customer_id').html('');
                $('#sender_name-field').val('');
                $('#sender_mobile-field').val('');
                $('#s_government_id').val('').change();
                $('#sender_address').val('');
                s_state_id = '';

                $.each(data.customers, function (i, content) {
                    $('#customer_id').append($("<option></option>").attr("value", content.id).text(content.name));
                    $('#customer_id').attr('data-name', content.name);
                    $('#customer_id').attr('data-mobile', content.mobile);
                });
                if (data.customers.length > 0) {
                    $('#sender_name-field').val(data.customers[0].name);
                    $('#sender_mobile-field').val(data.customers[0].mobile);
                }

                s_state_id = data.city_id;
                $('#s_government_id').val(data.government_id).change();
                $('#sender_address').val(data.address);
                $('#sender_latitude').val(data.latitude);
                $('#sender_longitude').val(data.longitude);
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });

        $.ajax({
            url: "<?php echo e(url('/mngrAdmin/get_stock_by_corporate/')); ?>" + "/" + $(this).val(),
            type: 'get',
            data: {},
            success: function (data) {
                if (data.length > 0) {
                    $('#stock_id-field').html('<option value="0" >Choose Stock</option>');

                    $.each(data, function (i, content) {
                        $('#stock_id-field').append($("<option></option>").attr("value", content.id).text(content.title));
                    });
                } else {
                    $('#stock_id-field').html('<option value="0" >Choose Stock</option>');
                }
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
    $("#customer_id").on('change', function () {
        $('#sender_name-field').val($(this).attr('data-name'));
        $('#sender_mobile-field').val($(this).attr('data-mobile'));
    });


    /*getdriver_by_agents*/
    $("#agent_id-field").on('change', function () {
        $.ajax({
            url: "<?php echo e(url('/mngrAdmin/getdriver_by_agents')); ?>" + "/" + $(this).val(),
            type: 'get',
            data: {},
            success: function (data) {
                $('#driver_id').html('<option value="" >Choose Drivers</option>');
                $.each(data, function (i, content) {
                    $('#driver_id').append($("<option></option>").attr("value", content.id).text(content.name));
                });
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
</script>
<?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/orders/js.blade.php ENDPATH**/ ?>
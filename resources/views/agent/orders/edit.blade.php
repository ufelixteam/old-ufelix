@extends('agent.layouts.app')
@section('css')

@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i>  Edit Orders #{{$order->id}}</h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <form action="{{ route('mngrAgent.orders.update', $order->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group col-md-6 col-sm-6 @if($errors->has('reciever_name')) has-error @endif">
                       <label for="reciever_name-field">Reciever_name</label>
                    <input type="text" id="reciever_name-field" name="reciever_name" class="form-control" value="{{ is_null(old("reciever_name")) ? $order->reciever_name : old("reciever_name") }}"/>
                       @if($errors->has("reciever_name"))
                        <span class="help-block">{{ $errors->first("reciever_name") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('reciever_mobile')) has-error @endif">
                       <label for="reciever_mobile-field">Reciever_mobile</label>
                    <input type="text" id="reciever_mobile-field" name="reciever_mobile" class="form-control" value="{{ is_null(old("reciever_mobile")) ? $order->reciever_mobile : old("reciever_mobile") }}"/>
                       @if($errors->has("reciever_mobile"))
                        <span class="help-block">{{ $errors->first("reciever_mobile") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('reciever_phone')) has-error @endif">
                       <label for="reciever_phone-field">Reciever_phone</label>
                    <input type="text" id="reciever_phone-field" name="reciever_phone" class="form-control" value="{{ is_null(old("reciever_phone")) ? $order->reciever_phone : old("reciever_phone") }}"/>
                       @if($errors->has("reciever_phone"))
                        <span class="help-block">{{ $errors->first("reciever_phone") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('reciever_latitude')) has-error @endif">
                       <label for="reciever_latitude-field">Reciever_latitude</label>
                    <input type="text" id="reciever_latitude-field" name="reciever_latitude" class="form-control" value="{{ is_null(old("reciever_latitude")) ? $order->reciever_latitude : old("reciever_latitude") }}"/>
                       @if($errors->has("reciever_latitude"))
                        <span class="help-block">{{ $errors->first("reciever_latitude") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('reciever_longitude')) has-error @endif">
                       <label for="reciever_longitude-field">Reciever_longitude</label>
                    <input type="text" id="reciever_longitude-field" name="reciever_longitude" class="form-control" value="{{ is_null(old("reciever_longitude")) ? $order->reciever_longitude : old("reciever_longitude") }}"/>
                       @if($errors->has("reciever_longitude"))
                        <span class="help-block">{{ $errors->first("reciever_longitude") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('reciever_address')) has-error @endif">
                       <label for="reciever_address-field">Reciever_address</label>
                    <textarea class="form-control" id="reciever_address-field" rows="4" name="reciever_address">{{ is_null(old("reciever_address")) ? $order->reciever_address : old("reciever_address") }}</textarea>
                       @if($errors->has("reciever_address"))
                        <span class="help-block">{{ $errors->first("reciever_address") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('reciever_famous_palce')) has-error @endif">
                       <label for="reciever_famous_palce-field">Reciever_famous_palce</label>
                    <textarea class="form-control" id="reciever_famous_palce-field" rows="4" name="reciever_famous_palce">{{ is_null(old("reciever_famous_palce")) ? $order->reciever_famous_palce : old("reciever_famous_palce") }}</textarea>
                       @if($errors->has("reciever_famous_palce"))
                        <span class="help-block">{{ $errors->first("reciever_famous_palce") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('sender_name')) has-error @endif">
                       <label for="sender_name-field">Sender_name</label>
                    <input type="text" id="sender_name-field" name="sender_name" class="form-control" value="{{ is_null(old("sender_name")) ? $order->sender_name : old("sender_name") }}"/>
                       @if($errors->has("sender_name"))
                        <span class="help-block">{{ $errors->first("sender_name") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('sender_mobile')) has-error @endif">
                       <label for="sender_mobile-field">Sender_mobile</label>
                    <input type="text" id="sender_mobile-field" name="sender_mobile" class="form-control" value="{{ is_null(old("sender_mobile")) ? $order->sender_mobile : old("sender_mobile") }}"/>
                       @if($errors->has("sender_mobile"))
                        <span class="help-block">{{ $errors->first("sender_mobile") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('sender_phone')) has-error @endif">
                       <label for="sender_phone-field">Sender_phone</label>
                    <input type="text" id="sender_phone-field" name="sender_phone" class="form-control" value="{{ is_null(old("sender_phone")) ? $order->sender_phone : old("sender_phone") }}"/>
                       @if($errors->has("sender_phone"))
                        <span class="help-block">{{ $errors->first("sender_phone") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('sender_latitude')) has-error @endif">
                       <label for="sender_latitude-field">Sender_latitude</label>
                    <input type="text" id="sender_latitude-field" name="sender_latitude" class="form-control" value="{{ is_null(old("sender_latitude")) ? $order->sender_latitude : old("sender_latitude") }}"/>
                       @if($errors->has("sender_latitude"))
                        <span class="help-block">{{ $errors->first("sender_latitude") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('sender_longitude')) has-error @endif">
                       <label for="sender_longitude-field">Sender_longitude</label>
                    <input type="text" id="sender_longitude-field" name="sender_longitude" class="form-control" value="{{ is_null(old("sender_longitude")) ? $order->sender_longitude : old("sender_longitude") }}"/>
                       @if($errors->has("sender_longitude"))
                        <span class="help-block">{{ $errors->first("sender_longitude") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('sender_address')) has-error @endif">
                       <label for="sender_address-field">Sender_address</label>
                    <textarea class="form-control" id="sender_address-field" rows="4" name="sender_address">{{ is_null(old("sender_address")) ? $order->sender_address : old("sender_address") }}</textarea>
                       @if($errors->has("sender_address"))
                        <span class="help-block">{{ $errors->first("sender_address") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('sender_famous_palce')) has-error @endif">
                       <label for="sender_famous_palce-field">Sender_famous_palce</label>
                    <textarea class="form-control" id="sender_famous_palce-field" rows="4" name="sender_famous_palce">{{ is_null(old("sender_famous_palce")) ? $order->sender_famous_palce : old("sender_famous_palce") }}</textarea>
                       @if($errors->has("sender_famous_palce"))
                        <span class="help-block">{{ $errors->first("sender_famous_palce") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('status')) has-error @endif">
                       <label for="status-field">Status</label>
                    <div class="btn-group" data-toggle="buttons"><label class="btn btn-primary"><input type="radio" value="true" name="status-field" id="status-field" autocomplete="off"> True</label><label class="btn btn-primary active"><input type="radio" name="status-field" value="false" id="status-field" autocomplete="off"> False</label></div>
                       @if($errors->has("status"))
                        <span class="help-block">{{ $errors->first("status") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('type')) has-error @endif">
                       <label for="type-field">Type</label>
                    <div class="btn-group" data-toggle="buttons"><label class="btn btn-primary"><input type="radio" value="true" name="type-field" id="type-field" autocomplete="off"> True</label><label class="btn btn-primary active"><input type="radio" name="type-field" value="false" id="type-field" autocomplete="off"> False</label></div>
                       @if($errors->has("type"))
                        <span class="help-block">{{ $errors->first("type") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('type_id')) has-error @endif">
                       <label for="type_id-field">Type_id</label>
                    <input type="text" id="type_id-field" name="type_id" class="form-control" value="{{ is_null(old("type_id")) ? $order->type_id : old("type_id") }}"/>
                       @if($errors->has("type_id"))
                        <span class="help-block">{{ $errors->first("type_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('weight_id')) has-error @endif">
                       <label for="weight_id-field">Weight_id</label>
                    <input type="text" id="weight_id-field" name="weight_id" class="form-control" value="{{ is_null(old("weight_id")) ? $order->weight_id : old("weight_id") }}"/>
                       @if($errors->has("weight_id"))
                        <span class="help-block">{{ $errors->first("weight_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('customer_id')) has-error @endif">
                       <label for="customer_id-field">Customer_id</label>
                    <input type="text" id="customer_id-field" name="customer_id" class="form-control" value="{{ is_null(old("customer_id")) ? $order->customer_id : old("customer_id") }}"/>
                       @if($errors->has("customer_id"))
                        <span class="help-block">{{ $errors->first("customer_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('payment_method_id')) has-error @endif">
                       <label for="payment_method_id-field">Payment_method_id</label>
                    <input type="text" id="payment_method_id-field" name="payment_method_id" class="form-control" value="{{ is_null(old("payment_method_id")) ? $order->payment_method_id : old("payment_method_id") }}"/>
                       @if($errors->has("payment_method_id"))
                        <span class="help-block">{{ $errors->first("payment_method_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('size_id')) has-error @endif">
                       <label for="size_id-field">Size_id</label>
                    <input type="text" id="size_id-field" name="size_id" class="form-control" value="{{ is_null(old("size_id")) ? $order->size_id : old("size_id") }}"/>
                       @if($errors->has("size_id"))
                        <span class="help-block">{{ $errors->first("size_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('notes')) has-error @endif">
                       <label for="notes-field">Notes</label>
                    <textarea class="form-control" id="notes-field" rows="4" name="notes">{{ is_null(old("notes")) ? $order->notes : old("notes") }}</textarea>
                       @if($errors->has("notes"))
                        <span class="help-block">{{ $errors->first("notes") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('order_number')) has-error @endif">
                       <label for="order_number-field">Order_number</label>
                    <input type="text" id="order_number-field" name="order_number" class="form-control" value="{{ is_null(old("order_number")) ? $order->order_number : old("order_number") }}"/>
                       @if($errors->has("order_number"))
                        <span class="help-block">{{ $errors->first("order_number") }}</span>
                       @endif
                    </div>
                <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAgent.orders.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  
@endsection


<div class="col-sm-12 col-md-12"  style="margin-top: 10px;">
		<div class="row">

		<div class="col-sm-5 col-md-5">
			<label for="status-field">{{__('backend.quantity')}}</label>
			<input type="number" id="quantity-field" name="quantity[]" class="form-control" placeholder="{{__('backend.quantity')}}" value="1" />
			@if($errors->has("quantity"))
			 <span class="help-block">{{ $errors->first("quantity") }}</span>
			@endif
		</div>

		<div class="form-group col-md-6 col-sm-6 @if($errors->has('items')) has-error @endif">
				 <label for="status-field">{{__('backend.shipment_material')}}</label>
	       <select id="items-field" name="items[]" class="form-control">
	            @if(! empty($shipment_tools))
	                @foreach($shipment_tools as $item)
	                    <option value="{{$item->id}}" {{ old("items") == $item->id ? 'selected' : '' }}>
													@if(session()->has('lang') == 'ar')
														{{$item->name_ar}}
													@else
													{{$item->name_en}}
													@endif
											 </option>
	                @endforeach
	            @endif
	        </select>
					@if($errors->has("items"))
					 <span class="help-block">{{ $errors->first("items") }}</span>
					@endif
	    </div>

	  	<div class="input-group">
	    	<span class="input-group-btn">
	        	<input type="button" id="add-field" value="+" class="btn btn-success add-more" style="margin-top: 25px;">
	    	</span>
		</div>
	</div>
</div>

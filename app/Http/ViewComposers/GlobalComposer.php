<?php

namespace App\Http\ViewComposers;

use App\Models\Device;
use App\Models\Governorate;
use App\Models\Notification;
use App\Models\Setting;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GlobalComposer
{

    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $allsettings = Setting::get();

        $settings = [];
        foreach ($allsettings as $setting) {

            $settings[$setting->key] = $setting->value;
            $settings[$setting->key . '_ar'] = $setting->value_ar;
        }

        $view->with('app_about', $settings);

        $app_governments = Governorate::orderBy('name_en', 'ASC')->get();
        if (Auth::guard('admin')->user() && (app('request')->is('mngrAdmin/*', 'en/mngrAdmin/*', 'ar/mngrAdmin/*'))) {
//            $count_notification = Notification::where('is_read', 0)->where('client_model', '3')->where('client_id', Auth::guard('admin')->user()->id)->count();
//
//            $admin_notification = Notification::where('is_read', 0)->where('client_model', '3')->where('client_id', Auth::guard('admin')->user()->id)->orderBy('created_at', 'DESC')->get();
            $view->with('count_notification', 0);
            $view->with('admin_notification', 0);

        }

        $view->with('app_governments', $app_governments);
        //1 admin , 2 driver , 3 agent , 4 customer
        if (Auth::guard('admin')->user() && (app('request')->is('mngrAdmin/*', 'en/mngrAdmin/*', 'ar/mngrAdmin/*'))) {
            if (Device::where('object_id', Auth::guard('admin')->user()->id)->doesntExist()) {
                Device::insert(['object_id' => Auth::guard('admin')->user()->id, 'object_type' => 1, 'device_id' => 10, 'device_type' => 'web', 'socket_token' => 'dIFLIOnM0jtTZ75WAAK2', 'status' => 0]);
            }
        } elseif (Auth::guard('Customer')->user() && (app('request')->is('customer/*', 'en/customer/*', 'ar/customer/*'))) {
            if (Device::where('object_id', Auth::guard('Customer')->user()->id)->doesntExist()) {
                Device::insert(['object_id' => Auth::guard('Customer')->user()->id, 'object_type' => 4, 'device_id' => 10, 'device_type' => 'web', 'socket_token' => 'dIFLIOnM0jtTZ75WAAK2', 'status' => 0]);
            }
        }
//        } elseif (Auth::guard('agent')->user()) {
//            if (Device::where('object_id', Auth::guard('agent')->user()->id)->doesntExist()) {
//                Device::insert(['object_id' => Auth::guard('agent')->user()->id, 'object_type' => 3, 'device_id' => 10, 'device_type' => 'web', 'socket_token' => 'dIFLIOnM0jtTZ75WAAK2', 'status' => 0]);
//            }
//        }

        if (Auth::guard('Customer')->user() && (app('request')->is('customer/*', 'en/customer/*', 'ar/customer/*'))) {
            $count_notification = Notification::where('is_read', 0)->where('client_model', '2')->where('client_id', Auth::guard('Customer')->user()->id)->count();
            $app_notification = Notification::where('is_read', 0)->where('client_model', '2')->where('client_id', Auth::guard('Customer')->user()->id)->take(5)->get();
            $view->with('count_notification', $count_notification);
            $view->with('app_notification', $app_notification);

        }

    }

}

?>

<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

	@include('captain.layouts.css')
	@yield('css')

    <style>
        .slimScrollBar{
        background: #0376d9 !important;
        width: 10px !important;
        opacity: 1 !important;
        }
    </style>


</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		@include('captain.layouts.navbar')

		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		@include('captain.layouts.sidemenu')
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">

					<div class="panel ">

						@yield('header')
						<div class="text-center col-md-10 col-md-offset-1 " >
							@include('flash::message')
						</div>

                        <div class="panel-body">
							@yield('content')
						</div>
					</div>

				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<footer>
			<div class="container-fluid">
				<p class="copyright">&copy; 2019 <a href="#" target="_blank">Ufelix</a>. All Rights Reserved.</p>
			</div>
		</footer>
	</div>

    <!-- Modal For import Excel sheet-->
    <div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="statusModalModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">{{__('backend.import_orders')}}</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{url('captainDashboard/reports/update_status')}}" method="post" name="validateStatusForm"
                          enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="file" name="file" id="input-file-max-fs" class="dropify"
                                       data-max-file-size="2M"
                                       accept=".xls,.xlsx,.xlsm,.xlsb,.xml,application/ms-excel,application/vnd.ms-excel"/>
                            </div>
                            <div class="col-6"></div>
                            <div class="col-md-12">
                                <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal"
                                        style="margin-left: 15px;">Close
                                </button>
                                <button type="submit"
                                        class="btn btn-primary pull-right">{{__('backend.import_orders')}}</button>
                            </div>

                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>

    @include('captain.layouts.js')
	@yield('scripts')

</body>

</html>

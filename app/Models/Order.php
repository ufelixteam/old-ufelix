<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Models\Activity;

class Order extends Model
{
//    use LogsActivity;

    protected static $logAttributes = ['status'];

    /*
        redirect :: for packup == 1 another order null or 0

    */

//    protected $appends = ['total_price', 'corporate_name'];

    protected $guarded = ['id'];

    protected $hidden = ['updated_at', 'r_state_id', 's_state_id'];

    public function from_government()
    {
        return $this->belongsTo('App\Models\Governorate', 's_government_id');
    }

    public function to_government()
    {
        return $this->belongsTo('App\Models\Governorate', 'r_government_id');
    }

    public function to_city()
    {
        return $this->belongsTo('App\Models\City', 'r_state_id');
    }

    public function types()
    {
        return $this->belongsTo('App\Models\OrderType', 'order_type_id');
    }

    public function images()
    {
        return $this->hasMany('App\Models\OrderImage', 'order_id');
    }

    public function logs()
    {
        return $this->hasMany(Activity::class, 'subject_id')
            ->where('subject_type', 'App\Models\Order');
    }

    public function tickets()
    {
        return $this->hasMany('App\Models\Ticket', 'order_id');
    }

    public function cancel_requests()
    {
        return $this->hasMany('App\Models\OrderCancelRequest', 'order_id');
    }

    public function delivery_problems()
    {
        return $this->hasMany('App\Models\OrderDeliveryProblem', 'order_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\OrderComment', 'order_id');
    }

    public function order_delay()
    {
        return $this->hasMany('App\Models\OrderDelay', 'order_id');
    }

    public function agent_review()
    {
        //  return $this->hasOne('App\Models\OrderView','order_id');
        return $this->hasOne('App\Models\Review', 'order_id');
    }

    public function drivers()
    {
        return $this->hasMany('App\Models\AcceptedOrder', 'order_id');
    }

    public function driver()
    {
        return $this->hasOne('App\Models\AcceptedOrder', 'order_id')
            ->join('drivers', 'drivers.id', '=', 'accepted_orders.driver_id')
            ->where('accepted_orders.status', '!=', 4);
    }

    public function accepted_driver()
    {
        return $this->hasOne('App\Models\AcceptedOrder', 'order_id')
            ->where('accepted_orders.status', '!=', 4);
    }

    public function InvoiceCaptain()
    {
        return $this->hasOne('App\Models\AcceptedOrder', 'order_id')
            ->orderBy('created_at', 'DESC');
    }

    public function getAcceptedAttribute()
    {
        //status = 4 cancelled order
        return AcceptedOrder::where('order_id', $this->id)->where('status', '!=', 4)->first();
    }

    public function oneDriver()
    {
        return $this->belongsToMany('App\Models\Driver', 'accepted_orders', 'order_id', 'driver_id')
            ->where('accepted_orders.status', '!=', 4);
    }

    public function color()
    {
        return $this->belongsTo('App\Models\Color', 'color_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }

    public function payment_method()
    {
        return $this->belongsTo('App\Models\PaymentMethod', 'payment_method_id');
    }/*
    public function typeData() {
        return $this->belongsTo('App\Models\OrderType','id');
    }
    public function type() {
        return $this->belongsTo('App\Models\VehicleType','type_id');
    }*/

    public function governorate_prices()
    {
        return $this->belongsTo('App\Models\GovernoratePrice', 'governorate_cost_id');
    }

    public function agent()
    {
        return $this->belongsTo('App\Models\Agent', 'agent_id');
    }

    public function warehouse()
    {
        return $this->belongsTo('App\Models\Store', 'moved_in');
    }

    /*we must checkstatus*/
    public function getStatusSpanAttribute()
    {
        if ($this->status == 0) {
            return "<span class='badge badge-pill label-black'>" . __('backend.pending') . "</span>";
        } else if ($this->status == 1) {
            return "<span class='badge badge-pill label-info'>" . __('backend.accepted') . "</span>";
        } else if ($this->status == 2) {
            return "<span class='badge badge-pill label-warning'>" . __('backend.received') . "</span>";
        } else if ($this->status == 3) {
            return "<span class='badge badge-pill label-success'>" . __('backend.delivered') . "</span>";
        } else if ($this->status == 4) {
            if ($this->status_before_cancel) {
                return "<span class='badge badge-pill label-danger'>" . __('backend.cancelled') . " </span>";
            } else {
                return "<span class='badge badge-pill label-danger'>" . __('backend.unverified') . " </span>";
            }
        } else if ($this->status == 5) {
            return "<span class='badge badge-pill label-danger'>" . __('backend.recalled') . "</span>";
        } else if ($this->status == 6) {
            return "<span class='badge badge-pill label-primary'>" . __('backend.waiting') . "</span>";
        } else if ($this->status == 7) {
            return "<span class='badge badge-pill label-black'>" . __('backend.dropped') . "</span>";
        } else if ($this->status == 8) {
            return "<span class='badge badge-pill label-danger'>" . __('backend.rejected') . "</span>";
        }
    }

    public function getWebsiteStatusSpanAttribute()
    {
        if ($this->status == 0) {
            return "<span class='badge badge-pill label-black'>" . __('backend.pending') . "</span>";
        } else if ($this->status == 1) {
            return "<span class='badge badge-pill label-info'>" . __('backend.accepted') . "</span>";
        } else if ($this->status == 2) {
            return "<span class='badge badge-pill label-warning'>" . __('backend.received_span') . "</span>";
        } else if ($this->status == 3) {
            return "<span class='badge badge-pill label-success'>" . __('backend.delivered') . "</span>";
        } else if ($this->status == 4) {
            if ($this->status_before_cancel) {
                return "<span class='badge badge-pill label-danger'>" . __('backend.cancelled') . " </span>";
            } else {
                return "<span class='badge badge-pill label-danger'>" . __('backend.unverified') . " </span>";
            }
        } else if ($this->status == 5) {
            return "<span class='badge badge-pill label-danger'>" . __('backend.recalled') . "</span>";
        } else if ($this->status == 6) {
            return "<span class='badge badge-pill label-primary'>" . __('backend.waiting') . "</span>";
        } else if ($this->status == 7) {
            return "<span class='badge badge-pill label-black'>" . __('backend.dropped') . "</span>";
        } else if ($this->status == 8) {
            return "<span class='badge badge-pill label-danger'>" . __('backend.rejected') . "</span>";
        }
    }

    public function getProblemsTableAttribute()
    {
        $table = '';
        if (count($this->comments)) {
            $clientComments = $this->comments->where('user_type', 2)->all();
            $ufelixComments = $this->comments->whereIn('user_type', [1, 3])->all();

            $table .= '<table class="scan-problems table table-condensed table-striped text-center">
                            <thead>
                            <tr>
                                <th>' . __('backend.driver') . '</th>
                                <th>' . __('backend.reason') . '</th>
                                <th>' . __('backend.date') . '</th>
                                <th>' . __('backend.created_by') . '</th>
                            </tr>
                            </thead>';

            if (count($clientComments)) {
                $table .= '<tr>
                                 <td colspan="4" class="text-center"><strong>' . __('backend.client_comments') . '</strong></td>

                            </tr>';
                foreach ($clientComments as $comment) {
                    $table .= '<tr>
                                 <td>' . (isset($comment->driver->name) ? str_replace("'", "", $comment->driver->name) : '-') . '</td>
                                 <td>' . ($comment->comment) . '</td >
                                 <td>' . (date("Y-m-d", strtotime($comment->created_at))) . '</td >
                                 <td>' . (!empty($comment->user) ? $comment->user->name : '-') . '</td >
                            </tr>';
                }
            }

            if (count($ufelixComments)) {
                $table .= '<tr>
                                 <td colspan="4" class="text-center"><strong>' . __('backend.ufelix_comments') . '</strong></td>

                            </tr>';
                foreach ($ufelixComments as $comment) {
                    $table .= '<tr>
                                 <td>' . (isset($comment->driver->name) ? str_replace("'", "", $comment->driver->name) : '-') . '</td>
                                 <td>' . ($comment->comment) . '</td >
                                 <td>' . (date("Y-m-d", strtotime($comment->created_at))) . '</td >
                                 <td>' . (!empty($comment->user) ? $comment->user->name : '-') . '</td >
                            </tr>';
                }
            }


            $table .= '<tbody>
                            </tbody>
                        </table>';
        }

        return $table;
    }

    public function getInvoiceStatusSpanAttribute()
    {
        if ($this->status == 2) {
            return "<span class='badge badge-pill label-warning'>" . __('backend.received') . "</span>";
        } else if ($this->status == 3) {
            if (($this->status == 3 && $this->delivery_status == 1) || ($this->status == 3 && !$this->delivery_status)) {
                return "<span class='badge badge-pill label-success'>" . __('backend.full_delivered') . "</span>";
            } elseif ($this->status == 3 && $this->delivery_status == 2) {
                return "<span class='badge badge-pill label-success'>" . __('backend.pr') . "</span>";
            } elseif ($this->status == 3 && $this->delivery_status == 4) {
                return "<span class='badge badge-pill label-success'>" . __('backend.replace') . "</span>";
            }
        } else if ($this->status == 5) {
            return "<span class='badge badge-pill label-danger'>" . __('backend.recalled') . "</span>";
//            if ($this->recalled_by == 2) {
//                return "<span class='badge badge-pill label-danger'>" . __('backend.recall_by_receiver') . "</span>";
//            } elseif ($this->recalled_by == 3) {
//                return "<span class='badge badge-pill label-danger'>" . __('backend.recall_by_sender') . "</span>";
//            }
        } else if ($this->status == 8) {
            return "<span class='badge badge-pill label-danger'>" . __('backend.rejected') . "</span>";
        } else if ($this->status == 4) {
            return "<span class='badge badge-pill label-danger'>" . __('backend.cancelled') . "</span>";
        }
    }

    public function getInvoiceStatusPrintAttribute()
    {
        if ($this->status == 2) {
            return "OFD";
        } else if ($this->status == 3) {
            if (($this->status == 3 && $this->delivery_status == 1) || ($this->status == 3 && !$this->delivery_status)) {
                return "Delivered";
            } elseif ($this->status == 3 && $this->delivery_status == 2) {
                return "Part";
            } elseif ($this->status == 3 && $this->delivery_status == 4) {
                return "Replace";
            }
        } else if ($this->status == 5) {
            return "Recall";
//            if ($this->recalled_by == 2) {
//                return "<span class='badge badge-pill label-danger'>" . __('backend.recall_by_receiver') . "</span>";
//            } elseif ($this->recalled_by == 3) {
//                return "<span class='badge badge-pill label-danger'>" . __('backend.recall_by_sender') . "</span>";
//            }
        } else if ($this->status == 8) {
            return "Reject";
        } else if ($this->status == 4) {
            return "Cancelled";
        }
    }

    public function getCostAttribute()
    {

        if (($this->status == 3 && !$this->delivery_status) || ($this->status == 3 && $this->delivery_status == 1)) {
            return $this->order_price;
        } elseif ($this->status == 5) {
            return 0;
        } elseif ($this->collected_cost) {
            return $this->collected_cost;
        } else {
            return 0;
        }

    }

    public function getInvoicePlaceAttribute()
    {
        if ($this->is_refund == 1) {
            return __('backend.refund');
        } else if ($this->warehouse_dropoff == 1) {
            return __('backend.w.h');
        } else if ($this->client_dropoff == 1) {
            return __('backend.client');
        }

        return '-';
    }

    public function getHasCommentsDuringTwoDaysAttribute()
    {
        if ($this->status == 2 || $this->status == 5 || $this->status == 8) {
            $from_date = $this->status == 2 ? date('Y-m-d H:i:s', strtotime($this->received_at)) : date('Y-m-d H:i:s', strtotime($this->recalled));
//            $to_date = date('Y-m-d H:i:s', strtotime($from_date. ' + 2 days'));

            $comments = collect($this->comments)->where('created_at', '>=', $from_date)
//                ->where('created_at', '<=', $to_date)
                ->count();

            if ($comments) {
                return true;
            }
        }

        return false;
    }

    public function getCorporateDeservedAttribute()
    {
        if ($this->payment_method_id == 1 && $this->status == 3) {
            return $this->order_price - ($this->overweight_cost + $this->delivery_price);
        } elseif ($this->payment_method_id == 2 && $this->status == 3) {
            return $this->order_price;
        } elseif ($this->payment_method_id == 1 && $this->status == 5) {
            return ($this->overweight_cost + $this->delivery_price) * -1;
        } elseif ($this->payment_method_id == 2 && $this->status == 5) {
            return ($this->overweight_cost + $this->delivery_price) * -1;
        } elseif ($this->status == 2) {
            return $this->order_price;
        }
    }

    public function getLastStatusDateAttribute()
    {
        if ($this->status == 0) {
            return $this->created_at;
        } else if ($this->status == 1) {
            return $this->accepted_at;
        } else if ($this->status == 2) {
            return $this->received_at;
        } else if ($this->status == 3) {
            return $this->delivered_at;
        } else if ($this->status == 4) {
            return $this->cancelled_at;
        } else if ($this->status == 5) {
            return $this->recalled_at;
        } else if ($this->status == 7) {
            return $this->dropped_at;
        } else if ($this->status == 8) {
            return $this->rejected_at;
        }
    }

    public function getProfileLastStatusDateAttribute()
    {
        if ($this->status == 0) {
            return $this->created_at;
        } else if ($this->status == 1) {
            return $this->accepted_at;
        } else if ($this->status == 2) {
            return $this->received_at;
        } else if ($this->status == 3) {
            return $this->delivered_date;
        } else if ($this->status == 4) {
            return $this->cancelled_at;
        } else if ($this->status == 5) {
            return $this->recalled_at;
        } else if ($this->status == 7) {
            return $this->dropped_at;
        } else if ($this->status == 8) {
            return $this->rejected_at;
        }
    }

    public function getRecallSpanAttribute()
    {
        if ($this->is_refund == 1) {
            return "<span class='badge badge-pill label-refund'>" . __('backend.refund') . "</span>";
        } elseif ($this->status == 3 && $this->delivery_status == 2) {
            return "<span class='badge badge-pill label-success'>" . __('backend.pr') . "</span>";
        } elseif ($this->status == 3 && $this->delivery_status == 4) {
            return "<span class='badge badge-pill label-black'>" . __('backend.replace') . "</span>";
        } elseif ($this->recalled_by == 1) {
            return "<span class='badge badge-pill label-black'>" . __('backend.admin') . "</span>";
        } else if ($this->recalled_by == 2) {
            return "<span class='badge badge-pill label-info'>" . __('backend.receiver') . "</span>";
        } else if ($this->recalled_by == 3) {
            return "<span class='badge badge-pill label-warning'>" . __('backend.sender') . "</span>";
        } else if ($this->status == 4) {
            return "<span class='badge badge-pill label-danger'>" . __('backend.cancelled') . "</span>";
        } else if ($this->status == 8) {
            return "<span class='badge badge-pill label-danger'>" . __('backend.rejected') . "</span>";
        } else {
            return "<span class='badge badge-pill label-danger'>" . __('backend.recalled') . "</span>";
        }
    }

    public function getStatusDetailsAttribute()
    {
        if ($this->status == 0) {
            return "<span class='badge badge-pill label-black'>" . __('backend.pending') . "</span>";
        } else if ($this->status == 1) {
            return "<span class='badge badge-pill label-info'>" . __('backend.accepted') . "</span>";
        } else if ($this->status == 2) {
            return "<span class='badge badge-pill label-warning'>" . __('backend.received') . "</span>";
        } else if ($this->status == 7) {
            return "<span class='badge badge-pill label-black'>" . __('backend.dropped') . "</span>";
        } else if ($this->is_refund == 1) {
            return "<span class='badge badge-pill label-refund'>" . __('backend.refund') . "</span>";
        } elseif (($this->status == 3 && $this->delivery_status == 1) || ($this->status == 3 && !$this->delivery_status)) {
            return "<span class='badge badge-pill label-success'>" . __('backend.full_delivered') . "</span>";
        } elseif ($this->status == 3 && $this->delivery_status == 2) {
            return "<span class='badge badge-pill label-success'>" . __('backend.pr') . "</span>";
        } elseif ($this->status == 3 && $this->delivery_status == 4) {
            return "<span class='badge badge-pill label-success'>" . __('backend.replace') . "</span>";
        } elseif ($this->status == 5) {
            return "<span class='badge badge-pill label-danger'>" . __('backend.recall') . "</span>";
        } else if ($this->status == 4) {
            if ($this->status_before_cancel) {
                return "<span class='badge badge-pill label-danger'>" . __('backend.cancelled') . " </span>";
            } else {
                return "<span class='badge badge-pill label-danger'>" . __('backend.unverified') . " </span>";
            }
        } else if ($this->status == 8) {
            return "<span class='badge badge-pill label-danger'>" . __('backend.rejected') . "</span>";
        } else {
            return "";
        }
    }

    public function getDeliveredDateAttribute()
    {

        $startTime = Carbon::parse($this->delivered_at);
        $finishTime = Carbon::parse($this->received_at);
        $totalDuration = $finishTime->diffInSeconds($startTime);

        if ($this->status == 3 && $totalDuration < 172800 && in_array($this->r_government_id, [1, 2])) {
            return Carbon::parse($this->received_at)->addSeconds(172800)->format('D, M d, Y h:m A');
        } elseif ($this->status == 3 && $totalDuration < 331200 && !in_array($this->r_government_id, [1, 2])) {
            return Carbon::parse($this->received_at)->addSeconds(331200)->format('D, M d, Y h:m A');
        }

        return $this->delivered_at;
    }

    public function getIsReceivedAttribute()
    {
        $startTime = Carbon::parse($this->received_at);
        $finishTime = Carbon::now();
        $totalDuration = $finishTime->diffInSeconds($startTime);

        if (($totalDuration < 172800 && $this->status == 3 && in_array($this->r_government_id, [1, 2])) ||
            ($totalDuration < 331200 && $this->status == 3 && !in_array($this->r_government_id, [1, 2]))) {
            return true;
        }

        return false;
    }

    public function getProfileStatusDetailsAttribute()
    {
        if ($this->status == 0) {
            return "<span class='badge badge-pill label-black'>" . __('backend.pending') . "</span>";
        } else if ($this->status == 1) {
            return "<span class='badge badge-pill label-info'>" . __('backend.accepted') . "</span>";
        } else if ($this->status == 2) {
            return "<span class='badge badge-pill label-warning'>" . __('backend.received') . "</span>";
        } else if ($this->status == 7) {
            return "<span class='badge badge-pill label-black'>" . __('backend.dropped') . "</span>";
        } else if ($this->is_refund == 1) {
            return "<span class='badge badge-pill label-refund'>" . __('backend.refund') . "</span>";
        } else if ($this->status == 3) {

            if ($this->is_received) {
                return "<span class='badge badge-pill label-warning'>" . __('backend.received') . "</span>";
            } else {
                if (($this->status == 3 && $this->delivery_status == 1) || ($this->status == 3 && !$this->delivery_status)) {
                    return "<span class='badge badge-pill label-success'>" . __('backend.full_delivered') . "</span>";
                } elseif ($this->status == 3 && $this->delivery_status == 2) {
                    return "<span class='badge badge-pill label-success'>" . __('backend.pr') . "</span>";
                } elseif ($this->status == 3 && $this->delivery_status == 4) {
                    return "<span class='badge badge-pill label-success'>" . __('backend.replace') . "</span>";
                }
            }
        } elseif ($this->status == 5) {
            return "<span class='badge badge-pill label-danger'>" . __('backend.recall') . "</span>";
        } else if ($this->status == 4) {
            if ($this->status_before_cancel) {
                return "<span class='badge badge-pill label-danger'>" . __('backend.cancelled') . " </span>";
            } else {
                return "<span class='badge badge-pill label-danger'>" . __('backend.unverified') . " </span>";
            }
        } else if ($this->status == 8) {
            return "<span class='badge badge-pill label-danger'>" . __('backend.rejected') . "</span>";
        } else {
            return "";
        }
    }

    public function getStatusDetailsWithoutRefundAttribute()
    {
        if ($this->status == 0) {
            return "<span class='badge badge-pill label-black'>" . __('backend.pending') . "</span>";
        } else if ($this->status == 1) {
            return "<span class='badge badge-pill label-info'>" . __('backend.accepted') . "</span>";
        } else if ($this->status == 2) {
            return "<span class='badge badge-pill label-warning'>" . __('backend.received') . "</span>";
        } else if ($this->status == 7) {
            return "<span class='badge badge-pill label-black'>" . __('backend.dropped') . "</span>";
        } elseif (($this->status == 3 && $this->delivery_status == 1) || ($this->status == 3 && !$this->delivery_status)) {
            return "<span class='badge badge-pill label-success'>" . __('backend.full_delivered') . "</span>";
        } elseif ($this->status == 3 && $this->delivery_status == 2) {
            return "<span class='badge badge-pill label-success'>" . __('backend.pr') . "</span>";
        } elseif ($this->status == 3 && $this->delivery_status == 4) {
            return "<span class='badge badge-pill label-success'>" . __('backend.replace') . "</span>";
        } elseif ($this->status == 5) {
            return "<span class='badge badge-pill label-danger'>" . __('backend.recall') . "</span>";
        } else if ($this->status == 4) {
            if ($this->status_before_cancel) {
                return "<span class='badge badge-pill label-danger'>" . __('backend.cancelled') . " </span>";
            } else {
                return "<span class='badge badge-pill label-danger'>" . __('backend.unverified') . " </span>";
            }
        } else if ($this->status == 8) {
            return "<span class='badge badge-pill label-danger'>" . __('backend.rejected') . "</span>";
        } else {
            return "";
        }
    }

    public function getProfileStatusDetailsWithoutRefundAttribute()
    {
        if ($this->status == 0) {
            return "<span class='badge badge-pill label-black'>" . __('backend.pending') . "</span>";
        } else if ($this->status == 1) {
            return "<span class='badge badge-pill label-info'>" . __('backend.accepted') . "</span>";
        } else if ($this->status == 2) {
            return "<span class='badge badge-pill label-warning'>" . __('backend.received') . "</span>";
        } else if ($this->status == 7) {
            return "<span class='badge badge-pill label-black'>" . __('backend.dropped') . "</span>";
        } else if ($this->status == 3) {

            if ($this->is_received) {
                return "<span class='badge badge-pill label-warning'>" . __('backend.received') . "</span>";
            } else {
                if (($this->status == 3 && $this->delivery_status == 1) || ($this->status == 3 && !$this->delivery_status)) {
                    return "<span class='badge badge-pill label-success'>" . __('backend.full_delivered') . "</span>";
                } elseif ($this->status == 3 && $this->delivery_status == 2) {
                    return "<span class='badge badge-pill label-success'>" . __('backend.pr') . "</span>";
                } elseif ($this->status == 3 && $this->delivery_status == 4) {
                    return "<span class='badge badge-pill label-success'>" . __('backend.replace') . "</span>";
                }
            }
        } elseif ($this->status == 5) {
            return "<span class='badge badge-pill label-danger'>" . __('backend.recall') . "</span>";
        } else if ($this->status == 4) {
            if ($this->status_before_cancel) {
                return "<span class='badge badge-pill label-danger'>" . __('backend.cancelled') . " </span>";
            } else {
                return "<span class='badge badge-pill label-danger'>" . __('backend.unverified') . " </span>";
            }
        } else if ($this->status == 8) {
            return "<span class='badge badge-pill label-danger'>" . __('backend.rejected') . "</span>";
        } else {
            return "";
        }
    }

    public function getCancelSpanAttribute()
    {
        if ($this->cancelled_by == 1) {
            return "<span class='badge badge-pill label-black'>" . __('backend.admin') . "</span>";
        } else if ($this->cancelled_by == 3) {
            return "<span class='badge badge-pill label-info'>" . __('backend.receiver') . "</span>";
        } else if ($this->cancelled_by == 2) {
            return "<span class='badge badge-pill label-warning'>" . __('backend.sender') . "</span>";
        } else {
            return "";
        }
    }

    public function getImageAttribute($value)
    {
        if ($value != "") {
            $value = asset("/api_uploades/order/" . $value);
        } else {
            $value = "";
        }
        return $value;

    }

    public function getIsAlert()
    {
        $date = Carbon::parse($this->received_at);
        $now = Carbon::now();

        $days = $date->diffInDays($now);

        $comments = $this->comments
            ->where('user_type', 3)
            ->where('created_at', '>', $date)->count();

        if (!$comments && $days >= 2) {
            return true;
        } else {
            return false;
        }
    }


    public function getScanColorAttribute()
    {
        $color = '';
        if (isset($this->comments) && count($this->comments) && $this->status != 3) {
            $color = 'background:#FFAF33;color:#000000;';
        } elseif (isset($this->order_delay) && count($this->order_delay)) {
            $last_delay = $this->order_delay()->orderBy('id', 'DESC')->first();
            $now = date("Y-m-d");
            if (($now > $last_delay->delay_at && $this->status != 3) || $now < $last_delay->delay_at) {
                $color = 'background:#FF3333;color:#FFFFFF;';
            } elseif ($now == $last_delay->delay_at || $this->status == 3) {
                $color = 'background:#ACDF25;color: #000000;';
            }
        } elseif ($this->cancel_requests()->count() && $this->status == 7) {
            $color = 'background:#ebd3d3;color:#000000;';
        }

        return $color;
    }

//    public function getIsPickupAttribute()
//    {
//        $order = PickUpOrder::where('order_id', $this->id)->where('status', '!=', 3)->first();
//        if (!$order) {
//            return '0';
//        } else {
//            $pickup = PickUp::where('id', $order->pick_up_id)->where('status', '!=', 3)->first();
//            if (!$pickup) {
//                return '0';
//            } else {
//                return '1';
//            }
//        }
//    }/* check if order is pickup or not */

    public function getIsPickupAttribute()
    {
        $pickup_orders_count = Order::leftJoin('pick_up_orders', 'pick_up_orders.order_id', '=', 'orders.id')
            ->leftJoin('pick_ups', 'pick_up_orders.pick_up_id', '=', 'pick_ups.id')
            ->where('orders.id', $this->id)
            ->where(function ($query) {
                $query->where('pick_up_orders.status', '!=', 3)
                    ->where('pick_ups.status', '!=', 3);
            })
            ->count();

        if ($pickup_orders_count) {
            return 1;
        } else {
            return 0;
        }

    }/* check if order is pickup or not */

    public function getCreatedAtAttribute($value)
    {
        if ($value != null) {
            $value = Carbon::parse($value)->setTimezone('UTC')->addHours(2)->toDayDateTimeString();
        }
        return $value;
    }

    public function getAcceptedAtAttribute($value)
    {
        if ($value != null) {
            $value = Carbon::parse($value)->setTimezone('UTC')->addHours(2)->toDayDateTimeString();
        }
        return $value;
    }

    public function getPaymentStatusSpanAttribute()
    {
        if ($this->payment_status == 1) {
            return "<span class='badge badge-pill label-info'>" . __('backend.collected') . "</span>";
        } elseif ($this->payment_status == 2) {
            return "<span class='badge badge-pill label-success'>" . __('backend.paid') . "</span>";
        }

        return '';
    }
    
    public function getCancelledAtAttribute($value)
    {
        if ($value != null) {
            $value = Carbon::parse($value)->setTimezone('UTC')->addHours(2)->toDayDateTimeString();
        }
        return $value;
    }

    public function getReceivedAtAttribute($value)
    {
        if ($value != null) {
            $value = Carbon::parse($value)->setTimezone('UTC')->addHours(2)->toDayDateTimeString();
        }
        return $value;
    }

    public function getDeliveredAtAttribute($value)
    {
        if ($value != null) {
            $value = Carbon::parse($value)->setTimezone('UTC')->addHours(2)->toDayDateTimeString();
        }
        return $value;
    }

    public function getTotalPriceAtAttribute()
    {
        return $this->delivery_price + $this->order_price;
    }

    public function getCorporateNameAtAttribute()
    {
        return $this->customer->Corporate->name;
    }

    public function getTotalPriceAttribute()
    {
        $delivery_fees = $this->delivery_price;
        $order_price = $this->order_price;
        $payment_method = $this->payment_method_id;
        $total_price = 0;
        if ($payment_method == "1") {
            $total_price = $order_price ? $order_price : $delivery_fees;
        } else if ($payment_method == "2") {
            $total_price = $delivery_fees + $order_price;
        }

        return $total_price;
    }

    public function getTotalPricePolicyAttribute()
    {
        $delivery_fees = $this->delivery_price;
        $order_price = $this->order_price;
        $payment_method = $this->payment_method_id;
        $total_price = 0;
        if ($payment_method == "1") {
            $total_price = $order_price ? $order_price : 0;
        } else if ($payment_method == "2") {
            $total_price = $delivery_fees + $order_price;
        }

        return $total_price;
    }

    public function getTotalPriceDriverAttribute()
    {
        $delivery_fees = $this->delivery_price;
        $order_price = $this->order_price;
        $payment_method = $this->payment_method_id;
        $total_price = 0;
        if ($payment_method == "1") {
            $total_price = $order_price;
        } else if ($payment_method == "2") {
            $total_price = $delivery_fees + $order_price;
        }

        return $total_price;
    }

    public function getIsPaidAttribute()
    {
        return Order::join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
            ->join('invoice_orders', 'invoice_orders.accepted_order_id', '=', 'accepted_orders.id')
            ->join('invoices', 'invoices.id', '=', 'invoice_orders.invoice_id')
            ->where('orders.id', $this->id)
            ->where('invoices.type', 1)
            ->where('invoices.status', 1)
            ->select('invoices.status')
            ->first();
    }

    public static function boot()
    {

        parent::boot();

        Order::updated(function ($model) {
            if ($model->store_id && ($model->qty != $model->getOriginal('qty'))) {
                $stock = Stock::find($model->store_id);
                if ($stock) {
                    $stock->increment('used_qty', ($model->qty - $model->getOriginal('qty')));
                }
            }

            if ($model->store_id && $model->qty && ($model->status != $model->getOriginal('status')) && ($model->status == 4 || $model->status == 5)) {
                $stock = Stock::find($model->store_id);
                if ($stock) {
                    $stock->decrement('used_qty', $model->qty);
                }
            }
        });

        Order::created(function ($model) {
            if ($model->store_id && $model->qty) {
                $stock = Stock::find($model->store_id);
                if ($stock) {
                    $stock->increment('used_qty', $model->qty);
                }
            }
        });

        Order::deleting(function ($model) {
            if ($model->store_id && $model->qty && $model->status != 4 && $model->status != 5) {
                $stock = Stock::find($model->store_id);
                if ($stock) {
                    $stock->decrement('used_qty', $model->qty);
                }
            }
        });
    }

}


@extends('backend.layouts.app')

@section('css')
<title>{{__('backend.cities')}} - {{__('backend.create')}}</title>
@endsection

@section('header')
  <div class="page-header">
    <h3><i class="glyphicon glyphicon-plus"></i> {{__('backend.cities')}} / {{__('backend.create')}} </h3>
  </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <form action="{{ route('mngrAdmin.cities.store') }}" method="POST" name='validateForm'>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
          <div class="form-group col-md-6 col-sm-6 @if($errors->has('city_name')) has-error @endif">
            <label for="city-field">{{__('backend.city_ar')}}: </label>
            <input type="text" id="city-field" name="city_name" class="form-control" value="{{ old("city_name") }}"/>
            @if($errors->has("city_name"))
              <span class="help-block">{{ $errors->first("city_name") }}</span>
            @endif
          </div>
           <div class="form-group col-md-6 col-sm-6 @if($errors->has('name_en')) has-error @endif">
            <label for="city_name_en">{{__('backend.city_en')}}: </label>
            <input type="text" id="city_name_en" name="name_en" class="form-control" value="{{ old("name_en") }}"/>
            @if($errors->has("name_en"))
              <span class="help-block">{{ $errors->first("name_en") }}</span>
            @endif
           </div>
        </div>
        <div class="row">
          <div class="form-group col-md-6 col-sm-6 @if($errors->has('governorate_id')) has-error @endif">
            <label for="governorate_id-field">{{__('backend.government')}}: </label>
            <select id="governorate_id-field" name="governorate_id" class="form-control" >
            @if(! empty($list_governorates) )
              @foreach($list_governorates as $governorate)
                <option value="{{$governorate->id}}">
                  @if(session()->has('lang') == 'ar')
                    {{$governorate->name_ar}}
                  @else
                    {{$governorate->name_en}}
                  @endif
                </option>
              @endforeach
            @endif
            </select>
              @if($errors->has("governorate_id"))
                <span class="help-block">{{ $errors->first("governorate_id") }}</span>
              @endif
          </div>
          <div class="form-group col-md-6 col-sm-6 @if($errors->has('status')) has-error @endif">
            <label for="status-field" style="display: block;">{{__('backend.status')}}: </label>
            <select id="status-field" name="status" class="form-control" value="{{ old("status") }}" >
              <option value="">{{__('backend.chosse_status')}}</option>
              <option value="0" {{ old("status") == "0" ? 'selected' : '' }}>{{__('backend.no')}}</option>
              <option value="1" {{ old("status") == "1" ? 'selected' : '' }}>{{__('backend.yes')}}</option>
            </select>
            @if($errors->has("status"))
              <span class="help-block">{{ $errors->first("status") }}</span>
            @endif
          </div>
        </div>
        <div class="well well-sm col-md-12 col-sm-12 col-xs-12">
          <button type="submit" class="btn btn-primary">{{__('backend.create')}}</button>
          <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.cities.index') }}"><i class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
        </div>
      </form>
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(function() {
    $("form[name='validateForm']").validate({
      // Specify validation rules
      rules: {
        city_name: {
          required: true,
        },
        name_en: {
          required: true,
        },
        governorate_id: {
          required: true,
        },
        status: {
          required: true,
        },
      },
      // Specify validation error messages
      messages: {
        city_name: {
          required: "{{__('backend.Please_Enter_City_Name')}}",
        },
        name_en: {
          required: "{{__('backend.Please_Enter_English_City_Name')}}",
        },
        governorate_id: {
          required: "{{__('backend.Please_Choose_a_Governorate_for_the_City')}}",
        },
        status: {
          required: "{{__('backend.Please_Enter_City_Status')}}",
        },
      },

      errorPlacement: function(error, element) {
          $(element).parents('.form-group').append(error)
      },
      highlight: function (element) {
          $(element).parent().addClass('error')
      },
      unhighlight: function (element) {
          $(element).parent().removeClass('error')
      },
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function(form) {
          form.submit();
      }
    });
  })
</script>
@endsection

@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.agent')}} - {{__('backend.orders')}}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <style>
        .jconfirm.jconfirm-modern .jconfirm-box div.jconfirm-title-c .jconfirm-icon-c {
            -webkit-transition: -webkit-transform .5s;
            transition: -webkit-transform .5s;
            transition: transform .5s;
            transition: transform .5s, -webkit-transform .5s;
            -webkit-transform: scale(0);
            transform: scale(0);
            display: block;
            margin-right: 0;
            margin-left: 0;
            margin-bottom: 10px;
            font-size: 69px;
            color: #9e1717;
        }

        .nav-tabs li:first-child.active a {
            color: #fff;
            background-color: #337ab7;
        }

        .nav-tabs li:last-child.active a {
            color: #fff;
            background-color: #dd4b39;
        }
    </style>
@endsection
@section('header')
    <div class="page-header clearfix">
        <h3>
            <div class="page-header">
                <h3><i class="glyphicon glyphicon-align-justify"></i>
                    {{__('backend.orders')}} {{__('backend.of_agent')}} [ # {{$agent->id}} - {{$agent->name}} ] </h3>
        </h3>
    </div>
    </h3>
    </div>
@endsection

@section('content')

    <div>

        <!-- Nav tabs -->
        <ul id="myTabs" class="nav nav-tabs nav-justified nav-pills" role="tablist">
            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab"
                                                      data-toggle="tab">{{__('backend.current')}}</a></li>
            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab"
                                       data-toggle="tab">{{__('backend.finished')}}</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="home">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                    @if(count($orders) > 0)
                        <!-- <form action="{{URL::asset('/mngrAdmin/pdf/agent')}}" method="get"> -->
                            <form action="{{URL::asset('/mngrAdmin/pdf/3/'. $id)}}" method="get" target="_blank">

                                <table class="table table-condensed table-striped text-center">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{__('backend.received_date')}}</th>
                                        <th>{{__('backend.order_type')}}</th>
                                        <th>{{__('backend.order_number')}}</th>
                                        <th>{{__('backend.order_price')}}</th>
                                        <th>{{__('backend.sender_name')}}</th>
                                        <th>{{__('backend.receiver_name')}}</th>
{{--                                        <th>{{__('backend.receiver_code')}}</th>--}}
                                        <th>{{__('backend.status')}}</th>
{{--                                        <th class="pull-right"></th>--}}
                                        <th>
                                            <input type="button" class="btn btn-info btn-xs" id="toggle"
                                                   value="{{__('backend.select')}}" onClick="do_this()"/>
                                            <button type="submit"
                                                    class="btn btn-warning btn-xs btn-pdf">{{__('backend.pdf')}}</button>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orders as $index => $order)
                                        @if($order->order_status == 1 && $order->accepted_orders_status == 0
                                          ||
                                          $order->order_status == 2 && $order->accepted_orders_status == 1)


                                            <tr class='clickable-row' data-href="{{ route('mngrAdmin.orders.show', $order->order_id) }}">
                                                <td>{{$order->order_id}}</td>
                                                <td>{{$order->received_date}}</td>
                                                <td>{{$order->type}}</td>
                                                <td>{{$order->order_number}}</td>
                                                <td>{{$order->order_price}}</td>
                                                <td>{{$order->sender_name}}</td>
                                                <td>{{$order->receiver_name}}</td>
{{--                                                <td>{{$order->receiver_code}}</td>--}}
                                                <td>
                                                    @if($order->order_status == 1 && $order->accepted_orders_status == 0)
                                                        <span
                                                            class='badge badge-pill badge-info'>{{__('backend.accepted')}}</span>
                                                    @elseif($order->order_status == 2 && $order->accepted_orders_status == 1)
                                                        <span
                                                            class='badge badge-pill badge-warning'>{{__('backend.received')}}</span>
                                                    @endif
                                                </td>
{{--                                                <td class="pull-right">--}}
{{--                                                    <a class="btn btn-xs btn-primary"--}}
{{--                                                       href="{{ route('mngrAdmin.orders.show', $order->order_id) }}"><i--}}
{{--                                                            class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}--}}
{{--                                                    </a>--}}
{{--                                                </td>--}}
                                                <td class="exclude-td">
                                                    <input type="checkbox" name="select[]"
                                                           value="{{$order->order_id}}"/>
                                                </td>
                                            </tr>
                                        @else
                                            @continue
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </form>
                        @else
                            <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
                        @endif
                    </div>
                </div>
            </div> <!-- tab end -->

            <div role="tabpanel" class="tab-pane" id="profile">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        @if(count($orders) > 0)
                            <form action="{{URL::asset('/mngrAdmin/pdf/3/'. $id)}}" method="get" target="_blank">
                                <table class="table table-condensed table-striped text-center">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{__('backend.received_date')}}</th>
                                        <th>{{__('backend.order_type')}}</th>
                                        <th>{{__('backend.order_number')}}</th>
                                        <th>{{__('backend.order_price')}}</th>
                                        <th>{{__('backend.sender_name')}}</th>
                                        <th>{{__('backend.receiver_name')}}</th>
{{--                                        <th>{{__('backend.receiver_code')}}</th>--}}
                                        <th>{{__('backend.status')}}</th>
{{--                                        <th class="pull-right"></th>--}}
                                        <th>
                                            <input type="button" class="btn btn-info btn-xs" id="toggle"
                                                   value="{{__('backend.select')}}" onClick="do_this()"/>
                                            <button type="submit"
                                                    class="btn btn-warning btn-xs btn-pdf">{{__('backend.pdf')}}</button>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orders as $index => $order)
                                        @if($order->order_status == 3 && $order->accepted_orders_status == 2
                                          ||
                                            $order->order_status == 1 && $order->accepted_orders_status == 3
                                          ||
                                            $order->order_status == 2 && $order->accepted_orders_status == 4)
                                            <tr class='clickable-row' data-href="{{ route('mngrAdmin.orders.show', $order->order_id) }}">
                                                <td>{{$index+1}}</td>
                                                <td>{{$order->received_date}}</td>
                                                <td>{{$order->type}}</td>
                                                <td>{{$order->order_number}}</td>
                                                <td>{{$order->order_price}}</td>
                                                <td>{{$order->sender_name}}</td>
                                                <td>{{$order->receiver_name}}</td>
{{--                                                <td>{{$order->receiver_code}}</td>--}}
                                                <td>
                                                    @if($order->order_status == 3 && $order->accepted_orders_status == 2)
                                                        <span
                                                            class='badge badge-pill badge-success'>{{__('backend.delivered')}}</span>
                                                    @elseif($order->order_status == 1 && $order->accepted_orders_status == 3)
                                                        <span
                                                            class='badge badge-pill badge-danger'>{{__('backend.cancelled')}}</span>
                                                    @elseif($order->order_status == 2 && $order->accepted_orders_status == 4)
                                                        <span
                                                            class='badge badge-pill badge-danger'>{{__('backend.recalled')}}</span>
                                                    @endif
                                                </td>
{{--                                                <td class="pull-right">--}}
{{--                                                    <a class="btn btn-xs btn-primary"--}}
{{--                                                       href="{{ route('mngrAdmin.orders.show', $order->order_id) }}"><i--}}
{{--                                                            class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}--}}
{{--                                                    </a>--}}
{{--                                                </td>--}}
                                                <td class="exclude-td">
                                                    <input type="checkbox" name="select[]"
                                                           value="{{$order->order_id}}"/>
                                                </td>
                                            </tr>
                                        @else
                                            @continue
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </form>
                        @else
                            <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
                        @endif
                    </div>
                </div>
            </div> <!-- tab end -->

        </div>

    </div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script>
        $('#myTabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        })
        $(function () {
            $(".ask_view").on('click', function () {
                var id = $(this).data('id');
                $.ajax({
                    url: '{!! url("/mngrAgent/ask_review_order/") !!}' + '/' + id,
                    type: 'get',

                }).done(function (data) {
                    if (data == "ok") {
                        $.alert({
                            title: 'Request Review Sent!',
                            icon: 'fa fa-smile-o',
                            theme: 'modern',
                            content: 'Your request sent to Ufelix Admin successfully!',
                            buttons: {
                                info: {
                                    text: 'OK',
                                    btnClass: 'btn-success',
                                    action: function () {
                                        window.location.href = window.location.href;
                                    }
                                }
                            }
                        });
                    }
                });
            });

        });


        // Select All Orders function For Print
        function do_this() {

            var checkboxes = document.getElementsByName('select[]');
            var button = document.getElementById('toggle');

            if (button.value == 'select') {
                for (var i in checkboxes) {
                    checkboxes[i].checked = 'FALSE';
                }
                button.value = 'deselect'
            } else {
                for (var i in checkboxes) {
                    checkboxes[i].checked = '';
                }
                button.value = 'select';
            }
        }

    </script>
@endsection

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ufelix</title>
    <!--<link rel="stylesheet" href="css/bootstrap.min.css">-->
    <!--<link rel="stylesheet" href="css/main.css">-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        .head {
            border-bottom: 4px solid #343a40;
        }
        .head h4 {
            font-weight: bold;

        }
        .head button {
            border: none;
            box-shadow: none;
            outline: none;
            padding: .3rem 0.75rem !important;
        }


        .logo-ufelix .img-thumbnail {
            border: none;
        }
        .logo-ufelix .line
        {
            margin-top: 5px;
            margin-bottom: .5rem;
            border-top: 2px solid #e7e9ea;
            width: 95%;
        }
        .data
        {
            background-color: #f80;
            height: 40px;
            color: #fff;
            line-height: 40px;
            font-weight: bold;

        }

        #table td, #table th{
            font-size: 13px;
            border-top: 1px solid #ff8800;
        }

        /*bootstrab class*/
        *,
        *::before,
        *::after {
            box-sizing: border-box;
        }

        html {
            font-family: sans-serif;
            line-height: 1.15;
            -webkit-text-size-adjust: 100%;
            -ms-overflow-style: scrollbar;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
        }


        article, aside, figcaption, figure, footer, header, hgroup, main, nav, section {
            display: block;
        }

        body {
            margin: 0;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, Noto Sans, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            text-align: left;
            background-color: #fff;
        }
        .list-inline {
            padding-left: 0;
            list-style: none;
        }
        .row {
            margin-right: -15px;
            margin-left: -15px;
        }
        .row-no-gutters {
            margin-right: 0;
            margin-left: 0;
        }
        .row-no-gutters [class*="col-"] {
            padding-right: 0;
            padding-left: 0;
        }
        .col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col,
        .col-auto, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm,
        .col-sm-auto, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .col-md,
        .col-md-auto, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg,
        .col-lg-auto, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl,
        .col-xl-auto {
            position: relative;
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
        }
        .img-thumbnail {
            padding: 0.25rem;
            background-color: #fff;
            border: 1px solid #dee2e6;
            border-radius: 0.25rem;
            max-width: 100%;
            height: auto;
        }
        .d-inline {
            display: inline !important;
        }
        h1, h2, h3, h4, h5, h6 {
            margin-top: 0;
            margin-bottom: 0.5rem;
        }
        h1, h2, h3, h4, h5, h6,
        .h1, .h2, .h3, .h4, .h5, .h6 {
            margin-bottom: 0.5rem;
            font-family: inherit;
            font-weight: 500;
            line-height: 1.2;
            color: inherit;
        }

        h1, .h1 {
            font-size: 2.5rem;
        }

        h2, .h2 {
            font-size: 2rem;
        }

        h3, .h3 {
            font-size: 1.75rem;
        }

        h4, .h4 {
            font-size: 1.5rem;
        }

        h5, .h5 {
            font-size: 1.25rem;
        }

        h6, .h6 {
            font-size: 1rem;
        }

        .text-right {
            text-align: right !important;
        }

        .text-center {
            text-align: center !important;
        }
        .table {
            border-collapse: collapse !important;
        }
        .table td,
        .table th {
            background-color: #fff !important;
            padding: .4rem 17px;
            font-size: 13px;
        }
        span {
            font-size: 13px;
        }
        .table-bordered thead td, .table-bordered thead th {
            border-bottom-width: 2px;
            width: 1%;
            text-align: center;
        }

        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #e9ecef;
        }
        .ml-md-auto,
        .mx-md-auto {
            margin-left: auto !important;
        }

        .table-bordered th,
        .table-bordered td {
            border: 1px solid #dee2e6;
        }

        .table-bordered thead th,
        .table-bordered thead td {
            border-bottom-width: 2px;
        }
        .d-inline {
            display: inline !important;
        }
        .col-sm-1 {
            -ms-flex: 0 0 8.333333%;
            flex: 0 0 8.333333%;
            max-width: 8.333333%;
        }
        .col-sm-2 {
            -ms-flex: 0 0 16.666667%;
            flex: 0 0 16.666667%;
            max-width: 16.666667%;
        }
        .col-sm-3 {
            -ms-flex: 0 0 25%;
            flex: 0 0 25%;
            max-width: 25%;
        }
        .col-sm-4 {
            -ms-flex: 0 0 33.333333%;
            flex: 0 0 33.333333%;
            max-width: 33.333333%;
        }
        .col-sm-5 {
            -ms-flex: 0 0 41.666667%;
            flex: 0 0 41.666667%;
            max-width: 41.666667%;
        }
        .col-sm-6 {
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }
        .col-sm-7 {
            -ms-flex: 0 0 58.333333%;
            flex: 0 0 58.333333%;
            max-width: 58.333333%;
        }
        .col-sm-8 {
            -ms-flex: 0 0 66.666667%;
            flex: 0 0 66.666667%;
            max-width: 66.666667%;
        }
        .col-sm-9 {
            -ms-flex: 0 0 75%;
            flex: 0 0 75%;
            max-width: 75%;
        }
        .col-sm-10 {
            -ms-flex: 0 0 83.333333%;
            flex: 0 0 83.333333%;
            max-width: 83.333333%;
        }
        .col-sm-11 {
            -ms-flex: 0 0 91.666667%;
            flex: 0 0 91.666667%;
            max-width: 91.666667%;
        }
        .col-sm-12 {
            -ms-flex: 0 0 100%;
            flex: 0 0 100%;
            max-width: 100%;
        }
        .container {
            min-width: 992px !important;
        }
        .container {
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }

        @media (min-width: 576px) {
            .container {
                max-width: 540px;
            }
        }

        @media (min-width: 768px) {
            .container {
                max-width: 720px;
            }
        }

        @media (min-width: 992px) {
            .container {
                max-width: 960px;
            }
        }

        @media (min-width: 1200px) {
            .container {
                max-width: 1140px;
            }
        }

        .row {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -15px;
        }

        .form-control-static {
            min-height: 34px;
            padding-top: 7px;
            padding-bottom: 7px;
            margin-bottom: 0;
        }
        .label-success {
            background-color: #5cb85c;
        }
        .label-danger{background-color:#d9534f;   border: 2px solid #73AD21;
            border-radius: 50px;}
        .img-thumbnail{padding:.25rem;background-color:#f8fafc;border:1px solid #dee2e6;border-radius:.25rem}
    </style>
</head>
<body>
<div class="container">
{{--First Row--}}
    <div style="width: 100%;">
        <div style="width: 10% ;display: inline-block; float: right">
            <img class="img-thumbnail" src="{{public_path().'/website/images/logo.png'}}" alt=""
                 style="height:75px; padding: 5px; margin: 0;">
        </div>
        <div style="width: 57% ; display: inline-block; float: left ; text-align: left; font-weight: bold;">
            <h3 style="color: #d93025">
                {{$store->name}} Warehouse Report
            </h3>
        </div>
    </div>

{{--Second Row--}}
    <div style="width: 100%">
        <div style="width: 20%; float: left">
            <div style="font-weight: bold;"> <label for="name" >{{__('backend.responsible')}}</label></div>
            <p>
                @foreach($store->responsibles as $responsible)
                    {{$responsible->name}}
                @endforeach
            </p>
        </div>

        <div style="width: 30%; float: left">
            <div style="font-weight: bold;"><label for="mobile">{{__('backend.responsible_phone')}}</label></div>
            <p class="form-control-static">{{$store->mobile}}</p>
        </div>
        <div style="width: 20%; float: left;">
            <div style="font-weight: bold;"><label for="governorate_id">{{__('backend.government')}}</label></div>
            <p class="form-control-static">{{ ! empty($store->governorate) ? $store->governorate->name_en : '' }}</p>
        </div>

        <div style="width: 20%; float: left;">
            <div style="font-weight: bold;"><label for="no_of_stocks">{{__('backend.no_of_stocks')}}</label></div>
            <p class="form-control-static">{{$store->stocks_count}}</p>
        </div>
    </div>

{{--Third Row--}}
<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">

        @if($store->stocks->count())
            <div class="col-sm-12"
                 style="border: 2px solid #fdb801;margin-bottom: 20px;margin-top: 20px;background-color: #fdb801;">
                <h4>
                    {{__('backend.stocks')}}
                </h4>
            </div>
            <table class="table table-condensed table-striped">
                <thead>
                <tr>
                    <th class="text-center">{{__('backend.no')}}</th>
                    <th class="text-center">{{__('backend.title')}}</th>
                    <th class="text-center">{{__('backend.store_date')}}</th>
                    <th class="text-center">{{__('backend.corporate')}}</th>
                    <th class="text-center">{{__('backend.dropped_by')}}</th>
                    <th class="text-center">{{__('backend.description')}}</th>
                    <th class="text-center">{{__('backend.actual_piece')}}</th>
                    <th class="text-center">{{__('backend.used_piece')}}</th>
                    <th class="text-center">{{__('backend.piece_status')}}</th>
                    <th></th>

                </tr>
                </thead>
                <tbody>
                @foreach($store->stocks as $stock)
                    <tr>
                        <td class="text-center">{{$loop->iteration}}</td>
                        <td class="text-center">{{$stock->title}}</td>
                        <td class="text-center">{{isset($stock->created_at) ? $stock->created_at : '-'}}</td>
                        <td class="text-center">{{isset($stock->corporate->name) ? $stock->corporate->name : '-'}}</td>
                        <td class="text-center">{{isset($stock->dropped_by) ? $stock->dropped_by : '-'}}</td>
                        <td class="text-center">{{isset($stock->description) ? $stock->description : '-'}}</td>
                        <td class="text-center">{{$stock->qty}}</td>
                        <td class="text-center">{{$stock->used_qty}}</td>
                        <td class="text-center">
                            @if($stock->qty == $stock->used_qty)
                                <span>{{__('backend.used')}}</span>
                            @else
                                <span>{{__('backend.not_used')}}</span>
                            @endif
                        </td>

                    </tr>
                @endforeach

                </tbody>
            </table>
        @endif
    </div>

</div>
</div>
</body>
</html>

@extends('backend.layouts.app')
@section('css')
  <title>{{__('backend.promo_codes')}}</title>
@endsection
@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.promo_codes')}}
            @if(permission('addPromeCode')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
              <a class="btn btn-success pull-right" href="{{ route('mngrAdmin.promo_codes.create') }}"><i class="glyphicon glyphicon-plus"></i> {{__('backend.add_promo_code')}}</a>
            @endif
        </h3>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            @if($promo_codes->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('backend.code')}}</th>
                            <th>{{__('backend.no_tries')}}</th>
                            <th>{{__('backend.discount')}}</th>
                            <th>{{__('backend.status')}}</th>
                            <th class="text-right"></th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($promo_codes as $promo_code)
                            <tr>
                                <td>{{$promo_code->id}}</td>
                                <td>{{$promo_code->code}}</td>
                                <td>{{$promo_code->no_tries}}</td>
                                <td>{{$promo_code->discount}}</td>
                                <td>{!! $promo_code->status_span !!}</td>
                                <td class="text-right">
                                    {{--<a class="btn btn-xs btn-primary" href="{{ route('mngrAdmin.promo_codes.show', $promo_code->id) }}"><i class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>--}}
                                    @if(permission('editPromoCode')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                      <a class="btn btn-xs btn-warning" href="{{ route('mngrAdmin.promo_codes.edit', $promo_code->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                                    @endif

                                    @if(permission('deletePromoCode')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                      <form action="{{ route('mngrAdmin.promo_codes.destroy', $promo_code->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
                                          <input type="hidden" name="_method" value="DELETE">
                                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                          <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}</button>
                                      </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $promo_codes->render() !!}
            @else
                <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
            @endif

        </div>
    </div>

@endsection

<?php

namespace App\Console\Commands;

use App\Models\DeliveryProblem;
use App\Models\OrderComment;
use App\Models\OrderDelay;
use App\Models\OrderDeliveryProblem;
use Illuminate\Console\Command;

class ConvertDelaysToComments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delays:convert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert Delays To Comments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        OrderDelay::chunk(200, function ($delays) {
            foreach ($delays as $delay) {
                $comment = new OrderComment();
                $comment->order_id = $delay->order_id;
                $comment->comment = ($delay->delay_comment ? $delay->delay_comment : 'تأجيل');
                $comment->latitude = 0;
                $comment->longitude = 0;
                $comment->user_type = 1;
                $comment->user_id = ($delay->created_by ? $delay->created_by : 44);
                $comment->is_from_delay = 1;
                $comment->created_at = $delay->created_at;
                $comment->updated_at = $delay->updated_at;
                $comment->save();

            }
        });

    }
}

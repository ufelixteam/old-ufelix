@extends('backend.layouts.app')

@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> OrderLogs
            <a class="btn btn-success pull-right" href="{{ route('mngrAdmin.order_logs.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </h3>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            @if($order_logs->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>DRIVER_ID</th>
                        <th>ORDER_ID</th>
                        <th>STATUS</th>
                            <th class="text-right"></th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($order_logs as $order_log)
                            <tr>
                                <td>{{$order_log->id}}</td>
                                <td>{{$order_log->driver_id}}</td>
                    <td>{{$order_log->order_id}}</td>
                    <td>{{$order_log->status}}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('mngrAdmin.order_logs.show', $order_log->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('mngrAdmin.order_logs.edit', $order_log->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('mngrAdmin.order_logs.destroy', $order_log->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $order_logs->render() !!}
            @else
                <h3 class="text-center alert alert-warning">No Result Found !</h3>
            @endif

        </div>
    </div>

@endsection
<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use App\Http\Requests;
use App\Models\Resource;
use Illuminate\Http\Request;
use Flash;
class ResourceController extends Controller {

	// Get All Resources
	public function index()
	{
		$resources = Resource::orderBy('id', 'desc')->paginate(50);
		return view('backend.resources.index', compact('resources'));
	}

	// Create Resource Page
	public function create()
	{
		return view('backend.resources.create');
	}

	// Added New Resource
	public function store(Request $request)
	{

			$this->validate($request,[
		     'name'   		 			=> 'required',
				 'email'   		 			=> 'required',
				 'started_at'  			=> 'required',
				 'renew_at'    			=> 'required',
				 'cost'    					=> 'required',
				 'website_link'    	=> 'required',
				 'username'    			=> 'required',
				 'password'    			=> 'required',
			]);

			$resource 								= new Resource();
			$resource->name 					= $request->input("name");
      $resource->email 					= $request->input("email");
      $resource->started_at 		= $request->input("started_at");
      $resource->renew_at 			= $request->input("renew_at");
      $resource->cost 					= $request->input("cost");
      $resource->website_link 	= $request->input("website_link");
      $resource->username 			= $request->input("username");
      $resource->password 			= $request->input("password");
			$resource->save();

			// Function Add Action In Activity Log By Samir
			addActivityLog('Add New Resource ' . $resource->name , ' اضافة مصدر جديد ' . $resource->name, 21); // Helper Function in App\Helpers.php

			Flash::success(__('backend.Resource_saved_successfully'));
			return redirect()->route('mngrAdmin.resources.index');
	}

	// View One Resource
	public function show($id)
	{
		$resource = Resource::findOrFail($id);
		return view('backend.resources.show', compact('resource'));
	}

	// Edit Resource
	public function edit($id)
	{
		$resource = Resource::findOrFail($id);
		return view('backend.resources.edit', compact('resource'));
	}

	// Update Resource
	public function update(Request $request, $id)
	{

			$this->validate($request,[
				 'name'   		 			=> 'required',
				 'email'   		 			=> 'required',
				 'started_at'  			=> 'required',
				 'renew_at'    			=> 'required',
				 'cost'    					=> 'required',
				 'website_link'    	=> 'required',
				 'username'    			=> 'required',
				 'password'    			=> 'required',
			]);

			$resource 									= Resource::findOrFail($id);
			$resource->name 						= $request->input("name");
      $resource->email 						= $request->input("email");
      $resource->started_at 			= $request->input("started_at");
      $resource->renew_at 				= $request->input("renew_at");
      $resource->cost 						= $request->input("cost");
      $resource->website_link 		= $request->input("website_link");
      $resource->username 				= $request->input("username");
      $resource->password 				= $request->input("password");
			$resource->save();

			// Function Add Action In Activity Log By Samir
			addActivityLog('Edit Resource ' . $resource->name , ' تعديل المصدر ' . $resource->name, 21); // Helper Function in App\Helpers.php

			Flash::success(__('backend.Resource_updated_successfully'));
			return redirect()->route('mngrAdmin.resources.index');
	}

	// Delete Resource
	public function destroy($id)
	{
		$resource = Resource::findOrFail($id);
		$resource->delete();

		// Function Add Action In Activity Log By Samir
		addActivityLog('Delete Resource ' . $resource->name , ' حذف المصدر ' . $resource->name, 21); // Helper Function in App\Helpers.php

		flash(__('backend.Resource_deleted_successfully'))->error();
		return redirect()->route('mngrAdmin.resources.index');
	}

}

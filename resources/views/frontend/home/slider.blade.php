<!-- Start Slider -->
<section id="slider" class="home-slider-x">
    <div class="home-slider">
        @if(! empty($sliders) && count($sliders) > 0)
            @foreach($sliders as $slider)
                <div class="item">
                    <div class="img" style="background-image: url({{$slider->image}})"></div>
                    <div class="container">
                        <div class="text-box">
                            <h1 class="pre-wrap">{{   $slider->title }}</h1>
                            <h2 class="pre-wrap">{{$slider->small_description }}</h2>
                            <p class="pre-wrap">
                                {{ $slider->description}}
                            </p>
                        </div>
                    </div>
                </div>


            @endforeach
        @endif
    </div>
</section>
<!-- End Slider -->


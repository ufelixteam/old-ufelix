@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.drivers_daily_report')}}</title>
    <style>
        .form-check-inline {
            display: -webkit-inline-box;
            display: -ms-inline-flexbox;
            display: inline-flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            padding-left: 0;
            margin-right: .75rem;
        }

        .form-check-inline input {
            margin: 5px;
        }
    </style>
    <link href="{{asset('assets/select2/select2.min.css')}}" rel="stylesheet">
    <style>
        .select2-container {
            width: 100% !important;
            display: block;
        }
    </style>
@endsection
@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="fa fa-file-excel-o"></i> {{__('backend.drivers_daily_report')}}

        </h3>

    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <form action="{{URL::asset('/mngrAdmin/driver-sheets/daily-report')}}" method="post" id="sort-form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group col-md-2 col-sm-6 col-xs-12">
                    <label for="from_date-field">{{__('backend.From_Date')}}</label>
                    <div class='input-group date ' id="datepicker1">
                        <input type="text" id="from_date-field" name="from_date" class="form-control"/>
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-2 col-sm-6 col-xs-12">
                    <label for="to_date-field">{{__('backend.To_Date')}}</label>
                    <div class='input-group date ' id="datepickers">
                        <input type="text" id="to_date-field" name="to_date" class="form-control"/>
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                    </div>
                </div>

                <div class="form-group col-md-2 col-sm-6 col-xs-12">
                    <label for="driver_id">{{__('backend.driver')}}</label>
                    <select class="form-control select2" id="driver_id" name="driver_id">
                        @if(! empty($drivers))
                            @foreach($drivers as $driver)
                                <option value="{{$driver->id}}">
                                    {{$driver->name}}
                                </option>
                            @endforeach
                        @endif
                    </select>
                </div>

                <div class="form-group col-md-2 col-sm-6 col-xs-12 ">
                    <button id="download-field" type="submit" class="btn btn-primary btn-block"
                            style="margin-top: 24px;"> {{__('backend.download')}} </button>
                </div>
            </form>
        </div>

        <div class=" row col-md-12 text-center" id="table-view">

        </div>
    </div>



@endsection
@section('scripts')
    <script src="{{asset('assets/select2/select2.min.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.select2').select2();
        });
    </script>
@endsection

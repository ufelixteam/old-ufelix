@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.Android_Versions')}} - {{__('backend.edit')}}</title>
@endsection

@section('header')
  <div class="page-header">
    <h3><i class="glyphicon glyphicon-edit"></i>  {{__('backend.Edit_Android_Varsion')}} - {{$versions->version}} </h3>
  </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
      <form action="{{ url('mngrAdmin/android/update') }}" name='validateForm' method="POST">
        <input type="hidden" name="id" value="{{$versions->id}}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group col-md-12 col-sm-12">
          <label for="name-field">{{__('backend.version_name')}}: </label>
          <input type="text" id="name-field" name="version" class="form-control" value="{{$versions->version}}"/>
        </div>
        <div class="form-group col-md-6 col-sm-6">
          <label for="status-field">{{__('backend.platform')}}: </label>
          <select id="status-field" name="platform" class="form-control">
            <option value="0" {{$versions->platform == 0 ? 'selected' : '' }}>{{__('backend.web')}}</option>
            <option value="1" {{$versions->platform == 1 ? 'selected' : '' }}>{{__('backend.Android_Client')}}</option>
            <option value="2" {{$versions->platform == 2 ? 'selected' : '' }}>{{__('backend.Android_Driver')}}</option>
            <option value="3" {{$versions->platform == 3 ? 'selected' : '' }}>{{__('backend.IOS_Client')}}</option>
          </select>
        </div>
        <div class="form-group col-md-6 col-sm-6">
          <label for="status-field">{{__('backend.status')}}: </label>
          <select id="status-field" name="status" class="form-control"  >
            <option value="0" {{$versions->status == 0 ? 'selected' : '' }}>{{__('backend.not_update')}}</option>
            <option value="1" {{$versions->status == 1 ? 'selected' : '' }}>{{__('backend.update')}}</option>
          </select>
        </div>
        <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
          <button type="submit" class="btn btn-primary">{{__('backend.save_edits')}}</button>
          <a class="btn btn-link pull-right" href="{{ url('mngrAdmin/android') }}"><i class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
        </div>
      </form>
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(function() {
    $("form[name='validateForm']").validate({
      // Specify validation rules
      rules: {
        version: {
          required: true,
        },
        platform: {
          required: true,
        },
        status: {
          required: true,
        },
      },
      // Specify validation error messages
      messages: {
        version: {
          required: "{{__('backend.Please_Enter_Name_Android_Version')}}",
        },
        platform: {
          required: "{{__('backend.Please_Chosse_Platform_Version')}}",
        },
        status: {
          required: "{{__('backend.Please_Chosse_Status_Version')}}",
        },
      },

      errorPlacement: function(error, element) {
          $(element).parents('.form-group').append(error)
      },

      highlight: function (element) {
          $(element).parent().addClass('error')
      },

      unhighlight: function (element) {
          $(element).parent().removeClass('error')
      },
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function(form) {
          form.submit();
      }
    });
  })
</script>
@endsection

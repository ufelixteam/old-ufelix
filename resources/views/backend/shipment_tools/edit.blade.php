@extends('backend.layouts.app')
@section('css')
  <title>{{__('backend.Shipment_Tools')}} - {{__('backend.edit')}}</title>
@endsection
@section('header')
  <div class="page-header">
    <h3><i class="glyphicon glyphicon-edit"></i>  {{__('backend.Edit_Shipment_Tool')}} -
      @if(session()->has('lang') == 'ar')
        {{$shipment_tool->name_ar}}
      @else
        {{$shipment_tool->name_en}}
      @endif
    </h3>
  </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
      <form action="{{ route('mngrAdmin.shipment_tools.update', $shipment_tool->id) }}" method="POST" name='validateForm'>
        <input type="hidden" name="_method" value="PUT" >
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
          <div class="form-group col-md-6 col-sm-6 @if($errors->has('name_en')) has-error @endif">
            <label for="title-field">{{__('backend.Title_in_English')}}</label>
            <input type="text" id="name-en-field" name="name_en" class="form-control" value="{{ is_null(old("name_en")) ? $shipment_tool->name_en : old("name_en") }}"/>
            @if($errors->has("name_en"))
              <span class="help-block">{{ $errors->first("name_en") }}</span>
            @endif
          </div>
          <div class="form-group col-md-6 col-sm-6 @if($errors->has('name_ar')) has-error @endif">
            <label for="title-field">{{__('backend.Title_in_Arabic')}}</label>
            <input type="text" id="name-ar-field" name="name_ar" class="form-control" value="{{ is_null(old("name_ar")) ? $shipment_tool->name_ar : old("name_ar") }}"/>
            @if($errors->has("name_ar"))
              <span class="help-block">{{ $errors->first("name_ar") }}</span>
            @endif
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-6 col-sm6 @if($errors->has('item_price')) has-error @endif">
            <label for="item_price-field">{{__('backend.Item_Price')}}</label>
            <input type="number" min="0" id="item_price-field" name="price" class="form-control" value="{{ is_null(old("price")) ? $shipment_tool->price : old("price") }}"/>
            @if($errors->has("price"))
              <span class="help-block">{{ $errors->first("price") }}</span>
            @endif
          </div>
          <div class="form-group col-md-6 col-sm-6 @if($errors->has('status')) has-error @endif">
            <label for="status-field" style="display: block;">{{__('backend.status')}}</label>
            <select id="status-field" name="status" class="form-control" value="{{ old("status") }}" >
              <option value="0" {{ old("status") == "0" || $shipment_tool->status == "0"  ? 'selected' : '' }}>{{__('backend.no')}}</option>
              <option value="1" {{ old("status") == "1" || $shipment_tool->status == "1"  ? 'selected' : '' }}>{{__('backend.yes')}}</option>
            </select>
            @if($errors->has("status"))
              <span class="help-block">{{ $errors->first("status") }}</span>
            @endif
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-12 col-sm-12 @if($errors->has('description')) has-error @endif">
            <label for="description-field">{{__('backend.description')}}</label>
            <textarea class="form-control" id="description-field" rows="4" name="description">{{ is_null(old("description")) ? $shipment_tool->description : old("description") }}</textarea>
            @if($errors->has("description"))
              <span class="help-block">{{ $errors->first("description") }}</span>
            @endif
          </div>
        </div>
        <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
          <button type="submit" class="btn btn-primary">{{__('backend.save_edits')}}</button>
          <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.shipment_tools.index') }}"><i class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
        </div>
      </form>
    </div>
  </div>
@endsection
@section('scripts')
  <script type="text/javascript">
  $(function() {
    $("form[name='validateForm']").validate({
      // Specify validation rules
      rules: {
        name_en: {
          required: true,
        },
        name_ar: {
          required: true,
        },
        price: {
          required: true,
        },
        status: {
          required: true,
        },
      },
      // Specify validation error messages
      messages: {
        name_en: {
          required: "{{__('backend.Please_Enter_the_English_Tool_Title')}}",
        },
        name_ar: {
          required: "{{__('backend.Please_Enter_the_Arabic_Tool_Title')}}",
        },
        price: {
          required: "{{__('backend.Please_Enter_Price_Tool')}}",
        },
        status: {
          required: "{{__('backend.Please_Enter_Status_Tool')}}",
        },
      },

      errorPlacement: function(error, element) {
          $(element).parents('.form-group').append(error)
      },
      highlight: function (element) {
          $(element).parent().addClass('error')
      },
      unhighlight: function (element) {
          $(element).parent().removeClass('error')
      },
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function(form) {
          form.submit();
      }
    });
  })
</script>
@endsection

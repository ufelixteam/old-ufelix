
<?php $__env->startSection('css'); ?>
    <title>About Us</title>

 
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('header'); ?>
    <div class="content-header">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> ABOUT-Us
        </h3>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row">

            <?php if($message = Session::get('message')): ?>
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong><?php echo e($message); ?></strong>
                </div>
            <?php endif; ?>
            <?php if($settings->count()): ?>
                <form action="<?php echo e(URL::asset('/mngrAdmin/setting_update')); ?>" method="POST">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                    <?php $__currentLoopData = $settings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $setting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <?php if($setting->display != "terms"): ?>

                            <?php if($setting->key == "smsprovider"): ?>

                                    <div class="form-group col-md-6 <?php if($errors->has($setting->key)): ?> has-error <?php endif; ?>">
                                        <label for="key-field"><?php echo e(ucwords(str_replace("_", " ", $setting->key))); ?></label>
                                        <select class="form-control"
                                                  name="<?php echo e($setting->key); ?>" class="form-control"
                                                  ><?php echo e($setting->value); ?>

                                                  <option value="victorlink" <?php echo e($setting->value =='victorlink'  ? 'selected' : ''); ?>>VICTORY link SMS</option>
                                                  <option value="smsmsr" <?php echo e($setting->value =='smsmsr'  ? 'selected' : ''); ?>>SMS Misr</option>
                                       </select>
                                        <?php if($errors->has("$setting->key")): ?>
                                            <span class="help-block"><?php echo e($errors->first("$setting->key")); ?></span>
                                        <?php endif; ?>
                                    </div>


                            <?php elseif($setting->options == "-1"): ?>
                            <div class="row col-md-12">
                            <div class="form-group col-sm-6 <?php if($errors->has($setting->key)): ?> has-error <?php endif; ?>">
                                <label for="key-field"><?php echo e(ucwords(str_replace("_", " ", $setting->display))); ?></label>
                                <textarea id="key-field" name="<?php echo e($setting->key); ?>" class="form-control" rows="5"><?php echo e($setting->value); ?></textarea>
                                <?php if($errors->has("$setting->key")): ?>
                                    <span class="help-block"><?php echo e($errors->first("$setting->key")); ?></span>
                                <?php endif; ?>
                            </div>

                            <div class="form-group col-sm-6 <?php if($errors->has($setting->key)): ?> has-error <?php endif; ?>">
                                <label for="key-field"><?php echo e(ucwords(str_replace("_", " ", $setting->display))); ?> AR</label>
                               
                                <textarea id="key-field" name="<?php echo e($setting->key.'_ar'); ?>" class="form-control" rows="5"><?php echo e($setting->value_ar); ?></textarea>

                                <?php if($errors->has("$setting->key")): ?>
                                    <span class="help-block"><?php echo e($errors->first("$setting->key")); ?></span>
                                <?php endif; ?>
                            </div>
                        </div>

                            <?php else: ?>

                            <div class="form-group col-sm-6 <?php if($errors->has($setting->key)): ?> has-error <?php endif; ?>">
                                <label for="key-field"><?php echo e(ucwords(str_replace("_", " ", $setting->display))); ?></label>
                                <input type="text" id="key-field" name="<?php echo e($setting->key); ?>" class="form-control"
                                       value="<?php echo e($setting->value); ?>"/>
                                <?php if($errors->has("$setting->key")): ?>
                                    <span class="help-block"><?php echo e($errors->first("$setting->key")); ?></span>
                                <?php endif; ?>
                            </div>

                             <?php endif; ?>

                        <?php else: ?>


                            <div class="row col-md-12">

                                

                                <div class="form-group col-md-6 <?php if($errors->has($setting->key)): ?> has-error <?php endif; ?>">
                                    <label for="key-field"><?php echo e(ucwords(str_replace("_", " ", $setting->key))); ?></label>
                                    <textarea type="text" class="form-control summernote" rows="10" cols="80"
                                              name="<?php echo e($setting->key); ?>" class="form-control"
                                              value="<?php echo e($setting->value); ?>"><?php echo e($setting->value); ?></textarea>
                                    <?php if($errors->has("$setting->key")): ?>
                                        <span class="help-block"><?php echo e($errors->first("$setting->key")); ?></span>
                                    <?php endif; ?>
                                </div>

                                <div class="form-group col-md-6  <?php if($errors->has($setting->key)): ?> has-error <?php endif; ?>">
                                    <label for="key-field"> <?php echo e(ucwords(str_replace("_", " ", $setting->key_ar))); ?></label>
                                    <textarea type="text" id="editor1" class="form-control summernote"
                                              name="<?php echo e($setting->key."_ar"); ?>" rows="10" cols="80" class="form-control"
                                              value="<?php echo e($setting->value_ar); ?>"><?php echo e($setting->value_ar); ?> </textarea>
                                    <?php if($errors->has("$setting->key")): ?>
                                        <span class="help-block"><?php echo e($errors->first("$setting->key")); ?></span>
                                    <?php endif; ?>
                                </div>

                               
                            </div>
                        <?php endif; ?>


                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                    <div class="well well-sm col-sm-12 col-md-12">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <a class="btn btn-link pull-right" href="<?php echo e(url('mngrAdmin/settings')); ?>"><i
                                class="glyphicon glyphicon-backward"></i> Back</a>
                    </div>
                </form>

            <?php endif; ?>

        </div>

    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection("scripts"); ?>
<!-- include summernote css/js -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
    <script>
        $('.summernote').summernote();
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/settings/index.blade.php ENDPATH**/ ?>
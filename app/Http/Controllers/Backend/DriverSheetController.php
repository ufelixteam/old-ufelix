<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use App\Models\AcceptedOrder;
use App\Models\Driver;
use App\Models\Order;
use Auth;
use Config;
use ExcelReport;
use Flash;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use PdfReport;

//use Elibyy\TCPDF\Facades\TCPDF;
class DriverSheetController extends Controller
{
    private $guardType;

    public function __construct(Request $request)
    {
        if ($request->is('mngrAdmin/*') || $request->is('admin/*')) {
            Config::set('auth.defaults.guard', 'admin');

        } elseif ($request->is('mngrAgent/*') || $request->is('agent/*')) {
            Config::set('auth.defaults.guard', 'agent');
        }
        $this->guardType = Config::get('auth.defaults.guard');
    }

    public function index(Request $request)
    {

        Excel::create('drivers_' . date('Y_m_d'), function ($excel) use ($request) {

            $ownDrivers = [];

            if ($request->filled('driver_id') && is_array($request->driver_id) && count($request->driver_id)) {
                $ownDrivers = Driver::where(function ($query) use ($request) {
                    $query->whereIn('id', $request->driver_id)
                        ->orWhereIn('manager_id', $request->driver_id);
                })->pluck('id')->toArray();
            }

            $type = 1;

            $date = $request->date ? 'orders.' . $request->date : 'orders.created_at';

            $orders = Order::with(['payment_method', 'to_government', 'to_city', 'delivery_problems' => function ($query) {
                $query->orderBy('created_at', 'DESC');
            }, 'order_delay' => function ($query) {
                $query->orderBy('created_at', 'DESC');
            }, 'comments' => function ($query) {
                $query->orderBy('created_at', 'DESC');
            }, 'customer' => function ($query) {
                $query->with('Corporate');
            }, 'oneDriver' => function ($query) {
                $query->with('manager');
            }])
                ->whereHas('drivers', function ($query) use ($request, $ownDrivers) {

                    if ($request->filled('status') && is_array($request->status)) {
                        $statuses = $request->status;
                        foreach ($statuses as $key => $status) {
                            if ($status == 1) {
                                $statuses[$key] = 0;
                            }
                        }

                        $query->whereIn('status', $statuses);
                    }

                    if ($request->filled('driver_id') && is_array($request->driver_id) && count($request->driver_id)) {
                        $query->whereIn('driver_id', $ownDrivers);
                    }

                    $query->where('status', '!=', 4);
                })
                ->select('orders.*')
                ->withCount(['order_delay', 'delivery_problems', 'comments'])
                ->orderBy('orders.created_at');

            if ($request->filled('from_date')) {
                $orders = $orders->whereDate($date, '>=', $request->from_date);
            }

            if ($request->filled('to_date')) {
                $orders = $orders->whereDate($date, '<=', $request->to_date);
            }

            if ($request->filled('status') && is_array($request->status)) {
                $orders = $orders->whereIn('orders.status', $request->status);
            }

            $excel->setTitle('Orders');

            $excel->sheet('Orders', function ($sheet) use ($orders, $type) {
                $sheet->loadView('backend.reports.orders')->with(['orders' => $orders, 'type' => $type]);
            });

        })->download('xls');
    }

    public function show()
    {
        $user = Auth::guard($this->guardType)->user();

//        $corporates = Corporate::select('corporates.*')->orderBy('corporates.id', 'desc');

        $drivers = Driver::select('drivers.*')->orderBy('name', 'ASC');

//        $corporates = Corporate::where('is_active', 1)->select('corporates.*')
//            ->orderBy('corporates.name', 'ASC');

        if ($user->is_warehouse_user) {

            $drivers = $drivers->join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                ->where('user_id', $user->id);

//            $corporates = $corporates->join('customers', 'customers.corporate_id', '=', 'corporates.id')
//                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
//                ->where('user_id', $user->id);

        }

        $drivers = $drivers->get();
//        $corporates = $corporates->get();

        return view('backend.reports.driver_sheets', compact('drivers'));
    }

    public function dailyReportIndex(Request $request)
    {

        $name = '';
        if (isset($request->driver_id)) {
            $name = Driver::where('id', $request->driver_id)->value('name');
        }

        Excel::create(str_replace(' ', '_', $name) . '_' . date('Y_m_d'), function ($excel) use ($request) {

            $lastOrder = AcceptedOrder::selectRaw('status as order_status')
                ->where('driver_id', $request->driver_id)
                ->where('status', '!=', 4)
                ->whereColumn('order_id', 'orders.id')
                ->latest()
                ->limit(1)
                ->getQuery();

            $start = $request->input('from_date');
            $end = $request->input('to_date');

            $orders = Order::with(['customer' => function ($query) {
                $query->with('corporate');
            }])
                ->join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
                ->with(['to_government', 'to_city', 'delivery_problems' => function ($query) {
                    $query->orderBy('created_at', 'DESC');
                }, 'order_delay' => function ($query) {
                    $query->orderBy('created_at', 'DESC');
                }, 'comments' => function ($query) {
                    $query->orderBy('created_at', 'DESC');
                }])
                ->select('orders.*')
                ->selectSub($lastOrder, 'order_status')
                ->when($start, function ($query, $start) {
                    return $query->where('orders.created_at', '>=', $start);
                })->when($end, function ($query, $end) {
                    return $query->where('orders.created_at', '<=', $end);
                })
                ->where('accepted_orders.status', '!=', 4)
                ->where('accepted_orders.driver_id', $request->driver_id)
                ->groupBy('order_id')
                ->orderBy('orders.created_at');

            $excel->setTitle('Orders');

            $excel->sheet('Orders', function ($sheet) use ($orders) {
                $sheet->loadView('backend.reports.captain-orders')->with(['orders' => $orders]);
            });

        })->download('xls');

    }

    public function dailyReport()
    {
        $user = Auth::guard($this->guardType)->user();

//        $corporates = Corporate::select('corporates.*')->orderBy('corporates.id', 'desc');

        $drivers = Driver::select('drivers.*')->orderBy('name', 'ASC');

//        $corporates = Corporate::where('is_active', 1)->select('corporates.*')
//            ->orderBy('corporates.name', 'ASC');

        if ($user->is_warehouse_user) {

            $drivers = $drivers->join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                ->where('user_id', $user->id);

//            $corporates = $corporates->join('customers', 'customers.corporate_id', '=', 'corporates.id')
//                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
//                ->where('user_id', $user->id);

        }

        $drivers = $drivers->get();
//        $corporates = $corporates->get();

        return view('backend.reports.driver_daily', compact('drivers'));
    }

}

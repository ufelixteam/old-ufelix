@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.documentations')}}</title>
@endsection

@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.documentations')}}
            @if(permission('addDocumentation')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
              <a class="btn btn-success pull-right" href="{{ url('mngrAdmin/docs/create') }}" style="margin-right: 10px;"><i class="glyphicon glyphicon-plus"></i> {{__('backend.add_doc')}}</a>
            @endif

            @if(permission('addTypeDocumentation')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
              <a class="btn btn-success pull-right" href="{{ url('mngrAdmin/docs/create-type') }}" style="margin-right: 10px;"><i class="glyphicon glyphicon-plus"></i> {{__('backend.add_doc_type')}}</a>
            @endif
        </h3>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <table class="table table-condensed table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>{{__('backend.type')}}</th>
                        <th>{{__('backend.title')}}</th>
                        <th>{{__('backend.modified')}}</th>
                        <th>{{__('backend.added_by')}}</th>
                        <th>{{__('backend.edited_by')}}</th>
                        <th class="text-right"></th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($list as $value)
                        <tr>
                            <td>{{$value->id}}</td>
                            <td> {{ App\Models\DocsType::findOrFail($value->parent_id)->title }} </td>
                            <td>{{$value->title}}</td>
                            <td>{{ $value->created_at }}</td>
                            <td> {{ $value->created_id == '' ? '' : App\Models\User::findOrFail($value->created_id)->name }} </td>
                            <td> {{ $value->updated_id == '' ? '' : App\Models\User::findOrFail($value->updated_id)->name }} </td>
                            <td class="text-right">
                                <!-- <a class="btn btn-xs btn-primary" href="{{ url('docs/1.0', $value->slug) }}"><i class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a> -->
                                @if(permission('editDocumentation')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                  <a class="btn btn-xs btn-primary" href="{{ url('mngrAdmin/docs/edit', $value->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                                @endif

                                @if(permission('deleteDocumentation')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                  <form action="{{ url('mngrAdmin/docs/destroy', $value->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
                                      <input type="hidden" name="_method" value="DELETE">
                                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                      <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}</button>
                                  </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {!! $list->appends($_GET)->links() !!}

        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('#driversTable').DataTable({
            "bPaginate": false
        });
    </script>
@endsection

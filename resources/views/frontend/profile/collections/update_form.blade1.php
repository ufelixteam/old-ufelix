<form action="{{ url('/customer/account/tmporders_create') }}" method="POST" id="order-form"
      style="margin-bottom: 70px;">

    <input type="hidden" id="collection_id" name="collection_id"
           value="{{ ! empty($collection_id) ? $collection_id : '0' }}">
    <input type="hidden" id="order_id" name="order_id" value="{{ ! empty($order) ? $order->id : '0' }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="row">
        <div class="col-md-6 col-xs-12">
            <div class="ribbon-wrapper card">
                <div class="ribbon ribbon-default">Sender Address</div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;">Government: </label>
                        <div class="col-12 col-md-8">

                            <select class=" select2 form-control s_government_id" id="s_government_id"
                                    name="s_government_id">
                                <option value="Null" data-display="Select">sender Government</option>
                                @if(! empty($governments))
                                @foreach($governments as $government)
                                <option value="{{$government->id}}" {{ ! empty($order) && $order->s_government_id ==
                                    $government->id ? 'selected' : '' }} >
                                    {{$government->name_en}}
                                </option>

                                @endforeach
                                @endif

                            </select>
                        </div>
                    </div>

                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;"> City: </label>
                        <div class="col-12 col-md-8">
                            <select class="select2 form-control s_state_id" id="s_state_id" name="s_state_id">
                                <option value="Null">sender City</option>
                                @if(! empty($s_cities))
                                @foreach($s_cities as $s_city)
                                <option value="{{$s_city->id}}" {{ ! empty($order) && $order->r_state_id == $s_city->id
                                    ? 'selected': '' }} >
                                    {{$s_city->name_en}}
                                </option>

                                @endforeach
                                @endif

                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;"> sender Address: </label>
                        <div class="col-12 col-md-8" style="position: relative;">
                            <i class="mdi mdi-map-marker-radius" data-toggle="modal" data-target="#map2Modal"
                               data-whatever="@fat"
                               style="text-align: center; font-size: 25px; top: 0px; position: absolute; right: 20px;"></i>
                            <input class="form-control" type="text" id="sender_address" name="sender_address"
                                   placeholder="sender Address"
                                   value="{{ ! empty($order) ? $order->sender_address : old(" sender_address") }}">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;"> Location Latitude : </label>
                        <div class="col-12 col-md-8">
                            <input class="form-control" type="text" name="sender_latitude" id="sender_latitude"
                                   placeholder="Latitude"
                                   value="{{ ! empty($order) ? $order->sender_latitude : '30.0668886' }}">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;"> Location Longitude : </label>
                        <div class="col-12 col-md-8">
                            <input class="form-control" type="text" name="sender_longitude" id="sender_longitude"
                                   placeholder="Longitude"
                                   value="{{ ! empty($order) ? $order->sender_longitude : '31.1962743' }}">
                        </div>
                    </div>

                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;">Delivery Price: </label>
                        <div class="col-12 col-md-8">
                            <input class="form-control" type="text" id="delivery_price-field" name="delivery_price"
                                   placeholder="Delivery_price"
                                   value="{{ ! empty($order) ? $order->delivery_price : '0' }}" readonly="readonly">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-xs-12">
            <div class="ribbon-wrapper card">
                <div class="ribbon ribbon-default">Receiver Data</div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;">Name: </label>
                        <div class="col-12 col-md-8">
                            <input id="receiver_name-field" name="receiver_name" class="form-control"
                                   value="{{ ! empty($order) ? $order->receiver_name : old(" receiver_name") }}"
                            type="text" placeholder="Name">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;">Mobile: </label>
                        <div class="col-12 col-md-8">
                            <input class="form-control" type="text" id="receiver_mobile-field" name="receiver_mobile"
                                   placeholder="Mobile" value="{{ ! empty($order) ? $order->receiver_mobile : old("
                                   receiver_mobile") }}" >
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;"> Mobile 2: </label>
                        <div class="col-12 col-md-8">
                            <input class="form-control" type="text" id="receiver_phone-field" name="receiver_phone"
                                   placeholder="Mobile 2" value="{{ ! empty($order) ? $order->receiver_phone : old("
                                   receiver_phone") }}">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;">Government: </label>
                        <div class="col-12 col-md-8">

                            <select class=" select2 form-control r_government_id" id="r_government_id"
                                    name="r_government_id">
                                <option value="Null" data-display="Select">Receiver Government</option>
                                @if(! empty($governments))
                                @foreach($governments as $government)
                                <option value="{{$government->id}}" {{ ! empty($order) && $order->r_government_id ==
                                    $government->id ? 'selected': '' }} >
                                    {{$government->name_en}}
                                </option>

                                @endforeach
                                @endif

                            </select>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;"> City: </label>
                        <div class="col-12 col-md-8">
                            <select class="select2 form-control r_state_id" id="r_state_id" name="r_state_id">
                                <option value="Null">Receiver City</option>
                                @if(! empty($r_cities))
                                @foreach($r_cities as $r_city)
                                <option value="{{$r_city->id}}" {{ ! empty($order) && $order->r_state_id == $r_city->id
                                    ? 'selected': '' }} >
                                    {{$r_city->name_en}}
                                </option>

                                @endforeach
                                @endif

                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;"> Receiver Address: </label>
                        <div class="col-12 col-md-8" style="position: relative;">
                            <i class="mdi mdi-map-marker-radius" data-toggle="modal" data-target="#map1Modal"
                               data-whatever="@fat"
                               style="text-align: center; font-size: 25px; top: 0px; position: absolute; right: 20px;"></i>
                            <input class="form-control" type="text" id="receiver_address" name="receiver_address"
                                   placeholder="Receiver Address"
                                   value="{{ ! empty($order) ? $order->receiver_address : old(" receiver_address") }}">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-4" style="font-size: 14px;margin-top: 5px;"> Location: </label>
                        <div class="col-12 col-md-8">
                            <div class="row">
                                <div class="col-6 col-md-6">
                                    <input class="form-control" type="text" name="receiver_latitude"
                                           id="receiver_latitude" placeholder="Latitude" value="30.0668886">
                                </div>
                                <div class="col-6 col-md-6">
                                    <input class="form-control" type="text" name="receiver_longitude"
                                           id="receiver_longitude" placeholder="Longitude" value="31.1962743">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-12 col-xs-12">
            <div class="ribbon-wrapper card">
                <div class="ribbon ribbon-info">Orders Data</div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-size: 14px;margin-top: 5px;">Order Type: </label>


                            <select class="select2 form-control" id="order_type_id" name="order_type_id">
                                <option value="Null" data-display="Select">Type</option>
                                @if(! empty($types))
                                @foreach($types as $type)
                                <option value="{{$type->id}}" {{ ! empty($order) && $order->order_type_id == $type->id ?
                                    'selected': '' }} >
                                    {{$type->name_en}}
                                </option>

                                @endforeach
                                @endif

                            </select>
                        </div>


                        <div class="col-md-6">
                            <label style="font-size: 14px;margin-top: 5px;">Type Name: </label>

                            <input class="form-control" type="text" name="type" id="type-field" placeholder="Type Name"
                                   value="{{ ! empty($order) ? $order->type : old(" type") }}">

                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-size: 14px;margin-top: 5px;">Stock: </label>

                            <select id="select2 stock_id-field" name="store_id" class="form-control">
                                <option value="Null">Choose Stock</option>
                                @if(! empty($stores))
                                @foreach($stores as $stock)
                                <option value="{{$stock->id}}" {{ $stock->id == old('store_id') ? 'selected' : '' }} >
                                    {{$stock->title}}
                                </option>
                                @endforeach
                                @endif

                            </select>

                        </div>

                        <div class="col-md-6">
                            <label style="font-size: 14px;margin-top: 5px;">Payment Method : </label>

                            <select id="payment_method_id-field" name="payment_method_id" class="form-control select2">
                                <option value="" data-display="Select">Payment Method</option>

                                @if(! empty($payments))
                                @foreach($payments as $payment)
                                <option value="{{$payment->id}}" {{ ! empty($order) && $order->payment_method_id ==
                                    $payment->id ? 'selected': '' }}>
                                    {{$payment->name}}
                                </option>
                                @endforeach
                                @endif

                            </select>

                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label style="font-size: 14px;margin-top: 20px;">Quantity Order : </label>

                                <input class="form-control" @if( ! empty($order->stock) ) max="{{$order->stock->qty}}"
                                @endif id="qty-field" type="number" name="qty" placeholder="Quantity" value="{{ !
                                empty($order) ? $order->qty : old("qty") }}">
                            </div>

                            <div class="col-md-6">
                                <label style="font-size: 14px;margin-top: 20px;">Order price: </label>
                                <input class="form-control" id="order_price-field" type="number" name="order_price"
                                       placeholder="Order price" value="{{ ! empty($order) ? $order->order_price : old("
                                       order_price") }}">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label style="font-size: 14px;margin-top: 5px;"> Notes: </label>

                                <textarea class="form-control" name="notes" rows="1" cols="80">{{ ! empty($order) ? $order->notes : old("notes") }}</textarea>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <button type="button" class="btn btn-warning btn-rounded btn-block" id="order-btn">Edit Order
        </button>

        <button type="button" data-value="{{ ! empty($collection_id) ? $collection_id : '0' }}"
                class="btn btn-rounded btn-default btn-block" id="reset-btn">Reset Form
        </button>

</form>

@include('frontend.profile.collections.js')

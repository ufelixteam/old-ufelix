@extends('agent.layouts.app')
@section('header')
<div class="page-header">
  <h3><i class="fa fa-user"></i>{{ ! empty($driver->name)?$driver->name:''}} - {{ ! empty($driver->mobile)?$driver->mobile:''}}</h3>
  {!! $driver->active_span !!}  {!! $driver->status_span !!}
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12 col-xs-12 col-sm-12">
    <div class="form-group col-sm-6">
      <label for="driving_licence_expired_date">Profile Image </label>
      <br>
      @if($driver->image)
        <a data-toggle="lightbox" href="#demoLightbox" >
          <img style="height: 100px;width:100px;" src="{{$driver->image}}">
        </a>
        <div id="demoLightbox" class="lightbox fade"  tabindex="-1" role="dialog" aria-hidden="true">
          <div class='lightbox-dialog'>
            <div class='lightbox-content'>
              <img src="{{ $driver->image }}">
            </div>
          </div>
        </div>
      @endif
    </div>

    <div class="form-group col-sm-6">
      <label for="email">Email</label>
      <p class="form-control-static">{{ ! empty($driver->email)?$driver->email:''}}</p>
    </div>
    <div class="form-group col-sm-6">
      <label for="phone">PHONE</label>
      <p class="form-control-static">{{  ! empty($driver->phone)?$driver->phone:''}}</p>
    </div>
    <div class="form-group col-sm-6">
      <label for="driving_licence_expired_date">National Id Number</label>
      <p class="form-control-static">{{ ! empty($driver->national_id_number)?$driver->national_id_number:''}}</p>
    </div>
    <div class="form-group col-sm-6">
      <label for="driving_licence_number">National Id Eexpired Date</label>
      <p class="form-control-static">{{! empty($driver->national_id_expired_date)?$driver->national_id_expired_date:''}}</p>
    </div>
    <div class="form-group col-sm-6">
      <label for="driving_licence_number">Driving Licence Number</label>
      <p class="form-control-static">{{! empty($driver->driving_licence_number)?$driver->driving_licence_number:''}}</p>
    </div>
      <div class="form-group col-sm-6">
        <label for="driving_licence_expired_date">Driving Licence Expired Date</label>
        <p class="form-control-static">{{! empty($driver->driving_licence_expired_date)?$driver->driving_licence_expired_date:''}}</p>
      </div>
      <div class="form-group col-sm-6">
        <label for="city_id">CITY</label>
        <p class="form-control-static">{{ ! empty($driver->city) ?  $driver->city->city : ''}}</p>
      </div>
      <div class="form-group col-sm-6">
        <label for="address">Address</label>
        <p class="form-control-static">{{  ! empty($driver->address)?$driver->address:''}}</p>
      </div>
      <div class="form-group col-sm-6">
        <label for="notes">Notes</label>
        <p class="form-control-static">{{ ! empty($driver->notes)?$driver->notes:''}}</p>
      </div>
      @if($driver->latitude && $driver->longitude)
        <div class="col-md-12 col-xs-12 col-sm-12">
          <div style="height:250px;">
            @php
              Mapper::map($driver->latitude, $driver->longitude, ['center' => false, 'marker' => false]);
              $map = Mapper::render() ;
              echo $map;
            @endphp
          </div>
        </div>
      @endif

      @if(! empty($driver->truck))
      <div class="form-group col-sm-12">
        <h3>Truck Information</h3>
        <hr>
        <div class="form-group col-sm-6">
          <label for="notes">MODEL NAME</label>
          <p class="form-control-static">{{ ! empty($driver->truck->model_name)?$driver->truck->model_name:''}}</p>
        </div>
        <div class="form-group col-sm-6">
          <label for="notes">PLATE NUMBER</label>
          <p class="form-control-static">{{ ! empty($driver->truck->plate_number)?$driver->truck->plate_number:''}}</p>
        </div>
        <div class="form-group col-sm-6">
          <label for="notes">CHASSI NUMBER</label>
          <p class="form-control-static">{{ ! empty($driver->truck->chassi_number)?$driver->truck->chassi_number:''}}</p>
        </div>
      </div>
      @endif
      <div class="form-group col-sm-6">
        <label for="driving_licence_expired_date">Driving Licence Image Front</label>
        <br>
        @if($driver->driving_licence_image_front)
          <a data-toggle="lightbox" href="#demoLightbox1" >
            <img style="height: 100px;width:100px;" src="{{ asset($driver->driving_licence_image_front) }}">
          </a>
          <div id="demoLightbox1" class="lightbox fade"  tabindex="-1" role="dialog" aria-hidden="true">
            <div class='lightbox-dialog'>
              <div class='lightbox-content'>
                <img src="{{ asset($driver->driving_licence_image_front) }}">
              </div>
            </div>
          </div>
        @endif
      </div>
      <div class="form-group col-sm-6">
        <label for="driving_licence_expired_date">Driving Licence Image Back</label>
        <br>
        @if($driver->driving_licence_image_back)
          <a data-toggle="lightbox" href="#demoLightbox2" >
            <img style="height: 100px;width:100px;" src="{{ asset( $driver->driving_licence_image_back) }}">
          </a>
          <div id="demoLightbox2" class="lightbox fade"  tabindex="-1" role="dialog" aria-hidden="true">
            <div class='lightbox-dialog'>
              <div class='lightbox-content'>
                <img src="{{ asset( $driver->driving_licence_image_back) }}">
              </div>
            </div>
          </div>
        @endif
      </div>
      <div class="form-group col-sm-6">
        <label for="driving_licence_expired_date">National Id Image Front</label>
        <br>
        @if($driver->national_id_image_front)
        <a data-toggle="lightbox" href="#demoLightbox3" >
          <img style="height: 100px;width:100px;" src="{{ asset( $driver->national_id_image_front) }}">
        </a>
        <div id="demoLightbox3" class="lightbox fade"  tabindex="-1" role="dialog" aria-hidden="true">
          <div class='lightbox-dialog'>
            <div class='lightbox-content'>
              <img src="{{ asset( $driver->national_id_image_front) }}">
            </div>
          </div>
        </div>
        @endif
      </div>
      <div class="form-group col-sm-6">
        <label for="driving_licence_expired_date">National Id Image Back</label>
        <br>
        @if($driver->national_id_image_back)
          <a data-toggle="lightbox" href="#demoLightbox4" >
            <img style="height: 100px;width:100px;" src="{{ asset( $driver->national_id_image_back) }}">
          </a>
          <div id="demoLightbox4" class="lightbox fade"  tabindex="-1" role="dialog" aria-hidden="true">
            <div class='lightbox-dialog'>
              <div class='lightbox-content'>
                <img src="{{ asset( $driver->national_id_image_back) }}">
              </div>
            </div>
          </div>
        @endif
      </div>
      <div class="form-group col-sm-6">
        <label for="driving_licence_expired_date">Criminal Record Image</label>
        <br>
        @if($driver->criminal_record_image_front)
          <a data-toggle="lightbox" href="#demoLightbox5" >
            <img style="height: 100px;width:100px;" src="{{ asset( $driver->criminal_record_image_front) }}">
          </a>
          <div id="demoLightbox5" class="lightbox fade"  tabindex="-1" role="dialog" aria-hidden="true">
            <div class='lightbox-dialog'>
              <div class='lightbox-content'>
                <img src="{{ asset( $driver->criminal_record_image_front) }}">
              </div>
            </div>
          </div>
        @endif
      </div>

      <div class="col-md-12 col-xs-12 col-sm-12">
        <a class="btn btn-link pull-right" href="{{ URL::previous() }}">
          <i class="glyphicon glyphicon-backward"></i>  Back
        </a>
      </div>
  </div>
</div>
@endsection

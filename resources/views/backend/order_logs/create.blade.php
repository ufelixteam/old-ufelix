@extends('backend.layouts.app')
@section('css')
 
@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-plus"></i> OrderLogs / Create </h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <form action="{{ route('mngrAdmin.order_logs.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group col-md-6 col-sm-6 @if($errors->has('driver_id')) has-error @endif">
                       <label for="driver_id-field">Driver_id</label>
                    <input type="text" id="driver_id-field" name="driver_id" class="form-control" value="{{ old("driver_id") }}"/>
                       @if($errors->has("driver_id"))
                        <span class="help-block">{{ $errors->first("driver_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('order_id')) has-error @endif">
                       <label for="order_id-field">Order_id</label>
                    <input type="text" id="order_id-field" name="order_id" class="form-control" value="{{ old("order_id") }}"/>
                       @if($errors->has("order_id"))
                        <span class="help-block">{{ $errors->first("order_id") }}</span>
                       @endif
                    </div>
                     <div class="form-group col-md-6 col-sm-6 @if($errors->has('status')) has-error @endif">
                       <label for="status-field" style="display: block;">Status</label>
                    <div class="btn-group" data-toggle="buttons"><label class="btn btn-success"><input type="radio" value="true" name="status-field" id="status1-field" autocomplete="off"> True</label><label class="btn btn-danger active"><input type="radio" name="status-field" value="false" id="status-field" autocomplete="off"> False</label></div>
                       @if($errors->has("status"))
                        <span class="help-block">{{ $errors->first("status") }}</span>
                       @endif
                    </div>
                <div class="well well-sm col-md-12 col-sm-12 col-xs-12">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.order_logs.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  
@endsection

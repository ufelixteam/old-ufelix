@extends('backend.layouts.app')
@section('css')
  <title>{{__('backend.agents')}} - {{__('backend.view')}}</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">

@endsection
@section('header')
<div class="page-header">
        <h3> {{__('backend.view')}} {{__('backend.agent')}} - {{$agent->name}} </h3> {!! $agent->status_span !!}


        <form action="{{ route('mngrAdmin.agents.destroy', $agent->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">

                @if(permission('agents')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                  <button id="active" type="button" value="{{ $agent->id }}"  class="btn btn-info">{{ $agent->status_txt }}</button>
                @endif

                <a class="btn  btn-primary" data-toggle="modal" data-target="#areaModal"><i class="glyphicon glyphicon-edit"></i> {{ __('backend.Add_Area') }}</a>


                @if(permission('editAgent')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                  <a class="btn btn-warning btn-group" role="group" href="{{ route('mngrAdmin.agents.edit', $agent->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                @endif

                @if(permission('deleteAgent')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                  @if(count($drivers) < 1 && $agent->id != 1 )
                    <button type="submit" class="btn btn-danger"> {{__('backend.delete')}} <i class="glyphicon glyphicon-trash"></i></button>
                  @endif
                @endif


            </div>
        </form>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <div class="form-group col-sm-3">
                 <label for="name">{{__('backend.name')}}</label>
                 <p class="form-control-static">{{$agent->name}}</p>
            </div>
                <div class="form-group col-sm-3">
                 <label for="mobile">{{__('backend.mobile_number')}}</label>
                 <p class="form-control-static">{{isset($agent->mobile) ? $agent->mobile : __('backend.not_found')}}</p>
            </div>
                <div class="form-group col-sm-3">
                 <label for="phone">{{__('backend.mobile_number2')}}</label>
                 <p class="form-control-static">{{isset($agent->phone) ? $agent->phone : __('backend.not_found')}}</p>
            </div>


                <div class="form-group col-sm-3">
                 <label for="fax">{{__('backend.fax')}}</label>
                 <p class="form-control-static">{{$agent->fax}}</p>
            </div>

                <div class="form-group col-sm-3">
                 <label for="commercial_record_number">{{__('backend.commercial_record_number')}}</label>
                 <p class="form-control-static">{{$agent->commercial_record_number}}</p>
            </div>
                <div class="form-group col-sm-3">
                 <label for="deal_date">{{__('backend.deal_date')}}</label>
                 <p class="form-control-static">{{$agent->deal_date}}</p>
            </div>

            <div class="form-group col-sm-3">
             <label for="bonus_of_delivery">{{__('backend.bonus_of_delivery')}}</label>
             <p class="form-control-static">{{$agent->bonus_of_delivery}}</p>
            </div>

            <div class="form-group col-sm-3">
             <label for="pickup_price">{{__('backend.pickup_price')}}</label>
             <p class="form-control-static">{{$agent->pickup_price}}</p>
            </div>

            <div class="form-group col-sm-3">
             <label for="bonus_of_pickup_for_one_order">{{__('backend.bonus_of_pickup_for_one_order')}}</label>
             <p class="form-control-static">{{$agent->bonus_of_pickup_for_one_order}}</p>
            </div>

            <div class="form-group col-sm-3">
             <label for="profit_rate">{{__('backend.profit_rate')}}</label>
             <p class="form-control-static">{{$agent->profit_rate}}</p>
            </div>

            <div class="form-group col-sm-3">
                 <label for="address">{{__('backend.country')}}</label>
                 <p class="form-control-static">
                   @if(\App\Models\Country::where('id', $agent->country_id)->exists())
                     {{\App\Models\Country::where('id', $agent->country_id)->value('name')}}
                   @else
                     {{__('backend.not_found')}}
                   @endif
                 </p>
            </div>
            <div class="form-group col-sm-3">
                 <label for="address">{{__('backend.government')}}</label>
                 <p class="form-control-static">
                   @if(\App\Models\Governorate::where('id', $agent->government_id)->exists())
                     {{\App\Models\Governorate::where('id', $agent->government_id)->value('name_en')}}
                   @else
                     {{__('backend.not_found')}}
                   @endif
                 </p>
            </div>
            <div class="form-group col-sm-3">
                 <label for="address">{{__('backend.city')}}</label>
                 <p class="form-control-static">
                   @if(\App\Models\City::where('id', $agent->city_id)->exists())
                     {{\App\Models\City::where('id', $agent->city_id)->value('name_en')}}
                   @else
                     {{__('backend.not_found')}}
                   @endif
                 </p>
            </div>

            <div class="form-group col-sm-6">
                 <label for="address">{{__('backend.address')}}</label>
                 <p class="form-control-static">{{$agent->address}}</p>
            </div>

            <div class="col-md-12 col-xs-12 col-sm-12">
              <div style="height:250px;margin-bottom: 30px;">
                @php
                  Mapper::map($agent->latitude, $agent->longitude, ['marker' => false])->informationWindow($agent->latitude, $agent->longitude,"Agent Name:  {$agent->name} - Mobile: {$agent->mobile}", ['icon' => [
                     'path' => 'M365.027,44.5c-30-29.667-66.333-44.5-109-44.5s-79,14.833-109,44.5s-45,65.5-45,107.5 c0,25.333,12.833,67.667,38.5,127c25.667,59.334,51.333,113.334,77,162s38.5,72.334,38.5,71c4-7.334,9.5-17.334,16.5-30    s19.333-36.5,37-71.5s33.167-67.166,46.5-96.5c13.334-29.332,25.667-59.667,37-91s17-55,17-71    C410.027,110,395.027,74.167,365.027,44.5z M289.027,184c-9.333,9.333-20.5,14-33.5,14c-13,0-24.167-4.667-33.5-14    s-14-20.5-14-33.5s4.667-24,14-33s20.5-13.5,33.5-13.5c13,0,24.167,4.5,33.5,13.5s14,20,14,33S298.36,174.667,289.027,184z',
                     'fillColor' => '#FFA500',
                     'fillOpacity' => 0.8,
                     'scale' => 0.08,
                     'strokeColor' => '#FF8C00',
                     'strokeWeight' => 1.5
                    ]]);
                  $map = Mapper::render() ;
                  echo $map;
                 @endphp
              </div>
            </div>


<div class="row">
  <div class="col-md-12 col-xs-12 col-sm-12">
    <div class="col-sm-12" style="border: 2px solid #607d8b;color: #fff;margin-bottom: 20px;background-color: #607d8b;" >
      <h4>{{__('backend.Agent_Area')}}</h4>
    </div>
            @if(! empty($areas))
                <table class="table table-condensed table-striped">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>{{__('backend.governorate')}}</th>
                      <th class="text-right"></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($areas as $governorate)
                    <tr>
                        <td>{{$governorate->id}}</td>
                        <td>
                          @if(session()->has('lang') == 'ar')

                            {{ ! empty($governorate->governorate) ? $governorate->governorate->name_ar : ''}}
                          @else
                            {{ ! empty($governorate->governorate) ? $governorate->governorate->name_en : ''}}
                          @endif
                        </td>

                        <td>
                            <a class="btn btn-xs btn-danger text-right delete-area" data-id="{{$governorate->agent_id}}" data-government="{{$governorate->government_id}}"  id="delete-area"><i class="fa fa-trash"></i> {{__('backend.delete')}}</a>

                        </td>
                    </tr>

                    @endforeach
                </tbody>
            </table>

            @endif
            <div class="col-md-12 col-xs-12 col-sm-12">
                <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.agents.index') }}"><i class="glyphicon glyphicon-backward"></i>{{__('backend.back')}}</a>
            </div>
        </div>
    </div>

</div>
</div>
@include('backend.agents.add_area')
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

<script>

	$(function () {
 		$('#active').on('click', function ()
		{
       		var Status = $(this).val();
        	$.ajax({
            	url: "{{ url('/mngrAdmin/agents/active/'.$agent->id) }}",
            	Type:"GET",
            	dataType : 'json',
        		success: function(response){
        			if(response==0){
        				$('#active').html('Active');

        			}else{
        				$('#active').html('UnActive');
        			}
            		location.reload();
   				}
        	});
    	});

    $(".delete-area").on('click',function(){
      var agentid = $(this).attr('data-id');
      var governmentid = $(this).attr('data-government');
      $.confirm({
            title     : '',
            theme     : 'modern' ,
            class     :  'danger',
            content   : '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Confirm Delete</span></div><div style="font-weight:bold;"><p>Are You sure To Delete This Area From Agent Area List ?</p></div>',
            buttons: {
                confirm: {
                    text: 'Delete Area From Agent Area List',
                    btnClass: 'btn-danger',
                    action: function () {
                        $.ajax({
                          url: "{{ url('/mngrAdmin/delete_area_agent/')}}",
                          type: 'DELETE',
                          data: {'_token':"{{csrf_token()}}",'agent_id':agentid,'government_id':governmentid},
                          success: function(data) {

                                if(data != 'false'){

                                    location.reload();
                                }else{
                                  jQuery('.alert-danger').append('<p>Something Error</p>');
                                }
                                window.scrollTo({ top: 0, behavior: 'smooth' });
                          },
                          error: function(data) {
                            console.log('Error:', data);
                          }
                        });
                    }
                },
                cancel: {
                    text: 'Cancel',
                    action: function () {
                    }
                }
            }
      });
    });
	});
</script>
@endsection

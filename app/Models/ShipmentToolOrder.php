<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShipmentToolOrder extends Model
{
    public function getNoItemsAttribute() {
    	$items = ShipmentOrderItem::where('shipment_order_id',$this->id)->count();
    	return $items;
    }

    public function items()	{
        return $this->hasMany('App\Models\ShipmentOrderItem','shipment_order_id');
    }

	  public function getStatusSpanAttribute() {
      if($this->status == 1){
          $value="<span class='badge badge-pill label-warning'>".__('backend.processing')."</span>";
      }else if($this->status == 2){
          $value="<span class='badge badge-pill label-success'>".__('backend.delivered')."</span>";
      }else{
        $value="<span class='badge badge-pill label-black'>".__('backend.pending')."</span>";
      }
      return $value;
    }

   public function ShipmentTool() {
      return $this->belongsToMany('App\Models\ShipmentTool', 'shipment_order_items', 'shipment_order_id', 'tool_id');
    }

	  public function agent() {
        return $this->belongsTo('App\Models\Agent','object_id');
    }
    public function customer() {
        return $this->belongsTo('App\Models\Customer','object_id');
    }
}

<!-- Start Slider -->
<section id="slider" class="home-slider-x">
	<div class="home-slider">
        <?php if(! empty($sliders) && count($sliders) > 0): ?>
            <?php $__currentLoopData = $sliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slider): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            	<div class="item">
                	<div class="img" style="background-image: url(<?php echo e($slider->image); ?>)"></div>
                    <div class="container">
                        <div class="text-box">
                           <h1><?php echo e($slider->title); ?></h1>
                            <h2><?php echo e($slider->small_description); ?></h2>
                            <p>
                                <?php echo e($slider->description); ?>

                            </p>
                        </div>
                    </div>
                </div>

                
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    	<?php endif; ?>
    </div>
</section>
<!-- End Slider -->

<?php /**PATH /home/www/ufelix/ufelix/resources/views/frontend/home/slider.blade.php ENDPATH**/ ?>
@extends('frontend.profile.layouts.app')
@section('title') @lang('backend.refund_collections') @endsection
@section('css')
    <link href="{{asset('profile/bootstrap-datepicker/css/bootstrap-datepicker.css')}}" rel="stylesheet">

    <style>
        .datepicker-dropdown {
            right: inherit !important;
        }

        input[type=checkbox], input[type=radio] {
            width: 18px;
            height: 18px;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="table-responsive m-t-40">
                <h1 class="title">
                    {{__('backend.refund_collections')}}
                </h1>

                <div class="alert alert-danger" hidden>
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong id="errorId"></strong>
                </div>

                <div class="list">
                    <div class="row">
                        <div class="col-md-12" style="margin-bottom: 20px; padding: 0">
                            <form>
                            <div class="group-control col-md-3 col-sm-3">
                                <input type="text" class="form-control" id="search-field" name="search"
                                       value="{{app('request')->input('search')}}"
                                       placeholder="{{__('backend.search')}} ">
                            </div>

                            <div class="group-control col-md-3 col-sm-3">
                                <div class="input-daterange input-group" id="datepicker">
                                    <input type="text" class="date form-control" name="start"
                                           value="{{app('request')->input('start')}}" autocomplete="off"/>
                                    <span class="input-group-addon">{{__('backend.to')}}</span>
                                    <input type="text" class="date form-control" name="end"
                                           value="{{app('request')->input('end')}}" autocomplete="off"/>
                                </div>
                            </div>

                            <div class="group-control col-md-2 col-sm-2">
                                <button type="submit" style="padding: 8px;" class="btn btn-primary btn-block"> {{__('backend.filter')}} </button>
                            </div>
                            </form>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12">
                            @if($collections->count())
                                <table id="theTable" class="table table-condensed table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{__('backend.serial_code')}}</th>
                                        <th>{{__('backend.customer')}}</th>
                                        <th>{{__('backend.count_orders')}}</th>
                                        <th>{{__('backend.Create_at')}}</th>
                                        <th>
                                            <input type="checkbox" class="form-control"
                                                   style="display: inline-block;vertical-align: bottom;" id="printAll">
                                            <a class="btn btn-xs btn btn btn-info" style="display: inline-block"
                                               id="print_refund">
                                                <i class="fa fa-print"></i> {{__('backend.excel')}}

                                            </a>
                                        </th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody class="text-center">

                                    @foreach($collections as $collection)
                                        <tr>
                                            <td>{{$collection->id}}</td>
                                            <td>{{$collection->serial_code}}</td>
                                            <td>{{isset($collection->customer->name) ? $collection->customer->name : '-'}}</td>
                                            <td>{{$collection->orders_count}}</td>
                                            <td>{{$collection->created_at}}</td>
                                            <td class="selectrefund">
                                                <input type="checkbox" class="selectprint" name="selectprint[]"
                                                       value="{{$collection->id}}"/>
                                            </td>
                                            <td>

                                                <a class="btn btn-xs btn-primary "
                                                   href="{{ app()->getLocale() == 'en' ? url('/en/customer/account/orders?refund_collection='.$collection->id) : url('/customer/account/orders?refund_collection='.$collection->id)}}">
                                                    {{__('backend.Orders_List')}}
                                                </a>
                                                <a class="btn btn-xs btn-primary " target="_blank"
                                                   href="{{ app()->getLocale() == 'en' ? url('/en/customer/account/print_refund?refund_collection_id='.$collection->id) : url('/customer/account/print_refund?refund_collection_id='.$collection->id)}}">
                                                    {{__('backend.print')}}
                                                </a>

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! $collections->appends($_GET)->links() !!}
                            @else
                                <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('profile/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script type="text/javascript">
        $('.date').datepicker({
            format: 'yyyy-mm-dd',
        });
        $('body').on('click', '#printAll', function (e) {
            $('input:checkbox.selectprint').not("[disabled]").not(this).prop('checked', this.checked);
        });

        $('body').on('click', '#print_refund', function (e) {

            let ids = [];

            $(".selectrefund input:checked").each(function () {
                ids.push($(this).val());

            });

            if (ids.length > 0) {
                $('<form action="{{ app()->getLocale() == 'en' ? url('/en/customer/account/refund_excel') : url('/customer/account/refund_excel')}}" method="post" target="_blank"><input value="' + ids + ' " name="ids"  />" {{csrf_field()}}"</form>').appendTo('body').submit();

            }

        });
    </script>
@endsection

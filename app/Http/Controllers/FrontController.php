<?php

namespace App\Http\Controllers;

use App\Events\OrderUpdated;
use App\Models\City;
use App\Models\CollectionOrder;
use App\Models\Contact;
use App\Models\Corporate;
use App\Models\Customer;
use App\Models\CustomerPrice;
use App\Models\Driver;
use App\Models\Governorate;
use App\Models\GovernorateKeyword;
use App\Models\GovernoratePrice;
use App\Models\Invoice;
use App\Models\Notification;
use App\Models\Order;
use App\Models\OrderCancelRequest;
use App\Models\OrderComment;
use App\Models\OrderType;
use App\Models\PaymentMethod;
use App\Models\PickUp;
use App\Models\RefundCollection;
use App\Models\Service;
use App\Models\Setting;
use App\Models\Stock;
use App\Models\Store;
use App\Models\TempOrder;
use App\Models\Ticket;
use App\Models\VerifyCode;
use App\Models\Wallet;
use Auth;
use Carbon\Carbon;
use DB;
use File;
use Flash;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class FrontController extends Controller
{
    //

    public function __construct(Request $request)
    {
        $this->middleware('setlocale');
    }

    public static function quickRandom()
    {
        //$pool                     = \Carbon\Carbon::now();
        //return str_pad(mt_rand(1,99999999),6,'0',STR_PAD_LEFT);
        return time();
    }

    public static function quickRandom1()
    {
        //$pool                     = \Carbon\Carbon::now();
        return str_pad(mt_rand(1, 99999999), 10, '0', STR_PAD_LEFT);
    }

    public function dashboard()
    {

//        $numbers = [
//            '5656399999', '5656562705', '5656552860', '5834306812', '5738581174', '5738669995', '5738689177', '5834039465', '5834082874', '5486604978', '5739865760', '5739775491', '5659102584', '5739876587', '5739881666', '5739897953', '5827981501', '5738995826', '5833668995', '5487775427', '5741456568', '5836019995', '5836031853', '5828307017', '5834665344', '5398530463', '5739786347', '5655369173', '5833618187', '5833853979', '5834524980', '5314167543', '5401340495', '5315073910', '5660820447', '5738251477', '5828148011', '5833893013', '5834076799', '5834564871', '5920943387', '5739742306', '5915147689', '5834133128', '5315201699', '5920832232', '5920994203', '5834018419', '5834054438', '5834140241', '5920838956', '5486779072', '5659020435', '5828736982', '5738911337', '5398883342', '5920767065', '5920873696', '5920935674', '5920948889', '5660902945', '5660926339', '5063023022', '5922540383', '5833656347', '5834640016', '5658606920', '5828604210', '5915341233', '5739062102', '5828008215', '5399546605', '5739860916', '5914419797', '5739095053', '5835801922', '5659929396', '5658516857', '5833925651', '5738243285', '5833874238', '5834218298', '5914965517', '5486321565', '5828171298', '5737976567', '5998504800', '5920705940', '5739014463', '5920749952', '5920757137', '5920853476', '5920970498', '5920977818', '5920986035', '5921005341', '5998490625', '5998499360', '6000565275', '5828754173', '5834387564', '5833612869', '5982025663', '6000538217', '5981711113', '5914548847', '5833766097', '5982053247', '5914978817', '5489086316', '5982064128', '5982156994', '5920742884', '5920785329', '5738650976', '5834125091', '5981606783', '5998515988', '5998520749', '6000534675', '6000548799', '5914543834', '5741160278', '5982029351', '5915711209', '5738891202', '5981600516', '5914556244', '5828210985', '5834182688', '5981839254', '5914437035', '5920928907', '5981681777', '6000530395', '8117827132', '3312384636', '1122714194', '8119212127', '2222698122', '1361150124', '2868106002', '2600160405', '4182373563', '2171144762', '3838114127', '6136662721', '1812275561', '0846810218', '9981991900', '4416123520', '2404261227', '8201528264', '4882813911', '8629323661', '2032651385', '2530481715', '7832462283', '4218146855', '3304661807', '1029853062', '2710420440', '8121060828', '2332131565', '1092542160', '0128371273', '7519170362', '1411914380', '4383821235', '4086040500', '4499092977', '2760610314', '3912024806', '4479061136', '3328542040', '0410814450', '1685154225', '9108669944', '5452261080', '1892082632', '5048423256', '8267766419', '5141681160', '6124126844', '2665524615', '2605210135', '6555043571', '3092618649', '3631993762', '8868468148', '6841285226', '1816216727', '9427944922', '0913182575', '1517026235', '1487644864', '8011915762', '4154220618', '5567466455', '6717281484', '6514482212', '2292544115', '1246508044', '2122682141', '4774845001', '3432252831', '6456462509', '6122241261', '2426261010', '3700830712', '9237241478', '1379241252', '9849085841', '2656724211', '1110918809', '1844268623', '9229921497', '3248228026', '7048641158', '1722482761', '8277205625', '7487831817', '1115812180', '6511568166', '2581182615', '6123430507', '3483125311', '9654459151', '4042561611', '3176835684', '1241536294', '9124245121', '6343443894', '9522548154', '3642424626', '8415514720', '4178142426', '4812146281', '5493341458', '2177112141', '4124245788', '7586547817', '7919939120', '1161922451', '9786979978', '0329343400', '1522869214', '2142944918', '0395077978', '1611784949', '6768424090', '4162006601', '5778148471', '1047279258', '2262604242', '4174448256', '9419112699', '1295571942', '5737515309', '6494144147', '7402970477', '1216271741', '9995921074', '0776912997', '0385529915', '5440540555', '2462441362', '0427019972', '1222122444', '5004316221', '7764214910', '9197773782', '2296422955', '1878139761', '6801740246', '7575595592', '9406601150', '4111514646', '4941226646', '7010297120', '1304580138', '1133113131', '1106842885', '7231222226', '2985484418', '5442618664', '2598242064', '9911151909', '5968410191', '8414898829', '6994106221', '9164941406', '9290934115', '3193230834', '1982747447', '9249944597', '2459212261', '4449449646', '9942041641', '3191669191', '1763995496', '5961417014', '4111186449', '8614296264', '5182414815', '7128241426', '1281462402', '7122142987', '6660467760', '7442461969', '6943891803', '7232675900', '0924498729', '6119694663', '5249571440', '9092070019', '1145161493', '1097492394', '0046861906', '9429292991', '3911210117', '1561287612', '6200165977', '3791746993', '1130961991', '8782046119', '9190314097', '9259916965', '6134123323', '2595404054', '1111915923', '6115200990', '1455095644', '8445144324', '0127271622', '4949499237', '6255112188', '9197891207', '7192422112', '3722927529', '1829818255', '9941214244', '8676648218', '0005038667', '6991760794', '6008680124', '1231121524', '6160990111', '3542582328', '4436006884', '1545960637', '8882462686', '6154823411', '6048782814', '3162588922', '2883042844', '4889484921', '6047429001', '0014888888', '1039949196', '5111178213', '6828371262', '5314437617', '6166116466', '8843866222', '1081978167', '0123855884', '1484317514', '1626642115', '6722168816', '2881383640', '8234166369', '1122618988', '6882282966', '1533166322', '9619269408', '1834506455', '2281160190', '1066569919', '1119829161', '9258231268', '4888612884', '5686801118', '1134311131', '1210351498', '3723185308', '1818702071', '1993243932', '8861065805', '7810612686', '5288172164', '3381350111', '0823174042', '8448272876', '0311928123', '0111123285', '4120440656', '3470711766', '0431132288', '4666816180', '2465065662', '1191315518', '6880666586', '8031180361', '3152316355', '4204480545', '5291522248', '6304562625', '6014281041', '6951093957', '3002221717', '2340086811', '1652185010', '2625741712', '4583078777', '8196819601', '4184404454', '1511886507', '2055215176', '5404478513', '5494248519', '1063156821', '4814858133', '4182298643', '4150583488', '4011937220', '7611257410', '1948046633', '7732202339', '1852114616', '0383996457', '5155631811', '1001122481', '7188163368', '8588717118', '9177044931', '6605406111', '1773521192', '0874141197', '5005657228', '1806730188', '4286076102', '0459262257', '1481069648', '5872500085', '5996184733', '0116854014', '3600343041', '0106365993', '1286659338', '0624860416', '9641196306', '7070151101', '5502980287', '0442244083', '2966863858', '4141361162', '1454151542', '2317235933', '1157774971', '0303816833', '4578317924', '1611062092', '5971974281', '4029707651', '1699916703', '1121114421', '9013033829', '2416324824', '2656326851', '2588982821', '7635680864', '2329172676', '9315111212', '3027108423', '2354237977', '1739223139', '3228114892', '2116320037', '1877512885', '7109088029', '1897023189', '6043834117', '4746134147', '2114000402', '4145737287', '3193119338', '1243145948', '5263613823', '8066041166', '6110416023', '9218789636', '6971441414', '3042841313', '4271220246', '8863068837', '3854882303', '1584556662', '7322357262', '0052591333', '9213819463', '0001118199', '8619730930', '5633726862'];
//
//        $dates = [
//            '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/21/2021', '3/10/2021', '3/10/2021', '3/10/2021', '3/10/2021', '3/10/2021', '3/10/2021', '3/10/2021', '3/10/2021', '3/10/2021', '3/10/2021', '3/10/2021', '3/10/2021', '3/10/2021', '3/10/2021', '3/10/2021', '3/10/2021', '3/10/2021', '3/10/2021', '3/10/2021', '3/10/2021', '3/10/2021', '3/10/2021', '3/10/2021', '3/11/2021', '3/11/2021', '3/11/2021', '3/11/2021', '3/11/2021', '3/11/2021', '3/11/2021', '3/11/2021', '3/11/2021', '3/11/2021', '3/11/2021', '3/11/2021', '3/11/2021', '3/11/2021', '3/11/2021', '3/11/2021', '3/11/2021', '3/11/2021', '3/11/2021', '3/11/2021', '3/11/2021', '3/11/2021', '3/11/2021', '3/11/2021', '3/11/2021', '3/11/2021', '3/11/2021', '3/11/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/13/2021', '3/14/2021', '3/14/2021', '3/14/2021', '3/14/2021', '3/14/2021', '3/14/2021', '3/14/2021', '3/14/2021', '3/14/2021', '3/14/2021', '3/14/2021', '3/14/2021', '3/14/2021', '3/14/2021', '3/14/2021', '3/14/2021', '3/14/2021', '3/14/2021', '3/14/2021', '3/14/2021', '3/14/2021', '3/14/2021', '3/14/2021', '3/14/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/15/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/16/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/17/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/18/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/5/2021', '3/6/2021', '3/6/2021', '3/6/2021', '3/6/2021', '3/6/2021', '3/6/2021', '3/6/2021', '3/6/2021', '3/6/2021', '3/6/2021', '3/6/2021', '3/6/2021', '3/6/2021', '3/8/2021', '3/8/2021', '3/8/2021', '3/8/2021', '3/8/2021', '3/8/2021', '3/8/2021', '3/8/2021', '3/8/2021', '3/8/2021', '3/8/2021'
//        ];
//
//        foreach ($numbers as $index => $number) {
//            $order = Order::where('order_number', $number)->where('status', 3)->first();
//            if ($order) {
//                $order->delivered_at = date('Y-m-d H:i:s', strtotime($dates[$index] . ' ' . date('H:i:s', strtotime($order->delivered_at))));
//                $order->save();
//            }
//        }

        $customer = auth()->guard('Customer')->user();

        $wallet = Wallet::where('object_id', $customer->corporate_id)->where('type', 2)->first();

        $dropped_orders = Order::where('orders.status', 7);
        $received_orders = Order::where('orders.status', 2);
        $delivered_orders = Order::where('orders.status', 3);
        $recalled_orders = Order::where('orders.status', 5);
        $cancelled_orders = Order::where('orders.status', 4);
        $refund_orders = Order::where('is_refund', 1);
        $rejected_orders = Order::where('orders.status', 8);

        if ($customer->is_manager) {
            $dropped_orders = $dropped_orders->join('customers', 'customers.id', 'orders.customer_id')
                ->where('corporate_id', $customer->corporate_id);
            $received_orders = $received_orders->join('customers', 'customers.id', 'orders.customer_id')
                ->where('corporate_id', $customer->corporate_id);
            $delivered_orders = $delivered_orders->join('customers', 'customers.id', 'orders.customer_id')
                ->where('corporate_id', $customer->corporate_id);
            $recalled_orders = $recalled_orders->join('customers', 'customers.id', 'orders.customer_id')
                ->where('corporate_id', $customer->corporate_id);
            $cancelled_orders = $cancelled_orders->join('customers', 'customers.id', 'orders.customer_id')
                ->where('corporate_id', $customer->corporate_id);
            $refund_orders = $refund_orders->join('customers', 'customers.id', 'orders.customer_id')
                ->where('corporate_id', $customer->corporate_id);
            $rejected_orders = $rejected_orders->join('customers', 'customers.id', 'orders.customer_id')
                ->where('corporate_id', $customer->corporate_id);
        } else {
            $dropped_orders = $dropped_orders->where('customer_id', $customer->id);
            $received_orders = $received_orders->where('customer_id', $customer->id);
            $delivered_orders = $delivered_orders->where('customer_id', $customer->id);
            $recalled_orders = $recalled_orders->where('customer_id', $customer->id);
            $cancelled_orders = $cancelled_orders->where('customer_id', $customer->id);
            $refund_orders = $refund_orders->where('customer_id', $customer->id);
            $rejected_orders = $rejected_orders->where('customer_id', $customer->id);
        }

        $dropped_orders = $dropped_orders->count();
        $received_orders = $received_orders->count();
        $delivered_orders = $delivered_orders->count();
        $recalled_orders = $recalled_orders->count();
        $cancelled_orders = $cancelled_orders->count();
        $refund_orders = $refund_orders->count();
        $rejected_orders = $rejected_orders->count();

        $months = [];
        for ($i = 0; $i < 12; $i++) {
            $months[] = date("F", strtotime(date('Y-m-01') . " -$i months"));
        }

        $month_dropped = Order::select(\DB::raw('MONTHNAME(orders.created_at) MONTH, COUNT(orders.id) count'))
            ->where('orders.status', 7)
            ->groupBy('MONTH');

        $month_received = Order::select(\DB::raw('MONTHNAME(orders.created_at) MONTH, COUNT(orders.id) count'))
            ->where('orders.status', 2)
            ->groupBy('MONTH');

        $month_delivered = Order::select(\DB::raw('MONTHNAME(orders.created_at) MONTH, COUNT(orders.id) count'))
            ->where('orders.status', 3)
            ->groupBy('MONTH');

        $month_recalled = Order::select(\DB::raw('MONTHNAME(orders.created_at) MONTH, COUNT(orders.id) count'))
            ->where('orders.status', 5)
            ->groupBy('MONTH');

        $month_cancelled = Order::select(\DB::raw('MONTHNAME(orders.created_at) MONTH, COUNT(orders.id) count'))
            ->where('orders.status', 4)
            ->groupBy('MONTH');

        $month_rejected = Order::select(\DB::raw('MONTHNAME(orders.created_at) MONTH, COUNT(orders.id) count'))
            ->where('orders.status', 8)
            ->groupBy('MONTH');

        $month_refund = Order::select(\DB::raw('MONTHNAME(orders.created_at) MONTH, COUNT(orders.id) count'))
            ->where('is_refund', 1)
            ->groupBy('MONTH');

        if ($customer->is_manager) {
            $month_dropped = $month_dropped->join('customers', 'customers.id', 'orders.customer_id')
                ->where('corporate_id', $customer->corporate_id);
            $month_received = $month_received->join('customers', 'customers.id', 'orders.customer_id')
                ->where('corporate_id', $customer->corporate_id);
            $month_delivered = $month_delivered->join('customers', 'customers.id', 'orders.customer_id')
                ->where('corporate_id', $customer->corporate_id);
            $month_recalled = $month_recalled->join('customers', 'customers.id', 'orders.customer_id')
                ->where('corporate_id', $customer->corporate_id);
            $month_cancelled = $month_cancelled->join('customers', 'customers.id', 'orders.customer_id')
                ->where('corporate_id', $customer->corporate_id);
            $month_rejected = $month_rejected->join('customers', 'customers.id', 'orders.customer_id')
                ->where('corporate_id', $customer->corporate_id);
            $month_refund = $month_refund->join('customers', 'customers.id', 'orders.customer_id')
                ->where('corporate_id', $customer->corporate_id);
        } else {
            $month_dropped = $month_dropped->where('customer_id', $customer->id);
            $month_received = $month_received->where('customer_id', $customer->id);
            $month_delivered = $month_delivered->where('customer_id', $customer->id);
            $month_recalled = $month_recalled->where('customer_id', $customer->id);
            $month_cancelled = $month_cancelled->where('customer_id', $customer->id);
            $month_rejected = $month_rejected->where('customer_id', $customer->id);
            $month_refund = $month_refund->where('customer_id', $customer->id);
        }

        $month_dropped = $this->associative_single($month_dropped->get()->toArray(), $months);
        $month_received = $this->associative_single($month_received->get()->toArray(), $months);
        $month_delivered = $this->associative_single($month_delivered->get()->toArray(), $months);
        $month_recalled = $this->associative_single($month_recalled->get()->toArray(), $months);
        $month_cancelled = $this->associative_single($month_cancelled->get()->toArray(), $months);
        $month_rejected = $this->associative_single($month_rejected->get()->toArray(), $months);
        $month_refund = $this->associative_single($month_refund->get()->toArray(), $months);


        return view('frontend.profile.index', compact('dropped_orders', 'received_orders', 'delivered_orders', 'recalled_orders',
            'cancelled_orders', 'month_received', 'month_dropped', 'month_delivered', 'month_recalled', 'month_cancelled', 'months', 'wallet', 'month_refund', 'refund_orders', 'rejected_orders', 'month_rejected'));

    }

    private function associative_single($items = [], $months = [])
    {
        $temp = array();
        foreach ($months as $key => $month) {
            $temp[$month] = 0;
        }

        foreach ($months as $key => $month) {
            foreach ($items as $index => $item) {
                $temp[$item['MONTH']] = $item['count'];
            }
        }

        return array_values($temp);

    }

    public function index()
    {

        $corporates = Corporate::orderBy('id', 'desc')->where('in_slider', 1)->get();
        $allsettings = Setting::get();

        $settings = [];
        foreach ($allsettings as $setting) {

            $settings[$setting->key] = $setting->value;
        }
        $services = Service::orderBy('id', 'desc')->where('is_publish', 1)->where('type', 1)->take(6)->get();
        $sliders = Service::orderBy('id', 'desc')->where('is_publish', 1)->where('type', 3)->take(5)->get();
        $why_us = Service::orderBy('id', 'desc')->where('is_publish', 1)->where('type', 2)->take(3)->get();

        return view('frontend.index', compact('corporates', 'settings', 'services', 'why_us', 'sliders'));

    }

    public function tracking(Request $request)
    {
        $search = app('request')->input('search');
        $search_type = app('request')->input('check');
        $order = null;
        if (!empty($search_type) && $search_type != "") {
            if (!empty($search_type) && $search_type == "1") {
                //sender
                $order = Order::where('order_number', $search)->first();
            } elseif (!empty($search_type) && $search_type == "2") {
                //receiver
                $order = Order::where('receiver_code', $search)->first();
            }
        }


        return view('frontend.tracking', compact('order', 'search', 'search_type'));
    }

    public function terms()
    {
        return view('frontend.terms');
    }

    public function get_register()
    {
        if (Auth::guard('Customer')->user()) {
            return redirect()->route('profile.dashboard', app()->getLocale() == 'en' ? 'en' : 'ar');
        }
        return view('frontend.register');
    }

    public function register(Request $request)
    {
        //recaptcha
        $this->validate($request, [
            'name' => 'required|min:3|max:50',
            'email' => 'email|required|unique:customers',
            'mobile' => 'required|min:11|max:11|unique:customers|regex:/(01)[0-9]{9}/',
            'company_name' => 'required|min:3|max:100',
            'password' => 'required|confirmed|min:6',
            'g-recaptcha-response' => 'required|captcha'
        ]);


        DB::beginTransaction();
        try {
            $corporate = new Corporate();
            $corporate->name = $request->company_name;
            $corporate->status = 0;
            $corporate->is_active = 0;
            $corporate->account_type = 2;

            $corporate->save();

            $customer = new Customer();
            $customer->name = $request->name;
            $customer->email = $request->email;
            $customer->mobile = $request->mobile;
            $customer->password = $request->password;
            $customer->corporate_id = $corporate->id;
            $customer->is_verify_mobile = 0;
            $customer->role_id = 4;
            $customer->save();

            DB::commit();

            $message = '';
            $digits = 4;
            $sms_code = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
            $model = 1;
            //type == 2 for active account
            $type = 1;
            //verify
            $this->new_code($customer->id, $model, $type, $sms_code);
            $message = 'Ufelix Verify Code : ' . $sms_code;

            $this->SendSMSCode(trim($request->mobile), $message);


            Flash::success(__('website.register_done'));
            Flash::success(__('website.send_verify_code'));

            $mobile = $request->mobile;
            //return redirect()->route('active_account', ['lang'=> app()->getLocale() == 'en' ?'':'en', 'mobile' =>$request->mobile ]);
            return view('frontend.activeaccount', compact('mobile'));

        } catch (Exception $e) {

            DB::rollback();

            flash(__('website.SomethingError'))->error();

            return redirect()->route('register', app()->getLocale() == 'en' ? 'en' : 'ar');
        }

    }

    function new_code($user_id, $user_model, $type, $code)
    {

        //user_model = 1 for customer --- 2 for captain

        VerifyCode::where('user_id', $user_id)->where('type', $type)->where('user_model', $user_model)->update(['is_used' => 1]);
        $verify_code = new VerifyCode();
        $verify_code->code = $code;
        $verify_code->user_model = $user_model;
        $verify_code->is_used = 0;
        $verify_code->user_id = $user_id;
        $verify_code->type = $type;
        $verify_code->invalid_date = Carbon::now()->addMinutes(30);
        return $verify_code->save();
    }

    function SendSMSCode($mobile, $message)
    {
        $provider = 'smsmsr';
        $setting = Setting::where('key', 'smsprovider')->first();
        if ($setting) {
            $provider = $setting->value;
        }
        if ($provider == 'smsmsr') {
            $username = "jIimB0A7";
            $password = "LJ9NCdC4xH";
            $client = new Client();
            $res = $client->request('POST', "https://smsmisr.com/api/webapi?username=" . $username . "&password=" . $password . "&language=1&sender=ufelix&mobile=2" . $mobile . "&message=" . $message, [
                'form_params' => []
            ]);
            return $res->getStatusCode();
            //dd( $res->getBody()->getContents()); die();

        } elseif ($provider == 'unifonic') {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, "http://api.unifonic.com/rest/Messages/Send");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);

            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "AppSid=Fit0ax1rTrBppUQzhaUOPJFUYl4fF&Recipient=002" . $mobile . "&Body=" . $message);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Content-Type: application/x-www-form-urlencoded"
            ));

            $resp = curl_exec($ch);
            curl_close($ch);
        } else if ($provider == "victorlink") {

            $url = 'https://smsvas.vlserv.com/KannelSending/service.asmx/SendSMSWithDLRA?username=ufelix&password=e99m991S9s&SMSLang=E&SMSSender=ufelix&SMSReceiver=002' . $mobile . "&SMSText=" . $message;
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            //curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            # Return response instead of printing.
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            # Send request.
            $result = curl_exec($ch);
            curl_close($ch);
            return 'true';

        }

    }

    public function get_changepassword()
    {
        return view('frontend.profile.changepassword');
    }

    public function get_active_account($mobile)
    {
        return view('frontend.activeaccount', compact('mobile'));
    }

    public function active_account(Request $request)
    {

        $mobile = $request->mobile;
        $validator = \Validator::make($request->all(), [

            'mobile' => 'required|numeric',
            'code' => 'required|numeric'
        ]);
        if ($validator->fails()) {

            return view('frontend.activeaccount', compact('mobile'))->withErrors($validator);
        }

        $customer = Customer::where('mobile', $request->mobile)->where('role_id', 4)->first();

        if (!$customer) {
            flash(__('website.login_not_exist_mobile'))->error();
            return view('frontend.activeaccount', compact('mobile'))->withErrors($validator);
        } else {

            $verify = VerifyCode::where('user_id', $customer->id)->where('code', $request->code)->where('type', 1)->where('user_model', 1)
                ->where('is_used', 0)->whereDate('invalid_date', '>=', Carbon::now())->first();

            if (!$verify) {
                flash(__('website.wrong_code'))->error();
                return view('frontend.activeaccount', compact('mobile'));
            }

            $customer->is_verify_mobile = 1;
            $customer->save();
            //send code to mobile

            VerifyCode::where('user_id', $customer->id)->where('code', $request->code)->where('type', 1)->where('user_model', 1)
                ->where('is_used', 0)->whereDate('invalid_date', '>=', Carbon::now())->update(['is_used' => 1]);
            Flash::success(__('website.done_verify_code'));

            return redirect()->route('login', app()->getLocale() == 'en' ? 'en' : 'ar');
        }
    }

    public function send_active_code(Request $request)
    {

        $request->validate([
            'mobile' => 'required|numeric'
        ]);

        $customer = Customer::where('mobile', $request->mobile)->where('role_id', 4)->first();

        if (!$customer) {
            flash(__('website.login_not_exist_mobile'))->error();
            return redirect()->back();
        } else {

            //send code to mobile
            $mobile = $request->mobile;
            $message = '';
            $digits = 4;
            $sms_code = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
            $model = 1;
            //type == 2 for active account
            $type = 1;
            //verify
            $this->new_code($customer->id, $model, $type, $sms_code);
            $message = 'Ufelix Verify Code : ' . $sms_code;

            $this->SendSMSCode(trim($request->mobile), $message);

            Flash::success(__('website.send_verify_code'));

            return view('frontend.verifyaccount', compact('mobile'));
        }
    }

    public function changepassword(Request $request)
    {
        $this->validate($request, [
            'oldpassword' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);
        $customer = auth()->guard('Customer')->user();

        $checkPassword = password_verify($request->oldpassword, $customer->password);

        if ($checkPassword == false) {
            flash(__('website.login_wrong_password'))->error();
            return redirect()->back();

        } else {
            $customer->password = bcrypt($request->password);
            $customer->save();

        }

        Flash::success(__('website.reset_password_done'));
        return redirect()->back();
    }

    public function get_login()
    {
        if (!empty(Auth::guard('Customer')->user())) {
            return redirect()->route('profile.dashboard', app()->getLocale() == 'en' ? 'en' : 'ar');
        }
        return view('frontend.login');
    }

    public function get_forgotpassword()
    {
        if (!empty(Auth::guard('Customer')->user())) {
            return redirect()->route('profile.dashboard', app()->getLocale() == 'en' ? 'en' : 'ar');
        }
        return view('frontend.forgotpassword');
    }/* login function**/

    public function forgotpassword(Request $request)
    {


        $request->validate([
            'mobile' => 'required|string|max:255|min:5'
        ]);

        $customer = Customer::where('mobile', $request->mobile)->where('role_id', 4)->first();

        if (!$customer) {
            flash(__('website.login_not_exist_mobile'))->error();
            return redirect()->back();
        } else {

            //send code to mobile

            $message = '';
            $digits = 4;
            $sms_code = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
            $model = 1;
            //type == 2 for forgot password
            $type = 2;
            //verify
            $this->new_code($customer->id, $model, $type, $sms_code);
            $message = 'Ufelix Verify Code : ' . $sms_code;

            $this->SendSMSCode(trim($request->mobile), $message);

            Flash::success(__('website.send_forgot_code'));
            $mobile = $request->mobile;
            return view('frontend.forgotcode', compact('mobile'));
        }
    }

    public function forgotpasswordcode(Request $request)
    {
        $mobile = $request->mobile;
        $code = $request->code;

        $validator = \Validator::make($request->all(), [

            'mobile' => 'required|string|max:255|min:5',
            'code' => 'required|numeric'
        ]);
        if ($validator->fails()) {

            return view('frontend.forgotcode', compact('mobile', 'code'))->withErrors($validator);
        }

        $customer = Customer::where('mobile', $request->mobile)->where('role_id', 4)->first();

        if (!$customer) {
            flash(__('website.login_not_exist_mobile'))->error();
            return view('frontend.forgotcode', compact('mobile', 'code'));
        } else {
            $verify = VerifyCode::where('user_id', $customer->id)->where('code', $request->code)->where('type', 2)->where('user_model', 1)
                ->where('is_used', 0)->whereDate('invalid_date', '>=', Carbon::now())->first();

            if (!$verify) {
                flash(__('website.wrong_code'))->error();
                return view('frontend.forgotcode', compact('mobile', 'code'));
            }
            //return to view page

            return view('frontend.resetpassword', compact('mobile', 'code'));
        }
    }

    public function resetpassword(Request $request)
    {

        $mobile = $request->mobile;
        $code = $request->code;

        $validator = \Validator::make($request->all(), [

            'mobile' => 'required|string|max:255',
            'code' => 'required|numeric',
            'password' => 'required|numeric|confirmed|min:4'
        ]);
        if ($validator->fails()) {

            return view('frontend.resetpassword', compact('mobile', 'code'))->withErrors($validator);
        }
        $customer = Customer::where('mobile', $request->mobile)->where('role_id', 4)->first();

        if (!$customer) {
            flash(__('website.login_not_exist_mobile'))->error();
            return view('frontend.resetpassword', compact('mobile', 'code'))->withErrors($validator);

        } else {
            $verify = VerifyCode::where('user_id', $customer->id)->where('code', $request->code)->where('type', 2)->where('user_model', 1)
                ->where('is_used', 0)->whereDate('invalid_date', '>=', Carbon::now())->first();

            if (!$verify) {
                flash(__('website.wrong_code'))->error();
                return view('frontend.resetpassword', compact('mobile', 'code'))->withErrors($validator);
            }
            $customer->password = $request->password;
            $customer->save();

            VerifyCode::where('user_id', $customer->id)->where('code', $request->code)->where('type', 2)->where('user_model', 1)
                ->where('is_used', 0)->whereDate('invalid_date', '>=', Carbon::now())->update(['is_used' => 1]);

            Flash::success(__('website.reset_password_done'));

            //return to view page
            return redirect()->route('login', app()->getLocale() == 'en' ? 'en' : 'ar');
        }
    }

    public function login(Request $request)
    {

        $request->validate([
            'password' => 'required|string',
            'mobile' => 'required|string|max:11|min:11'
        ]);

        $customer = Customer::where('mobile', $request->mobile)->where('role_id', 4)->first();

        if (!$customer) {
            flash(__('website.login_not_exist_mobile'))->error();
            return redirect()->back();
        } else {
            if ($customer->is_verify_mobile != 1) {
                flash(__('website.user_not_active'));

                return redirect()->back()->withInput($request->input());
            } elseif ($customer->is_block == 1) {
                flash(__('website.blocked'))->error();

                return redirect()->back()->withInput($request->input());
            } else {
                if (Auth::guard('Customer')->attempt(['mobile' => $request->mobile, 'password' => $request->password, 'role_id' => 4], true)) {
                    $customer = $customer;

                    if ($customer->is_verify_mobile != 1) {

                        Auth::guard('Customer')->logout();
                        flash(__('website.user_not_active'))->error();
                        return redirect()->back()->withInput($request->input());

                    } elseif ($customer->is_block == 1) {

                        Auth::guard('Customer')->logout();
                        flash(__('website.blocked'))->error();
                        return redirect()->back()->withInput($request->input());
                    }


                    // then check if this user belong to corporate before
                    $corporate = Corporate::where('id', $customer->corporate_id)->first();
                    if (!$corporate) {
                        # company dos not exist
                        Auth::guard('Customer')->logout();
                        flash(__('website.company_does_not_exist'))->error();
                        return redirect()->back()->withInput($request->input());

                    } else if ($corporate && $corporate->is_active != 1) {
                        Auth::guard('Customer')->logout();
                        flash(__('website.corporate_not_active'))->error();
                        return redirect()->back()->withInput($request->input());
                    }

                    \Config::set('auth.defines.guard', 'Customer');
                    Flash::success(__('website.login_done'));
                    return redirect()->route('profile.dashboard', app()->getLocale() == 'en' ? 'en' : 'ar');

                    // return redirect('/mngrAdmin/dashboard');

                } else {

                    \Config::set('auth.defines.guard', 'Customer');
                    flash(__('website.login_wrong_password'))->error();
                    return redirect()->back()->withInput($request->input());
                }
            }

        }

    }

    public function logout()
    {
        auth()->guard('Customer')->logout();
        return redirect()->route('login', app()->getLocale() == 'en' ? 'en' : 'ar');
    }

    public function about()
    {
        return view('frontend.about');
    }

    public function services()
    {
        $services = Service::orderBy('id', 'desc')->where('is_publish', 1)->where('type', 2)->paginate(15);

        return view('frontend.services', compact('services'));
    }

    public function captain()
    {
        $services = Service::orderBy('id', 'desc')->where('is_publish', 1)->where('type', 4)->paginate(15);

        return view('frontend.captain', compact('services'));
    }


    //Send sms message to smsmisr, unifonic provider

    public function customer()
    {
        $services = Service::orderBy('id', 'desc')->where('is_publish', 1)->where('type', 5)->paginate(15);

        return view('frontend.customer', compact('services'));
    }

    public function contact()
    {
        return view('frontend.contact');
    }

    public function contact_post(Request $request)
    {

        $request->validate([
            'name' => 'required|string|max:255',
            'mobile' => 'required|string|max:255',
            'message' => 'required|string',
            'g-recaptcha-response' => 'required|captcha'
        ]);
        $contact = new Contact();
        $contact->name = $request->input("name");
        $contact->email = $request->input("email");
        $contact->mobile = $request->input("mobile");
        $contact->company = $request->input("company");
        $contact->message = $request->input("message");
        $contact->type = 1;

        $contact->save();
        Flash::success(__('website.ContactMessageSent'));
        return redirect()->back();

    }

    // Create Temp Order

    public function order_form_list()
    {
        $customer = Auth::guard('Customer')->user();
        if ($customer) {
            $collections = CollectionOrder::with('customer')
                ->where(function ($query) {
                    $query->where('type', 1)
                        ->orWhere(function ($query) {
                            $query->where(function ($query) {
                                $query->whereNull('collection_orders.type')
                                    ->orWhere('collection_orders.type', '!=', 1);
                            })
                                ->where('is_saved', 1);
                        });
                });

            if ($customer->is_manager) {
                $collections = $collections->join('customers', 'customers.id', 'collection_orders.customer_id')
                    ->where('corporate_id', $customer->corporate_id);
            } else {
                $collections = $collections->where('customer_id', $customer->id);
            }

            $collections = $collections->select('collection_orders.*')->orderBy('collection_orders.created_at', 'DESC')
                ->paginate(15);

            return view('frontend.profile.collections.index', compact('collections', 'customer'));
        }
    }

    public function order_form(Request $request)
    {
        $collection_id = app('request')->input('collection');
        $orders = TempOrder::where('collection_id', $collection_id)->paginate(50);
        $allStores = Store::orderBy('id', 'ASC')->get();
        $governments = Governorate::orderBy('name_en', 'ASC')->get();
        $types = OrderType::orderBy('name_en', 'ASC')->get();
        $payments = PaymentMethod::where('status', 1)->orderBy('name', 'ASC')->get();
        $collection = null;
        $stores = [];
        $customer = Auth::guard('Customer')->user();


        if (!empty($customer)) {
            $stores = Stock::whereRaw('qty - used_qty > 0')->where('corporate_id', $customer->corporate_id)->get();
            $collection = CollectionOrder::where('id', $collection_id)->first();
            if (!empty($collection) && $collection != null) {
                $collection->corporate_id = Corporate::where('id', $customer->corporate_id)->value('id');
            }
            if ($collection && $collection->is_saved == 1) {
                flash(__('backend.Collection_that_you_try_to_open_saved_before'))->error();
                return redirect('/form_collections');
            }
        }

        if ($request->ajax()) {
            return [
                'view' => view('frontend.profile.collections.create_form', compact('governments', 'types', 'collection_id', 'orders', 'collection', 'customer', 'stores', 'payments', 'allStores'))->render(),
            ];
        }
        return view('frontend.profile.collection', compact('governments', 'types', 'collection_id', 'orders', 'collection', 'customer', 'stores', 'payments', 'allStores'));
    }/*save_collection save tmp orders to orders*/

    public function tmporders_create(Request $request)
    {
        $first_one = "0";
        $customer = Auth::guard('Customer')->user();
        if (!$customer) {
            //return to login
            return response()->json(['errors' => [__('backend.please_try_to_login')]]);
        }

        if ($request->ajax()) {
            $validator = \Validator::make($request->all(), [
                'receiver_name' => 'required',
                'receiver_mobile' => 'required',
                'payment_method_id' => 'required',
                'r_government_id' => 'required',
                'r_state_id' => 'required',
//                'sender_mobile' => 'required',
//                's_government_id' => 'required',
//                's_state_id' => 'required',
            ]);

        } else {

            $this->validate($request, [
                'receiver_name' => 'required',
                'receiver_mobile' => 'required',
                'payment_method_id' => 'required',
                'r_government_id' => 'required',
                'r_state_id' => 'required',
//                'sender_mobile' => 'required',
//                's_government_id' => 'required',
//                's_state_id' => 'required',
            ]);
        }

        if ($request->ajax()) {
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->all()]);
            }
        }

        if ($request->input("store_id") && $request->input("qty") <= 0) {
            if ($request->ajax()) {
                return response()->json(['errors' => [__('backend.stock_quantity')]]);
            } else {
                Flash::error(__('backend.stock_quantity'));
                return redirect()->back();
            }
        }

        DB::beginTransaction();
        try {
            $collection = CollectionOrder::where('id', $request->collection_id)->first();
            if (!$collection) {
                $collection = new CollectionOrder();
                $collection->customer_id = $customer->id;
                $collection->count_no = 0;
                $collection->is_saved = 0;
                $collection->complete_later = 0;
                $collection->uploaded_target = 4;
                $collection->type = 1;
                $collection->save();
                $first_one = "1";
            } else {
                $collection->save();
            }
            $collection_id = $collection->id;
            if (!empty($request->order_id) && $request->order_id > 0) {
                $order = TempOrder::where('id', $request->order_id)->first();
                if (!$order) {
                    $order = new TempOrder();
                }
            } else {
                $order = new TempOrder();
            }
            $order->reference_number = $request->input("reference_number");
            $order->delivery_type = $request->input("delivery_type");
            $order->sender_subphone = $request->input("sender_subphone");
            $order->receiver_name = $request->input("receiver_name");
            $order->receiver_mobile = $request->input("receiver_mobile");
            $order->receiver_phone = $request->input("receiver_phone");
            $order->receiver_latitude = $request->input("receiver_latitude");
            $order->receiver_longitude = $request->input("receiver_longitude");
            $order->receiver_address = $request->input("receiver_address");
            $order->collection_id = $collection->id;
            $order->sender_name = $customer->name;
            $order->sender_mobile = $customer->mobile;
            $order->sender_phone = $customer->phone;
            $order->sender_latitude = $customer->latitude;
            $order->sender_longitude = $customer->longitude;
            $order->sender_address = $customer->address;
            $order->r_state_id = $request->input("r_state_id");
            $order->s_state_id = $customer->city_id;
            $order->r_government_id = $request->input("r_government_id");
            $order->s_government_id = $customer->government_id;
            $order->status = 0;
            $order->type = $request->input("type");
            $order->order_type_id = $request->input("order_type_id");
            $order->agent_id = null;
            $order->customer_id = $collection->customer_id;
            $order->payment_method_id = $request->input("payment_method_id");
            $order->order_price = $request->input("order_price");
            //calculate from to
            $order->delivery_price = $request->input("delivery_price");
            $order->overload = $request->input("overload");
            $order->notes = $request->input("notes");
            $order->store_id = $request->input("store_id");
            $order->qty = $request->input("qty");
            $order->order_no = $this->randomNumber(1);//substr(uniqid(sha1(time())), 0, 8);
            $order->order_number = $this->randomNumber(2);//$this->quickRandom();
            $order->receiver_code = $this->randomNumber(3);//$this->quickRandom1();
            $order->forward_driver_id = null;

            if ($request->input("main_store") > 0) {
                $order->main_store = $request->input("main_store");
            }

            $order->save();
            DB::commit();
            //Flash::success('Order saved successfully.');
            if ($request->ajax()) {
                if ($validator->fails()) {
                    return response()->json(['errors' => $validator->errors()->all()]);
                }
                $orders = TempOrder::where('collection_id', $collection_id)->paginate(50);
                $orders->withPath('/' . app()->getLocale() . '/customer/account/order-form?collection=' . $collection_id);
                $governments = Governorate::orderBy('name_en', 'ASC')->get();
                #payment_methods
                $payments = PaymentMethod::where('status', 1)->orderBy('name', 'ASC')->get();
                $types = OrderType::orderBy('name_en', 'ASC')->get();
                if ($customer) {
                    $stores = Stock::whereRaw('qty - used_qty > 0')->where('corporate_id', $customer->corporate_id)->get();
                    $collection->corporate_id = $customer->corporate_id;
                }

                return [
                    'collection_id' => $collection_id,
                    'first_one' => $first_one,
                    'view' => view('frontend.profile.collections.tmptable', compact('orders', 'collection_id', 'collection'))->render(),
                    'view_order' => view('frontend.profile.collections.create_form', compact('governments', 'types', 'collection_id', 'orders', 'collection', 'customer', 'stores', 'payments'))->render(),
                    'js' => view('frontend.profile.collections.js', array('from_update' => true))->render(),
                ];
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['errors' => [__('backend.please_try_again_something_error')]]);
        }

    }

    #This used to reset form

    public function randomNumber($type = 1, $user_id = '')
    {
        return random_number($type, Auth::guard('Customer')->user()->id);
    }

    public function save_collection(Request $request)
    {
        $collection = CollectionOrder::where('id', $request->collection_id)->first();
        if ($collection) {
            $orders = TempOrder::where('collection_id', $collection->id)->get();
            if (!empty($orders) && count($orders)) {
                DB::beginTransaction();
                try {
                    $store_id = null;
                    $first_order = $orders[0];
                    if (!$store_id) {
                        $customer = Customer::find($first_order->customer_id);
                        if ($customer && $customer->store_id) {
                            $store_id = $customer->store_id;
                        } else {
                            $store = Store::where('status', 1)->where('pickup_default', 1)->first();
                            if ($store) {
                                $store_id = $store->id;
                            }
                        }
                    }

                    foreach ($orders as $tmporder) {
                        $order = new Order();
                        $order->reference_number = $tmporder->reference_number;
                        $order->delivery_type = $tmporder->delivery_type;
                        $order->sender_subphone = $tmporder->sender_subphone;
                        $order->receiver_name = $tmporder->receiver_name;
                        $order->receiver_mobile = $tmporder->receiver_mobile;
                        $order->receiver_phone = $tmporder->receiver_phone;
                        $order->receiver_latitude = $tmporder->receiver_latitude;
                        $order->receiver_longitude = $tmporder->receiver_longitude;
                        $order->receiver_address = $tmporder->receiver_address;
                        #//
                        $order->collection_id = $collection->id;
                        $order->sender_name = $tmporder->sender_name;
                        $order->sender_mobile = $tmporder->sender_mobile;
                        $order->sender_phone = $tmporder->sender_phone;
                        $order->sender_latitude = $tmporder->sender_latitude;
                        $order->sender_longitude = $tmporder->sender_longitude;
                        $order->sender_address = $tmporder->sender_address;
                        $order->r_state_id = $tmporder->r_state_id;
                        $order->s_state_id = $tmporder->s_state_id;
                        $order->r_government_id = $tmporder->r_government_id;
                        $order->s_government_id = $tmporder->s_government_id;
                        $order->status = 0;
                        $order->type = $tmporder->type;
                        $order->order_type_id = $tmporder->order_type_id;
                        $order->agent_id = $tmporder->agent_id;
                        $order->customer_id = $tmporder->customer_id;
                        $order->payment_method_id = $tmporder->payment_method_id;
                        $order->order_price = $tmporder->order_price;
                        $order->delivery_price = $tmporder->delivery_price;
                        $order->overload = $tmporder->overload;
                        $order->notes = $tmporder->notes;
                        $order->order_number = $tmporder->order_number;
                        $order->receiver_code = $tmporder->receiver_code;
                        $order->moved_in = $store_id;
                        if ($tmporder->order_no != "") {
                            $order->order_no = $tmporder->order_no;
                        } else {
                            $order->order_no = $this->randomNumber(1);//substr(uniqid(sha1(time())), 0, 8);
                        }

                        $order->qty = $tmporder->qty;
                        if ($tmporder->store_id > 0) {
                            $order->store_id = $tmporder->store_id;
                        }
                        $order->main_store = $tmporder->main_store;
                        if ($tmporder->main_store) {
                            $order->head_office = 1;
                        }
                        $order->save();
                        if (!empty($tmporder->forward_driver_id) && $tmporder->forward_driver_id > 0) {
                            // forward orded beefore

                            $accept_order = new AcceptedOrder();
                            $accept_order->order_id = $order->id;
                            $accept_order->driver_id = $tmporder->forward_driver_id;
                            $accept_order->delivery_price = $order->delivery_price;
                            $accept_order->status = 0;

                            $captain = Driver::find($request->driver_id);
                            if ($captain) {
                                $accept_order->driver_bonus = $captain->bouns_of_delivery;
                            }

                            $accept_order->save();

                            #then update order status to be accepted
                            $order->status = 1;
                            $order->accepted_at = Carbon::now();
                            $order->save();
                        }
                    }
                    $collection->saved_at = Carbon::now();
                    $collection->is_saved = 1;
                    $collection->save();

                    // Function Add Action In Activity Log By Samir
                    addActivityLog('Save Collection Orders #No ' . $order->collection_id, ' حفظ مجموعة الطلبات الجديدة #رقم ' . $order->collection_id, 7); // Helper Function in App\Helpers.php
                    TempOrder::where('collection_id', $collection->id)->delete();
                    DB::commit();
                    Flash::success(__('backend.Collection_saved_successfully'));
                    return redirect('/mngrAdmin/collection_orders?type=1');
                } catch (Exception $e) {
                    DB::rollback();
                    Flash::error(__('backend.Collection_not_Saved_Something_Error'));
                    return redirect('/mngrAdmin/collection_orders?type=1');
                }
            } else {
                flash(__('backend.Collection_that_you_try_to_save_does_not_have_orders'))->error();
                return redirect('/mngrAdmin/collection_orders?type=1');
            }
        } else {
            flash(__('backend.Collection_that_you_try_to_save_does_not_exist'))->error();
            return redirect('/mngrAdmin/collection_orders?type=1');
        }

    }

    public function complete_collection($id, Request $request)
    {
        $collection = CollectionOrder::where('id', $id)->first();
        if ($collection) {
            $orders = TempOrder::where('collection_id', $collection->id)->get();
            if (!empty($orders) && count($orders)) {
                DB::beginTransaction();
                try {
                    $store_id = null;
                    $first_order = $orders[0];
                    if (!$store_id) {
                        $customer = Customer::find($first_order->customer_id);
                        if ($customer && $customer->store_id) {
                            $store_id = $customer->store_id;
                        } else {
                            $store = Store::where('status', 1)->where('pickup_default', 1)->first();
                            if ($store) {
                                $store_id = $store->id;
                            }
                        }
                    }

                    foreach ($orders as $tmporder) {
                        $order = new Order();
                        $order->reference_number = $tmporder->reference_number;
                        $order->delivery_type = $tmporder->delivery_type;
                        $order->sender_subphone = $tmporder->sender_subphone;
                        $order->receiver_name = $tmporder->receiver_name;
                        $order->receiver_mobile = $tmporder->receiver_mobile;
                        $order->receiver_phone = $tmporder->receiver_phone;
                        $order->receiver_latitude = $tmporder->receiver_latitude;
                        $order->receiver_longitude = $tmporder->receiver_longitude;
                        $order->receiver_address = $tmporder->receiver_address;
                        #//
                        $order->collection_id = $collection->id;
                        $order->sender_name = $tmporder->sender_name;
                        $order->sender_mobile = $tmporder->sender_mobile;
                        $order->sender_phone = $tmporder->sender_phone;
                        $order->sender_latitude = $tmporder->sender_latitude;
                        $order->sender_longitude = $tmporder->sender_longitude;
                        $order->sender_address = $tmporder->sender_address;
                        $order->r_state_id = $tmporder->r_state_id;
                        $order->s_state_id = $tmporder->s_state_id;
                        $order->r_government_id = $tmporder->r_government_id;
                        $order->s_government_id = $tmporder->s_government_id;
                        $order->status = 0;
                        $order->type = $tmporder->type;
                        $order->order_type_id = $tmporder->order_type_id;
                        $order->agent_id = $tmporder->agent_id;
                        $order->customer_id = $tmporder->customer_id;
                        $order->payment_method_id = $tmporder->payment_method_id;
                        $order->order_price = $tmporder->order_price;
                        $order->delivery_price = $tmporder->delivery_price;
                        $order->overload = $tmporder->overload;
                        $order->notes = $tmporder->notes;
                        $order->order_number = $tmporder->order_number;
                        $order->receiver_code = $tmporder->receiver_code;
                        $order->moved_in = $store_id;
                        if ($tmporder->order_no != "") {
                            $order->order_no = $tmporder->order_no;
                        } else {
                            $order->order_no = $this->randomNumber(1);//substr(uniqid(sha1(time())), 0, 8);
                        }

                        $order->qty = $tmporder->qty;
                        if ($tmporder->store_id > 0) {
                            $order->store_id = $tmporder->store_id;
                        }
                        $order->main_store = $tmporder->main_store;
                        if ($tmporder->main_store) {
                            $order->head_office = 1;
                        }
                        $order->save();
                        if (!empty($tmporder->forward_driver_id) && $tmporder->forward_driver_id > 0) {
                            // forward orded beefore

                            $accept_order = new AcceptedOrder();
                            $accept_order->order_id = $order->id;
                            $accept_order->driver_id = $tmporder->forward_driver_id;
                            $accept_order->delivery_price = $order->delivery_price;
                            $accept_order->status = 0;

                            $captain = Driver::find($request->driver_id);
                            if ($captain) {
                                $accept_order->driver_bonus = $captain->bouns_of_delivery;
                            }

                            $accept_order->save();

                            #then update order status to be accepted
                            $order->status = 1;
                            $order->accepted_at = Carbon::now();
                            $order->save();
                        }
                    }
                    $collection->saved_at = Carbon::now();
                    $collection->is_saved = 1;
                    $collection->save();

                    // Function Add Action In Activity Log By Samir
                    TempOrder::where('collection_id', $collection->id)->delete();
                    DB::commit();
                    Flash::success(__('backend.Collection_saved_successfully'));
                    return response()->json([true]);
                } catch (Exception $e) {
                    DB::rollback();
                    Flash::error(__('backend.Collection_not_Saved_Something_Error'));
                    return response()->json([true]);
                }
            } else {
                flash(__('backend.Collection_that_you_try_to_save_does_not_have_orders'))->error();
                return response()->json([true]);
            }
        }
    }

    //uploadSheet

    public function tmporders_reset_view($id, Request $request)
    {
        $collection_id = $id;
        $user = Auth::guard('Customer')->user();
        $governments = Governorate::orderBy('name_en', 'ASC')->get();
        $agents = Agent::orderBy('name', 'ASC')->get();
        #payment_methods
        $allStores = Store::orderBy('id', 'ASC')->get();
        $payments = PaymentMethod::where('status', 1)->orderBy('name', 'ASC')->get();
        $types = OrderType::orderBy('name_en', 'ASC')->get();
        $Corporate_ids = Corporate::get();
        $customer = null;
        $drivers = [];
        $customers = [];
        $collection = null;
        $stores = [];

        if (!empty($collection_id)) {
            $collection = CollectionOrder::where('id', $collection_id)->first();
            if (!empty($collection) && $collection != null) {
                $customer = Customer::where('id', $collection->customer_id)->first();
                if ($customer) {
                    $customers = Customer::where('corporate_id', $customer->corporate_id)->get();
                    $collection->corporate_id = Corporate::where('id', $customer->corporate_id)->value('id');
                    $stores = Stock::whereRaw('qty - used_qty > 0')->where('corporate_id', $customer->corporate_id)->get();
                }
                if ($request->ajax()) {
                    return [
                        'view' => view('frontend.profile.collections.update_form', compact('drivers', 'customers', 'collection_id', 'payments', 'agents', 'governments', 'types', 'Corporate_ids', 'collection', 'stores', 'customer'))->render(),
                    ];
                }
            } else {
                return 'false';
            }
        } else {
            return 'false';
        }
    }

    public function tmporders_update_view($id, Request $request)
    {
        $governments = Governorate::orderBy('name_en', 'ASC')->get();
        #payment_methods
        $payments = PaymentMethod::where('status', 1)->orderBy('name', 'ASC')->get();
        $types = OrderType::orderBy('name_en', 'ASC')->get();
        $collection_id = 0;
        $order = TempOrder::where('id', $id)->first();
        $allStores = Store::orderBy('id', 'ASC')->get();
        $s_cities = [];
        $r_cities = [];
        $stores = [];
        $collection = null;
        $customer = Auth::guard('Customer')->user();

        if (!empty($order) && $order != null) {
            $collection_id = $order->collection_id;
            $collection = CollectionOrder::where('id', $collection_id)->first();
            $s_cities = City::where('governorate_id', $order->s_government_id)->orderBy('name_en', 'ASC')->get();
            $r_cities = City::where('governorate_id', $order->r_government_id)->orderBy('name_en', 'ASC')->get();
            if (!empty($order->customer) && $order->customer != null) {
                $order->corporate_id = $order->customer->corporate_id;
                $stores = Stock::whereRaw('qty - used_qty > 0')->where('corporate_id', $order->corporate_id)->get();
            }
        }
        if ($request->ajax()) {
            return [
                'view' => view('frontend.profile.collections.update_form', compact('r_cities', 's_cities', 'order', 'collection_id', 'payments', 'governments', 'types', 'collection', 'stores', 'allStores', 'customer'))->render(),
            ];
        }
    }

    public function getupdate()
    {
        $customer = auth()->guard('Customer')->user();
        $orders = CollectionOrder::with('customer')
            ->where(function ($query) {
                $query->whereNull('collection_orders.type')
                    ->orWhere('collection_orders.type', '!=', 1);
            });

        if ($customer->is_manager) {
            $orders = $orders->join('customers', 'customers.id', 'collection_orders.customer_id')
                ->where('corporate_id', $customer->corporate_id);
        } else {
            $orders = $orders->where('customer_id', $customer->id);
        }

        $orders = $orders->select('collection_orders.*')->orderBy('collection_orders.id', 'DESC')->paginate(15);

        return View('frontend.profile.excel.upload-excel', compact('orders', 'customer'));
    }

    public function process($id, Request $request)
    {
        $collection = CollectionOrder::find($id);

        if ($collection) {
            $customer_id = auth()->guard('Customer')->user()->id;
            $customer = auth()->guard('Customer')->user();
            $store_id = null;
            if (!$store_id) {
                if ($customer && $customer->store_id) {
                    $store_id = $customer->store_id;
                } else {
                    $store = Store::where('status', 1)->where('pickup_default', 1)->first();
                    if ($store) {
                        $store_id = $store->id;
                    }
                }
            }
            $payment_id = 1;

            $orders = [];
            $orders_errors = array();
            $file_has_data = false;

            $file = public_path('/api_uploades/client/corporate/sheets/' . $collection->file_name);

            if (\File::exists($file)) {
                Excel::filter('chunk')->load($file)->chunk(250, function ($results) use ($store_id, $customer, $payment_id, &$orders, &$orders_errors, &$file_has_data) {
//                dd($results);
                    foreach ($results as $i => $row) {

                        $file_has_data = true;
                        $row['validation_errors'] = '';
//                    if ($i == 0) {
//                        $orders_errors = array($row);
//                        continue;
//                    }
//
//                    dd($row);
                        if (!isset($row['alasm']) || !$row['alasm']) {
                            $row['validation_errors'] = $row['validation_errors'] . 'ادخل الاسم' . ' / ';
                        }
                        if (!isset($row['alaanoan']) || !$row['alaanoan']) {
                            $row['validation_errors'] = $row['validation_errors'] . 'ادخل العنوان' . ' / ';
                        }

                        if (!$row['rkm_altlyfon']) {
                            $row['validation_errors'] = $row['validation_errors'] . 'ادخل رقم الموبايل' . ' / ';
                        } else {
                            $mobile = preg_replace('/[^0-9]/', '', $row['rkm_altlyfon']);
                            if (strlen($mobile) > 11 || strlen($mobile) < 10 || (strlen($mobile) == 10 && substr($mobile, 0, 1) === "0") || (strlen($mobile) == 11 && substr($mobile, 0, 1) !== "0")) {
                                $row['validation_errors'] = $row['validation_errors'] . 'خطأ فى رقم الموبايل' . ' / ';
                            } elseif (strlen($mobile) == 10) {
                                $mobile = '0' . $mobile;
                            }

                            $row['rkm_altlyfon'] = $mobile;
                        }

                        if (isset($row['rkm_alrasl_alfraay']) && $row['rkm_alrasl_alfraay']) {
                            $sub_phone = preg_replace('/[^0-9]/', '', $row['rkm_alrasl_alfraay']);
                            if (strlen($sub_phone) > 11 || strlen($sub_phone) < 10 || (strlen($sub_phone) == 10 && substr($sub_phone, 0, 1) === "0") || (strlen($sub_phone) == 11 && substr($sub_phone, 0, 1) !== "0")) {
                                $row['validation_errors'] = $row['validation_errors'] . 'خطأ فى رقم الراسل الفرعي' . ' / ';
                            } elseif (strlen($sub_phone) == 10) {
                                $sub_phone = '0' . $sub_phone;
                            }

                            $row['rkm_alrasl_alfraay'] = $sub_phone;
                        }

                        if (!isset($row['alajmal']) || $row['alajmal'] === null || trim($row['alajmal']) === "") {
                            $row['validation_errors'] = $row['validation_errors'] . 'ادخل الإجمالي' . ' / ';
                        }

                        if (!isset($row['alajmal']) || !is_numeric(trim($row['alajmal']))) {
                            $row['validation_errors'] = $row['validation_errors'] . 'خطأ فى الإجمالي' . ' / ';
                        }

                        $government = GovernorateKeyword::where('keyword', $row['almhafth'])
                            ->select('governorate_id AS id')
                            ->first();

                        if (!$government) {
                            $government = Governorate::where('name_ar', @$row['almhafth'])
                                ->OrWhere('name_en', @$row['almhafth'])
                                ->first();
                        }

                        if (!$government) {
                            $row['validation_errors'] = $row['validation_errors'] . 'المحافظة غير موجودة' . ' / ';
                        }

                        $cost = 0;
                        $constObj = null;
                        if ($government) {
                            $to_id = $government->id;
                            if ($customer->corporate_id) {
                                $constObj = CustomerPrice::where('customer_id', $customer->id)
                                    ->where(function ($q) use ($to_id, $customer) {
                                        $q->where(function ($q1) use ($to_id, $customer) {
                                            $q1->where('start_station', $customer->government_id);
                                            $q1->where('access_station', $to_id);
                                        });
                                        $q->orWhere(function ($q1) use ($to_id, $customer) {
                                            $q1->where('access_station', $customer->government_id);
                                            $q1->where('start_station', $to_id);
                                        });
                                    })->where('status', 1)->first();
                            }
                            if (!$constObj) {
                                $constObj = GovernoratePrice::where(function ($q) use ($to_id, $customer) {
                                    $q->where(function ($q1) use ($to_id, $customer) {
                                        $q1->where('start_station', $customer->government_id);
                                        $q1->where('access_station', $to_id);
                                    });
                                    $q->orWhere(function ($q1) use ($to_id, $customer) {
                                        $q1->where('access_station', $customer->government_id);
                                        $q1->where('start_station', $to_id);
                                    });
                                })->where('status', 1)->first();

                                if (!$constObj) {
//                          dd($customer->government_id, $row['almhafth']);
                                    $row['validation_errors'] = $row['validation_errors'] . 'الشحن غير متاح للمحافظة' . ' / ';

                                } else {
                                    $cost = $constObj->cost;
                                }
                            } else {
                                $cost = $constObj->cost;
                            }
                        }

                        if ($row['validation_errors']) {
                            array_push($orders_errors, [
                                "rkm_alshhn" => @$row['rkm_alshhn'],
                                "alasm" => @$row['alasm'],
                                "rkm_altlyfon" => @$row['rkm_altlyfon'],
                                "rkm_tlyfon_aakhr" => @$row['rkm_tlyfon_aakhr'],
                                "alaanoan" => @$row['alaanoan'],
                                "almhafth" => (!empty($government) ? $government->id : ''),
                                "government" => @$row['almhafth'],
                                "alajmal" => @$row['alajmal'],
                                "altlb" => @$row['altlb'],
                                "mlahthat" => @$row['mlahthat'],
                                "rkm_alrasl_alfraay" => (isset($row['rkm_alrasl_alfraay']) ? $row['rkm_alrasl_alfraay'] : ""),
                                "noaa_altslym" => (isset($row['noaa_altslym']) ? $row['noaa_altslym'] : ""),
                                "validation_errors" => @$row['validation_errors']
                            ]);
                            continue;
                        }

                        $row['cost'] = $cost;
                        $row['almhafth'] = (!empty($government) ? $government->id : '');
                        $orders[] = $row;

                    }

                }, false);

//            \Illuminate\Support\Facades\File::delete(public_path('api_uploades/sheets/' . $file));
                $collection_id = null;
                if (count($orders)) {

//                    $collection = new CollectionOrder();
//                    $collection->customer_id = $customer_id;
//                    $collection->count_no = 0;
                    $collection->is_saved = 1;
//                    $collection->complete_later = 1;
                    $collection->saved_at = Carbon::now();
                    $collection->type = 1;
                    $collection->save();
//                    $collection_id = $collection->id;

//                    if ($collection) {
//                        CollectionOrder::where('id', $collectionOrderId)->update(['processed_collection_id' => $collection->id, 'status' => 1]);
//                    }

                    foreach ($orders as $row) {
                        $order = new Order();
                        $order->reference_number = $row['rkm_alshhn'];
                        $order->receiver_name = $row['alasm'];
                        $order->receiver_mobile = trim($row['rkm_altlyfon']);
                        $order->receiver_phone = trim($row['rkm_tlyfon_aakhr']);
                        $order->receiver_latitude = 0;
                        $order->receiver_longitude = 0;
                        $order->receiver_address = $row['alaanoan'];
                        $order->r_government_id = $row['almhafth'];
                        $order->r_state_id = null;
                        if (isset($row['rkm_alrasl_alfraay'])) {
                            $order->sender_subphone = $row['rkm_alrasl_alfraay'];
                        }
                        if (isset($row['noaa_altslym'])) {
                            $order->delivery_type = $row['noaa_altslym'];
                        }
                        #//
                        $order->sender_name = $customer->name;
                        $order->sender_mobile = $customer->mobile;
                        $order->sender_phone = $customer->phone;
                        $order->sender_latitude = $customer->latitude;
                        $order->sender_longitude = $customer->longitude;
                        $order->sender_address = $customer->address;
                        $order->s_government_id = $customer->government_id;
                        $order->s_state_id = $customer->city_id;
                        #//
                        $order->collection_id = $collection->id;
                        $order->status = 0;
                        $order->type = $row['altlb'];
                        $order->moved_in = $store_id;
                        $order->order_type_id = 1;
                        $order->agent_id = null;
                        $order->customer_id = $customer->id;
                        $order->payment_method_id = $payment_id;
                        $order->order_price = ($row['alajmal'] ? trim($row['alajmal']) : 0);
                        $order->delivery_price = $row['cost'];
                        $order->overload = 0;
                        $order->notes = $row['mlahthat'];
                        $order->order_no = $this->randomNumber(1);
                        $order->order_number = $this->randomNumber(2);
                        $order->receiver_code = $this->randomNumber(3);
                        $order->qty = 0;
                        $order->store_id = null;
                        $order->main_store = null;
                        $order->head_office = 0;

                        $order->save();
                    }
                }

                if (count($orders_errors)) {
                    return view('frontend.profile.excel.upload_sheet', compact('orders_errors', 'store_id', 'customer_id', 'collection'))
                        ->with('success', __('website.processed_orders', ['orders' => (isset($collection) ? Order::where('collection_id', $collection->id)->count() : 0)]));
//                $this->orders_report_errors($orders_errors);
                }

                if ($file_has_data) {
                    return redirect()->route('profile.orders', [(app()->getLocale() == 'en' ? 'en' : 'ar'), 'type' => 'pending'])
                        ->with('success', __('website.processed_orders', ['orders' => (isset($collection) ? Order::where('collection_id', $collection->id)->count() : 0)]));
                } else {
                    return redirect()->back()->with('warning', 'File is empty');
                }
            } else {
                return redirect()->back()->withErrors([__('backend.action_back_Error')]);
            }
        }
    }

    public function handle_sheet(Request $request)
    {

        $customer_id = $request->customer_id;
        $collection_id = $request->collection_id;
        $collection = CollectionOrder::find($collection_id);
        $customer = Customer::find($customer_id);
        $store_id = $request->store_id;
        $orders = [];
        $orders_errors = array();
        $payment_id = 1;

        if (count($request->orders)) {
            foreach ($request->orders as $i => $row) {

                $row['validation_errors'] = '';

                if (!$row['alasm']) {
                    $row['validation_errors'] = $row['validation_errors'] . 'ادخل الاسم' . ' / ';
                }
                if (!$row['alaanoan']) {
                    $row['validation_errors'] = $row['validation_errors'] . 'ادخل العنوان' . ' / ';
                }

                if (!$row['rkm_altlyfon']) {
                    $row['validation_errors'] = $row['validation_errors'] . 'ادخل رقم الموبايل' . ' / ';
                } else {
                    $mobile = preg_replace('/[^0-9]/', '', $row['rkm_altlyfon']);
                    if (strlen($mobile) > 11 || strlen($mobile) < 10 || (strlen($mobile) == 10 && substr($mobile, 0, 1) === "0") || (strlen($mobile) == 11 && substr($mobile, 0, 1) !== "0")) {
                        $row['validation_errors'] = $row['validation_errors'] . 'خطأ فى رقم الموبايل' . ' / ';
                    } elseif (strlen($mobile) == 10) {
                        $mobile = '0' . $mobile;
                    }

                    $row['rkm_altlyfon'] = $mobile;
                }

                if (isset($row['rkm_alrasl_alfraay']) && $row['rkm_alrasl_alfraay']) {
                    $sub_phone = preg_replace('/[^0-9]/', '', $row['rkm_alrasl_alfraay']);
                    if (strlen($sub_phone) > 11 || strlen($sub_phone) < 10 || (strlen($sub_phone) == 10 && substr($sub_phone, 0, 1) === "0") || (strlen($sub_phone) == 11 && substr($sub_phone, 0, 1) !== "0")) {
                        $row['validation_errors'] = $row['validation_errors'] . 'خطأ فى رقم الراسل الفرعي' . ' / ';
                    } elseif (strlen($sub_phone) == 10) {
                        $sub_phone = '0' . $sub_phone;
                    }

                    $row['rkm_alrasl_alfraay'] = $sub_phone;
                }

                if ($row['alajmal'] === null || trim($row['alajmal']) === "") {
                    $row['validation_errors'] = $row['validation_errors'] . 'ادخل الإجمالي' . ' / ';
                }

                $government = Governorate::find($row['almhafth']);

                if (!$government) {
                    $row['validation_errors'] = $row['validation_errors'] . 'المحافظة غير موجودة' . ' / ';
                }

                $cost = 0;
                $constObj = null;
                if ($government) {
                    $to_id = $government->id;
                    $constObj = CustomerPrice::where('customer_id', $customer->id)
                        ->where(function ($q) use ($to_id, $customer) {
                            $q->where(function ($q1) use ($to_id, $customer) {
                                $q1->where('start_station', $customer->government_id);
                                $q1->where('access_station', $to_id);
                            });
                            $q->orWhere(function ($q1) use ($to_id, $customer) {
                                $q1->where('access_station', $customer->government_id);
                                $q1->where('start_station', $to_id);
                            });
                        })->where('status', 1)->first();

                    if (!$constObj) {
                        $constObj = GovernoratePrice::where(function ($q) use ($to_id, $customer) {
                            $q->where(function ($q1) use ($to_id, $customer) {
                                $q1->where('start_station', $customer->government_id);
                                $q1->where('access_station', $to_id);
                            });
                            $q->orWhere(function ($q1) use ($to_id, $customer) {
                                $q1->where('access_station', $customer->government_id);
                                $q1->where('start_station', $to_id);
                            });
                        })->where('status', 1)->first();

                        if (!$constObj) {
                            $row['validation_errors'] = $row['validation_errors'] . 'الشحن غير متاح للمحافظة' . ' / ';

                        } else {
                            $cost = $constObj->cost;
                        }
                    } else {
                        $cost = $constObj->cost;
                    }
                }

                if ($row['validation_errors']) {
                    array_push($orders_errors, [
                        "rkm_alshhn" => $row['rkm_alshhn'],
                        "alasm" => $row['alasm'],
                        "rkm_altlyfon" => $row['rkm_altlyfon'],
                        "rkm_tlyfon_aakhr" => $row['rkm_tlyfon_aakhr'],
                        "alaanoan" => $row['alaanoan'],
                        "almhafth" => (!empty($government) ? $government->id : ''),
                        "alajmal" => $row['alajmal'],
                        "altlb" => $row['altlb'],
                        "mlahthat" => $row['mlahthat'],
                        "government" => $row['government'],
                        "rkm_alrasl_alfraay" => (isset($row['rkm_alrasl_alfraay']) ? $row['rkm_alrasl_alfraay'] : ""),
                        "noaa_altslym" => (isset($row['noaa_altslym']) ? $row['noaa_altslym'] : ""),
                        "validation_errors" => $row['validation_errors']
                    ]);
                    continue;
                }

                if ($row['government']) {
                    GovernorateKeyword::firstOrCreate([
                        'keyword' => $row['government'],
                        'governorate_id' => $row['almhafth']
                    ]);
                }

                $orders[] = $row;

            }
        }

        if ($collection_id) {
            $collection = CollectionOrder::find($collection_id);
        }

        if (count($orders)) {


//                $collection = new CollectionOrder();
//                $collection->customer_id = $customer_id;
//                $collection->count_no = 0;
            $collection->is_saved = 1;
//                $collection->complete_later = 1;
            $collection->saved_at = Carbon::now();
//                $collection->type = 1;
            $collection->save();
//                $collection_id = $collection->id;


            foreach ($orders as $row) {
                $order = new Order();
                $order->reference_number = $row['rkm_alshhn'];
                $order->receiver_name = $row['alasm'];
                $order->receiver_mobile = trim($row['rkm_altlyfon']);
                $order->receiver_phone = trim($row['rkm_tlyfon_aakhr']);
                $order->receiver_latitude = 0;
                $order->receiver_longitude = 0;
                $order->receiver_address = $row['alaanoan'];
                $order->r_government_id = $row['almhafth'];
                $order->r_state_id = null;
                if (isset($row['rkm_alrasl_alfraay'])) {
                    $order->sender_subphone = $row['rkm_alrasl_alfraay'];
                }
                if (isset($row['noaa_altslym'])) {
                    $order->delivery_type = $row['noaa_altslym'];
                }
                #//
                $order->sender_name = $customer->name;
                $order->sender_mobile = $customer->mobile;
                $order->sender_phone = $customer->phone;
                $order->sender_latitude = $customer->latitude;
                $order->sender_longitude = $customer->longitude;
                $order->sender_address = $customer->address;
                $order->s_government_id = $customer->government_id;
                $order->s_state_id = $customer->city_id;
                #//
                $order->collection_id = $collection->id;
                $order->status = 0;
                $order->type = $row['altlb'];
                $order->moved_in = $store_id;
                $order->order_type_id = 1;
                $order->agent_id = null;
                $order->customer_id = $customer->id;
                $order->payment_method_id = $payment_id;
                $order->order_price = ($row['alajmal'] ? trim($row['alajmal']) : 0);
                $order->delivery_price = $cost;
                $order->overload = 0;
                $order->notes = $row['mlahthat'];
                $order->order_no = $this->randomNumber(1);
                $order->order_number = $this->randomNumber(2);
                $order->receiver_code = $this->randomNumber(3);
                $order->qty = 0;
                $order->store_id = null;
                $order->main_store = null;
                $order->head_office = 0;

                $order->save();
            }
        }

        if (count($orders_errors)) {
            return view('frontend.profile.excel.upload_sheet', compact('orders_errors', 'store_id', 'customer_id', 'collection'))
                ->with('success', __('website.processed_orders', ['orders' => (isset($collection) ? Order::where('collection_id', $collection->id)->count() : 0)]));
        }

        return redirect()->route('profile.orders', [(app()->getLocale() == 'en' ? 'en' : 'ar'), 'type' => 'pending'])
            ->with('success', __('website.processed_orders', ['orders' => (isset($collection) ? Order::where('collection_id', $collection->id)->count() : 0)]));
    }

    public function export_sheet(Request $request)
    {
        $data[] = [];
        $data = array(array(
            'no.' => 'no.',
            'rkm_alshhn' => 'رقم الشحنة',
            'alasm' => 'الاسم',
            'rkm_altlyfon' => 'رقم التليفون',
            'rkm_tlyfon_aakhr' => 'رقم تليفون آخر',
            'alaanoan' => 'العنوان',
            'almhafth' => 'المحافظة',
            'alajmal' => 'الاجمالى',
            'altlb' => 'الطلب',
            'mlahthat' => 'ملاحظات',
            'اﻷخطاء',
        ));

        foreach ($request->orders as $i => $row) {
            array_push($data, array(
                $row['no.'],
                $row['rkm_alshhn'],
                $row['alasm'],
                $row['rkm_altlyfon'],
                $row['rkm_tlyfon_aakhr'],
                $row['alaanoan'],
                $row['government'],
                $row['alajmal'],
                $row['altlb'],
                $row['mlahthat'],
                $row['validation_errors'],
            ));
        }

        Excel::create('importing_order_result', function ($excel) use ($data) {
            $excel->sheet('SD 27-4', function ($sheet) use ($data) {
                $sheet->fromArray($data, null, 'A1', false, false);
                // Set width for multiple cells
                for ($intRowNumber = 1; $intRowNumber <= count($data) + 1; $intRowNumber++) {
                    $sheet->setSize('A' . $intRowNumber, 5, 10);
                    $sheet->setSize('B' . $intRowNumber, 25, 19);
                    $sheet->setSize('C' . $intRowNumber, 25, 19);
                    $sheet->setSize('D' . $intRowNumber, 25, 20);
                    $sheet->setSize('E' . $intRowNumber, 25, 18);
                    $sheet->setSize('F' . $intRowNumber, 100, 18);
                    $sheet->setSize('G' . $intRowNumber, 15, 18);
                    $sheet->setSize('H' . $intRowNumber, 10, 18);
                    $sheet->setSize('I' . $intRowNumber, 50, 18);
                    $sheet->setSize('J' . $intRowNumber, 100, 18);
                    $sheet->setSize('K' . $intRowNumber, 100, 18);
                }
                $sheet->cells('A1:I' . count($data), function ($cells) {

                    // manipulate the range of cells
                    $cells->setAlignment('center');

                });
                $sheet->cells('H1:H' . count($data), function ($cells) {

                    // manipulate the range of cells
                    $cells->setBackground('#9dc3e6');
                    $cells->setBorder('thin');

                });
                $sheet->cells('K1:K' . count($data), function ($cells) {

                    // manipulate the range of cells
                    $cells->setBackground('#ed1c24');
                    $cells->setBorder('solid');

                });

                $sheet->setStyle(array(
                    'font' => array(
                        'name' => 'Arial',
                        'size' => 10,
                        'bold' => false,
                        'Alignment' => 'center',
                    )
                ));
#

                $sheet->row(1, function ($row) {
                    // call row For Header
                    $row->setBackground('#002060');
                    $row->setFontColor('#ffffff');
                    $row->setFont(array(
                        'family' => 'Arial',
                        'size' => '14',
                        'bold' => false
                    ));
                    $row->setAlignment('center');

                });

            });
        })->export('xls');

    }

    public function upload(Request $request)
    {
//        $this->validate($request, [
//            'file' => 'mimes:xls,xlsx',
//        ]);

        $validator = Validator::make(
            [
                'file' => $request->file,
                'extension' => ($request->hasFile('file') ? ($request->file->getClientOriginalExtension()) : ""),
            ],
            [
                'file' => 'required',
                'extension' => 'required|in:xlsx,xls',
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $costomer = auth()->guard('Customer')->user()->id;

        if ($request->file("file") != "") {
            $allowedext = array("xls", "xlsx");
            $extension = File::extension($request->file("file")->getClientOriginalName());

            if (in_array($extension, $allowedext)) {
                $file = "sheet" . "_" . "." . $extension;
                $select = CollectionOrder::create([
                    'file_name' => $file,
                    'customer_id' => $costomer,
                    'type' => 0,
                    'uploaded_target' => 1,
                    'created_at' => date('Y-m-d h:i:s')
                ]);
//                dd($fileupload->id);
//                $select = CollectionOrder::where('customer_id', $costomer)->latest()->first();
                $fileuploadd = "sheet" . "_" . $select->id . "_" . "customer" . "_" . $costomer . "." . $extension;
                $request->file('file')->move(public_path('api_uploades/client/corporate/sheets/'), $fileuploadd);
                $reupdate = CollectionOrder::where('id', $select->id)->update(['file_name' => $fileuploadd]);
                Flash::success(__('backend.Sheet_uploaded_successfully'));
            } else {
                echo 'allowedext extension ';
            }
        } else {
            echo 'error';
        }
        return redirect()->back();
    }/* list customer notification*/

    public function indexprofilewebsite()
    {
        return view('frontend.profile.under_processing');
        $idcustome = auth()->guard('Customer')->user()->corporate_id;
        $corprates = Corporate::where('id', $idcustome)->get()->First();
        $invoices = Invoice::orderBy('id', 'desc')->where(['type' => 2, 'object_id' => $corprates->id])->paginate(50);
        return view('frontend.profile.invoices', compact('invoices'));
    }

    public function profilewallet()
    {
        $id = auth()->guard('Customer')->user()->corporate->id;
        $wallet = Wallet::where('object_id', $id)->where('type', 2)->first();

        if (!$wallet) {
            return view('frontend.profile.under_processing');
        }

        return view('frontend.profile.wallets', compact('wallet'));
    }

    public function notifications()
    {
        $id = auth()->guard('Customer')->user()->id;
        $notifications = Notification::where('client_id', $id)->where('client_model', 2)->paginate(15);
        return view('frontend.profile.notifications', compact('notifications'));

    }

    public function delete_notification(Request $request)
    {

        Notification::destroy($request->id);

        return redirect()->back();
    }

    public function upload_profile(Request $request)
    {
        //Fr3on
        $v = \Validator::make($request->all(), [
            'Photo' => 'image|mimes:jpeg,png,jpg,gif',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }
        $user_auth = auth()->guard('Customer')->user();
        if ($request->file("Photo") != null) {
            if ($request->newpassword == null) {

                $extension = File::extension($request->file("Photo")->getClientOriginalName());
                $image = 'profile-' . $request->corporate_id . time() . '_' . $request->customer_id . "." . $extension;
                $path = public_path('/api_uploades/client/profile');
                $request->file("Photo")->move($path, $image);
                $users = Customer::where('id', $request->customer_id)->update([
                    'password' => $request->oldpassword,
                    'image' => $image,
                ]);
                return redirect()->route('profile.dashboard', app()->getLocale() == 'en' ? 'en' : 'ar');
            } else {
                $users = Customer::where('id', $request->customer_id)->update([
                    'password' => bcrypt($request->newpassword),
                ]);
                return redirect()->route('profile.dashboard', app()->getLocale() == 'en' ? 'en' : 'ar');
            }
        } else {
            if ($request->newpassword != null) {
                $users = Customer::where('id', $request->customer_id)->update([
                    'password' => bcrypt($request->newpassword),
                ]);
                return redirect()->route('profile.dashboard', app()->getLocale() == 'en' ? 'en' : 'ar');
            } else {
                $users = Customer::where('id', $request->customer_id)->update([
                    'password' => $request->oldpassword,
                ]);
                return redirect()->route('profile.dashboard', app()->getLocale() == 'en' ? 'en' : 'ar');
            }

        }
    }

    public function orders(Request $request)
    {

        $usr = auth()->guard('Customer')->user()->id;
        $type = app('request')->input('type');
        $orderId = app('request')->input('id');
        $id = app('request')->input('customer');
        $collection = app('request')->input('collection');
        $r_government_id = app('request')->input('r_government_id');
        $date = app('request')->input('date');
        $search = app('request')->input('search');
        $refund_collection = app('request')->input('refund_collection');
        $customer = auth()->guard('Customer')->user();

        $settings = Setting::where('key', 'disable_real_date')->value('value');

        if ($orderId) {
            $order = order::where('orders.id', $orderId)->first();
            $settings = Setting::where('key', 'disable_real_date')->value('value');
            return View('frontend.profile.orderveiw', compact('order', 'settings'));

        }
        //if ($user->role_id  == '1') {
        if ($id) {
            $orders = Order::orderBy('orders.id', 'desc')->where('customer_id', $id)->paginate(50);

        } elseif (!empty($collection) && $collection > 0) {
            $type = "Collection Orders ";
            $orders = Order::orderBy('orders.id', 'desc')->where('collection_id', $collection);
            $orders = $orders->paginate(50);
            return View('frontend.profile.orders', compact('orders', 'type', 'customer'));

        } else {
            $orders = Order::with('customer')->select('orders.*')->orderBy('orders.id', 'desc');
            if ($customer->is_manager) {
                $orders = $orders->join('customers', 'customers.id', 'orders.customer_id')
                    ->where('corporate_id', $customer->corporate_id);
            } else {
                $orders = $orders->where('customer_id', $usr);
            }

            if ($type == 'pending') {
                $orders = $orders->where('orders.status', 0);

            } elseif ($type == 'dropped') {

                $orders = $orders->where('orders.status', 7);

            } else if ($type == 'waiting') {
                $todaydate = Carbon::now()->subDay(5)->toDateTimeString();
                $orders = $orders->where('orders.status', 6)->where('created_at', '>=', $todaydate);
            } elseif ($type == 'accept') {

                $orders = $orders->where('orders.status', 1);

            } elseif ($type == 'receive') {

                if ($settings) {
                    $orders = $orders->where(function ($query) {
                        $query->where('orders.status', 2)
                            ->orWhere(function ($query) {
                                $query->where('orders.status', 3)
                                    ->whereIn('orders.r_government_id', [1, 2])
                                    ->whereRaw('TIMESTAMPDIFF(SECOND, received_at, NOW()) < 172800');
                            })
                            ->orWhere(function ($query) {
                                $query->where('orders.status', 3)
                                    ->whereNotIn('orders.r_government_id', [1, 2])
                                    ->whereRaw('TIMESTAMPDIFF(SECOND, received_at, NOW()) < 331200');
                            });
                    });
                } else {
                    $orders = $orders->where('orders.status', 2);
                }

            } elseif ($type == 'deliver') {

                if ($settings) {
                    $orders = $orders->where('orders.is_refund', 0)->where(function ($query) {
                        $query->orWhere(function ($query) {
                            $query->where('orders.status', 3)
                                ->whereIn('orders.r_government_id', [1, 2])
                                ->whereRaw('TIMESTAMPDIFF(SECOND, received_at, NOW()) >= 172800');
                        })
                            ->orWhere(function ($query) {
                                $query->where('orders.status', 3)
                                    ->whereNotIn('orders.r_government_id', [1, 2])
                                    ->whereRaw('TIMESTAMPDIFF(SECOND, received_at, NOW()) >= 331200');
                            });
                    });
                } else {
                    $orders = $orders->where('orders.is_refund', 0)->where('orders.status', 3);
                }

            } elseif ($type == 'cancel') {
                $orders = $orders->where('orders.status', 4)
                    ->where(function ($query) {
                        $query->where('orders.status_before_cancel', 0)
                            ->orWhereNull('orders.status_before_cancel');
                    });

//                $orders = $orders->where('orders.status', 4);

            } elseif ($type == 'recall') {
                $orders = $orders->where(function ($query) {
                    $query->where('orders.status', 5)
                        ->orWhere('orders.status', 8)
                        ->orWhere(function ($query) {
                            $query->where('orders.status', 3)
                                ->where('delivery_status', 2);
                        })
                        ->orWhere(function ($query) {
                            $query->where('orders.status', 3)
                                ->where('delivery_status', 4);
                        })
                        ->orWhere(function ($query) {
                            $query->where('orders.status', 4)
                                ->where('status_before_cancel', '!=', 0);
                        });
                });
//                $orders = $orders->where('orders.status', 5);

            } elseif ($type == 'refund') {

                $orders = $orders->where('is_refund', 1);
            }
//            if (isset($search) && $search != "") {
//                $orders = $orders->where('id', 'like', '%' . $search . '%');
//            }

            if (isset($search) && $search != "") {
                $orders = $orders->where(function ($q) use ($search) {
                    $q->where('orders.id', 'like', '%' . $search . '%');
                    $q->OrWhere('order_number', 'like', '%' . $search . '%');
                    $q->OrWhere('receiver_name', 'like', '%' . $search . '%');
                    $q->OrWhere('receiver_mobile', 'like', '%' . $search . '%');
                    $q->orWhere('sender_mobile', 'like', '%' . $search . '%');
                    $q->orWhere('sender_name', 'like', '%' . $search . '%');
                    $q->orWhere('order_no', 'like', '%' . $search . '%');
                    $q->orWhere('reference_number', 'like', '%' . $search . '%');
                });
            }

            if ($r_government_id) {
                $orders = $orders->where('r_government_id', $r_government_id);
            }

            if ($refund_collection) {
                $orders = $orders->where('refund_collection_id', $refund_collection);
            }

            if (isset($date) && $date != "") {
                $orders = $orders->where('orders.created_at', 'like', $date . '%');
            }
        }
        if ($request->ajax()) {
            $orders = $orders->paginate(15);
            return [
                'view' => view('backend.orders.table', compact('orders', 'type', 'customer'))->render(),
            ];
        } else {
            $orders = $orders->paginate(15);
            return View('frontend.profile.orders', compact('orders', 'type', 'customer'));
        }
    }

    public function orderview($id)
    {
        $order = order::where('id', $id)->first();
        $settings = Setting::where('key', 'disable_real_date')->value('value');
        return View('frontend.profile.orderveiw', compact('order', 'settings'));
    }/*update*/

    public function order_edit_without_lang($id, Request $request)
    {
        return $this->order_edit('', $id, $request);
    }

    public function order_edit($lang = '', $id, Request $request)
    {
        $governments = Governorate::orderBy('name_en', 'ASC')->get();
        $payments = PaymentMethod::where('status', 1)->orderBy('name', 'ASC')->get();
        $types = OrderType::orderBy('name_en', 'ASC')->get();
        $order = order::where('id', $id)->first();
        $allStores = Store::orderBy('id', 'ASC')->get();
        $s_cities = [];
        $r_cities = [];
        $stores = [];
        $customer = Auth::guard('Customer')->user();

        if (!empty($order) && $order != null) {
            $s_cities = City::where('governorate_id', $order->s_government_id)->orderBy('name_en', 'ASC')->get();
            $r_cities = City::where('governorate_id', $order->r_government_id)->orderBy('name_en', 'ASC')->get();
            if (!empty($order->customer) && $order->customer != null) {
                $order->corporate_id = $order->customer->corporate_id;
                $stores = Stock::whereRaw('qty - used_qty > 0')->where('corporate_id', $order->corporate_id)->get();
            }
        }

        return View('frontend.profile.order_edit', compact('r_cities', 's_cities', 'order', 'payments', 'governments', 'types', 'stores', 'allStores', 'customer'));
    }

    public function order_update(Request $request, $id)
    {

        $this->validate($request, [
            'receiver_name' => 'required',
            'receiver_mobile' => 'required',
            'payment_method_id' => 'required',
            'r_government_id' => 'required',
            'r_state_id' => 'required'
        ]);

        $order = Order::findOrFail($id);

        if ($order) {
            $order->reference_number = $request->input("reference_number");
            $order->delivery_type = $request->input("delivery_type");
            $order->sender_subphone = $request->input("sender_subphone");
            $order->receiver_name = $request->input("receiver_name");
            $order->receiver_mobile = $request->input("receiver_mobile");
            $order->receiver_phone = $request->input("receiver_phone");
            $order->receiver_latitude = $request->input("receiver_latitude");
            $order->receiver_longitude = $request->input("receiver_longitude");
            $order->receiver_address = $request->input("receiver_address");
            $order->r_state_id = $request->input("r_state_id");
            $order->r_government_id = $request->input("r_government_id");
            $order->type = $request->input("type");
            $order->payment_method_id = $request->input("payment_method_id");
            $order->order_price = $request->input("order_price");
            $order->delivery_price = $request->input("delivery_price");
            $order->notes = $request->input("notes");
            $order->store_id = $request->input("store_id");
            $order->qty = $request->input("qty");

            if ($request->input("main_store") > 0) {
                $order->main_store = $request->input("main_store");
                $order->head_office = 1;
            } else {
                $order->head_office = 0;
            }

            $order->save();

            Flash::warning(__('backend.Order_Updated_successfully'));

            return redirect()->back();
        } else {
            return redirect('/mngrAdmin/404');
        }
    }/*quickRandom*/

    public function edit_profile(Request $request)
    {

        $user = auth()->guard('Customer')->user();

        $this->validate($request, [
            'name' => 'required|min:3|max:50',
            'email' => 'required|unique:customers,email,' . $user->id,
            'mobile' => 'required|unique:customers,mobile,' . $user->id,
            'company_name' => 'required|min:3|max:100',
            'company_email' => 'required|unique:corprates,email,' . $user->corporate_id,
            'company_mobile' => 'required|unique:corprates,mobile,' . $user->corporate_id,
        ]);

    }/*quickRandom*/

    public function test(Request $request)
    {
        //return $request->mobile;
        return $this->SendSMSCode(trim($request->mobile), $request->message);
    }

    public function cancel_orders_request(Request $request)
    {
        $reason = $request->reason;
        $customer = auth()->guard('Customer')->user();
        foreach ($request->orders_ids_to_cancel as $order_id) {
            $order_cancel_request = new OrderCancelRequest();
            $order_cancel_request->order_id = $order_id;
            $order_cancel_request->customer_id = $customer->id;
            $order_cancel_request->reason = $reason;
            $order_cancel_request->save();
        }
        return redirect()->back();

    }

    public function cancel_orders(Request $request)
    {
        $orders_ids = $request->orders_to_cancel;
        Order::orderBy('id')->whereIn('id', $orders_ids)->update(
            [
                'status' => '4',
                'cancelled_by' => '2',
                'cancelled_at' => Carbon::now(),
                'status_before_cancel' => 0
            ]);

        event(new OrderUpdated($orders_ids, Auth::guard('Customer')->user()->id, 2, 'cancel', ['cancelled_by' => 2]));

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function delete_collection($id)
    {

        $CollectionOrder = CollectionOrder::findOrFail($id);
        if ($CollectionOrder) {
            flash(__('backend.Collection_Order_deleted_successfully'))->error();

            Order::where('collection_id', $CollectionOrder->id)->where('status', 0)->delete();
            TempOrder::where('collection_id', $CollectionOrder->id)->delete();
            $CollectionOrder->delete();
        }
        return redirect()->route('profile.collectionOrders', app()->getLocale() == 'en' ? 'en' : 'ar');
    }

    public function addOrderComment(Request $request)
    {

        $message = '';
        DB::beginTransaction();
        try {

            $data = $request->only('order_id', 'comment');
            $data['user_id'] = Auth::guard('Customer')->user()->id;
            $data['user_type'] = 2;

            OrderComment::create($data);

            $ticket = new Ticket();
            $ticket->order_id = $request->input("order_id");
//            $ticket->user_id = Auth::guard('Customer')->user()->id;
            $ticket->ticket_number = random_number(2, Auth::guard('Customer')->user()->id);
            $ticket->reason = $request->input("comment");
            $ticket->assigned_to_id = Auth::guard('Customer')->user()->Corporate->responsible_id;
            $ticket->save();

            event(new OrderUpdated([$request->order_id],
                Auth::guard('Customer')->user()->id,
                2,
                'comments',
                ['comment' => $request->get('comment')]
            ));

            if ($request->ajax()) {
                $message = __('backend.add_order_comment_successfully');
            } else {
                Flash::success(__('backend.add_order_comment_successfully'));
            }

            DB::commit();

        } catch (Exception $e) {
            if ($request->ajax()) {
                $message = __('backend.Something_Error');
            } else {
                Flash::error(__('backend.Something_Error'));
            }

            DB::rollback();
        }

        if ($request->ajax()) {
            return response()->json(['message' => $message]);
        } else {
            return redirect()->back();
        }
    }

    public function profile_daily_report()
    {
        $user = Auth::guard('Customer')->user();
        $customers = Customer::orderBy('customers.id', 'DESC');
        if ($user->is_manager) {
            $customers = $customers->where('corporate_id', $user->corporate_id);
        } else {
            $customers = $customers->where('id', $user->id);
        }

        $customers = $customers->get();

        return view('frontend.profile.reports.index', compact('customers'));
    }

    public function recall_if_dropped($id)
    {
        $order = Order::findOrFail($id);
        if (isset($order)) {
            $order->recall_if_dropped = 1;
            $order->save();
            event(new OrderUpdated([$id], Auth::guard('Customer')->user()->id, 2, 'recall_if_dropped'));
            Flash::warning(__('backend.Order_Updated_successfully'));
        }

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function indexInvoice(Request $request)
    {

        $user = Auth::guard('Customer')->user();

        $invoices = Invoice::orderBy('id', 'desc')->where('type', 2)
            ->where('object_id', $user->corporate_id)
            ->orderBy('invoices.created_at', 'DESC');

        $invoices = $invoices->paginate('15');

        return view('frontend.profile.invoices.index', compact('invoices'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showInvoice($id, Request $request)
    {
        $customer = Auth::guard('Customer')->user();
        $invoice = Invoice::findOrFail($id);

        $pickups = PickUp::join('invoice_pickups', 'pick_ups.id', 'invoice_pickups.pickup_id')
            ->join('drivers', 'drivers.id', 'pick_ups.driver_id')
            ->whereIn('pick_ups.status', [2, 3])
            ->where('invoice_pickups.invoice_id', $id)
            ->select('pick_ups.id', 'pick_ups.pickup_number', 'pick_ups.pickup_number', 'drivers.name',
                'pick_ups.status', 'pick_ups.created_at', 'pick_ups.received_at', 'pick_ups.delivered_at', 'pick_ups.bonus_per_order',
                'pick_ups.delivery_price', 'invoice_pickups.captain_bonus', 'invoice_pickups.pickup_price', 'pick_ups.pickup_orders')
            ->withCount('pickup_orders');

        if ($customer->is_manager) {
            $pickups->where('corporate_id', $customer->corporate_id);
        } else {
            $pickups->where('customer_id', $customer->id);
        }

        $pickups = $pickups->get();

        // this all orders that the driver has approved
        $AcceptedOrders = Order::with(['customer' => function ($query) {
            $query->with('Corporate');
        }, 'to_government'])->join('accepted_orders', 'orders.id', '=', 'accepted_orders.order_id')
            ->join('drivers', 'accepted_orders.driver_id', '=', 'drivers.id')
            ->join('invoice_orders as invOr', 'accepted_orders.id', '=', 'invOr.accepted_order_id')
            ->join('invoices', 'invOr.invoice_id', '=', 'invoices.id')
            ->join('payment_methods', 'payment_methods.id', '=', 'orders.payment_method_id')
            ->select('orders.id', 'orders.type', 'orders.created_at', 'orders.order_price', 'orders.payment_method_id', 'orders.customer_id',
                'orders.status', 'accepted_orders.id as accepted_id', 'drivers.bouns_of_delivery as driver_bonus',
                'orders.delivery_price', 'orders.overweight_cost', 'drivers.recall_price as recall_price', 'drivers.reject_price as reject_price',
                'orders.order_price as price', 'orders.received_at', 'orders.recalled_by', 'orders.warehouse_dropoff',
                'orders.delivered_at', 'drivers.name', 'drivers.mobile', 'invOr.id as invoice_order_id', 'orders.order_number', 'orders.reference_number',
                'orders.r_government_id', 'orders.collected_cost', 'orders.delivery_status', 'orders.recalled_at', 'orders.rejected_at', 'orders.cancelled_at',
                'orders.overload', 'orders.receiver_name', 'payment_methods.name as payment_name', 'payment_method_id', 'invOr.captain_bonus', 'invOr.fees', 'invOr.f_fees', 'invOr.e_fees',
                DB::raw("(SELECT count(*) from invoice_orders as invOrr inner join invoices as inv on invOrr.invoice_id = inv.id where invOr.accepted_order_id = invOrr.accepted_order_id and inv.type = 1 and inv.collected = 1) as paid"))
            ->where('invoices.id', '=', $id);

        if ($customer->is_manager) {
            $AcceptedOrders->join('customers', 'customers.id', 'orders.customer_id')
                ->where('corporate_id', $customer->corporate_id);
        } else {
            $AcceptedOrders->where('customer_id', $customer->id);
        }

        if ($request->scan_collection_id) {
            $AcceptedOrders->join('scan_collection_orders', 'orders.id', '=', 'scan_collection_orders.order_id')
                ->where('scan_collection_orders.collection_id', $request->scan_collection_id);
        }


        if ($request->status) {
            if (in_array($request->status, [2, 3, 5, 8])) {
                $AcceptedOrders->where('orders.status', $request->status);
            } elseif ($request->status == 4) {
                $AcceptedOrders->where('orders.status', '4')
                    ->where(function ($query) {
                        $query->whereNUll('status_before_cancel')
                            ->orWhere('status_before_cancel', 0);
                    });
            }

        } else {
            $AcceptedOrders->where(function ($query) use ($invoice) {
                if ($invoice->type == 2) {
                    $query->whereIn('orders.status', [3, 8, 5])
                        ->orWhere(function ($query) {
                            $query->where('orders.status', '4')
                                ->where(function ($query) {
                                    $query->whereNUll('status_before_cancel')
                                        ->orWhere('status_before_cancel', 0);
                                });
                        });
                } else {
                    $query->whereIn('orders.status', [2, 3, 8, 5]);
                }
            });
        }

        if ($request->created_at) {
            $AcceptedOrders->whereDate('orders.created_at', '<=', $request->created_at);
        }

        if ($request->customer_id) {
            $AcceptedOrders->whereDate('orders.customer_id', $request->customer_id);
        }

        if ($request->last_update_date) {
            $AcceptedOrders->where(function ($query) use ($request, $invoice) {
                $query->whereDate('orders.delivered_at', $request->last_update_date)
                    ->orWhereDate('orders.recalled_at', $request->last_update_date)
                    ->orWhereDate('orders.rejected_at', $request->last_update_date);
                if ($invoice->type == 2) {
                    $query->orWhereDate('orders.cancelled_at', $request->last_update_date);
                }
            });
        }

        if ($request->paid == 1) {
            $AcceptedOrders->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('invoice_orders as invOrr')
                    ->join('invoices', 'invOrr.invoice_id', '=', 'invoices.id')
                    ->whereRaw('invOr.accepted_order_id = invOrr.accepted_order_id')
                    ->where('invoices.type', '=', 1)
                    ->where('invoices.collected', '=', 1);
            });
        } elseif ($request->paid == 2) {
            $AcceptedOrders->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('invoice_orders as invOrr')
                    ->join('invoices', 'invOrr.invoice_id', '=', 'invoices.id')
                    ->whereRaw('invOr.accepted_order_id = invOrr.accepted_order_id')
                    ->where('invoices.type', '=', 1)
                    ->where('invoices.collected', '=', 0);
            });
        }

        if ($request->filled('arrange')) {
            $AcceptedOrders->orderByRaw("FIELD(orders.status, 3,8,5,2)");
        }

        $AcceptedOrders->groupBy('invOr.accepted_order_id');

        if ($request->filled('limit')) {
            if ($request->limit != 'all') {
                $AcceptedOrders->limit($request->limit);
            }
        } else {
            $AcceptedOrders->limit(100);
        }

        $AllAcceptedPaginatedOrders = $AcceptedOrders->get();

        $customers = '';

        if ($invoice->type == 2) {
            $customers = Customer::where('corporate_id', $invoice->object_id)->get();
        }

        return view('frontend.profile.invoices.corporate_view',
            compact('invoice', 'AllAcceptedPaginatedOrders', 'pickups', 'customers'));


    } // end function show

    public function printInvoice($id, Request $request)
    {

        $user = Auth::guard('Customer')->user();

        $invoice = Invoice::findOrFail($id);

        $pickups = PickUp::join('invoice_pickups', 'pick_ups.id', 'invoice_pickups.pickup_id')
            ->join('drivers', 'drivers.id', 'pick_ups.driver_id')
            ->whereIn('pick_ups.status', [2, 3])
            ->where('invoice_pickups.invoice_id', $id)
            ->select('pick_ups.id', 'pick_ups.pickup_number', 'pick_ups.pickup_number', 'drivers.name',
                'pick_ups.status', 'pick_ups.created_at', 'pick_ups.received_at', 'pick_ups.delivered_at', 'pick_ups.bonus_per_order',
                'pick_ups.delivery_price', 'invoice_pickups.captain_bonus', 'invoice_pickups.pickup_price', 'pick_ups.pickup_orders')
            ->withCount('pickup_orders');

        if ($user->is_manager) {
            $pickups->where('corporate_id', $user->corporate_id);
        } else {
            $pickups->where('customer_id', $user->id);
        }

        $pickups = $pickups->get();

        // this all orders that the driver has approved
        $AcceptedOrders = Order::with(['customer' => function ($query) {
            $query->with('Corporate');
        }, 'to_government'])->join('accepted_orders', 'orders.id', '=', 'accepted_orders.order_id')
            ->join('drivers', 'accepted_orders.driver_id', '=', 'drivers.id')
            ->join('invoice_orders as invOr', 'accepted_orders.id', '=', 'invOr.accepted_order_id')
            ->join('invoices', 'invOr.invoice_id', '=', 'invoices.id')
            ->join('payment_methods', 'payment_methods.id', '=', 'orders.payment_method_id')
            ->select('orders.id', 'orders.type', 'orders.created_at', 'orders.order_price', 'orders.payment_method_id', 'orders.customer_id',
                'orders.status', 'accepted_orders.id as accepted_id', 'drivers.bouns_of_delivery as driver_bonus',
                'orders.delivery_price', 'orders.overweight_cost', 'drivers.recall_price as recall_price', 'drivers.reject_price as reject_price',
                'orders.order_price as price', 'orders.received_at', 'orders.recalled_by', 'orders.warehouse_dropoff',
                'orders.delivered_at', 'drivers.name', 'drivers.mobile', 'invOr.id as invoice_order_id', 'orders.order_number', 'orders.reference_number',
                'orders.r_government_id', 'orders.collected_cost', 'orders.delivery_status', 'orders.recalled_at', 'orders.rejected_at', 'orders.cancelled_at',
                'orders.overload', 'orders.receiver_name', 'payment_methods.name as payment_name', 'payment_method_id', 'invOr.captain_bonus', 'invOr.fees', 'invOr.f_fees', 'invOr.e_fees',
                DB::raw("(SELECT count(*) from invoice_orders as invOrr inner join invoices as inv on invOrr.invoice_id = inv.id where invOr.accepted_order_id = invOrr.accepted_order_id and inv.type = 1 and inv.collected = 1) as paid"))
            ->where('invoices.id', '=', $id);

        if ($user->is_manager) {
            $AcceptedOrders->join('customers', 'customers.id', 'orders.customer_id')
                ->where('corporate_id', $user->corporate_id);
        } else {
            $AcceptedOrders->where('customer_id', $user->id);
        }

        if ($request->status) {
            if (in_array($request->status, [2, 3, 5, 8])) {
                $AcceptedOrders->where('orders.status', $request->status);
            } elseif ($request->status == 4) {
                $AcceptedOrders->where('orders.status', '4')
                    ->where(function ($query) {
                        $query->whereNUll('status_before_cancel')
                            ->orWhere('status_before_cancel', 0);
                    });
            }

        } else {
            $AcceptedOrders->where(function ($query) use ($invoice) {
                if ($invoice->type == 2) {
                    $query->whereIn('orders.status', [3, 8, 5])
                        ->orWhere(function ($query) {
                            $query->where('orders.status', '4')
                                ->where(function ($query) {
                                    $query->whereNUll('status_before_cancel')
                                        ->orWhere('status_before_cancel', 0);
                                });
                        });
                } else {
                    $query->whereIn('orders.status', [2, 3, 8, 5]);
                }
            });
        }

        if ($request->created_at) {
            $AcceptedOrders->whereDate('orders.created_at', '<=', $request->created_at);
        }

        if ($request->customer_id) {
            $AcceptedOrders->whereDate('orders.customer_id', $request->customer_id);
        }

        if ($request->last_update_date) {
            $AcceptedOrders->where(function ($query) use ($request, $invoice) {
                $query->whereDate('orders.delivered_at', $request->last_update_date)
                    ->orWhereDate('orders.recalled_at', $request->last_update_date)
                    ->orWhereDate('orders.rejected_at', $request->last_update_date);
                if ($invoice->type == 2) {
                    $query->orWhereDate('orders.cancelled_at', $request->last_update_date);
                }
            });
        }

        if ($request->paid == 1) {
            $AcceptedOrders->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('invoice_orders as invOrr')
                    ->join('invoices', 'invOrr.invoice_id', '=', 'invoices.id')
                    ->whereRaw('invOr.accepted_order_id = invOrr.accepted_order_id')
                    ->where('invoices.type', '=', 1)
                    ->where('invoices.collected', '=', 1);
            });
        } elseif ($request->paid == 2) {
            $AcceptedOrders->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('invoice_orders as invOrr')
                    ->join('invoices', 'invOrr.invoice_id', '=', 'invoices.id')
                    ->whereRaw('invOr.accepted_order_id = invOrr.accepted_order_id')
                    ->where('invoices.type', '=', 1)
                    ->where('invoices.collected', '=', 0);
            });
        }

        if ($request->filled('arrange')) {
            $AcceptedOrders->orderByRaw("FIELD(orders.status, 3,8,5,2)");
        }

        $AcceptedOrders->groupBy('invOr.accepted_order_id');

        if ($request->filled('limit')) {
            if ($request->limit != 'all') {
                $AcceptedOrders->limit($request->limit);
            }
        } else {
            $AcceptedOrders->limit(100);
        }

        $AllAcceptedPaginatedOrders = $AcceptedOrders->get();

        return view('frontend.profile.invoices.corporate_printInvoice',
            compact('AllAcceptedPaginatedOrders', 'pickups', 'invoice', 'user'));

    }

    public function indexRefund(Request $request)
    {

        $user = Auth::guard('Customer')->user();
        $search = app('request')->input('search');

        $collections = RefundCollection::with(['customer' => function ($query) {
            $query->with(['corporate']);
        }])
            ->join('customers', 'customers.id', 'refund_collections.customer_id')
            ->whereNotNull('end_date')
            ->select('refund_collections.*')
            ->withCount('orders')
            ->orderBy('refund_collections.id', 'desc');

        $start = app('request')->input('start');
        $end = app('request')->input('end');

        $collections = $collections->when($start, function ($query, $start) {
            return $query->where('refund_collections.created_at', '>=', $start);
        })->when($end, function ($query, $end) {
            return $query->where('refund_collections.created_at', '<=', $end);
        });

        if (isset($search) && $search != "") {
            $collections = $collections->join('orders', 'orders.refund_collection_id', '=', 'refund_collections.id')
                ->join('corporates', 'corporates.id', '=', 'customers.corporate_id')
                ->where(function ($q) use ($search) {
                    $q->where('refund_collections.serial_code', 'like', '%' . $search . '%');
                    $q->OrWhere('orders.order_number', 'like', '%' . $search . '%');
                    $q->OrWhere('orders.order_no', 'like', '%' . $search . '%');
                    $q->OrWhere('customers.name', 'like', '%' . $search . '%');
                    $q->OrWhere('corporates.name', 'like', '%' . $search . '%');
                });
        }

        if ($user->is_manager) {
            $collections->where('corporate_id', $user->corporate_id);
        } else {
            $collections->where('refund_collections.customer_id', $user->id);
        }

        $collections = $collections->distinct()->paginate(50);

        if ($request->ajax()) {
            return [
                'view' => view('frontend.profile.refund_collections.table', compact('collections'))->render(),
            ];
        }

        return view('frontend.profile.refund_collections.index', compact('collections'));

    }

}

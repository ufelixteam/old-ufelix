@extends('agent.layouts.app')
@section('css')

@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i>  Edit ShipmentToolOrders #{{$shipment_tool_order->id}}</h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <form action="{{ route('mngrAgent.shipment_tool_orders.update', $shipment_tool_order->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="status" value="{{$shipment_tool_order->status}}">
                <input type="hidden" name="agent_id" value="{{Auth::guard('agent')->user()->agent_id}}">
              
                   
                  <div id="fields">
                    
                    @if( ! empty($shipment_tool_order->items) && count($shipment_tool_order->items) >0)

                    @php
                      $i = 0;
                    @endphp
                      @foreach($shipment_tool_order->items as $item)
                    <div class="col-sm-12 col-md-12"  style="margin-top: 10px;"> 

                      <div class="col-sm-5 col-md-5">
                        <input required  type="number"  name="quantity[]" class="form-control" placeholder="Quantity" value="{{$item->quantity}}"  />
                      </div>

                      <div class="form-group col-md-6 col-sm-6 @if($errors->has('items')) has-error @endif">           
                           <select id="items-field" name="items[]" id="items" class="form-control">
                                
                                @if(! empty($shipment_tools))
                                    @foreach($shipment_tools as $item)
                                        <option value="{{$item->id}}" {{ old("items") == $item->id || $item->tool_id == $item->id ? 'selected' : '' }}
                                        >{{$item->title}}</option>

                                    @endforeach

                                @endif
                            </select>
                           
                        </div>

                        @if($i == 0)
                        <div class="input-group">

                          <span class="input-group-btn">
                              <input type="button" id="add-field" value="+" class="btn btn-success add-more">
                          </span>
                      </div>

                      @else

                      <div class="input-group">

                        <span class="input-group-btn">
                            <input type="button" id="remove-field" value="-" class="btn btn-danger remove">
                        </span>
                    </div>
                      @endif
                    </div>

                    @php
                      $i ++ ;
                    @endphp
                      @endforeach
                    @else

                    <div id="fields">
                    <div class="col-sm-12 col-md-12"  style="margin-top: 10px;"> 

                      <div class="col-sm-5 col-md-5">
                        <input required  type="number"  name="quantity[]" class="form-control" placeholder="Quantity"  />
                      </div>

                      <div class="form-group col-md-6 col-sm-6 @if($errors->has('items')) has-error @endif">           
                           <select id="items-field" name="items[]" id="items" class="form-control">
                                
                                @if(! empty($shipment_tools))
                                    @foreach($shipment_tools as $item)
                                        <option value="{{$item->id}}" {{ old("items") == $item->id ? 'selected' : '' }}
                                        >{{$item->title}}</option>

                                    @endforeach

                                @endif
                            </select>
                           
                        </div>

                        <div class="input-group">

                          <span class="input-group-btn">
                              <input type="button" id="add-field" value="+" class="btn btn-success add-more">
                          </span>
                      </div>

                    </div>

                  </div>
                    @endif
                    </div>


                     <div class="form-group col-md-12 col-sm-12 @if($errors->has('notes')) has-error @endif">
                       <label for="notes-field">Notes</label>
                    <textarea class="form-control" id="notes-field" rows="4" name="notes">{{ is_null(old("notes")) ? $shipment_tool_order->notes : old("notes") }}</textarea>
                       @if($errors->has("notes"))
                        <span class="help-block">{{ $errors->first("notes") }}</span>
                       @endif
                    </div>
                <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAgent.shipment_tool_orders.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
    $(function(){
      $("body").on("click", ".remove", function () {
        $(this).closest(".remove-field").remove();

      });

      $(".add-more").on('click',function(){
        $.ajax({
            url: "{{url('/mngrAgent/new_shippeng_field/')}}" ,
            type: 'get',

            success: function (data) {
                $('#fields').append(data.view);

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
        


      });
    });
  </script>
@endsection

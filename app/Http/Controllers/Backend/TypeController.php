<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\VehicleType;
use App\Models\OrderType;
use Response;
use Flash;
use File;
use Auth;

class TypeController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		$types = OrderType::orderBy('id', 'desc')->paginate(50);
		return view('backend.types.index', compact('types'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {
		return view('backend.types.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request) {
		$type = new OrderType();
		$type->name_en = $request->input("name_en");
		$type->name_ar = $request->input("name_ar");
    $type->status = $request->input("status");
		$type->save();
    Flash::success(__('backend.Type_saved_successfully'));
		return redirect()->route('mngrAdmin.types.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		$type = OrderType::findOrFail($id);
		return view('backend.types.show', compact('type'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		$type = OrderType::findOrFail($id);
		return view('backend.types.edit', compact('type'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id) {
		$type = OrderType::findOrFail($id);
		$type->name = $request->input("name_en");
    $type->status = $request->input("status");
		$type->save();
    Flash::warning(__('backend.Type_Updated_successfully'));
		return redirect()->route('mngrAdmin.types.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		$type = OrderType::findOrFail($id);
		$type->delete();
    flash(__('backend.Type_deleted_successfully'))->error();
		return redirect()->route('mngrAdmin.types.index');
	}
}

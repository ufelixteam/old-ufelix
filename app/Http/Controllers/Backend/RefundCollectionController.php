<?php

namespace App\Http\Controllers\Backend;

use App\Events\OrderUpdated;
use App\Http\Controllers\Controller\Backend;
use App\Http\Requests;
use App\Models\Order;
use App\Models\RefundCollection;
use App\Models\RefundCollectionOrder;
use Auth;
use Carbon\Carbon;
use DB;
use Excel;
use File;
use Flash;
use Illuminate\Http\Request;
use Response;


class RefundCollectionController extends Controller
{

    public function __construct(Request $request)
    {
        if ($request->is('mngrAdmin/*') || $request->is('admin/*')) {
            \Config::set('auth.defaults.guard', 'admin');

        } elseif ($request->is('mngrAgent/*') || $request->is('agent/*')) {
            \Config::set('auth.defaults.guard', 'agent');
        }
        $this->guardType = \Config::get('auth.defaults.guard');
    }

    public function index(Request $request)
    {

        $user = Auth::guard($this->guardType)->user();
        $search = app('request')->input('search');

        $collections = RefundCollection::with(['customer' => function ($query) {
            $query->with(['corporate']);
        }])
            ->select('refund_collections.*')
            ->withCount('orders')
            ->orderBy('refund_collections.id', 'desc');

        $start = app('request')->input('start');
        $end = app('request')->input('end');
        $confirmed = app('request')->input('confirmed');

        $collections = $collections->when($start, function ($query, $start) {
            return $query->where('created_at', '>=', $start);
        })->when($end, function ($query, $end) {
            return $query->where('created_at', '<=', $end);
        })->when($confirmed, function ($query, $confirmed) {
            if ($confirmed == 2) {
                return $query->whereHas('orders', function ($query) {
                    $query->where('is_refund', 0);
                }, '=', 0);
            } else {
                return $query->whereHas('orders', function ($query) {
                    $query->where('is_refund', 0);
                }, '>', 0);
            }
        });

        if (isset($search) && $search != "") {
            $collections = $collections->join('orders', 'orders.refund_collection_id', '=', 'refund_collections.id')
                ->join('customers', 'customers.id', '=', 'refund_collections.customer_id')
                ->join('corporates', 'corporates.id', '=', 'customers.corporate_id')
                ->where(function ($q) use ($search) {
                    $q->where('refund_collections.serial_code', 'like', '%' . $search . '%');
                    $q->OrWhere('orders.order_number', 'like', '%' . $search . '%');
                    $q->OrWhere('orders.order_no', 'like', '%' . $search . '%');
                    $q->OrWhere('customers.name', 'like', '%' . $search . '%');
                    $q->OrWhere('corporates.name', 'like', '%' . $search . '%');
                });
        }

        if ($user->is_warehouse_user) {
            $collections = $collections->join('customers', 'customers.id', '=', 'refund_collections.customer_id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id);
        }

        $collections = $collections->distinct()->paginate(50);

        if ($request->ajax()) {
            return [
                'view' => view('backend.refund_collections.table', compact('collections'))->render(),
            ];
        }

        return view('backend.refund_collections.index', compact('collections'));

    }

    public function refund_orders(Request $request)
    {
        if ($request->ids) {
            $ids = $request->ids;
            Order::whereIn('id', $ids)
                ->where('refund_collection_id', $request->collection_id)->where('is_refund', 0)
                ->where('warehouse_dropoff', 1)
                ->where('client_dropoff', 1)
                ->where(function ($query) {
                    $query->where(function ($query) {
                        $query->where('status', 3)
                            ->where('delivery_status', 2);
                    })
                        ->orWhere(function ($query) {
                            $query->where('status', 3)
                                ->where('delivery_status', 4);
                        })
                        ->orWhereIn('status', [5, 8, 4]);
                })->update([
                    'is_refund' => 1,
                    'refund_at' => Carbon::now()
                ]);
        } elseif ($request->collection_id) {
            Order::where('refund_collection_id', $request->collection_id)->where('is_refund', 0)
                ->where('warehouse_dropoff', 1)
                ->where('client_dropoff', 1)
                ->where(function ($query) {
                    $query->where(function ($query) {
                        $query->where('status', 3)
                            ->where('delivery_status', 2);
                    })
                        ->orWhere(function ($query) {
                            $query->where('status', 3)
                                ->where('delivery_status', 4);
                        })
                        ->orWhereIn('status', [5, 8, 4]);
                })->update([
                'is_refund' => 1,
                'refund_at' => Carbon::now()
            ]);

            $ids = Order::where('refund_collection_id', $request->collection_id)->pluck('id')->toArray();
        }

        event(new OrderUpdated($ids,
            Auth::guard($this->guardType)->user()->id,
            1,
            'refund'
        ));

        Flash::success(__('backend.convert_pending_to_dropped'));

        if ($request->ajax()) {
            return __('backend.drop_off_package');
        }

        return redirect()->back();

    }/*refund orders*/
}

@extends('backend.layouts.app')
@section('css')
    <title>Orders</title>
    <style type="text/css">
        @media only screen and (max-width: 580px) {
            .hide-td{
                display: none;
            }
        }
        .box-body {
            min-height: 100px;
        }
        .stylish-input-group .input-group-addon{
            background: white !important;
        }
        .stylish-input-group .form-control{
            border-right:0;
            box-shadow:0 0 0;
            border-color:#ccc;
        }
        .stylish-input-group button{
            border:0;
            background:transparent;
        }

        @media print {
            a[href]:after {
                content: none !important;
            }

            #print {
                visibility: hidden;
            }
        }

    </style>
@endsection

@section('header')

    <div class="page-header clearfix">

        <h3 style="display: inline-block;">
            <i class="glyphicon glyphicon-align-justify"></i> Collection Orders
        </h3>

        <img class="img-thumbnail" style="width: 200px; height: 85px; float: right; border: none;" src="{{asset('public/backend/logo2.png')}}" alt="">

    </div>
@endsection
@section('content')

    <button id="print" class="btn btn-info pull-right"> Print </button>

    <div class='list'>
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                @if(! empty($orders) && count($orders) > 0)
                    <table id="theTable" class="table table-condensed table-striped text-center">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Receiver Name</th>
                            <th>Sender Name</th>
                            <th>From</th>
                            <th>To</th>
                            <th>Status</th>
                            <th style="font-weight: bold; color: #333;">Order Number</th>
                            <th style="font-weight: bold; color: #333;">Receiver Code</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td>{{$order->id}}</td>
                                <td>{{$order->receiver_name}}</td>
                                <td>{{$order->sender_name}}</td>
                                <td>
                                    @if(! empty($order->from_government)  && $order->from_government != null )
                                        <a style="color:#f5b404;font-weight: bold" target="_blank" href="{{URL::asset('/mngrAdmin/governorates/'.$order->s_government_id)}}" > {{$order->from_government->name_en}} </a>

                                    @endif

                                </td>


                                <td>
                                    @if(! empty($order->to_government)  && $order->from_government != null  ) <a style="color:#f5b404;font-weight: bold" target="_blank" href="{{URL::asset('/mngrAdmin/governorates/'.$order->r_government_id)}}" > {{$order->to_government->name_en}} </a>
                                    @endif

                                </td>

                                <td>
                                    @if($type == 'pending' )
                                        <span class='badge badge-pill badge-black'>Pending</span>
                                    @elseif($type == 'accept')
                                        <span class='badge badge-pill badge-info'>Accepted</span>
                                    @elseif($type == 'receive')
                                        <span class='badge badge-pill badge-warning'>O.F.D</span>
                                    @elseif($type == 'deliver')
                                        <span class='badge badge-pill badge-success'>Delivered</span>
                                    @elseif($type == 'cancel')
                                        <span class='badge badge-pill badge-danger'>Cancelled</span>
                                    @elseif($type == 'recall')
                                        <span class='badge badge-pill badge-danger'>Recalled</span>
                                    @elseif($type == 'reject')
                                        <span class='badge badge-pill badge-danger'>Rejected</span>
                                    @else
                                        {!! $order->status_span !!}
                                    @endif
                                </td>

                                <td style="font-weight: bold; color: #333;">{{$order->order_number}}</td>
{{--                                <td style="font-weight: bold; color: #333;">{{$order->receiver_code}}</td>--}}

                            </tr>

                        @endforeach
                        </tbody>
                    </table>
                @else
                    <h3 class="text-center alert alert-warning">No Result Found !</h3>
                @endif
            </div>
        </div>
        <div id="js-form" class="js-form">
        </div>

    </div>


@endsection
@section('scripts')
    <script>
        $(function(){

            $(function() {
                window.print();
                // window.history.back();
            });

            $(function() {
                document.querySelector("#print").addEventListener("click", function() {
                    window.print();
                    window.history.back();
                });
            });

        });
    </script>
@endsection

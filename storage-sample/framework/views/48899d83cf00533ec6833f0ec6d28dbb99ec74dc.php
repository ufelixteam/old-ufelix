
<?php $__env->startSection('css'); ?>
  <title><?php echo e(__('backend.the_orders')); ?> - <?php echo e(__('backend.view')); ?></title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">

<?php $__env->stopSection(); ?>
<?php $__env->startSection('header'); ?>
<div class="page-header">
  <h3><?php echo e(__('backend.Order_No')); ?> #<?php echo e($order->id); ?></h3>

    <?php echo $order->status_span; ?> Payment : <?php echo ! empty($order->payment_method ) ? '<span class="badge badge-pill label-success">'.$order->payment_method->name.'</span>' : ''; ?> Price:


    <?php if(! empty($order) && $order->payment_method_id == 1): ?>
        <?php echo e(! empty($order) ?  $order->delivery_price + ($order->overload * 5) : ''); ?>

        
    <?php elseif(! empty($order) && ($order->payment_method_id == 3 || $order->payment_method_id == 4 )): ?>
        <?php echo e(! empty($order) ? $order->order_price + $order->delivery_price + ($order->overload * 5): ''); ?>

    <?php elseif(! empty($order) && $order->payment_method_id == 6): ?>
        <?php echo e(! empty($order) ? $order->order_price: ''); ?>

    <?php elseif(! empty($order) && $order->payment_method_id == 5): ?>
        0
    <?php endif; ?>

    <br>
    <br>
    <span class="text-success"> Order Price: <?php echo e(! empty($order) ? $order->order_price: ''); ?> </span> <br>
    <span class="text-primary"> Delivery Price: <?php echo e(! empty($order) ? $order->delivery_price: ''); ?> </span><br>

    <div class="dropdown  pull-right">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
        </button>
        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
            <?php if( $order->status != 3    ): ?>
                <li>
                    <a href="<?php echo e(route('mngrAdmin.orders.edit', $order->id)); ?>"><i class="fa fa-edit"></i> <?php echo e(__('backend.edit')); ?></a>
                </li>
            <?php endif; ?>
            <li>
                <a href= "<?php echo e(URL::asset('/mngrAdmin/print_pdf/'.$order->id)); ?>" target="_blank"  data-id="<?php echo e($order->id); ?>"><i class="fa fa-print"></i> <?php echo e(__('backend.pdf')); ?></a>
            </li>
            <?php if($order->status == 2 && isset($order->accepted->status) && $order->accepted->status != 4): ?>
                <li>
                    <a  href="<?php echo e(route('mngrAdmin.orders.cancelOrder', $order->id)); ?>"><i class="fa fa-remove"></i>  <?php echo e(__('backend.cancelled')); ?></a>
                </li>
            <?php endif; ?>
            <?php if($order->status == 0 ): ?>
                <li>
                    <a id="forward_agent" data-toggle="modal" data-target="#forward_agentMo"><i class="fa fa-user-secret" aria-hidden="true"></i> <?php echo e(__('backend.Change_Agent')); ?></a>
                </li>
            <?php endif; ?>
            <?php if( ($order->status < 3  || $order->status == 6 ) && $order->is_pickup == "0" ): ?>
                <li>
                    <a id="forward"  data-toggle="modal" data-target="#exampleModal"> <i class="fa fa-share" aria-hidden="true"></i> <?php if($order->status != 0 && $order->status != 6 ): ?>  <?php echo e(__('backend.Cancel_and')); ?> <?php else: ?> <?php echo e(__('backend.forward')); ?> <?php endif; ?></a>
                </li>
            <?php endif; ?>
            <?php if($order->status != 3 && $order->status != 4  && $order->is_pickup == "0" ): ?>
                <li>
                    <a id="cancelOrderClient"  data-id="<?php echo e($order->id); ?>"><i class="fa fa-times" aria-hidden="true"></i><?php echo e(__('backend.Cancel_Order_Client_Required')); ?></a>
                </li>
            <?php endif; ?>

            <?php if($order->status != 3 && $order->status != 4  && $order->is_pickup == "0" ): ?>
                <li>
                    <a id="cancelOrderByAdmin"  data-id="<?php echo e($order->id); ?>"><i class="fa fa-times" aria-hidden="true"></i><?php echo e(__('backend.Cancel_Order_Admin_Required')); ?></a>
                </li>
            <?php endif; ?>

            <?php if($order->status == 5 ): ?>
            <li>
                <a href="<?php echo e(url('/mngrAdmin/forward_recall/'.$order->id)); ?>"><i class="fa fa-share"></i> <?php echo e(__('backend.edit_forward')); ?></a>
            </li>
            <?php endif; ?>

            <?php if($order->status == 2 && isset($order->accepted->status) && $order->accepted->status != 4): ?>
                <li>
                    <a data-toggle="modal" data-target="#recallModal" >
                        <i class="fa fa-history"></i>  <?php echo e(__('backend.recall')); ?></a>
                </li>
            <?php endif; ?>
                
                
                    
                        
                        
                        
                        
                        
                    
                
            <?php if($order->status == 4 || $order->status == 6 || $order->status == 0): ?>

            <li>
                <a data-id="<?php echo e($order->id); ?>" id="delete-order"><i class="fa fa-trash"></i> <?php echo e(__('backend.delete')); ?></a>
            </li>
            <?php endif; ?>

        </ul>
    </div>

</div>

<div class="row" style="padding: 12px 20px;">
        <div class="alert alert-danger" style="display:none"></div>
  <!-- Create at: -->
  <div class="form-group col-sm-3">
   <label for="receiver_name"><?php echo e(__('backend.Create_at')); ?>: </label>
   <p class="form-control-static" style="color: #0376d9; font-weight: bold;" ><?php if($order->created_at): ?><?php echo e($order->created_at); ?> <?php else: ?> <?php echo e(__('backend.No_Date')); ?> <?php endif; ?> </p>
  </div>

  <!-- Accept at: -->
  <div class="form-group col-sm-3">
    <label for="reciever_mobile"> <?php echo e(__('backend.Accept_at')); ?>: </label>
    <p class="form-control-static" style="color: #0376d9; font-weight: bold;" >


      <?php if( $order->status != 0 ): ?>
       <?php echo e($order->accepted_at); ?> 
      <?php else: ?> <?php echo e(__('backend.No_Date')); ?> <?php endif; ?>
      </p>
  </div>

  <!-- Receiver at: -->
  <div class="form-group col-sm-3">
   <label for="receiver_name"><?php echo e(__('backend.Received_at')); ?>: </label>
   <p class="form-control-static" style="color: #0376d9; font-weight: bold;" >
     <?php if( $order->status != 0  ): ?>
        
         <?php echo e($order->received_at); ?>

     
      <?php else: ?> <?php echo e(__('backend.No_Date')); ?>  <?php endif; ?>
    </p>
  </div>

  <!-- Deliver at: -->
  <div class="form-group col-sm-2">
    <label for="reciever_mobile"> <?php echo e(__('backend.Deliver_at')); ?>: </label>
    <p class="form-control-static" style="color: #0376d9; font-weight: bold;" >
      <?php if($order->status != 0 ): ?>
        <?php echo e($order->delivered_at); ?> 
      <?php else: ?> <?php echo e(__('backend.No_Date')); ?> <?php endif; ?>
    </p>
  </div>


</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="row">
  <div class="col-md-12 col-xs-12 col-sm-12">
    <div class="col-sm-12" style="border: 2px solid #607d8b;color: #fff;margin-bottom: 20px;background-color: #607d8b;" >
      <h4><?php echo e(__('backend.Receiver_Data')); ?></h4>
    </div>
      <div class="form-group col-sm-3">
       <label for="receiver_name"><?php echo e(__('backend.receiver_name')); ?>: </label>
       <p class="form-control-static"><?php echo e($order->receiver_name); ?></p>
      </div>
      <div class="form-group col-sm-3">
        <label for="reciever_mobile"><?php echo e(__('backend.mobile_number')); ?>: </label>
        <p class="form-control-static"><?php echo e($order->receiver_mobile); ?></p>
      </div>
      <div class="form-group col-sm-3">
         <label for="reciever_phone"><?php echo e(__('backend.mobile_number2')); ?>: </label>
         <p class="form-control-static"><?php echo e($order->receiver_phone); ?></p>
      </div>
      <div class="form-group col-sm-3">
         <label for="reciever_address"><?php echo e(__('backend.Receiver_Address')); ?>:</label>
         <p class="form-control-static"><?php echo e($order->receiver_address); ?></p>
      </div>
      <div class="form-group col-sm-12">
        <div class="row">
          <div class="col-sm-12"><h4><strong><?php echo e(__('backend.Customer_Account')); ?></strong></h4></div>
          <div class="col-sm-3"><strong><?php echo e(__('backend.name')); ?>: </strong><?php echo e(! empty($order->customer) ? $order->customer->name : ''); ?></div>
          <div class="col-sm-3"><strong><?php echo e(__('backend.email')); ?>: </strong><?php echo e(! empty($order->customer) ? $order->customer->email : ''); ?></div>
          <div class="col-sm-3"><strong><?php echo e(__('backend.mobile_number')); ?>: </strong><?php echo e(! empty($order->customer) ? $order->customer->mobile : ''); ?></div>
        </div>
      </div>

    <div class="col-sm-12" style="border: 2px solid #607d8b;color: #fff;margin-bottom: 20px;background-color: #607d8b;" >
        <h4><?php echo e(__('backend.Sender_Data')); ?></h4>
    </div>
      <div class="form-group col-sm-3">
        <label for="sender_name"><?php echo e(__('backend.sender_name')); ?>: </label>
        <p class="form-control-static"><?php echo e($order->sender_name); ?></p>
      </div>
      <div class="form-group col-sm-3">
        <label for="sender_mobile"><?php echo e(__('backend.mobile_number')); ?>: </label>
        <p class="form-control-static"><?php echo e($order->sender_mobile); ?></p>
      </div>
      <div class="form-group col-sm-3">
        <label for="sender_phone"><?php echo e(__('backend.mobile_number2')); ?>:</label>
        <p class="form-control-static"><?php echo e($order->sender_phone); ?></p>
      </div>
      <div class="form-group col-sm-3">
        <label for="sender_address"><?php echo e(__('backend.Sender_Address')); ?>: </label>
        <p class="form-control-static"><?php echo e($order->sender_address); ?></p>
      </div>

    <div class="col-sm-12" style="border: 2px solid #607d8b;color: #fff;margin-bottom: 20px;background-color: #607d8b;" >
    <h4><?php echo e(__('backend.Package_Data')); ?></h4>
    </div>

    <div class="form-group col-sm-3">
        <label for="notes"><?php echo e(__('backend.Image')); ?>: </label>
        <p class="form-control-static"><img src="<?php echo e(! empty($order->image) ? $order->image : '#'); ?> "></p>
      </div>
      <div class="form-group col-sm-3">
        <label for="type_id"><?php echo e(__('backend.order_type')); ?>: </label>
        <p class="form-control-static"><?php echo e(! empty($order->type) ? $order->type : ''); ?></p>
      </div>
       <div class="form-group col-sm-3">
         <label for="order_number"><?php echo e(__('backend.order_number')); ?>: </label>
         <p class="form-control-static"><?php echo e($order->order_number); ?></p>
      </div>
      <div class="form-group col-sm-3">
        <label for="notes"><?php echo e(__('backend.notes')); ?>: </label>
        <p class="form-control-static"><?php echo e(! empty($order->notes) ? $order->notes : '-'); ?></p>
      </div>
      <div class="form-group col-sm-3">
        <label for="order_number"><?php echo e(__('backend.receiver_code')); ?>: </label>
        <p class="form-control-static"><?php echo e($order->receiver_code); ?></p>
     </div>

     

    <div class="col-sm-12" style="border: 2px solid #607d8b;color: #fff;margin-bottom: 20px;background-color: #607d8b;" >
    <h4><?php echo e(__('backend.Driver_Data')); ?></h4>
    </div>
    <?php if(! empty($order->accepted ) && ! empty($order->accepted->driver)): ?>
      <div class="form-group col-sm-3">
        <label for="sender_name"><?php echo e(__('backend.Driver_Name')); ?>: </label>
        <p class="form-control-static"><?php echo e(! empty($order->accepted ) && ! empty($order->accepted->driver) ? $order->accepted->driver->name : ''); ?></p>
      </div>
      <div class="form-group col-sm-3">
        <label for="sender_mobile"><?php echo e(__('backend.mobile_number')); ?>: </label>
        <p class="form-control-static"><?php echo e(! empty($order->accepted ) && ! empty($order->accepted->driver) ? $order->accepted->driver->mobile : ''); ?></p>
      </div>
      <div class="form-group col-sm-3">
        <label for="email"><?php echo e(__('backend.email')); ?>: </label>
        <p class="form-control-static"><?php echo e(! empty($order->accepted ) && ! empty($order->accepted->driver) ? $order->accepted->driver->email : ''); ?></p>
      </div>
      <div class="form-group col-sm-3">
        <label for="sender_address"><?php echo e(__('backend.Driver_Address')); ?>: </label>
        <p class="form-control-static"><?php echo e(! empty($order->accepted ) && ! empty($order->accepted->driver) ? $order->accepted->driver->address : ''); ?></p>
      </div>
      <?php else: ?>
      <div class="form-group col-sm-4 col-sm-offset-4 text-center">
        <div class="alert alert-info" style="padding-bottom: 3px;">
          <h4><?php echo e(__('backend.Order_is_not_Accepted_yet')); ?></h4>
        </div>
      </div>
      <?php endif; ?>
      <div class="col-md-12 col-xs-12 col-sm-12">
        <a class="btn btn-link pull-right" href="<?php echo e(URL::previous()); ?>"><i class="fa fa-backward"></i> <?php echo e(__('backend.back')); ?></a>

      </div>
  </div>
</div>

<?php echo $__env->make('backend.orders.recall_modal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('backend.orders.driver_forward_modal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('backend.orders.agent_forward_modal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

<script>

/*Cancel-order*/
$("#cancelOrderByAdmin").on('click',function(){
    var id = $(this).attr('data-id');
    $.confirm({
        title     : '',
        theme     : 'modern' ,
        class     :  'danger',
        content   : '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Confirm Cancel</span></div><div style="font-weight:bold;"><p>Are You want To Cancel This Order ? According To Admin Wanted</p></div>',
        buttons: {
            confirm: {
                text: 'Cancel Order',
                btnClass: 'btn-blue',
                action: function () {
                    $.ajax({
                        url: "<?php echo e(url('/mngrAdmin/cancel_order_admin')); ?>",
                        type: 'post',
                        data: {'_token':"<?php echo e(csrf_token()); ?>",'id':id},
                        success: function(data) {
                            jQuery('.alert-danger').html('');
                            jQuery('.alert-danger').show();
                            if(data != 'false'){
                                jQuery('.alert-danger').append('<p>'+data+'</p>');
                                setTimeout(function() {
                                    location.reload();
                                }, 2000);
                            }else{
                                jQuery('.alert-danger').append('<p>Something Error</p>');
                            }
                            window.scrollTo({ top: 0, behavior: 'smooth' });
                        },
                        error: function(data) {
                            console.log('Error:', data);
                        }
                    });
                }
            },
            cancel: {
                text: 'Cancel',
                action: function () {
                }
            }
        }
    });
});
  $("#cancelOrderClient").on('click',function(){
    var id = $(this).attr('data-id');
      $.confirm({
            title     : '',
            theme     : 'modern' ,
            class     :  'danger',
            content   : '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Confirm Cancel</span></div><div style="font-weight:bold;"><p>Are You want To Cancel This Order ? According To Client Wanted</p></div>',
            buttons: {
                confirm: {
                    text: 'Cancel Order',
                    btnClass: 'btn-blue',
                    action: function () {
                        $.ajax({
                          url: "<?php echo e(url('/mngrAdmin/cancel_order_client')); ?>",
                          type: 'post',
                          data: {'_token':"<?php echo e(csrf_token()); ?>",'id':id},
                          success: function(data) {
                                jQuery('.alert-danger').html('');
                                jQuery('.alert-danger').show();
                                if(data != 'false'){
                                    jQuery('.alert-danger').append('<p>'+data+'</p>');
                                    setTimeout(function() {
                                        location.reload();
                                    }, 2000);
                                }else{
                                  jQuery('.alert-danger').append('<p>Something Error</p>');
                                }
                                window.scrollTo({ top: 0, behavior: 'smooth' });
                          },
                          error: function(data) {
                            console.log('Error:', data);
                          }
                        });
                    }
                },
                cancel: {
                    text: 'Cancel',
                    action: function () {
                    }
                }
            }
      });
    });

$(function () {
    $("#recall_by").on('change',function () {
        if ($(this).val() >1){
            $('#password_div').hide();
            $('#password').attr('readonly',true);
            $('#code_div').show();
            $('#code').attr('readonly',false);
            $("#send_div").show();
        }else{
            $('#password_div').show();
            $('#password').attr('readonly',false);

            $('#code_div').hide();
            $('#code').attr('readonly',true);
            $("#send_div").hide();
        }
    });
    $("#send_sms").on('click',function () {
        $.ajax({
            url: "<?php echo e(url('/mngrAdmin/send_cofirm_recall_sms')); ?>",
            type: 'post',
            data:  $("#recallForm").serialize() ,
            success: function(data) {
                jQuery('.danger').html('');
                jQuery('.danger').show();
                jQuery('.success').html('');
                jQuery('.success').show();
                console.log(data)
                if(data != 'false'){
                    jQuery('.success').append('<p>Sent Succefully</p>');
                    // setTimeout(function() {
                    //     location.reload();
                    // }, 2000);
                }else{
                    jQuery('.danger').append('<p>Something Error</p>');
                }
            },
            error: function(data) {
                console.log('Error:', data);
            }
        });
    });
    $("#send_recall").on('click',function () {
        $.ajax({
            url: "<?php echo e(url('/mngrAdmin/orders/recallOrder')); ?>",
            type: 'post',
            data:  $("#recallForm").serialize() ,
            success: function(data) {
                jQuery('.danger').html('');
                jQuery('.danger').show();
                jQuery('.success').html('');
                jQuery('.success').show();
                console.log(data)
                if(data != 'false'){
                    jQuery('.success').append('<p>Sent Succefully</p>');
                    // setTimeout(function() {
                    //     location.reload();
                    // }, 2000);
                }else{
                    jQuery('.danger').append('<p>Something Error</p>');
                }
            },
            error: function(data) {
                console.log('Error:', data);
            }
        });
    });
    $("#delete-order").on('click',function(){
      var id = $(this).attr('data-id');
      $.confirm({
            title     : '',
            theme     : 'modern' ,
            class     :  'danger',
            content   : '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Confirm Delete</span></div><div style="font-weight:bold;"><p>Are You sure To Delete This Order ?</p></div>',
            buttons: {
                confirm: {
                    text: 'Delete Order',
                    btnClass: 'btn-danger',
                    action: function () {
                        $.ajax({
                          url: "<?php echo e(url('/mngrAdmin/orders/')); ?>"+"/"+id,
                          type: 'DELETE',
                          data: {'_token':"<?php echo e(csrf_token()); ?>"},
                          success: function(data) {
                      
                                if(data != 'false'){
                         
                                    setTimeout(function() {
                                        location.reload();
                                        window.location.href = "<?php echo e(url('/mngrAdmin/orders?type=')); ?>"+data;
                                    }, 2000);
                                }else{
                                  jQuery('.alert-danger').append('<p>Something Error</p>');
                                }
                                window.scrollTo({ top: 0, behavior: 'smooth' });
                          },
                          error: function(data) {
                            console.log('Error:', data);
                          }
                        });
                    }
                },
                cancel: {
                    text: 'Cancel',
                    action: function () {
                    }
                }
            }
      });
    })

      });
  
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/orders/show.blade.php ENDPATH**/ ?>
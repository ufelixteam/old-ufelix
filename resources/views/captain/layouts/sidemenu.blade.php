<div id="sidebar-nav" class="sidebar">
    <div class="sidebar-scroll">
        <nav>
            <ul class="nav">
                <li><a href="{{url('captainDashboard')}}" class="{{ Request::is('captainDashboard/dashboard')  ? 'active' : '' }}">
                        <i class="lnr lnr-home"></i> <span>{{__('captain.dashboard')}}</span>
                    </a>
                </li>
                @if(Auth::guard('captain')->user()->is_manager)
                <li><a href="{!! URL::asset('captainDashboard/drivers') !!}"><i
                            class="fa fa-car"></i><span>{{__('captain.drivers')}}</span></a></li>
                @endif
                <li><a href="{!! URL::asset('captainDashboard/orders?type=ofd') !!}"><i
                            class="fa fa-check"></i><span>{{__('captain.ofd')}}</span></a>
                </li>
                <li><a href="{!! URL::asset('captainDashboard/orders?type=finish_not_dropped') !!}"><i
                            class="fa fa-battery"></i><span>{{__('captain.finished_orders_not_in_ufelix')}}</span></a>
                </li>
                <li><a href="{!! URL::asset('captainDashboard/orders?type=finish_dropped') !!}"><i
                            class="fa fa-battery"></i><span>{{__('captain.finished_orders_in_ufelix')}}</span></a>
                </li>
                <li>
                    <a type="button" class="import" style="margin-right: 30px;" href="#"
                            data-toggle="modal" data-target="#statusModal">
                        <i
                            class="fa fa-upload"></i><span> {{__('backend.update_status')}}
                    </a>
                </li>
                <li><a href="{!! URL::asset('captainDashboard/orders/all') !!}"><i
                            class="glyphicon glyphicon-align-justify"></i><span>{{__('captain.all_orders')}}</span></a>
                </li>
                <li><a class="{{ Request::is('captainDashboard/scan_order_qrcode') ? 'active' : '' }}"
                       href="{!! URL::asset('captainDashboard/scan_order_qrcode') !!}">
                        <i class="fa fa-camera"></i> <span>{{__('captain.scan_order')}}</span>
                    </a>
                </li>
{{--                <li>--}}
{{--                    <a href="#orderPages" data-toggle="collapse"--}}
{{--                       class="{{ Request::is('captainDashboard/orders*')  ? 'active' : 'collapsed' }} "--}}
{{--                       aria-expanded="{{Request::is('captainDashboard/orders*')  ? 'true' : 'false' }} ">--}}
{{--                        <i class="fa fa-tree"></i> <span>{{__('captain.the_orders')}}</span> <i--}}
{{--                            class="icon-submenu lnr lnr-chevron-left"></i></a>--}}
{{--                    <div id="orderPages" class="collapse {{  Request::is('captainDashboard/orders*')  ? 'in' : '' }}--}}
{{--                        " aria-expanded="{{ Request::is('captainDashboard/orders*')  ? 'true' : 'false' }}">--}}
{{--                        <ul class="nav">--}}
{{--                            <li><a href="{!! URL::asset('captainDashboard/orders?type=accept') !!}"><i class="fa fa-check"></i><span>{{__('captain.Accepted_Orders')}}</span></a>--}}
{{--                            </li>--}}
{{--                            <li><a href="{!! URL::asset('captainDashboard/orders?type=receive') !!}"><i--}}
{{--                                        class="fa fa-battery-half"></i><span>{{__('captain.Received_Orders')}}</span></a>--}}
{{--                            </li>--}}
{{--                            <li><a href="{!! URL::asset('captainDashboard/orders?type=deliver') !!}"><i--}}
{{--                                        class="fa fa-battery"></i><span>{{__('captain.Delivered_Orders')}}</span></a>--}}
{{--                            </li>--}}
{{--                            <li><a href="{!! URL::asset('captainDashboard/orders?type=cancel') !!}"><i class="fa fa-close"></i><span>{{__('captain.Cancelled_Orders')}}</span></a>--}}
{{--                            </li>--}}
{{--                            <li><a href="{!! URL::asset('captainDashboard/orders?type=recall') !!}"><i--}}
{{--                                        class="fa fa-history"></i><span>{{__('captain.Recalled_Orders')}}</span></a>--}}
{{--                            </li>--}}

{{--                            <li><a href="{!! URL::asset('captainDashboard/orders?type=current') !!}"><i class="fa fa-check"></i><span>{{__('captain.Current_Orders')}}</span></a>--}}
{{--                            </li>--}}
{{--                            <li><a href="{!! URL::asset('captainDashboard/orders?type=finish') !!}"><i--}}
{{--                                        class="fa fa-battery"></i><span>{{__('captain.Finished_Orders')}}</span></a>--}}
{{--                            </li>--}}

{{--                            <li><a href="{!! URL::asset('captainDashboard/orders/all') !!}"><i--}}
{{--                                        class="glyphicon glyphicon-align-justify"></i><span>{{__('captain.all_orders')}}</span></a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}

            </ul>
        </nav>
    </div>
</div>

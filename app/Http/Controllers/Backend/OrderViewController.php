<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\User;
use App\Models\Order;
use App\Models\Agent;
use App\Models\OrderView;
use Response;
use Flash;
use File;
use Auth;

class OrderViewController extends Controller {

private $guardType;
	public function __construct(Request $request)
    {
    	if($request->is('mngrAdmin/*') || $request->is('admin/*') ) {
            \Config::set('auth.defaults.guard', 'admin');
    	}elseif ($request->is('mngrAgent/*') || $request->is('agent/*')) {
    		\Config::set('auth.defaults.guard','agent');
    	}
        $this->guardType = \Config::get('auth.defaults.guard');
    }
	function active($id){
   	 	$order_view = OrderView::findOrFail($id);
    	if($order_view->status==0){
    	$order_view->status = 1;
        }else{
        $order_view->status =0;
    	}
 	   $order_view->save();
    	return $order_view->status;
    }
	#ask_review_order

	public function ask_review_order( $id)
	{
		$user = Auth::guard($this->guardType)->user();

		$order_view = new OrderView();

		$order_view->order_id = $id;
	        $order_view->status = 0;
	        $order_view->user_id = $user->id;
	        $order_view->agent_id = $user->agent_id;
	        $order_view->notes = "";

		if($order_view->save()){
			echo "ok";
		}else{
			echo "cancel";
		}


	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	    $user = Auth::guard($this->guardType)->user();

    		$order_views = OrderView::orderBy('id', 'desc')->paginate(50);
		if ($user->roles->first()->name == 'admin') {

		    return view('backend.order_views.index', compact('order_views'));
		}else{
		    return view('agent.order_views.index', compact('order_views'));
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('backend.order_views.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$order_view = new OrderView();

		$order_view->order_id = $request->input("order_id");
	        $order_view->status = $request->input("status");
	        $order_view->user_id = $request->input("user_id");
	        $order_view->agent_id = $request->input("agent_id");
	        $order_view->notes = $request->input("notes");

		$order_view->save();

        	Flash::success(__('backend.OrderView_saved_successfully'));
					return redirect()->route('mngrAdmin.order_views.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$order_view = OrderView::findOrFail($id);

		return view('backend.order_views.show', compact('order_view'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$order_view = OrderView::findOrFail($id);

		return view('backend.order_views.edit', compact('order_view'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$order_view = OrderView::findOrFail($id);

		$order_view->order_id = $request->input("order_id");
        $order_view->status = $request->input("status");
        $order_view->user_id = $request->input("user_id");
        $order_view->agent_id = $request->input("agent_id");
        $order_view->notes = $request->input("notes");

		$order_view->save();

        Flash::warning(__('backend.OrderView_Updated_successfully'));

		return redirect()->route('mngrAdmin.order_views.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$order_view = OrderView::findOrFail($id);
		$order_view->delete();

        flash(__('backend.OrderView_deleted_successfully'))->error();

		return redirect()->route('mngrAdmin.order_views.index');
	}

}

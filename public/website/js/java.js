$(window).load(function () {
	$('.loader').fadeOut(2000);
});

new WOW().init();
	wow = new WOW(
		{
			boxClass:     'wow',      // default
			animateClass: 'animated', // default
			offset:       0,          // default
			mobile:       true,       // default
			live:         true        // default
		}
	)
	wow.init();

// NextSection
$(function () {
    $('.nextSection').on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top + 50
        }, 500, 'linear');
    });
});

// Slider
$(document).ready(function() {

      $(".home-slider").owlCarousel({
		transitionStyle: "fadeUp",
        navigation: true,
        slideSpeed: 1000,
        paginationSpeed: 400,
        singleItem: true,
        responsive: true,
        autoPlay: true,
        pagination: true,
        stopOnHover: false,
        mouseDrag: false,
	   	navigationText : ["<i class='fa fa-angle-left'></i>" , "<i class='fa fa-angle-right'></i>"],
       	afterAction: function (el) {
            //remove class active
            this
                .$owlItems
                .removeClass('active');

            //add class active
            this
                .$owlItems //owl internal $ object containing items
                .eq(this.currentItem)
                .addClass('active');
        }
	  });

// Client
      var owl = $(".clients-slider");
      owl.owlCarousel({

       	autoPlay: true,
          itemsCustom : [
          [0, 1],
          [450, 1],
          [600, 2],
          [700, 2],
		  [900, 4],
          [1000, 5],
          [1200, 5],
          [1400, 5],
          [1600, 5]
        ],
		rowCustom: 2,
		navigation: false,
        slideSpeed: 1000,
        paginationSpeed: 400,
       	responsive: true,
       	pagination: true,
       	stopOnHover: false,
	   	navigationText : ["<i class='fa fa-arrow-left'></i>" , "<i class='fa fa-arrow-right'></i>"],
      });

});
//Drop
$('.menu-item-has-children').hover(function() {
	$(this).children('.sub-menu').stop(true, true).delay(100).fadeIn(200);
	}, function() {
	$(this).children('.sub-menu').stop(true, true).delay(100).fadeOut(200);
	});
$('.responsive').click( function(){
   $('.sub-menu').toggle();
});

// SideBar
$('.side-nav').mCustomScrollbar({
	autoHideScrollbar: false,
	setTop: 0,
	scrollInertia: 50,
	theme: "light-1"
});

$('.open-sidebar').on('click', function () {
	$('.sidebar').toggleClass('opened');
	$('.overlay_gen').fadeIn();
	$('body').addClass('sided');
});

$('.overlay_gen').on('click', function () {
	$('.sidebar').toggleClass('opened');
	$('.overlay_gen').fadeOut();
	$('body').removeClass('sided');
});



//Nav
$(window).on("scroll", function() {
    if($(window).scrollTop() > 50) {
        $("nav.moved").addClass("active");
    } else {
        //remove the background property so it comes transparent again (defined in your css)
       $("nav.moved").removeClass("active");
    }
});

// Top

$(window).scroll(function () {
        if ($(this).scrollTop() > 500) {
            $(".top").addClass("scrollNow");
        } else {
            $(".top").removeClass("scrollNow");
        }
    });

    $(".top").click(function () {
        $("html,body").animate({
            scrollTop: 0
        }, 500);
        return false;
    });


$(function() {
  $('.scroll').scrollOffset({
    offset: 80 // default: 0
  });
});


// jQuery counter
$('.counter').counterUp({
	delay: 50,
	time: 3000,
});

// Clients
$(document).ready(function() {
	$(".single-slider").owlCarousel({
		// transitionStyle: "fadeUp",
		// navigation: false,
       	// slideSpeed: 500,
       	// paginationSpeed: 400,
       	singleItem: true,
       	responsive: true,
       	// autoPlay: 6000,
       	// pagination: true,
       	// stopOnHover: false,
	});
});


$(".search").click(function(e) {
	$("#search").slideToggle(500);

});




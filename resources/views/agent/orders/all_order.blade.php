@extends('agent.layouts.app')
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<style>
    .jconfirm.jconfirm-modern .jconfirm-box div.jconfirm-title-c .jconfirm-icon-c {
    -webkit-transition: -webkit-transform .5s;
    transition: -webkit-transform .5s;
    transition: transform .5s;
    transition: transform .5s,-webkit-transform .5s;
    -webkit-transform: scale(0);
    transform: scale(0);
    display: block;
    margin-right: 0;
    margin-left: 0;
    margin-bottom: 10px;
    font-size: 69px;
    color: #9e1717;
}
</style>
@endsection
@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> Orders <span style="color: #5810c3;" >{{! empty($driver) ? ' of Driver:: '.$driver->name.' - '.$driver->mobile : ''}} </span>

        </h3>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            @if(count($orders) > 0)
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr style="text-transform: capitalize; font-size: 1.3rem;">
                            <th>#</th>
                            <th>Received Date</th>
                            <th>Order Type</th>
														<th>order price</th>
                            <th>receiver name</th>
                            <th>receiver mobile</th>
                            <th>sender name</th>
                            <th>sender mobile</th>
                            <th>Driver Name</th>
                            <th>status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($orders as $order)
                            <tr>
															<td>{{$order->order_id}}</td>
															<td>{{$order->received_date}}</td>
                                <td>{{$order->type}}</td>
                                <td>{{$order->order_price . ' '.'LE'}}</td>
                                <td>{{$order->order->receiver_name}}</td>
                                <td>{{$order->order->receiver_mobile}}</td>
                                <td>{{$order->order->sender_name}}</td>
                                <td>{{$order->order->sender_mobile}}</td>
                                <td>{{$order->driver->name}}</td>
                                <td>
                                  @if($order->order_status == 0 )
                                    <span class='badge badge-pill badge-black'>Pending</span>
                                  @elseif($order->order_status == 1 && $order->accepted_status == 0)
                                    <span class='badge badge-pill badge-info'>Accepted</span>
                                  @elseif($order->order_status == 2 && $order->accepted_status == 1)
                                    <span class='badge badge-pill badge-warning'>Received</span>
                                  @elseif($order->order_status == 3 && $order->accepted_status == 2)
                                    <span class='badge badge-pill badge-success'>Delivered</span>
                                  @elseif($order->accepted_status == 3)
                                    <span class='badge badge-pill badge-danger'>Cancelled</span>
                                  @elseif($order->accepted_status == 4)
                                    <span class='badge badge-pill badge-danger'>Recalled</span>
                                  @endif
																</td>
																<!-- status -->
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $orders->links() }}
            @else
                <h3 class="text-center alert alert-warning">No Result Found !</h3>
            @endif
        </div>
    </div>

@endsection

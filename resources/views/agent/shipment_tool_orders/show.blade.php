@extends('agent.layouts.app')
@section('header')
<div class="page-header">
        <h3>Show #{{$id}}</h3>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
			<h4>Items</h4>
             <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Material</th>
                            <th>price</th>
                            <th>Quantity</th>
                            <th>Total Price</th>
                            <th>status</th>
                            <th>notes</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach ($shipment_tool_order as $order)
                        <tr>
                            <td>{{$order->shipitemordes_id}}</td>
                            <td>{{$order->name_en}}</td>
                            <td>{{$order->price}}</td>
                            <td>{{$order->quantity}}</td>
                            <td>{{ $order->quantity * $order->price}}</td>
                            <td>
                              @if($order->status == 0 )
                                  <span class='badge badge-pill badge-black'>Pending</span>
                                  @else
                                  <span class='badge badge-pill badge-success'>done</span>
                                @endif
                            </td>
                            <td>{{ $order->notes }}</td>
                        </tr>
                        @endforeach
                    </tbody>
        		</table>
    </div>
</div>

          <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <a class="btn btn-link pull-right" href="{{ route('mngrAgent.shipment_tool_orders.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
            </div>
        </div>
    </div>

@endsection

<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        <?php if(! empty($orders) && count($orders) > 0): ?>
            <form method="post" id="myform" action="<?php echo e(url('/mngrAdmin/forward_order')); ?>">
                <input type="hidden" name="_token" id="token" value="<?php echo e(csrf_token()); ?>">
                <div class="table-responsive">
                    <table id="theTable" class="table table-condensed table-striped text-center"
                           style="min-height: 200px;">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>No</th>
                            <?php if($type == 'pending' ): ?>
                                <th><?php echo e(__('backend.agent')); ?></th>
                            <?php endif; ?>

                            <th><?php echo e(__('backend.collection')); ?></th>

                            <th><?php echo e(__('backend.receiver_name')); ?></th>
                            <th><?php echo e(__('backend.sender_name')); ?></th>
                            <th><?php echo e(__('backend.from')); ?></th>
                            <th><?php echo e(__('backend.to')); ?></th>
                            <th style="font-weight: bold; color: #333;"><?php echo e(__('backend.order_number')); ?></th>
                            <th style="font-weight: bold; color: #333;"><?php echo e(__('backend.receiver_code')); ?></th>
                            <?php if($type == 'receive' || $type=='deliver' ): ?>
                                <th><?php echo e(__('backend.delivery_price')); ?></th>
                            <?php endif; ?>
                            <?php if($type == 'recall' ): ?>
                                <th><?php echo e(__('backend.recall_by')); ?></th>

                                <th><?php echo e(__('backend.recall_at')); ?></th>
                            <?php endif; ?>

                            <?php if($type == 'cancel' ): ?>
                                <th><?php echo e(__('backend.cancel_by')); ?></th>
                                <th><?php echo e(__('backend.cancel_at')); ?></th>

                            <?php endif; ?>

                            <th style="width: 150px;">
                                <?php if($type == 'pending' || $type=='waiting' ): ?>

                                    <input type="button" class="btn btn-info btn-xs" id="toggle"
                                           value="<?php echo e(__('backend.select')); ?>" onClick="do_this()"/>
                                    <!-- <button type="submit" class="btn btn-warning btn-xs btn-pdf">PDF</button> -->
                                    <a class="btn btn-xs btn btn btn-success" id="forward" data-toggle="modal"
                                       data-target="#forwardModal">
                                        <i class="fa fa-eye"></i> <?php echo e(__('backend.forward')); ?>


                                    </a>
                                <?php endif; ?>
                                <button type="button" id="pdf-submit"
                                        class="btn btn-warning btn-pdf btn-xs "><?php echo e(__('backend.pdf')); ?></button>

                            </th>


                            <?php if($type == 'pending' || $type=='waiting' ): ?>

                                <th>
                                    <a class="btn btn-xs btn btn btn-primary" id="packup">
                                        <i class="fa fa-eye"></i> <?php echo e(__('backend.packup')); ?>


                                    </a>
                                </th>

                            <?php endif; ?>


                            <th></th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($order->id); ?></td>
                                <td><?php echo e($order->order_no); ?></td>
                                <?php if($type == 'pending' ): ?>
                                    <td><?php if(! empty($order->agent) ): ?> <a style="color:#f5b404;font-weight: bold"
                                                                        target="_blank"
                                                                        href="<?php echo e(URL::asset('/mngrAdmin/agents/'.$order->agent_id)); ?>"> <?php echo e($order->agent->name); ?> </a> <?php endif; ?>
                                    </td>
                                <?php endif; ?>
                                <td><?php echo e($order->collection_id); ?></td>
                                <td><?php echo e($order->receiver_name); ?></td>
                                <td>
                                    <?php echo e($order->sender_name); ?>

                                    <?php if($order->customer && $order->customer->Corporate): ?>
                                    </br><strong style="color: #0c81bc"> <?php echo e($order->customer->Corporate->name); ?></strong>
                                    <?php endif; ?>

                                </td>
                                <td>
                                    <?php if(! empty($order->from_government)  && $order->from_government != null ): ?>
                                        <a style="color:#f5b404;font-weight: bold" target="_blank"
                                           href="<?php echo e(URL::asset('/mngrAdmin/governorates/'.$order->s_government_id)); ?>"> <?php echo e($order->from_government->name_en); ?> </a>
                                    <?php endif; ?>
                                </td>

                                <td>
                                    <?php if(! empty($order->to_government)  && $order->from_government != null  ): ?> <a
                                        style="color:#f5b404;font-weight: bold" target="_blank"
                                        href="<?php echo e(URL::asset('/mngrAdmin/governorates/'.$order->r_government_id)); ?>"> <?php echo e($order->to_government->name_en); ?> </a>
                                    <?php endif; ?>
                                </td>
                                <td style="font-weight: bold; color: #a43;"><?php echo e($order->order_number); ?></td>
                                <td style="font-weight: bold; color: #a4e;"><?php echo e($order->receiver_code); ?></td>

                                <?php if($type == 'receive' || $type=='deliver' ): ?>
                                    <td><?php echo e($order->delivery_price); ?></td>
                                <?php endif; ?>
                                <?php if($type == 'recall' ): ?>
                                    <td><?php echo $order->recall_span; ?></td>
                                    <td><?php echo $order->recalled_at; ?></td>
                                <?php endif; ?>
                                <?php if($type == 'cancel' ): ?>
                                    <td><?php echo $order->cancel_span; ?></td>
                                    <td><?php echo $order->cancelled_at; ?></td>

                                <?php endif; ?>

                                <td class="selectpdf" id="selectpdf">
                                    <?php if($order->is_pickup == "0"): ?>

                                        <input type="checkbox" name="select[]" value="<?php echo e($order->id); ?>"
                                               data-value="<?php echo e($order->id); ?>"/>


                                    <?php endif; ?>

                                </td>

                                <td class="selectpackup" id="selectpackup">
                                    <?php if( ($type == 'pending' ) && $order->is_packedup !=1  && $order->collection_id != null ): ?>
                                        <?php if($order->is_pickup == "0"): ?>
                                            <input type="checkbox" class="selectpack" name="selectpackup[]"
                                                   value="<?php echo e($order->id); ?>" data-value="<?php echo e($order->collection_id); ?>"/>

                                        <?php else: ?>
                                            <span class='badge badge-pill label-warning'>Pick Up</span>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>

                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                            <li>
                                                <a href="<?php echo e(route('mngrAdmin.orders.show', $order->id )); ?>"><i
                                                        class="fa fa-eye"></i> <?php echo e(__('backend.view')); ?></a>
                                            </li>
                                            <?php if( $type != 'deliver' && $type != 'recall'  && ($type == 'cancel' && ( $order->cancelled_by == 1 ||  $order->cancelled_by == 2 ) ) ): ?>

                                                <li>
                                                    <a href="<?php echo e(route('mngrAdmin.orders.edit', $order->id)); ?>"><i
                                                            class="fa fa-edit"></i> <?php echo e(__('backend.edit')); ?></a>
                                                </li>
                                            <?php endif; ?>
                                            <?php if( $type == 'recall' ): ?>
                                                <li>
                                                    <a href="<?php echo e(url('/mngrAdmin/forward_recall/'.$order->id)); ?>"><i
                                                            class="fa fa-share"></i> <?php echo e(__('backend.edit_forward')); ?></a>
                                                </li>
                                            <?php endif; ?>
                                            <li>
                                                <a class=" printdata" data-id="<?php echo e($order->id); ?>"><i
                                                        class="fa fa-print"></i> <?php echo e(__('backend.pdf')); ?></a>
                                            </li>

                                            <?php if(  $type == 'cancel' ||  $type ==  'expired_waiting' || $type == 'pending' ): ?>
                                                <li>
                                                    <a data-id="<?php echo e($order->id); ?>" class="delete-order"><i
                                                            class="fa fa-trash"></i> <?php echo e(__('backend.delete')); ?></a>
                                                </li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        <div class="modal fade" id="forwardModal" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <input type="hidden" class="form-control" name="orderTypeID[]"
                                                   value="<?php echo e($order->order_type_id); ?>">
                                            <input type="hidden" class="form-control" name="governorateCostID[]"
                                                   value="<?php echo e($order->governorate_cost_id); ?>">
                                        </div>
                                        <p><?php echo e(__('backend.Forward_Orders_In_Agent_Or_Driver')); ?></p>
                                        <div class="form-group">
                                            <br>
                                            <select name="agent" id="agent" class="form-control agent" required>
                                                <option value="" selected><?php echo e(__('backend.Choose_Agent')); ?></option>
                                                <?php if(! empty($agents)): ?>
                                                    <?php $__currentLoopData = $agents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $agent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($agent->id); ?>"><?php echo e($agent->name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                            </select>

                                            <br>
                                            <select class="form-control" name="driverID" id="driver">
                                                <option value=""><?php echo e(__('backend.Choose_Driver')); ?></option>
                                            </select>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary"
                                                    onclick="document.forms['myform'].submit(); return false;"><?php echo e(__('backend.forward')); ?></button>
                                            <button type="button" class="btn btn-danger"
                                                    data-dismiss="modal"><?php echo e(__('backend.close')); ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </tbody>
                    </table>
                </div>
            </form>
            <?php echo $orders->appends($_GET)->links(); ?>

        <?php else: ?>
            <h3 class="text-center alert alert-warning">No Result Found !</h3>
        <?php endif; ?>
    </div>
</div>
<div id="js-form" class="js-form">
</div>
<?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/orders/table.blade.php ENDPATH**/ ?>
@extends('backend.layouts.app')
@section('css')
  <title>{{__('backend.Pricing_List')}} - {{__('backend.view')}}</title>
@endsection
@section('header')
<div class="page-header">
        <h1>{{__('backend.Show_Price_List')}} #{{$price_list->id}}</h1>
        <form action="{{ route('mngrAdmin.price_lists.destroy', $price_list->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('mngrAdmin.price_lists.edit', $price_list->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                <button type="submit" class="btn btn-danger">{{__('backend.delete')}} <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">#</label>
                    <p class="form-control-static">{{$price_list->id}}</p>
                </div>
                <div class="form-group">
                     <label for="target">{{__('backend.target')}}</label>
                     <p class="form-control-static">{{$price_list->target}}</p>
                </div>
                    <div class="form-group">
                     <label for="discount_percentage">{{__('backend.discount_percentage')}}</label>
                     <p class="form-control-static">{{$price_list->discount_percentage}} %</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('mngrAdmin.price_lists.index') }}"><i class="glyphicon glyphicon-backward"></i>  {{__('backend.back')}}</a>

        </div>
    </div>

@endsection

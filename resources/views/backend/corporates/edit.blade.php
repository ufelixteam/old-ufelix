@extends('backend.layouts.app')

@section('css')
    <title> {{__('backend.corporates')}} - {{__('backend.edit')}}</title>
    <link href="{{asset('assets/select2/select2.min.css')}}" rel="stylesheet">
    <style>
        .select2-container {
            width: 100% !important;
            display: block;
        }
        .error_user {
            display: inline-block;
            color: rgb(255, 255, 255);
            background-color: #9c3737;
            width: 100%;
            padding: 5px;
            margin-top: 3px;
            border-radius: 2px;
            text-align: center;
            font-weight: 500;
        }

        .form-group {
            min-height: 94px;
            margin-bottom: 0px;
        }

        .imgDiv img {
            height: 100px;
            width: 100px;
            border-radius: 50%;
        }
    </style>
@endsection

@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit_corporate')}} - {{$corprate->name}}</h3>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <form action="{{ route('mngrAdmin.corporates.update', $corprate->id) }}" method="POST"
                  enctype="multipart/form-data" name="corporate_edit">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group col-md-4 col-sm-4 @if($errors->has('name')) has-error @endif">
                    <label for="name-field">{{__('backend.name')}}</label>
                    <input type="text" id="name-field" name="name" class="form-control"
                           value="{{ !empty($corprate->name)?$corprate->name: old('name') }}"/>
                    @if($errors->has("name"))
                        <span class="help-block">{{ $errors->first("name") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-4 col-sm-4 @if($errors->has('email')) has-error @endif">
                    <label for="email-field">{{__('backend.email')}}</label>
                    <input type="text" id="email-field" name="email" class="form-control"
                           value="{{ !empty($corprate->email)?$corprate->email:old('email') }}"/>
                    @if($errors->has("email"))
                        <span class="help-block">{{ $errors->first("email") }}</span>
                    @endif
                </div>
                {{--                <div class="form-group col-md-4 col-sm-4 @if($errors->has('field')) has-error @endif">--}}
                {{--                    <label for="corporate-work-field">{{__('backend.corporate_work_field')}}: </label>--}}
                {{--                    <input type="text" id="corporate-work-field" name="field" class="form-control"--}}
                {{--                           value="{{ !empty($corprate->field)?$corprate->field:old('field') }}"/>--}}
                {{--                    @if($errors->has("field"))--}}
                {{--                        <span class="help-block">{{ $errors->first("field") }}</span>--}}
                {{--                    @endif--}}
                {{--                </div>--}}
                <div class="form-group col-md-4 col-sm-4 @if($errors->has('responsible_id')) has-error @endif">
                    <label for="responsible_id">{{__('backend.responsible')}}: </label>
{{--                    <input type="text" id="responsible-field" name="responsible" class="form-control"--}}
{{--                           value="{{ !empty($corprate->responsible)?$corprate->responsible:old('responsible') }}"/>--}}
                    <select class=" form-control" id="responsible_id" name="responsible_id">
                        <option value="">{{__('backend.responsible')}}</option>
                        @foreach($users as $user)
                            <option
                                value="{{$user->id}}" {{$user->id == $corprate->responsible_id ? 'selected' : ''}}>
                                {{$user->name}}
                            </option>
                        @endforeach
                    </select>
                    @if($errors->has("responsible_id"))
                        <span class="help-block">{{ $errors->first("responsible_id") }}</span>
                    @endif
                </div>

                <div class="form-group col-md-4 col-sm-4 @if($errors->has('order_type')) has-error @endif">
                    <label class="control-label">{{__('backend.order_type')}}</label>
                    <select class=" form-control" id="order_type" name="order_type">
                        @foreach($types as $type)
                            <option
                                value="{{$type->id}}" {{$type->id == $corprate->order_type ? 'selected' : ''}}>
                                {{$type->name_en}}
                            </option>
                        @endforeach
                    </select>
                    @if($errors->has("order_type"))
                        <span class="help-block">{{ $errors->first("order_type") }}</span>
                    @endif
                </div>
                <div id="other_type_order"
                     class="form-group col-md-4 col-sm-4 @if($errors->has('other_order_type')) has-error @endif"
                     @if($corprate->order_type != 5) style="display: none" @endif>
                    <label class="control-label">{{__('backend.other_order_type')}}</label>
                    <input type="text" id="other_order_type-field" name="other_order_type" class="form-control"
                           value="{{ !empty($corprate->other_order_type)?$corprate->other_order_type:old('other_order_type') }}"/>
                    @if($errors->has("other_order_type"))
                        <span class="help-block">{{ $errors->first("other_order_type") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-4 col-sm-4 @if($errors->has('shipping_type')) has-error @endif">
                    <label class="control-label">{{__('backend.shipping_type')}}</label>
                    <select class=" form-control" id="shipping_type" name="shipping_type">
                        <option
                            value="1" {{ $corprate->shipping_type == 1 ? 'selected' : ''}}>
                            {{__('backend.n_shipping')}}
                        </option>
                        <option
                            value="2" {{ $corprate->shipping_type == 2 ? 'selected' : ''}}>
                            {{__('backend.rt_shipping')}}
                        </option>
                    </select>
                    @if($errors->has("shipping_type"))
                        <span class="help-block">{{ $errors->first("shipping_type") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-4 col-sm-4 @if($errors->has('mobile')) has-error @endif">
                    <label for="mobile-field">{{__('backend.mobile')}}</label>
                    <input type="text" id="mobile-field" name="mobile" maxlength="11" class="form-control"
                           value="{{ !empty($corprate->mobile)?$corprate->mobile:old('mobile') }}"/>
                    @if($errors->has("mobile"))
                        <span class="help-block">{{ $errors->first("mobile") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-4 col-sm-4 @if($errors->has('phone')) has-error @endif">
                    <label for="phone-field">{{__('backend.mobile_number2')}}</label>
                    <input type="text" id="phone-field" name="phone" maxlength="11" class="form-control"
                           value="{{ !empty($corprate->phone)?$corprate->phone:old('phone') }}"/>
                    @if($errors->has("phone"))
                        <span class="help-block">{{ $errors->first("phone") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-4 col-sm-4 @if($errors->has('company_website')) has-error @endif">
                    <label for="company_website-field">{{__('backend.Officail_site')}}: </label>
                    <input type="text" id="company_website-field" name="company_website" class="form-control"
                           value="{{ !empty($corprate->company_website)?$corprate->company_website: old('company_website') }}"/>
                    @if($errors->has("company_website"))
                        <span class="help-block">{{ $errors->first("company_website") }}</span>
                    @endif
                </div>

                <div class="form-group col-md-4 col-sm-4 @if($errors->has('government_id')) has-error @endif">
                    <label for="government_id-field"> {{__('backend.government')}}: </label>
                    <select class="form-control government_id" id="government_id" name="government_id">
                        <option value="" data-display="Select">{{__('backend.government')}}</option>
                        @if(! empty($governments))
                            @foreach($governments as $government)
                                <option
                                    value="{{$government->id}}" {{ $corprate->government_id == $government->id ? 'selected' : ''}}>
                                    {{$government->name_en}}
                                </option>
                            @endforeach
                        @endif
                    </select>
                    @if($errors->has("government_id"))
                        <span class="help-block">{{ $errors->first("government_id") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-4 col-sm-4 @if($errors->has('city_id')) has-error @endif">
                    <label class="control-label" for="s_state_id">{{__('backend.city')}}</label>
                    <select class=" form-control city_id" id="city_id" name="city_id">
                        @foreach($cities as $city)
                            <option
                                value="{{$city->id}}" {{$city->governorate_id == $corprate->government_id && $city->id == $corprate->city_id ? 'selected' : ''}}>
                                {{$city->name_en}}
                            </option>
                        @endforeach
                    </select>
                    @if($errors->has("city_id"))
                        <span class="help-block">{{ $errors->first("city_id") }}</span>
                    @endif
                </div>
                <input type="hidden" name="old_cityId" value="{{$corprate->city_id}}"/>

                <div class="form-group col-md-4 col-sm-4 @if($errors->has('address')) has-error @endif">
                    <label for="address-field">{{__('backend.address')}}: </label>
                    <input type="text" id="address-field" name="address" class="form-control"
                           value="{{ !empty($corprate->address) ? $corprate->address: old('address') }}"/>
                    @if($errors->has("address"))
                        <span class="help-block">{{ $errors->first("address") }}</span>
                    @endif
                </div>

                {{--                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('latitude')) has-error @endif">--}}
                {{--                       <label for="latitude-field">{{__('backend.latitude')}}</label>--}}
                {{--                       <input type="text" id="latitude-field" name="latitude" class="form-control" value="{{ !empty($corprate->latitude) ? $corprate->latitude : '0' }}"/>--}}
                {{--                       @if($errors->has("latitude"))--}}
                {{--                        <span class="help-block">{{ $errors->first("latitude") }}</span>--}}
                {{--                       @endif--}}
                {{--                    </div>--}}
                {{--                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('longitude')) has-error @endif">--}}
                {{--                       <label for="longitude-field">{{__('backend.longitude')}}</label>--}}
                {{--                       <input type="text" id="longitude-field" name="longitude" class="form-control" value="{{ !empty($corprate->longitude) ? $corprate->longitude : '0' }}"/>--}}
                {{--                       @if($errors->has("longitude"))--}}
                {{--                        <span class="help-block">{{ $errors->first("longitude") }}</span>--}}
                {{--                       @endif--}}
                {{--                    </div>--}}

                <div
                    class="form-group col-md-4 col-sm-4 @if($errors->has('commercial_record_number')) has-error @endif">
                    <label for="commercial-record-field">{{__('backend.commercial_record_number')}}</label>
                    <input type="text" id="commercial-record-field" maxlength="14" name="commercial_record_number"
                           class="form-control"
                           value="{{ !empty($corprate->commercial_record_number) ? $corprate->commercial_record_number : old('commercial_record_number') }}"/>
                    @if($errors->has("commercial_record_number"))
                        <span class="help-block">{{ $errors->first("commercial_record_number") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-4 col-sm-4 @if($errors->has('is_active')) has-error @endif">
                    <label for="is_active-field">{{__('backend.is_active')}}</label>
                    <select id="is_active-field" name="is_active" class="form-control" value="{{ old("is_active") }}">
                        <option
                            value="0" {{ old("is_active") == "0" || $corprate->is_active == "0" ? 'selected' : '' }}>{{__('backend.no')}}</option>
                        <option
                            value="1" {{ old("is_active") == "1" || $corprate->is_active == "1" ? 'selected' : '' }}>{{__('backend.yes')}}</option>
                    </select>
                    @if($errors->has("is_active"))
                        <span class="help-block">{{ $errors->first("is_active") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-4 col-sm-4">
                    <label for="mobile-users">{{__('backend.store')}}</label>
                    <select class="form-control select2 disabled" id="store_id" name="store_id">
                        <option value="" data-dispaly="Select">{{__('backend.store')}}</option>
                        @if(! empty($stores))
                            @foreach($stores as $store)
                                <option value="{{$store->id}}" @if($corprate->store_id == $store->id) selected @endif>{{$store->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="form-group col-md-4 col-sm-4 @if($errors->has('transfer_method')) has-error @endif">
                    <label for="transfer_method">{{__('backend.transfer_method')}}</label>
                    <select id="transfer_method" name="transfer_method" class="form-control">
                        <option value="" data-display="Select">{{__('backend.choose_transfer_method')}}</option>
                        <option
                            value="1" {{ $corprate->transfer_method == 1 ? 'selected' : '' }}>{{__('backend.bank')}}</option>
                        <option
                            value="2" {{ $corprate->transfer_method == 2 ? 'selected' : '' }}>{{__('backend.mobicash')}}</option>
                        <option
                            value="3" {{ $corprate->transfer_method == 3 ? 'selected' : '' }}>{{__('backend.cash_with_captain')}}</option>
                    </select>
                    @if($errors->has("transfer_method"))
                        <span class="help-block">{{ $errors->first("transfer_method") }}</span>
                    @endif
                </div>

                <div
                    class="form-group col-md-4 col-sm-4 {{ $corprate->transfer_method != 1 ? 'hidden' : '' }} @if($errors->has('bank_name')) has-error @endif">
                    <label for="bank_name">{{__('backend.bank_name')}}</label>
                    <input type="text" id="bank_name" name="bank_name" class="form-control"
                           value="{{$corprate->bank_name}}"/>
                    @if($errors->has("bank_name"))
                        <span class="help-block">{{ $errors->first("bank_name") }}</span>
                    @endif
                </div>

                <div
                    class="form-group col-md-4 col-sm-4 {{ $corprate->transfer_method != 1 ? 'hidden' : '' }} @if($errors->has('bank_account')) has-error @endif">
                    <label for="bank_name">{{__('backend.bank_account')}}</label>
                    <input type="text" id="bank_account" name="bank_account" class="form-control"
                           value="{{$corprate->bank_account}}"/>
                    @if($errors->has("bank_account"))
                        <span class="help-block">{{ $errors->first("bank_account") }}</span>
                    @endif
                </div>

                <div
                    class="form-group col-md-4 col-sm-4 {{ $corprate->transfer_method != 2 ? 'hidden' : '' }} @if($errors->has('mobile_transfer')) has-error @endif">
                    <label for="mobile_transfer">{{__('backend.mobile_number')}}</label>
                    <input type="text" id="mobile_transfer" name="mobile_transfer" class="form-control"
                           value="{{$corprate->mobile_transfer}}"/>
                    @if($errors->has("mobile_transfer"))
                        <span class="help-block">{{ $errors->first("mobile_transfer") }}</span>
                    @endif
                </div>

                <div class="col-md-12">
                    <div
                        class="form-group col-md-4 col-sm-4 @if($errors->has('commercial_record_image')) has-error @endif">
                        <div id="commercial_record_image_upload">
                            @if($corprate->commercial_record_image)
                                <div class="imgDiv">
                                    <input type="hidden" value="{{ $corprate->commercial_record_image }}"
                                           name="commercial_record_image_old">
                                    <img src="{{ $corprate->commercial_record_image }}"
                                         style="width:100px;height:100px"
                                         onerror="this.src='{{asset('assets/images/image.png')}}'">
                                    <button type="button" class="btn btn-danger cancel">&times;</button>
                                </div>
                            @endif
                        </div>
                        <label for="commercial_record_image-field">{{__('backend.ContractForm')}}</label>
                        <input class="uploadPhoto btn btn-primary" type="file" name="commercial_record_image"
                               id="commercial_record_image">
                        @if($errors->has("commercial_record_image"))
                            <span class="help-block">{{ $errors->first("commercial_record_image") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('logo')) has-error @endif">
                        <div id="logo_upload">
                            @if($corprate->logo)
                                <div class="imgDiv">
                                    <input type="hidden" value="{{ $corprate->logo }}" name="logo_old">
                                    <img src="{{ $corprate->logo }}"
                                         style="width:100px;height:100px"
                                         onerror="this.src='{{asset('assets/images/image.png')}}'">
                                    <button type="button" class="btn btn-danger cancel">&times;</button>
                                </div>
                            @endif
                        </div>
                        <label for="logo-field">{{__('backend.logo')}}</label>
                        <input class="uploadPhoto btn btn-primary" type="file" name="logo" id="logo">
                        @if($errors->has("logo"))
                            <span class="help-block">{{ $errors->first("logo") }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group col-md-12 col-sm-12 col-xs-12 @if($errors->has('notes')) has-error @endif">
                    <label for="notes-field">@lang('backend.ContractNote')</label>
                    <textarea class="form-control" id="notes-field" rows="6"
                              name="notes">{{ $corprate->notes }}</textarea>
                    @if($errors->has("notes"))
                        <span class="help-block">{{ $errors->first("notes") }}</span>
                    @endif
                </div>

                <div class="well well-sm col-md-12 col-xs-12 col-sm-12" style="margin-top: 30px;">
                    <button type="submit" class="btn btn-primary" id="added_user">{{__('backend.save_edits')}}</button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.corporates.index') }}"><i
                            class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')

    <script src="{{asset('assets/select2/select2.min.js')}}"></script>

    <script type="text/javascript">
        $(function () {
            $('.select2').select2();
            $("#transfer_method").on('change', function () {
                if ($(this).val() == 1) {
                    $("#bank_name").parent().removeClass('hidden');
                    $("#bank_account").parent().removeClass('hidden');
                    $("#mobile_transfer").parent().addClass('hidden');
                } else if ($(this).val() == 2) {
                    $("#bank_name").parent().addClass('hidden');
                    $("#bank_account").parent().addClass('hidden');
                    $("#mobile_transfer").parent().removeClass('hidden');
                } else {
                    $("#bank_name").parent().addClass('hidden');
                    $("#bank_account").parent().addClass('hidden');
                    $("#mobile_transfer").parent().addClass('hidden');
                }
            });

            $("form[name='corporate_edit']").validate({
                // Specify validation rules
                rules: {
                    email: {
                        required: true,
                        email: true,
                    },
                    mobile: {
                        required: true,
                        number: true,
                        digits: true,
                        rangelength: [11, 11],
                    },

                    name: {
                        required: true,
                    },
                    // latitude: {
                    //     required: true,
                    //     number:true,
                    // },
                    // longitude: {
                    //     required: true,
                    //     number:true,
                    // },
                    // field: {
                    //     required: true,
                    // },
                    order_type: {
                        required: true,
                    },
                    shipping_type: {
                        required: true,
                    },
                    commercial_record_number: {
                        required: true,
                        number: true,
                    },
                    // commercial_record_image: {
                    //     required: true,
                    // },
                },
                // Specify validation error messages
                messages: {
                    name: {
                        required: "{{__('backend.Please_Enter_Corporate_Name')}}",
                    },
                    email: {
                        required: "{{__('backend.Please_Enter_Corporate_E-mail')}}",
                        email: "{{__('backend.Please_Enter_Correct_E-mail')}}",

                    },
                    mobile: {
                        required: "{{__('backend.Please_Enter_Corporate_Mobile')}}",
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                        rangelength: "{{__('backend.Mobile_Must_Be11_Digits')}}",

                    },
                    order_type: {
                        required: "{{__('backend.Please_Enter_Order_Type')}}"
                    },
                    shipping_type: {
                        required: "{{__('backend.Please_Enter_Shipping_Type')}}"
                    },
                    {{--latitude: {--}}
                        {{--    required: "{{__('backend.Please_Enter_Latitude')}}",--}}
                        {{--    number: "{{__('backend.Please_Enter_Avalid_Number')}}",--}}

                        {{--},--}}
                        {{--longitude: {--}}
                        {{--    number: "{{__('backend.Please_Enter_Avalid_Number')}}",--}}
                        {{--    required: "{{__('backend.Please_Enter_Longitude')}}",--}}
                        {{--},--}}
                        {{--field: {--}}
                        {{--    required: "{{__('backend.Please_Enter_Work_field')}}",--}}
                        {{--},--}}
                    commercial_record_number: {
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                        required: "{{__('backend.Please_Enter_Commercial_Record_Number')}}",
                    },
                    commercial_record_image: {
                        required: "{{__('backend.Please_Enter_Commercial_Record_Image')}}",
                    },
                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                },

                highlight: function (element) {
                    $(element).parent().addClass('error')
                },
                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {

                    form.submit();

                }
            });

            if ($("#corporate_has_client").val() == 1) {
                $('#add_client').css('display', 'block');
            } else {
                $('#add_client').css('display', 'none');
            }

            $("#corporate_has_client").change(function () {
                if ($(this).val() == 1) {
                    $('#add_client').css('display', 'block');
                } else {
                    $('#add_client').css('display', 'none');
                }
            });

            $("#order_type").on('change', function () {
                if ($(this).val() == 5) {
                    $("#other_type_order").show();
                } else {
                    $("#other_order_type-field").val('');
                    $("#other_type_order").hide();
                }
            });

            $("#government_id").on('change', function () {
                $('#city_id').html('<option value="" >Choose City</option>');
                if ($(this).val()) {
                    $.ajax({
                        url: "{{ url('/get_cities')}}" + "/" + $(this).val(),
                        type: 'get',
                        data: {},
                        success: function (data) {
                            $('#city_id').html('<option value="" >Choose City</option>');
                            $.each(data, function (i, content) {
                                $('#city_id').append($("<option></option>").attr("value", content.id).text(content.name_en));
                            });
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }
            });

            if ($("#corporate_has_user").val() == 1) {
                $('#add_user').css('display', 'block');
            } else {
                $('#add_user').css('display', 'none');
            }

            $("#corporate_has_user").change(function () {
                if ($(this).val() == 1) {

                    $('#add_user').css('display', 'block');
                    $('#added_user').attr('disabled', true);
                    $('.error_user').css('display', 'inline-block');

                    // $('#model_name-field', '#plate_number-field', '#chassi_number-field', '#year-field', '#vehicle_type_id-field', '#model_type_id-field', '#color_id-field', '#driving_licence_expired_date-field', '#license_end_date_field', '#driving_licence_image_front', '#license_image_front').blur();

                    $(".done_create_user").click(function (e) {

                        var NameError = true,
                            NumberError = true,
                            Number2Error = true,
                            emailError = true,
                            passwordError = true,
                            cpasswordError = true,
                            adderssError = true,
                            jobError = true;

                        if ($('#user-name-field').val() == '') {
                            NameError = true;
                        } else {
                            NameError = false;
                        }

                        if ($('#user-mobile-field').val() == '') {
                            NumberError = true;
                        } else {
                            NumberError = false;
                        }

                        if ($('#user-phone-field').val() == '') {
                            Number2Error = true;
                        } else {
                            Number2Error = false;
                        }

                        if ($('#user-email-field').val() == '') {
                            emailError = true;
                        } else {
                            emailError = false;
                        }

                        if ($('#user-password-field').val() == '') {
                            passwordError = true;
                        } else {
                            passwordError = false;
                        }

                        if ($('#user-confirm_password-field').val() == '') {
                            cpasswordError = true;
                        } else {
                            cpasswordError = false;
                        }

                        if ($('#user-address-field').val() == '') {
                            adderssError = true;
                        } else {
                            adderssError = false;
                        }

                        if ($('#user-job-field').val() == '') {
                            jobError = true;
                        } else {
                            jobError = false;
                        }

                        if (NameError === true || NumberError === true || Number2Error === true || emailError === true || passwordError === true || cpasswordError === true || adderssError === true || jobError === true) {
                            // console.log('error');
                            $('#added_user').attr('disabled', true);
                            e.preventDefault();
                            $('.error_user').css('display', 'inline-block');
                        } else {
                            // console.log('not error');
                            $('#added_user').attr('disabled', false);
                            $('.error_user').css('display', 'none');
                        }

                    });

                } else {
                    $('#add_user').css('display', 'none');
                    $('#added_user').attr('disabled', false);
                    $('.error_user').css('display', 'none');
                }
            });

        })
    </script>
@endsection

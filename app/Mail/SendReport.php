<?php

namespace App\Mail;

use App\Models\Invoice;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendReport extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The invoice instance.
     *
     * @var Invoice
     */
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data = [])
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $pdf_name = 'DailyReport' . '_' . $this->data['customer']->id . '_' . date('Y_m_d') . '.pdf';

        $pdf = \PDF::loadView('emails.reports.dailyReport', $this->data, [], ['format' => 'A4-L']);

        return $this->subject('DailyReport' . '_' . $this->data['customer']->id . '_' . date('Y_m_d'))
            ->view('emails.reports.report')
            ->with(
                [
                    'id' => $this->data['customer']->id,
                    'customer_name' => $this->data['customer']->name,
                    'corporate_name' => $this->data['customer']->corporate->name,
                ]
            )
            ->attachData($pdf->output(), $pdf_name, [
                'mime' => 'application/pdf',
            ]);
    }
}

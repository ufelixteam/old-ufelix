<!-- **** Modal **** -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><?php echo e(__('backend.Forward_Order')); ?></h5>
            </div>
            <div class="modal-body">
                <form class="" action="<?php echo e(url('mngrAdmin/ForwardOrders')); ?>" method="post">
                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="driver_id" value="<?php echo e(! empty($order->accepted) && ! empty($order->accepted->driver) ? $order->accepted->driver->id : ''); ?>">
                        <input type="hidden" class="form-control" name="order_id" value="<?php echo e($order->id); ?>">
                        <input type="hidden" class="form-control" name="accepted_id" value="<?php echo e(! empty($order->accepted) ? $order->accepted->id : ''); ?>">
                        <input type="hidden" class="form-control" name="governorate_cost_id" value="<?php echo e($order->governorate_cost_id); ?>">
                    </div>

                    <div class="form-group">
                        <select id="items-field" name="items" id="items" class="form-control">
                            <?php if(! empty($online_drivers)): ?>

                            <?php $__currentLoopData = $online_drivers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $driver): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($driver->id); ?>"
                                ><?php echo e($driver->name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        </select>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary"><?php echo e(__('backend.forward')); ?></button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo e(__('backend.close')); ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/orders/driver_forward_modal.blade.php ENDPATH**/ ?>

@if( app()->getLocale() == 'en' )
    <link href="{{ asset('/profile/css/bootstrap.min.css')}}" rel="stylesheet" media="screen">
	<link href="{{ asset('/profile/css/main.css')}}" rel="stylesheet" media="screen">
@else
    <link href="{{ asset('/profile/css/bootstrap.min.css')}}" rel="stylesheet" media="screen">

	<link href="{{ asset('/profile/css/bootstrap-rtl.min.css')}}" rel="stylesheet" media="screen">
	<link href="{{ asset('/profile/css/main.css')}}" rel="stylesheet" media="screen">

	<link href="{{ asset('/profile/css/main-ar.css')}}" rel="stylesheet" media="screen">
@endif

<link href="{{ asset('/profile/fonts/icomoon.css')}}" rel="stylesheet">
<link href="{{ asset('/profile/css/c3.css')}}" rel="stylesheet">
<link href="{{ asset('/profile/css/nv.d3.css')}}" rel="stylesheet">
<link href="{{ asset('/profile/css/chart.css')}}" rel="stylesheet">
<link href="{{ asset('/profile/css/cal-heatmap.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('/profile/css/circliful.css')}}">
<link rel="stylesheet" href="{{ asset('/profile/css/odometer.css')}}">





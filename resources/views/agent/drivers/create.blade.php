@extends('backend.layouts.app')

@section('css')
  <style>
    .error_vehicle{
      display: inline-block;
      color: rgb(255, 255, 255);
      background-color: #9c3737;
      width: 100%;
      padding: 5px;
      margin-top: 3px;
      border-radius: 2px;
      text-align: center;
      font-weight: 500;
    }
  </style>
  <title>{{__('backend.drivers')}} - {{__('backend.add_driver')}}</title>
@endsection

@section('header')
  <div class="page-header">
    <h3><i class="glyphicon glyphicon-plus"></i> {{__('backend.drivers')}} / {{__('backend.add_driver')}} </h3>
  </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <form action="{{ route('mngrAgent.drivers.store') }}" method="POST" enctype="multipart/form-data" id="driver_create" name="driver_create">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('name')) has-error @endif">
            <label for="name-field">{{__('backend.name')}}: </label>
            <input type="text" id="name-field" name="name" class="form-control" value="{{old("name")}}"/>
            @if($errors->has("name"))
              <span class="help-block">{{ $errors->first("name") }}</span>
            @endif
          </div>
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('email')) has-error @endif">
              <label for="email-field">{{__('backend.email')}}: </label>
              <input type="email" id="email-field" name="email" class="form-control" value="{{old("email")}}"/>
              @if($errors->has("email"))
                <span class="help-block">{{ $errors->first("email") }}</span>
              @endif
          </div>
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('mobile')) has-error @endif">
            <label for="mobile-field">{{__('backend.mobile_number')}}: </label>
            <input type="text" id="mobile-field" maxlength="11" name="mobile" class="form-control" value="{{old("mobile")}}"/>
            @if($errors->has("mobile"))
              <span class="help-block">{{ $errors->first("mobile") }}</span>
            @endif
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('phone')) has-error @endif">
            <label for="phone-field">{{__('backend.mobile_number2')}}:</label>
            <input type="text" id="phone-field" name="phone" max-maxlength="11" class="form-control" value="{{old("phone")}}"/>
            @if($errors->has("phone"))
              <span class="help-block">{{ $errors->first("phone") }}</span>
            @endif
          </div>
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('password')) has-error @endif">
            <label for="password-field">{{__('backend.password')}}: </label>
            <input type="password" id="password-field" name="password" class="form-control" value="{{old("password")}}"/>
            @if($errors->has("password"))
              <span class="help-block">{{ $errors->first("password") }}</span>
            @endif
          </div>
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('password')) has-error @endif">
            <label for="confirm_password-field">{{__('backend.confirm_password')}}: </label>
            <input type="password" id="confirm_password-field" name="confirm_password" class="form-control" value="{{ old("confirm_password") }}"/>
            @if($errors->has("confirm_password"))
              <span class="help-block">{{ $errors->first("confirm_password") }}</span>
            @endif
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('country_id')) has-error @endif">
            <label for="country_id-field">{{__('backend.choose_country')}}: </label>
            <select id="country_id-field" name="country_id" class="form-control">
              @if(! empty($countries))
                @foreach($countries as $country)
                  <option value="{{$country->id}}" {{ old("country_id") == $country->id ? 'selected' : '' }}
                    >{{$country->name}}</option>
                @endforeach
              @endif
            </select>
          </div>
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('government_id')) has-error @endif">
            <label for="government_id-field"> {{__('backend.government')}}: </label>
            <select  class="form-control government_id" id="government_id"  name="government_id">
                <option value="" data-display="Select">{{__('backend.government')}}</option>
                @if(! empty($governments))
                    @foreach($governments as $government)
                        <option value="{{$government->id}}" >
                          {{$government->name_en}}
                        </option>

                    @endforeach
                @endif
            </select>
            @if($errors->has("government_id"))
            <span class="help-block">{{ $errors->first("government_id") }}</span>
            @endif
          </div>
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('city_id')) has-error @endif">
            <label class="control-label" for="s_state_id">{{__('backend.city')}}</label>
            <select  class=" form-control city_id" id="city_id"  name="city_id" >
                <option value="Null" >{{__('backend.city')}}</option>
            </select>
            @if($errors->has("city_id"))
            <span class="help-block">{{ $errors->first("city_id") }}</span>
            @endif
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('address')) has-error @endif">
            <label for="address-field">{{__('backend.address')}}: </label>
            <input type="text" id="address-field" name="address" class="form-control" value="{{ old("address") }}"/>
            @if($errors->has("address"))
              <span class="help-block">{{ $errors->first("address") }}</span>
            @endif
          </div>
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('is_active')) has-error @endif">
            <label for="is_active-field">{{__('backend.isactive_verified_notverified')}}:</label>
            <select id="is_active-field" name="is_active" class="form-control" value="{{old("is_active")}}">
              <option value="0" {{ old("is_active") == "0" ? 'selected' : '' }}>{{__('backend.no')}}</option>
              <option value="1" {{ old("is_active") == "1" ? 'selected' : '' }}>{{__('backend.yes')}}</option>
            </select>
            @if($errors->has("is_active"))
              <span class="help-block">{{ $errors->first("is_active") }}</span>
            @endif
          </div>
        

        <div class="row">
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('latitude')) has-error @endif">
            <label for="latitude-field">{{__('backend.latitude')}}: </label>
            <input type="text" id="latitude-field" name="latitude" class="form-control" value="{{ old("latitude") ? old("latitude") : '0' }}" />
            @if($errors->has("latitude"))
              <span class="help-block">{{ $errors->first("latitude") }}</span>
            @endif
          </div>
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('longitude')) has-error @endif">
            <label for="longitude-field">{{__('backend.longitude')}}</label>
            <input type="text" id="longitude-field" name="longitude" class="form-control" value="{{ old("longitude") ? old("longitude") : '0' }}"/>
            @if($errors->has("longitude"))
              <span class="help-block">{{ $errors->first("longitude") }}</span>
            @endif
          </div>
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('national_id_number')) has-error @endif">
            <label for="national_id_number-field">{{__('backend.national_id_number')}}</label>
            <input type="text" id="national_id_number-field" name="national_id_number" class="form-control" value="{{old("national_id_number")}}"/>
            @if($errors->has("national_id_number"))
              <span class="help-block">{{ $errors->first("national_id_number") }}</span>
            @endif
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('national_id_expired_date')) has-error @endif">
            <label for="national_id_expired_date-field">{{__('backend.national_id_expired_date')}}</label>
            <div class='input-group date datepicker2'>
              <input type="text" id="national_id_expired_date-field" name="national_id_expired_date" class="form-control" value="{{ old("national_id_expired_date") }}"/>
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
            </div>
            @if($errors->has("national_id_expired_date"))
              <span class="help-block">{{ $errors->first("national_id_expired_date") }}</span>
            @endif
          </div>
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('notes')) has-error @endif">
            <label for="notes-field">{{__('backend.notes')}}: </label>
            <input type="text" id="notes-field" name="notes" class="form-control" value="{{ old("notes") }}"/>
            @if($errors->has("notes"))
              <span class="help-block">{{ $errors->first("notes") }}</span>
            @endif
          </div>
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('driver_has_vehicle')) has-error @endif">
            <label for="driver-has-vehicle-field">{{__('backend.driver_has_vehicle')}}</label>
            <select id="driver-has-vehicle-field" name="driver_has_vehicle" class="form-control" value="{{old("driver_has_vehicle")}}">
              <option value="">{{__('backend.choose_yes_or_no')}}</option>
              <option value="0" {{ old("driver_has_vehicle") == "0" ? 'selected' : '' }}>{{__('backend.no')}}</option>
              <option value="1" {{ old("driver_has_vehicle") == "1" ? 'selected' : '' }}>{{__('backend.yes')}}</option>
            </select>
            @if($errors->has("driver_has_vehicle"))
              <span class="help-block">{{ $errors->first("driver_has_vehicle") }}</span>
            @endif
          </div>
        </div>

       
        <br>
        <div class="row" id="add_vehicle">
          <div class="form-group col-md-4 col-sm-4"></div>
          <div class="form-group col-md-4 col-sm-4">
            <label for="driver-has-vehicle-field">{{__('backend.Click_To_Add_Vehicle_For_This_Driver')}}</label>
            <a class="btn btn-success btn-md btn-block" data-toggle="modal" data-target="#addVehicle" style="background-color: #444; border-color: #000;"><i class="fa fa-truck"></i> {{__('backend.add_vehicle')}}</a>
            <p class="error_vehicle" style="display: none;"> <i class="fa fa-exclamation-triangle"></i> {{__('backend.all_vehicle_data_must_be_filled')}} </b>
          </div>
        </div>
        </br>

        <div class="row">
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('driver_profile')) has-error @endif">
            <div id="driver_profile_upload"></div>
            <label for="driver_profile-field">{{__('backend.profile_image')}}: </label>
            <input required class="uploadPhoto btn btn-primary" type="file" name="driver_profile" id="driver_profile">
            @if($errors->has("driver_profile"))
              <span class="help-block">{{ $errors->first("driver_profile") }}</span>
            @endif
          </div>
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('national_id_image_front')) has-error @endif">
            <div id="national_id_image_front_upload"></div>
            <label for="national_id_image_front-field">{{__('backend.national_id_image_front')}}:</label>
            <input required class="uploadPhoto btn btn-primary" type="file" name="national_id_image_front" id="national_id_image_front">
            @if($errors->has("national_id_image_front"))
              <span class="help-block">{{ $errors->first("national_id_image_front") }}</span>
            @endif
          </div>
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('criminal_record_image_front')) has-error @endif">
            <div id="criminal_record_image_front_upload"></div>
            <label for="criminal_record_image_front-field">{{__('backend.criminal_record_image')}}: </label>
            <input required class="uploadPhoto btn btn-primary" type="file" name="criminal_record_image_front" id="criminal_record_image_front">
            @if($errors->has("criminal_record_image_front"))
              <span class="help-block">{{ $errors->first("criminal_record_image_front") }}</span>
            @endif
          </div>
        </div>



        <!-- Modal -->
        <div class="modal fade addVehicle" id="addVehicle" role="dialog">
          <div class="modal-dialog" style="width: 80%;">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"> {{__('backend.Add_New_Vehicle_For_This_Driver')}} </h4>
              </div>

              <div class="modal-body">
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">

                    <div class="row">
                      <div class="form-group col-md-6 col-sm-6 @if($errors->has('model_name')) has-error @endif">
                        <label for="model_name-field">{{__('backend.Model_Name')}}: </label>
                        <input type="text" id="model_name-field" name="model_name" class="form-control" value="{{ old("model_name") }}"/>
                        @if($errors->has("model_name"))
                          <span class="help-block">{{ $errors->first("model_name") }}</span>
                        @endif
                      </div>
                      <div class="form-group col-md-6 col-sm-6 @if($errors->has('plate_number')) has-error @endif">
                        <label for="plate_number-field">{{__('backend.Plate_Number')}}: </label>
                        <input type="text" id="plate_number-field" name="plate_number" class="form-control" value="{{ old("plate_number") }}"/>
                        @if($errors->has("plate_number"))
                          <span class="help-block">{{ $errors->first("plate_number") }}</span>
                        @endif
                      </div>
                    </div>

                    <div class="row">
                      <div class="form-group col-md-6 col-sm-6 @if($errors->has('chassi_number')) has-error @endif">
                         <label for="chassi_number-field">{{__('backend.Chassi_Number')}}: </label>
                         <input type="text" id="chassi_number-field" name="chassi_number" class="form-control" value="{{ old("chassi_number") }}"/>
                         @if($errors->has("chassi_number"))
                           <span class="help-block">{{ $errors->first("chassi_number") }}</span>
                         @endif
                      </div>
                      <div class="form-group col-md-6 col-sm-6 @if($errors->has('year')) has-error @endif">
                         <label for="year-field">{{__('backend.Model_Year')}}: </label>
                         {{ Form::selectYear('year',  date('Y'),1980,null,["class"=>"form-control", "id"=>"year-field"]) }}
                         @if($errors->has("year"))
                          <span class="help-block">{{ $errors->first("year") }}</span>
                         @endif
                      </div>
                    </div>

                    <div class="row">
                      <div class="form-group col-md-4 col-sm-4 @if($errors->has('type_id')) has-error @endif">
                        <label for="type_id-field">{{__('backend.Vehicle_Type')}}: </label>
                        <select id="vehicle_type_id-field" name="vehicle_type_id" class="form-control" >
                          @if(! empty($truck_types))
                              @foreach($truck_types as $type)
                                <option value="{{$type->id}}" {{ old("vehicle_type_id") == $type->id ? 'selected' : '' }}">{{$type->name_en}}</option>
                              @endforeach
                          @endif
                        </select>
                        @if($errors->has("vehicle_type_id"))
                          <span class="help-block">{{ $errors->first("vehicle_type_id") }}</span>
                        @endif
                      </div>
                      <div class="form-group col-md-4 col-sm-4 @if($errors->has('model_type_id')) has-error @endif">
                        <label for="model_type_id-field">{{__('backend.Model_Type')}}</label>
                        <select id="model_type_id-field" name="model_type_id" class="form-control" >
                        @if(! empty($model_types))
                          @foreach($model_types as $model)
                            <option value="{{$model->id}}" {{ old("model_type_id") == $model->id ? 'selected' : '' }}">{{$model->name}}</option>
                          @endforeach
                        @endif
                        </select>
                        @if($errors->has("model_type_id"))
                          <span class="help-block">{{ $errors->first("model_type_id") }}</span>
                        @endif
                      </div>
                      <div class="form-group col-md-4 col-sm-4 @if($errors->has('color_id')) has-error @endif">
                        <label for="color_id-field">{{__('backend.color')}}: </label>
                        <select id="color_id-field" name="color_id" class="form-control" >
                        @if(! empty($colors))
                          @foreach($colors as $color)
                            <option value="{{ $color->id }}" {{ old("color_id") == $color->id ? 'selected' : '' }}">{{$color->name}}</option>
                          @endforeach
                        @endif
                        </select>
                        @if($errors->has("color_id"))
                          <span class="help-block">{{ $errors->first("color_id") }}</span>
                        @endif
                      </div>
                    </div>

                    <div class="row">
                      <div class="form-group col-md-6 col-sm-6 @if($errors->has('driving_licence_expired_date')) has-error @endif">
                        <label for="driving_licence_expired_date-field">{{__('backend.drivier_licence_expiration_date')}}: </label>
                        <div class='input-group date datepicker2' >
                          <input type="text" id="driving_licence_expired_date-field" name="driving_licence_expired_date"
                                 class="form-control" value="{{ old("driving_licence_expired_date") }}"/>
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                        </div>
                        @if($errors->has("driving_licence_expired_date"))
                          <span class="help-block">{{ $errors->first("driving_licence_expired_date") }}</span>
                        @endif
                      </div>
                      <div class="form-group col-md-6 col-sm-6 @if($errors->has('license_end_date')) has-error @endif">
                        <label for="license_end_date_field">{{__('backend.License_End_Date')}}: </label>
                        <div class='input-group date datepicker2' >
                          <input type="text" id="license_end_date_field" name="license_end_date" class="form-control"  value="{{ old("license_end_date") }}"/>
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                        </div>
                        @if($errors->has("license_end_date"))
                          <span class="help-block">{{ $errors->first("license_end_date") }}</span>
                          @endif
                      </div>
                    </div>

                    <div class="row">
                      <div class="form-group col-md-6 col-sm-6  @if($errors->has('driving_licence_image_front')) has-error @endif">
                        <div id="driving_licence_image_front_upload"></div>
                        <label for="driving_licence_image_front-field">{{__('backend.drivier_licence_image_front')}}: </label>
                        <input required class="uploadPhoto btn btn-primary col-md-12" type="file" name="driving_licence_image_front" id="driving_licence_image_front">
                        @if($errors->has("driving_licence_image_front"))
                          <span class="help-block">{{ $errors->first("driving_licence_image_front") }}</span>
                        @endif
                      </div>
                      <div class="form-group col-md-6 col-sm-6 @if($errors->has('license_image_front')) has-error @endif">
                        <div id="license_image_front_upload"></div>
                        <label for="image-field">{{__('backend.License_Image_Front')}}:</label>
                        <input required class="uploadPhoto btn btn-primary col-md-12" type="file" name="license_image_front" id="license_image_front">
                        @if($errors->has("license_image_front"))
                          <span class="help-block">{{ $errors->first("license_image_front") }}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-primary done_create_vehicle" data-dismiss="modal">{{__('backend.done')}}</button>
              </div>
            </div>

          </div>
        </div>

        <div class="well well-sm col-md-12 col-sm-12 col-xs-12">
            <button type="submit" id="added_driver" class="btn btn-primary">{{__('backend.add_driver')}}</button>
            <a class="btn btn-link pull-right" href="{{ route('mngrAgent.drivers.index') }}"><i class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
        </div>

      </form>
    </div>
  </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(function () {

            $("form[name='driver_create']").validate({
                // Specify validation rules
                rules: {
                    name: {
                        required: true,
                    },
                    email: {
                        required: true,
                        email: true,
                        remote: {
                            url: "{!! url('/check-email-agent') !!}",
                            type: "get"
                        }
                    },
                    mobile: {
                        required: true,
                        number:true,
                        rangelength:[11,11],
                        remote: {
                            url: "{!! url('/check-mobile-agent') !!}",
                            type: "get"
                        }
                    },
                    phone: {
                        number:true,
                        rangelength:[11,11],
                        remote: {
                            url: "{!! url('/check-mobile-agent') !!}",
                            type: "get"
                        }
                    },
                    password: {
                        required: true,
                        minlength: 5
                    },
                    confirm_password: {
                      required: true,
                      equalTo: '#password-field',
                      minlength: 5
                    },
                    country_id: {
                      required: true,
                    },
                    government_id: {
                      required: true,
                    },
                    city_id: {
                      required: true,
                    },
                    address: {
                        required: true,
                    },
                    latitude: {
                        required: true,
                        number:true,
                    },
                    longitude: {
                        required: true,
                        number:true,
                    },
                    national_id_number: {
                        required: true,
                        number:true,
                        rangelength:[14,14],
                    },
                    national_id_expired_date: {
                        date:true,
                        required: true,
                    },
                    profit: {
                        required: true,
                        number:true,
                    },
                    basic_salary: {
                        required: true,
                        number:true,
                    },
                    bouns_of_delivery: {
                        // required: true,
                        number:true,
                    },
                    pickup_price: {
                        // required: true,
                        number:true,
                    },
                    bouns_of_pickup_for_one_order: {
                        // required: true,
                        number:true,
                    },
                    driver_has_vehicle: {
                        required: true,
                    },
                    driver_profile: {
                      required: true,
                    },
                    national_id_image_front: {
                        required: true,
                    },
                    criminal_record_image_front: {
                        required: true,
                    },
                    // driving_licence_image_front: {
                    //     required: true,
                    // },
                    // driving_licence_image_back: {
                    //     required: true,
                    // },
                    // national_id_image_back: {
                    //     required: true,
                    // },
                },

                // Specify validation error messages
                messages: {
                    name: {
                      required: "{{__('backend.Please_Enter_driver_Name')}}",
                    },
                    email: {
                        required: "{{__('backend.Please_Enter_Driver_E-mail')}}",
                        email: "{{__('backend.Please_Enter_Correct_E-mail')}}",
                        remote: jQuery.validator.format("{{__('backend.E-mail_already_exist')}}")
                    },
                    mobile: {
                        rangelength:"{{__('backend.Mobile_Must_Be11_Digits')}}",
                        required: "{{__('backend.Please_Enter_Driver_Mobile_Number')}}",
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                        remote: jQuery.validator.format("{{__('backend.Mobile_Number_already_exist')}}")
                    },
                    phone: {
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                        rangelength:"{{__('backend.Mobile_Must_Be11_Digits')}}",
                        remote: jQuery.validator.format("{{__('backend.Mobile_Number_already_exist')}}")
                    },
                    password: {
                        required: "{{__('backend.Please_Enter_Driver_Password')}}",
                        minlength: "{{__('backend.Password_must_be_more_than5_character')}}"
                    },
                    confirm_password: {
                        required: "{{__('backend.Confirm_password_is_wrong')}}",
                        equalTo: "{{__('backend.Confirm_password_is_wrong')}}",
                    },
                    country_id: {
                      required: "{{__('backend.Please_Chosse_The_Country')}}",
                    },
                    government_id: {
                      required: "{{__('backend.Please_Chosse_The_Government')}}",
                    },
                    city_id: {
                      required: "{{__('backend.Please_Select_City')}}",
                    },
                    address: {
                        required: "{{__('backend.Please_Enter_Driver_Address')}}",
                    },
                    latitude: {
                        required: "{{__('backend.Please_Enter_Latitude')}}",
                        number:"{{__('backend.Latitude_must_be_numbers')}}",
                    },
                    longitude: {
                        required: "{{__('backend.Please_Enter_Longitude')}}",
                        number:"{{__('backend.Longitude_must_be_numbers')}}",
                    },
                  
                    national_id_number: {
                        required: "{{__('backend.Please_Enter_National_Id_Number')}}",
                        number: "{{__('backend.National_Id_Must_Be_Number')}}",
                        rangelength: "{{__('backend.National_Id_Must_Be14_Digits')}}",
                    },
                    national_id_expired_date: {
                        date:"{{__('backend.Please_Enter_Correct_Date_Format')}}",
                        required: "{{__('backend.Please_Enter_National_Id_Expired_Date')}}",
                    },
                    profit: {
                        required: "{{__('backend.Please_Enter_Profit')}}",
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                    },
                    basic_salary: {
                        required: "{{__('backend.Please_Enter_Basic_Salary')}}",
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                    },
                    bouns_of_delivery: {
                        // required: "{{__('backend.Please_Enter_Bonus_Of_Delivery')}}",
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                    },
                    pickup_price: {
                        // required: "{{__('backend.Please_Enter_Pickup_Price')}}",
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                    },
                    bouns_of_pickup_for_one_order: {
                        // required: "{{__('backend.Please_Enter_Bonus_Of_Pickup_For_One_Order')}}",
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                    },
                    driver_has_vehicle: {
                        required: "{{__('backend.Please_Choose_If_Driver_Has_Vehicle_or_No')}}"
                    },
                    driver_profile: {
                        required: "{{__('backend.Please_Enter_Driver_Profile_Image')}}"
                    },
                    national_id_image_front: {
                        required: "{{__('backend.Please_Enter_drivier_national_id_image_front')}}"
                    },
                    criminal_record_image_front: {
                        required: "{{__('backend.please_enter_criminal_record_image_driver')}}"
                    },
                    // driving_licence_image_back: {
                    //     required: "{{__('backend.Please_Enter_drivier_licence_image_back')}}"
                    // },
                    // national_id_image_back: {
                    //     required: "{{__('backend.Please_Enter_drivier_national_id_image_back')}}"
                    // },
                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                },

                highlight: function (element) {
                    $(element).parent().addClass('error')
                },

                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                },

                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                }
            });

            if($( "#driver-has-vehicle-field" ).val() == 1) {
              $('#add_vehicle').css('display', 'block');
            } else {
              $('#add_vehicle').css('display', 'none');
            }

            $( "#driver-has-vehicle-field" ).change(function() {
              if( $(this).val() == 1) {

                  $('#add_vehicle').css('display', 'block');
                  $('#added_driver').attr('disabled', true);
                  $('.error_vehicle').css('display', 'inline-block');

                  // $('#model_name-field', '#plate_number-field', '#chassi_number-field', '#year-field', '#vehicle_type_id-field', '#model_type_id-field', '#color_id-field', '#driving_licence_expired_date-field', '#license_end_date_field', '#driving_licence_image_front', '#license_image_front').blur();

                  $(".done_create_vehicle").click(function(e){

                    var modelNameError            = true,
                        plateNumberError          = true,
                        chassiNumberError         = true,
                        yearError                 = true,
                        vehicleTypeError          = true,
                        modelTypeError            = true,
                        colorError                = true,
                        drivingLicenceDateError   = true,
                        licenseEndDateError       = true,
                        drivingLicenceImageError  = true,
                        licenseImageError         = true;

                        if($('#model_name-field').val() == ''){
                            modelNameError            = true;
                        } else {
                            modelNameError            = false;
                        }

                        if($('#plate_number-field').val() == ''){
                            plateNumberError            = true;
                        } else {
                            plateNumberError            = false;
                        }

                        if($('#chassi_number-field').val() == ''){
                            chassiNumberError            = true;
                        } else {
                            chassiNumberError            = false;
                        }

                        if($('#year-field').val() == ''){
                            yearError            = true;
                        } else {
                            yearError            = false;
                        }

                        if($('#vehicle_type_id-field').val() == ''){
                            vehicleTypeError            = true;
                        } else {
                            vehicleTypeError            = false;
                        }

                        if($('#model_type_id-field').val() == ''){
                            modelTypeError            = true;
                        } else {
                            modelTypeError            = false;
                        }

                        if($('#color_id-field').val() == ''){
                            colorError            = true;
                        } else {
                            colorError            = false;
                        }

                        if($('#driving_licence_expired_date-field').val() == ''){
                            drivingLicenceDateError            = true;
                        } else {
                            drivingLicenceDateError            = false;
                        }

                        if($('#license_end_date_field').val() == ''){
                            licenseEndDateError            = true;
                        } else {
                            licenseEndDateError            = false;
                        }

                        if($('#driving_licence_image_front').val() == ''){
                            drivingLicenceImageError       = true;
                        } else {
                            drivingLicenceImageError       = false;
                        }

                        if($('#license_image_front').val() == ''){
                            licenseImageError            = true;
                        } else {
                            licenseImageError            = false;
                        }

                        if(modelNameError === true || plateNumberError === true || chassiNumberError === true || yearError === true || vehicleTypeError === true || modelTypeError === true || colorError === true || drivingLicenceDateError === true || licenseEndDateError === true || drivingLicenceImageError === true || licenseImageError === true) {
                            // console.log('error');
                            $('#added_driver').attr('disabled', true);
                            e.preventDefault();
                            $('.error_vehicle').css('display', 'inline-block');
                        } else {
                            // console.log('not error');
                            $('#added_driver').attr('disabled', false);
                            $('.error_vehicle').css('display', 'none');
                        }

                  });

              } else {
                  $('#add_vehicle').css('display', 'none');
                  $('#added_driver').attr('disabled', false);
                  $('.error_vehicle').css('display', 'none');
              }
            });

        

            $("#government_id").on('change',function(){
                $.ajax({
                  url: "{{ url('/get_cities')}}"+"/"+$(this).val(),
                  type: 'get',
                  data: {},
                  success: function(data) {
                    $('#city_id').html('<option value="" >Choose City</option>');
                    $.each(data, function(i, content) {
                      $('#city_id').append($("<option></option>").attr("value",content.id).text(content.name_en));
                    });
                  },
                  error: function(data) {
                    console.log('Error:', data);
                  }
                });
            });

        })
    </script>
@endsection

@extends('agent.layouts.app')
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css">
<style>
    .jconfirm.jconfirm-modern .jconfirm-box div.jconfirm-title-c .jconfirm-icon-c {
    -webkit-transition: -webkit-transform .5s;
    transition: -webkit-transform .5s;
    transition: transform .5s;
    transition: transform .5s,-webkit-transform .5s;
    -webkit-transform: scale(0);
    transform: scale(0);
    display: block;
    margin-right: 0;
    margin-left: 0;
    margin-bottom: 10px;
    font-size: 69px;
    color: #9e1717;
}
.tab-content
{
  padding-top: 20px;
}
.nav-tabs li:first-child.active a
{
  color: #fff;background-color: #337ab7;
}
.nav-tabs li:last-child.active a
{
  color: #fff;background-color: #dd4b39;
}
</style>
@endsection
@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> Pilot Orders <span style="color: #5810c3;" >{{! empty($driver) ? ' of Driver:: '.$driver->name.' - '.$driver->mobile : ''}} </span>
        </h3>
    </div>
@endsection

@section('content')

<div>
  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
      <div class="row">
          <div class="col-md-12 col-xs-12 col-sm-12">
              @if(count($orders) > 0)
              <table class="table table-condensed table-striped text-center">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Received Date</th>
                    <th>Order Type</th>
                    <th>Order Price</th>
                    <th>Receiver Name</th>
                    <th>Status</th>
                    <th>Follow</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($orders as $order)
                    <tr>
                      <td>{{$order->order_id}}</td>
                      <td>{{$order->driver_name}}</td>
                      <td>{{$order->received_date}}</td>
                      <td>{{$order->type}}</td>
                      <td>{{$order->order_price}}</td>
                      <td>{{$order->receiver_name}}</td>
                      <td>
                        @if($order->order_status == 0 )
                          <span class='badge badge-pill badge-black'>Pending</span>
                        @elseif($order->order_status == 1 && $order->accepted_orders_status == 0)
                          <span class='badge badge-pill badge-info'>Accepted</span>
                        @elseif($order->order_status == 2 && $order->accepted_orders_status == 1)
                          <span class='badge badge-pill badge-warning'>Received</span>
                        @elseif($order->order_status == 3 && $order->accepted_orders_status == 2)
                          <span class='badge badge-pill badge-success'>Delivered</span>
                        @elseif($order->order_status == 1)
                          <span class='badge badge-pill badge-danger'>Cancelled</span>
                        @elseif($order->order_status == 2)
                          <span class='badge badge-pill badge-danger'>Recalled</span>
                        @endif
                      </td>
                      <td><a class="btn btn-xs btn-primary" href="{{url('mngrAgent/showpilotdriver',['driver' => $order->driver_id , 'order_id' => $order->order_id , 'delv_id' => $order->delivery_price , 'acc_id' => $order->acc_id]) }}"><i class="glyphicon glyphicon-thumbs-up"></i> follow</a></td>
                    </tr>

                  @endforeach
                </tbody>
              </table>
              {{$orders->links()}}
              @else
                  <h3 class="text-center alert alert-warning">No Result Found !</h3>
              @endif
          </div>
      </div>
      <!-- end -->
    </div>
@endsection

@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script>
$('#myTabs a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})


</script>

@endsection

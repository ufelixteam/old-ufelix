<div class="modal" tabindex="-1" role="dialog" id="map1Modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Select Receiver Location</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
       <div class="col-md-12 col-xs-12 col-sm-12">
        <div id="map1" style="width:100%;height:400px"></div>
      </div>
      </div>
      <div class="modal-footer">
     
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Select and Close</button>
      </div>
    </div>
  </div>
</div><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/dialog-map1.blade.php ENDPATH**/ ?>
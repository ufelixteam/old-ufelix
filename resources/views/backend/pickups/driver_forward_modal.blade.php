<!-- **** Modal **** -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{__('backend.Forward_pickup')}}</h5>
            </div>
            <div class="modal-body">
                <form class="" action="{{ url('mngrAdmin/forward_pickup') }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" class="form-control" name="ids" value="" id="forward_ids">
                    <input type="hidden" class="form-control" name="pickup_id" value="{{@$pickup->id}}">
                    <div class="form-group">
                        <select id="driver_id-field" name="driver_id" id="driver_id" class="form-control">
                            @if(! empty($drivers))
                            @foreach($drivers as $driver)
                                <option value="{{$driver->id}}" {{ @$pickup->driver_id  != '' && $pickup->driver_id  == $driver->id ? 'selected' : ''}}
                                >{{$driver->name}}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">{{__('backend.forward')}}</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">{{__('backend.close')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

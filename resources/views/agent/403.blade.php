@extends('agent.layouts.app')

@section('content')
<div class="col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1 main">

    <section class="content-header">
      <h1>
        403 Error Page
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{URL::asset('/mngrAgent/dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        
        <li class="active">403 Error</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="error-page">
        <h2 class="headline text-yellow"> 403</h2>

        <div class="error-content">
          <h3><i class="fa fa-warning text-yellow"></i> OPPs! Something Error!</h3>

          <p>
            You are Not Allwoed to Access this page
          </p>


        </div>
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
    </section>
    <!-- /.content -->

  
  </div>
@endsection
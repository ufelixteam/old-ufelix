@extends('backend.layouts.app')
@section('css')

@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i>  Edit Weights #{{$weight->id}}</h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <form action="{{ route('mngrAdmin.weights.update', $weight->id) }}" method="POST" name='validateForm'>
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group col-md-6 col-sm-6 @if($errors->has('weight')) has-error @endif">
                       <label for="weight-field">Weight</label>
                    <input type="text" id="weight-field" name="weight" class="form-control" value="{{ is_null(old("weight")) ? $weight->weight : old("weight") }}"/>
                       @if($errors->has("weight"))
                        <span class="help-block">{{ $errors->first("weight") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('status')) has-error @endif">
                       <label for="status-field">Status</label>
                    
                     <select id="status-field" name="status" class="form-control"  >
                        <option value="0" {{ old("status") == "0" ||  $weight->status == "0" ? 'selected' : '' }}>No</option>
                        <option value="1" {{ old("status") == "1" ||  $weight->status == "1" ? 'selected' : '' }}>Yes</option>
                      </select>
                       @if($errors->has("status"))
                        <span class="help-block">{{ $errors->first("status") }}</span>
                       @endif
                    </div>
                <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.weights.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
   <script type="text/javascript">
  $(function() {
    $("form[name='validateForm']").validate({
      // Specify validation rules
      rules: {
    
        weight: {
          required: true,

        },
         
      
      },
      // Specify validation error messages
      messages: {

        weight: {
          required: "Please Enter  Weight",

        },

      },

       errorPlacement: function(error, element) {
          $(element).parents('.form-group').append(error)
      },

      highlight: function (element) {
          $(element).parent().addClass('error')
      },
      unhighlight: function (element) {
          $(element).parent().removeClass('error')
      },
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function(form) {

          form.submit();
     
      }
    });
  })
</script>
@endsection

<!-- Start Header -->
<header>

  <a href="<?php echo e(app()->getLocale() == 'en' ? url('/en/') : url('/')); ?>" class="logo navbar-brand">
      <img src="<?php echo e(asset('/profile/images/logo.png')); ?>" lat="" />
    </a>
	<ul id="header-actions" class="clearfix">
		<li class="list-box hidden-xs dropdown">
             	<a id="drop2" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">
             		<i class=" icon-bell"></i> 
             	</a>
             	<span class="info-label blue-bg"><?php echo e(! empty($count_notification) ? $count_notification : '0'); ?></span>
             	<ul class="dropdown-menu imp-notify">
             		<li class="dropdown-header"> <?php echo app('translator')->getFromJson('website.youhave'); ?> <?php echo e(! empty($count_notification) ? $count_notification : '0'); ?> <?php echo app('translator')->getFromJson('website.notifications'); ?></li>
                <?php if(! empty($app_notification)): ?>
                  <?php $__currentLoopData = $app_notification; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notification): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                	<li>
                      <div class="icon">
                          <i class=" icon-info"></i> 
                      </div>
                   	<div class="details">
                          <strong class="text-warning"><?php echo e($notification->title); ?></strong> 
                          <span><?php echo e($notification->message); ?></span>
                      </div>
                	</li>

                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                	<li class="dropdown-footer"><a href="<?php echo e(app()->getLocale() == 'en' ? url('/en/customer/account/notifications') : url('/customer/account/notifications')); ?>"><?php echo app('translator')->getFromJson('website.seeallnotification'); ?></a></li>
                  <?php endif; ?>
             	</ul>
		        </li>
          
          <li class="list-box user-admin hidden-xs dropdown">
             <div class="admin-details">
                <div class="name"><?php echo e(Auth::guard('Customer')->user() ?  Auth::guard('Customer')->user()->name : ''); ?></div>
                <div class="designation"><?php echo e(Auth::guard('Customer')->user() && Auth::guard('Customer')->user()->Corporate ?  Auth::guard('Customer')->user()->Corporate->name : ''); ?></div>
             </div>
             <a id="drop4" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">
             		<i class="icon-user"></i>
             </a>
             <ul class="dropdown-menu sm">
                <li class="dropdown-content">
                <a href="<?php echo e(app()->getLocale() == 'en' ? url('/en/customer/account/changepassword') : url('/customer/account/changepassword')); ?>"><?php echo app('translator')->getFromJson('website.changepassword'); ?></a> 
                <a href="<?php echo e(route(\Illuminate\Support\Facades\Route::currentRouteName(),  app()->getLocale() == 'en' ? '':'en' )); ?>" ><?php echo e(app()->getLocale() == 'en' ? __('website.Arabic') : __('website.English')); ?></a>
                <a href="<?php echo e(app()->getLocale() == 'en' ? url('/en/customer/account/logout') : url('/customer/account/logout')); ?>"><?php echo app('translator')->getFromJson('website.Logout'); ?></a></li>
             </ul>
          </li>
          <li>
          	<button type="button" id="toggleMenu" class="toggle-menu">
              	<i class="collapse-menu-icon"></i>
              </button>
          </li>
       </ul>
</header>
  <!-- End Header --><?php /**PATH /home/www/ufelix/ufelix/resources/views/frontend/profile/layouts/header.blade.php ENDPATH**/ ?>
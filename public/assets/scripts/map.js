function myMap() {

    var mapCanvas = document.getElementById("map");

    var latitude = document.getElementById("latitude").value;

    var longitude = document.getElementById("longitude").value;
    var myCenter = new google.maps.LatLng(latitude, longitude);

    //var myCenter = new google.maps.LatLng(51.508742,-0.120850);

    var mapOptions = {center: myCenter, zoom: 14};
    var map = new google.maps.Map(mapCanvas, mapOptions);
    var marker = new google.maps.Marker({
        position: myCenter,
        draggable: true,
    });
    marker.setMap(map);
    // geocodePosition(marker.getPosition());
    new google.maps.event.addListener(marker, 'dragend', function () {

        // geocodePosition(marker.getPosition());
        $("#latitude").val(this.getPosition().lat());
        $("#longitude").val(this.getPosition().lng());

    });

    geocoder1 = new google.maps.Geocoder();
    var search1 = document.getElementById("receiver_address_autocomplete");
    var autocomplete1 = new google.maps.places.Autocomplete(search1);
    autocomplete1.addListener('place_changed', function () {
        var place1 = autocomplete1.getPlace();
        if (place1.geometry) {
            map.setCenter(place1.geometry.location);
            marker.setPosition(place1.geometry.location);
            new google.maps.event.trigger(marker, 'dragend');
        }
    });

    //var geoloccontrol = new klokantech.GeolocationControl(map, 20);

}

function geocodePosition(pos) {
    geocoder = new google.maps.Geocoder();
    geocoder.geocode({
        latLng: pos

    }, function (responses) {
        if (responses && responses.length > 0) {
            //   updateMarkerAddress(responses[0].formatted_address);
            $("#address").val(responses[0].formatted_address);
        } else {
            // updateMarkerAddress('Cannot determine address at this location.');
        }
    });
}

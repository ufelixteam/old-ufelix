@extends('backend.layouts.app')

@section('css')
    <title>{{__('backend.vehicles')}} - {{__('backend.edit')}}</title>
@endsection

@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}} {{__('backend.vehicle')}}
            - {{$truck->model_name}}</h3>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <form action="{{ route('mngrAdmin.trucks.update', $truck->id) }}" name="truck_create" method="POST"
                  enctype="multipart/form-data">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('model_name')) has-error @endif">
                        <label for="model_name-field">{{__('backend.Model_Name')}} : </label>
                        <input type="text" id="model_name-field" name="model_name" class="form-control"
                               value="{{ is_null(old("model_name")) ? $truck->model_name : old("model_name") }}"/>
                        @if($errors->has("model_name"))
                            <span class="help-block">{{ $errors->first("model_name") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('plate_number')) has-error @endif">
                        <label for="plate_number-field">{{__('backend.Plate_Number')}}: </label>
                        <input type="text" id="plate_number-field" name="plate_number" class="form-control"
                               value="{{ is_null(old("plate_number")) ? $truck->plate_number : old("plate_number") }}"/>
                        @if($errors->has("plate_number"))
                            <span class="help-block">{{ $errors->first("plate_number") }}</span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('chassi_number')) has-error @endif">
                        <label for="chassi_number-field">{{__('backend.Chassi_Number')}}: </label>
                        <input type="text" id="chassi_number-field" name="chassi_number" class="form-control"
                               value="{{ is_null(old("chassi_number")) ? $truck->chassi_number : old("chassi_number") }}"/>
                        @if($errors->has("chassi_number"))
                            <span class="help-block">{{ $errors->first("chassi_number") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('year')) has-error @endif">
                        <label for="year-field">{{__('backend.Model_Year')}}: </label>
                        {{ Form::selectYear('year',  date('Y'),1980,$truck->year ,["class"=>"form-control"]) }}
                        @if($errors->has("year"))
                            <span class="help-block">{{ $errors->first("year") }}</span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('status')) has-error @endif">
                        <label for="status-field">{{__('backend.is_active')}}</label>
                        <select id="status-field" name="status" class="form-control" value="{{ old("status")}}">
                            <option
                                value="0" {{ old("status") == "0" || $truck->status == "0" ? 'selected' : '' }}>{{__('backend.no')}}</option>
                            <option
                                value="1" {{ old("status") == "1" || $truck->status == "1" ? 'selected' : '' }}>{{__('backend.yes')}}</option>
                        </select>
                        @if($errors->has("status"))
                            <span class="help-block">{{ $errors->first("status") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('type_id')) has-error @endif">
                        <label for="vehicle_type_id-field">{{__('backend.Vehicle_Type')}}: </label>
                        <select id="vehicle_type_id-field" name="vehicle_type_id" class="form-control">
                            @if(! empty($truck_types))
                                @foreach($truck_types as $type)
                                    <option
                                        value="{{$type->id}}" {{ old("vehicle_type_id") == $type->id  || $truck->vehicle_type_id == $type->id  ? 'selected' : '' }}>{{$type->name_en}}</option>
                                @endforeach
                            @endif
                        </select>
                        @if($errors->has("vehicle_type_id"))
                            <span class="help-block">{{ $errors->first("vehicle_type_id") }}</span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('color_id')) has-error @endif">
                        <label for="color_id-field">{{__('backend.color')}}: </label>
                        <select id="color_id-field" name="color_id" class="form-control">
                            @if(! empty($colors))
                                @foreach($colors as $color)
                                    <option
                                        value="{{$color->id}}" {{ $truck->color_id == $color->id ? 'selected' : '' }}>{{$color->name}}</option>
                                @endforeach
                            @endif
                        </select>
                        @if($errors->has("color_id"))
                            <span class="help-block">{{ $errors->first("color_id") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('model_type_id')) has-error @endif">
                        <label for="model_type_id-field">{{__('backend.Model_Type')}}: </label>
                        <select id="model_type_id-field" name="model_type_id" class="form-control">
                            @if(! empty($model_types))
                                @foreach($model_types as $model)
                                    <option
                                        value="{{$model->id}}" {{ old("model_type_id") == $model->id || $truck->model_type_id == $model->id? 'selected' : '' }}>{{$model->name}}</option>
                                @endforeach
                            @endif
                        </select>
                        @if($errors->has("model_type_id"))
                            <span class="help-block">{{ $errors->first("model_type_id") }}</span>
                        @endif
                    </div>
                </div>
{{--                <div class="row">--}}
{{--                    <div hidden class="form-group col-md-6 col-sm-6 @if($errors->has('agent_id')) has-error @endif">--}}
{{--                        <label for="agent_id-field">{{__('backend.Belonging_Agent')}}: </label>--}}
{{--                        <select id="agent_id-field" name="agent_id" class="form-control">--}}
{{--                            <option value="-1">Choose Agent</option>--}}
{{--                            @if(! empty($agents))--}}
{{--                                @foreach($agents as $agent)--}}
{{--                                    <option--}}
{{--                                        value="{{$agent->id}}" {{ old("agent_id") == $agent->id || $truck->agent_id == $agent->id? 'selected' : '' }}>{{$agent->name}}</option>--}}
{{--                                @endforeach--}}
{{--                            @endif--}}
{{--                        </select>--}}
{{--                        @if($errors->has("agent_id"))--}}
{{--                            <span class="help-block">{{ $errors->first("agent_id") }}</span>--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                    @if($truck->driver_id == 0)--}}
{{--                        <div class="form-group col-md-6 col-sm-6 @if($errors->has('agent_id')) has-error @endif">--}}
{{--                            <label for="agent-id-field">{{__('backend.agent')}}: </label>--}}
{{--                            <select id="agent-id-field" name="agent_id" class="form-control">--}}
{{--                                @if(! empty($agents))--}}
{{--                                    @foreach($agents as $agent)--}}
{{--                                        <option--}}
{{--                                            value="{{$agent->id}}" {{ old("agent_id") == $agent->id || $truck->agent_id == $agent->id ? 'selected' : '' }}> {{$agent->name}} </option>--}}
{{--                                    @endforeach--}}
{{--                                @endif--}}
{{--                            </select>--}}
{{--                            @if($errors->has("agent_id"))--}}
{{--                                <span class="help-block">{{ $errors->first("agent_id") }}</span>--}}
{{--                            @endif--}}
{{--                        </div>--}}
{{--                    @endif--}}
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('license_end_date')) has-error @endif">
                        <label for="license_end_date_field">{{__('backend.License_End_Date')}}: </label>
                        <div class='input-group date datepicker2'>
                            <input type="text" name="license_end_date" class="form-control date"
                                   value="{{ !empty($truck->license_end_date) ? $truck->license_end_date : "" }}"/>
                            @if($errors->has("license_end_date"))
                                <span class="help-block">{{ $errors->first("license_end_date") }}</span>
                            @endif
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                <!--
          <div class="form-group col-md-12 col-sm-6 @if($errors->has('driver_id')) has-error @endif">
            <label for="driver_id-field">{{__('backend.Owner_Driver')}}: </label>
            <select id="driver_id-field" name="driver_id" class="form-control" >
              @if(! empty($drivers))
                    @foreach($drivers as $driver)
                        <option value="{{$driver->id}}" {{ old("driver_id") == $driver->id || $truck->driver_id == $driver->id ? 'selected' : '' }}>{{$driver->name}} - {{$driver->mobile}}</option>
                @endforeach
                @endif
                    </select>
@if($errors->has("driver_id"))
                    <span class="help-block">{{ $errors->first("driver_id") }}</span>
            @endif
                    </div>
                    <input type="hidden" name="old_driver_id" value="{{$truck->driver_id}}" >
          -->
                <!-- <div class="form-group col-md-6 col-sm-6 @if($errors->has('license_start_date')) has-error @endif">
            <label for="license_start_date_field">{{__('backend.License_Start_Date')}}: </label>
            <div class='input-group date ' id="datepicker">
              <input type="text" name="license_start_date" value="{{ !empty($truck->license_start_date) ? $truck->license_start_date : "" }}" class="form-control date"  />
              @if($errors->has("license_start_date"))
                    <span class="help-block">{{ $errors->first("license_start_date") }}</span>
              @endif
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                  </div>
                </div> -->
                </div>

                <input type="hidden" name="driver_id" value="{{$truck->driver_id}}"

                <div class="row">
                <!-- <div class="form-group col-md-6 col-sm-6 @if($errors->has('image')) has-error @endif">
            <div id="image_upload">
              @if($truck->image)
                    <div class="imgDiv">
                      <input type="hidden" value="{{ $truck->image }}" name="image_old" id="image_old">
                  <img src="{{ asset('public/backend/images/'.$truck->image) }}" style="width:100px;height:100px">
                  <button class="btn btn-danger cancel">&times;</button>
                </div>
              @endif
                    </div>
                    <label for="image-field">{{__('backend.Vehicle_Image')}}: </label>
            <input class="uploadPhoto btn btn-primary" type="file"name="image" id="image">
            @if($errors->has("image"))
                    <span class="help-block">{{ $errors->first("image") }}</span>
            @endif
                    </div> -->
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('license_image_front')) has-error @endif">
                        <div id="license_image_front_upload">
                            @if($truck->license_image_front)
                                <div class="imgDiv">
                                    <input type="hidden" value="{{ $truck->license_image_front }}"
                                           name="license_image_front_old" id="license_image_front_old">
                                    <img src="{{ $truck->license_image_front }}" style="width:100px;height:100px">
                                    <button class="btn btn-danger cancel">&times;</button>
                                </div>
                            @endif
                        </div>
                        <label for="image-field">{{__('backend.License_Image_Front')}}</label>
                        <input class="uploadPhoto btn btn-primary" type="file" name="license_image_front"
                               id="license_image_front">
                        @if($errors->has("license_image_front"))
                            <span class="help-block">{{ $errors->first("license_image_front") }}</span>
                        @endif
                    </div>
                <!-- <div class="form-group col-md-4 col-sm-6 @if($errors->has('license_image_back')) has-error @endif">
            <div id="license_image_back_upload">
              @if($truck->license_image_back)
                    <div class="imgDiv">
                      <input type="hidden" value="{{ $truck->license_image_back }}" name="license_image_back_old" id="license_image_back_old">
                <img src="{{ asset('public/backend/images/'.$truck->license_image_back) }}" style="width:100px;height:100px">
                <button class="btn btn-danger cancel">&times;</button>
              </div>
              @endif
                    </div>
                    <label for="license_image_back">{{__('backend.License_Image_Back')}}</label>
            <input class="uploadPhoto btn btn-primary" type="file" name="license_image_back" id="license_image_back">
            @if($errors->has("license_image_back"))
                    <span class="help-block">{{ $errors->first("license_image_back") }}</span>
            @endif
                    </div> -->
                </div>
                <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
                    <button type="submit" class="btn btn-primary">{{__('backend.save_edits')}}</button>
                    <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.trucks.index') }}"><i
                            class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function () {
            $("form[name='truck_create']").validate({
                // Specify validation rules
                rules: {
                    model_name: {
                        required: true
                    },
                    plate_number: {
                        required: true,
                    },
                    license_start_date: {
                        required: true,
                        date: true
                    },
                    license_end_date: {
                        required: true,
                        date: true
                    },
                    chassi_number: {
                        required: true,
                    },
                    vehicle_type_id: {
                        required: true
                    },
                    year: {
                        required: true
                    },
                    color_id: {
                        required: true
                    },
                    model_type_id: {
                        date: true
                    },
                    driver_id: {
                        required: true
                    },
                    image: {
                        // required: true
                    },
                    license_image_front: {
                        // required: true
                    },
                    // license_image_back: {
                    //      required: true
                    // },
                },
                // Specify validation error messages
                messages: {
                    password: {
                        required: "{{__('backend.Please_Enter_Driver_Password')}}",
                        minlength: "{{__('backend.Password_must_be_more_than5_character')}}"
                    },
                    model_name: {
                        required: "{{__('backend.Please_Enter_Model_Name')}}"
                    },
                    plate_number: {
                        required: "{{__('backend.Please_Enter_Plate_Number')}}",
                    },
                    chassi_number: {
                        required: "{{__('backend.Please_Enter_Chassi_Number')}}",
                    },
                    vehicle_type_id: {
                        required: "{{__('backend.Please_Enter_Vehicle_Type')}}"
                    },
                    year: {
                        required: "{{__('backend.Please_Enter_Year')}}"
                    },
                    color_id: {
                        required: "{{__('backend.Please_Enter_Color')}}"
                    },
                    model_type_id: {
                        required: "{{__('backend.Please_Enter_Model_Type')}}"
                    },
                    driver_id: {
                        required: "{{__('backend.Please_Enter_Driver')}}"
                    },
                    license_start_date: {
                        required: "{{__('backend.Pleas_Enter_License_Start_Date')}}",
                        date: "{{__('backend.Please_enter_avalid_date')}}"
                    },
                    license_end_date: {
                        required: "{{__('backend.Pleas_Enter_License_End_Date')}}",
                        date: "{{__('backend.Please_enter_avalid_date')}}"
                    },
                    image: {
                        required: "{{__('backend.Pleas_Chosse_Vehicle_Image')}}"
                    },
                    license_image_front: {
                        required: "{{__('backend.Pleas_Chosse_License_Image_Front')}}"
                    },
                    // license_image_back: {
                    //     required: "{{__('backend.Pleas_Chosse_License_Image_Back')}}"
                    // },
                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                },

                highlight: function (element) {
                    $(element).parent().addClass('error')
                },
                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                }
            });
        })
    </script>

@endsection

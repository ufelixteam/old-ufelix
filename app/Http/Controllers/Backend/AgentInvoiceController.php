<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;

use App\Models\AgentInvoiceOrder;
use App\Models\AgentInvoice;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Order;
use Response;
use Flash;
use File;
use Auth;

class AgentInvoiceController extends Controller {

	private $guardType;
	public function __construct(Request $request)
    {
    	if($request->is('mngrAdmin/*') || $request->is('admin/*') ){
    		\Config::set('auth.defaults.guard','admin');

    	}elseif ($request->is('mngrAgent/*') || $request->is('agent/*')) {
    		\Config::set('auth.defaults.guard','agent');
    	}
        $this->guardType = \Config::get('auth.defaults.guard');
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	    $user = Auth::guard($this->guardType)->user();

    	$agent_invoices = AgentInvoice::orderBy('id', 'desc')->paginate(50);
		if ($user->roles->first()->name == 'admin') {

		    return view('backend.agent_invoices.index', compact('agent_invoices'));
		}else{
		    return view('agent.agent_invoices.index', compact('agent_invoices'));
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('backend.agent_invoices.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
			$agent_invoice = new AgentInvoice();

			$agent_invoice->from_date = $request->input("from_date");
	        $agent_invoice->to_date = $request->input("to_date");
	        $agent_invoice->profit = $request->input("profit");
	        $agent_invoice->total_income = $request->input("total_income");
	        $agent_invoice->status = $request->input("status");
	        $agent_invoice->agent_id = $request->input("agent_id");

			$agent_invoice->save();

        	Flash::success(__('backend.AgentInvoice_saved_successfully'));
			return redirect()->route('mngrAdmin.agent_invoices.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$orders =[];
		$agent_invoice = AgentInvoice::findOrFail($id);
		$ids = AgentInvoiceOrder::where('invoice_id',$agent_invoice->id)->pluck('order_id')->toArray();
		//
		if(count($ids)){
			$orders = Order::whereIn('id',$ids)->get()->groupBy(function($item){ return $item->created_at->format('d M Y'); });
			//dd($orders);die();
		}
		$user = Auth::guard($this->guardType)->user();

		if ($user->roles->first()->name == 'admin') {

		    return view('backend.agent_invoices.show', compact('agent_invoice','orders'));
		}else{
		    return view('agent.agent_invoices.show', compact('agent_invoice','orders'));
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$agent_invoice = AgentInvoice::findOrFail($id);

		return view('backend.agent_invoices.edit', compact('agent_invoice'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$agent_invoice = AgentInvoice::findOrFail($id);

		$agent_invoice->from_date = $request->input("from_date");
        $agent_invoice->to_date = $request->input("to_date");
        $agent_invoice->profit = $request->input("profit");
        $agent_invoice->total_income = $request->input("total_income");
        $agent_invoice->status = $request->input("status");
        $agent_invoice->agent_id = $request->input("agent_id");

		$agent_invoice->save();

       	Flash::warning(__('backend.AgentInvoice_Updated_successfully'));

		return redirect()->route('mngrAdmin.agent_invoices.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$agent_invoice = AgentInvoice::findOrFail($id);
		$agent_invoice->delete();

        flash(__('backend.AgentInvoice_deleted_successfully'))->error();

		return redirect()->route('mngrAdmin.agent_invoices.index');
	}

}

@extends('frontend.profile.layouts.app')
@section('title') @lang('website.invoices') @endsection
@section('content')
<div class="row">
	<div class="col-12">
		<div class="">
			<div class="table-responsive m-t-40">
				<h1 class="title">@lang('website.invoices')</h1>

				<table id="example23" class="display nowrap table table-hover table-striped table-bordered" 
                            cellspacing="0" width="100%">
					<thead>
						<tr>
							<th scope="col"> @lang('website.ID')</th>
							<th scope="col"> @lang('website.StartDate')</th>
							<th scope="col"> @lang('website.EndDate')</th>
							<th scope="col"> @lang('website.Status')</th>
							<th scope="col"> @lang('website.Actions')</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($invoices as $invoice)
						<tr>
							<th scope="row">{{$invoice->id}}</th>
							<td>{{$invoice->start_date}}</td>
							<td>{{$invoice->end_date}}</td>
							<td>
								{!! $invoice->status_span !!}
							</td>
							<th>
								<a href="{{ url('/customer/profile/invoices' ,$invoice->id)}}" class="text-inverse p-r-10" data-toggle="tooltip" title="" data-original-title="show" aria-describedby="tooltip468946">
									<i class="icon-eye"></i>
								</a>
							</th>
						</tr>

						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

@endsection
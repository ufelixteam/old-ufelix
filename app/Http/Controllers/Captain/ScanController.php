<?php


namespace App\Http\Controllers\Captain;

use App\Events\OrderUpdated;
use App\Models\AcceptedOrder;

use App\Models\Driver;
use App\Models\Order;
use Auth;
use Config;
use DB;
use Illuminate\Http\Request;

class ScanController extends Controller
{

    private $guardType;

    public function __construct(Request $request)
    {
        Config::set('auth.defaults.guard', 'captain');
        $this->guardType = Config::get('auth.defaults.guard');

    }

    public function scan_page(Request $request)
    {
        $user = Auth::guard($this->guardType)->user();

        $code = [];

        if (app('request')->has('code') && app('request')->input('code')) {
            $code = array_map('trim', explode(',', app('request')->input('code')));
        }

        /** type to display any order  */
        $orders = null;

        if (!empty($code) && isset($code)) {
            $user = Auth::guard($this->guardType)->user();
            $ownDrivers = Driver::where(function($query) use($user){
                $query->where('id', $user->id)
                    ->orWhere('manager_id', $user->id);
            })->pluck('id')->toArray();

            $orders = Order::join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
                ->whereIn('accepted_orders.driver_id', $ownDrivers)
                ->where('orders.status', 2)
                ->where('accepted_orders.status', 2)
                ->with('comments', 'to_government', 'driver')
                ->select('orders.*')
                ->where(function ($query) use ($code) {
                    $query->whereIn('order_number', $code)
                        ->orWhereIn('order_no', $code);
                    foreach ($code as $item) {
                        if (is_numeric($item) && strlen($item) % 2 == 0) {
                            $query->orWhere('order_number', '=', substr($item, 1));
                        }
                    }
                })
                ->get();

        }

        $online_drivers = Driver::where('is_active', 1)
            ->where(function ($query) use ($user) {
                $query->where('manager_id', $user->id)
                    ->Orwhere('id', $user->id);
            })
            ->select('drivers.*')
            ->orderBy('drivers.name', 'ASC')
            ->get();

        /** if this ajax request mean that scan button pressed */
        if ($request->ajax()) {

            $data = [
                'count' => !empty($orders) ? count($orders) : [],
                'ids' => (!empty($orders) && count($orders) ? $orders->pluck('id')->toArray() : [])
            ];

            if (app('request')->input('init') == 1) {
                $data['view'] = view('captain.scan.scan-table', compact('orders', 'online_drivers'))->render();
            } else {
                if (!empty($orders) && count($orders)) {
                    foreach ($orders as $order) {
                        $data['views'][] = [
                            'view' => view('captain.scan.scan-tr', compact('order', 'online_drivers'))->render(),
                            'id' => $order->id
                        ];
                    }
                }
            }

            return $data;
        }

        // display scan page if request get from browser not ajax
        return view('captain.scan.scan', compact('online_drivers'));
    }/*scan_page*/

    public function scan_validation(Request $request)
    {
        $data = ['result' => true];
        $user = Auth::guard($this->guardType)->user();
        $ownDrivers = Driver::where(function($query) use($user){
            $query->where('id', $user->id)
                ->orWhere('manager_id', $user->id);
        })->pluck('id')->toArray();
        $ids = app('request')->input('ids');
        if (is_array($ids) && count($ids)) {
            if (app('request')->input('type') == 'forward') {
                $orders = Order::join('accepted_orders', 'accepted_orders.order_id', '=', 'orders.id')
                    ->whereIn('accepted_orders.driver_id', $ownDrivers)
                    ->where('orders.status', 2)
                    ->where('accepted_orders.status', 2)
                    ->whereIn('orders.id', $ids)
                    ->get();

                if (count($orders) != count($ids)) {
                    $data['result'] = false;
                }
            }
        }

        return $data;
    }

    public function scan_action(Request $request)
    {
        $data = ['ids' => []];

        $ids = app('request')->input('ids');
        if (is_array($ids) && count($ids)) {
            if (app('request')->input('type') == 'forward' && app('request')->input('driver_id')) {
                DB::beginTransaction();
                try {
                    $orders = Order::whereIn('id', $ids)->get();
                    AcceptedOrder::whereIn('order_id', $ids)->update(
                        [
                            'status' => 4
                        ]
                    );
                    foreach ($orders as $key => $order) {
                        AcceptedOrder::create(
                            [
                                'order_id' => $order->id,
                                'driver_id' => app('request')->input('driver_id'),
                                'status' => 2,
                                'old_status' => 2,
                                'delivery_price' => $order->delivery_price
                            ]
                        );

                    }

                    Order::whereIn('id', $ids)->update(['received_at' => date('Y-m-d H:i:s')]);

                    event(new OrderUpdated($ids, Auth::guard($this->guardType)->user()->id, 3, 'receive_orders'));

                    $data['ids'] = $ids;
                    $data['result'] = true;
                    $data['status'] = "<span class='badge badge-pill label-warning'>" . __('captain.received') . "</span>";
                    $data['message'] = __('captain.Order_Routed_saved_successfully');

                    $data['driver'] = Driver::find(app('request')->input('driver_id'));

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    $data['result'] = false;
                    $data['message'] = __('captain.Something_Error');
                }
            }
        }

        return $data;
    }
}

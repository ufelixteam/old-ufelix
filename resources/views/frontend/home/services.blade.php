<!-- Start Services -->

<section id="services" class="services-x">
	<div class="container">
    	<!-- Start Title -->
    	<div class="col-xs-12 test">
        	<div class="title">
            	<h3>@lang('website.services')</h3>
                <p>
                	 @if(! empty($app_about) )  {!!  app()->getLocale() == 'en' ? $app_about['service'] : $app_about['service_ar'] !!} @endif
                </p>
            </div>
        </div>
        <!-- End Title -->

        @if(! empty($services ) && count($services) > 0)
            @foreach($services as $service)
                <div class="col-md-4 col-xs-12">
                	<a href="#" class="block-serv">
                    	<div class="img-block">
                        	<div class="img" style="background-image: url({{$service->image}});"></div>
                        </div>
                        <div class="block-details">
                        	<h2 class="pre-wrap">{{$service->title}}</h2>
                            <p class="pre-wrap">
                                {{$service->description}}
                            </p>
                        </div>
                    </a>
                </div>
            @endforeach

        @endif


    </div>
</section>
<!-- End Services -->

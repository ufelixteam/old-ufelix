<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta name="author" content="Ufelix Team">
        <meta name="description" content="Ufelix - ufelix">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php echo $__env->yieldContent('title'); ?></title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo e(asset('/website/images/flogo.png')); ?>" />
        <link href="https://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Tajawal" rel="stylesheet">

        <link href="<?php echo e(asset('/website/css/style.css')); ?>" rel="stylesheet" type="text/css" />

        <?php if( app()->getLocale() == 'en' ): ?>
            <link href="<?php echo e(asset('/website/css/style-en.css')); ?>" rel="stylesheet" type="text/css" />
        <?php else: ?>
        <link href="<?php echo e(asset('/website/css/bootstrap-rtl.min.css')); ?>" rel="stylesheet" type="text/css" />
        <?php endif; ?>
        <link href="<?php echo e(asset('/website/css/mobile.css')); ?>" rel="stylesheet" type="text/css" />
        
        <link rel="stylesheet" href="<?php echo e(asset('/assets/vendor/font-awesome/css/font-awesome.min.css')); ?>">

        <?php echo $__env->yieldContent('css'); ?>
    </head>
    <body>
    	<!-- Start Top-h -->
    	<header class="toph">
        	<div class="container">
            	<div class="col-md-6 col-xs-12">
                	<ul>

                        <?php if(Auth::guard('Customer')->user()): ?>
                            <li>
                                <a class="btn login-btn" href="<?php echo e(app()->getLocale() == 'en' ? url('/en/customer/account') : url('/customer/account')); ?>">
                                    <i class="fa fa-user"></i> <?php echo app('translator')->getFromJson('website.Profile'); ?>
                                </a>
                            </li>

                        <?php else: ?>
                    	<li>
                            <a class="btn login-btn" href="<?php echo e(app()->getLocale() == 'en' ? url('/en/login') : url('/login')); ?>">
                                <i class="fa fa-user"></i> <?php echo app('translator')->getFromJson('website.Login'); ?>
                            </a>
                        </li>
                    	<li>
                            <a href="<?php echo e(app()->getLocale() == 'en' ? url('/en/register') : url('/register')); ?>" class="btn login-btn">
                                <i class="fa fa-user-plus"></i> <?php echo app('translator')->getFromJson('website.Register'); ?> 
                            </a>
                        </li>
                        <?php endif; ?>
                    </ul>
                </div>
                <div class="col-md-6 col-xs-12">
                	<p> <?php echo app('translator')->getFromJson('website.OpeningHours'); ?> : <?php if(! empty($app_about)  && ! empty($app_about['working_hour'] ) ): ?>  <?php echo e(app()->getLocale() == 'en' ? $app_about['working_hour'] : $app_about['working_hour_ar']); ?> <?php endif; ?></p>
                </div>
            </div>
        </header>
    	<!-- End Top-h -->
    	<!-- Start logo-h -->
        <section class="logo-h">
        	<div class="container">
            	<div class="col-md-3 col-xs-6">
                    <a href="<?php echo e(app()->getLocale() == 'en' ? url('/en/') : url('/')); ?>" class="logo navbar-brand">
                    	<img src="<?php echo e(asset('/website/images/logo.png')); ?>" lat="" />
                    </a>
                </div>
                <div class="col-md-9 col-xs-6 padding">
                	<div class="contact-h">
                    	<div class="col-md-4">
                        	<div class="details-con">
                            	<div class="icon-i">
                                	<i class="far fa-envelope"></i>
                                    <h1><?php echo app('translator')->getFromJson('website.Email'); ?> :</h1>
                                    <a href="mailto:email@yourdomain.com"> <?php if(! empty($app_about)  && ! empty($app_about['support_email'] )): ?>  <?php echo e($app_about['support_email']); ?> <?php endif; ?></a>
                                </div>
                            </div>
                        </div>
                    	<div class="col-md-4">
                        	<div class="details-con">
                            	<div class="icon-i">
                                	<i class="far fa-mobile-alt"></i>
                                    <h1><?php echo app('translator')->getFromJson('website.Mobile'); ?> :</h1>
                                    <span> <?php if(! empty($app_about) && ! empty($app_about['telephone'] ) ): ?>  <?php echo e($app_about['telephone']); ?> <?php endif; ?></span>
                                </div>
                            </div>
                        </div>
                    	<div class="col-md-4">
                        	<div class="details-con">
                            	<div class="icon-i">
                                	<i class="far fa-map-marked-alt"></i>
                                    <h1><?php echo app('translator')->getFromJson('website.FindUs'); ?> :</h1>
                                    <span> <?php if(! empty($app_about) && ! empty($app_about['address'] ) ): ?>  <?php echo e(app()->getLocale() == 'en' ? $app_about['address'] : $app_about['address_ar']); ?> <?php endif; ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
             		<button type="button" class="open-sidebar"><i class="fa fa-bars"></i></button>
                </div>
            </div>
        </section>
    	<!-- End logo-h -->
        <!-- Start Nav -->
        <nav class="navbar navbar-inverse">
          	<div class="container">
            	<div class="collapse navbar-collapse" id="myNavbar">
              		<ul class="nav navbar-nav">
                		<li class="current-menu-item"><a href="<?php echo e(app()->getLocale() == 'en' ? url('/en/') : url('/')); ?>"><?php echo app('translator')->getFromJson('website.Home'); ?></a></li>
                		<li><a class="nav-link scroll" href="<?php echo e(app()->getLocale() == 'en' ? url('/en/services') : url('/services')); ?>"><?php echo app('translator')->getFromJson('website.why_us'); ?></a></li>  
                		<li><a class="nav-link scroll" href="<?php echo e(app()->getLocale() == 'en' ? url('/en/captain') : url('/captain')); ?>"><?php echo app('translator')->getFromJson('website.captain'); ?></a></li>  
                		<li><a class="nav-link scroll" href="<?php echo e(app()->getLocale() == 'en' ? url('/en/customer') : url('/customer')); ?>"><?php echo app('translator')->getFromJson('website.client'); ?></a></li>  
                        <li><a class="btn-id" data-toggle="modal" data-target="#box-id"><?php echo app('translator')->getFromJson('website.track_order'); ?></a></li>

                		<li><a class="nav-link scroll" href="<?php echo e(app()->getLocale() == 'en' ? url('/en/contact') : url('/contact')); ?>"><?php echo app('translator')->getFromJson('website.Contact'); ?></a></li>
                    
              		</ul>
                    <a href="<?php echo e(route(\Illuminate\Support\Facades\Route::currentRouteName(),  app()->getLocale() == 'en' ?'':'en' )); ?>" class="lang"><?php echo e(app()->getLocale() == 'en' ? __('website.Arabic') : __('website.English')); ?></a>
            	</div>
          	</div>
            
        </nav>
        <div>
        	<!-- Start Sidebar -->
            <div class="overlay_gen"></div>
            <div class="sidebar">
                <div class="side-logo">
                    <a href="#">
                        <img src="<?php echo e(asset('/website/images/logo.png')); ?>" alt="">
                    </a>
                </div>
                <div class="side-social">
                    <ul>
                        <li>
                            <a href="#" class="lin">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="tw">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="ins">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="side-nav">
                    <ul>
                		<li class="current-menu-item"><a href="<?php echo e(app()->getLocale() == 'en' ? url('/en/') : url('/')); ?>"><?php echo app('translator')->getFromJson('website.Home'); ?></a></li>
                		<li><a class="nav-link scroll" href="<?php echo e(app()->getLocale() == 'en' ? url('/en/services') : url('/services')); ?>"><?php echo app('translator')->getFromJson('website.services'); ?></a></li>  
                		<li><a class="nav-link scroll" href="<?php echo e(app()->getLocale() == 'en' ? url('/en/captain') : url('/captain')); ?>"><?php echo app('translator')->getFromJson('website.captain'); ?></a></li>  
                		<li><a class="nav-link scroll" href="<?php echo e(app()->getLocale() == 'en' ? url('/en/customer') : url('/customer')); ?>"><?php echo app('translator')->getFromJson('website.client'); ?></a></li>  
                		<li><a class="nav-link scroll" href="<?php echo e(app()->getLocale() == 'en' ? url('/en/contact') : url('/contact')); ?>"><?php echo app('translator')->getFromJson('website.Contact'); ?></a></li>
                    </ul>
                </div>
            </div>
            <!-- End Sidebar -->
        </div>

        <main class="main-content">
        <?php echo $__env->yieldContent('content'); ?>
    </main>




    <footer>
            <div class="footer-top">
                <div class="container">
                    <!-- Start Col -->
                    <div class="col-md-2 col-xs-12">
                        <div class="logo logo-footer">
                            <a href="#">
                                <img src="<?php echo e(asset('/website/images/flogo.png')); ?>" alt="" />
                                <img src="<?php echo e(asset('/website/images/text-logo.png')); ?>" alt="" />
                            </a>
                        </div>
                    </div>
                    <!-- End -->
                    
                    <!-- Start Col -->
                    <div class="col-md-3 col-xs-12">
                        <div class="details-con-f">
                            <ul>
                                <li><i class="fa fa-mobile"></i> <?php if(! empty($app_about)  && ! empty($app_about['telephone'] )): ?>  <?php echo e($app_about['telephone']); ?> <?php endif; ?></li>
                                <li><i class="fa fa-envelope"></i> <?php if(! empty($app_about)  && ! empty($app_about['support_email'] )): ?>  <?php echo e($app_about['support_email']); ?> <?php endif; ?></li>
                                <li><i class="fa fa-map-marked-alt"></i> <?php if(! empty($app_about) && ! empty($app_about['address'] ) ): ?>  <?php echo e(app()->getLocale() == 'en' ? $app_about['address'] : $app_about['address_ar']); ?> <?php endif; ?></li>
                            </ul>
                        </div>
                    </div>
                    <!-- End -->
                    
                    <!-- Start Cal -->
                    <div class="col-md-4 col-xs-12">
                        
                        <ul class="links">
                            <li ><a href="<?php echo e(app()->getLocale() == 'en' ? url('/en/') : url('/')); ?>"><?php echo app('translator')->getFromJson('website.Home'); ?></a></li>
                            <li><a href="<?php echo e(app()->getLocale() == 'en' ? url('/en/services') : url('/services')); ?>"><?php echo app('translator')->getFromJson('website.why_us'); ?></a></li>  
                            <li><a href="<?php echo e(app()->getLocale() == 'en' ? url('/en/captain') : url('/captain')); ?>"><?php echo app('translator')->getFromJson('website.captain'); ?></a></li>  
                            <li><a href="<?php echo e(app()->getLocale() == 'en' ? url('/en/customer') : url('/customer')); ?>"><?php echo app('translator')->getFromJson('website.client'); ?></a></li>  
                            <li><a href="<?php echo e(app()->getLocale() == 'en' ? url('/en/contact') : url('/contact')); ?>"><?php echo app('translator')->getFromJson('website.Contact'); ?></a></li>
                        <li><a class="btn-id" data-toggle="modal" data-target="#box-id"> <?php echo app('translator')->getFromJson('website.track_order'); ?></a></li>

                            <li><a href="<?php echo e(app()->getLocale() == 'en' ? url('/en/terms') : url('/terms')); ?>"><?php echo app('translator')->getFromJson('website.Terms'); ?></a></li>
                        </ul>
                    </div>
                    <!-- End -->
                    
                    <div class="col-md-3 col-xs-12">
                        <div class="other-logos">
                            <a href="#"><img src="<?php echo e(asset('/images/google.png')); ?>" /></a>
                            <a href="#"><img src="<?php echo e(asset('/images/apple.png')); ?>" /></a>
                        </div>
                        <div class="social-media-footer">
                            <ul>
                                <li><a target="_blank" href="<?php if(! empty($app_about) && ! empty($app_about['facebook'] ) ): ?>  <?php echo e($app_about['facebook']); ?>  <?php else: ?> # <?php endif; ?>"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a target="_blank" href="<?php if(! empty($app_about) && ! empty($app_about['twitter'] ) ): ?>  <?php echo e($app_about['twitter']); ?>  <?php else: ?> # <?php endif; ?>"><i class="fab fa-twitter"></i></a></li>
                                <li><a target="_blank" href="<?php if(! empty($app_about) && ! empty($app_about['google'] ) ): ?>  <?php echo e($app_about['google']); ?>  <?php else: ?> # <?php endif; ?>"><i class="fab fa-google-plus-g"></i></a></li>
                                <li><a target="_blank" href="<?php if(! empty($app_about) && ! empty($app_about['linkedin'] ) ): ?>  <?php echo e($app_about['linkedin']); ?>  <?php else: ?> # <?php endif; ?>"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <p> <?php echo app('translator')->getFromJson('website.copywright'); ?><span ><?php echo app('translator')->getFromJson('website.ufelix'); ?></span></p>
                </div>
            </div>
        </footer>

         

        <?php echo $__env->make('frontend.layouts.track', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <script src="<?php echo e(asset('/website/js/jquery-1.11.0.min.js')); ?>"></script>
        <script src="<?php echo e(asset('/website/js/bootstrap.js')); ?>"></script>
        <script src="<?php echo e(asset('/website/js/owl.carousel.js')); ?>"></script>
        <script src="<?php echo e(asset('/website/js/wow.min.js')); ?>"></script>
        <script src="<?php echo e(asset('/website/js/responsiveCarousel.min.js')); ?>"></script>
        <script src="<?php echo e(asset('/website/js/jquery.mCustomScrollbar.concat.min.js')); ?>"></script>
        <script src="<?php echo e(asset('/website/js/aos.js')); ?>"></script>
        <script src="<?php echo e(asset('/website/js/jquery-scrolloffset.min.js')); ?>"></script>
        <script src="<?php echo e(asset('/website/js/jquery.counterup.min.js')); ?>"></script>
        <script src="<?php echo e(asset('/website/js/waypoints.min.js')); ?>"></script>
        <script src="<?php echo e(asset('/website/js/java.js')); ?>"></script>
        <script>
            AOS.init();
        </script>
        <?php echo $__env->yieldContent('scripts'); ?>
    </body>
</html><?php /**PATH /home/www/ufelix/ufelix/resources/views/frontend/layouts/app.blade.php ENDPATH**/ ?>
@extends('frontend.profile.layouts.app')

@section('title') @lang('website.UploadExcel') @endsection

@section('content')

    @if (count($errors) > 0)
    <div class="alert alert-danger text-center">
        <ul>
            @foreach ($errors->all() as $error)
                <li><i class="glyphicon glyphicon-remove"></i> {{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    @if(isset($success))
        <div class="col-md-12 col-sm-12 col-xs-12" @if(app()->getLocale() != 'en') style="text-align: right" @endif>
            <div class="alert alert-success">{{$success}}</div>
        </div>
    @endif
    <div class="container-fluid {{ app()->getLocale() == 'en' ? 'text-left' : 'text-right' }}">
        @include('flash::message')
    </div>
    <div class="row">
        <div class="col-md-12 table-responsive text-center mb-50 uploadArea">
            <h1 class="title"> <?php echo app('translator')->getFromJson('website.UploadExcel'); ?></h1>
            <form
                action="<?php echo e(app()->getLocale() == 'en' ? url('/en/customer/account/upload-excel') : url('/customer/account/upload-excel')); ?>"
                method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <img src="<?php echo e(asset('assets/images/upload.png')); ?>" class="uploadImg">
                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                            <h4 class="card-title"><?php echo app('translator')->getFromJson('website.FileUpload'); ?></h4>
                            <div class="col-md-12">
                                <label for="input-file-max-fs mb-50"
                                       style="margin-bottom: 50px !important"> <?php echo app('translator')->getFromJson('website.MaxSize'); ?></label>
                            </div>
                            <div class="col-md-5"></div>
                            <div class="col-md-4">
                                <input type="file" name="file" id="input-file-max-fs" class="dropify"
                                       data-max-file-size="2M"
                                       accept=".xls,.xlsx,.xlsm,.xlsb,.xml,application/ms-excel,application/vnd.ms-excel"
                                       required/>
                            </div>
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-info mt-50 " value="Save">
                            </div>
                        </div>
                    </div>

                </div>

            </form>

        </div>

        <div class="col-md-12 text-center">
            <div class="">
                <div class="table-responsive m-t-40">
                    <div class="button-box pull-right">
                    <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"
                                data-whatever="@fat"><?php echo app('translator')->getFromJson('website.UploadSheetExcel'); ?></button> -->
                    </div>
                    <div class="button-box pull-left">
                    <!--                           <button type="button" class="btn btn-info "><a href="#" style="color: #fff"
                                                                                               href="<?php echo e(asset('/backend/sheet.xlsx')); ?>"
                                                                                               download="sheet.xlsx"><?php echo app('translator')->getFromJson('website.Download'); ?></a>
                                                </button> -->
                        <button type="button" class="btn btn-info ">
                            <a style="color: #fff" href="<?php echo e(asset('/forma.xls')); ?>"><?php echo app('translator')->getFromJson('website.Download'); ?></a>
                        </button>
                    </div>

                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered"
                           cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th><?php echo app('translator')->getFromJson('website.ID'); ?></th>
                            <th><?php echo app('translator')->getFromJson('website.File'); ?></th>
                            @if($customer->is_manager)
                                <th>@lang('website.client')</th>
                            @endif
                            <th><?php echo app('translator')->getFromJson('website.ExcelCreatedAt'); ?></th>
                            <th><?php echo app('translator')->getFromJson('website.Actions'); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($orders)): ?>
                        <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <th scope="row"><?php echo e($file->id); ?></th>
                            <td><?php echo e($file->file_name); ?></td>
                            @if($customer->is_manager)
                                <td>{{$file->customer->name}}</td>
                            @endif
                            <td><?php echo e($file->created_at); ?></td>
                            <td>
                                <a href="<?php echo e(asset('/api_uploades/client/corporate/sheets/' . $file->file_name)); ?>"
                                   download="<?php echo e(asset('/api_uploades/client/corporate/sheets/' . $file->file_name)); ?>"
                                   class="text-inverse p-r-10" data-toggle="tooltip" title=""
                                   data-original-title="show" aria-describedby="tooltip468946">
                                    <i class="icon-download"></i>
                                </a>

                                @if($file->is_saved != 1)
                                <a style="margin:0 10px" href="<?php echo e(url('/customer/account/process-sheet/' . $file->id)); ?>"
                                   class="btn btn-primary">
                                    <?php echo app('translator')->getFromJson('website.process'); ?>
                                </a>
                                    @endif
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                        </tbody>
                    </table>
                    @if (! empty($orders) )
                        {!! $orders->appends($_GET)->links() !!}
                    @endif
                </div>
            </div>
        </div>
    </div>



    <!-- sample modal content -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"
                        id="exampleModalLabel1"><?php echo app('translator')->getFromJson('website.UploadExcelSheets'); ?></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>

                </div>

            </div>
        </div>
    </div>
    <!-- /.modal -->

@endsection

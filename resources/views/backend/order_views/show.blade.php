@extends('backend.layouts.app')
@section('header')
<div class="page-header">
        <h3>Show #{{$order_view->id}}</h3>{!! $order_view->status_span !!}
        <form action="{{ route('mngrAdmin.order_views.destroy', $order_view->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
               <button id="status" type="button" value="{{ $order_view->id }}" class="btn btn-success">{!! $order_view->status_txt !!}</button>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <div class="form-group col-sm-6">
                     <label for="order_id">ORDER_ID</label>
                     <p class="form-control-static"><a target="_blank" href="{{URL::asset('/mngrAdmin/orders/'.$order_view->order_id)}}">{{$order_view->order_id}}</a></p>
                </div>
                   
                    <div class="form-group col-sm-6">
                     <label for="agent_id">AGENT</label>
                     <p class="form-control-static"><a target="_blank" href="{{URL::asset('/mngrAdmin/agents/'.$order_view->agent_id)}}">{{! empty($order_view->agent )? $order_view->agent->name : ''}}</a></p>
                </div>
                    <div class="form-group col-sm-6">
                     <label for="notes">NOTES</label>
                     <p class="form-control-static">{{$order_view->notes}}</p>
                </div>
          
            <div class="col-md-12 col-xs-12 col-sm-12">
                <a class="btn btn-link pull-right" href="{{ URL::previous() }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
            </div>

        </div>
    </div>

@endsection
@section('scripts')
<script>
$(function () {
    $('#status').on('click', function () {
       	var Status = $(this).val();
        $.ajax({
            url: "{{ url('/mngrAdmin/order_views/active/'.$order_view->id) }}",
            Type:"GET",
            dataType : 'json',
        	success: function(response){
        		if(response==1){
        			$('#status').html('Accept');
        		}else{
        			$('#status').html('Not Accept');
        		}
            	location.reload();
   			}
        }); 
    });
});
</script>
@endsection
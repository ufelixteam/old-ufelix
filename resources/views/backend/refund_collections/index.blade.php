@extends('backend.layouts.app')
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">

    <title>{{__('backend.refund_collections')}}</title>
@endsection
@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.refund_collections')}}
        </h3>
    </div>
@endsection

@section('content')

    <div class="alert alert-danger" hidden>
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong id="errorId"></strong>
    </div>

    <div class="list">
        @include('backend.refund_collections.table')
    </div>
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

    <script type="text/javascript">
        $('body').on('click', '#printAll', function (e) {
            $('input:checkbox.selectprint').not("[disabled]").not(this).prop('checked', this.checked);
        });

        $('body').on('click', '#print_refund', function (e) {

            let ids = [];

            $(".selectrefund input:checked").each(function () {
                ids.push($(this).val());

            });

            if (ids.length > 0) {
                $('<form action="{{ URL::asset('mngrAdmin/refund_excel') }}" method="post" target="_blank"><input value="' + ids + ' " name="ids"  />" {{csrf_field()}}"</form>').appendTo('body').submit();

            }

        });
    </script>
@endsection

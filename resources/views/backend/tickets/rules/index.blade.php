@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.corporates')}}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <style>
        .toggle.ios, .toggle-on.ios, .toggle-off.ios {
            border-radius: 20px;
        }

        .toggle.ios .toggle-handle {
            border-radius: 20px;
        }

        @media only screen and (max-width: 580px) {
            .hide-td {
                display: none;
            }
        }

        .box-body {
            min-height: 100px;
        }

        .stylish-input-group .input-group-addon {
            background: white !important;
        }

        .stylish-input-group .form-control {
            border-right: 0;
            box-shadow: 0 0 0;
            border-color: #ccc;
        }

        .stylish-input-group button {
            border: 0;
            background: transparent;
        }

        .something-wrong {
            background: #9e8f9e;
        }
    </style>
@endsection
@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="fa fa-ticket"></i> {{__('backend.ticket_rules')}}
        </h3>
    </div>
@endsection

@section('content')

    <div class="list">
        @include('backend.tickets.rules.table')
    </div>
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <script>

        $('.toggle').bootstrapToggle();

        $('body').on('change', '.change_active', function (e) {
            let id = $(this).attr('data-id');
            $.ajax({
                url: '{{URL::asset("/mngrAdmin/change-active-ticket-rules")}}',
                type: 'post',
                data: {id: id, _token: "{{csrf_token()}}"},
                success: function (data) {

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

    </script>
@endsection

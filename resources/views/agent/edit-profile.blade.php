@extends('agent.layouts.app')
@section('css')

@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i>  Edit Agents #{{$agent->id}}</h3>
    </div>
@endsection

@section('content')


    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <form action="{{ URL::asset('mngrAgent/edit-profile') }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group col-md-6 col-sm-6 @if($errors->has('name')) has-error @endif">
                       <label for="name-field">Name</label>
                    <input type="text" id="name-field" name="name" class="form-control" value="{{ is_null(old("name")) ? $agent->name : old("name") }}"/>
                       @if($errors->has("name"))
                        <span class="help-block">{{ $errors->first("name") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('mobile')) has-error @endif">
                       <label for="mobile-field">Mobile</label>
                    <input type="text" id="mobile-field" name="mobile" class="form-control" value="{{ is_null(old("mobile")) ? $agent->mobile : old("mobile") }}"/>
                       @if($errors->has("mobile"))
                        <span class="help-block">{{ $errors->first("mobile") }}</span>
                       @endif
                    </div>
                      <div class="form-group col-md-6 col-sm-6 @if($errors->has('email')) has-error @endif">
                       <label for="email-field">E-mail</label>
                    <input type="text" id="email-field" name="email" class="form-control" value="{{ is_null(old("email")) ? $agent->email : old("email") }}"/>
                       @if($errors->has("email"))
                        <span class="help-block">{{ $errors->first("email") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('phone')) has-error @endif">
                       <label for="phone-field">Phone</label>
                    <input type="text" id="phone-field" name="phone" class="form-control" value="{{ is_null(old("phone")) ? $agent->phone : old("phone") }}"/>
                       @if($errors->has("phone"))
                        <span class="help-block">{{ $errors->first("phone") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('fax')) has-error @endif">
                       <label for="fax-field">Fax</label>
                    <input type="text" id="fax-field" name="fax" class="form-control" value="{{ is_null(old("fax")) ? $agent->fax : old("fax") }}"/>
                       @if($errors->has("fax"))
                        <span class="help-block">{{ $errors->first("fax") }}</span>
                       @endif
                    </div>
                  
                     <div class="form-group col-md-6 col-sm-6 @if($errors->has('profit_rate')) has-error @endif">
                       <label for="profit_rate-field">Profit Rate</label>
                    <input type="number" min="0" id="profit_rate-field" name="profit_rate" class="form-control" value="{{ is_null(old("profit_rate")) ? $agent->profit_rate : old("profit_rate") }}"/>
                       @if($errors->has("profit_rate"))
                        <span class="help-block">{{ $errors->first("profit_rate") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('commercial_record_number')) has-error @endif">
                       <label for="commercial_record_number-field">Commercial Record Number</label>
                    <input type="text" id="commercial_record_number-field" name="commercial_record_number" class="form-control" value="{{ is_null(old("commercial_record_number")) ? $agent->commercial_record_number : old("commercial_record_number") }}"/>
                       @if($errors->has("commercial_record_number"))
                        <span class="help-block">{{ $errors->first("commercial_record_number") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('deal_date')) has-error @endif">
                       <label for="deal_date-field">Deal Date</label>
                       <div class='input-group date ' id="datepicker">
                       

                    <input type="text" id="deal_date-field" name="deal_date" class="form-control" value="{{ is_null(old("deal_date")) ? $agent->deal_date : old("deal_date") }}"/>

                     <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        </div>
                       @if($errors->has("deal_date"))
                        <span class="help-block">{{ $errors->first("deal_date") }}</span>
                       @endif
                    </div>
                   
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('latitude')) has-error @endif">
                       <label for="latitude-field">Latitude</label>
                    <input type="text" id="latitude-field" name="latitude" class="form-control" value="{{ is_null(old("latitude")) ? $agent->latitude : old("latitude") }}"/>
                       @if($errors->has("latitude"))
                        <span class="help-block">{{ $errors->first("latitude") }}</span>
                       @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-6 @if($errors->has('longitude')) has-error @endif">
                       <label for="longitude-field">Longitude</label>
                    <input type="text" id="longitude-field" name="longitude" class="form-control" value="{{ is_null(old("longitude")) ? $agent->longitude : old("longitude") }}"/>
                       @if($errors->has("longitude"))
                        <span class="help-block">{{ $errors->first("longitude") }}</span>
                       @endif
                    </div>

                     <div class="form-group col-md-6 col-sm-6 @if($errors->has('password')) has-error @endif">
                       <label for="password-field">Password</label>
                    <input type="password" id="password-field" name="password" class="form-control" value="{{ old("password") }}"/>
                       @if($errors->has("password"))
                        <span class="help-block">{{ $errors->first("password") }}</span>
                       @endif
                    </div>


                     <div class="form-group col-md-6 col-sm-6 @if($errors->has('confirm_password')) has-error @endif">
                       <label for="confirm_password-field">Confirm Password</label>
                    <input type="password" id="confirm_password-field" name="confirm_password" class="form-control" value="{{ old("confirm_password") }}"/>
                       @if($errors->has("confirm_password"))
                        <span class="help-block">{{ $errors->first("confirm_password") }}</span>
                       @endif
                    </div>

                      <div class="form-group col-md-6 col-sm-6 @if($errors->has('address')) has-error @endif">
                       <label for="address-field">Address</label>
                    <textarea class="form-control" id="address-field" rows="4" name="address">{{ is_null(old("address")) ? $agent->address : old("address") }}</textarea>
                       @if($errors->has("address"))
                        <span class="help-block">{{ $errors->first("address") }}</span>
                       @endif
                    </div>

                    <div hidden class="form-group col-md-6 col-sm-6 @if($errors->has('status')) has-error @endif">
                       <label for="status-field" style="display: block;">Status</label>
                   
                         <input type="hidden" name="status" value="{{$agent->status}}" />
                       @if($errors->has("status"))
                        <span class="help-block">{{ $errors->first("status") }}</span>
                       @endif
                    </div>
                    
                <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
                    <button type="submit" class="btn btn-primary">Save</button>
                    
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  
@endsection

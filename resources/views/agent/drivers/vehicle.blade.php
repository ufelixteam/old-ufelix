@extends('agent.layouts.app')
@section('css')
<title>Driver - Vehicles</title>
@endsection
@section('header')
  <div class="page-header clearfix">
    <h3>
      <i class="glyphicon glyphicon-align-justify"></i> Driver Vehicles
    </h3>
  </div>
@endsection
@section('content')
  <div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
      @if($vehicles->count())
        <table class="table table-condensed table-striped">
          <thead>
            <tr>
              <th>#</th>
              <th>Model Name</th>
              <th>Plate Number</th>
              <th>Chassi Number</th>
              <th>Model Year</th>
              <th>color</th>
              <th>Vehicle Photo</th>
            </tr>
          </thead>

          <tbody>
            @foreach($vehicles as $vehicle)
              <tr>
                <td>{{$vehicle->id}}</td>
                <td>{{$vehicle->model_name}}</td>
                <td>{{$vehicle->plate_number}}</td>
                <td>{{$vehicle->chessi_number}}</td>
                <td>{{$vehicle->year}}</td>
                <td>{{$vehicle->color->name}}</td>
                <td> <img src="{{$vehicle->image}}" alt=""> </td>
              </tr>
            @endforeach
          </tbody>
        </table>

      @else
        <h3 class="text-center alert alert-warning">No Result Found !</h3>
      @endif
    </div>
  </div>
@endsection

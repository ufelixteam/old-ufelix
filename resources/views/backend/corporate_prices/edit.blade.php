@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.corporate_prices')}} - {{__('backend.edit')}}</title>
@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit_price')}} - #{{$corporatePrice->id}}
        </h3>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <form action="{{ route('mngrAdmin.corporatePrices.update', $corporatePrice->id) }}" method="POST"
                  name='validateForm'>
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group col-md-6 col-sm-6 @if($errors->has('start_station')) has-error @endif">
                    <label for="start_station-field">{{__('backend.from')}} </label>
                    <select id="start_station-field" name="start_station" class="form-control">
                        @if(! empty($list_governorates) )
                            @foreach($list_governorates as $governorate)
                                <option
                                    value="{{$governorate->id}}" {{$corporatePrice->start_station == $governorate->id ||  old("start_station") == $governorate->id ? 'selected' : ''}}>
                                    @if(session()->has('lang') == 'en')
                                        {{$governorate->name_ar}}
                                    @else
                                        {{$governorate->name_en}}
                                    @endif
                                </option>
                            @endforeach
                        @endif
                    </select>
                    @if($errors->has("start_station"))
                        <span class="help-block">{{ $errors->first("start_station") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-6 col-sm-6 @if($errors->has('access_station')) has-error @endif">
                    <label for="access_station-field">{{__('backend.to')}} </label>
                    <select id="access_station-field" name="access_station" class="form-control">
                        @if(! empty($list_governorates) )
                            @foreach($list_governorates as $governorate)
                                <option
                                    value="{{$governorate->id}}" {{$corporatePrice->access_station == $governorate->id ||  old("access_station") == $governorate->id ? 'selected' : ''}}>
                                    @if(session()->has('lang') == 'en')
                                        {{$governorate->name_ar}}
                                    @else
                                        {{$governorate->name_en}}
                                    @endif
                                </option>
                            @endforeach
                        @endif
                    </select>
                    @if($errors->has("access_station"))
                        <span class="help-block">{{ $errors->first("access_station") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-6 col-sm-6 @if($errors->has('cost')) has-error @endif">
                    <label for="cost-field">{{__('backend.cost')}}: </label>
                    <input type="text" id="cost-field" name="cost" class="form-control"
                           value="{{ is_null(old("cost")) ? $corporatePrice->cost : old("cost") }}"/>
                    @if($errors->has("cost"))
                        <span class="help-block">{{ $errors->first("cost") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-6 col-sm-6 @if($errors->has('recall_cost')) has-error @endif">
                    <label for="recall_cost-field">{{__('backend.recall_cost')}}: </label>
                    <input type="text" id="recall_cost-field" name="recall_cost" class="form-control"
                           value="{{ is_null(old("recall_cost")) ? $corporatePrice->recall_cost : old("recall_cost") }}"/>
                    @if($errors->has("recall_cost"))
                        <span class="help-block">{{ $errors->first("recall_cost") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-6 col-sm-6 @if($errors->has('reject_cost')) has-error @endif">
                    <label for="reject_cost-field">{{__('backend.reject_cost')}}: </label>
                    <input type="text" id="reject_cost-field" name="reject_cost" class="form-control"
                           value="{{ is_null(old("reject_cost")) ? $corporatePrice->reject_cost : old("reject_cost") }}"/>
                    @if($errors->has("reject_cost"))
                        <span class="help-block">{{ $errors->first("reject_cost") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-6 col-sm-6 @if($errors->has('cancel_cost')) has-error @endif">
                    <label for="cancel_cost-field">{{__('backend.cancel_cost')}}: </label>
                    <input type="text" id="cancel_cost-field" name="cancel_cost" class="form-control"
                           value="{{ is_null(old("cancel_cost")) ? $corporatePrice->cancel_cost : old("cancel_cost") }}"/>
                    @if($errors->has("cancel_cost"))
                        <span class="help-block">{{ $errors->first("cancel_cost") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-6 col-sm-6 @if($errors->has('overweight_cost')) has-error @endif">
                    <label for="overweight_cost-field">{{__('backend.overweight_cost')}}: </label>
                    <input type="text" id="overweight_cost-field" name="overweight_cost" class="form-control"
                           value="{{ is_null(old("overweight_cost")) ? $corporatePrice->overweight_cost : old("overweight_cost") }}"/>
                    @if($errors->has("overweight_cost"))
                        <span class="help-block">{{ $errors->first("overweight_cost") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-6 col-sm-6 @if($errors->has('status')) has-error @endif">
                    <label for="status-field">{{__('backend.status')}}: </label>
                    <select id="status-field" name="status" class="form-control">
                        <option
                            value="1" {{ old("status") == "1" ||  $corporatePrice->status == "1" ? 'selected' : '' }}>{{__('backend.yes')}}</option>
                        <option
                            value="0" {{ old("status") == "0" ||  $corporatePrice->status == "0" ? 'selected' : '' }}>{{__('backend.no')}}</option>
                    </select>
                    @if($errors->has("status"))
                        <span class="help-block">{{ $errors->first("status") }}</span>
                    @endif
                </div>
                <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
                    <button type="submit" class="btn btn-primary">{{__('backend.save_edits')}}</button>
                    <a class="btn btn-link pull-right"
                       href="{{ route('mngrAdmin.corporates.show', ['corporate_id' => $corporatePrice->corporate_id]) }}"><i
                            class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(function () {
            $("form[name='validateForm']").validate({
                // Specify validation rules
                rules: {
                    start_station: {
                        required: true,
                    },
                    access_station: {
                        required: true,
                    },
                    cost: {
                        required: true,
                        number: true
                    },
                    overweight_cost: {
                        required: true,
                        number: true
                    },
                },
                // Specify validation error messages
                messages: {
                    start_station: {
                        required: "{{__('backend.Please_Choose_Start_Destination')}}",
                    },
                    access_station: {
                        required: "{{__('backend.Please_Choose_End_Destination')}}",
                    },
                    cost: {
                        required: "{{__('backend.Please_Enter_The_Destination_Cost')}}",
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                    },
                    overweight_cost: {
                        required: "{{__('backend.Please_Enter_The_Destination_Overweight_Cost')}}",
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                    },
                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                },
                highlight: function (element) {
                    $(element).parent().addClass('error')
                },
                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                }
            });
        })
    </script>
@endsection

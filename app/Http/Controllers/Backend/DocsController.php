<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Docs;
use App\Models\DocsType;
use Response;
use Flash;
use File;
use Auth;

class DocsController extends Controller {

	// Get All Documentations
	public function index()
	{
		$list = Docs::orderBy('id', 'desc')->paginate(15);
		return view('backend.docs.index', compact('list'));
	}

	// Create Documentation Page
	public function create()
	{
      return view('backend.docs.create');
	}

	// Added New Documentation
	public function store(Request $request)
	{
      $v = \Validator::make($request->all(), [
          	'description' 	=> 'required',
          	'parent_id' 		=> 'required',
          	'title' 				=> 'required',
          	'slug' 					=> 'required|unique:docs',
      ]);

      if ($v->fails()) {
          return redirect()->back()->withErrors($v->errors());
      }

      Docs::insert([
					'parent_id' 		=> $request->parent_id ,
					'description' 	=> $request->description,
					'title' 				=> $request->title,
					'slug' 					=> $request->slug,
					'created_id' 		=> Auth::user()->id,
					'created_at' 		=> date('Y-m-d h:i:s')
			]);

			// Function Add Action In Activity Log By Samir
			addActivityLog('Add New Documentation ' . $request->title , ' اضافة وثيقة جديدة ' . $request->title, 34); // Helper Function in App\Helpers.php

      Flash::success(__('backend.Docs_saved_successfully'));
      return redirect('mngrAdmin/docs');
  }


  // Added New Type Documentation
  public function store_type(Request $request)
  {
      $v = \Validator::make($request->all(), [
          'title' => 'required|unique:docs_types'
      ]);

      if ($v->fails()) {
          return redirect()->back()->withErrors($v->errors());
      }

      DocsType::insert([
				 'title' => $request->title,
				 'created_at' => date('Y-m-d h:i:s')
			]);

			// Function Add Action In Activity Log By Samir
			addActivityLog('Add New Documentation Type ' . $request->title , ' اضافة نوع وثيقة ' . $request->title, 34); // Helper Function in App\Helpers.php

			Flash::success(__('backend.Doc_Type_saved_successfully'));
      return redirect('mngrAdmin/docs');
  }

	// View One Documentation
	public function show($id)
	{
      $data = Docs::findOrFail($id);
			return view('backend.docs.show', compact('data'));
	}


  // View One Type Documentation
  public function show_type($id)
  {
      $data = Docs_Type::findOrFail($id);
      return view('backend.docs.show_type', compact('data'));
  }

	// Edit Documentation Page
	public function edit($id)
	{
		//
	}

	// Update Documentation
	public function update(Request $request, $id)
	{

    $v = \Validator::make($request->all(), [
          'description' => 'required',
          'parent_id' 	=> 'required',
          'title' 			=> 'required',
      ]);

      if ($v->fails()) {
          return redirect()->back()->withErrors($v->errors());
      }

      Docs::where('id',$id)->update([
						'parent_id' 		=> $request->parent_id ,
						'description' 	=> $request->description,
						'title' 				=> $request->title,
						'slug' 					=> $request->slug,
						'updated_id' 		=> Auth::user()->id,
						'updated_at' 		=> date('Y-m-d h:i:s')
			]);

			// Function Add Action In Activity Log By Samir
			addActivityLog('Edit Documentation ' . $request->title , ' تعديل الوثيقة ' . $request->title, 34); // Helper Function in App\Helpers.php

			Flash::success(__('backend.Docs_updated_successfully'));
      return redirect('mngrAdmin/docs');
	}

	// Delete Documentation
	public function destroy($id)
	{

				$docs = Docs::findOrFail($id);
        Docs::where('id',$id)->delete();

				// Function Add Action In Activity Log By Samir
				addActivityLog('Delete Documentation ' . $docs->title , ' حذف الوثيقة ' . $docs->title, 34); // Helper Function in App\Helpers.php

				Flash::warning(__('backend.Docs_deleted_successfully'));
        return redirect('mngrAdmin/docs');
	}

}

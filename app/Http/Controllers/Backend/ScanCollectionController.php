<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use App\Models\ScanCollection;
use App\Models\ScanCollectionOrder;
use Illuminate\Http\Request;
use Auth;

class ScanCollectionController extends Controller
{

    public function __construct(Request $request)
    {
        if ($request->is('mngrAdmin/*') || $request->is('admin/*')) {
            \Config::set('auth.defaults.guard', 'admin');

        } elseif ($request->is('mngrAgent/*') || $request->is('agent/*')) {
            \Config::set('auth.defaults.guard', 'agent');
        }
        $this->guardType = \Config::get('auth.defaults.guard');
    }

    public function index(Request $request)
    {
        $user = Auth::guard($this->guardType)->user();

        $collections = ScanCollection::with(['driver'])
            ->withCount('orders')
            ->orderBy('scan_collections.id', 'desc');

        if ($user->is_warehouse_user) {
            $collections = $collections->join('drivers', 'drivers.id', '=', 'scan_collections.driver_id')
                ->join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                ->where('user_id', $user->id);
        }

        $type = $request->scan_type;
        if(isset($type) && $type != ""){
            $collections = $collections->where('scan_type', $type);
        }

        $search = $request->search;
        if (isset($search) && $search != "") {
            $collections = $collections->join('drivers', 'drivers.id', '=', 'scan_collections.driver_id')
                ->join('scan_collection_orders', 'scan_collection_orders.collection_id', '=', 'scan_collections.id')
                ->join('orders', 'orders.id', '=', 'scan_collection_orders.order_id')
                ->where(function ($q) use ($search) {
                    $q->where('scan_collections.id', 'like', '%' . $search . '%');
                    $q->OrWhere('scan_collections.collection_number', 'like', '%' . $search . '%');
                    $q->OrWhere('drivers.name', 'like', '%' . $search . '%');
                    $q->OrWhere('drivers.mobile', 'like', '%' . $search . '%');
                    $q->OrWhere('orders.id', 'like', '%' . $search . '%');
                    $q->OrWhere('orders.order_number', 'like', '%' . $search . '%');
                })
                ->groupBy('scan_collections.id');
        }

        $collections = $collections->paginate(50);

        if ($request->ajax()) {
            return [
                'view' => view('backend.scan_collections.table', compact('collections'))->render(),
            ];
        }

        return view('backend.scan_collections.index', compact('collections'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $scan_collection = ScanCollection::findOrFail($id);
        ScanCollectionOrder::where('collection_id', $scan_collection->id)->delete();
        $scan_collection->delete();

        return redirect('mngrAdmin/scan_collections');
    }
}

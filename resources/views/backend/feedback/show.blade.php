@extends('backend.layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <div class="form-group col-sm-6">
                <label for="nome">#</label>
                <p class="form-control-static">{{$feedback->title}}</p>
            </div>
            <div class="form-group col-sm-6">
                     <label for="weight">WEIGHT</label>
                     <p class="form-control-static">{{$feedback->message}}</p>
                </div>
                   
            <div class="col-md-12 col-xs-12 col-sm-12">
                <a class="btn btn-link" href="{{ route('mngrAdmin.weights.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
            </div>

        </div>
    </div>

@endsection
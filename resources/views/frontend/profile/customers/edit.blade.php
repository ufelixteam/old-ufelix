@extends('frontend.profile.layouts.app')

@section('title')
    @lang('website.edit_customer')
@endsection
@section('css')
    <style>
        .pac-container {
            z-index: 9999 !important;
        }

        .form-group {
            min-height: 99px;
            margin-bottom: 0px;
        }
    </style>
@endsection

@section('content')


    <div class="row" @if(app()->getLocale() != 'en') style="text-align: right" @endif>
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="table-responsive m-t-40">
                <h1 class="title">@lang('website.edit_customer')</h1>
                <form action="{{ route('profile.customers.update', ['locale' => (app()->getLocale() != 'en' ? 'ar' : 'en'), 'id' => $customer->id]) }}" method="POST" name="customer_edit"
                  enctype="multipart/form-data">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="container-fluid">
                    @include('flash::message')
                </div>
                <div class="row" style="margin-bottom: 30px;">
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('name')) has-error @endif">
                        <label for="name-field">{{__('backend.name')}}: </label>
                        <input class="form-control" type="text" name="name"
                               value="{{ !empty($customer->name)? $customer->name :  $customer->name}}">
                        @if($errors->has("name"))
                            <span class="help-block">{{ $errors->first("name") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('email')) has-error @endif">
                        <label for="role-field">{{__('backend.email')}}: </label>
                        <input class="form-control" type="email" name="email"
                               value="{{ !empty($customer->email)? $customer->email :  $customer->email}}">
                        @if($errors->has("email"))
                            <span class="help-block">{{ $errors->first("email") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('mobile')) has-error @endif">
                        <label for="type-field">{{__('backend.mobile')}}: </label>
                        <input class="form-control" type="text" name="mobile"
                               value="{{ !empty($customer->mobile)? $customer->mobile :  $customer->mobile}}"
                               maxlength="11">
                        @if($errors->has("mobile"))
                            <span class="help-block">{{ $errors->first("mobile") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('phone')) has-error @endif">
                        <label for="phone-field">{{__('backend.mobile_number2')}}: </label>
                        <input class="form-control" type="text" name="phone"
                               value="{{ !empty($customer->phone)? $customer->phone :   $customer->phone}}"
                               maxlength="11">
                        @if($errors->has("phone"))
                            <span class="help-block">{{ $errors->first("phone") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('national_id')) has-error @endif">
                        <label for="national_id-field">{{__('backend.national_id_number')}}: </label>
                        <input class="form-control" type="text" name="national_id"
                               value="{{ !empty($customer->national_id )? $customer->national_id :  $customer->national_id}}"
                               maxlength="14">
                        @if($errors->has("national_id"))
                            <span class="help-block">{{ $errors->first("national_id") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('job')) has-error @endif">
                        <label for="job-field">{{__('backend.job_name')}}: </label>
                        <input class="form-control" type="text" name="job"
                               value="{{ !empty($customer->job)? $customer->job:  $customer->job}}">
                        @if($errors->has("job"))
                            <span class="help-block">{{ $errors->first("job") }}</span>
                        @endif
                    </div>

                    {{--                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('country_id')) has-error @endif">--}}
                    {{--                        <label for="country_id-field">{{__('backend.choose_country')}}: </label>--}}
                    {{--                        <select id="country_id-field" name="country_id" class="form-control">--}}
                    {{--                            @if(! empty($countries)<div class="container-fluid">
                                @include('flash::message')
                            </div>)--}}
                    {{--                                @foreach($countries as $country)--}}
                    {{--                                    <option--}}
                    {{--                                        value="{{$country->id}}" {{ old("country_id") == $country->id  || $customer->country_id == $country->id  ? 'selected' : '' }}>{{$country->name}}</option>--}}
                    {{--                                @endforeach--}}
                    {{--                            @endif--}}
                    {{--                        </select>--}}
                    {{--                    </div>--}}
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('government_id')) has-error @endif">
                        <label for="government_id-field"> {{__('backend.government')}}: </label>
                        <select class="form-control s_government_id" id="government_id" name="government_id">
                            <option value="" data-display="Select">{{__('backend.government')}}</option>
                            @if(! empty($governments))
                                @foreach($governments as $government)
                                    <option
                                        value="{{$government->id}}" {{  old("government_id") == $government->id  ||  $customer->government_id == $government->id ? 'selected' : old("government_id")}}>
                                        {{$government->name_en}}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                        @if($errors->has("government_id"))
                            <span class="help-block">{{ $errors->first("government_id") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('city_id')) has-error @endif">
                        <label class="control-label" for="s_state_id">{{__('backend.city')}}</label>
                        <select class=" form-control s_state_id" id="city_id" name="city_id">
                            @foreach($cities as $city)
                                <option
                                    value="{{$city->id}}" {{ $city->governorate_id == $customer->government_id && $city->id == $customer->city_id ? 'selected' : old("city_id")}}>
                                    {{$city->name_en}}
                                </option>
                            @endforeach
                        </select>
                        @if($errors->has("city_id"))
                            <span class="help-block">{{ $errors->first("city_id") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('address')) has-error @endif">
                        <label for="national_id-field">{{__('backend.address')}}: </label>
                        <input class="form-control" type="text" name="address"
                               value="{{ !empty($customer->address)? $customer->address:  $customer->address}}">
                        @if($errors->has("address"))
                            <span class="help-block">{{ $errors->first("address") }}</span>
                        @endif
                    </div>

                    <div
                        class="form-group col-md-4 col-sm-4 @if($errors->has('latitude')) has-error @endif">
                        <div class="">
                            <label for="receiver_latitude"> {{__('backend.latitude')}}: </label>
                            <input type="text" required id="receiver_latitude" name="latitude" class="form-control"
                                   value="{{ $customer->latitude ? $customer->latitude  : '30.0668886'  }}"/>
                            <i class="icon-map-pin" data-toggle="modal" data-target="#map1Modal" data-whatever="@fat" style="margin-top: -7px;"></i>

                            @if($errors->has("latitude"))
                                <span class="help-block">{{ $errors->first("latitude") }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('longitude')) has-error @endif">
                        <label for="longitude"> {{__('backend.longitude')}}: </label>
                        <input type="text" required id="receiver_longitude" name="longitude"
                               class="form-control"
                               value="{{ $customer->longitude ? $customer->longitude  : '31.1962743'  }}"/>
                        @if($errors->has("longitude"))
                            <span class="help-block">{{ $errors->first("longitude") }}</span>
                        @endif
                    </div>

                    <div class="form-group col-md-4 col-sm-4">
                        <label for="mobile-users">{{__('backend.store')}}</label>
                        <select class="form-control select2 disabled" id="store_id" name="store_id">
                            <option value="" data-dispaly="Select">{{__('backend.store')}}</option>
                            @if(! empty($stores))
                                @foreach($stores as $store)
                                    <option value="{{$store->id}}" @if($customer->store_id == $store->id) selected @endif>{{$store->name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="form-group col-md-4 col-sm-4  @if($errors->has('password')) has-error @endif">
                        <label for="is_active-field">{{__('backend.password')}}</label>
                        <input class="form-control" type="password" name="password" value=""
                               placeholder="{{__('backend.enter_new_password')}}">
                        <span
                            class="help-block">{{__('backend.if_you_does_not_change_password_please_leave_it_empty')}}</span>
                        @if($errors->has("password"))
                            <span class="help-block">{{ $errors->first("password") }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4 col-sm-4">
                        <label for="manager-field">{{__('backend.manager')}}: </label>
                        <select id="manager-field" name="is_manager" class="form-control">
                            <option
                                value="0" {{ old("is_manager") == "0" ||  $customer->is_manager == "0" ? 'selected' : '' }}>{{__('backend.No')}}</option>
                            <option
                                value="1" {{ old("is_manager") == "1" ||  $customer->is_manager == "1" ? 'selected' : '' }}>{{__('backend.Yes')}}</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4 col-sm-4 @if($errors->has('image')) has-error @endif">
                        <div id="image_upload">
                            @if($customer->image)
                                <div class="imgDiv">
                                    <input type="hidden" value="{{ $customer->image }}" name="image_old">
                                    <img src="{{ $customer->image }}" style="width:100px;height:100px" onerror="this.src='{{asset('assets/images/user.png')}}'">
                                    <button class="btn btn-danger cancel">&times;</button>
                                </div>
                            @endif
                        </div>
                        <label for="image-field">{{__('backend.image')}}: </label>
                        <input class="uploadPhoto btn btn-primary" type="file" name="image" id="image">
                        @if($errors->has("image"))
                            <span class="help-block">{{ $errors->first("image") }}</span>
                        @endif
                    </div>
                </div>


                <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
                    <button type="submit" class="btn btn-primary">{{__('backend.save_edits')}}</button>
{{--                    <a class="btn btn-link pull-right"--}}
{{--                       href="{{ URL::previous() /*route('mngrAdmin.customers.index')*/ }}"><i--}}
{{--                            class="glyphicon glyphicon-backward"></i> {{__('backend.back')}} </a>--}}
                </div>
            </form>
            </div>
        </div>
    </div>

    @include('frontend.profile.collections.map')
    @include('frontend.profile.collections.map1')

@endsection

@section('scripts')

    <script type="text/javascript" src="{{ asset('/assets/scripts/map-order.js')}}"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key=AIzaSyDQiN4qc7pC9b1BRQwkqXXD28peMfWcHvw&callback=myMap"></script>
    @include('frontend.profile.collections.js')
@endsection

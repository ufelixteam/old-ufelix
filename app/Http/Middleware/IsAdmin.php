<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$guard = 'admin')
    {
        \Config::set('auth.defines.guard','admin');
         config(['auth.defaults.guard' => $guard]);
        if (Auth::guard($guard)->check()) {
            
            return $next($request);

        }else{
            /*if(Auth::guard('agent')->check()){
                return redirect('/mngrAgent/403');
            }else{
            
                return redirect('/mngrAdmin/login');
            }
            */
                return redirect('/mngrAdmin/login');

        }
    }
}

<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        @if($wallets->count())
            <table class="table table-condensed table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ ! empty($header) ? $header : '' }}</th>
                    <th>{{__('backend.value')}}</th>

                    <th>{{__('backend.status')}}</th>
                    <th class="text-right"></th>
                </tr>
                </thead>
                <tbody>
                @php $counter=0; @endphp
                @foreach($wallets as $wallet)
                    <tr>
                        <td>{{ ++$counter }}</td>
                        <td>{{ $wallet->$header['name'] }}</td>
                        <td>{{$wallet->amount}}</td>

                        <td>{!! $wallet->active_span !!}</td>
                        <td class="text-right">
                        @if(permission('viewLogs')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                            <a class="btn btn-xs btn-primary"
                               href="{{ route('mngrAdmin.wallets.show', $wallet->id) }}"><i
                                    class="fa fa-eye-open"></i> {{__('backend.view_transactions')}}</a>
                        @endif
                        <!-- <a class="btn btn-xs btn-warning" href="{{ route('mngrAdmin.wallets.edit', $wallet->id)."?type=".$wallet->type }}"><i class="fa fa-edit"></i> Edit</a> -->
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {!! $wallets->appends($_GET)->links() !!}

        @else
            <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
        @endif
    </div>
</div>

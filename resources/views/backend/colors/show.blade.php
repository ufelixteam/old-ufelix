@extends('backend.layouts.app')
@section('css')
  <title>Vehicles Colors - View</title>
@endsection
@section('header')
<div class="page-header">
        <h3>Show #{{$color->id}}</h3> {!! $color->status_span !!}
        <form action="{{ route('mngrAdmin.colors.destroy', $color->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('mngrAdmin.colors.edit', $color->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <div class="form-group">
                <label for="nome">#</label>
                <p class="form-control-static">{{$color->id}}</p>
            </div>
            <div class="form-group col-sm-6">
                     <label for="name">NAME</label>
                     <p class="form-control-static">{{$color->name}}</p>
                </div>


            <div class="col-md-12 col-xs-12 col-sm-12">
                <a class="btn btn-link" href="{{ route('mngrAdmin.colors.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
            </div>

        </div>
    </div>

@endsection

<?php $__env->startSection('css'); ?>
    <title><?php echo e(__('backend.driver')); ?> - <?php echo e(__('backend.orders')); ?></title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <style>
        .jconfirm.jconfirm-modern .jconfirm-box div.jconfirm-title-c .jconfirm-icon-c {
            -webkit-transition: -webkit-transform .5s;
            transition: -webkit-transform .5s;
            transition: transform .5s;
            transition: transform .5s, -webkit-transform .5s;
            -webkit-transform: scale(0);
            transform: scale(0);
            display: block;
            margin-right: 0;
            margin-left: 0;
            margin-bottom: 10px;
            font-size: 69px;
            color: #9e1717;
        }

        .nav-tabs li:first-child.active a {
            color: #fff;
            background-color: #337ab7;
        }

        .nav-tabs li:last-child.active a {
            color: #fff;
            background-color: #dd4b39;
        }
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('header'); ?>
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-align-justify"></i>
            <?php echo e(__('backend.orders_of_driver')); ?> [ # <?php echo e($driver->id); ?> - <?php echo e($driver->name); ?> ] </h3>
        </h3>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div>
        <form>
            <input type="hidden" name="sort" id="sort" value="<?php echo e(app('request')->input('sort', 'currentorders')); ?>">
            <div class="row" style="margin-bottom: 50px">
                <div class="col-md-6">
                    <div class="input-daterange input-group input-group-lg" id="datepicker">
                        <input type="text" class="input-sm form-control" name="start"
                               value="<?php echo e(app('request')->input('start')); ?>"/>
                        <span class="input-group-addon"><?php echo e(__('backend.to')); ?></span>
                        <input type="text" class="input-sm form-control" name="end"
                               value="<?php echo e(app('request')->input('end')); ?>"/>
                    </div>
                </div>
                <div class="col-md-6">
                    <button class="btn btn-success btn-lg">
                        <i class="fa fa-filter"></i>
                        <?php echo e(__('backend.add_filter')); ?>

                    </button>
                </div>
            </div>
        </form>
        <!-- Nav tabs -->
        <ul id="myTabs" class="nav nav-tabs nav-justified nav-pills" role="tablist">
            <li role="presentation" class="<?php echo e(! empty($sort) && $sort == 'currentorders' ? 'active' : ''); ?>"><a
                    href="#home" aria-controls="home" role="tab" data-toggle="tab" data-sort="currentorders"
                    class="order_type"><?php echo e(__('backend.current')); ?></a></li>
            <li role="presentation" class="<?php echo e(! empty($sort) && $sort == 'orders' ? 'active' : ''); ?>"><a href="#profile"
                                                                                                        aria-controls="profile"
                                                                                                        role="tab"
                                                                                                        data-toggle="tab"
                                                                                                        data-sort="orders"
                                                                                                        class="order_type"><?php echo e(__('backend.finished')); ?></a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane <?php echo e(! empty($sort) && $sort == 'currentorders' ? 'active' : ''); ?> "
                 id="home">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <?php if(! empty($orders) &&  count($currentorders) > 0): ?>
                            <form action="<?php echo e(URL::asset('/mngrAdmin/pdf/1/'. $id)); ?>" method="get" target="_blank">
                                <table class="table table-condensed table-striped text-center">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th><?php echo e(__('backend.order_type')); ?></th>
                                        <th><?php echo e(__('backend.order_number')); ?></th>
                                        <th><?php echo e(__('backend.order_price')); ?></th>
                                        <th><?php echo e(__('backend.sender_name')); ?></th>
                                        <th><?php echo e(__('backend.receiver_name')); ?></th>
                                        <th><?php echo e(__('backend.receiver_code')); ?></th>
                                        <th><?php echo e(__('backend.status')); ?></th>
                                        <th class="pull-right"></th>
                                        <th>
                                            <input type="button" class="btn btn-info btn-xs" id="toggle"
                                                   value="<?php echo e(__('backend.select')); ?>" onClick="do_this()"/>
                                            <button type="submit"
                                                    class="btn btn-warning btn-xs btn-pdf"><?php echo e(__('backend.pdf')); ?></button>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $__currentLoopData = $currentorders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                        <tr>

                                            <td><?php echo e($order->id); ?></td>
                                            <td><?php echo e($order->type); ?></td>
                                            <td><?php echo e($order->order_number); ?></td>
                                            <td><?php echo e($order->order_price); ?></td>
                                            <td><?php echo e($order->sender_name); ?></td>
                                            <td><?php echo e($order->receiver_name); ?></td>
                                            <td><?php echo e($order->receiver_code); ?></td>
                                            <td>
                                                <?php echo $order->status_span; ?>

                                            </td>
                                            <td class="pull-right">
                                                <a class="btn btn-xs btn-primary"
                                                   href="<?php echo e(route('mngrAdmin.orders.show', $order->id)); ?>"><i
                                                        class="glyphicon glyphicon-eye-open"></i> <?php echo e(__('backend.view')); ?>

                                                </a>
                                            </td>
                                            <td>
                                                <input type="checkbox" name="select[]" value="<?php echo e($order->id); ?>"/>
                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </form>
                            <?php echo $currentorders->appends(['sort' => 'currentorders'])->appends($_GET)->links(); ?>


                        <?php else: ?>
                            <h3 class="text-center alert alert-warning"><?php echo e(__('backend.No_result_found')); ?></h3>
                        <?php endif; ?>

                    </div>
                </div>
            </div> <!-- tab end -->


            <div role="tabpanel" class="tab-pane <?php echo e(! empty($sort) && $sort == 'orders' ? 'active' : ''); ?>" id="profile">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <?php if(! empty($orders) && count($orders) > 0): ?>
                            <form action="<?php echo e(URL::asset('/mngrAdmin/pdf/1/'. $id)); ?>" method="get" target="_blank">
                                <table class="table table-condensed table-striped text-center">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th><?php echo e(__('backend.order_type')); ?></th>
                                        <th><?php echo e(__('backend.order_number')); ?></th>
                                        <th><?php echo e(__('backend.order_price')); ?></th>
                                        <th><?php echo e(__('backend.sender_name')); ?></th>
                                        <th><?php echo e(__('backend.receiver_name')); ?></th>
                                        <th><?php echo e(__('backend.receiver_code')); ?></th>
                                        <th><?php echo e(__('backend.status')); ?></th>
                                        <th class="pull-right"></th>
                                        <th>
                                            <input type="button" class="btn btn-info btn-xs" id="toggle"
                                                   value="<?php echo e(__('backend.select')); ?>" onClick="do_this()"/>
                                            <button type="submit"
                                                    class="btn btn-warning btn-xs btn-pdf"><?php echo e(__('backend.pdf')); ?></button>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($order->id); ?></td>
                                            <td><?php echo e($order->type); ?></td>
                                            <td><?php echo e($order->order_number); ?></td>
                                            <td><?php echo e($order->order_price); ?></td>
                                            <td><?php echo e($order->sender_name); ?></td>
                                            <td><?php echo e($order->receiver_name); ?></td>
                                            <td><?php echo e($order->receiver_code); ?></td>
                                            <td>
                                                <?php echo $order->status_span; ?>

                                            </td>
                                            <td class="pull-right">
                                                <a class="btn btn-xs btn-primary"
                                                   href="<?php echo e(route('mngrAdmin.orders.show', $order->id)); ?>"><i
                                                        class="glyphicon glyphicon-eye-open"></i> <?php echo e(__('backend.view')); ?>

                                                </a>
                                            </td>
                                            <td>
                                                <input type="checkbox" name="select[]" value="<?php echo e($order->id); ?>"/>
                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </form>
                            <?php echo $orders->appends($_GET)->appends(['sort' => 'orders'])->links(); ?>


                        <?php else: ?>
                            <h3 class="text-center alert alert-warning"><?php echo e(__('backend.No_result_found')); ?></h3>
                        <?php endif; ?>
                    </div>
                </div>
            </div> <!-- tab end -->
        </div>

    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script>
        $('.input-daterange').datepicker({});

        $('.order_type').click(function (e) {
            $("#sort").val($(this).attr('data-sort'))
        });

        $('#myTabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        })
        $(function () {
            $(".ask_view").on('click', function () {
                var id = $(this).attr('data-id');
                $.ajax({
                    url: '<?php echo url("/mngrAgent/ask_review_order/"); ?>' + '/' + id,
                    type: 'get',

                }).done(function (data) {
                    if (data == "ok") {
                        $.alert({
                            title: "<?php echo e(__('backend.Request_Review_Sent')); ?>",
                            icon: 'fa fa-smile-o',
                            theme: 'modern',
                            content: "<?php echo e(__('backend.Your_request_sent_to_Ufelix_Admin_successfully')); ?>",
                            buttons: {
                                info: {
                                    text: 'OK',
                                    btnClass: 'btn-success',
                                    action: function () {
                                        window.location.href = window.location.href;
                                    }
                                }
                            }
                        });
                    }
                });
            });

        });


        // Select All Orders function For Print
        function do_this() {

            var checkboxes = document.getElementsByName('select[]');
            var button = document.getElementById('toggle');

            if (button.value == 'select') {
                for (var i in checkboxes) {
                    checkboxes[i].checked = 'FALSE';
                }
                button.value = 'deselect'
            } else {
                for (var i in checkboxes) {
                    checkboxes[i].checked = '';
                }
                button.value = 'select';
            }
        }


    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/drivers/showOrders.blade.php ENDPATH**/ ?>
@extends('backend.layouts.app')
@section('css')
    <title>{{__('backend.the_invoices')}} - {{__('backend.view')}}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <style>
        .invoice-header {
            margin-bottom: 0px;
            margin-top: 5px;
            color: #676a6d;
            text-decoration: underline;
            font-style: italic;
        }

        /*#ordersTable {*/
        /*    display: table;*/
        /*    overflow-x: auto;*/
        /*    white-space: nowrap;*/
        /*}*/

        .form-check-inline {
            display: -webkit-inline-box;
            display: -ms-inline-flexbox;
            display: inline-flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            padding-left: 0;
            margin-right: 1rem;
        }

        .form-check-inline input {
            margin: 5px;
        }
    </style>
@endsection
@section('header')
    <div class="page-header noprint">
        <div class="row">
            <div class="col-sm-2">
                <h3 class="invoice-header">{{__('backend.invoice_number')}}:</h3>
            </div>
            <div class="col-sm-2">
                <h4 class="invoice-header">#{{$invoice->invoice_no}}</h4>
            </div>
            <div class="col-sm-2">
                <h3 class="invoice-header">{{__('backend.date')}}:</h3>
            </div>
            <div class="col-sm-2">
                <h4 class="invoice-header">{{$invoice->created_at->format('Y-m-d')}}</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-2">
                <h3 class="invoice-header">{{__('backend.corporate_name')}}:</h3>
            </div>
            <div class="col-sm-2">
                <h4 class="invoice-header">{{@$invoice->owner->name}}</h4>
            </div>
            <div class="col-sm-2">
                <h3 class="invoice-header">{{__('backend.corporate_number')}}:</h3>
            </div>
            <div class="col-sm-2">
                <h4 class="invoice-header">#{{@$invoice->owner->id}}</h4>
            </div>
            <div class="col-sm-2">
                <h3 class="invoice-header">{{__('backend.customer')}}:</h3>
            </div>
            <div class="col-sm-2">
                <h4 class="invoice-header">{{@$invoice->owner ? @$invoice->owner->customers()->first()->name : '-'}}</h4>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-2">
                <h3 class="invoice-header">{{__('backend.transfer_method')}}:</h3>
            </div>
            <div class="col-sm-2">
                <h4 class="invoice-header">{!! @$invoice->owner->transfer_method_span !!}</h4>
            </div>
            @if(@$invoice->owner->transfer_method == 1)
                <div class="col-sm-2">
                    <h3 class="invoice-header">{{__('backend.bank_name')}}:</h3>
                </div>
                <div class="col-sm-2">
                    <h4 class="invoice-header">{{@$invoice->owner->bank_name}}</h4>
                </div>
                <div class="col-sm-2">
                    <h3 class="invoice-header">{{__('backend.bank_account')}}:</h3>
                </div>
                <div class="col-sm-2">
                    <h4 class="invoice-header">{{@$invoice->owner->bank_account}}</h4>
                </div>
            @elseif(@$invoice->owner->transfer_method == 2)
                <div class="col-sm-2">
                    <h3 class="invoice-header">{{__('backend.mobile_number')}}:</h3>
                </div>
                <div class="col-sm-2">
                    <h4 class="invoice-header">{{@$invoice->owner->mobile_transfer}}</h4>
                </div>
            @endif
        </div>

        <div class=" pull-right" role="group" id="group" aria-label="...">

        @if(permission('printInvoice')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
            <a href="{{url('mngrAdmin/print/invoice', $invoice->id)}}" target="_blank" id="printInvoice"
               class="btn btn-info"><i class="glyphicon glyphicon-print"></i> {{__('backend.print_invoice')}}</a>
            @endif

        </div>
    </div>
@endsection

@section('content')
    <h2 class="text-center">
        {{__('backend.Orders_List')}}
    </h2>

    <form action="" method="get">
        <div class="row" style="margin-bottom:15px;">
            <div class="col-md-12" style="padding: 0 25px">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="shipcostCheckbox" value="ship_cost"
                           name="thcheckbox[]">
                    <label class="form-check-label" for="shipcostCheckbox">{{__('backend.ship_cost')}}</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="settlementCheckbox" value="settlement"
                           name="thcheckbox[]">
                    <label class="form-check-label" for="settlementCheckbox">{{__('backend.settlement')}}</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="netCheckbox" value="net"
                           name="thcheckbox[]">
                    <label class="form-check-label" for="netCheckbox">{{__('backend.net')}}</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="placeCheckbox" value="place"
                           name="thcheckbox[]">
                    <label class="form-check-label" for="placeCheckbox">{{__('backend.place')}}</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="feesfromCheckbox" value="fees_from"
                           name="thcheckbox[]">
                    <label class="form-check-label" for="inlineCheckbox3">{{__('backend.fees_from')}}</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="createddateCheckbox" value="created_date"
                           name="thcheckbox[]">
                    <label class="form-check-label" for="inlineCheckbox3">{{__('backend.created_date')}}</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="lastupdatedateCheckbox" value="last_update_date"
                           name="thcheckbox[]">
                    <label class="form-check-label" for="inlineCheckbox3">{{__('backend.last_update_date')}}</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="referencenumberCheckbox" value="reference_number"
                           name="thcheckbox[]">
                    <label class="form-check-label" for="inlineCheckbox3">{{__('backend.reference_number')}}</label>
                </div>
                <div class="form-check form-check-inline pull-right">
                    <select name="limit" id="limit" class="form-control">
                        <option value="">{{__('backend.per_page')}}</option>
                        <option
                            value="100" {{app('request')->input('limit') == 100 ? 'selected' : ''}}>100
                        </option>
                        <option
                            value="200" {{app('request')->input('limit') == 200 ? 'selected' : ''}}>200
                        </option>
                        <option
                            value="500" {{app('request')->input('limit') == 500 ? 'selected' : ''}}>500
                        </option>
                        <option
                            value="1000" {{app('request')->input('limit') == 1000 ? 'selected' : ''}}>1000
                        </option>
                        <option
                            value="all" {{app('request')->input('limit') == 'all' ? 'selected' : ''}}>{{__('backend.all')}}</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row" style="margin-bottom:15px;">
            <div class="col-md-12">

                <div class="group-control col-md-2 col-sm-2">
                    <div class='input-group date' id="datepickerFilter">
                        <input type="text" name="created_at" class="form-control"
                               value="{{app('request')->input('created_at')}}"
                               placeholder="{{__('backend.created_at')}}"/>
                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>

                <div class="group-control col-md-2 col-sm-2">
                    <div class='input-group date' id="datepickerFilter2">
                        <input type="text" name="last_update_date" class="form-control"
                               value="{{app('request')->input('last_update_date')}}"
                               placeholder="{{__('backend.last_update_date')}}"/>
                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>

                <div class="group-control col-md-2 col-sm-2">
                    <select name="status" class="form-control">
                        <option value="">{{__('backend.status')}}</option>
                        <option
                            value="3" {{app('request')->input('status') == 3 ? 'selected' : ''}}>{{__('backend.delivered')}}</option>
                        <option
                            value="5" {{app('request')->input('status') == 5 ? 'selected' : ''}}>{{__('backend.recalled')}}</option>
                        <option
                            value="8" {{app('request')->input('status') == 8 ? 'selected' : ''}}>{{__('backend.rejected')}}</option>
                        <option
                            value="4" {{app('request')->input('status') == 4 ? 'selected' : ''}}>{{__('backend.cancelled')}}</option>
                    </select>
                </div>

                <div class="group-control col-md-2 col-sm-2">
                    <select name="customer_id" class="form-control">
                        <option value="">{{__('backend.user')}}</option>
                        @foreach($customers as $customer)
                            <option
                                value="{{$customer->id}}" {{app('request')->input('customer_id') == $customer->id ? 'selected' : ''}}>{{$customer->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="group-control col-md-2 col-sm-2">
                    <select name="paid" class="form-control">
                        <option value="">{{__('backend.pay_by_captain')}}</option>
                        <option
                            value="1" {{app('request')->input('paid') == 1 ? 'selected' : ''}}>{{__('backend.yes')}}</option>
                        <option
                            value="2" {{app('request')->input('paid') == 2 ? 'selected' : ''}}>{{__('backend.no')}}</option>
                    </select>
                </div>
            </div>

            <div class="col-md-12" style="margin-top: 20px;">
                <div class="group-control col-md-2 col-sm-2">
                    <button type="submit" class="btn btn-primary btn-block"> {{__('backend.filter_orders')}} </button>
                </div>

                <div class="group-control col-md-2 col-sm-2">
                    <a class="btn btn-primary btn-block"
                       href="{{url(url()->current().'?created_at='.app('request')->input('last_update_date').'&created_at='.app('request')->input('created_at').'&status='.app('request')->input('status').'&limit='.app('request')->input('limit').'&arrange=1')}}"> {{__('backend.arrange_by_type')}} </a>
                </div>

                <div class="group-control col-md-2 col-sm-2">
                    <a class="btn btn-primary btn-block"
                       href="#" id="editShipCost"> {{__('backend.change_ship_cost')}} </a>
                </div>
            </div>
        </div>
    </form>

    <form class="noprint" id="transactionForm" action="{{ route('mngrAdmin.invoices.Transaction') }}"
          method="POST">
        {{csrf_field()}}
        <input type="hidden" name="status" value="{{app('request')->input('status')}}"/>
        <input type="hidden" name="created_at" value="{{app('request')->input('created_at')}}"/>
        <input type="hidden" name="delivered_at" value="{{app('request')->input('delivered_at')}}"/>
        <input type="hidden" name="paid" value="{{app('request')->input('paid')}}"/>

        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">

                <table class="table table-condensed table-striped text-center" style="margin-top: 25px;">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{__('backend.customer')}}</th>
                        <th>{{__('backend.code')}}</th>
                        <th>{{__('backend.receiver_name')}}</th>
                        <th>{{__('backend.dest')}}</th>
                        <th class="reference_number" style="display: none">{{__('backend.reference_number')}}</th>
                        <th>{{__('backend.status')}}</th>
                        <th style="width: 65px !important;min-width: 65px !important;">{{__('backend.fees')}}</th>
                        <th style="width: 65px !important;min-width: 65px !important;">{{__('backend.e.fees')}}</th>
                        <th>{{__('backend.f.fees')}}</th>
                        <th>{{__('backend.total_cost')}}</th>
                        <th>{{__('backend.collected')}}</th>
                        <th class="ship_cost"
                            style="width: 65px !important;min-width: 65px !important;display: none">{{__('backend.ship_cost')}}</th>
                        <th>{{__('backend.residual')}}</th>
                        <th class="fees_from" style="display: none">{{__('backend.fees_from')}}</th>
                        <th class="place" style="display: none">{{__('backend.place')}}</th>
                        <th class="created_date" style="display: none">{{__('backend.created_date')}}</th>
                        <th class="last_update_date" style="display: none">{{__('backend.last_update_date')}}</th>
                        <th class="settlement" style="display: none">{{__('backend.settlement')}}</th>
                        <th class="net" style="display: none">{{__('backend.net')}}</th>
                        <th>{{__('backend.paid')}}</th>
                        <th class="noprint">
                            <input type="checkbox" class="form-control"
                                   style="display: inline-block;vertical-align: bottom;" id="print-all-orders" checked>
                            {{__('backend.Transfer_To_Other_Invoice')}}
                        </th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($AllAcceptedPaginatedOrders as $index => $Order)
                        @php
                            if($invoice->status == 1){
                                $shipCost = $Order->captain_bonus;
                            } else {
                                $shipCost = $Order->driver_bonus ? $Order->driver_bonus : 0;
                                if($Order->status == 5){
                                    $shipCost = $Order->recall_price ? $Order->recall_price : 0;
                                }elseif($Order->status == 8){
                                    $shipCost = $Order->reject_price ? $Order->reject_price : 0;
                                }elseif($Order->status == 4){
                                    $shipCost = 0;
                                }
                            }

                            $fees = 0;
                            if($invoice->status == 1){
                                $fees = $Order->fees;
                            }else{
                                $prices = \App\Models\CustomerPrice::where('customer_id', $Order->customer_id)
                                ->where('status', 1)
                                ->where(function ($q) use ($Order) {
                                    $q->where(function ($q1) use ($Order) {
                                        $q1->where('start_station', $Order->s_government_id);
                                        $q1->where('access_station', $Order->r_government_id);
                                    })->orWhere(function ($q1) use ($Order) {
                                        $q1->where('access_station', $Order->s_government_id);
                                        $q1->where('start_station', $Order->r_government_id);
                                    });
                                })->first();

                                if (empty($prices)) {
                                    $prices = \App\Models\GovernoratePrice::where('status', 1)
                                    ->where(function ($q) use ($Order) {
                                        $q->where(function ($q1) use ($Order) {
                                            $q1->where('start_station', $Order->s_government_id);
                                            $q1->where('access_station', $Order->r_government_id);
                                        })->orWhere(function ($q1) use ($Order) {
                                            $q1->where('access_station', $Order->s_government_id);
                                            $q1->where('start_station', $Order->r_government_id);
                                        });
                                    })->first();
                                }

                                $fees = $Order->delivery_price ? $Order->delivery_price : 0;
                                if($Order->status == 5){
                                    $fees = $prices->recall_cost ? $prices->recall_cost : 0;
                                }elseif($Order->status == 8){
                                    $fees = $prices->reject_cost ? $prices->reject_cost : 0;
                                }elseif($Order->status == 4){
                                    $fees = $prices->cancel_cost ? $prices->cancel_cost : 0;
                                }
                            }

                            /*$fees = $invoice->status == 1 ? $Order->fees : $Order->delivery_price;*/
                            $e_fees = $invoice->status == 1 ? $Order->e_fees : $Order->overweight_cost;
                            $f_fees = $fees + $e_fees;

                            $isDisabled = false;
                            $isChecked = true;
                            if($invoice->status == 1){
                                $isDisabled = true;
                            }

                            $settlement = $Order->cost - $shipCost;
                            $net = $f_fees - $shipCost;
                            $residual = $Order->cost - $f_fees;


                        @endphp
                        <input type="hidden" name="invoice_orders[]"
                               value="{{ $Order->invoice_order_id }}">
                        <tr class='clickable-row order-row'
                            data-href="{{ route('mngrAdmin.orders.show', $Order->id ) }}">
                            <td>{{($index+1)}}</td>
                            <td>{{$Order->customer->name}}</td>
                            <td>{{$Order->order_number}}</td>
                            <td>{{$Order->receiver_name}}</td>
                            <td>{{$Order->to_government->name_en}}</td>
                            <td class="reference_number"
                                style="display: none">{{$Order->reference_number}}</td>
                            <td>{!! $Order->invoice_status_span !!}</td>
                            <td class="fees exclude-td">
                                <input class="form-control fees_list"
                                       name="fees_list[{{@$Order->accepted_id}}]"
                                       value="{{$fees}}"
                                       @if($isDisabled) disabled @endif />
                            </td>
                            <td class="efees exclude-td">
                                <input class="form-control e_fees_list"
                                       name="e_fees_list[{{@$Order->accepted_id}}]"
                                       value="{{$e_fees}}"
                                       @if($isDisabled) disabled @endif />
                            </td>
                            <td class="ffees exclude-td">
                                {{$f_fees}}
                            </td>
                            <td class="total_price">{{$Order->total_price}}</td>
                            <td class="collected">{{ $Order->cost }}</td>

                            <td class="exclude-td ship_cost" style="display: none">
                                <input class="form-control price_list"
                                       name="price_list[{{@$Order->accepted_id}}]"
                                       value="{{$shipCost}}"
                                       @if($isDisabled) disabled @endif />
                            </td>

                            <td class="residual">{{$residual}}</td>

                            <td class="fees_from" style="display: none">{{$Order->payment_name }}</td>
                            <td class="place" style="display: none">{{ $Order->invoice_place }}</td>
                            <td class="created_date"
                                style="display: none">{{$Order->created_at ? date('Y-m-d', strtotime($Order->created_at)) : '-'}}</td>
                            <td class="last_update_date"
                                style="display: none">{{$Order->last_status_date ? date('Y-m-d', strtotime($Order->last_status_date)) : '-'}}</td>
                            <td class="settlement"
                                style="display: none">{{$settlement}}</td>
                            <td class="net"
                                style="display: none">{{$net}}</td>
                            <td>
                                @if ($Order->paid)
                                    <span class=' text-success'><i class='fa fa-check' aria-hidden='true'></i></span>
                                @else
                                    <span class=' text-danger'><i class='fa fa-times' aria-hidden='true'></i></span>
                                @endif
                            </td>
                            <td class="noprint sendIdClass exclude-td">
                                <input class="sendId" type="checkbox" name="sendId[]"
                                       value="{{ ! empty($Order->accepted) ? $Order->accepted->id : '' }}"
                                       @if($isDisabled) disabled @endif
                                       @if($isChecked) checked @endif
                                ><br>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md-12 col-xs-12 col-sm-12">
                <h2 class="text-center">
                    {{__('backend.tasks_list')}}
                </h2>
                <table class="table table-condensed table-striped text-center" style="margin-top: 25px;">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{__('backend.task_no')}}</th>
                        {{--                        <th>{{__('backend.orders_in_task')}}</th>--}}
                        <th>{{__('backend.dropped_date')}}</th>
                        <th style="width: 65px !important;min-width: 80px !important;">{{__('backend.task_cost')}}</th>
                        {{--                        <th>{{__('backend.task_cost')}}</th>--}}
                        <th class="noprint">
                            <input type="checkbox" class="form-control"
                                   style="display: inline-block;vertical-align: bottom;" id="print-all-tasks" checked>
                            {{__('backend.invoice_tasks')}}
                        </th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($pickups as $index => $pickup)
                        @php
                            $bonus = $invoice->status == 1 ? $pickup->captain_bonus : ($pickup->bonus_per_order ? $pickup->bonus_per_order : 0);
                            $pickup_price = $invoice->status == 1 ? $pickup->pickup_price : ($pickup->delivery_price ? $pickup->delivery_price : 0);
                            $total_cost = $bonus + $pickup_price;
                        @endphp
                        <tr class='clickable-row'
                            data-href="{{ route('mngrAdmin.pickups.show', $pickup->id ) }}">
                            <td>{{$pickup->id}}</td>
                            <td>{{$pickup->pickup_number}}</td>
                            {{--                            <td class="pickup_orders">{{$pickup->pickup_orders}}</td>--}}
                            <td>
                                {{$pickup->delivered_at}}
                            </td>
                            <td class="exclude-td">
                                <input class="form-control pickup_price" name="pickups[{{$pickup->id}}][price]"
                                       value="{{$total_cost}}"/>
                            </td>
                            {{--                            <td class="pickup_cost">--}}
                            {{--                                {{ $total_cost }}--}}
                            {{--                            </td>--}}
                            <td class="noprint sendIdTaskClass exclude-td">
                                <input class="sendIdTask" type="checkbox" name="sendIdTask[]"
                                       value="{{ $pickup->pickup_id }}"
                                       @if($invoice->status == 1) disabled @endif  checked>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="col-md-12 col-xs-12 col-sm-12" style="margin-top: 30px;">
                <h2 class="text-center">
                    @if($invoice->status == 1)
                        {{__('backend.Invoice_Bill')}}
                    @else
                        {{__('backend.create_invoice')}}
                    @endif
                </h2>
                <div id="Transaction">

                    <table class="table table-condensed table-striped text-center">
                        <thead style="color:#0e8a29">
                        <tr>
                            <th>{{__('backend.orders')}}#</th>
                            <th>{{__('backend.total_order_cost')}}</th>
                            <th>{{__('backend.total_collect')}}</th>
                            <th>{{__('backend.total_fees')}}</th>
                            <th>{{__('backend.total_task_cost')}}</th>
                            <th>{{__('backend.Ufelix_Deserved')}}</th>
                            <th>{{__('backend.total_residual')}}</th>
                            <th>{{__('backend.add_discount')}}</th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr>
                            <td><span id="orders_number">0</span></td>
                            <td><span id="bonus_delivery">0</span></td>
                            <td><span id="total_collect">0</span></td>
                            <td><span id="total_fees">0</span></td>
                            <td><span id="pickup_cost">0</span></td>
                            <td>
                                <input type="hidden" class="delivery_priceU" name="ufelix_des"
                                       value="0">
                                <span id="delivery_priceU">0</span>
                            </td>
                            <td>
                                <input type="hidden" class="delivery_price" name="customer_des"
                                       value="0">
                                <span id="delivery_price">0</span>
                            </td>

                            <td>
                                <div class="col-md-12">
                                    <div class='input-group'>
                                        <input id="percent" type="text" name="profit" class="form-control" @if($invoice->status) disabled @endif
                                        value="{{  $invoice->profit ? $invoice->profit : 0 }}"/>
                                        <div class="input-group-addon">
                                            <i class="fa fa-dollar"></i>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        </tbody>

                    </table>

                    <input type="hidden" name="Id" value="{{$invoice->id}}">
                    <input type="hidden" name="Type" value="{{$invoice->type}}">
                    <input type="hidden" name="objId" value="{{$invoice->object_id}}">

                @if($invoice->status == 0)
                    @if(permission('transactionInvoices')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                        <button type="button" class="btn btn-primary btn-block noprint" id="transactionBtn"
                                name="Transaction">{{__('backend.Confirm_Transaction')}}</button>
                        @endif
                    @endif

                </div>
            </div>

            {{--            <div class="col-md-12 col-xs-12 col-sm-12" style="margin-top: 30px;">--}}
            {{--                <h2 class="text-center">--}}
            {{--                    {{__('backend.Invoice_Bill')}}--}}
            {{--                </h2>--}}
            {{--                <div id="Transaction">--}}

            {{--                    <table class="table table-condensed table-striped text-center">--}}
            {{--                        <thead style="color:#0e8a29">--}}
            {{--                        <tr>--}}
            {{--                            <th>{{__('backend.orders')}}#</th>--}}
            {{--                            <th>{{__('backend.Total_order_price')}}</th>--}}
            {{--                            <th>{{__('backend.Total_Delivery')}}</th>--}}
            {{--                            <th>{{__('backend.pocket')}}</th>--}}
            {{--                            <th>{{__('backend.remain')}}</th>--}}
            {{--                            <th>{{__('backend.discount')}}</th>--}}
            {{--                            <th>{{__('backend.Corporate_Deserved')}}</th>--}}
            {{--                            <th>{{__('backend.pickup_cost')}}</th>--}}
            {{--                            <th>{{__('backend.final_deserved')}}</th>--}}
            {{--                            <th>{{__('backend.Ufelix_Deserved')}}</th>--}}
            {{--                            @if( $invoice->status == 0)--}}
            {{--                                <th>{{__('backend.add_discount')}}</th>--}}
            {{--                            @endif--}}
            {{--                        </tr>--}}
            {{--                        </thead>--}}
            {{--                        <tbody>--}}

            {{--                        <tr>--}}
            {{--                            <td><span id="orders_number">0</span></td>--}}
            {{--                            <td>--}}
            {{--                                <span id="priceTotal">0</span>--}}
            {{--                            </td>--}}
            {{--                            <td>--}}
            {{--                                <span id="deliveryTotal">0</span>--}}
            {{--                            </td>--}}
            {{--                            <td>--}}
            {{--                                    <span--}}
            {{--                                        id="pocketTotal">0</span>--}}

            {{--                            </td>--}}
            {{--                            <td>--}}
            {{--                                    <span--}}
            {{--                                        id="remainTotal">0</span>--}}
            {{--                            </td>--}}

            {{--                            <td id="percentTd">--}}
            {{--                                <strong--}}
            {{--                                    id="percentTotal">{{ $invoice->profit ? $invoice->profit : 0}}</strong>--}}
            {{--                            </td>--}}
            {{--                            <td>--}}
            {{--                                    <span id="delivery_price">--}}
            {{--                                            0--}}
            {{--                                    </span>--}}
            {{--                            </td>--}}
            {{--                            <td>--}}
            {{--                                <span id="pickup_cost">0</span>--}}
            {{--                            </td>--}}
            {{--                            <td>--}}
            {{--                                <input type="hidden" class="delivery_price" name="customer_des"--}}
            {{--                                       value="0">--}}
            {{--                                <span id="final_deserved">0</span>--}}
            {{--                            </td>--}}
            {{--                            <td>--}}
            {{--                                <input type="hidden" class="delivery_priceU" name="ufelix_des"--}}
            {{--                                       value="0">--}}
            {{--                                <span id="delivery_priceU">--}}
            {{--                                           0--}}
            {{--                                    </span>--}}
            {{--                            </td>--}}
            {{--                            <td>--}}
            {{--                                @if( $invoice->status == 0)--}}

            {{--                                    <div class="col-md-12">--}}
            {{--                                        <div class='input-group'>--}}
            {{--                                            <input id="percent" type="text" name="profit" class="form-control"--}}
            {{--                                                   value="{{  $invoice->profit ? $invoice->profit : 0 }}"/>--}}
            {{--                                            <div class="input-group-addon">--}}
            {{--                                                <i class="fa fa-dollar"></i>--}}
            {{--                                            </div>--}}
            {{--                                        </div>--}}
            {{--                                    </div>--}}
            {{--                                @endif--}}
            {{--                            </td>--}}
            {{--                        </tr>--}}
            {{--                        </tbody>--}}

            {{--                    </table>--}}

            {{--                    <input type="hidden" name="Id" value="{{$invoice->id}}">--}}
            {{--                    <input type="hidden" name="Type" value="{{$invoice->type}}">--}}
            {{--                    <input type="hidden" name="objId" value="{{$invoice->object_id}}">--}}

            {{--                @if($invoice->status == 0)--}}
            {{--                    @if(permission('transactionInvoices')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->--}}
            {{--                        <button type="button" class="btn btn-primary btn-block noprint" id="transactionBtn"--}}
            {{--                                name="Transaction">{{__('backend.Confirm_Transaction')}}</button>--}}
            {{--                        @endif--}}
            {{--                    @endif--}}


            {{--                </div>--}}
            {{--            </div>--}}

        </div>
    </form>

    <!-- Modal For Change Ship Cost-->
    <div class="modal fade" id="shipCostModal" tabindex="-1" role="dialog" aria-labelledby="shipCostModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">{{__('backend.change_ship_cost')}}</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <div class="group-control col-md-10 col-sm-10">
                                <input type="text" value="" name="ship_cost" id="ship_cost" class="form-control">
                            </div>
                        </div>
                        <div class="col-6"></div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal"
                                    style="margin-left: 15px;">{{__('backend.close')}}
                            </button>
                            <button type="button" class="btn btn-primary pull-right" id="change_ship_cost"
                            >{{__('backend.change')}}</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script>

        $(function () {
            $('body').on('click', '#print-all-orders', function (e) {
                $('input:checkbox.sendId').not("[disabled]").prop('checked', this.checked);
                calculate_invoice();
            });

            $('body').on('click', '#print-all-tasks', function (e) {
                $('input:checkbox.sendIdTask').not("[disabled]").prop('checked', this.checked);
                calculate_invoice();
            });

            function getUrlVars() {
                var vars = [], hash;
                var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                for (var i = 0; i < hashes.length; i++) {
                    hash = hashes[i].split('=');
                    vars[hash[0]] = hash[1];
                }
                return vars;
            }

            $("#limit").change(function () {
                let baseUrl = location.protocol + '//' + location.host + location.pathname + '/?limit=' + $(this).val();
                let url_vars = getUrlVars();
                console.log(url_vars);
                let queryString = '';
                for (var i in url_vars) {
                    if (i != 'limit' && url_vars[i] != undefined) {
                        queryString += '&' + i + '=' + url_vars[i];
                    }
                }

                window.location.href = baseUrl + queryString;
            });

            $("#editShipCost").click(function (e) {
                e.preventDefault();
                $("#shipCostModal").modal("show");
            });

            $("#change_ship_cost").click(function (e) {
                e.preventDefault();
                $("#shipCostModal").modal("hide");
                if ($("#ship_cost").val()) {
                    $('.order-row').find('.price_list').val($("#ship_cost").val()).trigger('input');
                    // $('.order-row').each(function (e) {
                    //     let shippCostEle = $(this).find('.price_list');
                    //     shippCostEle.val($("#ship_cost").val()).trigger('input');
                    // });
                }
            });

            $("input[name='thcheckbox[]']").change(function () {
                // if ($('[name="thcheckbox[]"]:checked').length) {
                //     $("#ordersTable").css("display", "block");
                // } else {
                //     $("#ordersTable").css("display", "table");
                // }
                $("input[name='thcheckbox[]']").each(function () {
                    if (this.checked) {
                        $("." + $(this).val()).show();
                    } else {
                        $("." + $(this).val()).hide();
                    }
                });
            });

            $(window).keydown(function (event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });

            calculate_invoice();

            $('.sendId').on('change', function () {
                calculate_invoice();
            });

            /*transactionBtn*/
            $('#transactionBtn').on('click', function () {
                if (!$(this).hasClass("disabled")) {
                    $.ajax({
                        url: '{{ url("mngrAdmin/invoices/Transact") }}',
                        type: 'post',

                        data: $("#transactionForm").serialize(),
                        success: function (data) {

                            if (data['statusCode'] == '420') {

                                $.alert({
                                    title: '',
                                    theme: 'modern',
                                    class: 'danger',
                                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Confirm Close</span></div><div style="font-weight:bold;"><p> We can not continue Transaction Operation .</p><p class="text-primary">' + data['message'] + '</p></div>',
                                });
                            } else {

                                $.alert({
                                    title: '',
                                    theme: 'modern',
                                    class: 'success',
                                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-smile-o  text-success"></i></span><span class="jconfirm-title">Confirm Done</span></div><div style="font-weight:bold;"><p class="text-primary">' + data['message'] + '</p></div>',
                                    onClose: function () {
                                        window.location.reload();
                                    },
                                });
                            }
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                    // }
                }

            });

            {{--/* changePercentDriver*/--}}
            {{--$("#changePercentDriver").on('click', function () {--}}
            {{--    var percent = parseFloat($('#percentDriver').val());--}}
            {{--    var percentVal = percent * parseFloat("{{$totalDelivery}}") / 100;--}}
            {{--    $("#d_profit").text(percent);--}}
            {{--    $("#d_commission").text(percentVal);--}}
            {{--    $("#u_commission").text(parseFloat("{{$totalDelivery}}") - parseFloat(percentVal));--}}
            {{--    if (parseFloat(percentVal) > parseFloat($('#c_Delivery').text())) {--}}

            {{--        $('.td1').text(0);--}}
            {{--        $('.td2').text(parseFloat(percentVal) - parseFloat($('#c_Delivery').text()));--}}
            {{--        $('.td3').text(parseFloat(percentVal));--}}
            {{--        $('.td4').text(parseFloat("{{$totalItem}}") - (parseFloat(percentVal) - parseFloat($('#c_Delivery').text())));--}}


            {{--        $('.inputtd1').val(0);--}}
            {{--        $('.inputtd2').val(parseFloat(percentVal) - parseFloat($('#c_Delivery').text()));--}}
            {{--        $('.inputtd3').val(parseFloat(percentVal));--}}
            {{--        $('.inputtd4').val(parseFloat("{{$totalItem}}") - (parseFloat(percentVal) - parseFloat($('#c_Delivery').text())));--}}


            {{--    } else if (parseFloat(percentVal) < parseFloat($('#c_Delivery').text())) {--}}

            {{--        $('.td1').text(parseFloat($('#c_Delivery').text()) - parseFloat(percentVal));--}}
            {{--        $('.td2').text(0);--}}
            {{--        $('.td3').text(parseFloat(percentVal));--}}
            {{--        $('.td4').text(parseFloat("{{$totalItem}}") - (parseFloat(percentVal) - parseFloat($('#c_Delivery').text())));--}}

            {{--        $('.inputtd1').val(parseFloat($('#c_Delivery').text()) - parseFloat(percentVal));--}}
            {{--        $('.inputtd2').val(0);--}}
            {{--        $('.inputtd3').val(parseFloat(percentVal));--}}
            {{--        $('.inputtd4').val(parseFloat("{{$totalItem}}") - (parseFloat(percentVal) - parseFloat($('#c_Delivery').text())));--}}


            {{--    } else if (parseFloat(percentVal) == parseFloat($('#c_Delivery').text())) {--}}

            {{--        $('.td1').text(0);--}}
            {{--        $('.td2').text(0);--}}
            {{--        $('.td3').text(parseFloat(percentVal));--}}
            {{--        $('.td4').text(parseFloat("{{$totalItem}}") - (parseFloat(percentVal) - parseFloat($('#c_Delivery').text())));--}}

            {{--        $('.inputtd1').val(0);--}}
            {{--        $('.inputtd2').val(0);--}}
            {{--        $('.inputtd3').val(parseFloat(percentVal));--}}
            {{--        $('.inputtd4').val(parseFloat("{{$totalItem}}") - (parseFloat(percentVal) - parseFloat($('#c_Delivery').text())));--}}
            {{--    }--}}

            {{--});--}}

            $("#percent").on('input', function () {
                calculate_invoice();
            });

            $(".e_fees_list").on('input', function () {
                const self = $(this).val() ? parseFloat($(this).val()) : 0;
                let parent = $(this).parents('tr');
                let shippCostEle = parent.find('.price_list');
                let netEle = parent.find('.net');
                let residualEle = parent.find('.residual');
                let collectedEle = parent.find('.collected');
                let fFeesEle = parent.find('.ffees');
                let feesEle = parent.find('.fees_list');

                let fFees = parseFloat(feesEle.val()) + self;

                fFeesEle.html(fFees);
                netEle.html(fFees - parseFloat(shippCostEle.val()));
                residualEle.html(parseFloat(collectedEle.html()) - fFees);

                calculate_invoice();
            });

            $(".fees_list").on('input', function () {
                const self = $(this).val() ? parseFloat($(this).val()) : 0;
                let parent = $(this).parents('tr');
                let eFeesEle = parent.find('.e_fees_list');
                let fFeesEle = parent.find('.ffees');
                let shippCostEle = parent.find('.price_list');
                let netEle = parent.find('.net');
                let residualEle = parent.find('.residual');
                let collectedEle = parent.find('.collected');

                let fFees = parseFloat(eFeesEle.val()) + self;

                fFeesEle.html(fFees);
                netEle.html(fFees - parseFloat(shippCostEle.val()));
                residualEle.html(parseFloat(collectedEle.html()) - fFees);

                calculate_invoice();
            });

            $(".fees_list").on('keyup', function (e) {
                if (e.which === 13) {
                    $(this).parents('tr').next().next().find('.fees_list').focus();
                }
            });

            $(".e_fees_list").on('keyup', function (e) {
                if (e.which === 13) {
                    $(this).parents('tr').next().next().find('.e_fees_list').focus();
                }
            });

            $(".price_list").on('keyup', function (e) {
                if (e.which === 13) {
                    $(this).parents('tr').next().next().find('.price_list').focus();
                }
            });

            $(".price_list").on('input', function () {
                const self = $(this).val() ? parseFloat($(this).val()) : 0;
                let parent = $(this).parents('tr');
                let settlementEle = parent.find('.settlement');
                let collectedEle = parent.find('.collected');
                let fFeesEle = parent.find('.ffees');
                let netEle = parent.find('.net');
                let residualEle = parent.find('.residual');

                settlementEle.html(parseFloat(collectedEle.html()) - self);
                netEle.html(parseFloat(fFeesEle.html()) - self);
                residualEle.html(parseFloat(collectedEle.html()) - parseFloat(fFeesEle.html()));

                calculate_invoice();
            });

            $(".pickup_bonus").on('input', function () {
                const self = $(this).val() ? parseFloat($(this).val()) : 0;
                var total_bonus = parseInt($(this).parents('tr').find('.pickup_orders').text()) * pickup_bonus;
                var pickup_price = parseFloat($(this).parents('tr').find('.pickup_price').val());
                var one_pickup_cost = self + total_bonus;
                $(this).parents('tr').find('.pickup_cost').text(one_pickup_cost);
                calculate_invoice();
            });

            $(".pickup_price").on('input', function () {
                const self = $(this).val() ? parseFloat($(this).val()) : 0;
                var pickup_bonus = parseFloat($(this).parents('tr').find('.pickup_bonus').val());
                var total_bonus = parseInt($(this).parents('tr').find('.pickup_orders').text()) * (pickup_bonus ? pickup_bonus : 0);
                var one_pickup_cost = self + total_bonus;
                $(this).parents('tr').find('.pickup_cost').text(one_pickup_cost);
                calculate_invoice();
            });

            function calculate_invoice() {
                var orders_number = 0;
                var total_collect = 0;
                var bonus_delivery = 0;
                var total_cost = 0;
                var pickup_cost = 0;
                var total_fees = 0;
                var total_f_fees = 0;
                var total_residual = 0;
                var total_settlement = 0;
                var deserved = 0;
                var ufelix_deserved = 0;
                var discount = $("#percent").val() ? parseFloat($("#percent").val()) : 0;

                if ((!$('input:checkbox.sendId').length || !$('input:checkbox.sendId:checked').not("[disabled]").length) &&
                    (!$('input:checkbox.sendIdTask').length || !$('input:checkbox.sendIdTask:checked').not("[disabled]"))) {
                    $("#transactionBtn").addClass('disabled');
                } else {
                    $("#transactionBtn").removeClass('disabled');
                }

                $('.sendId:checkbox:checked').each(function (e) {
                    let parent = $(this).parents('tr');
                    orders_number += 1;
                    total_collect += parseFloat(parent.find('.collected').text());
                    bonus_delivery += parseFloat(parent.find('.price_list').val() ? parent.find('.price_list').val() : 0);
                    total_fees += parseFloat(parent.find('.fees_list').val() ? parent.find('.fees_list').val() : 0);
                    total_f_fees += parseFloat(parent.find('.ffees').text());
                    total_residual += parseFloat(parent.find('.residual').text());
                    total_settlement += parseFloat(parent.find('.settlement').text());
                    total_cost += parseFloat(parent.find('.total_price').text());
                });

                $('.sendIdTask:checkbox:checked').each(function (e) {
                    let parent = $(this).parents('tr');
                    pickup_cost += parseFloat(parent.find('.pickup_price').val());
                });

                ufelix_deserved = total_f_fees + pickup_cost - discount;
                deserved = total_collect - ufelix_deserved;

                $("#orders_number").text(orders_number);
                $("#total_collect").text(total_collect);
                $("#bonus_delivery").text(total_cost);
                $("#pickup_cost").text(pickup_cost);
                $("#total_fees").text(total_f_fees);
                $('.delivery_price').val(deserved);
                $('#delivery_price').text(deserved);
                $('.delivery_priceU').val(ufelix_deserved);
                $('#delivery_priceU').text(ufelix_deserved);
            }


            // function calculate_invoice() {
            //     var orders_number = 0;
            //     var total_price = 0;
            //     var total_fees = 0;
            //     var pocket = 0;
            //     var deserved = 0;
            //     var final_deserved = 0;
            //     var pickup_cost = 0;
            //     var percent = 0;
            //     var remain = 0;
            //     var ufelix_deserved = 0;
            //     $('.sendId:checkbox:checked').each(function (e) {
            //         orders_number += 1;
            //         if ($(this).attr('data-price')) {
            //             total_price += parseFloat($(this).attr('data-price'));
            //         }
            //         if ($(this).attr('data-fees')) {
            //             total_fees += parseFloat($(this).attr('data-fees'));
            //         }
            //         if ($(this).attr('data-pocket')) {
            //             pocket += parseFloat($(this).attr('data-pocket'));
            //         }
            //         if ($(this).attr('data-deserved')) {
            //             deserved += parseFloat($(this).attr('data-deserved'));
            //         }
            //     });
            //
            //     $('.pickup_cost').each(function (e) {
            //         pickup_cost += parseFloat($(this).text());
            //     });
            //
            //     percent = parseFloat($('#percentTotal').text());
            //     percent = percent ? percent : 0;
            //     remain = (total_fees - pocket) - percent;
            //     deserved += percent;
            //     final_deserved = deserved - pickup_cost;
            //     ufelix_deserved = remain + pickup_cost;
            //
            //     $("#orders_number").text(orders_number);
            //     $("#priceTotal").text(total_price);
            //     $("#priceTotalInput").val(total_price);
            //     $("#deliveryTotal").text(total_fees);
            //     $("#pocketTotal").text(pocket);
            //     $('#remainTotal').text(remain);
            //     $("#percentTotal").text(percent);
            //     $("#pickup_cost").text(pickup_cost);
            //     $('#delivery_price').text(deserved);
            //     $('.delivery_price').val(final_deserved);
            //     $('#final_deserved').text(final_deserved);
            //     $('.delivery_priceU').val(ufelix_deserved);
            //     $('#delivery_priceU').text(ufelix_deserved);
            // }
        })
    </script>
@endsection


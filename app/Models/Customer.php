<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{

    protected $hidden = ['password', 'created_at', 'updated_at', 'provider', 'provider_id', 'type', 'is_block', 'sms_code'];
    protected $fillable = ['name', 'email', 'password', 'mobile', 'sms_code'];

    //Customer Relationships ...
    public function devices()
    {
        return $this->hasMany('App\Models\Device', 'object_id', 'id')
            ->where('object_type', 1);
    }

    public function CustomerDevice()
    {
        return $this->hasMany('App\Models\Device');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    public function task_days()
    {
        return $this->hasMany('App\Models\CustomerTaskDay');
    }

    public function pendingOrders()
    {
        return $this->hasMany('App\Models\Order')->where('orders.status', 0);
    }

    public function acceptedOrders()
    {
        return $this->hasMany('App\Models\Order')->where('orders.status', 1);
    }

    public function receivedOrders()
    {
        return $this->hasMany('App\Models\Order')->where('orders.status', 2);
    }

    public function deliveredOrders()
    {
        return $this->hasMany('App\Models\Order')->where('orders.status', 3);
    }

    public function cancelledOrders()
    {
        return $this->hasMany('App\Models\Order')->where('orders.status', 4);
    }

    public function recalledOrders()
    {
        return $this->hasMany('App\Models\Order')->where('orders.status', 5);
    }

    public function droppedOrders()
    {
        return $this->hasMany('App\Models\Order')->where('orders.status', 7);
    }

    public function rejectedOrders()
    {
        return $this->hasMany('App\Models\Order')->where('orders.status', 8);
    }

    public function prices()
    {
        return $this->hasMany('App\Models\CustomerPrice');
    }

    public function Corporate()
    {
        return $this->belongsTo('App\Models\Corporate');
    }

    public function governorate()
    {
        return $this->belongsTo('App\Models\Governorate', 'government_id');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City', 'city_id');
    }

    public function collection_orders()
    {
        return $this->hasOne('App\Models\CollectionOrder', 'customer_id');
    }

    public function store()
    {
        return $this->belongsTo('App\Models\Store', 'store_id');
    }

    public function getIsSomethingWrongAttribute()
    {

        $result = false;

        if (!$this->store_id) {
            $result = true;
        } elseif (!$this->government_id) {
            $result = true;
        }

        return $result;
    }

    //Customer Attributes ...
    public function getBlockSpanAttribute($value)
    {
        /*
        * is_block = 0 :: Not Blocked
        * is_block = 1 :: Blocked
        */
        if ($this->is_block == 1) {
            $value = "<span class='badge badge-pill label-danger'>" . __('backend.blocked') . "</span>";
        } else {
            $value = "<span class='badge badge-pill label-info'>" . __('backend.not_blocked') . "</span>";
        }
        return $value;
    }

    public function getBlockTxtAttribute($value)
    {
        /*
        * is_block = 0 :: Not Blocked
        * is_block = 1 :: Blocked
        */
        if ($this->is_block == 1) {
            $value = __('backend.not_blocked');
        } else {
            $value = __('backend.blocked');
        }
        return $value;
    }

    public function getActiveSpanAttribute($value)
    {
        /*
        * is_active = 0 :: Not Active
        * is_active = 1 :: Active
        */
        if ($this->is_active == 1) {
            $value = "<span class='badge badge-pill label-success'>" . __('backend.active') . "</span>";
        } else {
            $value = "<span class='badge badge-pill label-danger'>" . __('backend.not_activated') . "</span>";
        }
        return $value;
    }

    public function getMobileVerifySpanAttribute($value)
    {
        if ($this->is_verify_mobile != 1) {
            $value = "<span class='badge badge-pill label-danger'>" . __('backend.not_verified') . "</i></span>";
        } else {
            $value = "<span class='badge badge-pill label-success'>" . __('backend.verified') . "</span>";
        }
        return $value;
    }

    public function getStatusTxtAttribute($value)
    {
        /*
        * is_active = 0 :: Not Active
        * is_active = 1 :: Active
        */
        if ($this->is_active != 1) {
            $value = __('backend.active');
        } else {
            $value = __('backend.not_activated');
        }
        return $value;
    }

    /*type_span*/
    public function getTypeSpanAttribute()
    {
        if ($this->role_id == 3) {
            $value = "<span class='badge badge-pill label-info'>Individual Account</span>";
        } else {
            $value = "<span class='badge badge-pill label-warning'>Corporate Account</span>";
        }
        return $value;
    }


    public function getImageAttribute($value)
    {
        if ($value != "") {
            if (\File::exists(public_path("/api_uploades/client/profile/" . $value))) {
                $value = asset("/api_uploades/client/profile/" . $value);
            } else {
                $value = "";
            }
        } else {
            $value = "";
        }
        return $value;
    }

    #hash password
    public function setPasswordAttribute($pass)
    {
        $this->attributes['password'] = bcrypt($pass);
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [
            'type' => 'customer'
        ];
    }
}

@extends('captain.layouts.app')
@section('css')
    <title>{{__('captain.the_orders')}}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <style type="text/css">
        @media only screen and (max-width: 580px) {
            .hide-td {
                display: none;
            }
        }

        .box-body {
            min-height: 100px;
        }

        .stylish-input-group .input-group-addon {
            background: white !important;
        }

        .stylish-input-group .form-control {
            border-right: 0;
            box-shadow: 0 0 0;
            border-color: #ccc;
        }

        .stylish-input-group button {
            border: 0;
            background: transparent;
        }

        .toggle.ios, .toggle-on.ios, .toggle-off.ios {
            border-radius: 20px;
        }

        .toggle.ios .toggle-handle {
            border-radius: 20px;
        }

        .filter {
            margin-bottom: 10px;
        }

        .collected_cost.disabled {
            pointer-events: none;
            opacity: 0.6;
        }

        .firstOne td {
            border-top: 3px solid red !important;
        }

        .firstOne td:first-child {
            border-left: 3px solid red !important;
        }

        .firstOne td:last-child {
            border-right: 3px solid red !important;
        }

        .secondOne td {
            border-bottom: 2px solid red !important;
        }

        .secondOne td:first-child {
            border-left: 3px solid red !important;
        }

        .secondOne td:last-child {
            border-right: 3px solid red !important;
        }

        .popover {
            max-width: 800px !important;
        }

        .popover-title .close {
            position: relative;
            bottom: 1px;
            right: 2px;
        }

        .popover-title {
            padding: 0 !important;
            border-bottom: none !important;
        }

        .tooltip-inner {
            max-width: none !important;
            width: 100% !important;
        }

        .scan-problems.table-striped > tbody > tr:nth-of-type(odd) {
            background-color: initial !important;
        }
    </style>
@endsection

@section('header')
    <div class="page-header clearfix">
        <h3 style="display: inline-block; float: left">
            <i class="glyphicon glyphicon-align-justify"></i>
            @if(app('request')->input('type') != "") {{__('captain.'.app('request')->input('type'))}} @endif
        </h3>

        <a href="{{url('captainDashboard/reports/captain_report') . '?type=' . app('request')->input('type')}}"
           data-href="{{url('captainDashboard/reports/captain_report') . '?type=' . app('request')->input('type')}}"
           class="btn btn-info" style="float: right" id="download_excel">
            {{__('captain.download_excel')}}
        </a>
    </div>
@endsection
@section('content')

    @if (session('success'))
        <div class="alert alert-success alert-block text-center">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ session('success') }}</strong>
        </div>
    @endif
    @if (session('warning'))
        <div class="alert alert-warning alert-block text-center">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ session('warning') }}</strong>
        </div>
    @endif
    @include('error')

    <div class="alert alert-danger" hidden>
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong id="errorId"></strong>
    </div>

    <form action="{{URL::asset('/captainDashboard/orders')}}" method="get">
        <div class="row" style="margin-bottom:15px;" id="orders">
            <div class="col-md-12">
                <div class="col-md-3 col-sm-3 filter">
                    <div id="imaginary_container">
                        <div class="input-group stylish-input-group">
                            <input type="text" class="form-control" id="search-field" name="search"
                                   value="{{app('request')->search}}"
                                   placeholder="{{__('captain.search')}} ">
                            <span class="input-group-addon">
                              <button type="button" id="search-btn1">
                                <span class="glyphicon glyphicon-search"></span>
                              </button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 filter">
                    <div class='input-group date ' id="datepicker1">
                        <input type="text" id="fromDate" name="fromDate" class="form-control"
                               value="{{app('request')->fromDate}}"
                               placeholder="{{__('captain.From_Date')}}" autocomplete="off"/>
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 filter">
                    <div class='input-group date ' id="datepickers">
                        <input type="text" id="toDate" name="toDate" class="form-control"
                               value="{{app('request')->toDate}}"
                               placeholder="{{__('captain.To_Date')}}" autocomplete="off"/>
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                    </div>
                </div>

                @if(Auth::guard('captain')->user()->is_manager)
                <div class="col-md-3 col-sm-3 filter">
                    <div class="form-group">
                        <select class="form-control" name="driver_id" id="driver_id">
                            <option value="">{{__('captain.Choose_Driver')}}</option>
                            @if(! empty($online_drivers))
                                @foreach($online_drivers as $driver)
                                    <option value="{{$driver->id}}">{{$driver->name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                @endif

                {{--                <div class="col-md-3 col-sm-3 filter">--}}
                {{--                    <div class='input-group date' id='datepickerFilter'>--}}
                {{--                        <input type="text" id="date" name="date" class="form-control" value="{{app('request')->date}}"/>--}}
                {{--                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
            </div>
        </div>
    </form>
    <div class='list'>
        @if(app('request')->input('type') == 'ofd')
            @include('captain.orders.ofd_table')
        @else
            @include('captain.orders.table')
        @endif
    </div>
@endsection
@section('scripts')
    @include('captain.orders.js-search')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script type="text/javascript" src="{{asset('/assets/scripts/bootstrap-notify.min.js')}}"></script>

    <script>
        $('.testtooltip').popover({
            container: 'body'
            // content: $('#myPopoverContent').html(),
            // html: true,
            // trigger: "hover"
        });

        $(document).on("click", ".popover .close", function () {
            $(this).parents(".popover").popover('hide');
            $(this).parents(".popover").remove();
        });

        $.notifyDefaults({
            type: 'success',
            allow_dismiss: true
        });

        $(document).ready(function () {
            $('body').on('click', '#toggle', function () {

                switch ($(this).attr('data-state')) {
                    case "1" :
                        $("input.selectOrder:not(:disabled)").prop('checked', true).trigger('change');
                        // $(".selectOrder").prop('checked', true).trigger('change');
                        $(this).attr('data-state', "2");
                        break;
                    case "2" :
                        $("input.selectOrder:not(:disabled)").prop('checked', false).trigger('change');
                        // $(".selectOrder").prop('checked', false).trigger('change');
                        $(this).attr('data-state', "1");
                        break;
                }
            });

            $(document).on("click", ".inline_comment", function (e) {
                e.preventDefault();
                var self = $(this);
                $.ajax({
                    url: "{{ url('/captainDashboard/orders/add_order_comment')}}",
                    type: 'post',
                    data: {
                        comment: self.prev().val(),
                        user_type: 3,
                        order_id: self.attr('data-id'),
                        '_token': "{{csrf_token()}}"
                    },
                    success: function (data) {
                        self.parents('tr').prev().removeClass('firstOne');
                        self.parents('tr').removeClass('secondOne');
                        self.addClass('disabled');
                        self.prev().val('');
                        $.notify(data);
                        setTimeout(function () {
                            $.notifyClose('top-right');
                        }, 2000);
                    }
                });
            });

            $(document).on("click", ".save_status", function (e) {
                e.preventDefault();
                var self = $(this);
                let parent = $(this).parents("tr");
                let inline_comment = parent.next().find('.inline_comment');
                let comment = parent.next().find('.comment_text').val();
                let total_cost = parent.find('.total_cost').html();
                let collected_cost = parent.find('.collected_cost').val();
                let delivery_status = parent.find('.delivery_status:checked').val();
                $.ajax({
                    url: "{{ url('/captainDashboard/orders/save_status')}}",
                    type: 'post',
                    data: {
                        comment: comment,
                        total_cost: total_cost,
                        collected_cost: collected_cost,
                        delivery_status: delivery_status,
                        order_id: self.attr('data-id'),
                        '_token': "{{csrf_token()}}"
                    },
                    success: function (data) {
                        if (data.error) {
                            $.notify({
                                message: data.message
                            }, {
                                type: 'danger'
                            });
                        } else {
                            if (comment) {
                                self.parents('tr').next().removeClass('secondOne');
                                self.parents('tr').removeClass('firstOne');
                            }

                            self.remove();
                            inline_comment.remove();
                            $.notify({
                                message: data.message
                            });

                            location.reload();
                        }

                        setTimeout(function () {
                            $.notifyClose('top-right');
                        }, 2000);
                    }
                });
            });

            $(document).on("input", ".comment_text", function (e) {
                if ($(this).val()) {
                    $(this).next().removeClass('disabled');
                } else {
                    $(this).next().addClass('disabled');
                }
            });

            $('body').on('change', '.delivery_status', function () {
                let parent = $(this).parents("tr");
                let total_cost = parent.find('.total_cost');
                let collected_cost = parent.find('.collected_cost');
                collected_cost.removeClass('disabled');

                if ($(this).val() == 1) {
                    if (!collected_cost.val() || collected_cost.val() === '0') {
                        collected_cost.val(total_cost.html());
                    }
                }

                if ($(this).val() == 3) {
                    collected_cost.val(0).addClass('disabled');
                }

                if (this.checked) {
                    parent.find('.save_status').removeClass('disabled');
                } else {
                    parent.find('.save_status').addClass('disabled');
                }
            });

            $(document).on('dblclick', '.delivery_status', function () {
                if (this.checked) {
                    let parent = $(this).parents("tr");
                    $(this).prop('checked', false);
                    parent.find('.save_status').addClass('disabled');
                }
            });

            $(document).on("paste", "input.collected_cost", function (e) {
                if (e.originalEvent.clipboardData.getData('Text').match(/[^\d+\.\d+-]/)) {
                    e.preventDefault();
                }
            });

            $(document).on("keypress", "input.collected_cost", function (e) {
                if ((e.which < 48 || e.which > 58) && e.which != 46 && e.which != 41 && e.which != 45) {
                    return false;
                }
            });
        });

        $(function () {
            /*print*/
            $('.toggle').bootstrapToggle();


            $('body').on('change', '.selectOrder', function (e) {
                if ($('input[name="select[]"]').is(':checked')) {
                    $("#delay_orders").removeClass('disabled');
                    $("#deliver_orders").removeClass('disabled');
                    $("#receive_orders").removeClass('disabled');
                } else {
                    $("#delay_orders").addClass('disabled');
                    $("#deliver_orders").addClass('disabled');
                    $("#receive_orders").addClass('disabled');
                }
            });

            $('input[type=radio][name=delay_comment]').change(function () {
                if (this.value == 'Other reason') {
                    $("#delay_reason").show();
                } else {
                    $("#del_reason").val("");
                    $("#delay_reason").hide();
                }
            });

            $('body').on('click', '#saveDelay', function (e) {
                if ($("#delay_date").val()) {
                    let ids = [];
                    $(".selectOrder:checked").each(function () {
                        ids.push($(this).val());
                    });
                    $.ajax({
                        url: "{{ url('/captainDashboard/delay_orders')}}",
                        type: 'post',
                        data: {
                            ids: ids,
                            delay_date: $("#delay_date").val(),
                            delay_comment: $("input[name='delay_comment']:checked").val(),
                            delay_reason: $("#del_reason").val(),
                            change_status: $("input[name='change_status']:checked").val(),
                            flash: 1,
                            '_token': "{{csrf_token()}}"
                        },
                        success: function (data) {
                            // if (data['status'] != 'false') {
                            location.reload();
                            // }
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }
            });

            $('body').on('click', '#deliver_orders', function (e) {

                let ids = [];

                $(".selectpdf input:checked").each(function () {
                    ids.push($(this).val());

                });

                $.confirm({
                    title: '',
                    theme: 'modern',
                    class: 'danger',
                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-meh-o text-success"></i></span><span class="jconfirm-title">{{__("captain.Confirm")}}</span></div><div style="font-weight:bold;"><p>{{__("captain.delivery_orders_confirm")}}</p></div>',
                    buttons: {
                        confirm: {
                            text: '{{__("captain.Ok")}}',
                            btnClass: 'btn-danger',
                            action: function () {
                                let ids = [];
                                $(".selectOrder:checked").each(function () {
                                    ids.push($(this).val());
                                });
                                $.ajax({
                                    url: "{{ url('/captainDashboard/deliver_orders')}}",
                                    type: 'post',
                                    data: {ids: ids, '_token': "{{csrf_token()}}"},
                                    success: function (data) {
                                        // if (data['status'] != 'false') {
                                        location.reload();
                                        // }
                                    },
                                    error: function (data) {
                                        console.log('Error:', data);
                                    }
                                });
                            }
                        },
                        cancel: {
                            text: '{{__("captain.Cancel")}}',
                            action: function () {
                            }
                        }
                    }
                });

            });

            $('body').on('click', '#receive_orders', function (e) {

                let ids = [];

                $(".selectpdf input:checked").each(function () {
                    ids.push($(this).val());

                });

                $.confirm({
                    title: '',
                    theme: 'modern',
                    class: 'danger',
                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-meh-o text-success"></i></span><span class="jconfirm-title">{{__("captain.Confirm")}}</span></div><div style="font-weight:bold;"><p>{{__("captain.receive_orders_confirm")}}</p></div>',
                    buttons: {
                        confirm: {
                            text: '{{__("captain.Ok")}}',
                            btnClass: 'btn-danger',
                            action: function () {
                                let ids = [];
                                $(".selectOrder:checked").each(function () {
                                    ids.push($(this).val());
                                });
                                $.ajax({
                                    url: "{{ url('/captainDashboard/receive_orders')}}",
                                    type: 'post',
                                    data: {ids: ids, '_token': "{{csrf_token()}}"},
                                    success: function (data) {
                                        // if (data['status'] != 'false') {
                                        location.reload();
                                        // }
                                    },
                                    error: function (data) {
                                        console.log('Error:', data);
                                    }
                                });
                            }
                        },
                        cancel: {
                            text: '{{__("captain.Cancel")}}',
                            action: function () {
                            }
                        }
                    }
                });

            });

            $('#delay_date').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                language: 'en',
                todayHighlight: true,
                startDate: new Date()
            });

            $('body').on('click', '.printdata', function (e) {

                var url = "{{URL::asset('/captainDashboard/print_pdf/')}}/" + $(this).attr('data-id');
                $('<form action="' + url + '" method="get" target="_blank"></form>').appendTo('#orders').submit();
            });

            $('body').on('click', '.delete-order', function (e) {

                var id = $(this).attr('data-id');
                $.confirm({
                    title: '',
                    theme: 'modern',
                    class: 'danger',
                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Confirm Delete</span></div><div style="font-weight:bold;"><p>Are You sure To Delete This Order ?</p></div>',
                    buttons: {
                        confirm: {
                            text: 'Delete Order',
                            btnClass: 'btn-danger',
                            action: function () {
                                $.ajax({
                                    url: "{{ url('/captainDashboard/orders/')}}" + "/" + id,
                                    type: 'DELETE',
                                    data: {'_token': "{{csrf_token()}}"},
                                    success: function (data) {
                                        jQuery('.alert-danger').html('');
                                        jQuery('.alert-danger').show();
                                        if (data != 'false') {
                                            jQuery('.alert-danger').append('<p>' + data + '</p>');
                                            setTimeout(function () {
                                                location.reload();
                                            }, 2000);
                                        } else {
                                            jQuery('.alert-danger').append('<p>Something Error</p>');
                                        }
                                        window.scrollTo({top: 0, behavior: 'smooth'});
                                    },
                                    error: function (data) {
                                        console.log('Error:', data);
                                    }
                                });
                            }
                        },
                        cancel: {
                            text: 'Cancel',
                            action: function () {
                            }
                        }
                    }
                });
            });

            $('body').on('click', '#packup', function (e) {

                let ids = [];
                $(".selectpack:checked").each(function () {
                    ids.push($(this).val());
                });

                let collection_id = $('.selectpackup  input:checked').attr('data-value');

                let order_ids = $('.selectpackup  input:checked').val();

                $.confirm({
                    title: '',
                    theme: 'modern',
                    class: 'warning',
                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-simle-o  text-warning"></i></span><span class="jconfirm-title">Confirm Pick Up</span></div><div style="font-weight:bold;"><p>Are You sure To Pick Up This Orders ?</p></div>',
                    buttons: {
                        confirm: {
                            text: 'Pickup Order',
                            btnClass: 'btn-warning',
                            action: function () {
                                $.ajax({
                                    url: "{{ url('/captainDashboard/pickup_orders/')}}",
                                    type: 'POST',
                                    data: {'_token': "{{csrf_token()}}", 'ids': ids, 'collection_id': collection_id},
                                    success: function (data) {
                                        if (data['status'] != 'false') {
                                            $(".alert").hide();

                                            $('#orderpickup').val(data['orders']);
                                            $('#from-pick').val(data['frompick']);
                                            $('#delivery_price-field').val(data['cost']);
                                            $('#bonous-field').val(data['bonous']);

                                            $('#pickupModal').modal('show');

                                        } else {
                                            $('#errorId').html(data['message']);
                                            $(".alert").show();

                                            $('#pickupModal').modal('hide');

                                        }
                                        window.scrollTo({top: 0, behavior: 'smooth'});
                                    },
                                    error: function (data) {
                                        console.log('Error:', data);
                                    }
                                });
                            }
                        },
                        cancel: {
                            text: 'Cancel',
                            action: function () {
                            }
                        }
                    }
                });
            })

        });

        $('body').on('click', '#pdf-submit', function (e) {

            let ids = [];

            $(".selectpdf input:checked").each(function () {
                ids.push($(this).val());

            });
            if (ids.length > 0) {
                $('<form action="{{ url("/captainDashboard/scan_order_print/")}}" method="post" target="_blank"><input value="' + ids + ' " name="ids"  />" {{csrf_field()}}"</form>').appendTo('body').submit();

            } else {

                $.confirm({
                    title: '',
                    theme: 'modern',
                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Print PDF</span></div><div style="font-weight:bold;"><p>{{__("captain.Must_Choose_Orders_To_Print")}}</p></div>',
                    type: 'red',
                });
            }

        });

        $('body').on('change', '.warehouse_dropoff', function (e) {
            if ($(this).is(':checked')) {
                var self = $(this).attr('data-id');
                $(this).bootstrapToggle('disable');
                $("#client_dropoff_" + self).bootstrapToggle('enable');
                $("#selectrecallClient_" + self).prop('disabled', false);
                $("#selectMoved_" + self).prop('disabled', false);
                $.ajax({
                    url: "{{ url('/captainDashboard/orders/warehouse_dropoff/')}}",
                    type: 'POST',
                    data: {'_token': "{{csrf_token()}}", ids: [self]},
                    success: function (data) {

                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        });

        $('body').on('change', '.head_office', function (e) {
            var self = $(this).attr('data-id');
            var is_checked = $(this).is(':checked');
            $.ajax({
                url: "{{ url('/captainDashboard/orders/head_office/')}}",
                type: 'POST',
                data: {'_token': "{{csrf_token()}}", ids: [self], head_office: is_checked},
                success: function (data) {

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

        });

        $('body').on('change', '.client_dropoff', function (e) {
            if ($(this).is(':checked')) {
                var self = $(this).attr('data-id');
                $(this).bootstrapToggle('disable');
                $("#selectrecallClient_" + self).prop('disabled', true);
                $("#selectMoved_" + self).prop('disabled', true);
                $.ajax({
                    url: "{{ url('/captainDashboard/orders/client_dropoff/')}}",
                    type: 'POST',
                    data: {'_token': "{{csrf_token()}}", ids: [self]},
                    success: function (data) {

                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });

            }
        });

        $('body').on('click', '#dropAll', function (e) {
            $('input:checkbox.selectdrop').not("[disabled]").not(this).prop('checked', this.checked);
        });

        $('body').on('click', '#recallClientAll', function (e) {
            $('input:checkbox.selectrecallClient').not("[disabled]").not(this).prop('checked', this.checked).change();
        });

        $('body').on('change', '.selectrecallClient', function (e) {
            let customers = [];
            $(".selectrecallClient:checked").each(function () {
                customers.push($(this).attr('data-customer'));
            });

            const allEqual = arr => arr.every(v => v === arr[0]);

            if ($('input[name="selectrecallClient[]"]').is(':checked') && allEqual(customers)) {
                $("#send_client").removeClass('disabled');
            } else {
                $("#send_client").addClass('disabled');
            }
        });

        $('body').on('click', '#send_client', function (e) {

            let ids = [];
            let customer_id = '';
            $(".selectrecallClient:checked").each(function () {
                ids.push($(this).val());
                customer_id = $(this).attr('data-customer');
            });

            if (ids.length == 0) {
                return false;
            }

            $.confirm({
                title: '',
                theme: 'modern',
                class: 'warning',
                content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-simle-o  text-warning"></i></span><span class="jconfirm-title"> {{__("captain.confirm_send_client")}}</span></div><div style="font-weight:bold;"><p>{{__("captain.send_client_orders")}}</p></div>',
                buttons: {
                    confirm: {
                        text: '{{__("captain.send_client_text")}}',
                        btnClass: 'btn-warning',
                        action: function () {
                            $.ajax({
                                url: "{{ url('/captainDashboard/orders/send_to_client/')}}",
                                type: 'POST',
                                data: {'_token': "{{csrf_token()}}", 'ids': ids, 'customer_id': customer_id},
                                success: function (data) {
                                    $('<form action="{{ url("/captainDashboard/print_refund/")}}" method="post" target="_blank"><input value="' + data + ' " name="refund_collection_id"  />" {{csrf_field()}}"</form>').appendTo('body').submit();
                                    location.reload();
                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                            });
                        }
                    },
                    cancel: {
                        text: 'Cancel',
                        action: function () {
                        }
                    }
                }
            });
        });

        $('body').on('click', '#drop_off', function (e) {

            let ids = [];
            $(".selectdrop:checked").each(function () {
                ids.push($(this).val());
            });

            if (ids.length == 0) {
                return false;
            }

            $.confirm({
                title: '',
                theme: 'modern',
                class: 'warning',
                content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-simle-o  text-warning"></i></span><span class="jconfirm-title"> {{__("captain.confirm_drop_off")}}</span></div><div style="font-weight:bold;"><p>{{__("captain.drop_off_orders")}}</p></div>',
                buttons: {
                    confirm: {
                        text: '{{__("captain.drop_off_text")}}',
                        btnClass: 'btn-warning',
                        action: function () {
                            $.ajax({
                                url: "{{ url('/captainDashboard/orders/drop_orders/all/')}}",
                                type: 'POST',
                                data: {'_token': "{{csrf_token()}}", 'ids': ids},
                                success: function (data) {
                                    location.reload();
                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                            });
                        }
                    },
                    cancel: {
                        text: 'Cancel',
                        action: function () {
                        }
                    }
                }
            });
        });

        $('body').on('click', '#dropWarehouseAll', function (e) {
            $('input:checkbox.selectdropWarehouse').not("[disabled]").not(this).prop('checked', this.checked);
        });

        $('body').on('click', '#drop_warehouse_off', function (e) {

            let ids = [];
            $(".selectdropWarehouse:checked").each(function () {
                ids.push($(this).val());
            });

            if (ids.length == 0) {
                return false;
            }

            $.confirm({
                title: '',
                theme: 'modern',
                class: 'warning',
                content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-simle-o  text-warning"></i></span><span class="jconfirm-title"> {{__("captain.confirm_drop_off")}}</span></div><div style="font-weight:bold;"><p>{{__("captain.drop_off_orders")}}</p></div>',
                buttons: {
                    confirm: {
                        text: '{{__("captain.drop_off_text")}}',
                        btnClass: 'btn-warning',
                        action: function () {
                            $.ajax({
                                url: "{{ url('/captainDashboard/move_collections/drop_orders/')}}",
                                type: 'POST',
                                data: {
                                    '_token': "{{csrf_token()}}",
                                    'ids': ids,
                                    'collection_id': "{{app('request')->move_collection}}"
                                },
                                success: function (data) {
                                    location.reload();
                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                            });
                        }
                    },
                    cancel: {
                        text: 'Cancel',
                        action: function () {
                        }
                    }
                }
            });
        });

        $('body').on('click', '#moveAll', function (e) {
            $('input:checkbox.selectMoved').not("[disabled]").not(this).prop('checked', this.checked).trigger('change');
        });

        $('body').on('click', '#move_to', function (e) {
            let ids = [];
            $(".selectMoved:checked").each(function () {
                ids.push($(this).val());
            });

            $("#move_ids").val(ids);
        });

        $('body').on('change', '.selectMoved', function (e) {
            let warehouses = [];
            $(".selectMoved:checked").each(function () {
                warehouses.push($(this).attr('data-warehouse'));
            });

            const allEqual = arr => arr.every(v => v === arr[0]);

            if ($('input[name="selectMoved[]"]').is(':checked') && allEqual(warehouses)) {
                $("#move_to").removeClass('disabled');
            } else {
                $("#move_to").addClass('disabled');
            }
        });


        $('body').on('click', '.forward', function (e) {
            $("#forwardModal").modal("show");
        });

        $('body').on('click', '#forward_action', function (e) {

            let ids = [];

            $(".selectpdf input:checked").each(function () {
                ids.push($(this).val());

            });
            if (ids.length > 0) {
                $('<form action="{{ url("/captainDashboard/forward_orders/")}}" method="post"><input value="' + ids + ' " name="ids"  /><input value="' + $("#driver").val() + ' " name="driver_id"  />" {{csrf_field()}}"</form>').appendTo('body').submit();

            } else {

                $.confirm({
                    title: '',
                    theme: 'modern',
                    content: '<div class="jconfirm-title-c jconfirm-hand"><span class="jconfirm-icon-c"><i class="fa fa-frown-o  text-danger"></i></span><span class="jconfirm-title">Print PDF</span></div><div style="font-weight:bold;"><p>{{__("captain.Must_Choose_Orders")}}</p></div>',
                    type: 'red',
                });
            }

            $("#forwardModal").modal("hide");

        });

    </script>
@endsection

@extends('backend.layouts.app')

@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> Agent Order Views

        </h3>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            @if($order_views->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>ORDER ID</th>
                        	<th>STATUS</th>

                        	<th>AGENT</th>

                            <th class="text-right"></th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($order_views as $order_view)
                            <tr>
                                <td>{{$order_view->id}}</td>
                                <td><a target="_blank" href="{{URL::asset('/mngrAdmin/orders/'.$order_view->order_id)}}">{{$order_view->order_id}}</a></td>
                    			<td>{!! $order_view->status_span !!}</td>

                    			<td>{{! em/pty($order_view->agent )? $order_view->agent->name : '' }}</td>

                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('mngrAdmin.order_views.show', $order_view->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>

                                    <form action="{{ route('mngrAdmin.order_views.destroy', $order_view->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $order_views->render() !!}
            @else
                <h3 class="text-center alert alert-warning">No Result Found !</h3>
            @endif

        </div>
    </div>

@endsection

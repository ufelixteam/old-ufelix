@extends('backend.layouts.app')

@section('css')
    <title>{{__('backend.pickup_price_list')}}</title>
@endsection

@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.pickup_price_list')}}
        @if(permission('addPickupPriceList')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
            <a class="btn btn-success pull-right" href="{{ route('mngrAdmin.pickup_price_lists.create') }}"><i
                    class="glyphicon glyphicon-plus"></i> {{__('backend.add_pickup_price_list')}}</a>
            @endif
        </h3>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            @if($governoratePrices->count())
                <table class="table table-condensed table-striped text-center">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{__('backend.from')}}</th>
                        <th></th>
                        <th>{{__('backend.to')}}</th>
                        <th>{{__('backend.cost')}}</th>
                        <th>{{__('backend.the_bonous')}}</th>
                        <th>{{__('backend.status')}}</th>
                        <th class="pull-right"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($governoratePrices as $governoratePrice)
                        <tr>
                            <td>{{$governoratePrice->id}}</td>
                            <td>{{$governoratePrice->Start->name_en}}</td>
                            <td><i class="glyphicon glyphicon-arrow-right" style="color: blue;"></i></td>
                            <td>{{$governoratePrice->End->name_en}}</td>
                            <td>{{! empty($governoratePrice->cost) ? $governoratePrice->cost : 'Not Specified'}}</td>
                            <td>{{! empty($governoratePrice->bonous) ? $governoratePrice->bonous : 'Not Specified'}}</td>
                            <td>{!! $governoratePrice->status_span !!}</td>
                            <td class="pull-right">
                            {{--<a class="btn btn-xs btn-primary" href="{{route('mngrAdmin.pickup_price_lists.show', $governoratePrice->id)}}"><i class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>--}}
                            @if(permission('editShipmentDestinations')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <a class="btn btn-xs btn-warning"
                                   href="{{ route('mngrAdmin.pickup_price_lists.edit', $governoratePrice->id)}}"><i
                                        class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                            @endif
                            @if(permission('deleteShipmentDestinations')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                <form
                                    action="{{ route('mngrAdmin.pickup_price_lists.destroy', $governoratePrice->id) }}"
                                    method="POST" style="display: inline;"
                                    onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="submit" class="btn btn-xs btn-danger"><i
                                            class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}</button>
                                </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                {!! $governoratePrices->appends($_GET)->links() !!}

            @else
                <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
            @endif
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $('#theTable').DataTable({
            "pagingType": "full_numbers"
        });
    </script>
@endsection

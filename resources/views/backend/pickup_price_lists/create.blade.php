@extends('backend.layouts.app')
@section('css')
<title>{{__('backend.pickup_price_list')}} - {{__('backend.create')}}</title>
@endsection
@section('header')
  <div class="page-header">
    <h3><i class="glyphicon glyphicon-plus"></i> {{__('backend.pickup_price_list')}} / {{__('backend.create')}} </h3>
  </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <form action="{{ route('mngrAdmin.pickup_price_lists.store') }}" method="POST" name='validateForm'>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group col-md-6 col-sm-6 @if($errors->has('start_station')) has-error @endif">
          <label for="start_station-field">{{__('backend.from')}}: </label>
          <select id="start_station-field" name="start_station" class="form-control" >
          @if(! empty($list_governorates) )
            <option disabled selected>{{__('backend.Choose_Start')}}: </option>
            @foreach($list_governorates as $governorate)
              <option value="{{$governorate->id}}">{{$governorate->name_en}}</option>
            @endforeach
          @endif
          </select>
            @if($errors->has("start_station"))
              <span class="help-block">{{ $errors->first("start_station") }}</span>
            @endif
        </div>
        <div class="form-group col-md-6 col-sm-6 @if($errors->has('access_station')) has-error @endif">
          <label for="access_station-field">{{__('backend.to')}}: </label>
          <select id="access_station-field" name="access_station" class="form-control" >
          @if(! empty($list_governorates) )
          <option disabled selected>{{__('backend.Choose_End')}}: </option>
            @foreach($list_governorates as $governorate)
              <option value="{{$governorate->id}}">{{$governorate->name_en}}</option>
            @endforeach
          @endif
          </select>
            @if($errors->has("access_station"))
              <span class="help-block">{{ $errors->first("access_station") }}</span>
            @endif
        </div>
        <div class="form-group col-md-3 col-sm-3 @if($errors->has('cost')) has-error @endif">
          <label for="cost-field">{{__('backend.cost')}}: </label>
          <input type="text" id="cost-field" name="cost" class="form-control" value="{{ old("cost") }}"/>
          @if($errors->has("cost"))
            <span class="help-block">{{ $errors->first("cost") }}</span>
          @endif
        </div>

         <div class="form-group col-md-3 col-sm-3 @if($errors->has('bonous')) has-error @endif">
          <label for="bonous-field">{{__('backend.the_bonous')}}: </label>
          <input type="text" id="bonous-field" name="bonous" class="form-control" value="{{ old("bonous") }}"/>
          @if($errors->has("bonous"))
            <span class="help-block">{{ $errors->first("bonous") }}</span>
          @endif
        </div>
        <div class="form-group col-md-6 col-sm-6 @if($errors->has('status')) has-error @endif">
          <label for="status-field" style="display: block;">{{__('backend.status')}}: </label>
          <select id="status-field" name="status" class="form-control" value="{{ old("status") }}" >
            <option value="0" {{ old("status") == "0" ? 'selected' : '' }}>{{__('backend.no')}}</option>
            <option value="1" {{ old("status") == "1" ? 'selected' : '' }}>{{__('backend.yes')}}</option>
          </select>
          @if($errors->has("status"))
            <span class="help-block">{{ $errors->first("status") }}</span>
          @endif
        </div>
        <div class="well well-sm col-md-12 col-sm-12 col-xs-12">
          <button type="submit" class="btn btn-primary">{{__('backend.add_destination')}}</button>
          <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.pickup_price_lists.index') }}"><i class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
        </div>
      </form>
    </div>
  </div>
@endsection
@section('scripts')
<script type="text/javascript">
  $(function() {
    $("form[name='validateForm']").validate({
      // Specify validation rules
      rules: {
        start_station: {
          required: true,
        },
        access_station: {
          required: true,
        },
        cost: {
          required: true,
          number: true
        },
      },
      // Specify validation error messages
      messages: {
        start_station: {
          required: "{{__('backend.Please_Choose_Start_Destination')}}",
        },
        access_station: {
          required: "{{__('backend.Please_Choose_End_Destination')}}",
        },
        cost: {
          required: "{{__('backend.Please_Enter_The_Destination_Cost')}}",
          number: "{{__('backend.Please_Enter_Avalid_Number')}}",
        },
      },

      errorPlacement: function(error, element) {
          $(element).parents('.form-group').append(error)
      },
      highlight: function (element) {
          $(element).parent().addClass('error')
      },
      unhighlight: function (element) {
          $(element).parent().removeClass('error')
      },
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function(form) {
          form.submit();
      }
    });
  })
</script>
@endsection

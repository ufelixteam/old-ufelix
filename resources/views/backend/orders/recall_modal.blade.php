<div class="modal fade" id="recallModal" tabindex="-1" role="dialog" aria-labelledby="recallModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="forward_agentMo">{{__('backend.Recall_order')}}</h5>
            </div>
            <div class="modal-body ">
                <form class="" action="{{ url('mngrAdmin/orders/recallOrder') }}" method="post" id="recallForm">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="order_id" id="order_id" value="{{ $order->id }}">

                    <div class="form-group">
                        <label>Recall By </label>
                        <select name="recall_by" class="form-control" id="recall_by">
{{--                            <option value="1">Admin</option>--}}
                            <option value="2">Receiver</option>
                            <option value="3">Sender</option>
                        </select>
                    </div>
                    {{--                    <div class="form-group" id="send_div" hidden>--}}
                    {{--                        <button type="button" id="send_sms"--}}
                    {{--                                class="btn btn-primary btn-block">{{__('backend.send_sms')}}</button>--}}
                    {{--                        <div class="alert-danger danger">--}}

                    {{--                        </div>--}}
                    {{--                        <div class="alert-success success">--}}

                    {{--                        </div>--}}
                    {{--                    </div>--}}

                    {{--                    <div class="form-group" id="password_div">--}}
                    {{--                        <label>Admin password </label>--}}
                    {{--                        <input name="password" class="form-control" type="password" required id="password"/>--}}
                    {{--                    </div>--}}
                    {{--                    <div class="form-group" id="code_div" hidden>--}}
                    {{--                        <label> Code </label>--}}
                    {{--                        <input name="code" class="form-control" type="text" required id="code"/>--}}
                    {{--                    </div>--}}

                    <div class="modal-footer">
                        <button type="button" id="send_recall" class="btn btn-primary">{{__('backend.recall')}}</button>
                        <button type="button" class="btn btn-danger"
                                data-dismiss="modal">{{__('backend.close')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

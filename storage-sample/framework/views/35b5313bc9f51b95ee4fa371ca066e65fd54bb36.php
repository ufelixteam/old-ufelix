<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        <?php if($collection_orders->count()): ?>
            <table class="table table-condensed table-striped text-center">
                <thead>
                <tr>
                    <th>#</th>
                    <th><?php echo e(__('backend.customer')); ?></th>
                    <th><?php echo e(__('backend.corporate')); ?></th>
                    <th><?php echo e(__('backend.Create_at')); ?></th>
                    <th><?php echo e(__('backend.File_Name')); ?></th>

                    <?php if(! empty($type) && $type ==1 ): ?>
                        <th><?php echo e(__('backend.saved')); ?></th>
                        <th><?php echo e(__('backend.Saved_At')); ?></th>
                    <?php else: ?>

                        <th><?php echo e(__('backend.file')); ?></th>
                    <?php endif; ?>

                    <th></th>
                </tr>
                </thead>
                <tbody>

                <?php $__currentLoopData = $collection_orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $collection): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($collection->id); ?></td>
                        <td><?php echo e(! empty($collection->customer) ? $collection->customer->name : ''); ?></td>
                        <td><?php echo e(isset($collection->customer->Corporate) ? $collection->customer->Corporate->name : '-'); ?></td>
                        <td><?php echo e($collection->created_at); ?></td>
                        <td><?php echo e($collection->file_name); ?></td>

                        <?php if(! empty($type) && $type ==1 ): ?>
                            <td><?php echo $collection->saved_span; ?></td>
                            <td><?php echo e($collection->saved_at); ?></td>

                        <?php endif; ?>

                        <?php if(! empty($type) && $type == 1  ): ?>
                            <td>
                                <?php if($collection->is_saved != 1): ?>
                                    <a class="btn btn-xs btn-default "
                                       href="<?php echo e(URL::asset('mngrAdmin/orders/create?collection='.$collection->id)); ?>">
                                        <?php echo e(__('backend.edit')); ?>

                                    </a>

                                    <form
                                        action="<?php echo e(URL::asset('/mngrAdmin/collection_orders/'. $collection->id.'?type=1')); ?>"
                                        method="POST" style="display: inline;"
                                        onsubmit="if(confirm('<?php echo e(__('backend.msg_confirm_delete')); ?>')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                        <button type="submit" class="btn btn-xs btn-danger"><i
                                                class="glyphicon glyphicon-trash"></i> <?php echo e(__('backend.delete')); ?></button>
                                    </form>

                                <?php else: ?>
                                    <a class="btn btn-xs btn-primary "
                                       href="<?php echo e(URL::asset('mngrAdmin/orders?collection='.$collection->id)); ?>">
                                        <?php echo e(__('backend.Orders_List')); ?>

                                    </a>
                                <?php endif; ?>
                            </td>

                        <?php else: ?>
                            <td>
                                <a href="<?php echo e(asset('/api_uploades/client/corporate/sheets/'.$collection->file_name)); ?>">
                                    <?php if($collection->status == 0): ?>
                                        <form class=""
                                              action="<?php echo e(route('mngrAdmin.collection_orders.downloadStatus')); ?>"
                                              method="post">
                                            <input id="_token" type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                            <input id="id" type="hidden" name="id" value="<?php echo e($collection->id); ?>">
                                            <input id="status" type="hidden" name="status" value="1">
                                            <input id="file_name" type="hidden" name="file_name"
                                                   value="<?php echo e($collection->file_name); ?>">
                                            <button id="downloadStatus" type="submit" class="btn btn-primary"
                                                    name="button"><?php echo e(__('backend.Download_File')); ?></button>
                                        </form>
                                    <?php else: ?>
                                        <button type="submit" class="btn btn-warning"
                                                name="button"><?php echo e(__('backend.Download_File')); ?></button>
                                    <?php endif; ?>
                                </a>
                            </td>


                            <td class="text-right pull-right">
                                <a class="btn btn-xs btn-warning"
                                   href="<?php echo e(route('mngrAdmin.collection_orders.edit', $collection->id)); ?>"><i
                                        class="glyphicon glyphicon-edit"></i> <?php echo e(__('backend.edit')); ?></a>
                                <form
                                    action="<?php echo e(URL::asset('/mngrAdmin/collection_orders/'. $collection->id.'?type=0')); ?>"
                                    method="POST" style="display: inline;"
                                    onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                    <button type="submit" class="btn btn-xs btn-danger"><i
                                            class="glyphicon glyphicon-trash"></i> <?php echo e(__('backend.delete')); ?></button>
                                </form>
                            </td>

                        <?php endif; ?>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
            <?php echo $collection_orders->appends($_GET)->links(); ?>

        <?php else: ?>
            <h3 class="text-center alert alert-warning">No Result Found !</h3>
        <?php endif; ?>
    </div>
</div>
<?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/collection_orders/table.blade.php ENDPATH**/ ?>
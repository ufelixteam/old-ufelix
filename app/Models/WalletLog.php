<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalletLog extends Model
{
    protected $fillable = [
        'value', 'reason', 'type', 'wallet_id', 'invoice_id'
    ];

    public function getReasonAttribute()
    {
        if(app()->getLocale() == 'ar'){
            $value= $this->reason_ar;
        }else{
            $value= $this->reason_en;
        }
        return $value;
    }

    public function getTypeSpanAttribute()
    {
        if ($this->type == 1) {
            $value = "<span class='badge badge-pill label-info'>" . __('backend.withdraw') . "</span>";
        } else if ($this->type == 2) {
            $value = "<span class='badge badge-pill label-default'>" . __('backend.deposit') . "</span>";
        } else if ($this->type == 3) {
            $value = "<span class='badge badge-pill label-success'>" . __('backend.bonous') . "</span>";
        } else if ($this->type == 4) {
            $value = "<span class='badge badge-pill label-warning'>" . __('backend.subtract') . "</span>";
        } else {
            $value = "<span class='badge badge-pill label-info'></span>";
        }
        return $value;
    }
}


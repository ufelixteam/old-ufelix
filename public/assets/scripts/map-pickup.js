function myMap() {

    var mapCanvas1 = document.getElementById("map1");
    var myCenter1 = new google.maps.LatLng(30.0668886, 31.1962743);
    var mapOptions = {center: myCenter1, zoom: 14};

    var map1 = new google.maps.Map(mapCanvas1, mapOptions);

    var marker = new google.maps.Marker({
        position: myCenter1,
        draggable: true,
    });
    marker.setMap(map1);
    geocodePosition1(marker.getPosition());
    new google.maps.event.addListener(marker, 'dragend', function () {

        geocodePosition1(marker.getPosition());
        $("#receiver_latitude").val(this.getPosition().lat());
        $("#receiver_longitude").val(this.getPosition().lng());

    });

    function geocodePosition1(pos) {
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({
            latLng: pos

        }, function (responses) {
            if (responses && responses.length > 0) {
                $("#receiver_address").val(responses[0].formatted_address);
            } else {
            }
        });
    }

}

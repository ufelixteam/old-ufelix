@extends('backend.layouts.app')

@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> AgentInvoices
            <a class="btn btn-success pull-right" href="{{ route('mngrAdmin.agent_invoices.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </h3>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            @if($agent_invoices->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>FROM_DATE</th>
                        <th>TO_DATE</th>
                        <th>PROFIT</th>
                        <th>TOTAL_INCOME</th>
                        <th>STATUS</th>
                        <th>AGENT_ID</th>
                            <th class="text-right"></th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($agent_invoices as $agent_invoice)
                            <tr>
                                <td>{{$agent_invoice->id}}</td>
                                <td>{{$agent_invoice->from_date}}</td>
                    <td>{{$agent_invoice->to_date}}</td>
                    <td>{{$agent_invoice->profit}}</td>
                    <td>{{$agent_invoice->total_income}}</td>
                    <td>{{$agent_invoice->status}}</td>
                    <td>{{$agent_invoice->agent_id}}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('mngrAdmin.agent_invoices.show', $agent_invoice->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('mngrAdmin.agent_invoices.edit', $agent_invoice->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('mngrAdmin.agent_invoices.destroy', $agent_invoice->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $agent_invoices->render() !!}
            @else
                <h3 class="text-center alert alert-warning">No Result Found !</h3>
            @endif

        </div>
    </div>

@endsection
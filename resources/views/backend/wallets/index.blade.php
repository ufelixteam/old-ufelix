@extends('backend.layouts.app')

@section('css')
    <title>{{__('backend.the_wallets')}}</title>
@endsection

@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="fa fa-align-justify"></i> {{__('backend.wallets')}} {{ ! empty($header) ? $header : '' }}
        @if(permission('addWallet')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
            <a class="btn btn-success pull-right" href="{{ URL::asset('mngrAdmin/wallets/create?type='.$type) }}"><i
                    class="fa fa-plus"></i> {{__('backend.add_wallet')}}</a>
            @endif
        </h3>
    </div>
@endsection

@section('content')
    <div class="row" style="margin-bottom:15px;">
        <div class="col-md-12">
            <div class="group-control col-md-3 col-sm-3">
                <input type="hidden" name="type" value="{{request()->type}}" id="type">
                <select id="object_id" name="object_id" class="form-control">
                    <option value=""> {{__('backend.choose')}}</option>
                    @foreach($objects as $object)
                        <option value="{{$object->id}}">{{$object->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="list">
        @include('backend.wallets.table')
    </div>


@endsection

@section('scripts')
    <script type="text/javascript">

        $('#theTable').DataTable({
            "pagingType": "full_numbers"
        });

        $("#object_id").on('change', function () {
            $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
            $.ajax({
                url: '{{URL::asset("/mngrAdmin/wallets")}}' + '?type=' + $("#type").val() + '&object_id=' + $("#object_id").val(),
                type: 'get',
                success: function (data) {
                    $('.list').html(data.view);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });
    </script>
@endsection

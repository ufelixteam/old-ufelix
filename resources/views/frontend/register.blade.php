@extends('frontend.layouts.app')
@section('title') @lang('website.Register') @endsection
@section('content')
    <!-- Start App1 -->
    <section class=""><!-- req body-in -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-5  col-xs-12">

                    <img src="{{asset('assets/images/mnm.jpg')}}" class="uploadImg">

                </div>
                <div class="col-md-7  col-xs-12">
                    <div class="req-x">
                        <h3> @lang('website.Register')</h3>
                        <form action="{{url('/register')}}" method="post" name="registerForm" id="registerForm">
                            {{csrf_field()}}
                            <div
                                class="form-group col-md-6 col-xs-12 @if($errors->has('company_name')) has-error @endif">
                                <input type="text" id="company_name" name="company_name" value="{{old('company_name')}}"
                                       placeholder="@lang('website.Company')" class="form-control"/>
                                <i class="fa  fa-building"></i>
                                @if($errors->has("company_name"))
                                    <span class="help-block">{{ $errors->first("company_name") }}</span>
                                @endif
                            </div>
                            <div class="form-group col-md-6 col-xs-12 @if($errors->has('name')) has-error @endif">
                                <input type="text" id="name" name="name" value="{{old('name')}}"
                                       placeholder="@lang('website.Name')" class="form-control"/>
                                <i class="fa fa-user"></i>
                                @if($errors->has("name"))
                                    <span class="help-block">{{ $errors->first("name") }}</span>
                                @endif
                            </div>

                            <div class="form-group col-md-6 col-xs-12 @if($errors->has('email')) has-error @endif">
                                <input type="email" id="email" name="email" value="{{old('email')}}"
                                       placeholder="@lang('website.Email')" class="form-control"/>
                                <i class="fa fa-envelope"></i>
                                @if($errors->has("email"))
                                    <span class="help-block">{{ $errors->first("email") }}</span>
                                @endif
                            </div>
                            <div class="form-group col-md-6 col-xs-12 @if($errors->has('mobile')) has-error @endif">
                                <input type="text" id="mobile" name="mobile" value="{{old('mobile')}}"
                                       placeholder="@lang('website.Mobile')" class="form-control"/>
                                <i class="fa fa-phone"></i>
                                @if($errors->has("mobile"))
                                    <span class="help-block">{{ $errors->first("mobile") }}</span>
                                @endif
                            </div>
                            <div class="form-group col-md-6 col-xs-12 @if($errors->has('password')) has-error @endif">
                                <input type="password" id="password" name="password"
                                       placeholder="@lang('website.Password')" class="form-control"/>
                                <i class="fa fa-lock"></i>
                                @if($errors->has("password"))
                                    <span class="help-block">{{ $errors->first("password") }}</span>
                                @endif
                            </div>
                            <div
                                class="form-group col-md-6 col-xs-12 @if($errors->has('password_confirmation')) has-error @endif">
                                <input type="password" id="password_confirmation" name="password_confirmation"
                                       placeholder="@lang('website.ConfirmPassword')" class="form-control"/>
                                <i class="fa fa-lock"></i>
                                @if($errors->has("password_confirmation"))
                                    <span class="help-block">{{ $errors->first("password_confirmation") }}</span>
                                @endif
                            </div>
                            <div class="form-group col-md-12 col-xs-12">
                                <label class="contw">@lang('website.AcceptTerms')<a
                                        href="{{ app()->getLocale() == 'en' ? url('/en/terms') : url('/terms') }}">@lang('website.Terms') </a>
                                    <input type="radio" checked="checked" name="check" id="check">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="form-group col-md-12 col-xs-12">
                                {!! app('captcha')->display($attributes = [], $options = []) !!}
                                @if($errors->has("message"))
                                    <span class="help-block">{{ $errors->first("g-recaptcha-response") }}</span>
                                @endif
                            </div>
                            <div class="form-group col-md-12 col-xs-12">
                                <input type="submit" class="btn btn-style text-left" value="@lang('website.Register')"/>
                            </div>
                        </form>
                        <div class="row">
                            <div class="col-md-12 col-xs-12 my-account text-center">
                                @lang('website.AskLogin')

                                <a href="{{ app()->getLocale() == 'en' ? url('/en/login') : url('/login') }}"
                                   class="btn login-btn">@lang('website.Login')
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- End App1 -->
@endsection

@section('scripts')
    <script src="{{asset('/assets/scripts/jquery.validate.js')}}"></script>

    <script>

        $(function () {
            $("form[name='registerForm']").validate({
                rules: {
                    password: {
                        required: true,
                    },
                    password_confirmation: {
                        required: true,
                        equalTo: '#password',
                        minlength: 5,
                    },
                    company_name: {
                        required: true,
                    },
                    name: {
                        required: true,
                    },

                    email: {
                        required: true,
                        email: true,
                    },
                    mobile: {
                        required: true,
                        number: true,
                        rangelength: [11, 11],
                    }
                },
                // Specify validation error messages
                messages: {
                    password: {
                        required: "{{__('website.passwordLoginRequired')}}",
                    },
                    company_name: {
                        required: "{{__('website.companyRequired')}}",
                    },
                    name: {
                        required: "{{__('website.ContactNameRequired')}}",
                    },
                    password_confirmation: {
                        required: "{{__('backend.Confirm_password_is_wrong')}}",
                        equalTo: "{{__('backend.Confirm_password_is_wrong')}}",
                    },
                    mobile: {
                        required: "{{__('website.ContactMobileRequired')}}",
                        number: "{{__('website.Please_Enter_Avalid_Number')}}",
                        rangelength: "{{__('website.Mobile_Must_Be11_Digits')}}",
                    },

                    email: {
                        required: "{{__('backend.Please_Enter_Customer_E-mail')}}",
                        email: "{{__('backend.Please_Enter_Customer_Correct_E-mail')}}"
                    },


                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                    $(element).parent().addClass('error');
                    $(element).parent().find('label').addClass(' help-block');


                },
                highlight: function (element) {
                    $(element).parent().addClass('error');
                    $(element).parent().find('label').addClass(' help-block');

                },
                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                    $(element).parent().find('label').removeClass(' help-block');

                },

                submitHandler: function (form) {
                    form.submit();
                }
            });
        })
    </script>
@endsection

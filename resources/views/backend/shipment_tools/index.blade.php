@extends('backend.layouts.app')
@section('css')
  <title>{{__('backend.Shipment_Tools')}}</title>
@endsection
@section('header')
  <div class="page-header clearfix">
    <h3>
      <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.Shipment_Tools')}}
      @if(permission('addShipmentTools')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
        <a class="btn btn-success pull-right" href="{{ route('mngrAdmin.shipment_tools.create') }}"><i class="glyphicon glyphicon-plus"></i> {{__('backend.add_shipment_tool')}}</a>
      @endif
    </h3>
  </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
      @if($shipment_tools->count())
        <table class="table table-condensed table-striped text-center">
          <thead>
            <tr>
              <th># {{__('backend.no')}}.</th>
              <th>{{__('backend.title')}}</th>
              <th>{{__('backend.Item_Price')}}</th>
              <th>{{__('backend.status')}}</th>
              <th class="text-right"></th>
            </tr>
          </thead>
          <tbody>
            @foreach($shipment_tools as $index => $shipment_tool)
              <tr>
                <td>{{$index+1}}</td>
                <td>
                  @if(session()->has('lang') == 'ar')
                    {{$shipment_tool->name_ar}}
                  @else
                  {{$shipment_tool->name_en}}
                  @endif
                </td>
                <td>{{ $shipment_tool->price }}</td>
                <td>{!! $shipment_tool->status_span !!}</td>
                <td class="pull-right">
                  @if(permission('showShipmentTools')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                    <a class="btn btn-xs btn-primary" href="{{ route('mngrAdmin.shipment_tools.show', $shipment_tool->id) }}"><i class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>
                  @endif
                  @if(permission('editShipmentTools')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                    <a class="btn btn-xs btn-warning" href="{{ route('mngrAdmin.shipment_tools.edit', $shipment_tool->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                  @endif
                  @if(permission('deleteShipmentTools')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                    <form action="{{ route('mngrAdmin.shipment_tools.destroy', $shipment_tool->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}</button>
                    </form>
                  @endif
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
        {!! $shipment_tools->render() !!}
      @else
          <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
      @endif
    </div>
  </div>
@endsection

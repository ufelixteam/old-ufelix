<!DOCTYPE html>
<html lang="en">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width,initial-scale=1">
      <meta name="author" content="Ufelix Team">
      <link rel="shortcut icon" type="image/x-icon" href="{{asset('/website/images/flogo.png')}}" />

      <title>@yield('title')</title>
      @include('frontend.profile.layouts.css')
      @yield('css')

    </head>
    <body>
      @include('frontend.profile.layouts.header')
      @include('frontend.profile.layouts.sidemenu')

    <!-- Start DashBoard -->
    <div class="dashboard-wrapper dashboard-wrapper-lg">
    	<div class="container-fluid">
    			@yield('content')
      </div>
    </div>
    <!-- End DashBoard -->
		
    @include('frontend.profile.layouts.js')

    @yield('scripts')
	
</body>
</html>

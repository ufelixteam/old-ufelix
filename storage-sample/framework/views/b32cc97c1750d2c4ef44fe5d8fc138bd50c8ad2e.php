

<?php $__env->startSection('css'); ?>
  <title><?php echo e(__('backend.drivers')); ?></title>
  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <style>
        .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20px; }
        .toggle.ios .toggle-handle { border-radius: 20px; }
    
    @media  only screen and (max-width: 580px) {
        .hide-td{
            display: none;
        }
    }
    .box-body {
        min-height: 100px;
    }
    .stylish-input-group .input-group-addon{
        background: white !important;
    }
    .stylish-input-group .form-control{
        border-right:0;
        box-shadow:0 0 0;
        border-color:#ccc;
    }
    .stylish-input-group button{
        border:0;
        background:transparent;
    }
  </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
<div class="page-header clearfix">
  <h3>
    <i class="glyphicon glyphicon-align-justify"></i> <?php echo e(__('backend.drivers')); ?>

    <div class="btn-group pull-right" role="group" aria-label="...">
        <?php if(permission('addDriver')): ?> <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
          <a class="btn btn-success btn-group" role="group"  href="<?php echo e(route('mngrAdmin.drivers.create')); ?>"><i class="glyphicon glyphicon-plus"></i> <?php echo e(__('backend.add_driver')); ?></a>
        <?php endif; ?>
    </div>
  </h3>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="row" style="margin-bottom:15px;">
  <div class="col-md-12">
    <div class="row">
      <div class="group-control col-md-4 col-sm-4">
        <form action="<?php echo e(URL::asset('/mngrAdmin/drivers')); ?>" method="get" id="search-form" />
          <div id="imaginary_container">
            <div class="input-group stylish-input-group">
            <input type="text" class="form-control" id="search-field"  name="search" placeholder="<?php echo e(__('backend.search_by_name_or_mobile')); ?>" >
            <span class="input-group-addon">
              <button type="button" id="search-btn1">
                <span class="glyphicon glyphicon-search"></span>
              </button>
            </span>
            </div>
          </div>
        </form>
      </div>
      <div class="group-control col-md-2 col-sm-2">
        <select id="block-field" name="block" class="form-control">
          <option value="-1"><?php echo e(__('backend.sort_by_blocked')); ?> </option>
          <option value="0"><?php echo e(__('backend.not_blocked')); ?></option>
          <option value="1"><?php echo e(__('backend.blocked')); ?></option>
        </select>
      </div>
      <div class="group-control col-md-2 col-sm-2">
        <select id="active-field" name="active" class="form-control">
          <option value="-1"><?php echo e(__('backend.sort_by_active')); ?> </option>
          <option value="0"><?php echo e(__('backend.not_activated')); ?></option>
          <option value="1"><?php echo e(__('backend.activated')); ?></option>
        </select>
      </div>
     

      <div class="group-control col-md-2 col-sm-2">
        <select id="request_status-field" name="request_status" class="form-control">
          <option value="-1"><?php echo e(__('backend.accept_request')); ?> </option>
          <option value="1"><?php echo e(__('backend.online')); ?></option>
          <option value="0"><?php echo e(__('backend.Offline')); ?></option>
        </select>
      </div>

      <div class="group-control col-md-2 col-sm-2">
        <select id="status-field" name="status" class="form-control">
          <option value="-1"><?php echo e(__('backend.sort_by_status')); ?> </option>
          <option value="1"><?php echo e(__('backend.online')); ?></option>
          <option value="0"><?php echo e(__('backend.Offline')); ?></option>
          <option value="2"><?php echo e(__('backend.busy')); ?></option>
        </select>
      </div>

    </div>
    <br>
    <div class="row">
      
      
      <div class="group-control col-md-3 col-sm-3">
        <select id="verify_mobile-field" name="verify_mobile" class="form-control">
          <option value="-1"><?php echo e(__('backend.verfiy_mobile')); ?> </option>
          <option value="0"><?php echo e(__('backend.no')); ?></option>
          <option value="1"><?php echo e(__('backend.yes')); ?></option>
        </select>
      </div>
      <div class="group-control col-md-3 col-sm-3">
        <select id="verify_email-field" name="verify_email" class="form-control">
          <option value="-1"><?php echo e(__('backend.verify_email')); ?> </option>
          <option value="0"><?php echo e(__('backend.no')); ?></option>
          <option value="1"><?php echo e(__('backend.yes')); ?></option>
        </select>
      </div>

       <div class="group-control col-md-3 col-sm-3">
        <select id="agent-field" name="agent" class="form-control">
          <option value="-1"><?php echo e(__('backend.sort_by_agent')); ?> </option>
          <option value="1"><?php echo e(__('backend.ufelix')); ?></option>
          <option value="0"><?php echo e(__('backend.not_ufelix')); ?></option>
        </select>
      </div>
      
    </div>
  </div>
</div>

<div class= "list">
  <?php echo $__env->make('backend.drivers.table', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script>
  $('.toggle').bootstrapToggle();


// $('#driversTable').DataTable({
//   "pagingType": "full_numbers"
// });

$("#active-field").on('change', function() {
	$('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
    $.ajax({
      url: '<?php echo e(URL::asset("/mngrAdmin/drivers")); ?>'+'?active='+$("#active-field").val(),
      type: 'get',
      success: function(data) {
      	$('.list').html('');
        $('.list').html(data.view);
        $('#driversTable').DataTable({
          "pagingType": "full_numbers"
        });
      },
      error: function(data) {
          console.log('Error:', data);
      }
  });
});

$("#block-field").on('change', function() {
	$('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
    $.ajax({
      url: '<?php echo e(URL::asset("/mngrAdmin/drivers")); ?>'+'?block='+$("#block-field").val(),
      type: 'get',
      success: function(data) {
          $('.list').html(data.view);
          $('#driversTable').DataTable({
            "pagingType": "full_numbers"
          });
      },
      error: function(data) {
          console.log('Error:', data);
      }
  });
});

$("#status-field").on('change', function() {
	$('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
    $.ajax({
      url: '<?php echo e(URL::asset("/mngrAdmin/drivers")); ?>'+'?status='+$("#status-field").val(),
      type: 'get',
      success: function(data) {
          $('.list').html(data.view);
          $('#driversTable').DataTable({
            "pagingType": "full_numbers"
          });
      },
      error: function(data) {
          console.log('Error:', data);
      }
  });
});

$("#verify_mobile-field").on('change', function() {
  $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
    $.ajax({
      url: '<?php echo e(URL::asset("/mngrAdmin/drivers")); ?>'+'?verify_mobile='+$("#verify_mobile-field").val(),
      type: 'get',
      success: function(data) {
          $('.list').html(data.view);
          $('#driversTable').DataTable({
            "pagingType": "full_numbers"
          });
      },
      error: function(data) {
          console.log('Error:', data);
      }
  });
});

$("#verify_email-field").on('change', function() {
  $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
    $.ajax({
      url: '<?php echo e(URL::asset("/mngrAdmin/drivers")); ?>'+'?verify_email='+$("#verify_email-field").val(),
      type: 'get',
      success: function(data) {
          $('.list').html(data.view);
          $('#driversTable').DataTable({
            "pagingType": "full_numbers"
          });
      },
      error: function(data) {
          console.log('Error:', data);
      }
  });
});

//request

$("#request_status-field").on('change', function() {
  $('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
    $.ajax({
      url: '<?php echo e(URL::asset("/mngrAdmin/drivers")); ?>'+'?request_status='+$("#request_status-field").val(),
      type: 'get',
      success: function(data) {
          $('.list').html(data.view);
          $('#driversTable').DataTable({
            "pagingType": "full_numbers"
          });
      },
      error: function(data) {
          console.log('Error:', data);
      }
  });
});


$("#agent-field").on('change', function() {
	$('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
    $.ajax({
      url: '<?php echo e(URL::asset("/mngrAdmin/drivers")); ?>'+'?agent='+$("#agent-field").val(),
      type: 'get',
      success: function(data) {
          $('.list').html(data.view);
          $('#driversTable').DataTable({
            "pagingType": "full_numbers"
          });
      },
      error: function(data) {
          console.log('Error:', data);
      }
  });
});

$("#search-btn1").on('click', function() {
	$('.list').html('<div class= "text-center" style="color:#5673ea;font-size: 54px;"><h1 id="tempo" class="fa fa-refresh fa-spin text-center"></h1></div>');
	$.ajax({
    url: '<?php echo e(URL::asset("/mngrAdmin/drivers")); ?>'+'?active='+$("#status-field").val()+'&&block='+$("#block-field").val()+'&&search='+$("#search-field").val(),
    type: 'get',
    data: $("#search-form").serialize(),
    success: function(data) {
        $('.list').html(data.view);
        $('#driversTable').DataTable({
          "pagingType": "full_numbers"
        });
    },
    error: function(data) {
        console.log('Error:', data);
    }
 	});
});


$('.verify_mobile').on('change',function(){
    let id = $(this).attr('data-id');
    $.ajax({
        url: '<?php echo e(URL::asset("/mngrAdmin/verify-mobile-driver")); ?>',
        type: 'post',
        data:{id:id,_token:"<?php echo e(csrf_token()); ?>"},
        success: function(data) {

        },
        error: function(data) {
            console.log('Error:', data);
        }
    });
})


$('.change_verify').on('change',function(){
    let id = $(this).attr('data-id');
    $.ajax({
        url: '<?php echo e(URL::asset("/mngrAdmin/change-verify-driver")); ?>',
        type: 'post',
        data:{id:id,_token:"<?php echo e(csrf_token()); ?>"},
        success: function(data) {

        },
        error: function(data) {
            console.log('Error:', data);
        }
    });
});


$('.verify_email').on('change',function(){
    let id = $(this).attr('data-id');
    $.ajax({
        url: '<?php echo e(URL::asset("/mngrAdmin/verify-email-driver")); ?>',
        type: 'post',
        data:{id:id,_token:"<?php echo e(csrf_token()); ?>"},
        success: function(data) {

        },
        error: function(data) {
            console.log('Error:', data);
        }
    });
})

$('.change_block').on('change',function(){
    let id = $(this).attr('data-id');
    $.ajax({
        url: '<?php echo e(URL::asset("/mngrAdmin/change-block-driver")); ?>',
        type: 'post',
        data:{id:id,_token:"<?php echo e(csrf_token()); ?>"},
        success: function(data) {

        },
        error: function(data) {
            console.log('Error:', data);
        }
    });
})

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/drivers/index.blade.php ENDPATH**/ ?>
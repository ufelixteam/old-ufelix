@extends('backend.layouts.app')
@section('css')
  <title>V. Model - Add New</title>
@endsection
@section('header')
<div class="page-header">
        <h3>Show #{{$model_type->id}}</h3> {!! $model_type->status_span !!}
        <form action="{{ route('mngrAdmin.model_types.destroy', $model_type->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('mngrAdmin.model_types.edit', $model_type->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <div class="form-group">
                <label for="nome">#</label>
                <p class="form-control-static">{{$model_type->id}}</p>
            </div>
            <div class="form-group col-sm-6">
                     <label for="name">NAME</label>
                     <p class="form-control-static">{{$model_type->name}}</p>
                </div>


            <div class="col-md-12 col-xs-12 col-sm-12">
                <a class="btn btn-link" href="{{ route('mngrAdmin.model_types.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
            </div>

        </div>
    </div>

@endsection

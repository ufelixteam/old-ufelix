@extends('frontend.profile.layouts.app')

@section('title') @lang('website.Profile') @endsection

@section('content')

    <?php
    $user = Auth::guard('Customer')->user();
    ?>


    <div class="top-bar clearfix">
        <div class="row gutter">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="page-title">
                    <h3><?php echo app('translator')->getFromJson('website.Profile'); ?></h3>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <ul class="right-stats" id="mini-nav-right">
                    <!--        <li>
                               <a href="javascript:void(0)" class="btn btn-danger">
                                   <span>76</span>Sales
                               </a>
                           </li>
                           <li>
                               <a href="" class="btn btn-info">
                                   <span>18</span>Tasks
                               </a>
                           </li> -->
                    <li>
                        <a href="{{url('/customer/account/reports/daily_report') . '?customer_id=' . Auth::guard('Customer')->user()->id}}" class="btn btn-success">
                            <i class="icon-download6"></i> Export
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row gutter">

        <!-- Chart Area -->
        <!-- Chart script added in custom.js file-->
        <div class="col-md-8 graph">
            <h4>Orders Report</h4>
            <div class="app"></div>
        </div>

        <div class="row col-md-4 mt-28 graph">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="panel dashboard-delivery">
                    <div class="website-performance">
                        <div class="row gutter">
                            <div class="col-lg-7 col-md-6 col-sm-6 col-xs-6">
                                <img src="{{asset('assets/images/wallet.png')}}" width="30" height="30">
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="performance-stats">
                                    <h3 id="expensesOdometer" class="odometer">{{$wallet ? $wallet->amount : 0}}</h3>
                                    <h5>My Wallet</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="panel dashboard-cancelled">
                    <div class="website-performance">
                        <div class="row gutter">
                            <div class="col-lg-7 col-md-6 col-sm-6 col-xs-6">
                                <img src="{{asset('assets/images/waiting.ico')}}" width="30" height="30">
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="performance-stats">
                                    <h3 id="expensesOdometer" class="odometer">{{$dropped_orders}}</h3>
                                    <h5>Dropped orders</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="panel dashboard-cancelled">
                    <div class="website-performance">
                        <div class="row gutter">
                            <div class="col-lg-7 col-md-6 col-sm-6 col-xs-6">
                                <img src="{{asset('assets/images/cancelled-order.png')}}" width="30" height="30">
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="performance-stats">
                                    <h3 id="expensesOdometer" class="odometer">{{$cancelled_orders}}</h3>
                                    <h5>Cancelled orders</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="panel dashboard-recall">
                    <div class="website-performance">
                        <div class="row gutter">
                            <div class="col-lg-7 col-md-6 col-sm-6 col-xs-6">
                                <img src="{{asset('assets/images/deliverd.png')}}" width="30" height="30">
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="performance-stats">
                                    <h3 id="expensesOdometer" class="odometer">{{$recalled_orders}}</h3>
                                    <h5>Recall orders</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="panel dashboard-recall">
                    <div class="website-performance">
                        <div class="row gutter">
                            <div class="col-lg-7 col-md-6 col-sm-6 col-xs-6">
                                <img src="{{asset('assets/images/deliverd.png')}}" width="30" height="30">
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="performance-stats">
                                    <h3 id="expensesOdometer" class="odometer">{{$rejected_orders}}</h3>
                                    <h5>Reject orders</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="panel dashboard-sales">
                    <div class="website-performance">
                        <div class="row gutter">
                            <div class="col-lg-7 col-md-6 col-sm-6 col-xs-6">
                                <img src="{{asset('assets/images/Finance-Purchase-Order-icon.png')}}" width="50"
                                     height="30">
                                <!--      <div class="performance">
                                         <h5>Sales</h5>
                                         <div class="performance-graph">
                                             <div id="sales" class="chart-height5"></div>
                                         </div>
                                     </div> -->
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="performance-stats">
                                    <h3 id="salesOdometer" class="odometer">{{$received_orders}}</h3>
                                    <h5>O.F.D Orders</h5>
                                    <!-- <p>21.2%<i class="icon-triangle-up up"></i></p> -->
                                    <!-- <p>vs. 6.5% (prev.)</p> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="panel dashboard-recall">
                    <div class="website-performance">
                        <div class="row gutter">
                            <div class="col-lg-7 col-md-6 col-sm-6 col-xs-6">
                                <img src="{{asset('assets/images/deliverd.png')}}" width="30" height="30">
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="performance-stats">
                                    <h3 id="expensesOdometer" class="odometer">{{$refund_orders}}</h3>
                                    <h5>Refund orders</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="panel dashboard-deliverd">
                    <div class="website-performance">
                        <div class="row gutter">
                            <div class="col-lg-7 col-md-6 col-sm-6 col-xs-6">
                                <img src="{{asset('assets/images/deliverd.png')}}" width="30" height="30">
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="performance-stats">
                                    <h3 id="expensesOdometer" class="odometer">{{$delivered_orders}}</h3>
                                    <h5>Deliverd orders</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>
@endsection

@section('scripts')
    <script>
        var data = {
            labels: {!! json_encode($months) !!},
            datasets: [
                {
                    label: "Dropped Orders",
                    backgroundColor: '#3e5390',
                    borderColor: '#3e5390',
                    data: {!! json_encode($month_dropped) !!}
                },
                {
                    label: "Cancelled Orders",
                    backgroundColor: '#f56565',
                    borderColor: '#f56565',
                    data: {!! json_encode($month_cancelled) !!}
                },
                {
                    label: "Deliverd Orders",
                    backgroundColor: '#65f5bd',
                    borderColor: '#65f5bd',
                    data: {!! json_encode($month_delivered) !!}
                },

                {
                    label: "Recale",
                    backgroundColor: '#cc82ff',
                    borderColor: '#cc82ff',
                    data: {!! json_encode($month_recalled) !!}
                },

                {
                    label: "Reject",
                    backgroundColor: '#cc82ff',
                    borderColor: '#cc82ff',
                    data: {!! json_encode($month_rejected) !!}
                },

                {
                    label: "O.F.D Orders",
                    backgroundColor: '#ffd5dd',
                    borderColor: '#ffd5dd',
                    data: {!! json_encode($month_received) !!}
                },
                {
                    label: "Refund Orders",
                    backgroundColor: '#3e4e8b',
                    borderColor: '#3e4e8b',
                    data: {!! json_encode($month_refund) !!}
                }
            ]
        };

        //profile graph
        Chart.defaults.global.defaultFontFamily = "'Open Sans', sans-serif";

        class App extends React.Component {

            constructor(props) {
                super(props);
                this.options = {
                    type: 'line',
                    data: data,


                    options: {
                        scales: {
                            xAxes: [{
                                display: true
                            }],

                            yAxes: [{
                                type: "linear",
                                display: true,
                                position: "left"
                                // ticks: {
                                //   callback: function(value, index, values) {
                                //     return 'R$' + value;
                                //   }
                                // }
                            }]
                        },

                        legend: {
                            position: 'bottom'
                        },

                        responsive: true,
                        maintainAspectRatio: true,
                        aspectRatio: 1.4,
                        layout: {
                            padding: {
                                top: 16,
                                left: 16,
                                right: 16,
                                bottom: 32
                            }
                        }
                    }
                };


                this.toggleChart = this.toggleChart.bind(this);
            }

            toggleChart() {
                this.options.type = this.options.type === 'line' ? 'bar' : 'line';
                this.chart.destroy();
                this.chart = new Chart(this.ctx, this.options);
            }

            componentDidMount() {
                this.canvas = document.querySelector('canvas');
                this.ctx = this.canvas.getContext('2d');
                this.chart = new Chart(this.ctx, this.options);
            }

            render() {
                return (
                    React.createElement("div", null,
                        React.createElement("canvas", null),
                        React.createElement("div", {className: "text-center"},
                            React.createElement("button", {
                                className: "btn btn-primary",
                                onClick: this.toggleChart
                            }, "Toggle"))));


            }
        }


        ReactDOM.render(
            React.createElement(App, null),
            document.querySelector('.app'));

    </script>
    <script>
        window.odometerOptions = {
            auto: false, // Don't automatically initialize everything with class 'odometer'
            selector: '.odometer', // Change the selector used to automatically find things to be animated
            format: 'd', // Change how digit groups are formatted, and how many digits are shown after the decimal point
            duration: 3000, // Change how long the javascript expects the CSS animation to take
            theme: 'car', // Specify the theme (if you have more than one theme css file on the page)
            animation: 'count' // Count is a simpler animation method which just increments the value,
                               // use it when you're looking for something more subtle.
        };
    </script>
    <script src="{{ asset('/profile/js/odometer.min.js')}}"></script>

{{--    <script src="{{ asset('/profile/js/custom-odometer.js')}}"></script>--}}

@endsection



<?php $__env->startSection('css'); ?>
  <title> <?php echo e(__('backend.corporates')); ?> - <?php echo e(__('backend.edit')); ?></title>
  <style>
    .error_user{
      display: inline-block;
      color: rgb(255, 255, 255);
      background-color: #9c3737;
      width: 100%;
      padding: 5px;
      margin-top: 3px;
      border-radius: 2px;
      text-align: center;
      font-weight: 500;
    }
  </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i>  <?php echo e(__('backend.edit_corporate')); ?> - <?php echo e($corprate->name); ?></h3>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <form action="<?php echo e(route('mngrAdmin.corporates.update', $corprate->id)); ?>" method="POST" enctype="multipart/form-data" name="corporate_edit">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

                    <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('name')): ?> has-error <?php endif; ?>">
                       <label for="name-field"><?php echo e(__('backend.name')); ?></label>
                       <input type="text" id="name-field" name="name" class="form-control" value="<?php echo e(!empty($corprate->name)?$corprate->name: old('name')); ?>"/>
                       <?php if($errors->has("name")): ?>
                        <span class="help-block"><?php echo e($errors->first("name")); ?></span>
                       <?php endif; ?>
                    </div>
                    <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('email')): ?> has-error <?php endif; ?>">
                       <label for="email-field"><?php echo e(__('backend.email')); ?></label>
                       <input type="text" id="email-field" name="email" class="form-control" value="<?php echo e(!empty($corprate->email)?$corprate->email:old('email')); ?>"/>
                       <?php if($errors->has("email")): ?>
                        <span class="help-block"><?php echo e($errors->first("email")); ?></span>
                       <?php endif; ?>
                    </div>
                    <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('field')): ?> has-error <?php endif; ?>">
                      <label for="corporate-work-field"><?php echo e(__('backend.corporate_work_field')); ?>: </label>
                      <input type="text" id="corporate-work-field" name="field" class="form-control" value="<?php echo e(!empty($corprate->field)?$corprate->field:old('field')); ?>"/>
                      <?php if($errors->has("field")): ?>
                      <span class="help-block"><?php echo e($errors->first("field")); ?></span>
                      <?php endif; ?>
                    </div>
            
                    <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('mobile')): ?> has-error <?php endif; ?>">
                       <label for="mobile-field"><?php echo e(__('backend.mobile')); ?></label>
                       <input type="text" id="mobile-field" name="mobile" maxlength="11" class="form-control" value="<?php echo e(!empty($corprate->mobile)?$corprate->mobile:old('mobile')); ?>"/>
                       <?php if($errors->has("mobile")): ?>
                        <span class="help-block"><?php echo e($errors->first("mobile")); ?></span>
                       <?php endif; ?>
                    </div>
                    <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('phone')): ?> has-error <?php endif; ?>">
                       <label for="phone-field"><?php echo e(__('backend.mobile_number2')); ?></label>
                       <input type="text" id="phone-field" name="phone" maxlength="11" class="form-control" value="<?php echo e(!empty($corprate->phone)?$corprate->phone:old('phone')); ?>"/>
                       <?php if($errors->has("phone")): ?>
                        <span class="help-block"><?php echo e($errors->first("phone")); ?></span>
                       <?php endif; ?>
                    </div>
                    <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('company_website')): ?> has-error <?php endif; ?>">
                      <label for="company_website-field"><?php echo e(__('backend.Officail_site')); ?>: </label>
                      <input type="text" id="company_website-field" name="company_website" class="form-control" value="<?php echo e(!empty($corprate->company_website)?$corprate->company_website: old('company_website')); ?>"/>
                      <?php if($errors->has("company_website")): ?>
                      <span class="help-block"><?php echo e($errors->first("company_website")); ?></span>
                      <?php endif; ?>
                    </div>
               
                    <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('government_id')): ?> has-error <?php endif; ?>">
                      <label for="government_id-field"> <?php echo e(__('backend.government')); ?>: </label>
                      <select  class="form-control government_id" id="government_id"  name="government_id">
                          <option value="" data-display="Select"><?php echo e(__('backend.government')); ?></option>
                          <?php if(! empty($governments)): ?>
                              <?php $__currentLoopData = $governments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $government): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <option value="<?php echo e($government->id); ?>" <?php echo e($corprate->government_id == $government->id ? 'selected' : ''); ?>>
                                    <?php echo e($government->name_en); ?>

                                  </option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          <?php endif; ?>
                      </select>
                      <?php if($errors->has("government_id")): ?>
                      <span class="help-block"><?php echo e($errors->first("government_id")); ?></span>
                      <?php endif; ?>
                    </div>
                    <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('city_id')): ?> has-error <?php endif; ?>">
                      <label class="control-label" for="s_state_id"><?php echo e(__('backend.city')); ?></label>
                      <select  class=" form-control city_id" id="city_id"  name="city_id" >
                        <?php $__currentLoopData = $cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($city->id); ?>" <?php echo e($city->governorate_id == $corprate->government_id && $city->id == $corprate->city_id ? 'selected' : ''); ?>>
                               <?php echo e($city->name_en); ?>

                            </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </select>
                      <?php if($errors->has("city_id")): ?>
                      <span class="help-block"><?php echo e($errors->first("city_id")); ?></span>
                      <?php endif; ?>
                    </div>
                    <input type="hidden" name="old_cityId" value="<?php echo e($corprate->city_id); ?>" />

                    <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('address')): ?> has-error <?php endif; ?>">
                      <label for="address-field"><?php echo e(__('backend.address')); ?>: </label>
                      <input type="text" id="address-field" name="address" class="form-control" value="<?php echo e(!empty($corprate->address) ? $corprate->address: old('address')); ?>"/>
                      <?php if($errors->has("address")): ?>
                      <span class="help-block"><?php echo e($errors->first("address")); ?></span>
                      <?php endif; ?>
                    </div>
              
                    <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('latitude')): ?> has-error <?php endif; ?>">
                       <label for="latitude-field"><?php echo e(__('backend.latitude')); ?></label>
                       <input type="text" id="latitude-field" name="latitude" class="form-control" value="<?php echo e(!empty($corprate->latitude) ? $corprate->latitude : '0'); ?>"/>
                       <?php if($errors->has("latitude")): ?>
                        <span class="help-block"><?php echo e($errors->first("latitude")); ?></span>
                       <?php endif; ?>
                    </div>
                    <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('longitude')): ?> has-error <?php endif; ?>">
                       <label for="longitude-field"><?php echo e(__('backend.longitude')); ?></label>
                       <input type="text" id="longitude-field" name="longitude" class="form-control" value="<?php echo e(!empty($corprate->longitude) ? $corprate->longitude : '0'); ?>"/>
                       <?php if($errors->has("longitude")): ?>
                        <span class="help-block"><?php echo e($errors->first("longitude")); ?></span>
                       <?php endif; ?>
                    </div>

                    <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('commercial_record_number')): ?> has-error <?php endif; ?>">
                       <label for="commercial-record-field"><?php echo e(__('backend.commercial_record_number')); ?></label>
                       <input type="text" id="commercial-record-field" maxlength="14" name="commercial_record_number" class="form-control" value="<?php echo e(!empty($corprate->commercial_record_number)?$corprate->commercial_record_number:''); ?>"/>
                       <?php if($errors->has("commercial_record_number")): ?>
                        <span class="help-block"><?php echo e($errors->first("commercial_record_number")); ?></span>
                       <?php endif; ?>
                    </div>
                    <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('is_active')): ?> has-error <?php endif; ?>">
                       <label for="is_active-field"><?php echo e(__('backend.is_active')); ?></label>
                       <select id="is_active-field" name="is_active" class="form-control" value="<?php echo e(old("is_active")); ?>">
                         <option value="0" <?php echo e(old("is_active") == "0" || $corprate->is_active == "0" ? 'selected' : ''); ?>><?php echo e(__('backend.no')); ?></option>
                         <option value="1" <?php echo e(old("is_active") == "1" || $corprate->is_active == "1" ? 'selected' : ''); ?>><?php echo e(__('backend.yes')); ?></option>
                       </select>
                       <?php if($errors->has("is_active")): ?>
                        <span class="help-block"><?php echo e($errors->first("is_active")); ?></span>
                       <?php endif; ?>
                    </div>
                 <div class="col-md-12">
                    <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('commercial_record_image')): ?> has-error <?php endif; ?>">
                      <div id="commercial_record_image_upload">
                      <?php if($corprate->commercial_record_image): ?>
                          <div class="imgDiv">
                              <input type="hidden" value="<?php echo e($corprate->commercial_record_image); ?>" name="commercial_record_image_old">
                              <img src="<?php echo e($corprate->commercial_record_image); ?>"
                                   style="width:100px;height:100px">
                              <button  class="btn btn-danger cancel">&times;</button>
                          </div>
                      <?php endif; ?>
                      </div>
                      <label for="commercial_record_image-field"><?php echo e(__('backend.ContractForm')); ?></label>
                      <input  class="uploadPhoto btn btn-primary" type="file" name="commercial_record_image" id="commercial_record_image">
                      <?php if($errors->has("commercial_record_image")): ?>
                          <span class="help-block"><?php echo e($errors->first("commercial_record_image")); ?></span>
                      <?php endif; ?>
                    </div>

                    <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('logo')): ?> has-error <?php endif; ?>">
                      <div id="logo_upload">
                      <?php if($corprate->logo): ?>
                          <div class="imgDiv">
                              <input type="hidden" value="<?php echo e($corprate->logo); ?>" name="logo_old">
                              <img src="<?php echo e($corprate->logo); ?>"
                                   style="width:100px;height:100px">
                              <button  class="btn btn-danger cancel">&times;</button>
                          </div>
                      <?php endif; ?>
                      </div>
                      <label for="logo-field"><?php echo e(__('backend.logo')); ?></label>
                      <input  class="uploadPhoto btn btn-primary" type="file" name="logo" id="logo">
                      <?php if($errors->has("logo")): ?>
                          <span class="help-block"><?php echo e($errors->first("logo")); ?></span>
                      <?php endif; ?>
                    </div>
                  </div>
             <div class="form-group col-md-12 col-sm-12 col-xs-12 <?php if($errors->has('notes')): ?> has-error <?php endif; ?>">
                <label for="notes-field"><?php echo app('translator')->getFromJson('backend.ContractNote'); ?></label>
                <textarea class="form-control" id="notes-field" rows="6" name="notes"><?php echo e($corprate->notes); ?></textarea>
                <?php if($errors->has("notes")): ?>
                    <span class="help-block"><?php echo e($errors->first("notes")); ?></span>
                <?php endif; ?>
            </div>

                  <div class="well well-sm col-md-12 col-xs-12 col-sm-12">
                      <button type="submit" class="btn btn-primary" id="added_user"><?php echo e(__('backend.save_edits')); ?></button>
                      <a class="btn btn-link pull-right" href="<?php echo e(route('mngrAdmin.corporates.index')); ?>"><i class="glyphicon glyphicon-backward"></i>  <?php echo e(__('backend.back')); ?></a>
                  </div>
            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

  <script>
    $(function () {
        $("form[name='corporate_edit']").validate({
            // Specify validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                },
                mobile: {
                    required: true,
                    number:true,
                    rangelength:[11,11],
                    pattern: [0-9],
                    digits:true,
                },
               
                name: {
                    required: true,
                },
                latitude: {
                    required: true,
                    number:true,
                },
                longitude: {
                    required: true,
                    number:true,
                },
                field: {
                    required: true,
                },
                commercial_record_number: {
                    required: true,
                    number:true,
                },
                commercial_record_image: {
                    required: true,
                },
            },
            // Specify validation error messages
            messages: {
                name: {
                    required: "<?php echo e(__('backend.Please_Enter_Corporate_Name')); ?>",
                },
                email: {
                    required: "<?php echo e(__('backend.Please_Enter_Corporate_E-mail')); ?>",
                    email: "<?php echo e(__('backend.Please_Enter_Correct_E-mail')); ?>",
                  
                },
                mobile: {
                    required: "<?php echo e(__('backend.Please_Enter_Corporate_Mobile')); ?>",
                    number: "<?php echo e(__('backend.Please_Enter_Avalid_Number')); ?>",
                    rangelength:"<?php echo e(__('backend.Mobile_Must_Be11_Digits')); ?>",
                 
                },
               
                latitude: {
                    required: "<?php echo e(__('backend.Please_Enter_Latitude')); ?>",
                    number: "<?php echo e(__('backend.Please_Enter_Avalid_Number')); ?>",

                },
                longitude: {
                    number: "<?php echo e(__('backend.Please_Enter_Avalid_Number')); ?>",
                    required: "<?php echo e(__('backend.Please_Enter_Longitude')); ?>",
                },
                field: {
                    required: "<?php echo e(__('backend.Please_Enter_Work_field')); ?>",
                },
                commercial_record_number: {
                    number: "<?php echo e(__('backend.Please_Enter_Avalid_Number')); ?>",
                    required: "<?php echo e(__('backend.Please_Enter_Commercial_Record_Number')); ?>",
                },
                commercial_record_image: {
                  required: "<?php echo e(__('backend.Please_Enter_Commercial_Record_Image')); ?>",
                },
            },

            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error)
            },

            highlight: function (element) {
                $(element).parent().addClass('error')
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('error')
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function (form) {

                form.submit();

            }
        });


        if($( "#corporate_has_client" ).val() == 1) {
          $('#add_client').css('display', 'block');
        } else {
          $('#add_client').css('display', 'none');
        }

        $( "#corporate_has_client" ).change(function() {
          if( $(this).val() == 1) {
            $('#add_client').css('display', 'block');
          } else {
            $('#add_client').css('display', 'none');
          }
        });

        $("#government_id").on('change',function(){
              $.ajax({
                url: "<?php echo e(url('/get_cities')); ?>"+"/"+$(this).val(),
                type: 'get',
                data: {},
                success: function(data) {
                  $('#city_id').html('<option value="" >Choose City</option>');
                  $.each(data, function(i, content) {
                    $('#city_id').append($("<option></option>").attr("value",content.id).text(content.name_en));
                  });
                },
                error: function(data) {
                  console.log('Error:', data);
                }
              });
            });

            if($( "#corporate_has_user" ).val() == 1) {
              $('#add_user').css('display', 'block');
            } else {
              $('#add_user').css('display', 'none');
            }

            $( "#corporate_has_user" ).change(function() {
              if( $(this).val() == 1) {

                  $('#add_user').css('display', 'block');
                  $('#added_user').attr('disabled', true);
                  $('.error_user').css('display', 'inline-block');

                  // $('#model_name-field', '#plate_number-field', '#chassi_number-field', '#year-field', '#vehicle_type_id-field', '#model_type_id-field', '#color_id-field', '#driving_licence_expired_date-field', '#license_end_date_field', '#driving_licence_image_front', '#license_image_front').blur();

                  $(".done_create_user").click(function(e){

                    var NameError            = true,
                        NumberError          = true,
                        Number2Error         = true,
                        emailError           = true,
                        passwordError        = true,
                        cpasswordError       = true,
                        adderssError         = true,
                        jobError             = true;

                        if($('#user-name-field').val() == ''){
                            NameError            = true;
                        } else {
                            NameError            = false;
                        }

                        if($('#user-mobile-field').val() == ''){
                            NumberError            = true;
                        } else {
                            NumberError            = false;
                        }

                        if($('#user-phone-field').val() == ''){
                            Number2Error            = true;
                        } else {
                            Number2Error            = false;
                        }

                        if($('#user-email-field').val() == ''){
                            emailError            = true;
                        } else {
                            emailError            = false;
                        }

                        if($('#user-password-field').val() == ''){
                            passwordError            = true;
                        } else {
                            passwordError            = false;
                        }

                        if($('#user-confirm_password-field').val() == ''){
                            cpasswordError            = true;
                        } else {
                            cpasswordError            = false;
                        }

                        if($('#user-address-field').val() == ''){
                            adderssError            = true;
                        } else {
                            adderssError            = false;
                        }

                        if($('#user-job-field').val() == ''){
                            jobError            = true;
                        } else {
                            jobError            = false;
                        }

                        if(NameError === true || NumberError === true || Number2Error === true || emailError === true || passwordError === true || cpasswordError === true || adderssError === true || jobError === true) {
                            // console.log('error');
                            $('#added_user').attr('disabled', true);
                            e.preventDefault();
                            $('.error_user').css('display', 'inline-block');
                        } else {
                            // console.log('not error');
                            $('#added_user').attr('disabled', false);
                            $('.error_user').css('display', 'none');
                        }

                  });

              } else {
                  $('#add_user').css('display', 'none');
                  $('#added_user').attr('disabled', false);
                  $('.error_user').css('display', 'none');
              }
            });

    })
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/corporates/edit.blade.php ENDPATH**/ ?>
@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.Vehicle_Types')}}</title>
@endsection

@section('header')
    <div class="page-header clearfix">
        <h3>
          <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.Vehicle_Types')}}
          @if(permission('addVehicleType')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
            <a class="btn btn-success pull-right" href="{{ url('mngrAdmin/vehicle/create') }}"><i class="glyphicon glyphicon-plus"></i> {{__('backend.create')}}</a>
          @endif
        </h3>
    </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
      @if($types->count())
        <table class="table table-condensed table-striped">
          <thead>
            <tr>
              <th>#</th>
              <th>{{__('backend.name_en')}}</th>
              <th>{{__('backend.name_ar')}}</th>
              <th>{{__('backend.price')}}</status>
              <th>{{__('backend.status')}}</th>
              <th>{{__('backend.created_at')}}</th>
              <th class="text-right"></th>
            </tr>
          </thead>
          <tbody>
            @foreach($types as $type)
              <tr>
                <td>{{$type->id}}</td>
                <td>{{$type->name_en}}</td>
                <td>{{$type->name_ar}}</td>
                <td>{{$type->price}}</td>
                <td>
                  @if($type->status == 1 )
                  <span class="badge badge-pill label-success">{{__('backend.active')}}</span>
                  @else
                  <span class="badge badge-pill label-danger">{{__('backend.not_activated')}}</span>
                  @endif
                </td>
                <td>{{$type->created_at}}</td>
                <td class="text-right">
                    {{--<a class="btn btn-xs btn-primary" href="{{ route('mngrAdmin.VehicleType.show', $type->id) }}"><i class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a>--}}

                    @if(permission('editVehicleType')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                      <a class="btn btn-xs btn-warning" href="{{ url('mngrAdmin/vehicle/edit', $type->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                    @endif

                    @if(permission('deleteVehicleType')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                      <a class="btn btn-xs btn-danger" href="{{ url('mngrAdmin/vehicle/delete', $type->id) }}" onClick="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };"><i class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}</a>
                    @endif

                    <!-- <form action="" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                    </form> -->
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>

        {!! $types->appends($_GET)->links() !!}

      @else
        <h3 class="text-center alert alert-warning"> {{__('backend.No_result_found')}} </h3>
      @endif
    </div>
  </div>
@endsection

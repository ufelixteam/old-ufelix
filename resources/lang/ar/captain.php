<?php

return [
    'company' => 'يوفليكس',
    'dashboard' => 'لوحة التحكم',
    'signout' => 'تسجيل الخروج',
    'profile' => 'الصفحة الشخصية',
    'No_result_found' => 'لم يتم العثور علي اى نتائج !',
    'view' => 'عرض',
    'edit' => 'تعديل',
    'delete' => 'حذف',
    'back' => 'رجوع',
    'yes' => 'نعم',
    'no' => 'لا',
    'delivery_price' => 'تكلفة الشحن',
    'msg_confirm_delete' => 'حذف؟ هل انت متأكد؟!',
    'Save_changes' => 'حفظ التغيرات',
    'close' => 'إغلاق',
    'open' => 'فتح',
    'all_orders' => 'كل الشحنات',
    'ok' => 'تم',
    'id' => 'رقم',
    'create' => 'اضافة',
    'You_have' => 'انت لديك',
    'closed' => 'مغلقة',
    'opened' => 'مفتوحة',
    'processing' => 'معالجة',
    'publish' => 'منشور',
    'not_publish' => 'غير منشور',
    'withdraw' => 'سحب',
    'deposit' => 'وديعة',
    'bonous' => 'اضافة',
    'subtract' => 'خصم',
    'send_sms' => 'إرسال رسالة تأكيد',
    'packup_Orders' => 'packup_Orders',
    'packup' => 'packup',
    'not_found' => 'غير موجود',
    'add_user' => 'اضافة عميل',
    'add_delivery_problem' => 'أضف مشكلة توصيل',
    'location' => 'المكان',
    'choose_reason' => 'اختر سبب',
    'other_reason' => 'سبب اخر',
    'add_problem_successfully' => 'تم إضافة مشكلة تسليم بنجاح',
    'print_invoice' => 'ظباعة الفاتورة',
    'no_delivery_problems' => 'لا يوجد مشاكل فى التسليم',
    'order_delay' => 'تأجيلات الشحنة',
    'delay_at' => 'تاريخ التأجيل',
    'no_order_delay' => 'لا تأجيلات للشحنة',
    "cannot_send_delivery_problem" => "لا يمكنك حاليا ارسال شكوى",

    //////////////////////////////////////////////////////////////////////////// Dashboard
    'Count_Customers' => 'عدد العملاء',
    'More_info' => 'مزيد من المعلومات',
    'Count_ALL_TRUCKS' => 'عدد المركبات',
    'Count_Agents' => 'عدد الوكلاء',
    'Count_All_drivers' => 'عدد الكباتن',
    'Drivers_status_list' => 'قائمة حالات الكباتن',
    'Count_ONLINE_DRIVERS' => 'عدد الكباتن المتاحين',
    'Count_OFFLINE_DRIVERS' => 'عدد الكباتن الغير متاحين',
    'Count_BUSY_DRIVERS' => 'عدد الكباتن المشغولين',
    'Count_VERIFIED_DRIVERS' => 'عدد الكباتن الدي تم التحقق منهم',
    'Count_NOT_VERIFIED_DRIVERS' => 'عدد الكباتن الدي لم يتم التحقق منهم',
    'Count_ALL_ORDERS' => 'عدد جميع الطلبات',
    'Order_status_list' => 'قائمة حالات الطلبات',

    //////////////////////////////////////////////////////////////////////////// Customers
    'individual_clients' => 'العملاء الفردية',
    'clients' => 'العملاء',
    'customers' => 'العملاء',
    'customer' => 'العميل',
    'add_customer' => 'اضافة عميل',
    'sort_by_blocked' => 'فرز حسب الحظر',
    'not_blocked' => 'غير محظور',
    'blocked' => 'محظور',
    'sort_by_active' => 'فرز حسب النشاط',
    'not_activated' => 'غير نشط',
    'activated' => 'نشط',
    'sort_by_type' => 'فرز حسب النوع',
    'individual' => 'اشخاص',
    'corporates' => 'شركات',
    'name' => 'الاسم',
    'mobile' => 'رقم التليفون',
    'corporate' => 'الشركة',
    'active' => 'نشط',
    'inactive' => 'غير نشط',
    'block' => 'محظور',
    'verify_email' => 'التأكيد بالبريد',
    'verfiy_mobile' => 'التأكيد بالهاتف',
    'verfiy' => 'مؤكد',
    'not_verify' => 'غير مؤكد',
    'verified' => 'تأكيد',
    'job_name' => 'المسمي الوظيفي',
    'user_of_corporate' => 'عميل شركة',
    'user' => 'العميل',
    'search_by_name_or_mobile' => 'بحث من بالاسم او رقم الموبايل',

    // Add Customers
    'email' => 'البريد الالكتروني',
    'mobile_number' => 'رقم الهاتف',
    'mobile_number2' => 'رقم الهاتف 2',
    'password' => 'كلمة المرور',
    'confirm_password' => 'تأكيد كلمة المرور',
    'customer_type' => 'نوع العميل',
    'is_active' => 'نشط',
    'address' => 'العنوان',
    'upload_image' => 'تحميل الصور',
    'bellonging_corporate' => 'اختر شركة',
    'individual_user' => 'مستخدم شخصي',
    'corporate_user' => 'مستخدم شركة',

    'print' => 'طباعة',
    'order_notfound' => 'لم يتم العثور على هذه الشحنة',
    'scan_order' => 'مسح الشحنة',

    // Edit Customers
    'edit_customer_id_no' => 'تعديل العميل رقم',
    'enter_new_password' => 'ادخل كلمة المرور الجديدة',
    'if_you_does_not_change_password_please_leave_it_empty' => 'اذا كنت لا ترغب في تغيير كلمة المرور فرجاء تركها فارغة',
    'save_edits' => 'حفظ التعديلات',

    // Show Customer
    'the_orders' => 'الطلبات',
    'personal_information' => 'معلومات شخصية',
    'phone' => 'الهاتف',
    'corporate_information' => 'معلومات الشركة',
    'corporate_name' => 'اسم الشركة',
    'corporate_email' => 'البريد الالكتروني للشركة',
    'corporate_mobile' => 'رقم الشركة',
    'corporate_phone' => 'هاتف الشركة',

    // Show Orders Of Customer
    'orders' => 'طلبات',
    'of_customer' => 'العميل',
    'current' => 'الحالية',
    'finished' => 'المنتهية',
    'order_type' => 'نوع الطلب',
    'order_number' => 'رقم الطلب',
    'order_price' => 'سعر الطلب',
    'sender_name' => 'اسم المرسل',
    'receiver_name' => 'اسم المستلم',
    'receiver_code' => 'كود الاستلام',
    'status' => 'الحالة',
    'select' => 'تحديد',
    'deselect' => 'إلغاء التحديد',
    'accepted' => 'تم الموافقة',
    'received' => 'خرجت للتسليم',
    'pdf' => 'طباعة',
    'delivered' => 'تم التوصيل',
    'cancelled' => 'تم الإلغاء',
    'recalled' => 'تم الاسترجاع',

    //////////////////////////////////////////////////////////////////////////// Corporate
    'corporates' => 'الشركات',
    'corporate' => 'الشركة',
    'add_corporate' => 'اضافة شركة',
    'field' => 'المجال',
    'commercial_record_number' => 'رقم السجل التجاري',
    'commercial_record_image' => 'صورة السجل التجاري',
    'customers_information' => 'معلومات العملاء',
    'client_name' => 'اسم العميل',
    'company_information' => 'معلومات الشركة',
    'users_information' => 'معلومات عملاء الشركة',
    // Add Corporate
    'corporate_work_field' => 'مجال الشركة',
    'Add_Client_For_Corporate' => 'اضافة عميل الي الشركة',
    'Click_To_Add_Client_For_This_Corporate' => 'اضغط لاضافة عميل الي الشركة',
    'Add_New_Client_For_This_Corporate' => 'اضافة عميل جديد الي هذه الشركة',
    'Please_Choose_If_you_need_Add_Client_or_Not' => 'من فضلك اختر ما اذا كنت تريد اضافة عميل او لا',
    'Officail_site' => 'الموقع الرسمي للشركة',

    // Edit Corporate
    'edit_corporate' => 'تعديل شركة',
    'latitude' => 'خط العرض',
    'longitude' => 'خط الطول',

    // Show Orders Of Corporate
    'orders_of_corporate' => 'طلبات الشركة',
    'of_corporate' => 'الشركة',
    'Request_Review_Sent' => 'طلب مراجعة الارسال',
    'Your_request_sent_to_Ufelix_Admin_successfully' => 'تم ارسال طلبك الي المسئول يوفليكس بنجاح',

    //////////////////////////////////////////////////////////////////////////// Agents
    'agents' => 'الوكلاء',
    'agent' => 'الوكيل',
    'add_agent' => 'اضافة وكيل',
    'commercual_record_no' => 'رقم السجل التجاري',
    'deal_date' => 'تاريخ التعاقد ',
    'profit_rate' => 'معدل الربح',
    'drivers' => 'الكباتن',

    // Add Agent
    'fax' => 'الفاكس',
    'contract_date' => 'تاريخ العقد',
    'start_date' => 'تاريخ البدأ',

    // Show Orders Of Agent
    'of_agent' => 'الوكيل',
    'received_date' => 'Received Date',

    //////////////////////////////////////////////////////////////////////////// Drivers
    'driver' => 'كابتن',
    'add_driver' => 'اضافة كابتن',
    'sort_by_status' => 'فرز حسب الحالة',
    'online' => 'متاح',
    'Offline' => 'غير متاح',
    'busy' => 'مشغول',
    'sort_by_agent' => 'فرز حسب الوكيل',
    'ufelix' => 'يوفليكس',
    'not_ufelix' => 'خارج يوفليكس',
    'This_Driver_Not_Have_Any_Vehicles' => 'هذا السائق لا يوجد لدية اي مركبة',
    'Click_To_Add_Vehicle_For_This_Driver' => 'اضغط لاضافة مركبة لهذا الكابتن',
    'Click_To_Add_financial_For_This_Driver' => 'اضغط لاضافة الامور المالية لهذا الكابتن',
    'driver_financial' => 'الامور المالية للكابتن',
    'all_vehicle_data_must_be_filled' => 'يجب ملئ جميع بيانات المركبة اولا',
    'all_user_data_must_be_filled' => 'يجب ملئ جميع بيانات العميل اولا',
    'corporate_has_user' => 'شركة لديها عميل',

    // Add Driver
    'drivier_licence_number' => 'رقم رخصة القيادة',
    'drivier_licence_expiration_date' => 'تاريخ انتهاء صلاحية رخصة القيادة',
    'national_id_number' => 'رقم البطاقة الشخصية',
    'national_id_expired_date' => 'تاريخ انتهاء البطاقة الشخصية',
    'availability_status' => 'الحالة المتاحة',
    'isactive_verified_notverified' => 'النشاط',
    'choose_country' => 'اختر الدولة',
    'choose_beloning_agent' => 'اختر الوكيل',
    'notes' => 'ملاحظات',
    'criminal_record_image' => 'صورة السجل الجنائي',
    'profile_image' => 'الصورة الشخصية',
    'drivier_licence_image_front' => 'صورة رخصة السائق',
    'drivier_licence_image_back' => 'صورة رخصة السائق الخلفية',
    'national_id_image_front' => 'صورة البطاقة الشخصية',
    'national_id_image_back' => 'صورة البطاقة الشخصية الخلفية',
    'driver_has_vehicle' => 'كابتن لديه سيارة',
    'basic_salary' => 'الراتب الاساسي',
    'bonus_of_delivery' => 'بونص التوصيل',
    'pickup_price' => 'ثمن تجمع البيكب',
    'recall_price' => 'ثمن المرتجع',
    'reject_price' => 'ثمن الإلغاء',
    'bonus_of_pickup_for_one_order' => 'بونص تجميع الشحنة الواحدة',
    'choose_yes_or_no' => 'اختر نعم او لا',
    'has_vehicle' => 'يمكلك مركبة',
    'have' => 'يملك',
    'not_have' => 'لا يملك',
    'Add_New_Vehicle_For_This_Driver' => 'اضافة مركبة جديدة لهذا الكابتن',
    'Edit_Vehicle_For_This_Driver' => 'تعديل المركبة الخاصة لهذا الكابتن',

    // Edit Drivers
    'edit_driver' => 'تعديل الكابتن',
    'new_password' => 'كلمة المرور الجديد',
    'attention' => 'انتباه',
    'If_It_Chosen_That_It_Does_Not_Have_a_Vehicle_its_Vehicle_Will_Be_Deleted' => 'اذا تم اختار لا يملك مركبة.. فانه سيتم حذف المركبة الخاصة به',

    // Show Driver
    'No_Notes_to_Show' => 'لا توجد اي ملاحظات',
    'Driver_location' => 'موقع الكابتن',
    'Belonging_Agent_Information' => 'معلومات عن الوكيل',
    'Belonging_Agent_Name' => 'اسم الوكيل',
    'Belonging_Agent_Mobile' => 'رقم هاتف الوكيل',
    'Belonging_Agent_Mobile2' => 'رقم هاتف الوكيل 2',
    'Truck_Information' => 'معلومات الشاحنة',
    'id' => 'رقم',
    'Model_Name' => 'اسم الموديل',
    'Plate_Number' => 'رقم اللوحة',
    'Chassi_Number' => 'رقم الشاسيه',
    'View_All' => 'عرض الكل',

    // Show Orders Of Driver
    'of_driver' => 'الكابتن',
    'Search_by_Model_Name_Or_Plate_Number' => 'بحث من خلال اسم الموديل او رقم اللوحة',
    'drivers_of_agent' => 'كباتن الوكيل',
    'orders_of_driver' => 'طلبات الكابتن',

    //////////////////////////////////////////////////////////////////////////// Vehicles
    'vehicles' => 'المركبات',
    'vehicle' => 'المركبة',
    'add_vehicle' => 'اضافة مركبة',
    'edit_vehicle' => 'تعديل المركبة',

    // Add Vehicle
    'Model_Year' => 'تاريخ الموديل',
    'Vehicle_Type' => 'نوع المركبة',
    'color' => 'اللون',
    'Model_Type' => 'نوع الموديل',
    'Belonging_Agent' => 'الوكيل',
    'Choose_Agent' => 'اختر وكيل',
    'Owner_Driver' => 'السائق المالك',
    'License_Start_Date' => 'تاريخ بداية الرخصة',
    'License_End_Date' => 'تاريخ انتهاء صلاحية رخصة المركبة',
    'Vehicle_Image' => 'صورة المركبة',
    'License_Image_Front' => 'صورة رخصة المركبة',
    'License_Image_Back' => 'صورة الرخصة الخلفية',

    // Show Vehicle
    'year' => 'السنة',
    'type' => 'النوع',
    'Belonging_Driver_Information' => 'معلومات الكابتن',
    'driving_licence_expired_date' => 'تاريخ انتهاء رخصة القيادة',
    'This_Truck_do_not_assigned_to_driver' => 'لم يتم تعيين كابتن لهذه المركبة',

    // Map
    'map' => 'الخريطة',
    'Track_Drivers' => 'مسار الكباتن',

    //////////////////////////////////////////////////////////////////////////// Wallets
    'the_wallets' => 'المحافظ',
    'wallets' => 'محافظ',
    'wallet' => 'محفظة',
    'order' => 'شحنة',
    'add_wallet' => 'اضافة محفظة',
    'view_logs' => 'عرض السجلات',
    'value' => 'القيمة',
    'Ufelix_Account' => 'حساب يوفليكس',

    // Add Wallet
    'User_has_Already_Wallet' => 'هذا الشخص لدية محفظة بالفعل',
    'available' => 'متاح',
    'Not_Available' => 'غير متاح',

    // Show Wallet
    'Wallet_Owner_ID' => 'رقم مالك المحفظة',
    'Wallet_Owner' => 'مالك المحفظة',
    'Wallet_Amount' => 'مبلغ المحفظة',
    'Wallet_Log' => 'سجل المحفظة',
    'invoice' => 'الفاتورة',
    'reason' => 'السبب',
    'No_Wallet_Log' => 'لا يوجد سجل للمحفظة',
    'Withdrawal_Amount_From_Wallet' => 'المبلغ الذي سيتم سحبة من المحفظة',
    'amount' => 'المبلغ',
    'Add_Amount_To_Wallet' => 'المبلغ الذي سوف يضاف الي المحفظة',
    'Confirm_Close' => 'تأكيد الاغلاق',
    'msg_close_wallet' => 'لا يمكنك غلق المحفظة و ذلك بسبب وجود مال داخل المحفظة',
    'msg_close_wallet2' => 'برجاء سحب المبلغ من المحفظة اولا قبل الإغلاق',
    'Something_error' => 'يوجد خطأ ما',

    //////////////////////////////////////////////////////////////////////////// Invoices
    'the_invoices' => 'الفواتير',
    'invoices' => 'فواتير',
    'invoice' => 'الفاتورة',
    'the_driver' => 'الكابتن',
    'the_corporate' => 'الشركة',
    'the_agent' => 'الوكيل',
    'invoice_type' => 'نوع الفاتورة',

    // Show Invoice
    'Orders_List' => 'قائمة الطلبات',
    'Delivered_Item' => 'المنتج',
    'Item_Price' => 'سعر المنتج',
    'Order_Date' => 'تاريخ الطلب',
    'weight' => 'الوزن',
    'Delivery_Price' => 'تكلفة الشحن',
    'Desrved_Price' => 'المبلغ المستحق',
    'Received_Date' => 'تاريخ الاستلام',
    'Delivered_Date' => 'تاريخ التسليم',
    'Invoice_Bill' => 'الفاتورة النهائية',
    'Total_order_Price' => 'اجمالي مبلغ الطلبات',
    'Total_Delivery' => 'اجمالي مبلغ التوصيل',
    'Driver_Pocket' => 'محفظة الكابتن',
    'Driver_Commission' => 'عمولة الكابتن',
    'UFelix_Commission' => 'عمولة يوفليكس',
    'Driver_Depth' => 'Driver Depth',
    'UFelix_Depth' => 'UFelix Depth',
    'Total_Driver_Deserved' => 'المبلغ المستحق للكابتن',
    'Total_UFelix_Deserved' => 'المبلغ المستحق ليوفليكس',
    'pay' => 'دفع',
    'Pay_Invoice' => 'دفع الفاتورة',
    'Something_Went_Wrong_please_Try_again_in_a_minute' => '.هناك خطأ ما! يرجى المحاولة خلال دقيقة واحدة',
    'Please_Enter_the_Client_Phone_No_to_Confirm' => 'من فضلك ادخل رقم موبايل العميل للتأكيد',

    'Phone_Number_can_not_be' => 'رقم الهاتف لا يمكن ان يكون',
    'Empty_or_Less_than14' => 'فارغا او اقل من 14',
    'number' => 'رقم !',
    'Phone_Number_is' => 'رقم الهاتف',
    'wrong' => 'خطأ',
    'Enter_the_Verification_Code_you_Recieved' => 'أدخل رمز التحقق الذي تلقيته',
    'Verification_Code_can_not_be' => 'رمز التحقق لا يمكن أن يكون',
    'Empty_or_Less_than4' => 'فارغا او اقل من 4',
    'confirm' => 'تأكيد',
    'Code_Verification' => 'رمز التحقق',
    'done' => 'تم',
    'Driver_Name' => 'اسم الكابتن',
    'Driver_Mobile' => 'هاتف الكابتن',
    'Transfer_To_Other_Invoice' => 'شحنات الفاتورة',
    'recall' => 'الغاء',
    'Confirm_Price_List' => 'تأكيد قائمة الأسعار',
    'Transfer_Orders' => 'نقل الطلبات',
    'Reset_To_Default' => 'اعادة تعيين الافتراضي',
    'Shipment_Orders' => 'طلبات الشحن',
    'material' => 'مواد',
    'price' => 'السعر',
    'quantity' => 'الكمية',
    'Total_Price' => 'السعر الكلي',
    'convert_pending_to_dropped' => 'تم انزال الشحنات فى المخزن بنجاح',
    'confirm_drop_orders' => 'تأكيد انزال الشحنات',
    'pending' => 'معلقة',
    'dropped' => 'انزلت في المخزن',
    'This_Invoice_calculated_by_Price_list_$' => 'هذه الفاتورة محسوبة حسب قائمة الأسعار %',
    'This_Invoice_calculated_by_Percentage_%' => 'هذه الفاتورة محسوبة حسب النسبة المئوية %',
    'pocket' => 'المحفظة',
    'remain' => 'المتبقي',
    'discount' => 'الخصم',
    'Corporate_Deserved' => 'المستحق للشركة',
    'Ufelix_Deserved' => 'المستحق ليوفليكس',
    'Captain_Deserved' => 'المستحق للكابتن',
    'Target_Percentage_Discount_%' => 'الخصم بالنسبة المؤية %',
    'commission' => 'العمولة',
    'deserved' => 'المستحق',
    'Calculate_the_total_after_discount' => 'حساب الاجمالي بعد الخصم',
    'Calculate_Percentage' => 'حساب النسبة المؤية',
    'Confirm_Percentage' => 'تأكيد النسبة',
    'Confirm_Transaction' => 'تأكيد العملية',
    'individual_client' => 'عميل فردي',

    //////////////////////////////////////////////////////////////////////////// Orders
    'the_orders' => 'الطلبات',
    'add_order' => 'اضافة طلب',
    'Pending_Orders' => 'طلبات معلقة',
    'Dropped_Orders' => 'طلبات انزلت فى المخزن',
    'Accepted_Orders' => 'طلبات تم الموافقة عليها',
    'Received_Orders' => 'طلبات مستلمة',
    'Delivered_Orders' => 'طلبات قيد التوصيل',
    'Cancelled_Orders' => 'طلبات ملغية',
    'Recalled_Orders' => 'طلبات مسترجعة',
    'Waiting_Orders' => 'طلبات منتظرة',
    'Final_Cancellation_Orders' => 'طلبات الغيت نهائيا',
    'Expired_Waiting_Orders' => 'طلبات منتهية الانتظار',
    'Collection_Order_Excel' => 'مجموعة طلبات اكسل',
    'import_orders' => 'استراد الطلبات',
    'Collection_Order_Form' => 'نموذج اضافة الطلبات',
    'search' => 'بحث...',
    'sender_Government' => 'محافظة المرسل',
    'sender_City' => 'مدينة المرسل',
    'Receiver_Government' => 'محافظة المستلم',
    'Receiver_City' => 'مدينة المستلم ',
    'from' => 'من',
    'to' => 'الي',
    'forward' => 'توجية',
    'Forward_Orders_In_Agent_Or_Driver' => 'قم بتوجيه هذا الطلب علي وكيل او كابتن',
    'Choose_Driver' => 'اختر كابتن',
    'action_back' => 'تغير حاله الشحنه',

    // Create Orders
    'Create_Orders_Collection' => 'انشاء مجموعة طلبات',
    'government' => 'المحافظة',
    'city' => 'المدينة',
    'Order_Data' => 'بيانات الطلب',
    'Type_Name' => 'الاسم',
    'store' => 'المخزن',
    'Choose_Store' => 'اختر مخزن',
    'stock' => 'المخزن الفرعي',
    'Choose_Stock' => 'اختر مخزن فرعي',
    'Quantity_Order' => 'كمية الطلبات',
    'overload' => 'الوزن',
    'Fees_From' => 'تحصيل الرسوم من',
    'Sender_Mobile' => 'هاتف المرسل',
    'Receiver_Mobile' => 'هاتف المستلم',

    // Edit Order
    'Edit_Order' => 'تعديل الطلب',
    'Receiver_Mobile2' => 'هاتف المستلم 2',
    'Receiver_Latitude' => 'خط عرض المستلم',
    'Receiver_Longitude' => 'خط طول المستلم',
    'Sender_Mobile2' => 'هاتف المرسل 2',
    'created' => 'تم انشائة',
    'arrived' => 'تم الوصول',
    'Sender_Latitude' => 'خط عرض المرسل',
    'Sender_Longitude' => 'خط طول المرسل',
    'Choose_Order_Type' => 'اختر نوع الطلب',
    'Order_Type_ID' => 'رقم نوع الطلب',
    'Customer_ID' => 'مسلسل العميل',
    'Payment_Method_ID' => 'مسلسل طريقةالدفع',

    // Show Orders
    'Order_No' => 'طلب رقم .',
    'Cancel_and' => 'الغاء و',
    'Change_Agent' => 'تغيير الوكيل',
    'Cancel_Order_Client_Required' => 'الغاء الطلب / العميل مطلوب',
    'Create_at' => 'تاريخ الانشاء',
    'Accept_at' => 'تم القبول في',
    'No_Date' => 'لا يوجد تاريخ',
    'Received_at' => 'تم الاستلام في',
    'Deliver_at' => 'تم التوصيل في',
    'Receiver_Data' => 'بيانات الاستقبال',
    'Receiver_Address' => 'عنوان المستلم',
    'Customer_Account' => 'حساب العميل',
    'Sender_Data' => 'بيانات المرسل',
    'Sender_Address' => 'عنوان المرسل',
    'Package_Data' => 'بيانات مختوي الطلب',
    'Driver_Data' => 'بيانات الكابتن',
    'Driver_Address' => 'عنوان الكابتن',
    'Order_is_not_Accepted_yet' => 'لم يتم قبول الطلب بعد !',
    'Forward_Order' => 'توجيه الطلب',
    'Forward_Order_To_Another_Agent' => 'توجيه الطلب الي وكيل اخر',

    //////////////////////////////////////////////////////////////////////////// Collection Orders Excel
    'Order_Collection' => 'مجموعة الطلبات',
    'Collection_Orders' => 'مجموعة الطلبات',
    'form' => 'نموذج',
    'excel' => 'اكسل',
    'add_Collection' => 'اضافة مجموعة',
    'Fillter_Page' => 'تصفية الصفحة',
    'Upload_After_Check' => 'تحميل بعد التحقق',
    'Sort_By_Saved' => 'فرز حسب الحفظ',
    'Not_Saved' => 'لم يحفظ بعد',
    'saved' => 'تم الحفظ',
    'File_Name' => 'اسم الملف',
    'Saved_At' => 'تاريخ الحفظ',
    'file' => 'الملف',
    'Download_File' => 'تحميل الملف',

    // Add Collection Orders
    'choose_corporate' => 'اختر شركة',
    'choose_customer' => 'اختر عميل',
    'Add_File' => 'اضافة ملف',

    // Edit Collection Orders
    'sheet' => 'الملف',

    //////////////////////////////////////////////////////////////////////////// Scan
    'scan' => 'فحص',
    'Scanned_Code' => '(فحص الكود (رقم الطلب / رمز الاستلام',
    'scanned' => 'فحص',
    'captain_entry' => 'Captain Entry',
    'captain_exit' => 'Captain Exit',
    'moved_to' => 'Moved To',
    'moved_drop' => 'Moved drop',
    'pending_dropped' => 'Pending dropped',
    'moved_dropped' => 'Moved dropped',
    'pickup_dropped' => 'Pickup dropped',
    'filter' => 'Filter',
    'orders_dropped' => 'Orders Dropped',
    'orders_moved_to' => 'Orders Moved To WareHouse',
    'collected_cost' => 'المبلغ المحصل فعليا',
    'full_delivered' => 'تسليم كلي',
    'part_recall' => 'تسليم جزئي',
    'changes_saved_successfully' => 'Changes Saved Successfully',

    //////////////////////////////////////////////////////////////////////////// Notifications
    'notifications' => 'الاشعارات',
    'notification' => 'اشعار',
    'send_notifications' => 'ارسال اشعار',
    'Notifications_Types' => 'انواع الاشعارات',
    'device' => 'الجهاز',
    'title' => 'العنوان',
    'message' => 'الرسالة',
    'date' => 'التاريخ',
    'audience' => 'الجمهور',
    'Published_by' => 'ارسلت من قبل ',
    'Notifications_Timeline' => 'المخطط الزمني للاشعارات',

    // Add Notifications
    'new' => 'جديد',
    'New_Notification' => 'اشعار جديد',
    'Notification_Type' => 'نوع الاشعار',
    'Select_type' => 'حدد النوع',
    'Send_to' => 'ارسال الي',
    'Select_Sender_type' => 'حدد نوع المرسل',
    'all' => 'الكل',
    'captains' => 'الكباتن',
    'individuals' => 'الافراد',
    'devices' => 'الاجهزة',
    'Select_device' => 'حدد جهاز',
    'android' => 'الاندرويد',
    'ios' => 'IOS',
    'browser' => 'المتصفح',
    'send' => 'ارسال',

    // Show Notifications
    'show_notification' => 'عرض الاشعار',

    // Edit Notifications
    'edit_notification' => 'تعديل الاشعار',

    //////////////////////////////////////////////////////////////////////////// Notifications Types
    'update' => 'تعديل',
    'save' => 'حفظ',

    //////////////////////////////////////////////////////////////////////////// Shipment Tools
    'shipment' => 'الشحن',
    'shipment_material' => 'اداة الشحن',
    'shipment_material_orders' => 'ادوات شحن الطلبات',
    'shipment_destinations' => 'وجهات الشحن',
    'Shipment_Tools' => 'ادوات الشحن',
    'add_shipment_tool' => 'اضافة اداة شحن',

    // Add Shipment Tool
    'New_Shipment_Material' => 'شحنة المواد الجديدة',
    'Title_in_English' => 'العنوان بالانجليزي',
    'Title_in_Arabic' => 'العنوان بالعربي',
    'description' => 'الوصف',

    // Edit Shipmet Tools
    'Edit_Shipment_Tool' => 'تعديل اداة الشحن',

    //////////////////////////////////////////////////////////////////////////// Shipment Tool Orders
    'Shipment_Orders' => 'طلبات الشحن',
    'Shipment_Tool_Orders' => 'ادوات طلبات الشحن',
    'Add_Shipment_Orders' => 'اضافة طلب شحن',
    'orgnization' => 'المنظمة',

    // Add Shipment Tool Orders
    'rejected' => 'مرفوض',

    // Edit Shipment Tool Orders
    'Edit_Shipment_Orders' => 'تعديل طلب الشحن',

    // Show Shipment Tool Orders
    'Order_Items' => 'عناصر الطلب',

    //////////////////////////////////////////////////////////////////////////// Shipment Destinations
    'add_shipment_destinations' => 'اضافة وجهة شحن',
    'cost' => 'التكلفة',
    'overweight_cost' => 'تكلفة الوزن الزائد',

    // Add Shipment Destinations
    'Choose_Start' => 'اختر البداية',
    'Choose_End' => 'اختر النهاية',
    'add_destination' => 'اضافة واجهة',

    // Edit Shipment Destinations
    'Edit_Destination' => 'تعديل الواجهة',

    //////////////////////////////////////////////////////////////////////////// Stores
    'stores' => 'المخازن',
    'add_store' => 'اضافة مخزن',
    'chosse_Government' => 'اختر المحافظة',
    'no_of_stocks' => ' عدد المخزون',
    'responsible' => 'المسؤول',
    'responsible_phone' => 'رقم المسؤول',
    'select_all' => 'تحديد الكل',
    'no_selected_stores' => 'لم يتم تحديد اي من المخازن',


    // Edit Store
    'edit_store' => 'تعديل المخزن',

    // Show Store
    'show_store' => 'المخزن',
    'no' => 'رقم',
    'store_date' => 'تاريخ التخزين',
    'dropped_by' => 'Dropped By',
    'actual_piece' => 'الكميه',
    'used_piece' => 'الكميه المستخدمه',

    //////////////////////////////////////////////////////////////////////////// Stocks
    'stocks' => 'المخازن الفرعية',
    'add_stock' => 'اضافة مخزن فرعي',

    // Edit Stocks
    'Edit_Stock' => 'تعديل المخزن الفرعي',
    'Show_Stock' => 'عرض المخزن الفرعي',
    'used' => 'مستخدم',
    'not_used' => 'غير مستخدم',
    'piece_status' => 'حالة الكميه',

    ////////////////////////////////////////////////////////////////////////////  Promo Codes
    'promo_codes' => 'رموز الترويج',
    'add_promo_code' => 'اضافة رمز ترويجي',
    'code' => 'الرمز',
    'no_tries' => 'رقم المحاولة',

    // Edit Promo Codes
    'edit_promo_code' => 'تعديل الرمز الترويجي',

    //////////////////////////////////////////////////////////////////////////// Employees
    'employees' => 'الموظفين',
    'add_employee' => 'اضافة موظف',
    'salary' => 'المرتب',
    'job_title' => 'المسمي الوظيفي',

    // Add Employee
    'bio' => 'السيرة الذاتية',
    'image' => 'الصورة الشخصية',

    // Edit Employee
    'edit_employee' => 'تعديل الموظف',

    // Show Employee
    'show_employee' => 'عرض الموظف',

    //////////////////////////////////////////////////////////////////////////// Permission
    'permissions' => 'الصلاحيات',
    'type_permission' => 'انواع الصلاحيات',
    'roles' => 'الادوار',
    'permission_name' => 'اسم الصلاحية',
    'permission_type' => 'نوع الصلاحية',
    'choose_permission_type' => 'اختر نوع الصلاحية',
    'Edit_Permission' => 'تعديل الصلاحية',

    //////////////////////////////////////////////////////////////////////////// Permission Types
    'Edit_Permission_Type' => 'تعديل نوع الصلاحية',

    //////////////////////////////////////////////////////////////////////////// Roles
    'admin_roles' => 'اداور المشرفين',
    'role_name' => 'اسم الدور',
    'Choose_Permissions' => 'اختر الصلاحيات',
    'Choose_Method' => 'اختر طريقة',
    'Total_Price' => 'السعر الإجمالي',
    'Edit_Roles' => 'تعديل الدور',
    'Show_Role' => 'عرض دور',
    'permission' => 'صلاحيات',

    //////////////////////////// Others /////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////// Android Versions
    'others' => 'اخري',
    'Android_Versions' => 'اصدرات الاندرويد',
    'version' => 'الاصدار',
    'created_at' => 'تاريخ الانشاء',
    'updated_at' => 'تاريخ التعديل',
    'platform' => 'المنصة',
    'update' => 'محدث',
    'not_update' => 'غير محدث',
    'Android_Client' => 'اندرويد عميل',
    'Android_Driver' => 'اندرويد كابتن',
    'IOS_Client' => 'IOS عميل',
    'web' => 'موقع',

    // Add Android Versions
    'Android_Versions' => 'اصدار الاندرويد',
    'chosse_platform' => 'اختر المنصة',
    'chosse_status' => 'اختر الحالة',

    // Edit Android Version
    'Edit_Android_Varsion' => 'تعديل اصدار الاندرويد',
    'version_name' => 'اسم الاصدار',

    //////////////////////////////////////////////////////////////////////////// Order Types
    'Order_Types' => 'انواع الطلبات',
    'name_en' => 'الاسم بالانجليزية',
    'name_ar' => 'الاسم بالعربية',

    // Edit Order Type
    'Edit_Order_Type' => 'تعديل نوع الطلب',

    //////////////////////////////////////////////////////////////////////////// Resources
    'resources' => 'المصادر',
    'started_at' => 'تاريخ البدأ',
    'renew_at' => 'تاريخ التجديد',
    'username' => 'اسم المستخدم',

    // Show Resource
    'Resources_No' => 'مصدر رقم',

    // Edit Resource
    'Edit_Resources' => 'تعديل المصدر',

    //////////////////////////////////////////////////////////////////////////// Vehicles Model
    'Vehicles_Model' => 'طراز المركبات',

    // Edit Vehicles Model
    'Edit_Vehicles_Model' => 'تعديل طراز المركبة',

    //////////////////////////////////////////////////////////////////////////// Vehicle Types
    'Vehicle_Types' => 'انواع المركبات',

    // Edit Vehicle Types
    'Edit_Vehicle_Types' => 'تعديل نوع المركبة',

    //////////////////////////////////////////////////////////////////////////// Vehicle Colors
    'Vehicle_Colors' => 'الوان المركبات',
    'color' => 'اللون',
    'Color_Name' => 'اسم اللون',

    // Edit Vehicle Colors
    'Edit_Vehicle_Colors' => 'تعديل لون المركبة',

    //////////////////////////////////////////////////////////////////////////// Payment Methods
    'Payment_Methods' => 'طرق الدفع',
    'Payment_Method' => 'طريقة الدفع',

    // Edit Payment Method
    'Edit_Payment_Method' => 'تعديل طريقة الدفع',

    //////////////////////////////////////////////////////////////////////////// Countries
    'countries' => 'البلدان',
    'c_image' => 'الصورة',

    // Edit Countries
    'Edit_Countries' => 'تعديل البلد',

    //////////////////////////////////////////////////////////////////////////// Governorates
    'governorates' => 'المحافظات',
    'governorate' => 'المحافظة',
    'country' => 'البلد',

    // Add Government
    'governorate_en' => 'المحافظة بالانجليزية',
    'governorate_ar' => 'المحافظة بالعربية',

    // Edit Governorate
    'Edit_Governorate' => 'تعديل محافظة',

    //////////////////////////////////////////////////////////////////////////// Cities
    'cities' => 'المدن',
    'sort_by_city' => 'فرز حسب المدينة',

    // Add Cities
    'city_ar' => 'اسم المدينة بالعربية',
    'city_en' => 'اسم المدينة بالانجليزية',

    // Edit Cities
    'edit_city' => 'تعديل مدينة',

    //////////////////////////////////////////////////////////////////////////// Pricing List
    'Pricing_List' => 'قائمة الاسعار',
    'target' => 'الهدف',
    'discount_percentage' => 'نسبة الخصم',

    // Edit Price List
    'Edit_Price_List' => 'تعديل قائمة الاسعار',

    // Show Price List
    'Show_Price_List' => 'عرض قائمة الاسعار',

    //////////////////////////////////////////////////////////////////////////// Corporate Target
    'corporate_target' => 'هدف الشركة',
    'add_corporate_target' => 'اضافة هدف للشركة',
    'Add_New_Deal_For_This_Corporate' => 'اضافة عرض جديد لهذه الشركة',
    'Add_New_Deal_For_Corporate' => 'اضافة عرض جديد للشركة',
    'Add_Deal' => 'اضافة عرض',
    'select_corporate_for_show_targets' => 'اختر الشركة لعرض الاهداف الخاصة بها',
    'target_from' => 'الهدف من',
    'target_to' => 'الهدف الي',
    'this_corporate_not_have_any_target' => 'هذه الشركة لا تحتوي علي اي هدف',
    'Not_Have_Any_Target' => 'لا يوجد اي هدف... برجاء  اضافة هدف اولا',
    'corporate_target_saved_successfully' => 'تم حفظ هدف الشركة بنجاح',
    'corporate_target_updated_successfully' => 'تم تعديل هدف الشركة بنجاح',
    'corporate_target_deleted_successfully' => 'تم حذف هدف الشركة بنجاح',


    ///////////////////// End Others //////////////////////////////

    //////////////////////////////////////////////////////////////////////////// Documentations
    'documentations' => 'الوثائق',
    'documentation_types' => 'انواع الوثائق',
    'add_doc_type' => 'اضافة نوع وثيقة',
    'add_doc' => 'اضافة وثيقة',
    'modified' => 'تاريخ التعديل',
    'added_by' => 'اضافة بواسطة',
    'edited_by' => 'تعديل بواسطة',
    'slug' => 'الرابط',
    'choose_docs_type' => 'اختر نوع الوثيقة',

    //////////////////////////////////////////// Reports
    'reports' => 'التقارير',
    'From_Date' => 'التاريخ من',
    'To_Date' => 'التاريخ الي',
    'Search_about' => 'البحث عن',
    'requests' => 'الارسالات',
    'Order_By' => 'ترتيب حسب',
    'display' => 'عرض',
    'download' => 'تحميل',

    //////////////////////////////////// Contact Us
    'Contact_Us' => 'الاتصال',
    'contacts' => 'الاتصال',

    // Add Contact
    'add_contact' => 'اضافة اتصال',

    // Edit Contact
    'Edit_Contacts' => 'تعديل الاتصال',

    //////////////////////////////////////////////////////////////////////////// Admins
    'admins' => 'المشرفين',
    'admin' => 'المشرف',
    'add_admin' => 'اضافة مشرف',
    'role' => 'الدور',
    'choose_role' => 'اختر دور',

    // Edit Admins
    'edit_admin' => 'تعديل المشرف',

    // Show Admin
    'Block' => 'حظر',
    'Unblock' => 'عدم الحظر',

    //////////////////////////////////////////////////////////////////////////// About Us
    'about_us' => 'من نحن',
    'Google_Plus' => 'جوجل بلس',
    'Website_Link' => 'رابط الموقع',
    'Twitter_Link' => 'رابط تويتر',
    'YouTube_Link' => 'رابط اليوتيوب',
    'Facebook_Link' => 'رابط فيسبوك',
    'Telephone_No2' => 'رقم الهاتف 2',
    'Telephone_No1' => 'رقم الهاتف 1',
    'Support_E-mail' => 'البريد الالكتروني للدعم',
    'LinkedIn_Link' => 'رابط لينكدان',
    'Instagram_Link' => 'رابط انسنجرام',
    'Pinterest' => 'بينتريست',
    'Dribble' => 'دريبل',
    'Vimeo' => 'فيميو',
    'Tumbler Link' => 'ابط تيمبلر',
    'About' => 'من نحن',
    'No_Agents' => 'رقم الوكيل',
    'No_Customers' => 'رقم العميل',
    'No_Corporates' => 'رقم الشركة',
    'No_Drivers' => 'رقم الكابتن',
    'No_Orders' => 'رقم الطلب',
    'No_Vehicles' => 'رقم المركبة',
    'settings' => 'الاعدادات',


    'activity_log' => 'سجل النشاطات',
    'usertype' => 'نوع المستخدم',
    'url' => 'الرابط',
    'subject' => 'المحتوي',
    'time' => 'الوقت',
    'recall_by' => 'بواسطة',
    'recall_by_receiver' => 'مرتجع من المستلم',
    'recall_by_sender' => 'مرتجع من المرسل',
    'recall_at' => 'التاريخ',
    'select_date_for_show_activity_log' => 'حدد التاريخ لعرض سجلات النشاطات',
    'deleted_activityLog_successfully' => 'تم حذف سجل النشاط بنجاح',
    'select_activity_log_type' => 'حدد نوع سجل النشاط',


    //////////////////////////////////////////////////////////////////////////// Activity Log Type
    'Activity_log_type' => ' انواع سجل النشاط ',
    'edit_type' => 'تعديل النوع',
    'activity_log_Type_saved_successfully' => 'تم حفظ نوع سجل النشاط بنجاح',
    'activity_log_Type_Updated_successfully' => 'تم تعديل نوع سجل النشاط بنجاح',
    'activity_log_Type_Deleted_successfully' => 'تم حذف نوع سجل النشاط بنجاح',


    // validation error messages For Add Customer
    'Please_Enter_Customer_Password' => 'من فضلك ادخل كلمة مرور العميل',
    'Password_must_be_more_than5_character' => 'كلمة المرور يجب الا يقل عن 5 ارقام',
    'Please_Enter_Customer_Name' => 'من فضلك ادخل اسم العميل',
    'Please_Enter_Your_Address' => 'من فضلك ادخل عنوان العميل',
    'Confirm_password_is_wrong' => 'تأكيد كلمة المرور غيبر صحيحة',
    'Please_Enter_Customer_E-mail' => 'من فضلك ادخل البريد الالكتروني للعميل',
    'Please_Enter_Customer_Correct_E-mail' => 'من فضلك ادخل البريد الالكتروني صحيح',
    'E-mail_already_exist' => 'البريد الالكتروني موجود بالفعل',
    'Please_Enter_Customer_Mobile' => 'من فضلك ادخل رقم موبايل العميل',
    'Mobile_Must_Be11_Digits' => 'يجب ان يتكون رقم الهاتف 11 رقم',
    'Mobile_already_exist' => 'رقم الهاتف هذا موجود بالفعل',
    'Please_Enter_Customer_Phone' => 'من فضلك ادخل هاتف العميل',
    'Please_Enter_Customer_Image' => 'من فضلك ادخل الصورة الشخصية للعميل',

    // validation error messages For Add Corporate
    'Please_Choose_Corporate' => 'من فضلك اختر الشركة',
    'Please_Choose_Customer' => 'من فضلك اختر العميل',
    'Please_Enter_File' => 'من فضلك ادخل الملف',
    'Please_Enter_Corporate_Name' => 'من فضلك ادخل اسم الشركة',
    'Please_Enter_Corporate_E-mail' => 'من فضلك ادخل البريد الالكتروني للشركة',
    'Please_Enter_Corporate_Mobile' => 'من فضلك ادخل رقم موبايل الشركة',
    'Please_Enter_Corporate_Mobile2' => 'من فضلك ادخل رقم موبايل الشركة 2',
    'Please_Enter_Work_field' => 'يرجى إدخال حقل العمل',
    'Please_Enter_Commercial_Record_Image' => 'يرجي ادخال صورة السجل التجاري',

    // validation error messages For Add Agent
    'Please_Enter_Agent_Name' => 'من فضلك ادخل اسم الوكيل',
    'Please_Enter_Agent_Mobile_Number' => 'من فضلك ادخل رقم الهاتف',
    'Mobile_Number_already_exist' => 'رقم الهاتف هذا موجود بالفعل',
    'Mobile_Number2_already_exist' => 'رقم الهاتف  هذا موجود بالفعل',
    'Please_Enter_Agent_E-mail' => 'من فضلك ادخل البريد الالكتروني للوكيل',
    'Please_Enter_Correct_E-mail' => 'من فضلك اخل البريد الالكتروني صحيح',
    'E-mail_already_exist' => 'البريد الالكتروني هذا موجود بالفعل',
    'Please_Enter_Commercial_Record_Number' => 'من فضلك ادخل رقم السجل التجاري',
    'Please_Enter_Agent_Password' => 'من فضلك ادخل كلمة المرور',
    'Password_must_be_more_than5_character' => 'كلمة المرور يجب أن تكون أكثر من 5 أحرف',
    'Please_Enter_Agent_confirm_Password' => 'من فضلك ادخل تأكيد كلمة المرور',
    'Confirm_password_is_wrong' => 'تأكيد كلمة المرور غير صحيحة',

    // validation error messages For Add Driver
    'Please_Enter_Driver_Password' => 'من فضلك ادخل كلمة المرور',
    'Please_Enter_driver_Name' => 'من فضلك ادخل اسم الكابتن',
    'Password_must_be_more_than5_character' => 'كلمة المرور يجب ان تكون اكثر من 5 احرف',
    'Confirm_password_is_wrong' => 'تأكيد كلمة المرور غير صحيحة',
    'Please_Enter_Driver_E-mail' => 'من فضلك ادخل البريد الالكتروني',
    'Please_Enter_Driver_Correct_E-mail' => 'من فضلك ادخل البريد الالكتروني صحيح',
    'E-mail_already_exist' => 'البريد الالكتروني هذا موجود بالفعل',
    'Mobile_Must_Be11_Digits' => 'يجب ان يتكون رقم الهاتف من 11 رقم',
    'Please_Enter_Driver_Mobile_Number' => 'من فضلك ادخل رقم الهاتف للكابتن',
    'Mobile_Number_already_exist' => 'رقم الهاتف هذا موجود بالفعل !',
    'Please_Select_Agent' => 'من فضلك اختر وكيل',
    'Please_Enter_Latitude' => 'من فضلك ادخل خط العرض',
    'Latitude_must_be_numbers' => 'يجب ان يحتوي خط العرض علي ارقام فقط',
    'Please_Enter_Longitude' => 'من فضلك ادخل خط الطول',
    'Longitude_must_be_numbers' => 'يجب ان يحتوي خط الطول علي ارقام',
    'Please_Enter_Correct_Date_Format' => 'من فضلك ادخل التاريخ صحيح',
    'Please_Enter_Driving_Licence_Expired_Date' => 'من فضلك ادخل تاريخ انتهاء رخصة القيادة',
    'Please_Enter_National_Id_Number' => 'من فضلك ادخل رقم البطاقة الشخصية',
    'National_Id_Must_Be14_Digits' => 'يجب ان يتكون الرقم القومي من 14 رقما',
    'National_Id_Must_Be_Number' => 'رقم البطاقة الشخصية يجب ان يحتوي علي ارقام',
    'Please_Enter_Driving_Licence_Number' => 'من فضلك اردخل رقم رخصة القيادة',
    'Driving_Licence_Must_Be_Number' => 'يجب ان يحتوي رقم رخصة القيادة علي ارقام',
    'Please_Enter_Correct_Date_Format' => 'من فضلك ادخل التاريخ صحيح',
    'Please_Enter_National_Id_Expired_Date' => 'من فضلك ادخل تاريخ انتهاء البطاقة الشخصية',
    'Please_Enter_Driver_Address' => 'من فضلك ادخل عنوان الكابتن',

    'please_enter_criminal_record_image_driver' => 'من فضلك ادخل صورة السجل الجنائي للكابتن',
    'Please_Choose_One_Shipping_Type' => 'من فضلك اختر على اﻷقل نوع واحد من أنواع الشحن',
    'Please_Enter_Driver_Profile_Image' => 'من فضلك ادخل الصورة الشخصية للكابتن',
    'Please_Enter_drivier_licence_image_front' => 'من فضلك ادخل صورة رخصة السائق الامامية',
    'Please_Enter_drivier_licence_image_back' => 'من فضلك ادخل صورة رخصة السائق الخلفية',
    'Please_Enter_drivier_national_id_image_front' => 'من فضلك ادخل صورة البطاقة الشخصية الامامية للكابتن',
    'Please_Enter_drivier_national_id_image_back' => 'من فضلك ادخل صورة البطاقة الشخصية الخلفية للكابتن',

    // validation error messages For Edit Driver
    'Please_Select_City' => 'من فضلك اختر المدينة',
    'Please_Enter_Driver_Profile_Image' => 'من فضلك ادخل الصورةالشخصية',
    'Please_Enter_Correct_Extenstion_jpeg_OR_png' => 'يجب ان يكون امتداد الصورة (jpeg OR png)',
    'Please_Enter_Driving_Licence_Image' => 'من فضلك ادخل صورة رخصة القيادة',
    'Please_Enter_National_Id_Image' => 'من فضلك ادخل صورة البطاقة الشخصية',

    // validation error messages For Add Driver
    'Please_Enter_Model_Name' => 'من فضلك ادخل اسم الموديل',
    'Please_Enter_Plate_Number' => 'من فضلك ادخل رقم اللوحة',
    'Please_Enter_Chassi_Number' => 'من فضلك ادخل رقم الشاسيه',
    'Please_Enter_Vehicle_Type' => 'من فضلك ادخل نوع المركبة',
    'Please_Enter_Year' => 'من فضلك ادخل السنة',
    'Please_Enter_Color' => 'من فضلك ادخل اللون',
    'Please_Enter_Model_Type' => 'من فضلك ادخل نوع الموديل',
    'Please_Enter_Driver' => 'من فضلك ادخل الكابتن',
    'Pleas_Enter_License_Start_Date' => 'من فضلك ادخل تاريخ بداية الرخصة',
    'Pleas_Enter_License_End_Date' => 'من فضلك ادخل تاريخ انتهاء الرخصة',
    'Pleas_Chosse_Vehicle_Image' => 'من فضلك ادخل صورة المركبة',
    'Pleas_Chosse_License_Image_Front' => 'من فضلك ادخل صورة الرخصة الامامية',
    'Pleas_Chosse_License_Image_Back' => 'من فضلك ادخل صورة الرخصة الخلفية',
    'Please_Choose_If_Driver_Has_Vehicle_or_No' => 'من فضلك اختر ما اذا كان الكابتن يمتلك مركبة او لا',
    'Please_Enter_Profit' => 'من فضلك ادخل معدل الربح',
    'Please_Enter_Basic_Salary' => 'من فضلك ادخل الراتب الاساسي',
    'Please_Enter_Bonus_Of_Delivery' => 'من فضلك ادخل بونص التوصيل',
    'Please_Enter_Pickup_Price' => 'من فضلك ادخل ثمن تجميع البيكب',
    'Please_Enter_Bonus_Of_Pickup_For_One_Order' => 'من فضلك ادخل بونص تجميع الشحنة الواحدة',

    // validation error messages For Add Wallet
    'Please_Enter_the_value' => 'من فضلك ادخل القيمة',

    // validation error messages For Add Shipment Tools
    'Please_Enter_the_English_Tool_Title' => 'من فضلك ادخل عنوان الاداة بالانجليزي',
    'Please_Enter_the_Arabic_Tool_Title' => 'من فضلك ادخل عنوان الاداة بالعربي',
    'Please_Enter_Price_Tool' => 'من فضللك ادخل سعر الاداة',
    'Please_Enter_Status_Tool' => 'من فضلك ادخل حالة الاداة',
    'Please_Chosse_Agent_Name' => 'من فضلك اختر اسم الوكيل',


    // validation error messages For Add Shipment Destination
    'Please_Choose_Start_Destination' => 'من فضلك اختر بداية الواجهة',
    'Please_Choose_End_Destination' => 'من فضلك اختر نهاية الواجهة',
    'Please_Enter_The_Destination_Cost' => 'من فضلك ادخل التكلفة',
    'Please_Enter_The_Destination_Overweight_Cost' => 'من فضلك ادخل تكلفة الوزن الزائد',

    // validation error messages For Add Store
    'Please_Enter_Store_Name' => 'من فضلك ادخل اسم المخزن',
    'Please_Chosse_The_Government' => 'من فضلك اختر المحافظة',
    'Please_Chosse_The_Status' => 'من فضلك اختر الحالة',
    'Please_Enter_The_Address' => 'من فضلك ادخل العنوان',
    'Please_Enter_Responsible' => 'من فضلك ادخل اسم االمسؤول',

    // validation error messages For Add Stock
    'Please_Enter_Stock_Name' => 'من فضلك ادخل اسم المخزن',
    'Please_Enter_Quantity' => 'من فضلك ادخل الكمية',
    'Please_Chosse_The_Main_Store' => 'من فضلك اختر المخزن الرئيسي',
    'Please_Chosse_The_Corporate' => 'من فضلك اختر الشركة',
    'Please_Enter_The_Description' => 'من فضلك ادخل الوصف',

    // validation error messages For Add promo Code
    'Please_Enter_Discount' => 'من فضلك ادخل الخصم',
    'Please_Enter_No_of_tries' => 'من فضلك ادخل رقم المحاولة',
    'Please_Enter_Code' => 'من فضلك ادخل الرمز',

    // validation error messages For Add Employee
    'Please_Enter_Employee_Name' => 'من فضلك ادخل اسم الموظف',
    'Please_Enter_Employee_Email' => 'من فضلك ادخل البريد الالكتروني',
    'Please_Enter_Mobile_Number' => 'من فضلك ادخل رقم الهاتف',
    'Please_Enter_Employee_Salary' => 'من فضلك ادخل المرتب',
    'Please_Enter_Job_Title' => 'من فضلك ادخل المسمي الوظيفي',
    'Please_Enter_Avalid_Number' => 'من فضلك ادخل رقما صالحا',
    'Please_Enter_Employee_Correct_Email' => 'من فضلك اجل البريد الالكتروني صحيح للموظف',

    // validation error messages For Add Permission
    'Please_Enter_Permission_Name' => 'من فضلك ادخل اسم الصلاحية',
    'Please_Chosse_Type_Permission' => 'من فضلك اختر نوع الصلاحية',

    // validation error messages For Add Permission Type
    'Please_Enter_Permission_Type' => 'من فضلك ادخل نوع الصلاحية',

    // validation error messages For Add Admin Role
    'Please_Enter_Role_Name' => 'من فضلك ادخل اسم الدور',
    'Please_Selected_Permission_Administrator' => 'من فضلك قم بتحديد صلاحيات المشرف',

    // validation error messages For Add Activity Log Type
    'Please_Enter_Name_Activity_Log_Type' => 'من فضلك ادخل اسم نوع سجل النشاط',

    // validation error messages For Add Android Version
    'Please_Enter_Name_Android_Version' => 'من فضلك ادخل اسم اصدار الاندرويد',
    'Please_Chosse_Platform_Version' => 'من فضلك اختر منصة الاصدار',
    'Please_Chosse_Status_Version' => 'من فضلك اختر حالة الاصدار',

    // validation error messages For Add Admin
    'Please_Enter_Admin_Name' => 'من فضلك ادخل اسم المشرف',
    'Confirm_password_is_wrong' => 'تأكيد كلمة المرور غير صحيحة',
    'Please_Enter_Admin_Email' => 'من فضلك ادخل البريد الالكتروني للمشرف',
    'Please_Enter_User_Correct_E-mail' => 'من فضلك ادخل بريد الكتروني صحيح',
    'Please_Enter_User_Password' => 'من فضلك ادخل كلمة المرور',
    'Password_must_be_more_than_5_character' => 'كلمة المرور يجب ان تكون اكثر من 5 ارقام',
    'Please_Choose_Admin_Role' => 'من فضلك اختر دور المشرف',

    // validation error messages For Add Order Types
    'Please_Enter_Type_English_Name' => 'من فضلك ادخل اسم النوع بالانجليزية',
    'Please_Enter_Type_Arabic_Name' => 'من فضلك ادخل اسم النوع بالعربية',
    'Please_Enter_Price_Type_Order' => 'من فضلك ادخل سعر نوع الطلب',
    'Please_Chosse_Status_Type_Order' => 'من فضلك اختر حالة نوع الطلب',

    // validation error messages For Sacn Order
    'no_changes_detected' => 'لا يوجد تغير حدث',


    // validation error messages For Add Resources
    'Please_Enter_Resource_Name' => 'من فضلك ادخل اسم المصدر',
    'Please_Enter_Resource_Email' => 'من فضلك ادخل البريد الالكتروني للمصدر',
    'Please_Enter_Started_At' => 'من فضلك ادخل تاريخ البدأ',
    'Please_Enter_Renew_At' => 'من فضلك ادخل تاريخ التجديد',
    'Please_Enter_The_Cost' => 'من فضلك ادخل سعر التكلفة',
    'Please_Enter_Website_Link' => 'من فضلك ادخل رابط الموقع',
    'Please_Enter_Username' => 'من فضلك ادخل اسم المستخدم',
    'Please_Enter_Password' => 'من فضلك ادخل كلمة المرور',
    'Please_enter_avalid_date' => 'من فضلك ادخل تاريخ صحيح',
    'Please_enter_avalid_price' => 'من فضلك ادخل مبلغ صحيح',
    'Please_Enter_Correct_Website_Link' => 'من فضلك ادخل رابط الموقع صحيح',

    // validation error messages For Add Vehicles Model
    'Please_Enter_Model_Type_Name' => 'من فضلك ادخل اسم طراز المركبة',

    // validation error messages For Add Vehicles Type
    'Please_Enter_Vehicel_Price' => 'من فضلك ادخل تكلفة المركبة',

    // validation error messages For Add Vehicles Color
    'Please_Enter_Color_Name' => 'من فضلك ادخل اسم اللون',

    // validation error messages For Add Payment Method
    'Please_Enter_Title_of_Payment_Method' => 'من فضلك ادخل عنوان طريقة الدفع',

    // validation error messages For Add countries
    'Please_Enter_Country_Name' => 'من فضلك ادخل اسم البلد',
    'Please_Enter_Code_of_Country' => 'من فضلك ادخل كود البلد علي سبيل المثال (eg)',

    // validation error messages For Add countries Governorates
    'Please_Enter_City_Name' => 'من فضلك ادخل اسم المدينة بالعربية',
    'Please_Choose_a_Governorate_for_the_City' => 'من فضلك اختر المافظة الخاصة بالمدينة',
    'Please_Enter_Governorate_English_Name' => 'من فضلك ادخل اسم المحافظة بالانجليزية',
    'Please_Enter_Governorate_Arabic_Name' => 'من فضلك ادخل اسم المحافظة بالعربية',
    'Please_Enter_English_City_Name' => 'من فضلك ادخل اسم المدينة بالانجليزية',
    'Please_Enter_City_Status' => 'من فضلك اختر حالة المدينة',
    'Please_Enter_Governorate_Status' => 'من فضلك اختر حالة المحافظة',

    // validation error messages For Edit Collection Orders
    'Corporate_already_exist' => 'هذه الشركة موجودة بالفعل',
    'Please_Enter_field' => 'من فضلك ادخل الحقل',
    'Something_went_Wrong_please_try_again_later' => 'هناك خطأ ما .. يرجى المحاولة فى وقت لاحق',

    // validation error messages For Add Price List
    'Please_Enter_The_Target' => 'من فضلك ادخل الهدف',
    'Please_Enter_discount_percentage' => 'من فضلك ادخل نسبة الخصم',
    'Please_Enter_avalid_Target' => 'من فضلك ادخل الهدف صحيحا',
    'Please_Enter_avalid_discount_percentage' => 'من فضلك ادخل نسبة الخصم صحيحا',

    // validation error messages For Add Documentations
    'Please_Choose_Documentation_Type' => 'من فضلك اختر نوع الوثيقة',
    'Please_Enter_Documentation_Title' => 'من فضلك ادخل عنوان الوثيقة',
    'Please_Enter_Documentation_Slug' => 'من فضلك ادخل رابط الوثيقة',
    'Please_Enter_Documentation_Description' => 'من فضلك ادخل وصف الوثيقة',

    // validation error messages For Add Contact
    'Please_Enter_The_Name' => 'من فضلك ادخل الاسم',
    'Please_Enter_Email_Address' => 'من فضلك ادخل البريد الالكتروني',
    'Please_Enter_Mobile_Number' => 'من فضلك ادخل رقم الهاتف',
    'Please_Enter_Message_Title' => 'من فضلك ادخل عنوان الرسالة',
    'Please_Enter_The_Message_Content' => 'من فضلك ادخل محتوي الرسالة',
    'Please_Enter_Correct_Number' => 'من فضلك ادخال الرقم صالحا',
    'Please_Enter_Notifaction_Type' => 'من فضلك ادخل نوع الاشعار',

    //////////////////////////////////////////////////////////////////////////// Controllers /////////////////////////////////////////////////////////////////////////

    // AdminController
    'Admin_is_added_successfully' => 'تم اضافةالمشرف بنجاح',
    'user_is_unblocked_successfully' => 'تم الغاء حظر العضو بنجاح.',
    'User_is_blocked_Successfully' => 'تم حظر العضو بنجاح',
    'User_is_updated_successfully' => 'تم التعديل بنجاح',
    'User_deleted_successfully' => 'تم الحذف بنجاح',

    // AdminRoleController
    'Role_Admin_saved_successfully' => 'تم حفظ دور المشرف بنجاح.',
    'Role_Admin_Updated_successfully' => 'تم تعديل دور المشرف بنجاح',
    'Role_Admin_deleted_successfully' => 'تم الحذف بنجاح',

    // AgentController
    'Agent_saved_successfully' => 'تم اضافة الوكيل بنجاح.',
    'Agent_Updated_successfully' => 'تم تعديل الوكيل بنجاح',
    'Agent_does_not_deleted_May_b_have_drivers_and_orders' => 'لا يمكن حذف الوكيل .. ربما لديه سائقين او طلبات',
    'Agent_deleted_successfully' => 'تم حذف الوكيل بنجاح',
    'Agent_does_not_exist' => 'الوكيل غير موجود',

    // AgentInvoiceController
    'AgentInvoice_saved_successfully' => 'تم حفظ فاتورة الوكيل بنجاح',
    'AgentInvoice_Updated_successfully' => 'تم تعديل فاتورة الوكيل بنجاح',
    'AgentInvoice_deleted_successfully' => 'تم حذف فاتورة الوكيل بنجاح',

    // CityController
    'City_Added_successfully' => 'تم اضافة المدينة بنجاح',
    'City_Updated_successfully' => 'تم تعديل المدينة بنجاح',
    'City_deleted_successfully' => 'تم حذف المدينة بنجاح',

    // CollectionOrderController
    'Order_Forward_saved_successfully' => 'تم توجيه الطلب بنجاح',
    'Order_Forward_not_Saved_Something_Error' => 'لم يتم توجيه الطلب .. حدث خطأ ما',
    'Insert_Record_successfully' => 'تم ادراج السجل بنجاح',
    'File_Does_not_Exist_Please_try_to_upload_it_again' => 'الملف غير موجود .. يرجي محاولة التحميل مرة اخري',
    'Please_Enter_Correct_File' => 'برجاء ادخال ملف صحيح',
    'Collection_Orders_saved_successfully' => 'تم حفظ مجموعة الطلبات بنجاح',
    'Collection_Orders_Updated_successfully' => 'تم تعديل مجموعة الطلبات بنجاح',
    'Collection_Order_deleted_successfully' => 'تم حذف مجموعة الطلبات بنجاح',

    // ColorController
    'Color_saved_successfully' => 'تم حفظ اللون بنجاح',
    'Color_Updated_successfully' => 'تم تعديل اللون بنجاح',
    'Color_deleted_successfully' => 'تم حذف اللون بنجاح',

    // ContactController
    'Contact_saved_successfully' => 'تم حفظ الاتصال بنجاح',
    'Contact_Updated_successfully' => 'تم تعديل الاتصال بنجاح',
    'Contact_deleted_successfully' => 'تمحذف الاتصال بنجاح',

    // CorprateController
    'Corporate_saved_successfully' => 'تم اضافة الشركة بنجاح',
    'Corporate_Updated_successfully' => 'تم تعديل الشركة بنجاح',
    'Corprate_deleted_successfully' => 'تم حذف الشركة بنجاح',
    'Can_not_delete_Corporate_because_of_Orders_dependanices' => 'لا يمكن حذف الشركة .. لوجود طلبات لديها',

    // CountryController
    'Country_saved_successfully' => 'تم حفظ البلد بنجاح',
    'Country_Updated_successfully' => 'تم تعديل البلد بنجاح',
    'Country_deleted_successfully' => 'تم حذف البلد بنجاح',

    // CustomerController
    'we_sent_you_a_verification_message_please_check_your_email_and_verify_it' => 'لقد قمنا بارسال تحقق لك .. يرجي التحقق من بريدك الالكتروني',
    'Something_Error' => 'هناك خطأ ما',
    'Wrong_Mobile_or_Password' => 'خطأ في رقم الهاتف اوكلمة المرور',
    'Wrong_Email_or_Password' => 'خطأ في البريد الالكتروني اوكلمة المرور',
    'Email_or_password_does_not_exist' => 'البريد الالكتروني او كلمة المرور غير موجود',
    'Send_a_message_to_change_your_password' => 'إرسال رسالة لتغيير كلمة المرور الخاصة بك',
    'Wrong_Email' => 'خطأ في البريد الالكتروني',
    'You_are_already_Logged' => 'انت مسجل بالفعل',
    'Validation_Link_is_wrong_Resent_E-mail_again' => 'رابط التحقق خطأ ، إعادة إرسال البريد الإلكتروني مرة أخرى',
    'Email_does_not_exist' => 'البريد الالكتروني غير موجود',
    'Succeeded_your_password_has_changed_Try_login_Now' => 'تم تغيير كلمة المرور بنجاح .. حاول تسجيل الدخول الان',
    'Reset_Passworde_is_invalid_Something_is_wrong_Try_Again' => 'اعادة تعيين كلمة المرور غير صحيحة .. هناك خطأ ما .. حاول مرة اخري',
    'Customer_saved_successfully' => 'تم حفظ العميل بنجاح',
    'Customer_Updated_successfully' => 'تم تعديل العميل بنجاح',
    'Customer_does_not_exist' => 'العميل غير موجود',
    'Customer_deleted_successfully' => 'تم حذف العميل بنجاح',
    'problem_deleted_successfully' => 'تم حذف مشكلة التسليم بنجاح',
    'Customer_not_deleted_because_orders_not_empty_and_collectionorders_not_empty' => 'لن يتم حذف العميل .. نظرا لوجود مجموعة من الطلبات',
    'Invalid_Format' => 'تنسيق غير صحيح',
    'Collection_that_you_try_to_open_saved_before' => 'المجموعة التي تحاول فتحها تم حفظها من قبل',
    'please_try_to_login' => 'من فضلك يرجي محاولة تسجيل الدخول',
    'please_try_again_something_error' => 'من فضلك اعد المحاولة .. هناك خطأ ما',

    // DriverController
    'Driver_saved_successfully' => 'تم حفظ الكابتن بنجاح',
    'Please_Enter_Driver_Profile' => 'من فضلك ادخل صورة الكابتن',
    'Please_Enter_Criminal_Driver_Image' => 'من فضلك ادخل الصورة الجنائية للكابتن',
    'Please_Enter_Driving_Licence_Image_Back' => 'من فضلك ادخل صورة رخصة القيادة الخلفية',
    'Please_Enter_Driving_Licence_Image_Front' => 'من فضلك ادخل صورة رخصة القيادة الامامية',
    'Please_Enter_National_Id_Image_Front' => 'من فضلك ادخل صورة البطاقة الشخصية الامامية',
    'Please_Enter_National_Id_Image_Back' => 'من فضلك ادخل صورة البطاقة الشخصية الخلفية',
    'Driver_Updated_successfully' => 'تم تعديل الكابتن بنجاح',
    'Driver_deleted_successfully' => 'تم حذف الكابتن بنجاح',
    'Driver_not_delete_Delete_trucks_or_his_orders_before' => 'لا يمكن حذف الكابتن .. الرجاء حذف الطلبات اولا',

    // EmployeeController
    'Employee_saved_successfully' => 'تم حفظ الموظف بنجاح',
    'Employee_updated_successfully' => 'تم تعديل الموظف بنجاح',
    'Employee_deleted_successfully' => 'تم حذف الموظف بنجاح',

    // governorateController
    'Governorate_Added_successfully' => 'تم اضافة المحافظة بنجاح',
    'Governorate_Updated_successfully' => 'تم تعديل المحافظة بنجاح',
    'Theres_a_Shipment_Line_already_setup_for_this_Governorate_try_to_delete_it_first' => 'There\'s a Shipment Line already set-up for this Governorate, try to delete it first!',
    'Governorate_deleted_successfully' => 'تم حذف المحافظة بنجاح',

    // governoratePriceController
    'Destination_Added_successfully' => 'تم اضافة الواجهه بنجاح',
    'Destination_Updated_successfully' => 'تم تعديل الواجهه بنجاح',
    'Destination_deleted_successfully' => 'تم حذف الواجهه بنجاح',
    'Can_not_delete_Destination_because_of_Order_Dependency' => 'لا يمكن حذف الواجهه بسبب الطلبات التابعة لها',

    // InvoiceController
    'Please_Eneter_Correct_Phone_Number' => 'من فضلك ادخل رقمالهاتف صحيح',
    'We_Can_not_send_SMS_Something_Error' => 'لا يمكن ارسال الرسالة .. هناك خطأ ما',
    'Phone_Number_is_Wrong' => 'رقم الهاتف غير صحيح',
    'Invalid_Format' => 'تنسيق غير صحيح',
    'Wrong_Code_Something_Error' => 'رمز غير صحيح .. هناك خطأ ما',
    'No_Order_Sent_You_must_choose_Order_To_transfer_it' => 'لم يتم ارسال الطلب .. يجب عليك اختيار الطلب لنقله',
    'Old_Wallet_Error_Not_Found' => 'خطأ في المحفظة القديمة .. غير موجودة',
    'Done_Orders_transfer_successfully_please_reload_this_page' => 'حسنا . تم نقل الطلب بنجاح .. يرجي اعادة تحميل الصفحة',
    'This_User_does_not_have_wallet' => 'هذا الشخص ليس لديه اي محفظة',
    'The_Wallet_of_This_User_is_closed' => 'محفظة هذا الشخص مغلقة',
    'Invoice_Payment' => 'دفع الفاتورة',
    'Ufelix_Deserved_From_Invoice_Payment' => 'المستحق ليوفليكس من دفع الفاتورة',
    'Order_Price_From_Invoice_Payment' => 'سعر الطلب مندفع الفاتورة',
    'Done_This_Invoice_closed_successfully_please_reload_this_page' => 'حسنا . تم اغلاق الفاتورة بنجاح .. يرجي اعادة تحميل الصفحة',
    'This_Inovice_does_not_exist' => 'هذه الفاتورة غير موجودة',
    'Invoice_saved_successfully' => 'تمحفظ الفاتورة بنجاح',
    'Invoice_Updated_successfully' => 'تم تعديل الفاتورة بنجاح',
    'Invoice_deleted_successfully' => 'تم حذف الفاتورة بنجاح',
    'Return_to_Default_Saved_successfully' => 'العودة إلى الافتراضي',
    'This_invoice_does_not_exist' => 'هذخ الفاتورة غير موجودة',
    'Price_List_Saved_successfully' => 'تم حفظ قائمة الاسعار بنجاح',
    'No_Pricing_Sent_You_must_predefine_Order_Price' => 'لم يتم إرسال أي سعر يجب عليك تحديد السعر المطلوب',
    'Percentage_Saved_successfully' => 'تم حفظ النسبة المؤية بنجاح',
    'Invoice_Order_count' => 'لا بد من إختيار عدد أقل من الطلبات',


    // LanguageController
    'Language_saved_successfully' => 'تم حفظ اللغةبنجاح',
    'Language_Updated_successfully' => 'تم تعديل اللغة بنجاح',
    'Language_deleted_successfully' => 'تم حذف اللغة بنجاح',

    // ModelTypeController
    'Model_Type_saved_successfully' => 'تم حفظ نوع الطراز بنجاح',
    'Model_Type_Updated_successfully' => 'تم تعديل نوع الطراز بنجاح',
    'Model_Type_deleted_successfully' => 'تم حذف نوع الطراز بنجاح',

    // NotificationController
    'Notification_saved_successfully' => 'تم حفظ الاشعار بنجاح',
    'Notification_Updated_successfully' => 'تم تعديل الاشعار بنجاح',
    'Notification_deleted_successfully' => 'تم حذف الاشعار بنجاح',
    'Notification_Type_created_successfully' => 'تم انشاء نوع الاشعار بنجاح',
    'Notification_Type_updated_successfully' => 'تم تعديل نوع الاشعار بنجاح',
    'Notification_Type_Deleted_successfully' => 'تم حذف نوع الاشعار بنجاح',

    // OrderController
    'Order_Routed_saved_successfully' => 'تمحفظ مسار الطلب بنجاح',
    'Order_does_not_exist' => 'الطلب غير موجود',
    'Order_saved_successfully' => 'تم حفظ الطلب بنجاح',
    'Order_Canceled_after_Receiving' => 'طلب ملغي بعد الاستلام',
    'Somthing_went_Wrong_Please_try_again' => 'حدث خطأ ما .. يرجي اعادة المحاولة',
    'Collection_that_you_try_to_open_saved_before' => 'المجموعة التي تحاول فتحها تم حفظها من قبل',
    'Collection_saved_successfully' => 'تم حفظ المجموعة بنجاح',
    'Collection_not_Saved_Something_Error' => 'لم يتم حفظ المجموعة .. حدث خطأ ما',
    'Collection_that_you_try_to_save_does_not_have_orders' => 'المجموعة التي تحاول حفظها لا تحتوي علي طلبات',
    'Collection_that_you_try_to_save_does_not_exist' => 'المجموعة التي تحاول حفظها غير موجودة',
    'Order_Deleted_successfully' => 'تم حذف الطلب بنجاح',
    'Order_Updated_successfully' => 'تم تعديل الطلب بنجاح',
    'Cannot_Be_Delete_This_Order' => 'لا يمكن حذف هذا الطلب',
    'No_Any_Selected_Orders' => 'لم يتم تحديد اي من الطلبات',
    'Please_Select_Any_Order' => 'من فضلك قم بتحديد اي من الطلبات',
    'Order_Cancel_successfully' => 'تم الغاء الطلب بنجاح',
    'action_back_successfully' => 'تم تغير الحالة الشحنه بنجاح',
    'action_back_Error' => 'حدث خطا',

    // OrderLogController
    'OrderLog_saved_successfully' => 'تم حفظ سجل الطلب بنجاح',
    'OrderLog_Updated_successfully' => 'تم تعديل سجل الطلب بنجاح',
    'OrderLog_deleted_successfully' => 'تم حذف سجل الطلب بنجاح',

    // OrderViewController
    'OrderView_saved_successfully' => 'تم حفظ عرض الطلب بنجاح',
    'OrderView_Updated_successfully' => 'تم تعديل عرض الطلب بنجاح',
    'OrderView_deleted_successfully' => 'تم حذف عرض الطلب بنجاح',

    // PaymentMethodController
    'PaymentMethod_saved_successfully' => 'تم حفظ طريقة الدفع بنجاح',
    'PaymentMethod_Updated_successfully' => 'تم تعديل طريقة الدفع بنجاح',
    'PaymentMethod_deleted_successfully' => 'تم حذف طريقة الدفع بنجاح',

    // PermissionController
    'Permission_saved_successfully' => 'تم حفظ الصلاحية بنجاح',
    'Permission_Updated_successfully' => 'تم تعديل الصلاحية بنجاح',
    'Permission_Deleted_successfully' => 'تم حذف الصلاحية بنجاح',

    // PriceListController
    'Price_List_saved_successfully' => 'تم حفظ قائمة الاسعار',
    'Price_List_updated_successfully' => 'تم تعديل قائمة الاسعار',
    'Price_List_deleted_successfully' => 'تم حذف قائمة الاسعار',

    // PromoCodeController
    'PromoCode_saved_successfully' => 'تم حفظ الرمز الترويجي بنجاح',
    'PromoCode_Updated_successfully' => 'تم تعديل الرمز الترويجي بنجاح',
    'PromoCode_deleted_successfully' => 'تم حذف الرمز الترويجي بنجاح',

    // ResourceController
    'Resource_saved_successfully' => 'تم اضافة المصدر بنجاح',
    'Resource_updated_successfully' => 'تم تعديل المصدر بنجاح',
    'Resource_deleted_successfully' => 'تم حذف المصدر بنجاح',

    // SettingController
    'Data_created_successfully' => 'تم انشاء البيانات بنجاح',
    'Data_updated_successfully' => 'تم تعديل البيانات بنجاح',
    'Data_deleted_successfully' => 'تم حذف البيانات بنجاح',

    // ShipmentToolController
    'ShipmentTool_saved_successfully' => 'تم حفظ اداة الشحن بنجاح',
    'ShipmentTool_Updated_successfully' => 'تم تعديل اداة الشحن بنجاح',
    'ShipmentTool_deleted_successfully' => 'تم حذف اداة الشحن بنجاح',

    // ShipmentToolOrderController
    'ShipmentToolOrder_saved_successfully' => 'تم حفظ اداة شحن الطلب بنجاح',
    'ShipmentToolOrder_Updated_successfully' => 'تم تعديل اداةشحن الطلب بنجاح',
    'ShipmentToolOrder_deleted_successfully' => 'تم حذف اداة شحن الطلب بنجاح',

    // SizeController
    'Size_saved_successfully' => 'تم حفظ الحجم بنجاح',
    'Size_Updated_successfully' => 'تم تعديل الحجم بنجاح',
    'Size_deleted_successfully' => 'تم حذف الحجم بنجاح',

    // StockController
    'Stock_created_successfully' => 'تم اضافة المخزن الفرعي بنجاح',
    'Stock_Updated_successfully' => 'تم تعديل المخزن الفرعي بنجاح',
    'Stock_deleted_successfully' => 'تم حذف المخزن الفرعي بنجاح',

    // StoreController
    'Store_created_successfully' => 'تم حفظ المخزن بنجاح',
    'Store_Updated_successfully' => 'تم تعديل المخزن بنجاح',
    'Store_deleted_successfully' => 'تم حذف المخزن بنجاح',

    // TruckController
    'Truck_saved_successfully' => 'تم حفظ الشاحنة بنجاح',
    'Please_Enter_Veichle_Image' => 'من فضلك ادخل صورة الشاحنة',
    'Please_Enter_License_Image_Front' => 'من فضلك ادخل صورة الرخصة الامامية',
    'Please_Enter_License_Image_Back' => 'ادخل صورة الرخصة الخلفية',
    'Truck_Updated_successfully' => 'تم تعديل الشاحنة بنجاح',
    'Truck_deleted_successfully' => 'تم حذف الشاحنة بنجاح',


    // TypePermissionController
    'Type_Permission_saved_successfully' => 'تم حفظ نوع الصلاحية بنجاح',
    'Type_Permission_Updated_successfully' => 'تم تعديل نوع الصلاحية بنجاح',
    'Type_Permission_Deleted_successfully' => 'تم حذف نوع الصلاحية بنجاح',

    // UserController
    'User_added_successfully' => 'تم اضافة المستخدم بنجاح',
    'user_is_blocked_successfully' => 'يتم حظر المستخدم بنجاح',
    'You_are_blocke' => 'لقد تم حظرك',
    'Email_or_Password_is_wrong' => 'هناك خطأ في البريد الالكتروني اوكلمة المرور',
    'You_are_Not_Verify_Please_Contact_with_admin' => 'لم يتم تأكيدك .. يرجي الاتصال بالمشرف',

    // vehicleController
    'Vehicle_Type_saved_successfully' => 'تم حفظ نوع الشاحنة ',
    'Vehicle_Type_Updated_successfully' => 'تم تعديل نوع الشاحنة',
    'Vehicle_Type_deleted_successfully' => 'تم حذف نوع الشاحنة',

    // VehicleTypeController
    'Type_saved_successfully' => 'تم حفظ النوع بنجاح',
    'Type_Updated_successfully' => 'تم تعديل النوع بنجاح',
    'Type_deleted_successfully' => 'تم حذف النوع بنجاح',

    // WalletController
    'Wallet_saved_successfully' => 'تم اضافة المحفظة بنجاح',
    'Wallet_does_not_deleted_Something_error' => 'لم يتم حذف المحفظة .. هناك خطأ ما',
    'Something_error_Wallet_does_not_exist' => 'هناك خطأ ما .. المحفظة غير موجودة',
    'Wallet_Updated_successfully' => 'تم تعديل المحفظة بنجاح',
    'Wallet_does_not_deleted_Wallet_have_some_operation' => 'لم يتم حذف المحفظة .. يوجد عليها بعض العمليات',
    'Wallet_deleted_successfully' => 'تم حذف المحفظة بنجاح',
    'Money_added_To_Wallet_successfully' => 'تمت إضافة الأموال إلى المحفظة بنجاح',
    'Wallet_does_not_exist_Something_error' => 'المحفظة غير موجودة .. يوجد خطأ ما',
    'Money_withdrawal_To_Wallet_successfully' => 'تم سحب الاموال من المحفظة بنجاح',
    'Wallet_driver_not_found' => 'هذا السائق ليس موجود ضمن قائمه السائقين !',
    'Wallet_corporate_not_found' => 'هذذه الشركه غير موجوده ضمن قائمه العملاء!',
    'Wallet_agent_not_found' => 'هذا الوكيل ليس موجود ضمن قائمه الوكلاء !',

    // WeightController
    'Weight_saved_successfully' => 'تم حفظ الوزن بنجاح',
    'Weight_Updated_successfully' => 'تم تعديل الوزن بنجاح',
    'Weight_deleted_successfully' => 'تم حذف الوزن بنجاح',
    'edit_forward' => 'تعديل و توجيه',

    // Documentations
    'Docs_saved_successfully' => 'تم حفظ الوثيقة بنجاح',
    'Doc_Type_saved_successfully' => 'تم حفظ نوع الوثيقة بنجاح',
    'Docs_updated_successfully' => 'تم تعديل الوثيقة بنجاح',
    'Docs_deleted_successfully' => 'تم حذف الوثيقة بنجاح',
    'PickUp_Orders' => 'PickUp Orders',

    'PickUp_saved_successfully' => 'PickUp Saved Successfully.',
    'PickUp_updated_successfully' => 'PickUp Updated Successfully.',
    'PickUp_deleted_successfully' => 'PickUp Deleted Successfully.',
    'Ticket_saved_successfully' => 'Ticket Saved Successfully.',
    'Ticket_updated_successfully' => 'Ticket Updated Successfully.',
    'Ticket_deleted_successfully' => 'Ticket Deleted Successfully.',
    'drop_off' => 'Drop-off',
    'drop_off_text' => 'Drop-off Orders',
    'drop_off_text_pickups' => 'Drop-off Pickups',
    'confirm_drop_off' => 'Confirm Drop Off',
    'drop_off_orders' => 'Are You sure To Drop-Off This Orders ?',
    'drop_off_pickups' => 'Are You sure To Drop-Off This Pickups ?',
    'drop_off_package' => 'Your Package is dropped off successfully',

    'Order_Packup_not_Saved_Something_Error' => 'PickUp does not belong to the same Destination or same Source',
    'Pickup_Numper' => 'Pickup Number',
    'Driver' => 'Captain',
    'Agent' => 'Agent',
    'Status' => 'Status',
    'Pickup_No' => 'Pickup No',
    'Pickup_is_not_Accepted_yet' => 'Pickup does not added to Captain yet ..',
    'Packup_orders' => 'Pickup Orders',
    'Forward_pickup' => 'Forward Pickup',
    'Forward_Pickup_To_Another_Agent' => 'Forward Pickup To Another Agent',
    'Forward_Pickup_cancel' => 'Something wrong .. Forward Pickup not completed',
    'Forward_Pickup_saved_successfully' => 'Forward Pickup saved successfully',
    'Pickup_deleted_successfully' => 'Pickup deleted successfully',
    'captain_received_date' => 'Collect Date',
    'captain_received_time' => 'Collect Time',
    'Forward_at' => 'Forward Date',
    'Edit_PickUp' => 'Edit PickUp',
    'add_filter' => 'Add Filter',
    'create_pickup' => 'إنشاء بيكب',
    'collection' => 'Collection',
    'dropped' => 'Dropped',
    'collected' => 'Collected',
    'logo' => 'Logo',
    'ContractNote' => 'Contract Note',
    'ContractForm' => 'Contract PDF OR Image',
    'add_pickup_price_list' => 'New Pickup Price List',
    'add_pickup_store' => 'New Pickup Center',
    'pickup_store' => 'Pickup Center',
    'pickup_price_list' => 'Pickup Price List',
    'the_bonous' => 'Bonus per order',
    'pickup_default' => 'Set As PickUp Default',
    'dropped_pickup_store' => 'Dropped Pickup Center',
    'Add_Area_Title' => 'Add Area To Agent',
    'Add_Area' => 'Add Area',
    'Agent_Area_saved_successfully' => ' Area Added To Agent successfully',
    'SomeThing_Error' => ' SomeThing Error',
    'Agent_Area' => ' Agent Covered Area List',
    'delete_Agent_Area_saved_successfully' => ' Area Deleted To Agent successfully',


    'list_receivers' => 'List Receivers',
    'Must_Choose_Orders_To_Print' => 'Must Choose Orders To Print',
    'Must_Choose_Orders' => 'Must Choose Orders',
    'Must_Choose_Orders_To_Action_Back' => 'يجب اختيار الشحنه لتغير الحاله',

    'why_us' => 'Why Us',
    'services' => 'Servics',
    'website' => 'Website',

    'Projects' => 'Projects',
    'Customers' => 'Customers',
    'Services' => 'Services',
    'Posts' => 'Posts',
    'ERRORS' => 'ERRORS',


    'Post' => 'Posts',

    'Project' => 'Projects',
    'Service' => 'Services',
    'the_Post' => 'Post',
    'the_Service' => 'Service',
    'the_Project' => 'Project',
    'Admin' => 'Admins',
    'waiting' => 'Waiting',

    'title_ar' => 'Title Arabic',
    'title_en' => 'Title English',
    'small_description_ar' => 'Small Description Arabic',
    'small_description_en' => 'Small Description English',
    'description_ar' => 'Description Arabic',
    'description_en' => 'Description English',
    'Image' => 'Image',
    'Publish' => 'Publish',
    'slider' => 'Slider',
    'accept_request' => 'Accept Request',
    'Invoice' => 'Invoice',

    'choose_ship_line' => 'Choose Ship Line',
    'ship_line' => 'Ship Line',
    'between_governments' => 'Between Governments',
    'internal_government' => 'Internal Government',
    'internal_city' => 'Internal City',
    'start_government' => 'Start Government',
    'destination_government' => 'Destination Government',

    'totalbounus' => 'Total Bonus',
    'cancel_pickup' => 'إلغاء البيكب',
    'delete_Pickup_saved_successfully' => 'Delete Pickup successfully',
    'Update_Pickup_saved_successfully' => 'Update Pickup successfully',

    'available_quantity' => 'Available Quantity',
    'used_quantity' => 'Used Quantity',
    'stock_quantity' => 'Please choose quantity for selected stock',
    'not_verified' => 'غير مفعل',
    'verified' => 'مفعل',
    'Orderview' => 'عرض الشحنة',
    'completed' => 'اكتمل',
    'under_processing' => 'تحت الإنشاء',

    'pay_by_captain' => 'الدفع عن طريق الكابتن',
    'filter_orders' => 'تصفية الشحنات',
    'paid' => 'مدفوع',
    'not_paid' => 'غير مدفوع',
    'view_transactions' => 'عرض المعاملات',
    'Wallet_Transactions' => 'سجل المعاملات',

    'shipping_type' => 'نوع الشحن',
    'n_shipping' => 'شحن عادي',
    'rt_shipping' => 'شحن سريع',
    'rt_b2c_shipping' => 'شحن سريع شركات',
    'rt_c2c_shipping' => 'شحن سريع أفراد',
    'Please_Enter_Corporate_Order_Type' => 'من فضلك ادخل نوع الطلب للشركة',
    'Please_Enter_Corporate_Shipping_Type' => 'من فضلك ادخل نوع الشحن للشركة',
    'receiver' => 'المستلم',
    'client' => 'العميل',
    'warehouse' => 'المكتب الرئيسي',
    'forward_with_fees' => 'توجيه بالتكلفة',
    'forward_without_fees' => 'توجية بلا تكلفة',
    'choose_driver' => 'اختر كابتن',
    'fees_type' => 'اختر نوع حساب التكلفة',
    'remove_from_old_invoices' => 'حذف من الفواتير السابقة',
    'Recall_order' => 'إسترجاع الشحنة',
    'corporate_number' => 'رقم الشركة',
    'corporate_username' => 'مستخدم الشركة',
    'special_action' => 'إجراء خاص',
    'driver_name' => 'اسم الكابتن',
    'driver_number' => 'رقم الكابتن',
    'invoice_number' => 'رقم الفاتورة',
    'Total' => 'الإجمالي',
    'add_discount' => 'اضف خصم',
    'pickup_cost' => 'تكلفة البيكب',
    'final_deserved' => 'الإستحقاق النهائي',
    'total_collect' => 'الإجمالي',
    'bonus_delivery' => 'بونص التوصيل',
    'total_pickup_cost' => 'التكلفة الكلية للبيكب',
    'total_captain_deserved' => 'الإستحقاق النهائي للكابتن',
    'Pickups_List' => 'قائمة البيكب',

    'captain' => 'الكابتن',
    'pickup_no' => 'رقم البيكب',
    'orders_in_pickup' => 'عدد الشحنات فى البيكب',
    'Created_Date' => 'تاريخ الإنشاء',
    'Received_Date' => 'تاريخ الإستلام',
    'bonus_for_pickup' => 'بونص التوصيل للشحنة الواحدة',
    'pickup_price' => 'سعر البيكب',
    'total_bonus' => 'البونص الإجمالي',
    'transfer_method' => 'طريقة التحويل',
    'choose_transfer_method' => 'اختر طريقة التحويل',
    'bank_name' => 'اسم البنك',
    'bank_account' => 'رقم الحساب',
    'bank' => 'البنك',
    'mobicash' => 'Mobicash',
    'cash_with_captain' => 'النقدية مع الكابتن',
    'cash_in_office' => 'النقدية في المكتب',
    'headoffice' => 'المكتب الرئيسي',
    'delay' => 'تأجيل',
    'delay_orders' => 'تأجيل الشحنات',
    'choose_another_day' => 'إختر يوم أخر',
    'delay_orders_successfully' => 'تم تأجيل الشحنات بنجاح',
    'order_delayed' => 'تم تأجيل الشحنة الي يوم',
    'delivery_problems' => 'مشاكل التسليم',
    'deliver' => 'تسليم',
    'Confirm' => 'تأكيد',
    'Cancel' => 'إلغاء',
    'Ok' => 'موافق',
    'delivery_orders_confirm' => 'هل تريد تحويل الشحنات الى حالة التسليم؟',
    'orders_delivered' => 'تم تسليم الشحنات',
    'orders_recalled' => 'تم إسترجاع الشحنات',
    'orders_cancelled' => 'تم إلغاء الشحنات',
    'Receive' => 'إستلام',
    'receive_orders_confirm' => 'هل تريد تحويل الشحنات الى حالة الإستلام؟',
    'recall_orders_confirm' => 'هل تريد تحويل الشحنات الى حالة المرتجع؟',
    'cancel_orders_confirm' => 'هل تريد تحويل الشحنات الى حالة الإلغاء؟',
    'dropped_orders_confirm' => 'Do you want to transfer orders to Dropped status?',
    'moved_to_orders_confirm' => 'هل تريد نقل الشحنه للمخزن؟',
    'orders_received' => 'تم إستلام الشحنات',
    'note' => 'ملحوظة',
    'no_fees' => 'عفوا .. يوفليكس لا تدعم هذه المنطقة',
    'bad_choice' => 'رفض الطلب بسبب سوء الاختيار',
    'change_status' => 'تغيير الحالة',
    'other_reason' => 'سبب أخر',
    'delay_1' => "المستلم لا يرد على المكالمة",
    'delay_2' => "يطلب المستلم التأخير إلى تاريخ آخر",
    'delay_3' => 'هاتف المستلم مغلق',
    'delay_4' => 'رقم المستلم غير صحيح',
    'delay_5' => 'يطلب المستلم تغيير الموقع',
    'corporate_users' => 'مستخدمي الشركة',
    'mobile_app' => 'التطبيق',
    'website' => 'الموقع',
    'dashboard' => 'لوحة التحكم',
    'account_type' => 'نوع الحساب',
    'other_order_type' => 'نوع أخر للشحنات',
    'logistics' => 'لوجستية',
    'daily_report' => 'تقرير يومي',
    'corporate_prices' => 'أسعار الشركة',
    'recall_cost' => 'Recall Cost',
    'cancel_cost' => 'تكلفته الالغاء',
    'add_price' => 'أضف سعر جديد',
    'edit_price' => 'تعديل سعر',
    'generate_prices' => 'انشاء كل الاسعار',
    'download_papers' => 'تحميل المستندات',

    'move_collections' => 'شحنات مرسلة',
    'move_collection' => 'شحنات مرسلة',
    'count_orders' => 'عدد الشحنات',
    'move_by' => 'بواسطة',
    'not_dropped' => 'لم يتم الإنزال',
    'move_to' => 'انقل الى',
    'move_orders' => 'نقل الشحنات الى المخزن',
    'move' => 'نقل',
    'total_cost' => 'التكلفة',
    'remove' => 'حذف',
    'send_client' => 'إرسال للعميل',
    'action_date' => 'التاريخ',
    'pr' => 'تسليم جزئي',
    'confirm_send_client' => 'Confirm Send to Client',
    'send_client_orders' => 'Are You sure To Send This Orders?',
    'send_client_text' => 'Send Orders',
    'refund_collections' => 'الشحنات المرتجعة',
    'scan_collections' => 'مجموعات المسح',
    'replace' => 'استبدال',
    'serial_code' => 'رمز التسلسل',
    'export' => 'تصدير',
    'Dropped_at' => 'تاريخ الإنزال',
    'Recalled_at' => 'تاريج المرتجع',
    'Recalled_Date' => 'تاريج المرتجع',
    'Cancelled_at' => 'تاريج الإلغاء',
    'update_status' => 'تحديث الحالات',
    'Excel' => 'اكسل',
    'delivery_status' => 'حالة التسليم',
    'reject' => 'رفض مباشر',
    'refund' => 'تم إعادته',
    'responsibles' => 'المسؤولين',
    'users' => 'الموظفون',
    'task_orders' => 'Task Orders',
    'create_task_orders' => 'Create task orders',
    'task_type' => 'نوع المهمة',
    'pickup' => 'بيكب',
    'cache_collect' => 'تسليم كاش',
    'recall_orders' => 'إرجاع شحنات',
    'cache' => 'كاش',
    'task' => 'مهمة',
    'edit_task' => 'تعديل مهمة',
    'to_warehouse' => 'من المخزن',
    'from_warehouse' => 'إلى المخزن',
    'scan_type' => 'نوع المسح',
    'warehouse_role' => 'مسئول مخزن',

    'Please_Enter_Order' => 'من فضلك ادخل رقم الشحنة',
    'Please_Enter_Reason' => 'من فضلك ادخل الشبب',
    'Please_Choose_Status' => 'من فضلك اختر الحالة',
    'tickets' => 'التذاكر',
    'ticket' => 'تذكرة',
    'create_ticket' => 'انشاء تذكرة',
    'ticket_no' => 'رقم التذكرة',
    'created_by' => 'بواسطة',
    'system' => 'النظام',
    'closed_at' => 'تاريخ الغلق',
    'Order_Number' => 'رقم الشحنة',
    'order_log' => 'سجل الشحنة',
    'no_log' => 'لا يوجد سجل للشحنة',
    'comment' => 'السبب',
    'closed_comment' => 'سبب الغلق',
    'assigned_to' => 'موجهة إلى',
    'closed_by' => 'الغلق بواسطة',
    'dropped_in' => 'إنزال في',
    'close_tickets' => 'إغلاق التذاكر',
    'generate_tickets' => 'توليد التذاكر',
    'in_warehouse' => 'فى المخزن',
    'in_client' => 'عند العميل',
    'warehouse_manager' => 'مدير المخزن',
    'corporates_excel' => 'شيت الشركات',
    'drivers_excel' => 'شيت الكباتن',
    'drivers_daily_report' => 'التقرير اليومي للكباتن',
    'money' => 'الفلوس',
    'pickup_orders' => 'شحنات البك اب',
    'task_confirm_type' => 'تأكيد نوع المهمة',
    'received_span' => 'تم خروج الشحنة',
    'manager' => 'مدير',
    'No' => 'لا',
    'Yes' => 'نعم',
    'edit_customer' => 'تعديل العميل',
    'Current_Orders' => 'الشحنات الحالية',
    'Finished_Orders' => 'الشحنات المنتهية',
    'ofd' => 'شحنات خرجت للتسليم',
    'finished_orders_not_in_ufelix' => 'شحنات منتهية لم تصل يوفيلكس',
    'finished_orders_in_ufelix' => 'شحنات منتهية وصلت يوفيلكس',
    'upload_sheet' => 'رفع شيت',
    'download_excel' => 'إنزال شيت',
    'have_not_permission_on_order' => 'لا تملك صلاحية على هذه الشحنة',
    'must_write_comment' => 'لا يمكن تمام العملية بدون ذكر سبب الرفض والارتجاع',
    'order_updated_successfully' => 'تم تحديث الشحنة بنجاح',
    'order_comments' => 'تعليقات الشحنة',
    'ufelix_comments' => 'تعليقات يوفليكس',
    'client_comments' => 'تعليقات العميل',
    'add_order_comment' => 'أضف تعليق للشحنة',
    'add_order_comment_successfully' => 'تمت إضافة تعليق للشحنة بنجاح',
    'no_order_comments' => 'لا يوجد تعليقات للشحنة',
    'order_comment_deleted_successfully' => 'تم حذف تعليق الشحنة بنجاح',

];

<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        @if(!empty($orders) && count($orders))
            <form method="post" id="myform" action="{{ url('/mngrAdmin/forward_order') }}">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <div class="table-responsive">
                    <table id="theTable" class="entries table table-condensed table-striped text-center">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__("captain.status")}}</th>
                            <th>{{__('captain.receiver_name')}}</th>
                            <th>{{__('captain.sender_name')}}</th>
                            <th>{{__('captain.to')}}</th>
                            <th>{{__('captain.order_number')}}</th>
                            <th>{{__('captain.driver')}}</th>
                            <th>{{__('captain.total_price')}}</th>
                            <th>
                                <a class="action_btn btn btn-warning" id="toggle" data-state="1"> Select</a>
                            </th>
                            <th></th>

                        </tr>
                        </thead>
                        <thead id="floatThead" style="display: none">
                        <tr>
                            <th>#</th>
                            <th>{{__("captain.status")}}</th>
                            <th>{{__('captain.receiver_name')}}</th>
                            <th>{{__('captain.sender_name')}}</th>
                            <th>{{__('captain.to')}}</th>
                            <th>{{__('captain.order_number')}}</th>
                            <th>{{__('captain.driver')}}</th>
                            <th>{{__('captain.total_price')}}</th>
                            <th>
                                <a class="action_btn btn btn-warning" id="toggle" data-state="1"> Select</a>
                            </th>
                            <th></th>

                        </tr>
                        </thead>
                        <tbody id="tbody">
                        @foreach($orders as $i => $order)
                            @include('captain.scan.scan-tr', ['order' => $order, 'serial' => ($i+1)])
                        @endforeach
                        <div class="modal fade" id="forwardModal" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <p>{{__('captain.Forward_Orders_In_Agent_Or_Driver')}}</p>
                                        <div class="form-group">
                                            <br>
                                            <select class="form-control" name="driverID" id="driver">
                                                <option value="">{{__('captain.Choose_Driver')}}</option>
                                                @if(! empty($online_drivers))
                                                    @foreach($online_drivers as $driver)
                                                        <option value="{{$driver->id}}">{{$driver->name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary"
                                                    id="forward_action">{{__('captain.forward')}}</button>
                                            <button type="button" class="btn btn-danger"
                                                    data-dismiss="modal">{{__('captain.close')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        </tbody>
                    </table>
                </div>


            </form>
        @else
            <h3 class="text-center alert alert-warning">No Result Found !</h3>
        @endif
    </div>
</div>
<div id="js-form" class="js-form">
</div>

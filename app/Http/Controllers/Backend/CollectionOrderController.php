<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use App\Http\Requests;
use App\Models\Agent;
use App\Models\CollectionOrder;
use App\Models\Corporate;
use App\Models\Customer;
use App\Models\Driver;
use App\Models\Order;
use App\Models\Store;
use App\Models\TempOrder;
use Auth;
use DB;
use Excel;
use File;
use Flash;
use Illuminate\Http\Request;
use Response;
use function foo\func;


class CollectionOrderController extends Controller
{

    public function __construct(Request $request)
    {
        if ($request->is('mngrAdmin/*') || $request->is('admin/*')) {
            \Config::set('auth.defaults.guard', 'admin');

        } elseif ($request->is('mngrAgent/*') || $request->is('agent/*')) {
            \Config::set('auth.defaults.guard', 'agent');
        }
        $this->guardType = \Config::get('auth.defaults.guard');
    }

    public function importExcel()
    {
        if (Input::hasFile('import_file')) {
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function ($reader) {
            })->get();
            if (!empty($data) && $data->count()) {
                foreach ($data as $key => $value) {
                    $insert[] = ['title' => $value->title, 'description' => $value->description];
                }
                if (!empty($insert)) {
                    DB::table('orders')->insert($insert);
                    dd(__('backend.Insert_Record_successfully'));
                }
            }
        }
        return back();
    }

    public function downloadStatus(Request $request)
    {
        $file = CollectionOrder::where('id', $request->id)->first();
        $fileP = public_path('api_uploades/client/corporate/sheets/') . $file->file_name;
        $exist = File::exists($fileP);
        if ($exist == false) {
            Flash::error(__('backend.File_Does_not_Exist_Please_try_to_upload_it_again'));
            return redirect()->back();
        } else {
            $upFile = CollectionOrder::where('id', $request->id)->update(['status' => 1]);
            return response()->download(public_path('api_uploades/client/corporate/sheets/' . $request->file_name));
        }
    }

    public function index(Request $request)
    {
        $type = app('request')->input('type');
        $user = Auth::guard($this->guardType)->user();

        $agents = Agent::all();
        $allStores = Store::where('status', 1);

        if ($user->is_warehouse_user) {
            $allStores = $allStores->join('store_user', 'store_user.store_id', '=', 'stores.id')
                ->where('user_id', $user->id);
        }

        $allStores = $allStores->get();

        $pickup_store = Store::where('pickup_default', 1)->first();

        $corporates = Corporate::where('is_active', 1)->select('corporates.*')
            ->orderBy('corporates.name', 'ASC');
        $drivers = Driver::where('is_active', 1)->select('drivers.*');

        if ($user->is_warehouse_user) {
            $corporates = $corporates->join('customers', 'customers.corporate_id', '=', 'corporates.id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id);

            $drivers = $drivers->join('store_user', 'store_user.store_id', '=', 'drivers.store_id')
                ->where('user_id', $user->id);
        }

        $drivers = $drivers->get();
        $corporates = $corporates->get();

        $saved = app('request')->input('saved');
        $search = app('request')->input('search');
        $collection_type = app('request')->input('collection_type');

        $collection_orders = CollectionOrder::select('collection_orders.*')->withCount(['listorders', 'temporders'])->with(['customer' => function ($query) {
            $query->with('Corporate');
        }, 'listorders' => function ($query) {
            $query->where("status", 0);
        }])->orderBy('collection_orders.id', 'desc');

        if ($user->is_warehouse_user) {
            $collection_orders = $collection_orders->join('customers', 'customers.id', '=', 'collection_orders.customer_id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id);
        }

        if (isset($saved) && $saved != -1) {
            if($saved == 1){
                $collection_orders = $collection_orders->where('is_saved', $saved);
            }else{
                $collection_orders = $collection_orders->where(function ($query){
                    $query->where('is_saved', 0)
                        ->orWhereNull('is_saved');
                });
            }

        }

        if (isset($collection_type) && $collection_type != -1) {
            $collection_orders = $collection_orders->where('uploaded_target', $collection_type);
        }

        if (isset($search) && $search != "") {
            $collection_orders = $collection_orders->join('customers', 'customers.id', '=', 'collection_orders.customer_id')
                ->join('corporates', 'corporates.id', '=', 'customers.corporate_id')
                ->join('orders', 'orders.collection_id', '=', 'collection_orders.id')
                ->where(function ($q) use ($search) {
                    $q->where('collection_orders.id', 'like', '%' . $search . '%');
                    $q->OrWhere('customers.name', 'like', '%' . $search . '%');
                    $q->OrWhere('corporates.name', 'like', '%' . $search . '%');
                    $q->OrWhere('orders.id', 'like', '%' . $search . '%');
                })
                ->groupBy('collection_orders.id');
        }

        $collection_orders = $collection_orders->paginate(50);
        if ($request->ajax()) {
            return [
                'view' => view('backend.collection_orders.table', compact('collection_orders', 'type'))->render(),
            ];
        }

        return view('backend.collection_orders.index', compact('collection_orders', 'type', 'pickup_store', 'allStores', 'agents', 'corporates', 'drivers'));

    }

    function fetch(Request $request)
    {
        $select = $request->get('select');
        $value = $request->get('value');
        $dependent = $request->get('dependent');
        $data = Customer::where('corporate_id', $value)->get();
        $output = '<option value="">Select ' . ucfirst($dependent) . '</option>';
        foreach ($data as $row) {
            $output .= '<option value="' . $row->id . '">' . $row->name . '</option>';
        }
        echo $output;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $user = Auth::guard($this->guardType)->user();

        $customers = Corporate::where('is_active', 1)->select('corporates.*')->orderBy('corporates.name', 'ASC');

        if ($user->is_warehouse_user) {
            $customers = $customers->join('customers', 'customers.corporate_id', '=', 'corporates.id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id);
        }

        $customers = $customers->get();

        return view('backend.collection_orders.create', compact('customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'sheet' => 'required|mimes:csv,xlsx,xls',
            'customer_id' => 'required'
        ]);

        $collection_orders = new CollectionOrder();
        $collection_orders->customer_id = $request->input("customer_id");
        $collection_orders->count_no = 0;
        $collection_orders->uploaded_target = 1;

        if ($request->file("sheet") != "") {
            $extension = File::extension($request->file("sheet")->getClientOriginalName());
            $select = CollectionOrder::orderBy('id', 'DESC')->first();
            if (!empty($select)) {
                $select = $select->id + 1;
            } else {
                $select = 1;
            }
            $image = "sheet" . "_" . $select . "_" . "customer" . "_" . $collection_orders->customer_id . "." . $extension;
            $collection_orders->file_name = $image;
            $request->file("sheet")->move(public_path('api_uploades/client/corporate/sheets/'), $image);
        }
        $collection_orders->save();

        Flash::success(__('backend.Collection_Orders_saved_successfully'));
        return redirect()->route('mngrAdmin.collection_orders.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $CollectionOrders = CollectionOrder::findOrFail($id);
        return view('backend.collection_orders.show', compact('CollectionOrders'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $collection_orders = CollectionOrder::findOrFail($id);

        $user = Auth::guard($this->guardType)->user();

        $customers = Corporate::where('is_active', 1)->select('corporates.*')->orderBy('corporates.name', 'ASC');

        if ($user->is_warehouse_user) {
            $customers = $customers->join('customers', 'customers.corporate_id', '=', 'corporates.id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id);
        }

        $customers = $customers->get();

        return view('backend.collection_orders.edit', compact('collection_orders', 'customers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @param Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'sheet' => 'required|mimes:csv,xlsx,xls',
            'customer_id' => 'required'
        ]);

        $collection_orders = CollectionOrder::findOrFail($id);
        $collection_orders->customer_id = $request->input("customer_id");
        $collection_orders->count_no = 0;
        if ($request->file("sheet") != "") {
            $extension = File::extension($request->file("sheet")->getClientOriginalName());

            $image = "sheet" . "_" . $collection_orders->id . "_" . "customer" . "_" . $collection_orders->customer_id . "." . $extension;
            $collection_orders->file_name = $image;
            $request->file("sheet")->move(public_path('api_uploades/client/corporate/sheets/'), $image);
        } elseif (!$request->input("sheet_old")) {
            flash(__('backend.Please_Enter_Correct_File'))->error();
            return redirect()->back();
        }
        $collection_orders->save();

        Flash::warning(__('backend.Collection_Orders_Updated_successfully'));
        return redirect()->route('mngrAdmin.collection_orders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $type = app('request')->input('type');

        $CollectionOrder = CollectionOrder::findOrFail($id);
//        if (!empty($CollectionOrder->listorders) && count($CollectionOrder->listorders)) {
//            Flash::warning('Sorry ! Collection Order #' . $CollectionOrder->id . ' does not  deleted. May be have orders ');
//
//        } else {
        flash(__('backend.Collection_Order_deleted_successfully'))->error();

        Order::where('collection_id', $CollectionOrder->id)->where('status', 0)->delete();
        TempOrder::where('collection_id', $CollectionOrder->id)->delete();
        if($CollectionOrder->type == 1){
            $CollectionOrder->delete();
        }else{
            $CollectionOrder->is_saved = 0;
            $CollectionOrder->saved_at = null;
            $CollectionOrder->save();
        }

        // Function Add Action In Activity Log By Samir
        addActivityLog('Delete Collection Orders #No ' . $CollectionOrder->id, ' حذف مجموعة الطلبات #رقم ' . $CollectionOrder->id, 7); // Helper Function in App\Helpers.php


//        }

        return redirect('mngrAdmin/collection_orders?type=' . $type);
    }

    public function delete_collection(Request $request)
    {

        $CollectionOrder = CollectionOrder::findOrFail($request->collection_id);

        flash(__('backend.Collection_Order_deleted_successfully'))->error();

        Order::where('collection_id', $CollectionOrder->id)->where('status', 0)->delete();
        TempOrder::where('collection_id', $CollectionOrder->id)->delete();
        $CollectionOrder->delete();

    }

    public function print_collection($id, Request $request)
    {

        if ($id) {
            $orders = Order::with(['payment_method', 'to_government', 'to_city', 'warehouse', 'customer' => function ($query) {
                $query->with(['Corporate' => function ($query) {
                    $query->with('manager');
                }]);
            }])
                ->where('collection_id', $id)
                ->orderBy('receiver_name', 'ASC')
                ->get();

            foreach ($orders as $order) {
                if (!empty($order->customer->Corporate->manager->first())) {
                    $order->corporteName = $order->customer->name;
                } else {
                    $order->corporteName = $order->customer->Corporate->name;
                }
//                $customer = Customer::where('id', $order->customer_id)->value('corporate_id');
//                $order->corporteName = Corporate::where('id', $customer)->value('name');
            }
            $data = ['orders' => $orders];
            $pdf = \PDF::loadView('backend.orders.pdf-orders', $data, [], ['format' => 'A5-L']);
            return $pdf->stream(time());
        } else {
            //error
            flash(__('backend.Must_Choose_Orders_To_Print'))->error();
            redirect()->back();
        }


    }
}

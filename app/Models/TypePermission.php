<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypePermission extends Model
{

    public function permissions(){
        return $this->hasMany('App\Models\Permission', 'type_permission_id', 'id');
    }

}

@extends('captain.layouts.app')

@section('css')
    <style>
        .error_vehicle {
            display: inline-block;
            color: rgb(255, 255, 255);
            background-color: #9c3737;
            width: 100%;
            padding: 5px;
            margin-top: 3px;
            border-radius: 2px;
            text-align: center;
            font-weight: 500;
        }

        .form-group {
            min-height: 94px;
            margin-bottom: 0px;
        }

        .input-group {
            -moz-box-shadow: none !important;
            -webkit-box-shadow: none !important;
            box-shadow: none !important;
        }

        .input-group {
            width: 100%;
        }
    </style>
    <title>{{__('captain.drivers')}} - {{__('captain.add_driver')}}</title>
@endsection

@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-plus"></i> {{__('captain.drivers')}} / {{__('captain.add_driver')}} </h3>
    </div>
@endsection

@section('content')
    @foreach($errors->all() as $error)
        {{$error}}
    @endforeach
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <form action="{{ route('captainDashboard.drivers.store') }}" method="POST" enctype="multipart/form-data"
                  id="driver_create" name="driver_create">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group col-md-4 col-sm-4 @if($errors->has('name')) has-error @endif">
                    <label for="name-field">{{__('captain.name')}}: </label>
                    <input type="text" id="name-field" name="name" class="form-control" value="{{old("name")}}"/>
                    @if($errors->has("name"))
                        <span class="help-block">{{ $errors->first("name") }}</span>
                    @endif
                </div>

                <div class="form-group col-md-4 col-sm-4 @if($errors->has('mobile')) has-error @endif">
                    <label for="mobile-field">{{__('captain.mobile_number')}}: </label>
                    <input type="text" id="mobile-field" maxlength="11" name="mobile" class="form-control"
                           value="{{old("mobile")}}"/>
                    @if($errors->has("mobile"))
                        <span class="help-block">{{ $errors->first("mobile") }}</span>
                    @endif
                </div>

                <div class="form-group col-md-4 col-sm-4 @if($errors->has('password')) has-error @endif">
                    <label for="password-field">{{__('captain.password')}}: </label>
                    <input type="password" id="password-field" name="password" class="form-control"/>
                    @if($errors->has("password"))
                        <span class="help-block">{{ $errors->first("password") }}</span>
                    @endif
                </div>
                <div class="form-group col-md-4 col-sm-4 @if($errors->has('password')) has-error @endif">
                    <label for="confirm_password-field">{{__('captain.confirm_password')}}: </label>
                    <input type="password" id="confirm_password-field" name="confirm_password" class="form-control"/>
                    @if($errors->has("confirm_password"))
                        <span class="help-block">{{ $errors->first("confirm_password") }}</span>
                    @endif
                </div>

                <div class="well well-sm col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px">
                    <button type="submit" id="added_driver"
                            class="btn btn-primary">{{__('captain.add_driver')}}</button>
                    <a class="btn btn-link pull-right" href="{{ route('captainDashboard.drivers.index') }}"><i
                            class="glyphicon glyphicon-backward"></i> {{__('captain.back')}}</a>
                </div>

            </form>
        </div>
    </div>

@endsection

@section('scripts')

    <script type="text/javascript">
        $(function () {

            $("form[name='driver_create']").validate({
                // Specify validation rules
                rules: {
                    name: {
                        required: true,
                    },
                    mobile: {
                        required: true,
                        number: true,
                        rangelength: [11, 11],
                        remote: {
                            url: "{!! url('/captainDashboard/check-mobile-driver') !!}",
                            type: "get"
                        }
                    },
                    password: {
                        required: true,
                        minlength: 5
                    },
                    confirm_password: {
                        required: true,
                        equalTo: '#password-field',
                        minlength: 5
                    },
                },

                // Specify validation error messages
                messages: {
                    name: {
                        required: "{{__('captain.Please_Enter_driver_Name')}}",
                    },
                    mobile: {
                        rangelength: "{{__('captain.Mobile_Must_Be11_Digits')}}",
                        required: "{{__('captain.Please_Enter_Driver_Mobile_Number')}}",
                        number: "{{__('captain.Please_Enter_Avalid_Number')}}",
                        remote: jQuery.validator.format("{{__('captain.Mobile_Number_already_exist')}}")
                    },
                    password: {
                        required: "{{__('captain.Please_Enter_Driver_Password')}}",
                        minlength: "{{__('captain.Password_must_be_more_than5_character')}}"
                    },
                    confirm_password: {
                        required: "{{__('captain.Confirm_password_is_wrong')}}",
                        equalTo: "{{__('captain.Confirm_password_is_wrong')}}",
                    },
                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                },

                highlight: function (element) {
                    $(element).parent().addClass('error')
                },

                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                },

                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                }
            });
        });
    </script>
@endsection

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">

    <style>
        td, th {
            text-align: center;
            vertical-align: middle;
        }

    </style>
</head>

<body>
<table>
    <tr>
        <th>No</th>
        <th>
            ID
        </th>
        <th>
            CREATE DATE
        </th>
        <th>
            LAST STATUS DATE
        </th>
        <th>
            STATUS
        </th>
        <th>
            WAREHOUSE
        </th>
        <th>
            COLLECTED COST
        </th>
        @if($type == 1)
            <th>
                CORPORATE
            </th>
            <th>
                USER
            </th>
        @endif
        <th>
            Captain
        </th>
        <th>
            ORDER NUMBER
        </th>
        <th>
            CODE
        </th>
        <th>
            RECEIVER NAME
        </th>
        <th>
            RECEIVER ADDRESS
        </th>
        <th>
            RECEIVER MOBILE
        </th>
        <th>
            GOV
        </th>
        <th>
            CITY
        </th>
        <th>
            NOTES
        </th>
        <th>
            TYPE
        </th>
        <th>
            TOTAL
        </th>
        <th>
            CLIENT
        </th>
        <th>
            UFELIX
        </th>
        <th>
            DELAY
        </th>
        <th>
            DATE
        </th>
        <th>
            DELIVERY PROBLEMS
        </th>
        <th>
            DATE
        </th>
    </tr>
    @php
        $i=1;
    @endphp
    @if(! empty($orders))
        @foreach($orders->cursor() as $order)
            @if((count($order->delivery_problems) >= count($order->order_delay)) && count($order->delivery_problems) > 0)
                @foreach($order->delivery_problems as $j => $problem)
                    @if($loop->first)
                        <tr>
                            <td>{{$i}}</td>
                            <td>
                                {{$order->id}}
                            </td>
                            <td>
                                {{$order->created_at}}
                            </td>
                            <td>
                                {{$order->last_status_date}}
                            </td>
                            <td>
                                {!!$order->status_details_without_refund!!}
                            </td>
                            <td>
                                @if($order->is_refund == 1)
                                    Refund
                                @elseif($order->client_dropoff == 1)
                                    Client
                                @elseif($order->warehouse_dropoff == 1)
                                    Warehouse
                                @endif
                            </td>
                            <td>
                                {{$order->collected_cost}}
                            </td>
                            @if($type == 1)
                                <td>
                                    {{$order->customer->Corporate->name}}
                                </td>
                                <td>
                                    {{$order->customer->name}}
                                </td>
                            @endif
                            <td>
                                @if(! empty($order->accepted ) && ! empty($order->accepted->driver))
                                    {{$order->accepted->driver->name}}
                                @else
                                    -
                                @endif
                            </td>
                            <td>
                                {{$order->order_no}}
                            </td>
                            <td>
                                {{$order->order_number}}
                            </td>
                            <td>
                                {{$order->receiver_name}}
                            </td>
                            <td>
                                {{mb_substr(htmlentities(strip_tags($order->receiver_address)),0,150, "utf-8")}}
                            </td>
                            <td>
                                {{$order->receiver_mobile}}
                            </td>
                            <td>
                                {{isset($order->to_government->name_en) ? $order->to_government->name_en : ''}}
                            </td>
                            <td>
                                {{isset($order->to_city->name_en) ? $order->to_city->name_en : ''}}
                            </td>
                            <td>
                                {{$order->notes}}
                            </td>
                            <td>
                                {{isset($order->types->name_en) ? $order->types->name_en : ''}}
                            </td>
                            <td>
                                {{$order->total_price}}
                            </td>
                            <td>
                                {{$order->total_price - $order->delivery_price}}
                            </td>
                            <td>
                                {{$order->delivery_price}}
                            </td>
                            <td>
                                @if(isset($order->order_delay[$j]))
                                    {{$order->order_delay[$j]->delay_comment}}
                                @endif
                            </td>
                            <td>
                                @if(isset($order->order_delay[$j]))
                                    {{$order->order_delay[$j]->delay_at}}
                                @endif
                            </td>
                            <td>
                                {{mb_substr(($problem->reason ? htmlentities(strip_tags($problem->reason)) : $problem->problem->reason_en),0,150, "utf-8")}}
                            </td>
                            <td>
                                {{$problem->created_at}}
                            </td>
                        </tr>
                    @else
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            @if($type == 1)
                                <td></td>
                                <td></td>
                            @endif
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                @if(isset($order->order_delay[$j]))
                                    {{$order->order_delay[$j]->delay_comment}}
                                @endif
                            </td>
                            <td>
                                @if(isset($order->order_delay[$j]))
                                    {{$order->order_delay[$j]->delay_at}}
                                @endif
                            </td>
                            <td>
                                {{mb_substr(($problem->reason ? htmlentities(strip_tags($problem->reason)) : $problem->problem->reason_en),0,150, "utf-8")}}
                            </td>
                            <td>
                                {{$problem->created_at}}
                            </td>
                        </tr>
                    @endif
                @endforeach
            @elseif(count($order->delivery_problems) < count($order->order_delay))
                @foreach($order->order_delay as $t => $order_delay)
                    @if($loop->first)
                        <tr>
                            <td>{{$i}}</td>
                            <td>
                                {{$order->id}}
                            </td>
                            <td>
                                {{$order->created_at}}
                            </td>
                            <td>
                                {{$order->last_status_date}}
                            </td>
                            <td>
                                {!!$order->status_details_without_refund!!}
                            </td>
                            <td>
                                @if($order->is_refund == 1)
                                    Refund
                                @elseif($order->client_dropoff == 1)
                                    Client
                                @elseif($order->warehouse_dropoff == 1)
                                    Warehouse
                                @endif
                            </td>
                            <td>
                                {{$order->collected_cost}}
                            </td>
                            @if($type == 1)
                                <td>
                                    {{$order->customer->Corporate->name}}
                                </td>
                                <td>
                                    {{$order->customer->name}}
                                </td>
                            @endif
                            <td>
                                @if(! empty($order->accepted ) && ! empty($order->accepted->driver))
                                    {{$order->accepted->driver->name}}
                                @else
                                    -
                                @endif
                            </td>
                            <td>
                                {{$order->order_no}}
                            </td>
                            <td>
                                {{$order->order_number}}
                            </td>
                            <td>
                                {{mb_substr(htmlentities(strip_tags($order->receiver_address)),0,150, "utf-8")}}
                            </td>
                            <td>
                                {{$order->receiver_name}}
                            </td>
                            <td>
                                {{$order->receiver_mobile}}
                            </td>
                            <td>
                                {{isset($order->to_government->name_en) ? $order->to_government->name_en : ''}}
                            </td>
                            <td>
                                {{isset($order->to_city->name_en) ? $order->to_city->name_en : ''}}
                            </td>
                            <td>
                                {{$order->notes}}
                            </td>
                            <td>
                                {{isset($order->types->name_en) ? $order->types->name_en : ''}}
                            </td>
                            <td>
                                {{$order->total_price}}
                            </td>
                            <td>
                                {{$order->total_price - $order->delivery_price}}
                            </td>
                            <td>
                                {{$order->delivery_price}}
                            </td>
                            <td>
                                {{$order_delay->delay_comment ? $order_delay->delay_comment : ''}}
                            </td>
                            <td>
                                {{$order_delay->delay_at}}
                            </td>
                            <td>
                                @if(isset($order->delivery_problems[$t]))
                                    {{mb_substr(($order->delivery_problems[$t]->reason ? htmlentities(strip_tags($order->delivery_problems[$t]->reason)) : $order->delivery_problems[$t]->problem->reason_en),0,150, "utf-8")}}
                                @endif
                            </td>
                            <td>
                                @if(isset($order->delivery_problems[$t]))
                                    {{$order->delivery_problems[$t]->created_at}}
                                @endif
                            </td>
                        </tr>
                    @else
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            @if($type == 1)
                                <td></td>
                                <td></td>
                            @endif
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                {{$order_delay->delay_comment ? $order_delay->delay_comment : ''}}
                            </td>
                            <td>
                                {{$order_delay->delay_at}}
                            </td>
                            <td>
                                @if(isset($order->delivery_problems[$t]))
                                    {{mb_substr(($order->delivery_problems[$t]->reason ? htmlentities(strip_tags($order->delivery_problems[$t]->reason)) : $order->delivery_problems[$t]->problem->reason_en),0,150, "utf-8")}}
                                @endif
                            </td>
                            <td>
                                @if(isset($order->delivery_problems[$t]))
                                    {{$order->delivery_problems[$t]->created_at}}
                                @endif
                            </td>
                        </tr>
                    @endif
                @endforeach
            @else
                <tr>
                    <td>{{$i}}</td>
                    <td>
                        {{$order->id}}
                    </td>
                    <td>
                        {{$order->created_at}}
                    </td>
                    <td>
                        {{$order->last_status_date}}
                    </td>
                    <td>
                        {!!$order->status_details_without_refund!!}
                    </td>
                    <td>
                        @if($order->is_refund == 1)
                            Refund
                        @elseif($order->client_dropoff == 1)
                            Client
                        @elseif($order->warehouse_dropoff == 1)
                            Warehouse
                        @endif
                    </td>
                    <td>
                        {{$order->collected_cost}}
                    </td>
                    @if($type == 1)
                        <td>
                            {{$order->customer->Corporate->name}}
                        </td>
                        <td>
                            {{$order->customer->name}}
                        </td>
                    @endif
                    <td>
                        @if(! empty($order->accepted ) && ! empty($order->accepted->driver))
                            {{$order->accepted->driver->name}}
                        @else
                            -
                        @endif
                    </td>
                    <td>
                        {{$order->order_no}}
                    </td>
                    <td>
                        {{$order->order_number}}
                    </td>
                    <td>
                        {{$order->receiver_name}}
                    </td>
                    <td>
                        {{mb_substr(htmlentities(strip_tags($order->receiver_address)),0,150, "utf-8")}}
                    </td>
                    <td>
                        {{$order->receiver_mobile}}
                    </td>
                    <td>
                        {{isset($order->to_government->name_en) ? $order->to_government->name_en : ''}}
                    </td>
                    <td>
                        {{isset($order->to_city->name_en) ? $order->to_city->name_en : ''}}
                    </td>
                    <td>
                        {{$order->notes}}
                    </td>
                    <td>
                        {{isset($order->types->name_en) ? $order->types->name_en : ''}}
                    </td>
                    <td>
                        {{$order->total_price}}
                    </td>
                    <td>
                        {{$order->total_price - $order->delivery_price}}
                    </td>
                    <td>
                        {{$order->delivery_price}}
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
            @endif

            @php
                $i++;
            @endphp
        @endforeach
    @endif
</table>
</body>
</html>

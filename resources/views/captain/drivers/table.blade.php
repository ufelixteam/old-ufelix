<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        @if($drivers->count())
            <table class="table table-condensed table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('captain.name')}}</th>
                    <th>{{__('captain.mobile')}}</th>
                    <th>{{__('captain.active')}}</th>
                    <th class="text-right"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($drivers as $driver)
                    <tr>
                        <td>
                            {{$driver->id}}</td>
                        <td>{{$driver->name}}</td>
                        <td>{{$driver->mobile}}</td>

                        <td><input data-id="{{$driver->id}}" data-size="mini" class="toggle change_verify"
                                   {{$driver->is_active == 1 ? 'checked' : ''}} data-onstyle="success" type="checkbox"
                                   data-style="ios" data-on="Yes" data-off="No">
                        </td>

                        <td class="text-right">

                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                    <li><a href="{{ route('captainDashboard.drivers.edit', $driver->id) }}"><i
                                                class="glyphicon glyphicon-edit"></i> {{__('captain.edit')}}</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {!! $drivers->appends($_GET)->links() !!}

        @else
            <h3 class="text-center alert alert-warning">{{__('captain.No_result_found')}}</h3>
        @endif
    </div>
</div>

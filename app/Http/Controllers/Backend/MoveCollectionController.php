<?php

namespace App\Http\Controllers\Backend;

use App\Events\OrderUpdated;
use App\Http\Controllers\Controller\Backend;
use App\Http\Requests;
use App\Models\CollectionOrder;
use App\Models\Corporate;
use App\Models\Customer;
use App\Models\MoveCollection;
use App\Models\MoveCollectionOrder;
use App\Models\Order;
use Auth;
use Carbon\Carbon;
use DB;
use Excel;
use File;
use Flash;
use Illuminate\Http\Request;
use Response;


class MoveCollectionController extends Controller
{

    public function __construct(Request $request)
    {
        if ($request->is('mngrAdmin/*') || $request->is('admin/*')) {
            \Config::set('auth.defaults.guard', 'admin');

        } elseif ($request->is('mngrAgent/*') || $request->is('agent/*')) {
            \Config::set('auth.defaults.guard', 'agent');
        }
        $this->guardType = \Config::get('auth.defaults.guard');
    }

    public function importExcel()
    {
        if (Input::hasFile('import_file')) {
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function ($reader) {
            })->get();
            if (!empty($data) && $data->count()) {
                foreach ($data as $key => $value) {
                    $insert[] = ['title' => $value->title, 'description' => $value->description];
                }
                if (!empty($insert)) {
                    DB::table('orders')->insert($insert);
                    dd(__('backend.Insert_Record_successfully'));
                }
            }
        }
        return back();
    }

    public function downloadStatus(Request $request)
    {
        $file = CollectionOrder::where('id', $request->id)->first();
        $fileP = public_path('api_uploades/client/corporate/sheets/') . $file->file_name;
        $exist = File::exists($fileP);
        if ($exist == false) {
            Flash::error(__('backend.File_Does_not_Exist_Please_try_to_upload_it_again'));
            return redirect()->back();
        } else {
            $upFile = CollectionOrder::where('id', $request->id)->update(['status' => 1]);
            return response()->download(public_path('api_uploades/client/corporate/sheets/' . $request->file_name));
        }
    }

    public function index(Request $request)
    {
        $user = Auth::guard($this->guardType)->user();

        $collections = MoveCollection::with(['driver', 'store'])
            ->withCount('orders')
            ->orderBy('id', 'desc');

        if ($user->is_warehouse_user) {
            $collections = $collections->leftJoin('store_user', 'store_user.store_id', '=', 'move_collections.store_id')
                ->leftJoin('store_user as su', 'su.store_id', '=', 'move_collections.from_store_id')
//                ->whereNotNull('move_collections.from_store_id')
//                ->whereNotNull('move_collections.store_id')
                ->where(function ($query) use ($user) {
                    $query->where('su.user_id', $user->id)
                        ->orWhere('store_user.user_id', $user->id);
                })->groupBy('move_collections.id');
        }

        $collections = $collections->paginate(50);

        if ($request->ajax()) {
            return [
                'view' => view('backend.move_collections.table', compact('collections'))->render(),
            ];
        }

        return view('backend.move_collections.index', compact('collections'));

    }

    function fetch(Request $request)
    {
        $select = $request->get('select');
        $value = $request->get('value');
        $dependent = $request->get('dependent');
        $data = Customer::where('corporate_id', $value)->get();
        $output = '<option value="">Select ' . ucfirst($dependent) . '</option>';
        foreach ($data as $row) {
            $output .= '<option value="' . $row->id . '">' . $row->name . '</option>';
        }
        echo $output;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $user = Auth::guard($this->guardType)->user();

        $customers = Corporate::where('is_active', 1)->orderBy('corporates.id', 'desc');
        if ($user->is_warehouse_user) {
            $customers = $customers->join('customers', 'customers.corporate_id', '=', 'corporates.id')
                ->join('store_user', 'store_user.store_id', '=', 'customers.store_id')
                ->where('user_id', $user->id);
        }
        $customers = $customers->select('corporates.*')->get();

        return view('backend.collection_orders.create', compact('customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $from_store_id = null;
        $ids = explode(',', app('request')->input('ids'));
        if (is_array($ids) && count($ids)) {
            $first_order = Order::find($ids[0]);
            if ($first_order) {
                $from_store_id = $first_order->moved_in;
            }
        }

        $collection = new MoveCollection();
        $collection->store_id = $request->input("store_id");
        $collection->driver_id = $request->input("driver_id");
        $collection->cost = $request->input("cost");
        $collection->move_by = $request->input("move_by");
        $collection->from_store_id = $from_store_id;

        if ($request->hasFile('image')) {
            $imgExt = $request->file('image')->getClientOriginalExtension();
            $image = 'collection-' . time() . "." . $imgExt;
            $request->file("image")->move(public_path('api_uploades/move_collections/'), $image);
        } else {
            $image = '';
        }

        $collection->image = $image;

        $collection->save();

        if (is_array($ids) && count($ids)) {
            foreach ($ids as $id) {
                $move_order = new MoveCollectionOrder();
                $move_order->order_id = $id;
                $move_order->collection_id = $collection->id;
                $move_order->save();
            }

            Order::whereIn('id', $ids)->update(['move_collection_id' => $collection->id]);

            event(new OrderUpdated($ids, Auth::guard($this->guardType)->user()->id, 1, 'moved_to', ['store_id' => $collection->store_id]));
        }
        if ($request->ajax()) {
            $data['ids'] = $ids;
            $data['result'] = true;
            $data['message'] = __('backend.Collection_Orders_saved_successfully');
            return $data;
        }


        Flash::success(__('backend.Collection_Orders_saved_successfully'));

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $CollectionOrders = CollectionOrder::findOrFail($id);
        return view('backend.collection_orders.show', compact('CollectionOrders'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $collection_orders = CollectionOrder::findOrFail($id);
        $customers = Customer::orderBy('id', 'desc')->get();
        return view('backend.collection_orders.edit', compact('collection_orders', 'customers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @param Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $collection_orders = CollectionOrder::findOrFail($id);
        $collection_orders->customer_id = $request->input("customer_id");
        $collection_orders->count_no = 0;
        $counter = 0;
        if ($request->file("sheet") != "") {
            $counter++;
            $extension = File::extension($request->file("sheet")->getClientOriginalName());

            $image = "sheet" . "_" . $collection_orders->id . "_" . "customer" . "_" . $collection_orders->customer_id . "." . $extension;
            $collection_orders->file_name = $image;
            $request->file("sheet")->move(public_path('api_uploades/client/corporate/sheets/'), $image);
        } elseif (!$request->input("sheet_old")) {
            flash(__('backend.Please_Enter_Correct_File'))->error();
            return redirect()->back();
        }
        $collection_orders->save();

        Flash::warning(__('backend.Collection_Orders_Updated_successfully'));
        return redirect()->route('mngrAdmin.collection_orders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $type = app('request')->input('type');

        $CollectionOrder = CollectionOrder::findOrFail($id);
        if (!empty($CollectionOrder->listorders) && count($CollectionOrder->listorders)) {
            Flash::warning('Sorry ! Collection Order #' . $CollectionOrder->id . ' does not  deleted. May be have orders ');

        } else {
            flash(__('backend.Collection_Order_deleted_successfully'))->error();

            $CollectionOrder->delete();

            // Function Add Action In Activity Log By Samir
            addActivityLog('Delete Collection Orders #No ' . $CollectionOrder->id, ' حذف مجموعة الطلبات #رقم ' . $CollectionOrder->id, 7); // Helper Function in App\Helpers.php


        }

        return redirect('mngrAdmin/collection_orders?type=' . $type);
    }

    public function drop_orders(Request $request)
    {
        $ids = [];
        if ($request->ids) {
            $ids = $request->ids;
            $id = $request->collection_id;
            $collection = MoveCollection::find($id);
            $orders = MoveCollectionOrder::whereIn('order_id', $ids)->pluck('order_id')->toArray();
            MoveCollectionOrder::whereIn('order_id', $ids)->update(['dropped' => 1, 'dropped_at' => Carbon::now()]);
            Order::whereIn('id', $orders)->update(['moved_in' => $collection->store_id, 'moved_at' => Carbon::now(), 'move_collection_id' => null]);
        } elseif ($request->collection_id) {
            $id = $request->collection_id;
            $collection = MoveCollection::find($id);
            $orders = MoveCollectionOrder::where('collection_id', $id)->pluck('order_id')->toArray();
            MoveCollectionOrder::where('collection_id', $id)->update(['dropped' => 1, 'dropped_at' => Carbon::now()]);
            Order::whereIn('id', $orders)->update(['moved_in' => $collection->store_id, 'moved_at' => Carbon::now(), 'move_collection_id' => null]);
        }

        event(new OrderUpdated($orders, Auth::guard($this->guardType)->user()->id, 1, 'moved_dropped', ['store_id' => $collection->store_id]));

        Flash::success(__('backend.convert_pending_to_dropped'));

        if ($request->ajax()) {
            return __('backend.drop_off_package');
        }

        return redirect()->back();

    }/*dropped orders*/
}

<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use App\Http\Requests;
use App\Models\Agent;
use App\Models\Color;
use App\Models\Driver;
use App\Models\ModelType;
use App\Models\Truck;
use App\Models\VehicleType;
use Auth;
use File;
use Flash;
use Illuminate\Http\Request;
use Response;

class TruckController extends Controller
{

    private $guardType;

    public function __construct(Request $request)
    {
        if ($request->is('mngrAdmin/*') || $request->is('admin/*')) {
            \Config::set('auth.defaults.guard', 'admin');
        } elseif ($request->is('mngrAgent/*') || $request->is('agent/*')) {
            \Config::set('auth.defaults.guard', 'agent');
        }

        $this->guardType = \Config::get('auth.defaults.guard');
    }

    // Get All Trucks
    public function index(Request $request)
    {
        $types = VehicleType::orderBy('id', 'ASC')->get();
        $user = Auth::guard($this->guardType)->user();
        $trucks = Truck::orderBy('id', 'desc')/*->where('agent_id', '>', 0)*/->where('driver_id', 0);

        // Check If Active Or Block Filter Or Search By Name
        $type = app('request')->input('type');
        $size = app('request')->input('size');
        $search = app('request')->input('search');

        if (isset($search) && $search != "") {
            $trucks = $trucks->where('model_name', 'like', '%' . $search . '%')->orWhere('plate_number', 'like', '%' . $search . '%');
        }

        if (isset($type) && $type != -1) {
            $trucks = $trucks->where('vehicle_type_id', $type);
        }

        $trucks = $trucks->paginate(15);

        if ($user->role_id == '1') {
            if ($request->ajax()) {
                return [
                    'view' => view('backend.trucks.table', compact('trucks', 'types'))->render(),
                ];
            }
            return view('backend.trucks.index', compact('trucks', 'types'));
        } else {
            return view('agent.trucks.index', compact('trucks'));
        }
    }

    // Added Truck Page
    public function create()
    {
        $truck_types = VehicleType::orderBy('name_en', 'ASC')->get();
        $model_types = ModelType::orderBy('name', 'ASC')->get();
        $colors = Color::orderBy('name', 'ASC')->get();
        $agents = Agent::orderBy('name', 'ASC')->get();
        $user = Auth::guard($this->guardType)->user();

        if ($user->role_id == '1') {
            $drivers = Driver::where('is_active', 1)->where('driver_has_vehicle', 1)->orderBy('name', 'ASC')->get();
            return view('backend.trucks.create', compact('drivers', 'truck_types', 'colors', 'model_types', 'agents'));
        } else {
            $drivers = Driver::where('agent_id', $user->agent_id)->where('is_active', 1)->orderBy('name', 'ASC')->get();
            return view('agent.trucks.create', compact('drivers', 'truck_types', 'colors', 'model_types', 'agents'));
        }
    }

    // Add New Truck
    public function store(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'model_name' => 'required',
            'plate_number' => 'required',
            'chassi_number' => 'required',
            'year' => 'required',
            'status' => 'required',
            'vehicle_type_id' => 'required',
            'color_id' => 'required',
            'model_type_id' => 'required',
//            'agent_id' => 'required',
            'license_end_date' => 'required',
            'image' => 'mimes:jpeg,png,jpg,gif',
            'license_image_front' => 'mimes:jpeg,png,jpg,gif',
            // 'license_start_date'      => 'required',
            // 'driver_id'               => 'required',
            // 'license_image_back'      => 'mimes:jpeg,png,jpg,gif',
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        $truck = new Truck();
        $user = Auth::guard($this->guardType)->user();
        $truck->model_name = $request->input("model_name");
        $truck->plate_number = $request->input("plate_number");
        $truck->chassi_number = $request->input("chassi_number");
        $truck->year = $request->input("year");
        $truck->vehicle_type_id = $request->input("vehicle_type_id");
        $truck->agent_id = $request->input("agent_id");
        $truck->color_id = $request->input("color_id");
        $truck->model_type_id = $request->input("model_type_id");
        $truck->driver_id = 0;
        $truck->license_end_date = $request->input("license_end_date");
        // $truck->license_start_date    = $request->input("license_start_date");

        if ($user->role_id == '1') {
            $truck->status = $request->input("status");
        } else {
            $truck->status = 0;
        }

        $counter = 0;

        if ($request->file("image") != "") {
            $counter++;
            $extension = File::extension($request->file("image")->getClientOriginalName());
            $image = 'truck-' . time() . $counter . "." . "jpg";
            $truck->image = $image;
            $request->file("image")->move(public_path('backend/images/'), $image);
        }

        if ($request->file("license_image_front") != "") {
            $counter++;
            $extension = File::extension($request->file("license_image_front")->getClientOriginalName());
            $image = 'truck-' . time() . $counter . "." . "jpg";
            $truck->license_image_front = $image;
            $request->file("license_image_front")->move(public_path('backend/images/'), $image);
        }

        if ($request->file("license_image_back") != "") {
            $counter++;
            $extension = File::extension($request->file("license_image_back")->getClientOriginalName());
            $image = 'truck-' . time() . $counter . "." . "jpg";
            $truck->license_image_back = $image;
            $request->file("license_image_back")->move(public_path('backend/images/'), $image);
        }

        $truck->save();
        Flash::success(__('backend.Truck_saved_successfully'));
        $user = Auth::guard($this->guardType)->user();
        if ($user->role_id == '1') {
            return redirect()->route('mngrAdmin.trucks.index');
        } else {
            return redirect()->route('mngrAgent.trucks.index');
        }
    }

    // View One Truck
    public function show($id)
    {
        $truck = Truck::findOrFail($id);
        $user = Auth::guard($this->guardType)->user();
        if ($user->role_id == '1') {
            return view('backend.trucks.show', compact('truck'));
        } else {
            return view('agent.trucks.show', compact('truck'));
        }
    }

    // Edit Truck Page
    public function edit($id)
    {
        $truck = Truck::findOrFail($id);
        $truck_types = VehicleType::orderBy('name_en', 'ASC')->get();
        $model_types = ModelType::orderBy('name', 'ASC')->get();
        $colors = Color::orderBy('name', 'ASC')->get();
        $agents = Agent::orderBy('name', 'ASC')->get();
        $user = Auth::guard($this->guardType)->user();

        if ($user->role_id == '1') {
            $drivers = Driver::where('is_active', 1)->where('driver_has_vehicle', 1)->orderBy('name', 'ASC')->get();
            return view('backend.trucks.edit', compact('truck', 'drivers', 'truck_types', 'colors', 'model_types', 'agents'));
        } else {
            $drivers = Driver::where('agent_id', $user->agent_id)->where('is_active', 1)->orderBy('name', 'ASC')->get();
            return view('agent.trucks.edit', compact('truck', 'drivers', 'truck_types', 'colors', 'model_types', 'agents'));
        }
    }

    // Update Truck
    public function update(Request $request, $id)
    {
        $v = \Validator::make($request->all(), [
            'model_name' => 'required',
            'plate_number' => 'required',
            'chassi_number' => 'required',
            'year' => 'required',
            'status' => 'required',
            'vehicle_type_id' => 'required',
            'color_id' => 'required',
            'model_type_id' => 'required',
//            'agent_id' => 'required',
            'license_end_date' => 'required',
            'license_image_front' => 'mimes:jpeg,png,jpg,gif',
            // 'image'                   => 'mimes:jpeg,png,jpg,gif',
            // 'license_start_date'      => 'required',
            // 'driver_id'               => 'required',
            // 'license_image_back'      => 'mimes:jpeg,png,jpg,gif',
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        $truck = Truck::findOrFail($id);
        $truck->model_name = $request->input("model_name");
        $truck->plate_number = $request->input("plate_number");
        $truck->chassi_number = $request->input("chassi_number");
        $truck->year = $request->input("year");
        $truck->status = $request->input("status");
        $truck->vehicle_type_id = $request->input("vehicle_type_id");
        $truck->color_id = $request->input("color_id");
        $truck->model_type_id = $request->input("model_type_id");
        $truck->driver_id = $request->input("driver_id");
        $truck->license_end_date = $request->input("license_end_date");
        // $truck->license_start_date  = $request->input("license_start_date");

        $counter = 0;


        if ($request->file("license_image_front") != "") {
            $counter++;
            $extension = File::extension($request->file("license_image_front")->getClientOriginalName());
            $image = 'truck-' . time() . $counter . "." . "jpg";
            $truck->license_image_front = $image;
            $request->file("license_image_front")->move(public_path('backend/images/'), $image);
        } elseif (!$request->input("license_image_front_old")) {
            flash(__('backend.Please_Enter_License_Image_Front'))->error();
            return redirect()->back();
        }

        // if ($request->file("image") != "") {
        //     $counter++;
        //     $extension                      = File::extension($request->file("image")->getClientOriginalName());
        //     $image                          = 'truck-' . time() . $counter . "." . "jpg";
        //     $truck->image                   = $image;
        //     $request->file("image")->move(public_path('backend/images/'), $image);
        // } elseif (!$request->input("image_old")) {
        //     flash(__('backend.Please_Enter_Veichle_Image'))->error();
        //     return redirect()->back();
        // }

        // if ($request->file("license_image_back") != "") {
        //     $counter++;
        //     $extension                      = File::extension($request->file("license_image_back")->getClientOriginalName());
        //     $image                          = 'truck-' . time() . $counter . "." . "jpg";
        //     $truck->license_image_back      = $image;
        //     $request->file("license_image_back")->move(public_path('backend/images/'), $image);
        // } elseif (!$request->input("license_image_back_old")) {
        //     flash(__('backend.Please_Enter_License_Image_Back'))->error();
        //     return redirect()->back();
        // }

        $truck->save();

        // If Driver Not Have Vehicle Update Column driver_has_vehicle = 0 And Have Update Column driver_has_vehicle = 1
        /* $driver = Driver::findOrFail($request->input('old_driver_id'));
        if (count($driver->truck) > 0) {
          $driver                       = Driver::findOrFail($request->input("driver_id"));
          $driver->driver_has_vehicle   = 1;
          $driver->save();
        } else {
          $driver                       = Driver::findOrFail($request->input('old_driver_id'));
          $driver->driver_has_vehicle   = 0;
          $driver->save();
        } */

        Flash::warning(__('backend.Truck_Updated_successfully'));
        $user = Auth::guard($this->guardType)->user();
        if ($user->role_id == '1') {
            return redirect()->route('mngrAdmin.trucks.index');
        } else {
            return redirect()->route('mngrAgent.trucks.index');
        }
    }

    public function DriverVehicle($id)
    {
        $vehicles = Truck::where('driver_id', $id)->get();
        return view('agent.drivers.vehicle', compact('vehicles'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {

        $truck = Truck::findOrFail($id);
        $truck->delete();

        // If Driver Not Have Vehicle Update Column driver_has_vehicle == 0
        /* $driver = Driver::findOrFail($truck->driver_id);
        if (count($driver->truck) > 0) {

        } else {
          $driver = Driver::findOrFail($driver->id);
          $driver->driver_has_vehicle = 0;
          $driver->save();
        } */

        flash(__('backend.Truck_deleted_successfully'))->error();
        $user = Auth::guard($this->guardType)->user();

        if ($user->role_id == '1') {
            return redirect()->route('mngrAdmin.trucks.index');
        } else {
            return redirect()->route('mngrAgent.trucks.index');
        }
        return redirect()->route('mngrAdmin.trucks.index');
    }

    function change_status($id)
    {
        $truck = Truck::findOrFail($id);
        if ($truck->status == 0) {
            $truck->status = 1;
        } else {
            $truck->status = 0;
        }
        $truck->save();
        return $truck->status;
    }
}

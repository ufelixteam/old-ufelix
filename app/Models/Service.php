<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    //
    public function getImageAttribute($value)
    {
        if($value != ""){
            $value=asset("/images/services/".$value);
        }else{
            $value = "";
        }
        return $value;
    }
   

    public function getPublishSpanAttribute($value)
    {
        if($this->is_publish == 1){
            $value="<span class='badge badge-pill label-success'>".__('backend.publish')."</span>";
        }else{
            $value="<span class='badge badge-pill  label-danger'>".__('backend.not_publish')."</span>";
        }
        return $value;
    }

    public function getTitleAttribute()
    {
        if(app()->getLocale() == 'ar'){
            $value= $this->title_ar;
        }else{
            $value= $this->title_en;
        }
        return $value;
    }

    public function getSmallDescriptionAttribute()
    {
        if(app()->getLocale() == 'ar'){
            $value= $this->small_description_ar;
        }else{
            $value= $this->small_description_en;
        }
        return $value;
    }

    public function getDescriptionAttribute()
    {
        if(app()->getLocale() == 'ar'){
            $value= $this->description_ar;
        }else{
            $value= $this->description_en;
        }
        return $value;
    }

    

    
}

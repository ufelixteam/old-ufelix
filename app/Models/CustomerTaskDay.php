<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerTaskDay extends Model
{
    protected $guarded = ['id'];

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }
}

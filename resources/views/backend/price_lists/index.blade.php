@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.Pricing_List')}}</title>
@endsection

@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.Pricing_List')}}
            @if(permission('addPriceList')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
              <a class="btn btn-success pull-right" href="{{ route('mngrAdmin.price_lists.create') }}"><i class="glyphicon glyphicon-plus"></i> {{__('backend.create')}}</a>
            @endif
        </h1>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($price_lists->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('backend.target')}}</th>
                            <th>{{__('backend.discount_percentage')}}</th>
                            <th class="text-right"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($price_lists as $price_list)
                            <tr>
                                <td>{{$price_list->id}}</td>
                                <td>{{$price_list->target}}</td>
                                <td>% {{$price_list->discount_percentage}}</td>
                                <td class="text-right">
                                    <!-- <a class="btn btn-xs btn-primary" href="{{ route('mngrAdmin.price_lists.show', $price_list->id) }}"><i class="glyphicon glyphicon-eye-open"></i> {{__('backend.view')}}</a> -->
                                    @if(permission('edditPriceList')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                      <a class="btn btn-xs btn-warning" href="{{ route('mngrAdmin.price_lists.edit', $price_list->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                                    @endif

                                    @if(permission('deletePriceList')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                      <form action="{{ route('mngrAdmin.price_lists.destroy', $price_list->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
                                          <input type="hidden" name="_method" value="DELETE">
                                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                          <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}</button>
                                      </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {!! $price_lists->appends($_GET)->links() !!}

            @else
                <h3 class="text-center alert alert-info">{{__('backend.No_result_found')}}</h3>
            @endif
        </div>
    </div>

@endsection

<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use App\Models\Governorate;
use App\Models\GovernoratePrice;
use App\Models\Order;
use Auth;
use Flash;
use Illuminate\Http\Request;
use Response;

class governoratePriceController extends Controller
{

    // Get Cities By Country ID
    public function get_governorate($id)
    {
        return Governorate::where('country_id', $id)->get();
    }

    // Get All Governorate Price
    public function index()
    {
        $governoratePrices = GovernoratePrice::orderBy('id', 'desc')->get();
        return view('backend.governoratePrice.index', compact('governoratePrices'));
    }

    // Create Governorate Price Page
    public function create()
    {
        $list_governorates = Governorate::orderBy('name_en', 'ASC')->get();
        return view('backend.governoratePrice.create', compact('list_governorates'));
    }

    // Added New Governorate Price
    public function store(Request $request)
    {
        /*
        * status = 0 :: Not Verify
        * status = 1 :: Verify
        */

        $v = \Validator::make($request->all(), [
            'start_station' => 'required',
            'access_station' => 'required',
            'cost' => 'required',
            'overweight_cost' => 'required',
            'status' => 'required',
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        $governoratePrice = new GovernoratePrice();
        $governoratePrice->start_station = $request->input("start_station");
        $governoratePrice->access_station = $request->input("access_station");
        $governoratePrice->cost = $request->input("cost");
        $governoratePrice->recall_cost = $request->input("recall_cost");
        $governoratePrice->reject_cost = $request->input("reject_cost");
        $governoratePrice->cancel_cost = $request->input("cancel_cost");
        $governoratePrice->overweight_cost = $request->input("overweight_cost");
        $governoratePrice->status = $request->input("status");
        $governoratePrice->save();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Add New Shipment Destinations #No ' . $governoratePrice->id, ' اضافة وجهة شحن جديدة رقم ' . $governoratePrice->id, 11); // Helper Function in App\Helpers.php

        Flash::success(__('backend.Destination_Added_successfully'));
        return redirect()->route('mngrAdmin.governoratePrices.index');
    }

    // View One Governorate Price
    public function show($id)
    {
        $governorate = Governorate::findOrFail($id);
        return view('backend.governorates.show', compact('governorate'));
    }

    // Edit Governorate Price Page
    public function edit($id)
    {
        $governoratePrice = GovernoratePrice::findOrFail($id);
        $list_governorates = Governorate::orderBy('name_en', 'ASC')->get();
        return view('backend.governoratePrice.edit', compact('governoratePrice', 'list_governorates'));
    }

    // Update Governorate Price
    public function update(Request $request, $id)
    {
        /*
        * status = 0 :: Not Verify
        * status = 1 :: Verify
        */

        $v = \Validator::make($request->all(), [
            'start_station' => 'required',
            'access_station' => 'required',
            'cost' => 'required',
            'overweight_cost' => 'required',
            'status' => 'required',
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        $governoratePrice = GovernoratePrice::findOrFail($id);
        $governoratePrice->start_station = $request->input("start_station");
        $governoratePrice->access_station = $request->input("access_station");
        $governoratePrice->cost = $request->input("cost");
        $governoratePrice->recall_cost = $request->input("recall_cost");
        $governoratePrice->reject_cost = $request->input("reject_cost");
        $governoratePrice->cancel_cost = $request->input("cancel_cost");
        $governoratePrice->overweight_cost = $request->input("overweight_cost");
        $governoratePrice->status = $request->input("status");
        $governoratePrice->save();

        // Function Add Action In Activity Log By Samir
        addActivityLog('Edit Shipment Destinations #No ' . $governoratePrice->id, ' تعديل واجهة الشحن رقم  ' . $governoratePrice->id, 11); // Helper Function in App\Helpers.php

        Flash::warning(__('backend.Destination_Updated_successfully'));
        return redirect()->route('mngrAdmin.governoratePrices.index');
    }

    // Delete Governorate Price
    public function destroy($id)
    {
        $governoratePrice = GovernoratePrice::findOrFail($id);
        $OrderGovernoratePrice = Order::where('governorate_cost_id', $id)->first();
        if ($OrderGovernoratePrice == null) { // If Not Have Orders Governorate Price Deleted
            $governoratePrice->delete();

            // Function Add Action In Activity Log By Samir
            addActivityLog('Delete Shipment Destinations #No ' . $governoratePrice->id, ' حذف واجهة الشحن رقم ' . $governoratePrice->id, 11); // Helper Function in App\Helpers.php

            flash(__('backend.Destination_deleted_successfully'))->error();
            return redirect()->route('mngrAdmin.governoratePrices.index');
        } else { // If Haves Orders Governorate Price Not Deleted
            Flash::error(__('backend.Can_not_delete_Destination_because_of_Order_Dependency'));
            return redirect()->back();
        }
    }

    public function save_cost(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'cost' => 'required|numeric',
            'recall_cost' => 'required|numeric',
            'reject_cost' => 'required|numeric',
            'cancel_cost' => 'required|numeric',
            'id' => 'required',
        ]);

        if ($v->fails()) {
            return [false];
        }

        GovernoratePrice::where('id', $request->input("id"))
            ->update([
                'cost' => $request->input("cost"),
                'recall_cost' => $request->input("recall_cost"),
                'reject_cost' => $request->input("reject_cost"),
                'cancel_cost' => $request->input("cancel_cost")
            ]);

        return [true];
    }
}

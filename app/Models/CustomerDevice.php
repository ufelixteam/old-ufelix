<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerDevice extends Model
{
  protected $fillable = [
    'customer_id', 'version_id', 'token', 'device_id', 'online', 'socket_id', 'status'
  ];

  public function Customer()
  {
    return $this->belongsTo('App\Modles\Customer');
  }

  public function Version()
  {
    return $this->belongsTo('App\Models\Version');
  }
}

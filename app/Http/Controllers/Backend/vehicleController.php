<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\VehicleType;
use Response;
use Flash;
use File;
use Auth;

class vehicleController extends Controller {

	// Get All Vehicle Type
	public function index() {
		$types = VehicleType::orderBy('id', 'desc')->paginate(50);
		return view('backend.VehicleType.index', compact('types'));
	}

	// Create Vehicle Type Page
	public function create() {
		return view('backend.VehicleType.create');
	}

	// Added New Vehicle Type
	public function store(Request $request) {
			/*
			* status = 0 :: Not Active
			* status = 1 :: Active
			*/
			$this->validate($request,[
					'name_ar'   		=>'required',
					'name_en'       =>'required',
					'price'         =>'required',
					'status'    		=>'required',
			]);
			$type 						= new VehicleType();
			$type->name_en 		= $request->input("name_en");
			$type->name_en 		= $request->input("name_ar");
			$type->price     	= $request->input("price");
    	$type->status 		= $request->input("status");
			$type->save();

			// Function Add Action In Activity Log By Samir
			addActivityLog('Add New Vehicle Type ' . $type->name_en , ' اضافة نوع مركبة جديدة ' . $type->name_en, 23); // Helper Function in App\Helpers.php

    	Flash::success(__('backend.Vehicle_Type_saved_successfully'));
			return redirect('mngrAdmin/vehicle');
	}

	// View One Vehicle Type
	public function show($id) {
		$type = VehicleType::findOrFail($id);
		return view('backend.VehicleType.show', compact('type'));
	}

	// Edit Vehicle Type Page
	public function edit($id) {
		$type = VehicleType::findOrFail($id);
		return view('backend.VehicleType.edit', compact('type'));
	}

	// Update Vehicle Type
	public function update(Request $request, $id) {
			/*
			* status = 0 :: Not Active
			* status = 1 :: Active
			*/
			$this->validate($request,[
					'name_ar'   		=>'required',
					'name_en'       =>'required',
					'price'         =>'required',
					'status'    		=>'required',
			]);
			$type 						= VehicleType::findOrFail($id);
			$type->name_ar 		= $request->input("name_ar");
			$type->name_en 		= $request->input("name_en");
			$type->price 			= $request->input("price");
    	$type->status 		= $request->input("status");
			$type->save();

			// Function Add Action In Activity Log By Samir
			addActivityLog('Edit Vehicle Type ' . $type->name_en , ' تعديل نوع المركبة ' . $type->name_en, 23); // Helper Function in App\Helpers.php

    	Flash::warning(__('backend.Vehicle_Type_Updated_successfully'));
			return redirect('mngrAdmin/vehicle');
	}

	// Delete Vehicle Type
	public function destroy($id) {
			$type = VehicleType::findOrFail($id);
			$type->delete();

			// Function Add Action In Activity Log By Samir
			addActivityLog('Delete Vehicle Type ' . $type->name_en , ' حذف نوع المركبة ' . $type->name_en, 23); // Helper Function in App\Helpers.php

    	flash(__('backend.Vehicle_Type_deleted_successfully'))->error();
			return back();
	}

}

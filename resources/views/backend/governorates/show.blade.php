@extends('backend.layouts.app')
@section('header')
<div class="page-header">
        <h3>{{__('backend.view')}} #{{$governorate->id}}</h3> {!! $governorate->status_span !!}
        <form action="{{ route('mngrAdmin.cities.destroy', $governorate->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm({{__('backend.msg_confirm_delete')}})) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('mngrAdmin.cities.edit', $governorate->id) }}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>
                <button type="submit" class="btn btn-danger"> {{__('backend.delete')}} <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <div class="form-group">
                <label for="nome">#</label>
                <p class="form-control-static"></p>
            </div>
            <div class="form-group col-sm-6">
                     <label for="city">{{__('backend.governorate')}}</label>
                     <p class="form-control-static">{{$governorate->name}}</p>
                </div>
                    <div class="form-group col-sm-6">
                     <label for="code">{{__('backend.code')}}</label>
                     <p class="form-control-static">{{$governorate->code}}</p>
                </div>
                    <div class="form-group col-sm-6">
                     <label for="country_id">{{__('backend.country')}}</label>
                     <p class="form-control-static">{{ ! empty($governorate->country) ?  $governorate->country->name : ''}}</p>
                </div>


            <div class="col-md-12 col-xs-12 col-sm-12">
                <a class="btn btn-link" href="{{ route('mngrAdmin.cities.index') }}"><i class="glyphicon glyphicon-backward"></i>  {{__('backend.back')}}</a>
            </div>

        </div>
    </div>

@endsection

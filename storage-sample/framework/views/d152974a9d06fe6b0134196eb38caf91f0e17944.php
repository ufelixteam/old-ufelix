

<?php $__env->startSection('css'); ?>
  <style>
    .error_vehicle{
      display: inline-block;
      color: rgb(255, 255, 255);
      background-color: #9c3737;
      width: 100%;
      padding: 5px;
      margin-top: 3px;
      border-radius: 2px;
      text-align: center;
      font-weight: 500;
    }
  </style>
  <title><?php echo e(__('backend.drivers')); ?> - <?php echo e(__('backend.add_driver')); ?></title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
  <div class="page-header">
    <h3><i class="glyphicon glyphicon-plus"></i> <?php echo e(__('backend.drivers')); ?> / <?php echo e(__('backend.add_driver')); ?> </h3>
  </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <form action="<?php echo e(route('mngrAdmin.drivers.store')); ?>" method="POST" enctype="multipart/form-data" id="driver_create" name="driver_create">
        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
      
          <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('name')): ?> has-error <?php endif; ?>">
            <label for="name-field"><?php echo e(__('backend.name')); ?>: </label>
            <input type="text" id="name-field" name="name" class="form-control" value="<?php echo e(old("name")); ?>"/>
            <?php if($errors->has("name")): ?>
              <span class="help-block"><?php echo e($errors->first("name")); ?></span>
            <?php endif; ?>
          </div>
          <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('email')): ?> has-error <?php endif; ?>">
              <label for="email-field"><?php echo e(__('backend.email')); ?>: </label>
              <input type="email" id="email-field" name="email" class="form-control" value="<?php echo e(old("email")); ?>"/>
              <?php if($errors->has("email")): ?>
                <span class="help-block"><?php echo e($errors->first("email")); ?></span>
              <?php endif; ?>
          </div>
          <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('mobile')): ?> has-error <?php endif; ?>">
            <label for="mobile-field"><?php echo e(__('backend.mobile_number')); ?>: </label>
            <input type="text" id="mobile-field" maxlength="11" name="mobile" class="form-control" value="<?php echo e(old("mobile")); ?>"/>
            <?php if($errors->has("mobile")): ?>
              <span class="help-block"><?php echo e($errors->first("mobile")); ?></span>
            <?php endif; ?>
          </div>
   
          <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('phone')): ?> has-error <?php endif; ?>">
            <label for="phone-field"><?php echo e(__('backend.mobile_number2')); ?>:</label>
            <input type="text" id="phone-field" name="phone"  maxlength="11"  class="form-control" value="<?php echo e(old("phone") ? old("phone") : '0100'); ?>"/>
            <?php if($errors->has("phone")): ?>
              <span class="help-block"><?php echo e($errors->first("phone")); ?></span>
            <?php endif; ?>
          </div>
          <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('password')): ?> has-error <?php endif; ?>">
            <label for="password-field"><?php echo e(__('backend.password')); ?>: </label>
            <input type="password" id="password-field" name="password" class="form-control" />
            <?php if($errors->has("password")): ?>
              <span class="help-block"><?php echo e($errors->first("password")); ?></span>
            <?php endif; ?>
          </div>
          <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('password')): ?> has-error <?php endif; ?>">
            <label for="confirm_password-field"><?php echo e(__('backend.confirm_password')); ?>: </label>
            <input type="password" id="confirm_password-field" name="confirm_password" class="form-control" />
            <?php if($errors->has("confirm_password")): ?>
              <span class="help-block"><?php echo e($errors->first("confirm_password")); ?></span>
            <?php endif; ?>
          </div>
 
          <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('country_id')): ?> has-error <?php endif; ?>">
            <label for="country_id-field"><?php echo e(__('backend.choose_country')); ?>: </label>
            <select id="country_id-field" name="country_id" class="form-control">
              <?php if(! empty($countries)): ?>
                <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <option value="<?php echo e($country->id); ?>" <?php echo e(old("country_id") == $country->id ? 'selected' : ''); ?>

                    ><?php echo e($country->name); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              <?php endif; ?>
            </select>
          </div>
          <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('government_id')): ?> has-error <?php endif; ?>">
            <label for="government_id-field"> <?php echo e(__('backend.government')); ?>: </label>
            <select  class="form-control government_id" id="government_id"  name="government_id">
                <option value="" data-display="Select"><?php echo e(__('backend.government')); ?></option>
                <?php if(! empty($governments)): ?>
                    <?php $__currentLoopData = $governments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $government): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($government->id); ?>" >
                          <?php echo e($government->name_en); ?>

                        </option>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            </select>
            <?php if($errors->has("government_id")): ?>
            <span class="help-block"><?php echo e($errors->first("government_id")); ?></span>
            <?php endif; ?>
          </div>
          <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('city_id')): ?> has-error <?php endif; ?>">
            <label class="control-label" for="s_state_id"><?php echo e(__('backend.city')); ?></label>
            <select  class=" form-control city_id" id="city_id"  name="city_id" >
                <option value="Null" ><?php echo e(__('backend.city')); ?></option>
            </select>
            <?php if($errors->has("city_id")): ?>
            <span class="help-block"><?php echo e($errors->first("city_id")); ?></span>
            <?php endif; ?>
          </div>
        
          <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('address')): ?> has-error <?php endif; ?>">
            <label for="address-field"><?php echo e(__('backend.address')); ?>: </label>
            <input type="text" id="address-field" name="address" class="form-control" value="<?php echo e(old("address")); ?>"/>
            <?php if($errors->has("address")): ?>
              <span class="help-block"><?php echo e($errors->first("address")); ?></span>
            <?php endif; ?>
          </div>
          <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('is_active')): ?> has-error <?php endif; ?>">
            <label for="is_active-field"><?php echo e(__('backend.isactive_verified_notverified')); ?>:</label>
            <select id="is_active-field" name="is_active" class="form-control" >
              <option value="0" <?php echo e(old("is_active") == "0" ? 'selected' : ''); ?>><?php echo e(__('backend.no')); ?></option>
              <option value="1" <?php echo e(old("is_active") == "1" ? 'selected' : ''); ?>><?php echo e(__('backend.yes')); ?></option>
            </select>
            <?php if($errors->has("is_active")): ?>
              <span class="help-block"><?php echo e($errors->first("is_active")); ?></span>
            <?php endif; ?>
          </div>
          <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('agent_id')): ?> has-error <?php endif; ?>">
            <label for="agent_id-field"><?php echo e(__('backend.choose_beloning_agent')); ?>: </label>
            <select id="agent_id-field" name="agent_id" id="agent_id" class="form-control">
             <!-- <option value="0">Choose Beloning Agent: </option> -->
              <?php if(! empty($agents)): ?>
                  <?php $__currentLoopData = $agents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $agent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($agent->id); ?>" <?php echo e(old("agent_id") == $agent->id ? 'selected' : ''); ?>

                      ><?php echo e($agent->name); ?></option>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              <?php endif; ?>
            </select>
            <?php if($errors->has("agent_id")): ?>
              <span class="help-block"><?php echo e($errors->first("agent_id")); ?></span>
            <?php endif; ?>
          </div>

          
          
          <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('notes')): ?> has-error <?php endif; ?>">
            <label for="notes-field"><?php echo e(__('backend.notes')); ?>: </label>
            <input type="text" id="notes-field" name="notes" class="form-control" value="<?php echo e(old("notes")); ?>"/>
            <?php if($errors->has("notes")): ?>
              <span class="help-block"><?php echo e($errors->first("notes")); ?></span>
            <?php endif; ?>
          </div>
          
   
          <div class="form-group financial col-md-4 col-sm-4 <?php if($errors->has('basic_salary')): ?> has-error <?php endif; ?>">
            <label for="basic-salary-field"><?php echo e(__('backend.basic_salary')); ?>: </label>
            <input type="number"  id="basic-salary-field" min="0" name="basic_salary" class="form-control" value="<?php echo e(old("basic_salary") ? old("basic_salary") : '0'); ?>"/>
            <?php if($errors->has("basic_salary")): ?>
              <span class="help-block"><?php echo e($errors->first("basic_salary")); ?></span>
            <?php endif; ?>
          </div>
          <div class="form-group financial col-md-4 col-sm-4 <?php if($errors->has('bouns_of_delivery')): ?> has-error <?php endif; ?>">
            <label for="bouns-of-delivery-field"><?php echo e(__('backend.bonus_of_delivery')); ?>: </label>
            <input type="number"  id="bouns-of-delivery-field" min="0" name="bouns_of_delivery" class="form-control" value="<?php echo e(old("bouns_of_delivery") ? old("bouns_of_delivery") : '0'); ?>"/>
            <?php if($errors->has("bouns_of_delivery")): ?>
              <span class="help-block"><?php echo e($errors->first("bouns_of_delivery")); ?></span>
            <?php endif; ?>
          </div>
   
          <div class="form-group financial col-md-4 col-sm-4 <?php if($errors->has('pickup_price')): ?> has-error <?php endif; ?>">
            <label for="pickup-price-field"><?php echo e(__('backend.pickup_price')); ?>: </label>
            <input type="number"  id="pickup-price-field" min="0" name="pickup_price" class="form-control" value="<?php echo e(old("pickup_price") ? old("pickup_price") : '0'); ?>"/>
            <?php if($errors->has("pickup_price")): ?>
              <span class="help-block"><?php echo e($errors->first("pickup_price")); ?></span>
            <?php endif; ?>
          </div>
          <div class="form-group financial col-md-4 col-sm-4 <?php if($errors->has('bouns_of_pickup_for_one_order')): ?> has-error <?php endif; ?>">
            <label for="bouns-of-pickup-for-one-order-field"><?php echo e(__('backend.bonus_of_pickup_for_one_order')); ?>: </label>
            <input type="number"  id="bouns-of-pickup-for-one-order-field" min="0" name="bouns_of_pickup_for_one_order" class="form-control" value="<?php echo e(old("bouns_of_pickup_for_one_order") ? old("bouns_of_pickup_for_one_order") : '0'); ?>"/>
            <?php if($errors->has("bouns_of_pickup_for_one_order")): ?>
              <span class="help-block"><?php echo e($errors->first("bouns_of_pickup_for_one_order")); ?></span>
            <?php endif; ?>
          </div>
          <div class="form-group financial col-md-4 col-sm-4 <?php if($errors->has('profit')): ?> has-error <?php endif; ?>">
            <label for="profit-field"><?php echo e(__('backend.profit_rate')); ?>: </label>
            <input type="number"  id="profit-field" min="0" name="profit" class="form-control" value="<?php echo e(old("profit") ? old("profit") : '0'); ?>"/>
            <?php if($errors->has("profit")): ?>
              <span class="help-block"><?php echo e($errors->first("profit")); ?></span>
            <?php endif; ?>
          </div>

          <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('national_id_number')): ?> has-error <?php endif; ?>">
            <label for="national_id_number-field"><?php echo e(__('backend.national_id_number')); ?></label>
            <input type="text" id="national_id_number-field" name="national_id_number" maxlength="14"  class="form-control" value="<?php echo e(old("national_id_number")); ?>"/>
            <?php if($errors->has("national_id_number")): ?>
              <span class="help-block"><?php echo e($errors->first("national_id_number")); ?></span>
            <?php endif; ?>
          </div>
       
          <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('national_id_expired_date')): ?> has-error <?php endif; ?>">
            <label for="national_id_expired_date-field"><?php echo e(__('backend.national_id_expired_date')); ?></label>
            <div class='input-group date datepicker2'>
              <input type="text" id="national_id_expired_date-field" name="national_id_expired_date" class="form-control" value="<?php echo e(old("national_id_expired_date")); ?>"/>
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
            </div>
            <?php if($errors->has("national_id_expired_date")): ?>
              <span class="help-block"><?php echo e($errors->first("national_id_expired_date")); ?></span>
            <?php endif; ?>
          </div>

          <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('driver_has_vehicle')): ?> has-error <?php endif; ?>">
            <label for="driver-has-vehicle-field"><?php echo e(__('backend.driver_has_vehicle')); ?></label>
            <select id="driver-has-vehicle-field" name="driver_has_vehicle" class="form-control">
              <option value=""><?php echo e(__('backend.choose_yes_or_no')); ?></option>
              <option value="0" <?php echo e(old("driver_has_vehicle") == "0" ? 'selected' : ''); ?>><?php echo e(__('backend.no')); ?></option>
              <option value="1" <?php echo e(old("driver_has_vehicle") == "1" ? 'selected' : ''); ?>><?php echo e(__('backend.yes')); ?></option>
            </select>
            <?php if($errors->has("driver_has_vehicle")): ?>
              <span class="help-block"><?php echo e($errors->first("driver_has_vehicle")); ?></span>
            <?php endif; ?>
          </div>
          <div class=" col-md-12">

          <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('driver_profile')): ?> has-error <?php endif; ?>">
            <div id="driver_profile_upload"></div>
            <label for="driver_profile-field"><?php echo e(__('backend.profile_image')); ?>: </label>
            <input required class="uploadPhoto btn btn-primary" type="file" name="driver_profile" id="driver_profile">
            <?php if($errors->has("driver_profile")): ?>
              <span class="help-block"><?php echo e($errors->first("driver_profile")); ?></span>
            <?php endif; ?>
          </div>
          <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('national_id_image_front')): ?> has-error <?php endif; ?>">
            <div id="national_id_image_front_upload"></div>
            <label for="national_id_image_front-field"><?php echo e(__('backend.national_id_image_front')); ?>:</label>
            <input required class="uploadPhoto btn btn-primary" type="file" name="national_id_image_front" id="national_id_image_front">
            <?php if($errors->has("national_id_image_front")): ?>
              <span class="help-block"><?php echo e($errors->first("national_id_image_front")); ?></span>
            <?php endif; ?>
          </div>
          <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('criminal_record_image_front')): ?> has-error <?php endif; ?>">
            <div id="criminal_record_image_front_upload"></div>
            <label for="criminal_record_image_front-field"><?php echo e(__('backend.criminal_record_image')); ?>: </label>
            <input required class="uploadPhoto btn btn-primary" type="file" name="criminal_record_image_front" id="criminal_record_image_front">
            <?php if($errors->has("criminal_record_image_front")): ?>
              <span class="help-block"><?php echo e($errors->first("criminal_record_image_front")); ?></span>
            <?php endif; ?>
          </div>
        </div>

          <div class="has_vechile col-md-12" hidden>
            <div class="form-group col-md-6 col-sm-6 <?php if($errors->has('model_name')): ?> has-error <?php endif; ?>">
              <label for="model_name-field"><?php echo e(__('backend.Model_Name')); ?>: </label>
              <input type="text" id="model_name-field" name="model_name" class="form-control" value="<?php echo e(old("model_name")); ?>"/>
              <?php if($errors->has("model_name")): ?>
                <span class="help-block"><?php echo e($errors->first("model_name")); ?></span>
              <?php endif; ?>
            </div>
            <div class="form-group col-md-6 col-sm-6 <?php if($errors->has('plate_number')): ?> has-error <?php endif; ?>">
              <label for="plate_number-field"><?php echo e(__('backend.Plate_Number')); ?>: </label>
              <input type="text" id="plate_number-field" name="plate_number" class="form-control" value="<?php echo e(old("plate_number")); ?>"/>
              <?php if($errors->has("plate_number")): ?>
                <span class="help-block"><?php echo e($errors->first("plate_number")); ?></span>
              <?php endif; ?>
            </div>
      
            <div class="form-group col-md-6 col-sm-6 <?php if($errors->has('chassi_number')): ?> has-error <?php endif; ?>">
               <label for="chassi_number-field"><?php echo e(__('backend.Chassi_Number')); ?>: </label>
               <input type="text" id="chassi_number-field" name="chassi_number" class="form-control" value="<?php echo e(old("chassi_number")); ?>"/>
               <?php if($errors->has("chassi_number")): ?>
                 <span class="help-block"><?php echo e($errors->first("chassi_number")); ?></span>
               <?php endif; ?>
            </div>
            <div class="form-group col-md-6 col-sm-6 <?php if($errors->has('year')): ?> has-error <?php endif; ?>">
               <label for="year-field"><?php echo e(__('backend.Model_Year')); ?>: </label>
               <?php echo e(Form::selectYear('year',  date('Y'),1980,null,["class"=>"form-control", "id"=>"year-field"])); ?>

               <?php if($errors->has("year")): ?>
                <span class="help-block"><?php echo e($errors->first("year")); ?></span>
               <?php endif; ?>
            </div>
    
            <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('type_id')): ?> has-error <?php endif; ?>">
              <label for="type_id-field"><?php echo e(__('backend.Vehicle_Type')); ?>: </label>
              <select id="vehicle_type_id-field" name="vehicle_type_id" class="form-control" >
                <?php if(! empty($truck_types)): ?>
                    <?php $__currentLoopData = $truck_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <option value="<?php echo e($type->id); ?>" <?php echo e(old("vehicle_type_id") == $type->id ? 'selected' : ''); ?>"><?php echo e($type->name_en); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
              </select>
              <?php if($errors->has("vehicle_type_id")): ?>
                <span class="help-block"><?php echo e($errors->first("vehicle_type_id")); ?></span>
              <?php endif; ?>
            </div>
            <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('model_type_id')): ?> has-error <?php endif; ?>">
              <label for="model_type_id-field"><?php echo e(__('backend.Model_Type')); ?></label>
              <select id="model_type_id-field" name="model_type_id" class="form-control" >
              <?php if(! empty($model_types)): ?>
                <?php $__currentLoopData = $model_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $model): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <option value="<?php echo e($model->id); ?>" <?php echo e(old("model_type_id") == $model->id ? 'selected' : ''); ?>"><?php echo e($model->name); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              <?php endif; ?>
              </select>
              <?php if($errors->has("model_type_id")): ?>
                <span class="help-block"><?php echo e($errors->first("model_type_id")); ?></span>
              <?php endif; ?>
            </div>
            <div class="form-group col-md-4 col-sm-4 <?php if($errors->has('color_id')): ?> has-error <?php endif; ?>">
              <label for="color_id-field"><?php echo e(__('backend.color')); ?>: </label>
              <select id="color_id-field" name="color_id" class="form-control" >
              <?php if(! empty($colors)): ?>
                <?php $__currentLoopData = $colors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $color): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <option value="<?php echo e($color->id); ?>" <?php echo e(old("color_id") == $color->id ? 'selected' : ''); ?>"><?php echo e($color->name); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              <?php endif; ?>
              </select>
              <?php if($errors->has("color_id")): ?>
                <span class="help-block"><?php echo e($errors->first("color_id")); ?></span>
              <?php endif; ?>
            </div>
          
            <div class="form-group col-md-6 col-sm-6 <?php if($errors->has('driving_licence_expired_date')): ?> has-error <?php endif; ?>">
              <label for="driving_licence_expired_date-field"><?php echo e(__('backend.drivier_licence_expiration_date')); ?>: </label>
              <div class='input-group date datepicker2' >
                <input type="text" id="driving_licence_expired_date-field" name="driving_licence_expired_date"
                       class="form-control" value="<?php echo e(old("driving_licence_expired_date")); ?>"/>
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
              </div>
              <?php if($errors->has("driving_licence_expired_date")): ?>
                <span class="help-block"><?php echo e($errors->first("driving_licence_expired_date")); ?></span>
              <?php endif; ?>
            </div>
            <div class="form-group col-md-6 col-sm-6 <?php if($errors->has('license_end_date')): ?> has-error <?php endif; ?>">
              <label for="license_end_date_field"><?php echo e(__('backend.License_End_Date')); ?>: </label>
              <div class='input-group date datepicker2' >
                <input type="text" id="license_end_date_field" name="license_end_date" class="form-control"  value="<?php echo e(old("license_end_date")); ?>"/>
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
              </div>
              <?php if($errors->has("license_end_date")): ?>
                <span class="help-block"><?php echo e($errors->first("license_end_date")); ?></span>
                <?php endif; ?>
            </div>
        
            <div class="form-group col-md-6 col-sm-6  <?php if($errors->has('driving_licence_image_front')): ?> has-error <?php endif; ?>">
              <div id="driving_licence_image_front_upload"></div>
              <label for="driving_licence_image_front-field"><?php echo e(__('backend.drivier_licence_image_front')); ?>: </label>
              <input required class="uploadPhoto btn btn-primary col-md-12" type="file" name="driving_licence_image_front" id="driving_licence_image_front">
              <?php if($errors->has("driving_licence_image_front")): ?>
                <span class="help-block"><?php echo e($errors->first("driving_licence_image_front")); ?></span>
              <?php endif; ?>
            </div>
            <div class="form-group col-md-6 col-sm-6 <?php if($errors->has('license_image_front')): ?> has-error <?php endif; ?>">
              <div id="license_image_front_upload"></div>
              <label for="image-field"><?php echo e(__('backend.License_Image_Front')); ?>:</label>
              <input required class="uploadPhoto btn btn-primary col-md-12" type="file" name="license_image_front" id="license_image_front">
              <?php if($errors->has("license_image_front")): ?>
                <span class="help-block"><?php echo e($errors->first("license_image_front")); ?></span>
              <?php endif; ?>
            </div>
          </div>

        <div class="well well-sm col-md-12 col-sm-12 col-xs-12">
            <button type="submit" id="added_driver" class="btn btn-primary"><?php echo e(__('backend.add_driver')); ?></button>
            <a class="btn btn-link pull-right" href="<?php echo e(route('mngrAdmin.drivers.index')); ?>"><i class="glyphicon glyphicon-backward"></i> <?php echo e(__('backend.back')); ?></a>
        </div>

      </form>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(function () {

            $("form[name='driver_create']").validate({
                // Specify validation rules
                rules: {
                    name: {
                        required: true,
                    },
                    email: {
                        required: true,
                        email: true,
                        remote: {
                            url: "<?php echo url('/mngrAdmin/check-email-driver'); ?>",
                            type: "get"
                        }
                    },
                    mobile: {
                        required: true,
                        number:true,
              	        rangelength:[11,11],
                        remote: {
                            url: "<?php echo url('/mngrAdmin/check-mobile-driver'); ?>",
                            type: "get"
                        }
                    },
                    phone: {
                        number:true,
                        rangelength:[11,11],
                        remote: {
                            url: "<?php echo url('/mngrAdmin/check-phone-driver'); ?>",
                            type: "get"
                        }
                    },
                    password: {
                        required: true,
                        minlength: 5
                    },
                    confirm_password: {
                      required: true,
                      equalTo: '#password-field',
                      minlength: 5
                    },
                    country_id: {
                      required: true,
                    },
                    government_id: {
                      required: true,
                    },
                    city_id: {
                      required: true,
                    },
                    address: {
                        required: true,
                    },
                    agent_id: {
                        required: true,
                    },
                    national_id_number: {
                        number:true,
                        rangelength:[14,14],
                    },
                    national_id_expired_date: {
                        date:true,
                      //  required: true,
                    },
                    profit: {
                        //required: true,
                        number:true,
                    },
                    basic_salary: {
                        //required: true,
                        number:true,
                    },
                    bouns_of_delivery: {
                        // required: true,
                        number:true,
                    },
                    pickup_price: {
                        // required: true,
                        number:true,
                    },
                    bouns_of_pickup_for_one_order: {
                        // required: true,
                        number:true,
                    },
                    driver_has_vehicle: {
                        required: true,
                    },
                    driver_profile: {
                      required: true,
                    },
                    national_id_image_front: {
                        required: true,
                    },
                    criminal_record_image_front: {
                        required: true,
                    },
                    // driving_licence_image_front: {
                    //     required: true,
                    // },
                    // driving_licence_image_back: {
                    //     required: true,
                    // },
                    // national_id_image_back: {
                    //     required: true,
                    // },
                },

                // Specify validation error messages
                messages: {
                    name: {
                      required: "<?php echo e(__('backend.Please_Enter_driver_Name')); ?>",
                    },
                    email: {
                        required: "<?php echo e(__('backend.Please_Enter_Driver_E-mail')); ?>",
                        email: "<?php echo e(__('backend.Please_Enter_Correct_E-mail')); ?>",
                        remote: jQuery.validator.format("<?php echo e(__('backend.E-mail_already_exist')); ?>")
                    },
                    mobile: {
                        rangelength:"<?php echo e(__('backend.Mobile_Must_Be11_Digits')); ?>",
                        required: "<?php echo e(__('backend.Please_Enter_Driver_Mobile_Number')); ?>",
                        number: "<?php echo e(__('backend.Please_Enter_Avalid_Number')); ?>",
                        remote: jQuery.validator.format("<?php echo e(__('backend.Mobile_Number_already_exist')); ?>")
                    },
                    phone: {
                        number: "<?php echo e(__('backend.Please_Enter_Avalid_Number')); ?>",
                        rangelength:"<?php echo e(__('backend.Mobile_Must_Be11_Digits')); ?>",
                        remote: jQuery.validator.format("<?php echo e(__('backend.Mobile_Number_already_exist')); ?>")
                    },
                    password: {
                        required: "<?php echo e(__('backend.Please_Enter_Driver_Password')); ?>",
                        minlength: "<?php echo e(__('backend.Password_must_be_more_than5_character')); ?>"
                    },
                    confirm_password: {
                        required: "<?php echo e(__('backend.Confirm_password_is_wrong')); ?>",
                        equalTo: "<?php echo e(__('backend.Confirm_password_is_wrong')); ?>",
                    },
                    country_id: {
                      required: "<?php echo e(__('backend.Please_Chosse_The_Country')); ?>",
                    },
                    government_id: {
                      required: "<?php echo e(__('backend.Please_Chosse_The_Government')); ?>",
                    },
                    city_id: {
                      required: "<?php echo e(__('backend.Please_Select_City')); ?>",
                    },
                    address: {
                        required: "<?php echo e(__('backend.Please_Enter_Driver_Address')); ?>",
                    },
                    latitude: {
                        required: "<?php echo e(__('backend.Please_Enter_Latitude')); ?>",
                        number:"<?php echo e(__('backend.Latitude_must_be_numbers')); ?>",
                    },
                    longitude: {
                        required: "<?php echo e(__('backend.Please_Enter_Longitude')); ?>",
                        number:"<?php echo e(__('backend.Longitude_must_be_numbers')); ?>",
                    },
                    agent_id: {
                        required: "<?php echo e(__('backend.Please_Select_Agent')); ?>",
                    },
                    national_id_number: {
                        required: "<?php echo e(__('backend.Please_Enter_National_Id_Number')); ?>",
                        number: "<?php echo e(__('backend.National_Id_Must_Be_Number')); ?>",
                        rangelength: "<?php echo e(__('backend.National_Id_Must_Be14_Digits')); ?>",
                    },
                    national_id_expired_date: {
                        date:"<?php echo e(__('backend.Please_Enter_Correct_Date_Format')); ?>",
                        required: "<?php echo e(__('backend.Please_Enter_National_Id_Expired_Date')); ?>",
                    },
                    profit: {
                        required: "<?php echo e(__('backend.Please_Enter_Profit')); ?>",
                        number: "<?php echo e(__('backend.Please_Enter_Avalid_Number')); ?>",
                    },
                    basic_salary: {
                        required: "<?php echo e(__('backend.Please_Enter_Basic_Salary')); ?>",
                        number: "<?php echo e(__('backend.Please_Enter_Avalid_Number')); ?>",
                    },
                    bouns_of_delivery: {
                        // required: "<?php echo e(__('backend.Please_Enter_Bonus_Of_Delivery')); ?>",
                        number: "<?php echo e(__('backend.Please_Enter_Avalid_Number')); ?>",
                    },
                    pickup_price: {
                        // required: "<?php echo e(__('backend.Please_Enter_Pickup_Price')); ?>",
                        number: "<?php echo e(__('backend.Please_Enter_Avalid_Number')); ?>",
                    },
                    bouns_of_pickup_for_one_order: {
                        // required: "<?php echo e(__('backend.Please_Enter_Bonus_Of_Pickup_For_One_Order')); ?>",
                        number: "<?php echo e(__('backend.Please_Enter_Avalid_Number')); ?>",
                    },
                    driver_has_vehicle: {
                        required: "<?php echo e(__('backend.Please_Choose_If_Driver_Has_Vehicle_or_No')); ?>"
                    },
                    driver_profile: {
                        required: "<?php echo e(__('backend.Please_Enter_Driver_Profile_Image')); ?>"
                    },
                    national_id_image_front: {
                        required: "<?php echo e(__('backend.Please_Enter_drivier_national_id_image_front')); ?>"
                    },
                    criminal_record_image_front: {
                        required: "<?php echo e(__('backend.please_enter_criminal_record_image_driver')); ?>"
                    },
                    // driving_licence_image_back: {
                    //     required: "<?php echo e(__('backend.Please_Enter_drivier_licence_image_back')); ?>"
                    // },
                    // national_id_image_back: {
                    //     required: "<?php echo e(__('backend.Please_Enter_drivier_national_id_image_back')); ?>"
                    // },
                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                },

                highlight: function (element) {
                    $(element).parent().addClass('error')
                },

                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                },

                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                }
            });

          
            if($( "#driver-has-vehicle-field" ).val() == 1) {
              $('.has_vechile').show();
            } else {
              $('.has_vechile').hide();
            }

            $( "#driver-has-vehicle-field" ).change(function() {
              if( $(this).val() == 1) {

              $('.has_vechile').show();
                  

                  // $('#model_name-field', '#plate_number-field', '#chassi_number-field', '#year-field', '#vehicle_type_id-field', '#model_type_id-field', '#color_id-field', '#driving_licence_expired_date-field', '#license_end_date_field', '#driving_licence_image_front', '#license_image_front').blur();

                  $(".done_create_vehicle").click(function(e){

                    var modelNameError            = true,
                        plateNumberError          = true,
                        chassiNumberError         = true,
                        yearError                 = true,
                        vehicleTypeError          = true,
                        modelTypeError            = true,
                        colorError                = true,
                        drivingLicenceDateError   = true,
                        licenseEndDateError       = true,
                        drivingLicenceImageError  = true,
                        licenseImageError         = true;

                        if($('#model_name-field').val() == ''){
                            modelNameError            = true;
                        } else {
                            modelNameError            = false;
                        }

                        if($('#plate_number-field').val() == ''){
                            plateNumberError            = true;
                        } else {
                            plateNumberError            = false;
                        }

                        if($('#chassi_number-field').val() == ''){
                            chassiNumberError            = true;
                        } else {
                            chassiNumberError            = false;
                        }

                        if($('#year-field').val() == ''){
                            yearError            = true;
                        } else {
                            yearError            = false;
                        }

                        if($('#vehicle_type_id-field').val() == ''){
                            vehicleTypeError            = true;
                        } else {
                            vehicleTypeError            = false;
                        }

                        if($('#model_type_id-field').val() == ''){
                            modelTypeError            = true;
                        } else {
                            modelTypeError            = false;
                        }

                        if($('#color_id-field').val() == ''){
                            colorError            = true;
                        } else {
                            colorError            = false;
                        }

                        if($('#driving_licence_expired_date-field').val() == ''){
                            drivingLicenceDateError            = true;
                        } else {
                            drivingLicenceDateError            = false;
                        }

                        if($('#license_end_date_field').val() == ''){
                            licenseEndDateError            = true;
                        } else {
                            licenseEndDateError            = false;
                        }

                        if($('#driving_licence_image_front').val() == ''){
                            drivingLicenceImageError       = true;
                        } else {
                            drivingLicenceImageError       = false;
                        }

                        if($('#license_image_front').val() == ''){
                            licenseImageError            = true;
                        } else {
                            licenseImageError            = false;
                        }

                        if(modelNameError === true || plateNumberError === true || chassiNumberError === true || yearError === true || vehicleTypeError === true || modelTypeError === true || colorError === true || drivingLicenceDateError === true || licenseEndDateError === true || drivingLicenceImageError === true || licenseImageError === true) {
                            // console.log('error');
                            $('#added_driver').attr('disabled', true);
                            e.preventDefault();
                            $('.error_vehicle').css('display', 'inline-block');
                        } else {
                            // console.log('not error');
                            $('#added_driver').attr('disabled', false);
                            $('.error_vehicle').css('display', 'none');
                        }

                  });

              } else {
              $('.has_vechile').hide();
                  
              }
            });

            if($( "#agent_id-field" ).val() == 1) {
              $('.financial').css('display', 'block');
            } else {
              $('.financial').css('display', 'none');
            }

            $( "#agent_id-field" ).change(function() {
              if( $(this).val() == 1) {
                $('.financial').css('display', 'block');
              } else {
                $('.financial').css('display', 'none');
              }
            });

            $("#government_id").on('change',function(){
                $.ajax({
                  url: "<?php echo e(url('/get_cities')); ?>"+"/"+$(this).val(),
                  type: 'get',
                  data: {},
                  success: function(data) {
                    $('#city_id').html('<option value="" >Choose City</option>');
                    $.each(data, function(i, content) {
                      $('#city_id').append($("<option></option>").attr("value",content.id).text(content.name_en));
                    });
                  },
                  error: function(data) {
                    console.log('Error:', data);
                  }
                });
            });

        })
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/www/ufelix/ufelix/resources/views/backend/drivers/create.blade.php ENDPATH**/ ?>
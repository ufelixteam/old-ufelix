<?php

namespace App\Http\Middleware;

use Closure;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        //This middleware for website
        $lang = $request->segment(1);

        if (empty($lang) || ($lang != 'en' && $lang != 'ar')) {

            $lang = 'ar';

        }/* Make default lang en*/

        app()->setLocale($lang);

        //echo app()->getLocale();die();


        return $next($request);
    }
}

@extends('backend.layouts.app')

@section('css')
<title>{{__('backend.agent')}} - {{__('backend.add_agent')}}</title>
@endsection

@section('header')
  <div class="page-header">
      <h3><i class="glyphicon glyphicon-plus"></i> {{__('backend.agents')}} - {{__('backend.add_agent')}} </h3>
  </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <form action="{{ route('mngrAdmin.agents.store') }}" method="POST" name='agent'>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="row">
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('name')) has-error @endif">
            <label for="name-field">{{__('backend.name')}}: </label>
            <input type="text" id="name-field" name="name" class="form-control" value="{{ old("name") }}"/>
            @if($errors->has("name"))
              <span class="help-block">{{ $errors->first("name") }}</span>
            @endif
          </div>
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('email')) has-error @endif">
            <label for="email-field">{{__('backend.email')}}: </label>
            <input type="text" id="email-field" name="email" class="form-control" value="{{ old("email") }}"/>
            @if($errors->has("email"))
              <span class="help-block">{{ $errors->first("email") }}</span>
            @endif
          </div>
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('mobile')) has-error @endif">
            <label for="mobile-field">{{__('backend.mobile_number')}}: </label>
            <input type="text" id="mobile-field" name="mobile" class="form-control" value="{{ old("mobile") }}"/>
            @if($errors->has("mobile"))
              <span class="help-block">{{ $errors->first("mobile") }}</span>
            @endif
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('phone')) has-error @endif">
            <label for="phone-field">{{__('backend.mobile_number2')}}: </label>
            <input type="text" id="phone-field" name="phone" class="form-control" value="{{ old("phone") }}"/>
            @if($errors->has("phone"))
              <span class="help-block">{{ $errors->first("phone") }}</span>
            @endif
          </div>
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('fax')) has-error @endif">
            <label for="fax-field">{{__('backend.fax')}}: </label>
            <input type="text" id="fax-field" name="fax" class="form-control" value="{{ old("fax") }}"/>
            @if($errors->has("fax"))
              <span class="help-block">{{ $errors->first("fax") }}</span>
            @endif
          </div>
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('commercial_record_number')) has-error @endif">
            <label for="commercial_record_number-field">{{__('backend.commercial_record_number')}}: </label>
            <input type="text" id="commercial_record_number-field" name="commercial_record_number" class="form-control" value="{{ old("commercial_record_number") }}"/>
            @if($errors->has("commercial_record_number"))
              <span class="help-block">{{ $errors->first("commercial_record_number") }}</span>
            @endif
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('country_id')) has-error @endif">
            <label for="country_id-field">{{__('backend.choose_country')}}: </label>
            <select id="country_id-field" name="country_id" class="form-control">
              @if(! empty($countries))
                @foreach($countries as $country)
                  <option value="{{$country->id}}" {{ old("country_id") == $country->id ? 'selected' : '' }}
                    >{{$country->name}}</option>
                @endforeach
              @endif
            </select>
          </div>
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('government_id')) has-error @endif">
            <label for="government_id-field"> {{__('backend.government')}}: </label>
            <select  class="form-control government_id" id="government_id"  name="government_id">
                <option value="" data-display="Select">{{__('backend.government')}}</option>
                @if(! empty($governments))
                    @foreach($governments as $government)
                        <option value="{{$government->id}}" >
                          {{$government->name_en}}
                        </option>

                    @endforeach
                @endif
            </select>
            @if($errors->has("government_id"))
            <span class="help-block">{{ $errors->first("government_id") }}</span>
            @endif
          </div>
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('city_id')) has-error @endif">
            <label class="control-label" for="s_state_id">{{__('backend.city')}}</label>
            <select  class=" form-control city_id" id="city_id"  name="city_id" >
                <option value="Null" >{{__('backend.city')}}</option>
            </select>
            @if($errors->has("city_id"))
            <span class="help-block">{{ $errors->first("city_id") }}</span>
            @endif
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('start_date')) has-error @endif">
            <label for="start_date-field">{{__('backend.start_date')}}: </label>
            <div class='input-group date' id="datepicker1">
              <input type="text" id="start_date-field" name="start_date" class="form-control" value="{{ old("start_date") }}"/>
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
            </div>
            @if($errors->has("start_date"))
              <span class="help-block">{{ $errors->first("start_date") }}</span>
            @endif
          </div>
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('deal_date')) has-error @endif">
            <label for="deal_date-field">{{__('backend.contract_date')}}: </label>
            <div class='input-group date' id="datepicker">
              <input type="text" id="deal_date-field" name="deal_date" class="form-control" value="{{ old("deal_date") }}"/>
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
            </div>
            @if($errors->has("deal_date"))
              <span class="help-block">{{ $errors->first("deal_date") }}</span>
            @endif
          </div>
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('status')) has-error @endif">
            <label for="status-field" style="display: block;">{{__('backend.active')}}: </label>
            <select id="status-field" name="status" class="form-control">
              <option value="0" {{ old("status") == "0" ? 'selected' : '' }}>{{__('backend.no')}}</option>
              <option value="1" {{ old("status") == "1" ? 'selected' : '' }}>{{__('backend.yes')}}</option>
            </select>
            @if($errors->has("status"))
              <span class="help-block">{{ $errors->first("status") }}</span>
            @endif
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('bonus_of_delivery')) has-error @endif">
            <label for="bonus_of_delivery_field">{{__('backend.bonus_of_delivery')}}: </label>
            <input type="number"  id="bonus_of_delivery_field" min="0" name="bonus_of_delivery" class="form-control" value="{{ old("bonus_of_delivery") ? old("bonus_of_delivery") : '0' }}"/>
            @if($errors->has("bonus_of_delivery"))
              <span class="help-block">{{ $errors->first("bonus_of_delivery") }}</span>
            @endif
          </div>
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('pickup_price')) has-error @endif">
            <label for="pickup_price_field">{{__('backend.pickup_price')}}: </label>
            <input type="number"  id="pickup_price_field" min="0" name="pickup_price" class="form-control" value="{{ old("pickup_price") ? old("pickup_price") : '0' }}"/>
            @if($errors->has("pickup_price"))
              <span class="help-block">{{ $errors->first("pickup_price") }}</span>
            @endif
          </div>
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('bonus_of_pickup_for_one_order')) has-error @endif">
            <label for="bonus_of_pickup_for_one_order_field">{{__('backend.bonus_of_pickup_for_one_order')}}: </label>
            <input type="number"  id="bonus_of_pickup_for_one_order_field" min="0" name="bonus_of_pickup_for_one_order" class="form-control" value="{{ old("bonus_of_pickup_for_one_order") ? old("bonus_of_pickup_for_one_order") : '0' }}"/>
            @if($errors->has("bonus_of_pickup_for_one_order"))
              <span class="help-block">{{ $errors->first("bonus_of_pickup_for_one_order") }}</span>
            @endif
          </div>
        </div>


        <div class="row">
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('profit_rate')) has-error @endif">
            <label for="profit_rate-field">{{__('backend.profit_rate')}}: </label>
            <input type="number"  id="profit_rate-field" min="0" name="profit_rate" class="form-control" value="{{ old("profit_rate") ? old("profit_rate") : '0' }}"/>
            @if($errors->has("profit_rate"))
              <span class="help-block">{{ $errors->first("profit_rate") }}</span>
            @endif
          </div>
          <input type="hidden" id="latitude-field" name="latitude" class="form-control" value="0"/>
          <input type="hidden" id="longitude-field" name="longitude" class="form-control" value="0"/>
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('password')) has-error @endif">
            <label for="password-field">{{__('backend.password')}}: </label>
            <input type="password" id="password-field" name="password" class="form-control" value="{{ old("password") }}"/>
            @if($errors->has("password"))
              <span class="help-block">{{ $errors->first("password") }}</span>
            @endif
          </div>
          <div class="form-group col-md-4 col-sm-4 @if($errors->has('password_confirmation')) has-error @endif">
            <label for="confirm_password-field">{{__('backend.confirm_password')}}: </label>
            <input type="password" id="confirm_password-field" name="password_confirmation" class="form-control" value="{{ old("password_confirmation") }}"/>
              @if($errors->has("password_confirmation"))
                <span class="help-block">{{ $errors->first("password_confirmation") }}</span>
              @endif
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-12 col-sm-6 @if($errors->has('address')) has-error @endif">
            <label for="address-field">{{__('backend.address')}}: </label>
            <textarea class="form-control" id="address-field" rows="4" name="address">{{ old("address") }}</textarea>
            @if($errors->has("address"))
              <span class="help-block">{{ $errors->first("address") }}</span>
            @endif
          </div>
        </div>
        <div class="well well-sm col-md-12 col-sm-12 col-xs-12">
            <button type="submit" class="btn btn-primary">{{__('backend.add_agent')}}</button>
            <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.agents.index') }}"><i class="glyphicon glyphicon-backward"></i>{{__('backend.back')}}</a>
        </div>
      </form>
    </div>
  </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(function () {
            $("form[name='agent']").validate({
                // Specify validation rules
                rules: {
                    name: {
                        required: true,
                    },
                    mobile: {
                        required: true,
                        number:true,
                        rangelength:[11,11],
                        remote: {
                            url: "{!! url('/check-mobile-agent') !!}",
                            type: "get"
                        }
                    },
                    phone: {
                        number:true,
                        rangelength:[11,11],
                        remote: {
                            url: "{!! url('/check-phone-agent') !!}",
                            type: "get"
                        }
                    },
                    email: {
                        required: true,
                        email: true,
                        remote: {
                            url: "{!! url('/check-email-agent') !!}",
                            type: "get"
                        }
                    },
                    commercial_record_number: {
                        required: true,
                        number: true,
                    },
                    country_id: {
                      required: true,
                    },
                    government_id: {
                      required: true,
                    },
                    city_id: {
                      required: true,
                    },
                    password: {
                        required: true,
                        minlength: 5
                    },
                    password_confirmation: {
                        required: true,
                        equalTo: '#password-field',
                        minlength: 5
                    },
                    profit_rate: {
                      number: true,
                    },
                    bonus_of_delivery: {
                        // required: true,
                        number:true,
                    },
                    pickup_price: {
                        // required: true,
                        number:true,
                    },
                    bonus_of_pickup_for_one_order: {
                        // required: true,
                        number:true,
                    },
                    deal_date: {
                      date: true,
                    },
                    start_date: {
                      date: true,
                    },
                },
                // Specify validation error messages
                messages: {
                    name: {
                        required: "{{__('backend.Please_Enter_Agent_Name')}}",
                    },
                    mobile: {
                        required: "{{__('backend.Please_Enter_Agent_Mobile_Number')}}",
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                        rangelength:"{{__('backend.Mobile_Must_Be11_Digits')}}",
                        remote: jQuery.validator.format("{{__('backend.Mobile_Number_already_exist')}}")
                    },
                    phone: {
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                        rangelength:"{{__('backend.Mobile_Must_Be11_Digits')}}",
                        remote: jQuery.validator.format("{{__('backend.Mobile_Number2_already_exist')}}")
                    },
                    email: {
                        required: "{{__('backend.Please_Enter_Agent_E-mail')}}",
                        email: "{{__('backend.Please_Enter_Correct_E-mail')}}",
                        remote: jQuery.validator.format("{{__('backend.E-mail_already_exist')}}")
                    },
                    commercial_record_number: {
                        required: "{{__('backend.Please_Enter_Commercial_Record_Number')}}",
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                    },
                    country_id: {
                      required: "{{__('backend.Please_Chosse_The_Country')}}",
                    },
                    government_id: {
                      required: "{{__('backend.Please_Chosse_The_Government')}}",
                    },
                    city_id: {
                      required: "{{__('backend.Please_Select_City')}}",
                    },
                    password: {
                        required: "{{__('backend.Please_Enter_Agent_Password')}}",
                        minlength: "{{__('backend.Password_must_be_more_than5_character')}}"
                    },
                    password_confirmation: {
                        required: "{{__('backend.Please_Enter_Agent_confirm_Password')}}",
                        equalTo: "{{__('backend.Confirm_password_is_wrong')}}"
                    },
                    profit_rate: {
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                    },
                    bonus_of_delivery: {
                        // required: "{{__('backend.Please_Enter_Bonus_Of_Delivery')}}",
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                    },
                    pickup_price: {
                        // required: "{{__('backend.Please_Enter_Pickup_Price')}}",
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                    },
                    bonus_of_pickup_for_one_order: {
                        // required: "{{__('backend.Please_Enter_Bonus_Of_Pickup_For_One_Order')}}",
                        number: "{{__('backend.Please_Enter_Avalid_Number')}}",
                    },
                    deal_date: {
                        date: "{{__('backend.Please_enter_avalid_date')}}",
                    },
                    start_date: {
                        date: "{{__('backend.Please_enter_avalid_date')}}",
                    },
                },

                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error)
                },
                highlight: function (element) {
                    $(element).parent().addClass('error')
                },
                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                }
            });


            $("#government_id").on('change',function(){
                $.ajax({
                  url: "{{ url('/get_cities')}}"+"/"+$(this).val(),
                  type: 'get',
                  data: {},
                  success: function(data) {
                    $('#city_id').html('<option value="" >Choose City</option>');
                    $.each(data, function(i, content) {
                      $('#city_id').append($("<option></option>").attr("value",content.id).text(content.name_en));
                    });
                  },
                  error: function(data) {
                    console.log('Error:', data);
                  }
                });
            });


        })
    </script>
@endsection

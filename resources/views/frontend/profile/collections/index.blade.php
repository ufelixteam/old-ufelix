@extends('frontend.profile.layouts.app')
@section('title') Collection Orders @endsection

@section('content')
    <div class="flash-message"></div>

    <div class="alert alert-danger" hidden>
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong id="errorId"></strong>
    </div>
    @if (session('success') || !empty($success))
        <div class="alert alert-success alert-block text-center">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ !empty($success) ? $success : session('success') }}</strong>
        </div>
    @endif
    @if (session('warning'))
        <div class="alert alert-warning alert-block text-center">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ session('warning') }}</strong>
        </div>
    @endif

    <div class="row">
        <div class="col-12">
            <div class="">
                <div class="table-responsive m-t-40">
                    <h1 class="title"> @lang('website.FormCollection')</h1>
                    <div class="button-box">
                        <a href="{{ app()->getLocale() == 'en'  ? url('/en/customer/account/order-form') :  url('/customer/account/order-form') }}"
                           class="btn btn-primary pull-right" style=""><i style="margin: 0 10px"
                                                                          class="mdi mdi-plus"></i>@lang('website.AddCollection')
                        </a>
                    </div>
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered"
                           cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>@lang('website.CreatedAt')</th>
                            <th> @lang('website.Saved')</th>
                            @if($customer->is_manager)
                                <th>@lang('website.client')</th>
                            @endif
                            <th>@lang('website.Actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(! empty($collections) )
                            @foreach($collections as $collection)
                                <tr>
                                    <td>{{$collection->id}}</td>

                                    <td>{{$collection->created_at}}</td>
                                    <td>{!! $collection->saved_span !!}</td>
                                    @if($customer->is_manager)
                                        <td>{{$collection->customer->name}}</td>
                                    @endif
                                    <td>
                                        @if($collection->is_saved != 1)
                                            <a class="btn btn-xs @if($collection->complete_later == 1) btn-success @else btn-info @endif"
                                               href="{{ app()->getLocale() == 'en' ? url('/en/customer/account/order-form?collection='.$collection->id) : url('/customer/account/order-form?collection='.$collection->id) }}">
                                                @if($collection->complete_later == 1)
                                                    Complete
                                                @else
                                                    Edit
                                                @endif

                                            </a>
                                        @else
                                            <a class="btn btn-xs btn-primary "
                                               href="{{ app()->getLocale() == 'en' ? url('/en/customer/account/orders?collection='.$collection->id) : url('/customer/account/orders?collection='.$collection->id) }}">
                                                List Orders
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach

                        @endif
                        </tbody>
                    </table>
                    @if (! empty($collections) )
                        {!! $collections->appends($_GET)->links() !!}
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection

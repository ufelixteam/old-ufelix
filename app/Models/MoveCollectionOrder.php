<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MoveCollectionOrder extends Model
{

    protected $guarded = ['id'];

    public function collection()
    {
        return $this->belongsTo('App\Models\MoveCollection', 'collection_id');
    }

    public function getDroppedSpanAttribute()
    {
        if ($this->dropped) {
            return "<span class='badge badge-pill label-success'>" . __('backend.dropped') . "</span>";
        } else {
            return "<span class='badge badge-pill label-danger'>" . __('backend.not_dropped') . "</span>";
        }

    }
}

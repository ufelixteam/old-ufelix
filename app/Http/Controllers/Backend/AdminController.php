<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Contact;
use Auth;
use File;
class AdminController extends Controller
{

    #list users
    public function index() {
        $users = User::where('role_id', '1')->paginate(15);
        return view('backend.users.index', compact('users'));
    }

    public function create() {
        return view('backend.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request) {
         $this->validate($request, [
            'name' => 'string|max:255',
            'email' => 'max:255|unique:users',
            'mobile' => 'required|string|max:255|unique:users',
            'password' => 'required|min:4|confirmed',
        ]);
        $user = new User();
        $user->name = $request->input("name");
        $user->email = $request->input("email");
        $user->mobile = $request->input("mobile");
        $user->is_active = 1;
        $user->password = bcrypt($request->input("password"));
        $user->save();
        return redirect()->route('mngrAdmin.users.index')->with('message', __('backend.Admin_is_added_successfully'));
    }

    public function block_user(Request $request) {
        $user = User::findOrFail($request->user_id);
        if($user->is_active == 1){
            $user->is_active = 0;
            $msg = __('backend.user_is_unblocked_successfully');
        } else {
            $user->is_active = 1;
            $msg = __('backend.User_is_blocked_Successfully');
        }
        return response()->json([
          'data' => [
            'success' => $user->save(),
            'message'=> $msg,
          ]
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        $user = User::findOrFail($id);
        return view('backend.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id){
        $user = User::findOrFail($id);
        return view('backend.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param Request $request
     * @return Response
     */
    public function update(Request $request, $id) {
         $this->validate($request, [
            'name' => 'string|max:255',
            'email' => 'email|max:255|unique:users,email,'.$id,
            'mobile' => 'required|string|max:255|unique:users,mobile,'.$id,
        ]);
        $user = User::findOrFail($id);
        $user->name = $request->input("name");
        $user->email = $request->input("email");
        $user->mobile = $request->input("mobile");
        $user->save();
        return redirect()->route('mngrAdmin.users.index')->with('message', __('backend.User_is_updated_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->route('mngrAdmin.users.index')->with('message', __('backend.User_deleted_successfully'));
    }

}

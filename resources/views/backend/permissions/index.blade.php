@extends('backend.layouts.app')

@section('css')
  <title>{{__('backend.permissions')}}</title>
@endsection

@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> {{__('backend.permissions')}}
        </h3>

    </div>
@endsection

@section('content')
    @if(permission('addPermission')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
      <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
              <form action="{{ route('mngrAdmin.permissions.store') }}" method="POST" name='validateForm'>
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">

                  <div class="form-group col-md-6 col-sm-6 @if($errors->has('name')) has-error @endif">
                     <label for="name-field">{{__('backend.permission_name')}}:</label>
                     <input type="text" id="name-field" name="name" class="form-control" value="{{ old("name") }}"/>
                     @if($errors->has("name"))
                      <span class="help-block">{{ $errors->first("name") }}</span>
                     @endif
                  </div>

                  <div class="form-group col-md-6 col-sm-6 @if($errors->has('type_permission_id')) has-error @endif">
                    <label for="type_permission_id" style="display: block;">{{__('backend.permission_type')}}: </label>
                    <select id="type_permission_id" name="type_permission_id" class="form-control">
                      <option value="" {{ old("type_permission_id") == "" ? 'selected' : '' }}>{{__('backend.choose_permission_type')}}</option>
                      @foreach($typePermission as $type)
                        <option value="{{$type->id}}" {{ old("type_permission_id") == $type->id ? 'selected' : '' }}>{{$type->name}}</option>
                      @endforeach
                    </select>
                    @if($errors->has("type_permission_id"))
                      <span class="help-block">{{ $errors->first("type_permission_id") }}</span>
                    @endif
                  </div>

                  <div class="well well-sm col-md-12 col-sm-12 col-xs-12">
                      <button type="submit" class="btn btn-primary">{{__('backend.create')}}</button>
                      <a class="btn btn-link pull-right" href="{{ route('mngrAdmin.permissions.index') }}"><i class="glyphicon glyphicon-backward"></i> {{__('backend.back')}}</a>
                  </div>
              </form>
          </div>
      </div>
    @endif

    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            @if($permissions->count())
                <table class="table table-condensed table-striped">
                    <thead>
                      <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">{{__('backend.permission_name')}}</th>
                        <th class="text-center">{{__('backend.permission_type')}}</th>
                        <th class="text-right"></th>
                      </tr>
                    </thead>

                    <tbody>
                        @foreach($permissions as $permission)
                            <tr>
                                <td class="text-center">{{$permission->id}}</td>
                                <td class="text-center">{{$permission->name}}</td>
                                <td class="text-center">{{!empty ($permission->typePermission) ? $permission->typePermission->name : 'Not Found'}}</td>
                                <td class="text-right">

                                  @if(permission('editPermission')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                    <a class="btn btn-xs btn-warning"  data-toggle="modal" data-target="#{{$permission->id}}"><i class="glyphicon glyphicon-edit"></i> {{__('backend.edit')}}</a>

                                    <!-- Modal -->
                                    <div class="modal fade" id="{{$permission->id}}" role="dialog">
                                      <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title pull-left">{{__('backend.Edit_Permission')}} - <b>{{$permission->name}}</b> </h4>
                                          </div>

                                          <form action="{{ route('mngrAdmin.permissions.update', $permission->id) }}" method="POST">
                                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                              <input type="hidden" name="_method" value="PUT">

                                              <div class="modal-body">

                                                <div class="form-group col-md-12 col-sm-12 @if($errors->has('name')) has-error @endif">
                                                   <label for="name-field">{{__('backend.permission_name')}}:</label>
                                                   <input type="text" id="name-field" name="name" class="form-control" value="{{$permission->name}}"/>
                                                   @if($errors->has("name"))
                                                    <span class="help-block">{{ $errors->first("name") }}</span>
                                                   @endif
                                                </div>

                                                <div class="form-group col-md-12 col-sm-12 @if($errors->has('type_permission_id')) has-error @endif">
                                                  <label for="type_permission_id" style="display: block;">{{__('backend.permission_type')}}: </label>
                                                  <select id="type_permission_id" name="type_permission_id" class="form-control">
                                                    <option value="" data-display="Select">{{__('backend.choose_permission_type')}}</option>
                                                    @if(!empty ($typePermission))
                                                      @foreach($typePermission as $type)
                                                        <option value="{{$type->id}}"  @if($type->id == $permission->type_permission_id) selected @endif>{{$type->name}}</option>
                                                      @endforeach
                                                    @endif
                                                  </select>
                                                  @if($errors->has("type_permission_id"))
                                                    <span class="help-block">{{ $errors->first("type_permission_id") }}</span>
                                                  @endif
                                                </div>

                                              </div>
                                              <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">{{__('backend.edit')}}</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('backend.close')}}</button>
                                              </div>
                                          </form>
                                        </div>

                                      </div>
                                    </div>
                                  @endif

                                  @if(permission('deletePermission')) <!-- This Function Displays All The Buttons For All Peoples Have The Permission [Helpers.php] -->
                                    <!-- <form action="{{ route('mngrAdmin.permissions.destroy', $permission->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('{{__('backend.msg_confirm_delete')}}')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> {{__('backend.delete')}}</button>
                                    </form> -->
                                  @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $permissions->render() !!}
            @else
                <h3 class="text-center alert alert-warning">{{__('backend.No_result_found')}}</h3>
            @endif

        </div>
    </div>

@endsection

@section('scripts')
   <script type="text/javascript">
  $(function() {
    $("form[name='validateForm']").validate({
      // Specify validation rules
      rules: {
        name: {
          required: true,
        },
        type_permission_id: {
          required: true,
        },
      },

      // Specify validation error messages
      messages: {
        name: {
          required: "{{__('backend.Please_Enter_Permission_Name')}}",
        },
        type_permission_id: {
          required: "{{__('backend.Please_Chosse_Type_Permission')}}",
        },
      },

       errorPlacement: function(error, element) {
          $(element).parents('.form-group').append(error)
      },

      highlight: function (element) {
          $(element).parent().addClass('error')
      },

      unhighlight: function (element) {
          $(element).parent().removeClass('error')
      },
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function(form) {
          form.submit();
      }
    });
  })
</script>
@endsection
